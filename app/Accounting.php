<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounting extends Model
{
    public function from(){
        return $this->belongsTo("App\User", "user_from");
    }

    public function to(){
        return $this->belongsTo("App\User", "user_to");
    }
}
