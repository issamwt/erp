<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    public function tasks(){
        return $this->hasMany("App\Task", "milestone_id");
    }
}
