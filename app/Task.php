<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function users(){
        return $this->belongsToMany("App\User", "task_user");
    }

    public function user(){
        return $this->belongsTo("App\User");
    }

    public function meeting(){
        return $this->belongsTo("App\Meeting", "daftar_id");
    }

    public function taskStatus(){
        return $this->belongsTo("App\TaskStatus", "status");
    }

    public function project(){
        return $this->belongsTo("App\Project", "daftar_id");
    }

    public function attachments(){
        return $this->morphMany('App\Attachment', 'attachmentable');
    }

    public function milestone(){
        return $this->belongsTo("App\Milestone", "milestone_id");
    }

    public function subtasks(){
        return $this->HasMany("App\Subtask");
    }
    public function comments(){
        return $this->HasMany("App\Comment", "parent_id")->with("user");
    }

    
/*

      public static function myTasks($userId, $permissions){
        if(in_array('all_cases', $permissions))
            return Task::where("id", ">", 0);
        else
            return Task::whereHas('users', function ($q) use ($userId) {
                $q->where('user_id', $userId);
            })->orWhereHas("acase.users", function ($q) use ($userId){
                $q->where('user_id', $userId)->where("type", 1);
            });
    }
    public function charged(){
        return $this->belongsToMany("App\User", "task_user", "task_id", "user_id")->where('type', 1);
    }

    public function collaborators(){
        return $this->belongsToMany("App\User", "task_user", "task_id", "user_id")->where('type', 0);
    }








    public function acase(){
        return $this->belongsTo("App\ACase", "case_id");
    }





    public function fields(){
        return $this->hasMany("App\Field", "other_id")->where("type", "tasks");
    }
*/
}
