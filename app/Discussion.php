<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    public function attachments(){
        return $this->morphMany('App\Attachment', 'attachmentable');
    }
    
    public function user(){
        return $this->belongsTo("App\User", "user_id");
    }

    public function discussions(){
        return $this->HasMany("App\Discussion", "discussion_id")->orderby("id", "desc");
    }
}
