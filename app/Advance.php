<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advance extends Model
{
    public function kafil(){
        return $this->belongsTo("App\User", "kafil_id");
    }
}
