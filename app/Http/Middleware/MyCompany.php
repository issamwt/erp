<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Company;

class MyCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $company = Company::where("slug", @$request->company)->first();
        if(!$company)
            return abort(404);

        if(Auth::guard("user")->check()){
            if(Auth::guard("user")->user()->company_id!=$company->id)
                return abort(404);
        }

        return $next($request);
    }
}
