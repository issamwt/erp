<?php

namespace App\Http\Middleware;

use Closure;
use App\Company;

class Daftar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $daftar)
    {
        $company = Company::where("slug", @$request->company)->first();

        if($company->{$daftar}!=1)
            return redirect()->back();

        return $next($request);
    }
}
