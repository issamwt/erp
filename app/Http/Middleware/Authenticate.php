<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if(empty($guards)){
            if (!Auth::guard('web')->check()) {
                return redirect()->route('login');
            }
        }else{
            $guard = $guards[0];
            switch (@$guard) {
                case 'web':
                    if (!Auth::guard($guard)->check()) {
                        return redirect()->route('login');
                    }
                    break;
                case 'user':
                    if (!Auth::guard("user")->check()) {
                        return redirect()->route('company.login', @$request->company);
                    }
                    break;
                default:
                    if (!Auth::guard($guard)->check()) {
                        return redirect('/home');
                    }
                    break;
            }
        }

        return $next($request);
    }
}
