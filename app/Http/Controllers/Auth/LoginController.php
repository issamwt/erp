<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use App\Admin;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'phone';
    }

    function convert($string){
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠'];
        $num = range(9, 0);
        $englishNumbersOnly = str_replace($arabic, $num, $string);
        return $englishNumbersOnly;
    }

    public function send_sms($receiver, $message)
    {
        $receiver = str_replace(" ", "", $receiver);
        $receiver = str_replace("-‎", "", $receiver);
        $receiver = str_replace("(‎", "", $receiver);
        $receiver = str_replace(")‎", "", $receiver);
        $receiver = $this->convert($receiver);

        if ($receiver == "")
            return;
        $receiver = str_replace(" ", "", $receiver);

        if (0 === strpos($receiver, '00') or 0 === strpos($receiver, '+'))
            $receiver = $receiver;
        else{
            $receiver = "00966" . substr($receiver, -9);
        }

        $login = '966502480256';
        $pass = 'Aa@0502480256';
        $sender = 'ERP';
        $data = array(
            'Username' => $login,
            'Password' => $pass,
            'Tagname' => $sender,
            'RecepientNumber' => str_replace('+', '00', $receiver),
            'Message' => $message,
            'SendDateTime' => 0,
            'EnableDR' => false
        );
        $data_string = json_encode($data, JSON_UNESCAPED_UNICODE);
        $ch = curl_init('http://api.yamamah.com/SendSMS');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        $response->StatusDescription;
        return (@$response->Status) ? @$response->Status : 0;
    }

    public function login(Request $request){
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();

            /*if(Auth::attempt(['phone' => $request->phone, 'password' => $request->input('password'), 'status' => 1])){
                return redirect()->route('admin.home');
            }*/

            $user->status = 0;
            $user->activation_code = $this->generateActivationCode();
            date_default_timezone_set("Asia/Riyadh");
            $user->code_sent_at = (Carbon::now());
            $user->save();
            $msg = 'كود التفعيل الخاص بك هو : '.$user->activation_code;
            if($this->send_sms($user->phone, $msg)){
                $request->session()->put('phone', $request->input('phone'));
                $request->session()->put('password', $request->input('password'));
                return view('auth.verification', ['id' => $user->id])->with(['user' => $user]);
            }
            dd("not here");
            if ($user->status==1 && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
                $this->incrementLoginAttempts($request);
                return redirect()->route('verification', ['id' => $user->id]);
            }
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function generateActivationCode(){
        $x = rand(111111,999999);
        return $x;
    }

    public function sendNewVerificationCode($id, Request $request){
        $request->session()->keep(['phone', 'password']);

        $user = Admin::find($id);
        if(!$user){
            return redirect()->route('admin.home');
        }

        if($user->status==0){
            date_default_timezone_set("Asia/Riyadh");
            $sentAt = Carbon::now();
            $user->code_sent_at = $sentAt;
            $user->activation_code = $this->generateActivationCode();
            $user->save();
            $msg = 'كود التفعيل الخاص بك هو : '.$user->activation_code;
            if($this->send_sms($user->phone, $msg)){
                $request->session()->flash('success','تم إرسال كود تفعيل جديد');
                return view('auth.verification', ['id' => $user->id])->with(['user' => $user]);
            }else{
                $request->session()->flash('error','عفوا .. لم نستطع إرسال كود التغعيل الخاص بك.');
                return view('auth.verification', ['id' => $user->id])->with(['user' => $user]);
            }
        }else{
            return redirect()->route('admin.home');
        }
    }

    public function verifyUser($id, Request $request){
        $request->session()->keep(['phone', 'password']);

        $user = Admin::find($id);
        if(!$user){
            return redirect()->route('index');
        }
        
        if($user->status==0) {
            date_default_timezone_set("Asia/Riyadh");
            $now = Carbon::now();
            $sentAt = (Carbon::parse($user->code_sent_at))->addMinutes(5);
            $diff = $now->diffInSeconds($sentAt, false);
            if($diff>0){
                if ($user->activation_code == $request->input('verification_code')) {
                    $user->status = 1;
                    $user->save();
                    $phone = $request->session()->get('phone',0);
                    $password = $request->session()->get('password','');
                    if (Auth::attempt(['phone' => $phone, 'password' => $password, 'status' => 1])) {
                        $request->session()->forget('phone');
                        $request->session()->forget('password');
                        return redirect()->route('admin.home');
                    }else{
                        return redirect()->route('login');
                    }
                } else {
                    $request->session()->flash('error','كود التفعيل غير صحيح');
                    return view('auth.verification', ['id' => $user->id])->with(['user' => $user]);
                }
            }else{
                $request->session()->flash('error','عفوا .. إنتهى الوقت المسموح لإدخال هذا الكود');
                return view('auth.verification', ['id' => $user->id])->with(['user' => $user]);
            }
        }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect()->route("admin.home");
    }

}
