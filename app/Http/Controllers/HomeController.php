<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Message;
use Password;
use Auth;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\User;
use App\Company;
use App\Meeting;
use App\Invited;
use App\Setting;
use App\Task;

class HomeController extends Controller
{
    use ResetsPasswords;

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('front.home');
    }

    public function contact(Request $request){
        $message = new Message;
        $message->name = $request->name;
        $message->phone = $request->phone;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->save();
    }

    public function showResetForm(Request $request, $token = null){
        return view('company.passwords_reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $slug = null;
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) use($slug) {
            $this->resetPassword($user, $password);
            Auth::guard('user')->login($user);
            $company = Company::find($user->company_id);
            $slug = $company->slug;
        }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? ($slug)?redirect()->route("company.home", $slug):$this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    public function broker()
    {
        return Password::broker("users");
    }

    protected function guard()
    {
        return Auth::guard("user");
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        $this->guard()->login($user);
    }

    // meetting

    public function meeting($meeting_id=null, $user_id=null){
        if(!$meeting_id or !$user_id)
            return view("front.meeting.blank");
        $userx       = User::find($user_id);
        $company    = Company::find(@$userx->company_id);
        $meeting    = Meeting::find($meeting_id);
        if(!$userx or !$company or !$meeting)
            return view("front.meeting.blank");

        $users      = collect();
        $users->push($meeting->manager);
        if($meeting->amin) $users->push($meeting->amin);
        foreach ($meeting->users as $u) $users->push($u);
        foreach ($meeting->clients as $c) $users->push($c);
        $ids = []; foreach ($users as $u) array_push($ids, $u->id);

        if(!in_array($user_id, $ids))
            return view("front.meeting.blank");

        if($meeting->status!=0)
            return view("front.meeting.blank");

        $invited = Invited::where(["meeting_id"=>$meeting_id, "user_id"=>$user_id])->first();
        $userx->invited_infos = $invited;

        return view("front.meeting.invited", compact("userx", "meeting", "company", "invited"));

    }

    public function save_invited(Request $request){
        $invited = Invited::where(["meeting_id"=>$request->meeting_id, "user_id"=>$request->user_id])->first();
        if(!$invited)
            $invited = new Invited;

        $invited->meeting_id = $request->meeting_id;
        $invited->user_id = $request->user_id;
        $invited->attendance = $request->attendance;
        $invited->note = $request->note;
        $invited->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function reminder(){
        date_default_timezone_set("Asia/Riyadh");
        $meetings = Meeting::where("status", 0)->get();
        foreach ($meetings as $meeting){
            $day        = Setting::where("key", "reminder_day".$meeting->company_id)->first();
            $hour       = Setting::where("key", "reminder_hour".$meeting->company_id)->first();
            $minute     = Setting::where("key", "reminder_minute".$meeting->company_id)->first();
            if($day and $hour and $minute){
                $day = $day->value;
                $hour = $hour->value;
                $minute = $minute->value;
                $before = $day*24*60+$hour*60+$minute;
                $now    = Carbon::now();
                $meeting_time = $meeting->from_date." ".$meeting->from_time.":00";
                $meeting_time = Carbon::parse($meeting_time)->format('Y-m-d H:i:s');
                $difference   = $now->diffInMinutes($meeting_time);

                if($meeting_time>=$now){
                    if($difference==$before){
                        $users=[$meeting->manager_id, $meeting->amin_id];
                        foreach ($meeting->users as $u) array_push($users, $u->id);
                        foreach ($meeting->clients as $u) array_push($users, $u->id);
                        $setting = Setting::where("key", "reminder_message".$meeting->company_id)->first();
                        if($setting)
                            $msg = @$setting->value;
                        else
                            $msg = "هذه رسالة لتذكيرك بالإجتماع.";
                        foreach ($users as $id){
                            $user = User::find($id);
                            if($user){
                                $email = $user->email;
                                $phone = $user->phone;
                                $message = "الإجتماع : ".$meeting->name;
                                $url = route("meeting_url", ["meeting"=>$meeting->id, "user_id"=>$user->id]);
                                $urllink = "<a href='".$url."'>".$url."</a>";
                                mailit($email, $message, $message."<br>".$msg."<br>".$urllink, $meeting->company_id, "meetings");
                                sms_yamamah($phone, $message."\n".$msg."\n\n".$url, $meeting->company_id, "meetings");
                                fcmit("user_".$id, ["message" => $message." : ".$msg]);
                            }
                        }
                    }
                }
            }
        }
    }

    // cronjob

    public function cronjob(){
        $tasks      = Task::where("status", "!=", 1)->where("daftar", "prm")->orderBy('id', 'desc')->get();

        foreach ($tasks as $task){
            $d = new \DateTime($task->to_date);
            $today = new \DateTime();
            if($d<$today){
                echo $task->name."<br>";
                $task->status = 2;
                $task->save();
            }
        }
    }

}
