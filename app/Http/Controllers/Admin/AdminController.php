<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;

class AdminController extends Controller
{
    public function index(){
        $admins = Admin::orderby("id", "desc")->get();

        return view("admin.admins.index", compact("admins"));
    }

    public function create(){
        return view("admin.admins.create");
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'phone' => 'required|string|max:255|unique:admins',
            'password' => 'required|string|min:6',
        ]);
        $admin              = new Admin;
        $admin->name        = $request->name;
        $admin->phone       = $request->phone;
        $admin->email       = $request->email;
        $admin->password    = bcrypt($request->password);
        $admin->save();

        return redirect()->route("admin.admins.index")->with("success", "تم حفظ المستخدم الجديد !");
    }

    public function show($id){
        //
    }

    public function edit($id=null){
        $admin = Admin::find($id);
        if(!$id or !$admin)
            return redirect()->back();

        return view("admin.admins.edit", compact("admin"));
    }

    public function update(Request $request, $id){
        $admin = Admin::find($id);
        if(!$id or !$admin)
            return redirect()->back();

        $request->validate(['name' => 'required|string|max:255', 'email' => 'required|string|email|max:255','phone' => 'required|string|max:255']);
        if($request->password)
            $request->validate(['password' => 'required|string|min:6']);
        $other = Admin::where("email", $request->email)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "البريد الإلكتروني مستعمل !");
        $other = Admin::where("phone", $request->phone)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "رقم الجوال مستعمل !");

        $admin->name        = $request->name;
        $admin->phone       = $request->phone;
        $admin->email       = $request->email;
        if($request->password)
            $admin->password    = bcrypt($request->password);
        $admin->save();

        return redirect()->back()->with("success", "تم حفظ المستخدم الجديد !");
    }
    
    public function destroy($id){
        $admin = Admin::find($id);
        $admin->delete();

        return redirect()->route("admin.admins.index")->with("success", "تم حذف المستخدم !");
    }
}
