<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\User;

class CompaniesController extends Controller
{
    public function index(){
        $companies = Company::orderby("id", "desc")->get();

        return view("admin.companies.index", compact("companies"));
    }

    public function add_company(){
        return view("admin.companies.add");
    }

    public function save_company(Request $request){
        $request->validate(["name"=>"required|string"]);
        $request->validate(["slug"=>"required|string|unique:companies"]);
        $request->validate([
            'namex' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $company            = new Company;
        $company->name      = $request->name;
        $company->slug      = str_replace(" ", "", trim($request->slug));
        $company->meetings  = ($request->meetings)?1:0;
        $company->crm       = ($request->crm)?1:0;
        $company->prm       = ($request->prm)?1:0;
        $company->hrm       = ($request->hrm)?1:0;
        $company->cards     = ($request->cards)?1:0;
        $company->passwords = ($request->passwords)?1:0;

        if($request->hasFile("image")){
            $request->validate(["image"=>"required|image"]);
            $path = $request->file('image')->store('uploads/companies');
            $company->image = $path;
        }

        $company->save();

        $user              = new User;
        $user->name        = $request->namex;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->role_id     = 1;
        $user->type        = 0;
        $user->password    = bcrypt($request->password);
        $user->company_id       = $company->id;
        $user->save();

        return redirect()->route("admin.companies")->with("success", "تم إضافة الشركة بنجاح");
    }

    public function edit_company($id=null){
        $company = Company::find($id);
        if(!$id or !$company)
            return redirect()->back();

        return view("admin.companies.edit", compact("company"));
    }

    public function update_company(Request $request){
        $company = Company::find($request->id);
        if(!$request->id or !$company)
            return redirect()->back();

        $request->validate(["name"=>"required|string"]);
        $request->validate(["slug"=>"required|string"]);
        $others = Company::where("slug", $request->slug)->where("id", "!=", $request->id)->first();
        if($others)
            return redirect()->back()->with("error", "الرابط مستعمل من قبل");

        $company->name      = $request->name;
        $company->slug      = str_replace(" ", "", trim($request->slug));
        $company->meetings  = ($request->meetings)?1:0;
        $company->crm       = ($request->crm)?1:0;
        $company->prm       = ($request->prm)?1:0;
        $company->hrm       = ($request->hrm)?1:0;
        $company->cards     = ($request->cards)?1:0;
        $company->passwords = ($request->passwords)?1:0;

        if($request->hasFile("image")){
            $request->validate(["image"=>"required|image"]);
            $path = $request->file('image')->store('uploads/companies');
            $company->image = $path;
        }

        $company->save();

        return redirect()->back()->with("success", "تم تعديل الشركة بنجاح");
    }
    
    public function delete_company($id=null){
        return back()->with("error", "لا يمكن حذف الشركات حاليا !");
    }

}
