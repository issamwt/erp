<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessageController extends Controller
{
    public function index(){
        $messages = Message::orderby("created_at", "asc")->get();

        return view("admin.messages.index", compact("messages"));
    }

    public function create(){

    }

    public function store(Request $request){

    }

    public function show($id){

    }

    public function edit($id=null){
    }

    public function update(Request $request, $id){

    }

    public function destroy($id){
        $message = Message::find($id);
        $message->delete();

        return redirect()->route("admin.messages.index")->with("success", "تم حذف الرسالة !");
    }
}
