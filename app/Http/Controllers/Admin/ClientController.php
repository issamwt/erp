<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;

class ClientController extends Controller
{
    public function index(){
        $clients = User::where("type", 1)->with("company")->orderby("id", "desc")->get();

        return view("admin.clients.index", compact("clients"));
    }

    public function create(){
        $companies = Company::orderby("name", "asc")->get();

        return view("admin.clients.create", compact("companies"));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|unique:users',
            'password' => 'required|string|min:6',
            'company_id' => 'required',
            'image' => 'required|image',
        ]);
        $client            = new User;
        $client->name        = $request->name;
        $client->email       = $request->email;
        $client->phone       = $request->phone;
        $client->role_id     = 0;
        $client->type        = 1;
        $client->password    = bcrypt($request->password);
        $client->company_id  = $request->company_id;
        $path = $request->file('image')->store('uploads/clients');
        $client->image = $path;
        $client->save();

        return redirect()->route("admin.clients.index")->with("success", "تم حفظ العميل الجديد !");
    }

    public function show($id){
        //
    }

    public function edit($id=null){
        $client = User::find($id);
        if(!$id or !$client)
            return redirect()->back();
        $companies = Company::orderby("name", "asc")->get();

        return view("admin.clients.edit", compact("client", "companies"));
    }

    public function update(Request $request, $id){
        $client = User::find($id);
        if(!$id or !$client)
            return redirect()->back();

        $request->validate(['name' => 'required|string|max:255', 'email' => 'required|string|email|max:255','company_id' => 'required',]);
        if($request->password)
            $request->validate(['password' => 'required|string|min:6']);

        $other = User::where("email", $request->email)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "البريد الإلكتروني مستعمل !");

        $other = User::where("phone", $request->phone)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "رقم الجوال مستعمل !");

        $client->name        = $request->name;
        $client->email       = $request->email;
        $client->phone       = $request->phone;
        $client->company_id       = $request->company_id;
        if($request->password)
            $client->password    = bcrypt($request->password);
        if($request->hasFile("image")){
            $request->validate(['image' => 'required|image']);
            $path = $request->file('image')->store('uploads/clients');
            $client->image = $path;
        }
        $client->save();

        return redirect()->back()->with("success", "تم تعديل بيانات العميل !");
    }

    public function destroy($id){
        $client = User::find($id);
        $client->delete();

        return redirect()->route("admin.clients.index")->with("success", "تم حذف العميل !");
    }
}
