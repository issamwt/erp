<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;

class UserController extends Controller
{
    public function index(){
        $users = User::where("type", 0)->with("company")->orderby("id", "desc")->get();

        return view("admin.users.index", compact("users"));
    }

    public function create(){
        $companies = Company::orderby("name", "asc")->get();

        return view("admin.users.create", compact("companies"));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|unique:users',
            'password' => 'required|string|min:6',
            'company_id' => 'required',
            'image' => 'required|image',
        ]);
        $user              = new User;
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->role_id     = 1;
        $user->type        = 0;
        $user->password    = bcrypt($request->password);
        $user->company_id       = $request->company_id;
        $path = $request->file('image')->store('uploads/users');
        $user->image = $path;
        $user->save();

        return redirect()->back()->with("success", "تم حفظ الموظف الجديد !");
    }

    public function show($id){
        //
    }

    public function edit($id=null){
        $user = User::find($id);
        if(!$id or !$user)
            return redirect()->back();
        $companies = Company::orderby("name", "asc")->get();

        return view("admin.users.edit", compact("user", "companies"));
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        if(!$id or !$user)
            return redirect()->back();

        $request->validate(['name' => 'required|string|max:255', 'email' => 'required|string|email|max:255','company_id' => 'required',]);
        if($request->password)
            $request->validate(['password' => 'required|string|min:6']);

        $other = User::where("email", $request->email)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "البريد الإلكتروني مستعمل !");

        $other = User::where("phone", $request->phone)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "رقم الجوال مستعمل !");

        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->company_id       = $request->company_id;
        if($request->password)
            $user->password    = bcrypt($request->password);
        if($request->hasFile("image")){
            $request->validate(['image' => 'required|image']);
            $path = $request->file('image')->store('uploads/users');
            $user->image = $path;
        }
        $user->save();

        return redirect()->back()->with("success", "تم تعديل بيانات الموظف !");
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();

        return redirect()->back()->with("success", "تم حذف الموظف !");
    }
}
