<?php

namespace App\Http\Controllers\Company;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Company\Companies;
Use Auth;
use Validator;
use App\TaskStatus;
use App\CategoryProject;
use App\ExpenseCategory;
use App\Tax;
use App\Percentage;
use App\InvoiceCategory;
use App\PaymentMethod;
use App\User;
use App\City;
use App\Project;
use App\Task;
use App\Timer;
use App\Milestone;
use App\Attachment;
use App\Subtask;
use App\Comment;
use App\Discussion;
use App\FileType;
use App\File;
use App\Role;
use App\Notif;

class Prm extends Companies
{
    public function __construct(){
        parent::__construct();

        $this->middleware("daftar:prm");
    }

    public function statistics(){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $projects_num   = Project::where("company_id", @$company->id)->count();
        $projects_new_num       = Project::where("company_id", @$company->id)->where("status", 0)->count();
        $projects_active_num    = Project::where("company_id", @$company->id)->where("status", 1)->count();
        $projects_completed_num = Project::where("company_id", @$company->id)->where("status", 2)->count();
        $tasks_num      = Task::where(["company_id"=>@$company->id, "daftar"=>"prm"])->count();
        $tasks_num0      = Task::where(["company_id"=>@$company->id, "daftar"=>"prm", "status"=>0])->count();
        $disc_num       = Discussion::where(["company_id"=>@$company->id, "discussion_id"=>0])->count();
        $files_num      = File::where(["company_id"=>@$company->id])->count();
        $task_statuses  = TaskStatus::whereIn("company_id", [@$company->id, 0])->withCount(["tasks"=>function($q)use($company){return $q->where(["daftar"=>"prm", "company_id"=>$company->id]);}])->get();

        return view("company.prm.statistics", compact("company", "user", "projects_num", "tasks_num", "disc_num", "files_num",
            "projects_new_num", "projects_active_num", "projects_completed_num", "tasks_num0", "task_statuses"));
    }

    // Settings

    public function settings(){
        if(!permissions_general("prm_settings"))
            return back();

        $company        = $this->company;

        $user           = Auth::guard("user")->user();
        $taskStatuses   = TaskStatus::whereIn("company_id", [0, @$company->id])->get();
        $ProjectTypes   = CategoryProject::where("company_id", $company->id)->get();
        $expenseCategories = ExpenseCategory::where("company_id", $company->id)->get();
        $taxes          = Tax::where("company_id", $company->id)->get();
        $percentages    = Percentage::where("company_id", $company->id)->get();
        $invoiceCategories = InvoiceCategory::where("company_id", $company->id)->get();
        $paymentMethods = PaymentMethod::where("company_id", $company->id)->get();
        $roles          = Role::where("company_id", $company->id)->orderby("name", "asc")->get();
        $notifs         = Notif::where("company_id", $company->id)->where("daftar", "prm")->orderby("id", "asc")->get();

        return view("company.prm.settings", compact("company", "user", "taskStatuses", "ProjectTypes", "expenseCategories", "taxes", "percentages",
            "invoiceCategories", "paymentMethods", "roles", "notifs"));
    }

    public function save_tabtab($c, $tab=null){
        session(['tabtab' => $tab]);
    }

    public function save_permissions(Request $request){
        $company = $this->company;
        $role = Role::find($request->id);
        if(!$role or $role->company_id != $company->id)
            return back();

        $permissions = [];
        if($request->add_project)
            array_push($permissions,"add_project");
        if($request->update_project)
            array_push($permissions,"update_project");
        if($request->delete_project)
            array_push($permissions,"delete_project");
        if($request->add_task)
            array_push($permissions,"add_task");
        if($request->update_task)
            array_push($permissions,"update_task");
        if($request->delete_task)
            array_push($permissions,"delete_task");


        if($request->add_milestone)
            array_push($permissions,"add_milestone");
        if($request->update_milestone)
            array_push($permissions,"update_milestone");
        if($request->delete_milestone)
            array_push($permissions,"delete_milestone");

        if($request->add_discussion)
            array_push($permissions,"add_discussion");

        if($request->add_filetype)
            array_push($permissions,"add_filetype");
        if($request->add_file)
            array_push($permissions,"add_file");
        if($request->delete_file)
            array_push($permissions,"delete_file");


        if($request->tab1)
            array_push($permissions,"tab1");
        if($request->tab2)
            array_push($permissions,"tab2");
        if($request->tab3)
            array_push($permissions,"tab3");
        if($request->tab4)
            array_push($permissions,"tab4");
        if($request->tab5)
            array_push($permissions,"tab5");
        if($request->tab6)
            array_push($permissions,"tab6");
        if($request->tab7)
            array_push($permissions,"tab7");
        if($request->tab8)
            array_push($permissions,"tab8");

        $permissions = serialize($permissions);

        $role->permissions_prm = $permissions;
        $role->save();

        return back()->with("success", "تم حفظ الصلاحيات !");
    }

    public function add_task_status(Request $request){
        $company                = $this->company;

        $taskStatus             = new TaskStatus;
        $taskStatus->company_id = $company->id;
        $taskStatus->name       = $request->name;
        $taskStatus->color      = $request->color;
        $taskStatus->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_task_status($c, $id){
        if($id==1 or $id==2)
            return back();

        $ts = TaskStatus::find($id);
        $ts->delete();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function edit_task_status(Request $request){
        if($request->id==1 or $request->id==2)
            return;

        $ts = TaskStatus::find($request->id);
        $ts->name = $request->name;
        $ts->color = $request->color;
        $ts->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function add_projectType(Request $request){
        $company              = $this->company;

        $category             = new CategoryProject;
        $category->company_id = $company->id;
        $category->name       = $request->name;
        $category->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_projectType($c, $id){
        $p = CategoryProject::find($id);
        $p->delete();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_ProjectType(Request $request){
        $p         = CategoryProject::find($request->id);
        $p->name   = $request->name;
        $p->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function add_expense_category(Request $request){
        $company              = $this->company;

        $e             = new ExpenseCategory;
        $e->company_id = $company->id;
        $e->name       = $request->name;
        $e->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_expense_category(Request $request){
        $e         = ExpenseCategory::find($request->id);
        $e->name   = $request->name;
        $e->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_expense_category($c, $id){
        $e = ExpenseCategory::find($id);
        $e->delete();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function add_tax(Request $request){
        $company              = $this->company;

        $t              = new Tax;
        $t->company_id  = $company->id;
        $t->name        = $request->name;
        $t->percentage  = $request->percentage;
        $t->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_tax(Request $request){
        $t = Tax::find($request->id);
        $t->name = $request->name;
        $t->percentage = $request->percentage;
        $t->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_tax($c, $id){
        $t = Tax::find($id);
        $t->delete();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function add_percentage(Request $request){
        $company              = $this->company;

        $t              = new Percentage;
        $t->company_id  = $company->id;
        $t->percentage  = $request->percentage;
        $t->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_percentage(Request $request){
        $t = Percentage::find($request->id);
        $t->percentage = $request->percentage;
        $t->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_percentage($c, $id){
        $t = Percentage::find($id);
        $t->delete();

        //return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function add_invoice_category(Request $request){
        $company              = $this->company;

        $i              = new InvoiceCategory;
        $i->company_id  = $company->id;
        $i->name        = $request->name;
        $i->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_invoice_category(Request $request){
        $i          = InvoiceCategory::find($request->id);
        $i->name    = $request->name;
        $i->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_invoice_category($c, $id){
        $t = InvoiceCategory::find($id);
        $t->delete();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function add_payment_method(Request $request){
        $company                    = $this->company;

        $pm                         = new PaymentMethod;
        $pm->company_id             = $company->id;
        $pm->title                  = $request->title;
        $pm->description            = $request->description;
        $pm->available_on_invoice   = ($request->available_on_invoice) ? 1 : 0;
        $pm->minimum_payment_amount = $request->minimum_payment_amount;
        $pm->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_payment_method(Request $request){
        $pm = PaymentMethod::find($request->id);
        $pm->title = $request->title;
        $pm->description = $request->description;
        $pm->available_on_invoice = ($request->available_on_invoice) ? 1 : 0;
        $pm->minimum_payment_amount = $request->minimum_payment_amount;
        $pm->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_payment_method($c, $id){
        $t = PaymentMethod::find($id);
        $t->delete();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function savenotif(Request $request){
        $notifs = Notif::where("daftar", "prm")->get();
        foreach ($notifs as $notif){
            if($request->has("app_".$notif->id))
                $notif->app = 1;
            else
                $notif->app = 0;
            if($request->has("email_".$notif->id))
                $notif->email = 1;
            else
                $notif->email = 0;
            if($request->has("sms_".$notif->id))
                $notif->sms = 1;
            else
                $notif->sms = 0;
            $notif->message = $request->{"message_".$notif->id};
            $notif->save();
        }

        //return back()->with("success", "تم حفظ البيانات");
    }

    // Projects

    public function projects(){
        $company    = $this->company;
        $projects   = Project::where("company_id", $company->id)->with("type", "client", "users", "manager")->get();
        $clients    = User::where("company_id", $company->id)->where("type", 1)->orderby("name", "asc")->get();
        $users      = User::where("company_id", $company->id)->where("type", 0)->orderby("name", "asc")->get();

        foreach ($projects as $project){
            $now = Carbon::now()->format("Y-m-d");
            if($project->date_to<$now and $project->status!=3){
                $project->status = 4;
                $project->save();
            }
        }

        return view("company.prm.projects", compact("company", "projects", "clients", "users"));
    }

    public function add_project(){
        if(!permissions_prm("add_project"))
            return back();

        $company    = $this->company;
        $categories = CategoryProject::where("company_id", $company->id)->orderby("name", "asc")->get();
        $cities     = City::where("company_id", $company->id)->orderby("name", "asc")->get();
        $clients    = User::where("company_id", $company->id)->where("type", 1)->orderby("name", "asc")->get();
        $users      = User::where("company_id", $company->id)->where("type", 0)->orderby("name", "asc")->get();

        return view("company.prm.add_project", compact("company", "clients", "users", "categories", "cities"));
    }

    public function add_city(Request $request){
        $company            = $this->company;

        $city               = new City;
        $city->company_id   = $company->id;
        $city->name         = $request->name;
        $city->save();

        return intval($city->id);
    }

    public function save_project(Request $request){
        $company            = $this->company;

        $project                        = new Project;
        $project->company_id            = $company->id;
        $project->name                  = $request->name;
        $project->category_project_id   = $request->category_project_id;
        $project->client_id             = $request->client_id;
        //$project->city_id               = $request->city_id;
        $project->user_id               = $request->user_id;
        $project->price                 = $request->price;
        $project->brief                 = $request->brief;
        //$project->notes                 = $request->notes;

        $project->date_from             = $request->date_from;
        $project->date_to               = $request->date_to;

        $project->status                = 0;

        $project->save();

        $project->users()->attach($request->user_id, ["type"=>1]);

        if(!empty($request->collaborators)){
            foreach ($request->collaborators as $c){
                $project->users()->attach($c, ["type"=>0]);
            }
        }

        $users = [$request->user_id];
        if(!empty($request->collaborators)){
            foreach ($request->collaborators as $c){
                array_push($users, $c);
            }
        }
        foreach ($users as $u)
            notification($u, "add_project", $company->id, "prm", null, $project->id);


        return redirect()->route("company.prm.projects", $company->slug)->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_project($c, $id=null){
        if(!permissions_prm("delete_project"))
            return back();

        $company    = $this->company;
        $project = Project::where("company_id", $company->id)->where("id", $id)->with("users")->first();
        if (!$id or !$project)
            return back();

        foreach ($project->users as $user)
            $project->users()->detach($user);
        $project->delete();

        return redirect()->back()->with("success", "تم حذف البيانات بنجاح !");
    }

    public function edit_project($c, $id=null){
        if(!permissions_prm("update_project"))
            return back();

        $company    = $this->company;
        $project = Project::where("company_id", $company->id)->where("id", $id)->with("type", "client", "users")->first();
        if (!$id or !$project)
            return back();

        $categories = CategoryProject::where("company_id", $company->id)->orderby("name", "asc")->get();
        $cities     = City::where("company_id", $company->id)->orderby("name", "asc")->get();
        $clients    = User::where("company_id", $company->id)->where("type", 1)->orderby("name", "asc")->get();
        $users      = User::where("company_id", $company->id)->where("type", 0)->orderby("name", "asc")->get();

        return view("company.prm.edit_project", compact("company", "project",  "clients", "users", "categories", "cities"));
    }

    public function update_project(Request $request){
        $company    = $this->company;
        $project = Project::where("company_id", $company->id)->where("id", $request->id)->with("type", "client", "users")->first();
        if (!$request->id or !$project)
            return back();

        $project->name                  = $request->name;
        $project->category_project_id   = $request->category_project_id;
        $project->client_id             = $request->client_id;
        //$project->city_id               = $request->city_id;
        $project->user_id               = $request->user_id;
        $project->price                 = $request->price;
        $project->brief                 = $request->brief;
        //$project->notes                 = $request->notes;
        $project->date_from             = $request->date_from;
        $project->date_to               = $request->date_to;
        $project->save();

        //$project->users()->wherePivot('type', 1)->detach();
        foreach ($project->users as $user)
            $project->users()->detach($user);

        $project->users()->attach($request->user_id, ["type"=>1]);

        if(!empty($request->collaborators)){
            foreach ($request->collaborators as $c){
                $project->users()->attach($c, ["type"=>0]);
            }
        }

        $users = [$request->user_id];
        if(!empty($request->collaborators)){
            foreach ($request->collaborators as $c){
                array_push($users, $c);
            }
        }
        foreach ($users as $u)
            notification($u, "update_project", $company->id, "prm", null, $project->id);

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function pdf_project($c, $id=null){
        $company    = $this->company;

        $project = Project::where("company_id", $company->id)->where("id", $id)->first();
        if(!$project)
            return back();

        return view("company.prm.pdf_project", compact("project", "company"));

    }

    // Project

    public function project($c, $id=null){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();

        $project        = Project::where("company_id", $company->id)->where("id", $id)->first();
        if(!$project)
            return back();

        $now = Carbon::now()->format("Y-m-d");
        if($project->date_to<$now and $project->status!=3){
            $project->status = 4;
            $project->save();
        }

        $users          = User::where("company_id", $company->id)->where("type", 0)->orderby("name", "asc")->get();

        $tasks          = Task::with("users","taskStatus")->where(["company_id"=>$company->id,"daftar"=>"prm", "daftar_id"=> $id])->orderBy('id', 'desc')->get();
        $taskStatus     = TaskStatus::whereIn("company_id", [@$company->id, 0])->with(["tasks"=>function($q)use($id, $company){return $q->where(["company_id"=>$company->id, "daftar_id"=> $id]);}])->get();
        $cureent_timer  = Timer::where(["company_id"=>$company->id, "daftar_id"=> $id, "user_id"=>$user->id, "status" => "open"])->first();
        $milestones     = Milestone::where(["company_id"=>$company->id, "daftar_id"=>$id])->orderby("id", "desc")->get();
        $fileTypes      = FileType::where(["company_id"=>$company->id, "daftar_id"=>$project->id])->orderby("id", "asc")->get();
        $files          = File::where(["company_id"=>$company->id, "daftar_id"=>$project->id])->with("user", "type")->orderby("id", "desc")->get();
        $timers         = Timer::with("user", "task")->where(["company_id"=>$company->id, "daftar_id"=>$project->id])->orderby("id", "desc")->get();
        $discussions    = Discussion::orderby("id", "desc")->where(["company_id"=>$company->id, "daftar_id"=>$project->id, "discussion_id"=>0])->with("attachments", "user", "discussions.user")->get()->keyby("id");
        foreach ($discussions as $d){
            if($d->user_id!=$user->id){
                if($d->for_users){
                    $arr = explode(",", $d->for_users);
                    if(!in_array($user->id, $arr))
                        $discussions->forget($d->id);
                }
            }
        }

        return view("company.prm.project.index", compact("project", "user", "company", "tasks", "taskStatus", "users",
            "cureent_timer", "milestones", "discussions", "fileTypes", "files", "timers"));
    }

    public function project_status($c, $id=null, $status=null){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();

        $project        = Project::where("company_id", $company->id)->where("id", $id)->first();
        if(!$project or !$id or !$status)
            return back();

        $status = ($status==99)?0:$status;
        $project->status = $status;
        $project->save();

        return back()->with("success", "تم تعديل المشروع");
    }

    public function projectTab($c, $num=1){
        session(['projectTab' => $num]);
    }

    public function addCollaborator(Request $request){
        $project_id = $request->project_id;
        $user_id = $request->id;
        $project = Project::find($project_id);
        if($project){
            $project->users()->attach($user_id, ["type"=>0]);
            $company    = $this->company;
            $users = [];
            foreach ($project->users as $u)
                array_push($users, $u->id);
            foreach ($users as $u)
                notification($u, "add_contributer", $company->id, "prm", null, $project->id);
        }

    }

    public function sendCollaborator(Request $request){
        $company    = $this->company;

        $id         = $request->id;
        $user       = User::find($id);
        $email      = @$user->email;
        $subject    = $request->subject;
        $name       = Auth::guard("user")->user()->name;
        $msg        = "(رسالة من ".$name." ) ".$request->msg;
        if($email){
            mailit($email, $subject, $msg, $company->id, $daftar="prm");
        }
    }

    public function deleteCollaborator(Request $request){
        $project_id = $request->project_id;
        $user_id = $request->id;
        $project = Project::find($project_id);
        $project->users()->detach($user_id);

        $company    = $this->company;
        $users = [];
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "delete_contributer", $company->id, "prm", null, $project->id);
    }

    public function change_timer(Request $request, $c, $project_id=null, $start=null){
        if(!$project_id or !$start)
            return back();

        $company        = $this->company;
        $user_id        = Auth::guard("user")->user()->id;
        $note           = $request->note;
        $task_id        = $request->task_id;
        $price          = $request->price;
        $now            = get_current_utc_time("Asia/Riyadh");

        $timer = Timer::where(["company_id"=>$company->id, "daftar_id"=> $project_id, "user_id"=>$user_id, "status" => "open"])->first();

        if($start=="open" and !$timer){
            //no record found, create a new record
            $t              = new Timer;
            $t->company_id  = $company->id;
            $t->daftar_id   = $project_id;
            $t->task_id     = $task_id;
            $t->user_id     = $user_id;
            $t->status      = "open";
            $t->start_time  = $now;
            $t->save();
        }elseif($start=="closed" and $timer->id){
            //timer is running, stop the timer
            $timer->end_time = $now;
            $timer->status   = "closed";
            $timer->note     = $note;
            $timer->task_id  = $task_id;
            $timer->price    = $price;
            $timer->save();
        }

        return redirect()->back()->with("success", "تم حفظ البيانات بنجاح");
    }

    // Milestones

    public function addMilestone($c, $project_id=null){
        if(!permissions_prm("add_milestone"))
            return back();

        $company    = $this->company;

        $project    = Project::where("company_id", $company->id)->where("id", $project_id)->first();
        if(!$project_id or !$project)
            return back();

        return view("company.prm.project.milestones.add", compact("company", "project"));
    }

    public function saveMilestone(Request $request){
        $company        = $this->company;

        $milestone      = new Milestone;
        $milestone->company_id  = $company->id;
        $milestone->daftar_id   = $request->project_id;
        $milestone->name        = $request->name;
        $milestone->from_date   = $request->from_date;
        $milestone->to_date     = $request->to_date;
        $milestone->score       = $request->score;
        $milestone->details     = $request->details;
        $milestone->save();

        $users = [];
        $project = Project::find($milestone->daftar_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "add_milestone_prm", $company->id, "prm", null, $project->id);

        return redirect()->route("company.prm.project", [@$company->slug, $request->project_id])->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function editMilestone($c, $id=null){
        if(!permissions_prm("update_milestone"))
            return back();

        $company    = $this->company;

        $milestone  = Milestone::where(["company_id"=>$company->id, "id"=>$id])->first();
        if(!$id or !$milestone)
            return back();

        $project    = Project::where("company_id", $company->id)->where("id", $milestone->daftar_id)->first();
        if(!$project)
            return back();

        return view("company.prm.project.milestones.edit", compact("company", "project", "milestone"));
    }

    public function updateMilestone(Request $request){
        $company        = $this->company;

        $milestone              = Milestone::find($request->id);
        if(!$request->id or !$milestone or @$milestone->company_id!=$company->id)
            return back();

        $milestone->name        = $request->name;
        $milestone->from_date   = $request->from_date;
        $milestone->to_date     = $request->to_date;
        $milestone->score       = $request->score;
        $milestone->details     = $request->details;
        $milestone->save();

        return redirect()->route("company.prm.project", [@$company->slug, $milestone->daftar_id])->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function deleteMilestone($c, $id=null){
        if(!permissions_prm("delete_milestone"))
            return back();

        $company        = $this->company;

        $milestone              = Milestone::find($id);
        if(!$id or !$milestone or @$milestone->company_id!=$company->id)
            return back();

        $users = [];
        $project = Project::find($milestone->daftar_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "delete_milestone_prm", $company->id, "prm", null, $project->id);

        $milestone->delete();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    //Tasks

    public function addTask($c, $project_id=null){
        if(!permissions_prm("add_task"))
            return back();

        $company    = $this->company;

        $project    = Project::where("company_id", $company->id)->where("id", $project_id)->first();
        if(!$project_id or !$project)
            return back();

        $milestones = Milestone::where(["company_id"=>$company->id, "daftar_id"=>$project->id])->orderby("id", "desc")->get();

        return view("company.prm.project.tasks.add", compact("company", "project", "milestones"));
    }

    public function saveMilestonet(Request $request){
        $company        = $this->company;

        $milestone      = new Milestone;
        $milestone->company_id  = $company->id;
        $milestone->daftar_id   = $request->project_id;
        $milestone->name        = $request->name;
        $milestone->from_date   = $request->from_date;
        $milestone->to_date     = $request->to_date;
        $milestone->score       = 0;
        $milestone->details     = $request->details;
        $milestone->save();

        $users = [];
        $project = Project::find($milestone->daftar_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "delete_task_prm", $company->id, "prm", null, $project->id);

        return $milestone->id;

    }

    public function saveTask (Request $request){
        $company    = $this->company;

        $task               = new Task;
        $task->company_id   = $company->id;
        $task->daftar       = "prm";
        $task->daftar_id    = $request->project_id;
        $task->user_id      = $request->charged;
        $task->name         = $request->name;
        $task->from_date    = $request->from_date;
        $task->to_date      = $request->to_date;
        $task->num_days     = $request->num_days;
        $task->priority     = $request->priority;
        $task->note         = $request->details;
        $task->milestone_id = $request->milestone_id;
        $task->status       = 0;

        $task->save();

        $this->task_change_milestone($task);

        if(!empty($request->collaborators))
            $task->users()->sync($request->collaborators);

        $validation = [];
        if($request->hasFile("attachments")) {
            $validation['attachments.*'] = 'mimes:jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip';
        }

        $request->validate($validation);

        if($request->hasFile("attachments")){
            foreach ($request->attachments as $attachment){
                $path = $attachment->storeAs('uploads/attachments', time()."_".$attachment->getClientOriginalName());
                $attachment = new Attachment;
                $attachment->path = $path;
                $task->attachments()->save($attachment);
            }
        }

        $users = [];
        $project = Project::find($task->daftar_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "add_task_prm", $company->id, "prm", null, $project->id);

        return redirect()->route("company.prm.project", [@$company->slug, $request->project_id])->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function statusTask($c, $id=null, $status=1){
        $task = Task::find($id);
        if(!$id or !$task)
            return redirect()->back();

        $task->status = $status;
        $task->oldstatus = $status;
        $task->save();

        $this->task_change_milestone($task);

        return back()->with("success", "تم تعديل حالة المهمة");
    }

    public function task_change_milestone($task){
        $milestone = Milestone::with("tasks")->where("id", $task->milestone_id)->first();
        if($milestone){
            $num = count($milestone->tasks);
            if(count($milestone->tasks)>0){
                $confirmed = 0;
                foreach ($milestone->tasks as $t){
                    if($t->status==1)
                        $confirmed++;
                }
                $perc = intval($confirmed/$num*100);
                $milestone->score = $perc;
                $milestone->save();
            }
        }
        $project = Project::with("tasks")->where("id", $task->daftar_id)->first();
        if($project){
            $num = count($project->tasks);
            $confirmed = 0;
            foreach ($project->tasks as $t){
                if($t->status==1)
                    $confirmed++;
            }
            if($confirmed>=$num)
                $project->status=3;
            else
                $project->status=1;
            $project->save();
        }

    }

    public function editTask($c, $id=null){
        if(!permissions_prm("update_task"))
            return back();

        $company    = $this->company;

        $task       = Task::with("users","taskStatus")->where(["company_id"=>$company->id,"daftar"=>"prm", "id"=> $id])->orderBy('id', 'desc')->first();
        if(!$id or !$task)
            return back();

        $project    = Project::where("company_id", $company->id)->where("id", $task->daftar_id)->first();
        if(!$project)
            return back();

        $milestones = Milestone::where(["company_id"=>$company->id, "daftar_id"=>$project->id])->orderby("id", "desc")->get();

        return view("company.prm.project.tasks.edit", compact("company", "project", "milestones", "task"));
    }

    public function updateTask(Request $request){
        if(!permissions_prm("update_task"))
            return back();

        $company    = $this->company;

        $task       = Task::with("users","taskStatus")->where(["company_id"=>$company->id,"daftar"=>"prm", "id"=> $request->id])->orderBy('id', 'desc')->first();
        if(!$task)
            return back();

        $task->user_id      = $request->charged;
        $task->name         = $request->name;
        $task->from_date    = $request->from_date;
        $task->to_date      = $request->to_date;
        $task->num_days     = $request->num_days;
        $task->priority     = $request->priority;
        $task->note         = $request->details;
        $task->milestone_id = $request->milestone_id;
        //$task->status       = 1;

        $task->save();

        if(!empty($request->collaborators))
            $task->users()->sync($request->collaborators);

        $validation = [];
        if($request->hasFile("attachments")) {
            $validation['attachments.*'] = 'mimes:jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip';
        }

        $request->validate($validation);

        if($request->hasFile("attachments")){
            foreach ($request->attachments as $attachment){
                $path = $attachment->storeAs('uploads/attachments', time()."_".$attachment->getClientOriginalName());
                $attachment = new Attachment;
                $attachment->path = $path;
                $task->attachments()->save($attachment);
            }
        }

        $this->task_change_milestone($task);

        $users = [];
        $project = Project::find($task->daftar_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "update_task_prm", $company->id, "prm", null, $project->id);

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function deleteTaskAttachment($c, $id=null){
        $at = Attachment::find($id);
        if(!$id or !$at)
            return back();
        $at->delete();
    }

    public function deleteTask($c, $id=null){
        if(!permissions_prm("delete_task"))
            return back();

        $company    = $this->company;

        $task       = Task::with("users","taskStatus", "attachments")->where(["company_id"=>$company->id,"daftar"=>"prm", "id"=> $id])->orderBy('id', 'desc')->first();
        if(!$id or !$task)
            return back();

        $project    = Project::where("company_id", $company->id)->where("id", $task->daftar_id)->first();
        if(!$project)
            return back();

        foreach ($task->users as $user){
            $task->users()->detach($user);
        }
        foreach ($task->attachments as $attachment){
            $attachment->delete();
        }
        $task->delete();

        $this->task_change_milestone($task);

        $users = [];
        $project = Project::find($task->daftar_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "delete_task_prm", $company->id, "prm", null, $project->id);

        return back()->with("success", "تم حذف المهمة");
    }

    public function showTask($c, $id=null){
        $company    = $this->company;

        $task       = Task::with("users","taskStatus")->where(["company_id"=>$company->id,"daftar"=>"prm", "id"=> $id])->orderBy('id', 'desc')->first();
        if(!$id or !$task)
            return back();

        $project    = Project::where("company_id", $company->id)->where("id", $task->daftar_id)->first();
        if(!$project)
            return back();

        $milestones = Milestone::where(["company_id"=>$company->id, "daftar_id"=>$project->id])->orderby("id", "desc")->get();

        return view("company.prm.project.tasks.show", compact("company", "project", "milestones", "task"));
    }

    public function gantt_update(Request $request){
        try{
            $id =  $request->id;
            $start_date = date("Y-m-d", strtotime($request->start_date));
            $end_date = date("Y-m-d", strtotime($request->end_date));
            $task = Task::find($id);
            if(!$id or !$task or ! $start_date or !$end_date)
                return;
            else{
                $task->from_date = $start_date;
                $task->to_date = $end_date;
                $task->save();
            }
        }catch (\Exception $e){
            //echo $e->getMessage();
        }
    }

    // Subtasks

    public function add_sub_task(Request $request){
        $subtask            = new Subtask;
        $subtask->name      = $request->name;
        $subtask->task_id   = $request->task_id;
        $subtask->from_date = $request->from_date;
        $subtask->to_date   = $request->to_date;
        $subtask->user_id   = $request->user_id;
        $subtask->status    = 0;
        $subtask->save();

        $task               = Task::with("subtasks.user")->where("id", $request->task_id)->first();
        $this->subtask_change_task($task);
        $project            = Project::with("users")->where("id", $task->daftar_id)->first();

        return view("company.prm.project.tasks.list", compact("task", "project"));
    }

    public function confirm_task(Request $request){
        $subtask            = Subtask::find($request->id);
        $subtask->status    = ($subtask->status==0)?1:0;
        $subtask->save();

        $task               = Task::with("subtasks")->where("id", $request->task_id)->first();
        $this->subtask_change_task($task);
        $project            = Project::with("users")->where("id", $task->daftar_id)->first();

        return view("company.prm.project.tasks.list", compact("task", "project"));
    }

    public function subtask_change_task($task){
        $num = count($task->subtasks);
        $confirmed = Subtask::where("task_id", $task->id)->where("status", 1)->count();
        if($num==$confirmed){
            $task->oldstatus=$task->status;
            $task->status=1;
            $task->save();
        }else{
            $task->status=$task->oldstatus;
            $task->save();
        }
        $this->task_change_milestone($task);
    }

    public function edit_task(Request $request){
        $subtask            = Subtask::find($request->id);
        $subtask->name      = $request->name;
        $subtask->from_date = $request->from_date;
        $subtask->to_date   = $request->to_date;
        $subtask->user_id   = $request->user_id;
        $subtask->save();

        $task = Task::with("subtasks")->where("id", $request->task_id)->first();
        $project            = Project::with("users")->where("id", $task->daftar_id)->first();

        return view("company.prm.project.tasks.list", compact("task", "project"));
    }

    public function delete_stask(Request $request){
        $subtask = Subtask::find($request->id);
        $subtask->delete();

        $task = Task::with("subtasks")->where("id", $request->task_id)->first();
        $this->subtask_change_task($task);
        $project            = Project::with("users")->where("id", $task->daftar_id)->first();

        return view("company.prm.project.tasks.list", compact("task", "project"));
    }

    public function saveTaskComment(Request $request){

        $comment                = new Comment;
        $comment->parent_id     = $request->task_id;
        $comment->user_id       = Auth::guard("user")->user()->id;
        $comment->texte         = $request->comment;
        $comment->save();

        if($request->hasFile("attachments")){
            $validator = Validator::make($request->all(), ['attachments.*'=>'required|mimes:jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip']);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }else{
                foreach ($request->attachments as $attachment){
                    $path = $attachment->storeAs('uploads/attachments', time()."_".$attachment->getClientOriginalName());
                    $attachment = new Attachment;
                    $attachment->path = $path;
                    $attachment->user_id = Auth::guard("user")->user()->id;
                    $comment->attachments()->save($attachment);
                }
            }
        }
        return response()->json(['success'=>"ok"]);
    }

    public function deleteTaskComment($c, $id=null){
        $c = Comment::find($id);
        if(!$id or !$c)
            return;
        $c->delete();
    }

    public function saveReplyTaskComment(Request $request){

        $comment                = new Comment;
        $comment->comment_id    = $request->comment_id;
        $comment->user_id       = Auth::guard("user")->user()->id;
        $comment->texte         = $request->comment;
        $comment->save();

        if($request->hasFile("attachments")){
            $validator = Validator::make($request->all(), ['attachments.*'=>'required|mimes:jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip']);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }else{
                foreach ($request->attachments as $attachment){
                    $path = $attachment->storeAs('uploads/attachments', time()."_".$attachment->getClientOriginalName());
                    $attachment = new Attachment;
                    $attachment->path = $path;
                    $attachment->user_id = Auth::guard("user")->user()->id;
                    $comment->attachments()->save($attachment);
                }
            }
        }
        return response()->json(['success'=>"ok"]);
    }

    // Discussions

    public function addDiscussion($c, $id=null){
        if(!permissions_prm("add_discussion"))
            return back();

        $company    = $this->company;
        $user       = Auth::guard("user")->user();

        $project    = Project::where("company_id", $company->id)->where("id", $id)->first();
        if(!$project)
            return back();

        return view("company.prm.project.discussions.add", compact("company", "user", "project"));
    }

    public function saveDiscussion(Request $request){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();

        $project    = Project::where("company_id", $company->id)->where("id", $request->project_id)->first();
        if(!$project)
            return back();

        $validation = [];
        if($request->has("image")) {
            $validation['image.*'] = 'mimes:jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip';
        }
        $request->validate($validation);

        date_default_timezone_set("Asia/Riyadh");
        $discussion             = new Discussion;
        $discussion->company_id = $company->id;
        $discussion->daftar_id  = $request->project_id;
        $discussion->user_id    = $user->id;
        $discussion->text       = $request->text;
        $discussion->type       = ($request->type)?$request->type:0;
        if($request->name)
            $discussion->name   = $request->name;
        if($request->discussion_id)
            $discussion->discussion_id = $request->discussion_id;
        if($request->for_users and !empty($request->for_users))
            if(!in_array(0, $request->for_users))
                $discussion->for_users   = implode(",", $request->for_users);
        $discussion->save();


        if($request->has("image")) {
            foreach ($request->image as $image){
                $path = $image->storeAs('uploads/attachments', time()."_".$image->getClientOriginalName());
                $attachment = new Attachment;
                $attachment->path = $path;
                $attachment->user_id = $user->id;
                $discussion->attachments()->save($attachment);
            }
        }

        $users = [];
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "add_discussion_prm", $company->id, "prm", null, $project->id);

        return redirect()->route("company.prm.project", [@$company->slug, $request->project_id])->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function deleteDiscussion($c, $id=null){
        $discussion = Discussion::find($id);
        if(!$id or !$discussion)
            return back();

        foreach ($discussion->attachments as $attachment){
            $attachment->delete();
        }

        foreach ($discussion->discussions as $d){
            foreach ($d->attachments as $attachment){
                $attachment->delete();
            }
            foreach ($d->discussions as $dd){
                foreach ($dd->attachments as $attachment){
                    $attachment->delete();
                }

                $dd->delete();
            }
            $d->delete();
        }
        $discussion->delete();

        return back()->with("success", "تم حذف البيانات بنجاح !");
    }

    // Files

    public function saveFileType(Request $request){
        $company                = $this->company;

        $fileType               = new FileType;
        $fileType->name         = $request->name;
        $fileType->company_id   = $company->id;
        $fileType->daftar_id    = $request->project_id;
        $fileType->save();
    }

    public function saveFile(Request $request){
        $company                = $this->company;
        $user                   = Auth::guard("user")->user();

        $validator              = Validator::make($request->all(), ['file'=>'required|mimes:jpg,jpeg,png,bmp,pdf,doc,docx,csv,zip']);
        if(!$request->file_type_id)
            return response()->json(['errors'=>["error"=>"يجب إختيار نوع الملف أولا !"]]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }else{
            $file               = new File;
            $path               = $request->file->storeAs('uploads/files', time()."_".$request->file->getClientOriginalName());
            $size               = $this->formatBytes($request->file->getSize());
            $file->name         = $request->file->getClientOriginalName();
            $file->path         = $path;
            $file->company_id   = $company->id;
            $file->size         = $size;
            $file->user_id      = $user->id;
            $file->daftar_id    = $request->project_id;
            $file->file_type_id = $request->file_type_id;
            $file->save();

            $users = [];
            $project = Project::find($request->project_id);
            foreach ($project->users as $u)
                array_push($users, $u->id);
            foreach ($users as $u)
                notification($u, "add_file_prm", $company->id, "prm", null, $project->id);

            return response()->json(['result'=>$request->file_type_id]);
        }

        $users = [];
        $project = Project::find($request->project_id);
        foreach ($project->users as $u)
            array_push($users, $u->id);
        foreach ($users as $u)
            notification($u, "add_file_prm", $company->id, "prm", null, $project->id);
    }

    public function formatBytes($size, $precision = 2)
    {
        if ($size > 0) {
            $size = (int) $size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }

    public function downloadFile($c,$id=null){
        $file = File::find($id);
        if(!$id or !$file)
            return redirect()->back()->with("error", "الملف غير موجود");

        return response()->download(storage_path("app/".$file->path));
    }

    public function deleteFile($c, $id=null){
        $file = File::find($id);
        if(!$id or !$file)
            return;

        $file->delete();

        return response()->json(['result'=>$file->file_type_id]);
    }

    public function deleteFileType($c, $id=null){
        $company = $this->company;

        $filet = FileType::find($id);
        if(!$id or !$filet)
            return;

        File::where(["company_id"=>$company->id, "file_type_id"=>$id])->delete();

        $filet->delete();

        return response()->json(['result'=>$filet->id]);
    }

    // Timers

    public function saveTimer(Request $request){
        $company        = $this->company;
        $user_id        = Auth::guard("user")->user()->id;

        $t              = new Timer;
        $t->company_id  = $company->id;
        $t->daftar_id   = $request->project_id;
        $t->task_id     = $request->task_id;
        $t->user_id     = $user_id;
        $t->status      = "closed";
        $t->start_time  = $request->start_date." ".$request->start_time;
        $t->end_time    = $request->end_date." ".$request->end_time;
        $t->note        = $request->note;
        $t->price       = $request->price;
        $t->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function updateTimer(Request $request, $c, $id){

        $t              = Timer::find($id);
        $t->task_id     = $request->task_id;
        $t->start_time  = $request->start_date." ".$request->start_time;
        $t->end_time    = $request->end_date." ".$request->end_time;
        $t->note        = $request->note;
        $t->price       = $request->price;
        $t->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function deleteTimer($c, $id){
        $t              = Timer::find($id);
        $t->delete();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    // Tasks

    public function tasks(){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();

        $projects   = Project::where("company_id", $company->id)->with("type", "client", "users", "manager", "milestones")->get();
        $tasks      = Task::with("users","taskStatus", "project")->where(["company_id"=>$company->id,"daftar"=>"prm"])->orderBy('id', 'desc')->get();
        $taskStatus = TaskStatus::whereIn("company_id", [@$company->id, 0])->get();
        $users      = User::where("type", 0)->where("company_id", $company->id)->get();
        $clients    = User::where("type", 1)->where("company_id", $company->id)->get();

        return view("company.prm.tasks.index", compact("company", "user", "projects", "tasks", "users", "clients", "taskStatus"));
    }

}
