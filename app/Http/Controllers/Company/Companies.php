<?php

namespace App\Http\Controllers\Company;

use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Auth;
use Password;
use View;
use App\Company;
use App\Setting;
use App\Item;
use App\Role;
use App\User;
use App\Archive;
use App\Attendance;
use App\Notification;
use App\Event;
use App\Meeting;
use App\Email;

class Companies extends Controller
{
    use SendsPasswordResetEmails;
    use ResetsPasswords;

    public $company = null;
    public $user = null;

    public function __construct()
    {
        date_default_timezone_set('Asia/Riyadh');
        $slug = request("company");
        $company = Company::where("slug", $slug)->first();
        if(!$company)
            return abort(404);
        $this->company = $company;

        $this->middleware("auth:user")->except("login", "do_login", "showLinkRequestForm", "sendResetLinkEmail");
        $this->middleware("guest:user")->only("login", "do_login", "showLinkRequestForm", "sendResetLinkEmail");
        $this->middleware("mycompany");

        $this->middleware(function ($request, $next) use ($company) {
            if(Auth::guard("user")->check()){
                $today = date('Y-m-d');
                $user           = Auth::guard("user")->user();
                $present = Attendance::where(["company_id"=>$company->id, "user_id"=>$user->id, "status"=>"open", "thedate"=>$today])->first();
                if($present)
                    View::share('present', true);
                else
                    View::share('present', false);

                $notifications = Notification::with("notif")->where("user_id", $user->id)->where("status", 0)->orderby("id", "desc")->get();
                View::share('new_notifications', $notifications);
            }
            return $next($request);
        });

    }

    public function login(){
        $company=$this->company;

        return view("company.login", compact("company"));
    }

    public function do_login(Request $request){
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = ['email' => $request->email, 'password' => $request->password, 'company_id' => $this->company->id, "status"=>1];
        if(Auth::guard("user")->attempt($credentials, $request->filled('remember'))){
            $request->session()->regenerate();
            return redirect()->route("company.home", $this->company->slug);
        }else{
            throw ValidationException::withMessages(['email' => [trans('auth.failed')]]);
        }
    }

    public function logout(Request $request){
        Auth::guard("user")->logout();
        $request->session()->invalidate();

        return redirect()->route("company.login", @$this->company->slug);
    }

    public function showLinkRequestForm(){
        $company=$this->company;

        return view('company.passwords_email', compact("company"));
    }

    public function sendResetLinkEmail(Request $request){
        $request->validate(['email' => 'required|email']);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }
    
    public function broker()
    {
        return Password::broker("users");
    }

    protected function guard()
    {
        return "user";
    }

    public function index(){
        $company    = $this->company;
        $user       = @Auth::guard("user")->user();

        return view("company.index", compact("company", "user"));
    }

    public function homepage(){
        $company = $this->company;
        $user = Auth::guard("user")->user();

        return view("company.homepage", compact("company", "user"));
    }

    // Settings

    public function general_settings(){
        //dd(config("mail"));
        if(!permissions_general("general_settings"))
            return back();

        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $settings   = Setting::all()->keyby("key");
        $roles      = Role::where("company_id", $company->id)->orderby("name", "asc")->get();

        $smsnum = 0;
        if(@$settings["yamamah_username".$company->id]->value and @$settings["yamamah_password".$company->id]->value){
            $u = @$settings["yamamah_username".$company->id]->value;
            $p = @$settings["yamamah_password".$company->id]->value;
            ini_set("allow_url_fopen", 1);
            try{
                $ch = curl_init("http://api.yamamah.com/GetCredit/".$u."/".urlencode($p));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',));
                $result = json_decode(curl_exec($ch));
                curl_close($ch);
                if(@$result->GetCreditResult->Status==1)
                    $smsnum = @$result->GetCreditResult->Credit;
            }
            catch (\Exception $e){}
        }

        return view("company.settings.general_settings", compact("company", "user", "settings", "roles", "smsnum"));
    }

    public function general_settings_save1(Request $request){
        $mycompany = $this->company;
        $company = Company::find($request->id);
        if(!$company or $company->id!=$mycompany->id)
            return back();

        $request->validate(["name"=>"required|string"]);
        $request->validate(["slug"=>"required|string"]);
        $others = Company::where("slug", $request->slug)->where("id", "!=", $request->id)->first();
        if($others)
            return redirect()->back()->with("error", "الرابط مستعمل من قبل");

        $company->name      = $request->name;
        $company->slug      = str_replace(" ", "", trim($request->slug));

        if($request->hasFile("image")){
            $request->validate(["image"=>"required|image"]);
            $path = $request->file('image')->store('uploads/companies');
            $company->image = $path;
        }

        $company->save();

        return redirect()->route("company.general_settings", @$company->slug)->with("success", "تم تعديل الشركة بنجاح");
    }

    public function save_generals_smtp(Request $request){
        $company = $this->company;
        foreach ($request->except("_token", "send_test_mail_to".$company->id) as $k=>$v){
            $this->s_save($k, $v, $company->id);
        }

        if ($request->send_test_mail_to) {
            mailit($request->send_test_mail_to, "رسالة إختبار", "هذه رسالة إختبار من نظام ERP", $company->id,  "settings");
        }

        //return back()->with("success", "تم حفظ البيانات !");
    }

    public function save_yamamah_sms(Request $request){
        $company = $this->company;
        foreach ($request->except("_token", "sms_test") as $k=>$v){
            if($k=="sms_default")
                $this->s_save($k, $v, 0);
            else
                $this->s_save($k, $v, $company->id);
        }

        if($request->sms_test and $request->sms_test!="")
            sms_yamamah($request->sms_test, "هذه رسالة تجربة من ERP", $company->id, "settings");

        //return back()->with("success", "تم حفظ البيانات !");
    }

    public function save_pusher(Request $request){
        $company = $this->company;
        foreach ($request->except("_token") as $k=>$v){
            $this->s_save($k, $v, $company->id);
        }

        //return back()->with("success", "تم حفظ البيانات !");
    }

    public function save_zoom(Request $request){
        $company = $this->company;
        foreach ($request->except("_token") as $k=>$v){
            $this->s_save($k, $v, $company->id);
        }

        return back()->with("success", "تم حفظ البيانات !");
    }
    
    protected function s_save($k, $v, $company_id){
        $setting = Setting::where("key", $k)->first();
        if($setting){
            $setting->value = $v;
        }else{
            $setting = new Setting;
            $setting->company_id = $company_id;
            $setting->key        = $k;
            $setting->value      = $v;
        }
        $setting->save();
    }

    public function save_permissions(Request $request){
        $company = $this->company;
        $role = Role::find($request->id);
        if(!$role or $role->company_id != $company->id)
            return;

        $permissions = [];

        if($request->add_user)
            array_push($permissions,"add_user");
        if($request->delete_user)
            array_push($permissions,"delete_user");
        if($request->update_user)
            array_push($permissions,"update_user");
        if($request->show_user)
            array_push($permissions,"show_user");

        if($request->add_client)
            array_push($permissions,"add_client");
        if($request->delete_client)
            array_push($permissions,"delete_client");
        if($request->update_client)
            array_push($permissions,"update_client");
        if($request->show_client)
            array_push($permissions,"show_client");

        if($request->general_settings)
            array_push($permissions,"general_settings");
        if($request->meetings_settings)
            array_push($permissions,"meetings_settings");
        if($request->prm_settings)
            array_push($permissions,"prm_settings");
        if($request->passwords_settings)
            array_push($permissions,"passwords_settings");
        if($request->hrm_settings)
            array_push($permissions,"hrm_settings");
        if($request->messages_archive)
            array_push($permissions,"messages_archive");


        if($request->meetings_system)
            array_push($permissions,"meetings_system");
        if($request->prm_system)
            array_push($permissions,"prm_system");
        if($request->passwords_system)
            array_push($permissions,"passwords_system");
        if($request->hrm_system)
            array_push($permissions,"hrm_system");
        if($request->calendar_system)
            array_push($permissions,"calendar_system");

        $permissions = serialize($permissions);

        $role->permissions_general = $permissions;
        $role->save();

        //return back()->with("success", "تم حفظ الصلاحيات !");
    }

    public function messages_archive(){
        if(!permissions_general("messages_archive"))
            return back();

        $company    = $this->company;
        try{
            $users      = User::where("company_id", $company->id)->orderby("name", "asc")->get();
            $userss = []; foreach ($users as $u) array_push($userss, $u->id);
            $notificationss = Notification::with("notif", "user")->whereIn("user_id", $userss)->orderby("id", "desc")->paginate(20);
            $archives   = Archive::where("company_id", $company->id)->orderby("id", "desc")->paginate(20, ['*'], 'archive');

        }catch (\Exception $e){dd($e->getMessage());}




        //dd("drljg");

        return view("company.settings.messages_archive", compact("company", "users", "archives", "notificationss"));
    }

    public function messages_archive_save(Request $request){
        $archive                = new Archive;
        $archive->company_id    = $request->company_id;
        $archive->daftar        = $request->daftar;
        $archive->user_id       = $request->user_id;
        $archive->type          = $request->type;
        $archive->message       = $request->message;
        $archive->error         = $request->error;
        $archive->save();

        return back();
    }

    // Roles

    public function save_role(Request $request){
        $mycompany = $this->company;
        $company = Company::find($request->id);
        if(!$company or $company->id!=$mycompany->id)
            return back();

        $role               = new Role;
        $role->name         = $request->name;
        $role->company_id   = $company->id;
        $role->save();

        //return back()->with("success", "تم إنشاء الدور الجديد !");
    }

    public function update_role(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $role = Role::find($id);
                $role->name = $v;
                $role->save();
            }
        }

        //return back()->with("success", "تم تعديل الدور !");
    }

    public function delete_role($c, $id=null){
        $mycompany = $this->company;
        $role = Role::find($id);
        if(!$role or $role->company_id!=$mycompany->id)
            return;

        $role->delete();

        //return back()->with("success", "تم تعديل الدور !");
    }

    // Notifications

    public function notifications(){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $notifications  = Notification::with("user", "notif")->where("user_id", $user->id)->orderby("id", "asc")->get();

        return view("company.settings.notifications", compact("company", "user", "notifications"));
    }

    public function notification($c, $n_id){
        $company        = $this->company;
        
        $n  = Notification::with("user", "notif")->where("id", $n_id)->first();
        $n->status=1;
        $n->save();

        if($n and $n->notif and $n->user){
            if(@$n->meeting_id){
                return redirect()->route("company.meetings.meeting", [$company->slug, @$n->meeting_id]);
            }
            elseif(@$n->project_id){
                return redirect()->route("company.prm.project", [$company->slug, @$n->project_id]);
            }else{
                if(@$n->notif->name=="add_user")
                    return redirect()->route("company.hrm.employee", [$company->slug, $n->user->id]);
                elseif(@$n->notif->name=="delete_user")
                    return redirect()->route("company.hrm.employees", $company->slug);
                elseif(@$n->notif->name=="add_allowance" or @$n->notif->name=="delete_allowance" or @$n->notif->name=="add_deduction" or @$n->notif->name=="delete_deduction"
                    or @$n->notif->name=="add_advance" or @$n->notif->name=="delete_advance"
                    or @$n->notif->name=="add_custody" or @$n->notif->name=="deliver_custody"or @$n->notif->name=="delete_custody")
                    return redirect()->route("company.hrm.finances", [$company->slug, $n->user->id]);
                elseif(@$n->notif->name=="add_vacation" or @$n->notif->name=="accept_vacation" or @$n->notif->name=="reject_vacation"
                    or @$n->notif->name=="add_training" or @$n->notif->name=="delete_training" or @$n->notif->name=="add_mandate" or @$n->notif->name=="delete_mandate")
                    return redirect()->route("company.hrm.adminisitrative_affairs", [$company->slug, $n->user->id]);
                elseif(@$n->notif->name=="send_accounting")
                    return redirect()->route("company.hrm.accounting", [$company->slug, $n->user->id]);
                elseif(@$n->notif->name=="add_website" or @$n->notif->name=="update_website" or @$n->notif->name=="delete_website")
                    return redirect()->route("company.passwords.websites", $company->slug);
            }
        }else{
            return back();
        }
    }

    public function notification_readed(){
        $user   = Auth::guard("user")->user();
        $ns     = Notification::where("user_id", $user->id)->where("status", 0)->get();
        foreach ($ns as $n){
            $n->status = 1;
            $n->save();
        }

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    // Calendar

    public function calendar(){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();

        $events         = Event::where("company_id", $company->id)->orderby("id", "desc")->get();
        $meetings       = Meeting::where("company_id", @$company->id)->get();

        return view("company.calendar.index", compact("company", "user", "events", "meetings"));
    }

    public function save_calendar(Request $request){
        $event              = new Event;
        $event->company_id  = $request->company_id;
        $event->name        = $request->name;
        $event->start_date  = $request->start_date;
        $event->start_time  = $request->start_time;
        $event->end_date    = $request->end_date;
        $event->end_time    = $request->end_time;
        $event->description = $request->description;
        $event->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_event(Request $request){
        $event = Event::find($request->id);
        $event->name = $request->name;
        $event->start_date = $request->start_date;
        $event->start_time = $request->start_time;
        $event->end_date = $request->end_date;
        $event->end_time = $request->end_time;
        $event->description = $request->description;
        $event->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function delete_event($c, $id){
        $event = Event::find($id);
        $event->delete();

        return back()->with("success", "تم حذف البيانات بنجاح !");
    }

    // Inbox

    public function inbox(){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $users      = User::where("company_id", $company->id)->orderby("name", "asc")->get();
        $emails     = Email::with("from","to")->where("to_id", $user->id)->orderby("id", "desc")->get();

        return view("company.inbox.inbox", compact("company", "user", "users", "emails"));
    }

    public function outbox(){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $users      = User::where("company_id", $company->id)->orderby("name", "asc")->get();
        $emails     = Email::with("from","to")->where("from_id", $user->id)->orderby("id", "desc")->get();

        return view("company.inbox.outbox", compact("company", "user", "users", "emails"));
    }

    public function draft(){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $users      = User::where("company_id", $company->id)->orderby("name", "asc")->get();

        return view("company.inbox.draft", compact("company", "user", "users"));
    }

    public function sendEmail(Request $request){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();

        $email      = new Email;
        if($request->hasFile("image")){
            $request->validate(["image"=>"required|image"]);
            $path = $request->file('image')->store('uploads/companies');
            $email->image = $path;
        }
        $email->from_id     = $user->id;
        $email->to_id       = $request->to_id;
        $email->subject     = $request->subject;
        $email->message     = $request->message;
        $email->save();

        $userx = User::find($request->to_id);

        mailit($userx->email, $request->subject, $request->message, $company->id, $daftar="emails");

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function email_details($c, $id=null){
        $email = Email::find($id);
        if($email)
            dd($email);
        else
            dd("هذه الرسالة غير موجودة !");
    }

}
