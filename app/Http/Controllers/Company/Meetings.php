<?php

namespace App\Http\Controllers\Company;

use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Company\Companies;
use Auth;
use Carbon\Carbon;
use App\Company;
use App\Location;
use App\User;
use App\Meeting;
use App\CategoryMeeting;
use App\Axis;
use App\Comment;
use App\Attachment;
use App\Task;
use App\Vote;
use App\Item;
use App\Eva;
use App\Evaluation;
use App\Invited;
use App\VoteUser;
use App\Role;
use App\Setting;
use App\Notif;

class Meetings extends Companies
{
    public function __construct(){
        parent::__construct();

        $this->middleware("daftar:meetings");
    }

    public function statistics(){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $users_num      = User::where("company_id", @$company->id)->count();
        $clients0_num   = User::where("company_id", @$company->id)->where(["type"=>1, "role_id"=>0])->count();
        $clients1_num   = User::where("company_id", @$company->id)->where(["type"=>1, "role_id"=>1])->count();
        $employees_num  = User::where("company_id", @$company->id)->where("type", 0)->count();
        ////
        $all_meetings   = Meeting::where("company_id", @$company->id)->count();
        $completed_meetings  = Meeting::where("company_id", @$company->id)->where("status", 2)->count();
        $canceled_meetings   = Meeting::where("company_id", @$company->id)->where("status", 3)->count();
        $coming_meetings= 0;
        $meetings       = Meeting::where("company_id", @$company->id)->get();
        $today = date("Y-m-d H:i");
        foreach ($meetings as $meeting){
            if(strtotime($meeting->from_date." ".$meeting->from_time)>=strtotime($today))
                $coming_meetings++;
        }
        ////
        $all_tasks      = Task::where("company_id", @$company->id)->count();
        $finished_tasks = Task::where("company_id", @$company->id)->where("status", 1)->count();
        $new_tasks      = Task::where("company_id", @$company->id)->where("status", 1)->count();

        return view("company.meetings.statistics", compact("company", "user", "meetings",
            "users_num", "clients0_num", "clients1_num", "employees_num",
            "all_meetings", "completed_meetings", "coming_meetings", "canceled_meetings",
            "all_tasks", "finished_tasks", "new_tasks"));
    }

    // Settings

    public function settings(){
        if(!permissions_general("meetings_settings"))
            return back();

        $company    = $this->company;

        $user       = Auth::guard("user")->user();
        $categories = CategoryMeeting::where("company_id", $company->id)->get();
        $items      = Item::all();
        $roles      = Role::where("company_id", $company->id)->orderby("name", "asc")->get();
        $locations  = Location::where("company_id", $company->id)->orderby("name", "asc")->get();
        $settings   = Setting::all()->keyby("key");
        $notifs     = Notif::where("company_id", $company->id)->where("daftar", "meetings")->orderby("id", "asc")->get();

        return view("company.meetings.settings", compact("company", "user", "categories", "items", "roles", "locations", "settings", "notifs"));
    }

    public function save_category(Request $request){
        $company            = $this->company;
        $category           = new CategoryMeeting;
        $category->name     = $request->name;
        $category->company_id = $company->id;
        $category->save();

        //return redirect()->back()->with("success", "تم حفظ السلسة بنجاح !");
    }

    public function update_category(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $category = CategoryMeeting::find($id);
                $category->name = $v;
                $category->save();
            }
        }

        //return redirect()->back()->with("success", "تم تعديل السلسة بنجاح !");
    }

    public function delete_category($c, $id){
        $category = CategoryMeeting::find($id);
        $category->delete();

        return redirect()->back()->with("success", "تم حذف السلسة بنجاح !");
    }

    public function save_item(Request $request){
        $company            = $this->company;
        $item               = new Item;
        $item->name         = $request->name;
        $item->company_id   = $company->id;
        $item->save();

        //return redirect()->back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_item(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $item = Item::find($id);
                $item->name = $v;
                $item->save();
            }
        }
        //return redirect()->back()->with("success", "تم تعديل البيانات بنجاح !");
    }

    public function delete_item($c, $id){
        $item = Item::find($id);
        $item->delete();

        return redirect()->back()->with("success", "تم حذف البيانات بنجاح !");
    }

    public function save_location_settings(Request $request){
        $company                = $this->company;
        $location               = new Location;
        $location->company_id   = $company->id;
        $location->name         = $request->name;
        $location->save();

        //return redirect()->back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function update_location_settings(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $location       = Location::find($id);
                $location->name = $v;
                $location->save();
            }
        }

        return redirect()->back()->with("success", "تم تعديل البيانات بنجاح !");
    }

    public function delete_location_settings($c, $id){
        $location = Location::find($id);
        $location->delete();

        return redirect()->back()->with("success", "تم حذف البيانات بنجاح !");
    }

    public function save_permissions(Request $request){
        $company = $this->company;
        $role = Role::find($request->id);
        if(!$role or $role->company_id != $company->id)
            return back();

        $permissions = [];
        if($request->add_meeting)
            array_push($permissions,"add_meeting");
        if($request->update_meeting)
            array_push($permissions,"update_meeting");
        if($request->delete_meeting)
            array_push($permissions,"delete_meeting");
        if($request->accept_meeting)
            array_push($permissions,"accept_meeting");
        if($request->cancel_meeting)
            array_push($permissions,"cancel_meeting");
        if($request->delay_meeting)
            array_push($permissions,"delay_meeting");
        if($request->notify_meeting)
            array_push($permissions,"notify_meeting");
        if($request->finish_meeting)
            array_push($permissions,"finish_meeting");
        if($request->add_axis)
            array_push($permissions,"add_axis");
        if($request->update_axis)
            array_push($permissions,"update_axis");
        if($request->delete_axis)
            array_push($permissions,"delete_axis");
        if($request->order_axis)
            array_push($permissions,"order_axis");
        if($request->start_finish_axis)
            array_push($permissions,"start_finish_axis");
        if($request->add_comment_axis)
            array_push($permissions,"add_comment_axis");
        if($request->add_file_axis)
            array_push($permissions,"add_file_axis");
        if($request->add_task)
            array_push($permissions,"add_task");
        if($request->update_task)
            array_push($permissions,"update_task");
        if($request->delete_task)
            array_push($permissions,"delete_task");
        if($request->order_task)
            array_push($permissions,"order_task");
        if($request->add_file)
            array_push($permissions,"add_file");
        if($request->delete_file)
            array_push($permissions,"delete_file");
        if($request->add_vote)
            array_push($permissions,"add_vote");
        if($request->update_vote)
            array_push($permissions,"update_vote");
        if($request->delete_vote)
            array_push($permissions,"delete_vote");
        if($request->add_evaluation)
            array_push($permissions,"add_evaluation");
        $permissions = serialize($permissions);

        $role->permissions_meetings = $permissions;
        $role->save();

        return back()->with("success", "تم حفظ الصلاحيات !");
    }

    public function save_meetings_settings(Request $request){
        $company = $this->company;
        foreach ($request->except("_token") as $k=>$v){
            $this->s_save($k, $v, $company->id);
        }

        //return back()->with("success", "تم حفظ البيانات !");
    }

    protected function s_save($k, $v, $company_id){
        $setting = Setting::where("key", $k)->first();
        if($setting){
            $setting->value = $v;
        }else{
            $setting = new Setting;
            $setting->company_id = $company_id;
            $setting->key        = $k;
            $setting->value      = $v;
        }
        $setting->save();
    }

    public function savenotif(Request $request){
        $notifs = Notif::where("daftar", "meetings")->get();
        foreach ($notifs as $notif){
            if($request->has("app_".$notif->id))
                $notif->app = 1;
            else
                $notif->app = 0;
            if($request->has("email_".$notif->id))
                $notif->email = 1;
            else
                $notif->email = 0;
            if($request->has("sms_".$notif->id))
                $notif->sms = 1;
            else
                $notif->sms = 0;
            $notif->message = $request->{"message_".$notif->id};
            $notif->save();
        }

        return back()->with("success", "تم حفظ البيانات");
    }

    // Meetings Tasks

    public function tasks(){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meetings   = Meeting::with("category", "location", "manager", "amin", "users", "clients", "attachments")->where("company_id", $company->id)->orderby("id", "desc")->get()->keyby("id");
        $categories = CategoryMeeting::where("company_id", $company->id)->orderby("name", "asc")->get();
        $tasks      = Task::with("meeting", "users")->where(["daftar"=>"meeting", "company_id"=>$company->id])->orderby("created_at", "desc")->get()->keyby("id");
        $users      = User::where("type", 0)->where("company_id", $company->id)->get();
        $clients    = User::where("type", 1)->where("company_id", $company->id)->get();

        foreach ($clients as $c){
            $users->push($c);
        }

        foreach ($meetings as $meeting){
            $userss = collect();
            @$meeting->manager->invited = "manager";
            $userss->push($meeting->manager);
            if($meeting->amin){
                $meeting->amin->invited = "amin";
                $userss->push($meeting->amin);
            }
            foreach ($meeting->users as $u){
                $u->invited = "invited";
                $userss->push($u);
            }
            foreach ($meeting->clients as $c){
                $c->invited = "invited";
                $userss->push($c);
            }
            $meeting->usersx = $userss;
        }

        if($user->role_id!=1)
            foreach ($meetings as $meeting){
                $ids = [];
                foreach ($meeting->usersx as $u)
                    array_push($ids, $u->id);
                if(!in_array($user->id, $ids))
                    $meetings->forget($meeting->id);
            }

        $ids = [];
        foreach ($meetings as $meeting){
            array_push($ids, $meeting->id);
        }

        foreach ($tasks as $t){
            if(!in_array($t->daftar_id, $ids))
                $tasks->forget($t->id);
        }


        return view("company.meetings.tasks.index", compact("company", "user", "meetings", "tasks", "users", "categories"));
    }

    // Meetings

    public function meetings(){
        $company = $this->company;
        $user = Auth::guard("user")->user();
        $meetings = Meeting::with("category", "location", "manager", "amin", "users", "clients", "attachments")->where("company_id", $company->id)->orderby("id", "desc")->get()->keyby("id");
        $all_users      = User::where("type", 0)->where("company_id", $company->id)->get();

        foreach ($meetings as $meeting){
            $users = collect();
            @$meeting->manager->invited = "manager";
            $users->push($meeting->manager);
            if($meeting->amin){
                $meeting->amin->invited = "amin";
                $users->push($meeting->amin);
            }
            foreach ($meeting->users as $u){
                $u->invited = "invited";
                $users->push($u);
            }
            foreach ($meeting->clients as $c){
                $c->invited = "invited";
                $users->push($c);
            }
            $meeting->usersx = $users;
        }

        if($user->role_id!=1)
            foreach ($meetings as $meeting){
                $ids = [];
                foreach ($meeting->usersx as $u)
                    array_push($ids, $u->id);
                if(!in_array($user->id, $ids))
                    $meetings->forget($meeting->id);
            }



        return view("company.meetings.meetings", compact("company", "user", "meetings", "all_users"));
    }

    public function add_meeting(Request $request){
        if(!permissions_meetings("add_meeting"))
            return back();
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $locations  = Location::where("company_id", $company->id)->orderby("name", "asc")->get();
        $users      = User::where("type", 0)->where("company_id", $company->id)->get();
        $clients    = User::where("type", 1)->where("company_id", $company->id)->get();
        $categories = CategoryMeeting::where("company_id", $company->id)->orderby("name", "asc")->get();

        return view("company.meetings.add_meeting", compact("company", "user", "locations", "users", "clients", "categories"));
    }

    public function save_meeting(Request $request){
        if(!permissions_meetings("add_meeting"))
            return back();
        $meeting                = new Meeting;
        $meeting->company_id    = @$this->company->id;
        $meeting->category_id   = $request->category_id;
        $meeting->name          = $request->name;
        $meeting->location_id   = $request->location_id;
        $meeting->from_date     = $request->from_date;
        $meeting->from_time     = $request->from_time;
        $meeting->duration      = 0;
        $meeting->online        = $request->online;
        $meeting->record        = ($request->record)?1:0;
        $meeting->gmap_url      = $request->gmap_url;
        $meeting->type          = $request->type;
        $meeting->repeats       = ($request->repeats)?$request->repeats:0;
        $meeting->manager_id    = $request->manager;
        $meeting->amin_id       = $request->amin;
        $meeting->save();

        if(!empty($request->users))
            $meeting->users()->sync($request->users);

        if(!empty($request->clients))
            $meeting->clients()->sync($request->clients);

        $company = Company::find($meeting->company_id);

        $users  = [$request->manager, $request->amin];
        if($request->users)
            $users  = array_merge($users, @$request->users);
        if($request->clients)
            $users  = array_merge($users, @$request->clients);

        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                $email = $user->email;
                $phone = $user->phone;
                $message = "الإجتماع : ".$meeting->name;
                $msg = "لقد تم إنشاء إجتماع جديد".
                $url = route("meeting_url", ["meeting"=>$meeting->id, "user_id"=>$user->id]);
                $urllink = "<a href='".$url."'>".$url."</a>";
                mailit($email, $message, $message."<br>".$msg."<br>".$urllink, $company->id, "meetings");
                sms_yamamah($phone, $message."\n".$msg."\n\n".$url, $company->id, "meetings");

                notification($user->id, "add_meeting", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->route("company.meetings.meetings", @$company->slug)->with("success", "تم إضافة الإجتماع بنجاح !");
    }

    public function save_location(Request $request){
        $location = new Location;
        $location->name = $request->name;
        $location->company_id = $this->company->id;
        $location->save();

        return $location->id;
    }

    public function edit_meeting(Request $request, $c, $id){
        if(!permissions_meetings("update_meeting"))
            return back();
        $company = $this->company;
        $user = Auth::guard("user")->user();
        $meeting = Meeting::with("users", "clients")->where("id", $id)->first();
        if(!$meeting or $meeting->company_id!=$company->id)
            return redirect()->back();

        $users=[];
        foreach ($meeting->users as $u)
            array_push($users, $u->id);
        $meeting->usersx = $users;
        $clients=[];
        foreach ($meeting->clients as $c)
            array_push($clients, $c->id);
        $meeting->clientsx = $clients;
        
        $locations = Location::where("company_id", $company->id)->orderby("name", "asc")->get();
        $users = User::where("type", 0)->where("company_id", $company->id)->get();
        $clients = User::where("type", 1)->where("company_id", $company->id)->get();
        $categories = CategoryMeeting::where("company_id", $company->id)->orderby("name", "asc")->get();

        return view("company.meetings.edit_meeting", compact("company", "user", "meeting", "locations", "users", "clients", "categories"));
    }

    public function update_meeting(Request $request){
        if(!permissions_meetings("update_meeting"))
            return back();
        $meeting                = Meeting::find($request->id);
        $meeting->company_id    = @$this->company->id;
        $meeting->category_id   = $request->category_id;
        $meeting->name          = $request->name;
        $meeting->location_id   = $request->location_id;
        $meeting->from_date     = $request->from_date;
        $meeting->from_time     = $request->from_time;
        $meeting->online        = $request->online;
        $meeting->record        = ($request->record)?1:0;
        $meeting->gmap_url      = $request->gmap_url;
        $meeting->type          = $request->type;
        $meeting->repeats       = ($request->repeats)?$request->repeats:0;
        $meeting->manager_id    = $request->manager;
        $meeting->amin_id       = $request->amin;
        if($meeting->status==1)
            $meeting->start_at = Carbon::now();
        $meeting->save();

        $meeting->users()->sync($request->users);

        $meeting->clients()->sync($request->clients);

        $company = Company::find($meeting->company_id);

        $users  = [$request->manager, $request->amin];
        if($request->users)
            $users  = array_merge($users, @$request->users);
        if($request->clients)
            $users  = array_merge($users, @$request->clients);

        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "update_meeting", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->route("company.meetings.meetings", @$company->slug)->with("success", "تم تعديل الإجتماع بنجاح !");
    }

    public function meeting($c, $id=null){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::with("category", "location", "manager", "amin", "users", "clients", "attachments", "votes.votes")->where("id", $id)->first();
        $categories = CategoryMeeting::where("company_id", $company->id)->orderby("name", "asc")->get();
        if(!$meeting or !$meeting->company_id==$company->id)
            return back();

        // Invited
        $users      = collect();
        $meeting->manager->invited = "manager";
        $users->push($meeting->manager);
        if($meeting->amin){
            $meeting->amin->invited = "amin";
            $users->push($meeting->amin);
        }
        foreach ($meeting->users as $u){
            $u->invited = "invited";
            $users->push($u);
        }
        foreach ($meeting->clients as $c){
            $c->invited = "invited";
            $users->push($c);
        }

        foreach ($users as $u){
            $invited = Invited::where(["meeting_id"=>$meeting->id, "user_id"=>$u->id])->first();
            $u->invited_infos = $invited;
        }

        $axes       = Axis::with("speaker", "comments.user", "attachments")->where("meeting_id", $id)->orderby("orderx", "asc")->get();

        // Duration
        $duration   = 0;
        foreach ($axes as $axis)
            $duration += ($axis->hours*60)+$axis->minutes;
        $meeting->duration = $duration;
        $meeting->save();

        $tasks      = Task::where(["company_id"=>@$company->id, "daftar"=>"meeting", "daftar_id"=>$id])->with("user", "users")->orderby("id", "desc")->get();
        $items      = Item::all();
        $evas       = Eva::with("user", "evaluations.item")->where(["daftar"=>"meeting", "daftar_id"=>$meeting->id])->orderby("id", "desc")->get();
        $eva_exist  = Eva::where(["daftar"=>"meeting", "daftar_id"=>$meeting->id, "user_id"=>$user->id])->exists();

        // Zoom
        init();
        $zoom_url   = $zoom_time = null;
        if($meeting->status==1 && $meeting->zoom_id){
            try{
                $zoom = new \Fessnik\Zoom\Zoom();
                $zmeeting = $zoom->meetings->meeting($meeting->zoom_id);
                if($zmeeting){
                    $zoom_time = $zmeeting["created_at"];
                    $zoom_url = $zmeeting["join_url"];
                }

            }catch (\Exception $e){/*dd($e->getMessage());*/}
        }

        // Votes
        if($meeting->status==1 or $meeting->status==2){
            if($meeting->status==2)
                foreach ($meeting->votes as $vote){
                    $vote->status = 2;
                    $vote->save();
                }
            if($meeting->status==1){
                foreach ($meeting->votes as $vote){
                    if($vote->status==1){
                        $now    = Carbon::now();
                        $start  = $vote->start_at;
                        $diff = ($vote->hours*60+$vote->minutes);
                        $actual_passed   = $now->diffInMinutes($start);
                        if($actual_passed>=$diff){
                            $vote->status = 2;
                            $vote->save();
                        }
                    }
                }
            }
        }

        foreach ($meeting->votes as $vote){
            $vote_me    = true;
            if(count($vote->votes)>0){
                foreach ($vote->votes as $vote_user){
                    if($vote_user->user_id==$user->id)
                        $vote_me = false;
                }
            }
            $vote->vote_me = $vote_me;
        }

        //Settings
        $settings   = Setting::all()->keyby("key");
        $meetings   = Meeting::where("company_id", $company->id)->where("id", "!=", $id)->orderby("name", "asc")->get();

        $old_tasks = collect();
        $c = [];
        foreach ($meetings as $m){
            if($m->from_date <= $meeting->from_date and $m->category_id==$meeting->category_id){
                $ts = Task::with("meeting")->where("daftar_id", $m->id)->where("status", 0)->get();
                foreach ($ts as $t)
                    $old_tasks->push($t);
            }
        }

        return view("company.meetings.meeting.index", compact("company", "user", "meeting", "users", "axes", "tasks", "items", "evas", "eva_exist", "zoom_url", "zoom_time", "settings", "meetings", "old_tasks", "categories"));
    }

    public function meeting_nav(Request $request){
        $request->session()->put('meeting_nav', $request->num);
    }

    // Axis

    public function save_axis(Request $request){
        if(!permissions_meetings("add_axis"))
            return back();
        $company = $this->company;

        $axes = Axis::where("meeting_id", $request->meeting_id)->where("company_id", $company->id)->count();

        $axis = new Axis;
        $axis->orderx       = $axes+1;
        $axis->company_id   = $company->id;
        $axis->meeting_id   = $request->meeting_id;
        $axis->speaker_id   = $request->speaker_id;
        $axis->name         = $request->name;
        $axis->hours        = $request->hours;
        $axis->minutes      = $request->minutes;
        $axis->save();

        $meeting    = Meeting::find($request->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "save_axis", $company->id, "meetings", $meeting->id);
            }
        }


        return redirect()->back()->with("success", "تم إضافة محور الإجتماع بنجاح !");
    }

    public function save_axis_comment(Request $request){
        if(!permissions_meetings("add_comment_axis"))
            return back();

        $company = $this->company;
        $user = Auth::guard("user")->user();
        $axis = Axis::find($request->axis_id);

        $comment = new Comment;
        $comment->texte = $request->texte;
        $comment->user()->associate($user);
        $axis->comments()->save($comment);

        if($request->hasFile("attachment")){
            $request->validate(["attachment"=>"required|mimes:jpeg,png,jpg,gif,doc,docx,dot,zip,pdf"]);
            $path = $request->attachment->store("uploads/attachments");
            $attachment = new Attachment;
            $attachment->path = $path;
            $attachment->user_id = $user->id;
            $comment->attachments()->save($attachment);
        }

        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "update_axis", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم إضافة التعليق بنجاح !");
    }

    public function save_axis_attachments(Request $request){
        if(!permissions_meetings("add_file_axis"))
            return back();
        $user = Auth::guard("user")->user();
        $axis = Axis::find($request->axis_id);

        $request->validate(["attachments.*"=>"required|mimes:jpeg,png,jpg,gif,doc,docx,dot,zip,pdf"]);
        foreach ($request->attachments as $att){
            $attachment = new Attachment;
            $path = $att->store("uploads/attachments");
            $attachment->path = $path;
            $attachment->user_id = $user->id;
            $axis->attachments()->save($attachment);
        }

        $company = $this->company;
        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "update_axis", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم إضافة الملفات بنجاح !");
    }

    public function delete_axis($c, $id){
        if(!permissions_meetings("delete_axis"))
            return back();
        $axis = Axis::find($id);


        $company = $this->company;
        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "delete_axis", $company->id, "meetings", $meeting->id);
            }
        }

        foreach ($axis->comments as $comment){
            $comment->attachments()->delete();
            $comment->delete();
        }
        $axis->attachments()->delete();
        $axis->delete();

        return redirect()->back()->with("success", "تم حذف المحور !");
    }

    public function update_axis(Request $request){
        if(!permissions_meetings("update_axis"))
            return back();
        $axis = Axis::find($request->axis_id);
        $axis->speaker_id   = $request->speaker_id;
        $axis->name         = $request->name;
        $axis->hours        = $request->hours;
        $axis->minutes      = $request->minutes;
        $axis->save();

        $company = $this->company;
        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "update_axis", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم تعديل محور الإجتماع بنجاح !");
    }

    public function start_axis($c, $id){
        if(!permissions_meetings("start_finish_axis"))
            return back();
        $axis           = Axis::find($id);
        $axis->status   = 1;
        $axis->start_at = Carbon::now();
        $axis->save();

        $company = $this->company;
        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "start_axis", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم تشغيل محور الإجتماع بنجاح !");
    }

    public function cancel_axis($c, $id){
        if(!permissions_meetings("start_finish_axis"))
            return back();
        $axis           = Axis::find($id);
        $axis->status   = 3;
        $axis->save();

        return redirect()->back()->with("success", "تم إلغاء محور الإجتماع بنجاح !");
    }

    public function delay_axis(Request $request, $c, $id){
        if(!permissions_meetings("start_finish_axis"))
            return back();
        $axis               = Axis::find($id);
        $axis->meeting_id   = $request->meeting_id;
        $axis->save();

        $company = $this->company;
        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "delay_axis", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم تأجيل محور الإجتماع بنجاح !");
    }

    public function finish_axis($c, $id){
        if(!permissions_meetings("start_finish_axis"))
            return back();
        $axis           = Axis::find($id);
        $axis->status   = 2;
        $axis->start_at = null;
        $axis->save();

        $company = $this->company;
        $meeting    = Meeting::find($axis->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "finish_axis", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم إنهاء محور الإجتماع بنجاح !");
    }

    public function reorder_axes(Request $request, $c, $id){
        if(!permissions_meetings("order_axis"))
            return;
        foreach ($request->data as $k=>$v){
            $axis = Axis::where("meeting_id", $id)->where("id", $k)->first();
            if($axis){
                $axis->orderx = $v;
                $axis->save();
            }
        }
    }

    // Tasks

    public function save_task(Request $request){
        if(!permissions_meetings("add_task"))
            return back();
        $company            = $this->company;

        $task               = new Task;
        $task->company_id   = $company->id;
        $task->daftar       = "meeting";
        $task->daftar_id    = $request->meeting_id;
        $task->user_id      = $request->user_id;
        $task->name         = $request->name;
        $task->thedate      = $request->thedate;
        $task->note         = $request->note;
        $task->save();

        if(!empty($request->users))
            $task->users()->sync($request->users);

        $company = $this->company;
        $meeting    = Meeting::find($task->daftar_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "add_meeting_task", $company->id, "meetings", $meeting->id);
            }
        }

        //return redirect()->back()->with("success", "تم إضافة المهمة بنجاح !");
    }

    public function update_task(Request $request){
        if(!permissions_meetings("update_task"))
            return back();
        
        $company            = $this->company;

        $task               = Task::find($request->id);
        if(!$task or $task->company_id!=$company->id)
            return redirect()->back();

        //$task->user_id      = $request->user_id;
        $task->name         = $request->name;
        $task->thedate      = $request->thedate;
        $task->note         = $request->note;
        $task->save();

        if($request->users)
            $task->users()->sync($request->users);

        $company = $this->company;
        $meeting    = Meeting::find($task->daftar_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "update_meeting_task", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم تعديل المهمة بنجاح !");
    }

    public function delete_task($c, $id){
        if(!permissions_meetings("delete_task"))
            return back();
        $company = $this->company;
        $task = Task::with("users")->where("id", $id)->first();
        if(!$task or @$task->company_id!=@$company->id)
            return;


        $company = $this->company;
        $meeting    = Meeting::find($task->daftar_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "delete_meeting_task", $company->id, "meetings", $meeting->id);
            }
        }

        $task->users()->sync([]);
        $task->delete();

        //return redirect()->back()->with("success", "تم حذف المهمة بنجاح !");
    }

    public function change_task_status($c, $id, $status=0){
        if(!permissions_meetings("update_task"))
            return;
        $company = $this->company;
        $task = Task::with("users")->where("id", $id)->first();
        if(!$task or @$task->company_id!=@$company->id)
            return redirect()->back();

        $task->status = $status;
        $task->save();

        //return redirect()->back()->with("success", "تم تعديل المهمة بنجاح !");
    }

    // Attachment

    public function save_attachment(Request $request){
        if(!permissions_meetings("add_file"))
            return back();
        $user       = Auth::guard("user")->user();
        $company    = $this->company;
        $meeting    = Meeting::find($request->meeting_id);
        if(!$meeting or @$meeting->company_id!=@$company->id)
            return redirect()->back();

        $attachment             = new Attachment;
        $attachment->name       = $request->name;
        $attachment->url        = $request->url;
        $attachment->user_id    = $user->id;

        if($request->hasFile("path")){
            $request->validate(["path"=>"required|mimes:jpeg,png,jpg,gif,doc,docx,dot,zip,pdf"]);
            $path = $request->path->store("uploads/attachments");
            $attachment->path = $path;
        }

        $meeting->attachments()->save($attachment);

        $company = $this->company;
        $meeting    = Meeting::find($request->meeting_id);
        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "add_file_meeting", $company->id, "meetings", $meeting->id);
            }
        }

        return redirect()->back()->with("success", "تم إضافة الملف بنجاح !");
    }

    public function delete_attachment($c, $id){
        if(!permissions_meetings("delete_file"))
            return back();
        $attachment = Attachment::find($id);
        $meeting = Meeting::find($attachment->attachmentable_id);
        $company    = $this->company;
        if(!$meeting or @$meeting->company_id!=@$company->id)
            return redirect()->back();

        $attachment->delete();
        
        return redirect()->back()->with("success", "تم حذف الملف بنجاح !");
    }

    // Votes

    public function save_vote(Request $request){
        if(!permissions_meetings("add_vote"))
            return back();
        $vote               = new Vote;
        $vote->meeting_id   = $request->meeting_id;
        $vote->type         = $request->type;
        if($request->type==1 or $request->type==2)
            $vote->options  = serialize($request->aaa);
        elseif($request->type==3)
            $vote->options  = $request->options2;
        $vote->name         = $request->name;
        $vote->hours        = $request->hours;
        $vote->minutes      = $request->minutes;
        $vote->open         = ($request->open or $request->type==3)?1:0;
        $vote->save();

        return redirect()->back()->with("success", "تم إضافة التصويت بنجاح !");
    }

    public function update_vote(Request $request){
        if(!permissions_meetings("update_vote"))
            return back();
        $vote = Vote::find($request->id);
        $meeting = Meeting::find($vote->meeting_id);
        $company    = $this->company;
        if(!$meeting or @$meeting->company_id!=@$company->id)
            return redirect()->back();

        $vote->name         = $request->name;
        $vote->hours        = $request->hours;
        $vote->minutes      = $request->minutes;
        $vote->note         = $request->note;
        $vote->type         = $request->type;
        if($request->type==1 or $request->type==2)
            $vote->options  = serialize($request->aaa);
        elseif($request->type==3)
            $vote->options  = $request->options2;
        $vote->open         = ($request->open or $request->type==3)?1:0;
        $vote->save();

        return redirect()->back()->with("success", "تم إضافة التصويت بنجاح !");
    }

    public function start_vote($c, $id, $status=0){
        $vote = Vote::find($id);

        $vote->status = intval($status);
        $vote->start_at = Carbon::now();
        $vote->save();
        dd($vote);
    }

    public function delete_vote($c, $id){
        if(!permissions_meetings("delete_vote"))
            return back();
        $vote = Vote::find($id);
        $meeting = Meeting::find($vote->meeting_id);
        $company    = $this->company;
        if(!$meeting or @$meeting->company_id!=@$company->id)
            return redirect()->back();

        $votes = VoteUser::where("vote_id", $id)->get();
        foreach ($votes as $v)
            $v->delete();

        $vote->delete();

        return redirect()->back()->with("success", "تم حذف التصويت بنجاح !");
    }

    public function vote_vote(Request $request){
        $vote = Vote::find($request->id);
        $meeting = Meeting::find($vote->meeting_id);
        $company    = $this->company;
        if(!$meeting or @$meeting->company_id!=@$company->id)
            return redirect()->back();

        $vote_user           = new VoteUser;
        if($request->note)
            $vote_user->note = $request->note;
        if($vote->type==2)
            $vote_user->result   = implode(";", $request->result);
        else
            $vote_user->result   = $request->result;
        $vote_user->vote_id  = $request->id;
        $vote_user->user_id  = $request->user_id;
        $vote_user->save();

        return redirect()->back()->with("success", "تم تعديل التصويت بنجاح !");
    }

    // Evaluation

    public function save_evaluation(Request $request){
        if(!permissions_meetings("add_evaluation"))
            return back();
        $meeting = Meeting::find($request->meeting_id);
        $company = $this->company;
        $user    = Auth::guard("user")->user();
        if(!$meeting or @$meeting->company_id!=@$company->id)
            return redirect()->back();

        $eva = Eva::where(["daftar"=>"meeting", "daftar_id"=>$request->meeting_id, "user_id"=>$user->id])->first();
        if($eva)
            return back()->with("error", "لقد سبق أن قمت بتقييم هذا الإجتماع !");

        $eva             = new Eva;
        $eva->daftar     = "meeting";
        $eva->daftar_id  = $request->meeting_id;
        $eva->user_id    = $user->id;
        $eva->save();

        foreach ($request->except("_token", "meeting_id") as $k=>$v){
            $evaluation             = new Evaluation;
            $evaluation->eva_id     = $eva->id;
            $evaluation->item_id    = $k;
            $evaluation->num        = $v;
            $evaluation->save();
        }

        return redirect()->back()->with("success", "تم إضافة التقييم بنجاح !");
    }

    public function delete_evaluation($c, $id){
        $user       = Auth::guard("user")->user();
        $eva        = Eva::with("user", "evaluations")->where("id", $id)->where("user_id", $user->id)->first();
        if(!$eva)
            return redirect()->back();

        foreach($eva->evaluations as $evaluation){
            $evaluation->delete();
        }
        $eva->delete();

        return redirect()->back()->with("success", "تم حذف التقييم بنجاح !");
    }

    public function update_evaluation(Request $request){
        $user       = Auth::guard("user")->user();
        $eva        = Eva::with("user", "evaluations")->where("id", $request->id)->where("user_id", $user->id)->first();
        if(!$eva)
            return redirect()->back();

        foreach ($request->except("_token", "id") as $k=>$v){
            $evaluation             = Evaluation::find($k);
            $evaluation->num        = $v;
            $evaluation->save();
        }

        return redirect()->back()->with("success", "تم حذف التقييم بنجاح !");
    }

    // Invited

    public function save_invited(Request $request){
        $invited = Invited::where(["meeting_id"=>$request->meeting_id, "user_id"=>$request->user_id])->first();
        if(!$invited)
            $invited = new Invited;

        $invited->meeting_id = $request->meeting_id;
        $invited->user_id = $request->user_id;
        $invited->attendance = $request->attendance;
        $invited->note = $request->note;
        $invited->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    public function save_invited2(Request $request){
        $company = $this->company;

        $invited = Invited::where(["meeting_id"=>$request->meeting_id, "user_id"=>$request->user_id])->first();
        if(!$invited)
            $invited = new Invited;

        $invited->meeting_id = $request->meeting_id;
        $invited->user_id = $request->user_id;
        if($request->attendance2==1){
            $meeting = Meeting::find($request->meeting_id);
            if($meeting->status==1){
                $s = Setting::where("key", "partialAttendance".$company->id)->first();
                if($s){
                    $s = intval($s->value);
                    $now    = Carbon::now();
                    $start  = $meeting->start_at;
                    $actual_passed   = $now->diffInMinutes($start);
                    $should_passed = intval($meeting->duration/100*$s);
                    echo $actual_passed."<br>".$should_passed;
                    $invited->attendance2 = ($actual_passed>$should_passed)?5:1;
                }else{
                    $invited->attendance2 = 1;
                }
            }else{
                //$invited->attendance2 = 5;
                $invited->attendance2 = $request->attendance2;
            }
        }else{
            $invited->attendance2 = $request->attendance2;
        }
        $invited->note = $request->note;
        $invited->save();

        return back()->with("success", "تم حفظ البيانات بنجاح !");
    }

    // Buttons

    public function accept_meeting($c, $id=null){
        if(!permissions_meetings("accept_meeting"))
            return back();
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::find($id);
        if(!$id or !$meeting or !$meeting->company_id==$company->id)
            return back();

        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);

        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "accept_meeting", $company->id, "meetings", $meeting->id);
            }
        }

        // Invited
        $users      = collect();
        $meeting->manager->invited = "manager";
        $users->push($meeting->manager);
        if($meeting->amin){
            $meeting->amin->invited = "amin";
            $users->push($meeting->amin);
        }
        foreach ($meeting->users as $u){
            $u->invited = "invited";
            $users->push($u);
        }
        foreach ($meeting->clients as $c){
            $c->invited = "invited";
            $users->push($c);
        }
        foreach ($users as $u){
            $invited = Invited::where(["meeting_id"=>$meeting->id, "user_id"=>$u->id])->first();
            if(!$invited){
                $invited = new Invited;
                $invited->meeting_id = $meeting->id;
                $invited->user_id = $u->id;
                $invited->attendance = 3;
                $invited->save();
            }
        }

        $meeting->status = 1;
        $meeting->start_at = Carbon::now();
        $meeting->save();
        fcmit("accept_meeting", ["message"=>"لقد بدأ إجتماع \"".$meeting->name."\"", "meeting_id"=>$id, "user_id"=>$user->id, "company_id"=>$user->company_id]);

        return back()->with("success", "تم البدأ في الإجتماع !");
    }

    public function zoom_meeting($c, $id=null){
        if(!permissions_meetings("accept_meeting"))
            return back();

        $meeting    = Meeting::find($id);
        try{
            init();
            $zoom = new \Fessnik\Zoom\Zoom();
            $data = ["topic"=>$meeting->name, "type"=>1, "duration"=>100, "timezone"=>"Asia/Riyadh", "password"=>"", "agenda"=>$meeting->name,
                "settings"=>["host_video"=>true, "participant_video"=>true, "cn_meeting"=>false, "in_meeting"=>false, "join_before_host"=>false, "mute_upon_entry"=>false, "watermark"=>false, "use_pmi"=>false,
                    "approval_type"=>0, "audio"=>"both", "auto_recording"=>"local", "enforce_login"=>false,]];
            $users = $zoom->users->list();
            $zoom_user = @$users["users"][0];
            if($zoom_user){
                $zoom_meeting = $zoom->meetings->create($zoom_user["id"], $data);
                if($zoom_meeting["code"]!=201){
                    return back()->with("error", "لا يوجد حساب مستخدم في zoom !");
                } else{
                    //$zmeeting = $zoom->meetings->meeting($zoom_meeting["id"]);
                    $meeting->zoom_id = $zoom_meeting["id"];
                    $meeting->save();
                }
            }else{
                return back()->with("error", "لا يوجد حساب مستخدم في zoom !");
            }
        }catch (\Exception $e){/*dd($e->getMessage());*/}


        return back()->with("success", "تم تشغيل zoom !");
    }

    public function cancel_meeting($c, $id=null){
        if(!permissions_meetings("cancel_meeting"))
            return back();
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::find($id);
        if(!$id or !$meeting or !$meeting->company_id==$company->id)
            return back();

        $meeting->status = 3;
        $meeting->zoom_id = null;
        $meeting->save();

        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);

        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "cancel_meeting", $company->id, "meetings", $meeting->id);
            }
        }

        return back()->with("success", "تم إلغاء الإجتماع !");
    }

    public function delay_meeting(Request $request){
        if(!permissions_meetings("delay_meeting"))
            return back();
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::find($request->id);
        if(!$request->id or !$meeting or !$meeting->company_id==$company->id)
            return back();

        $meeting->from_date = $request->from_date;
        $meeting->from_time = $request->from_time;
        //$meeting->to_time   = $request->to_time;
        $meeting->zoom_id   = null;
        if($meeting->status==1)
            $meeting->status=0;
        $meeting->save();

        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);
        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "delay_meeting", $company->id, "meetings", $meeting->id);
            }
        }

        return back()->with("success", "تم تأجيل الإجتماع !");
    }

    public function notify_meeting(Request $request){
        if(!permissions_meetings("notify_meeting"))
            return back();

        $company    = $this->company;
        $setting = Setting::where("key", "reminder_message".$company->id)->first();
        if($setting)
            $msg = @$setting->value;
        else
            $msg = "هذه رسالة لتذكيرك بالإجتماع.";

        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::find($request->id);
        if(!$request->id or !$meeting or !$meeting->company_id==$company->id)
            return back();
        if(is_array($request->users) and in_array(0, $request->users)){
            $users  = [$meeting->manager_id, $meeting->amin_id];
            if($meeting->users)
                foreach ($meeting->users as $u)
                    array_push($users, $u->id);
            if($meeting->clients)
                foreach ($meeting->clients as $u)
                    array_push($users, $u->id);

            foreach ($users as $id){
                $user = User::find($id);
                if($user and  $user->email and $user->phone){
                    notification($user->id, "remind_meeting", $company->id, "meetings", $meeting->id);
                }
            }
        }else{
            foreach ($request->users as $id){
                $user = User::find($id);
                if($user){
                    $email = $user->email;
                    $phone = $user->phone;
                    notification($user->id, "remind_meeting", $company->id, "meetings", $meeting->id);
                    /*if($request->methods and is_array($request->methods)){
                        $message = "الإجتماع : ".$meeting->name;
                        $url = route("meeting_url", ["meeting"=>$meeting->id, "user_id"=>$user->id]);
                        $urllink = "<a href='".$url."'>".$url."</a>";
                        if(in_array(0, $request->methods))
                            mailit($email, $message, $message."<br>".$msg."<br>".$urllink, $company->id, "meetings");
                        if(in_array(1, $request->methods))
                            sms_yamamah($phone, $message."\n".$msg."\n\n".$url, $company->id, "meetings");
                        if(in_array(2, $request->methods))
                            fcmit("user_".$id, ["message" => $message." : ".$msg]);
                    }*/
                }
            }
        }

        return back()->with("success", "تم إرسال التذكير الإجتماع !");
    }

    public function pdf_meeting($c, $id=null){
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::with("category", "location", "manager", "amin", "users", "clients", "attachments", "votes.votes")->where("id", $id)->first();
        if(!$id or !$meeting or !$meeting->company_id==$company->id)
            return back();

        $users      = collect();
        $meeting->manager->invited = "manager";
        $users->push($meeting->manager);
        if($meeting->amin){
            $meeting->amin->invited = "amin";
            $users->push($meeting->amin);
        }
        foreach ($meeting->users as $u){
            $u->invited = "invited";
            $users->push($u);
        }
        foreach ($meeting->clients as $c){
            $c->invited = "invited";
            $users->push($c);
        }

        foreach ($users as $u){
            $invited = Invited::where(["meeting_id"=>$meeting->id, "user_id"=>$u->id])->first();
            $u->invited_infos = $invited;
        }

        //dd($meeting);
        $axes       = Axis::with("speaker", "comments.user", "attachments")->where("meeting_id", $id)->orderby("orderx", "asc")->get();
        $tasks      = Task::where(["company_id"=>@$company->id, "daftar"=>"meeting", "daftar_id"=>$id])->with("user", "users")->orderby("id", "desc")->get();
        $evas       = Eva::with("user", "evaluations.item")->where(["daftar"=>"meeting", "daftar_id"=>$meeting->id])->orderby("id", "desc")->get();
        $items      = Item::all();

        $html = view('company.meetings.meeting.pdf_meeting',["users"=>$users, 'meeting'=>$meeting, "axes"=>$axes, "tasks"=>$tasks, "company"=>$company, "evas"=>$evas, "items"=>$items])->render(); // file render
        return $html;

    }

    public function delete_meeting($c, $id){
        if(!permissions_meetings("delete_meeting"))
            return back();
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::with("category", "location", "manager", "amin", "users", "clients", "attachments", "votes")->where("id", $id)->first();
        if(!$meeting or !$meeting->company_id==$company->id)
            return back();

        $axes       = Axis::with("speaker", "comments.user", "attachments")->orderby("id", "desc")->get();
        $tasks      = Task::where(["company_id"=>@$company->id, "daftar"=>"meeting", "daftar_id"=>$id])->with("user", "users")->orderby("id", "desc")->get();
        $evas       = Eva::with("user", "evaluations.item")->where(["daftar"=>"meeting", "daftar_id"=>$meeting->id])->orderby("id", "desc")->get();


        $users  = [$meeting->manager_id, $meeting->amin_id];
        if($meeting->users)
            foreach ($meeting->users as $u)
                array_push($users, $u->id);
        if($meeting->clients)
            foreach ($meeting->clients as $u)
                array_push($users, $u->id);

        foreach ($users as $id){
            $user = User::find($id);
            if($user and  $user->email and $user->phone){
                notification($user->id, "delete_meeting", $company->id, "meetings", $meeting->id);
            }
        }


        $meeting->users()->sync([]);
        $meeting->clients()->sync([]);
        foreach ($axes as $axis){
            foreach ($axis->comments as $comment){
                $comment->attachments()->delete();
                $comment->delete();
            }
            $axis->attachments()->delete();
            $axis->delete();
        }
        foreach ($tasks as $task){
            $task->users()->sync([]);
            $task->delete();
        }
        $meeting->attachments()->delete();
        $meeting->votes()->delete();
        foreach ($evas as $eva){
            foreach($eva->evaluations as $evaluation){
                $evaluation->delete();
            }
            $eva->delete();
        }
        Invited::where("meeting_id", $meeting->id)->delete();
        $meeting->delete();

        return back()->with("success", "تم حذف الإجتماع نهائيا !");
    }

    public function finish_meeting($c, $id){
        if(!permissions_meetings("finish_meeting"))
            return back();
        $user    = Auth::guard("user")->user();
        $meeting = Meeting::find($id);
        $meeting->status=2;
        $meeting->zoom_id = null;
        $meeting->save();
        fcmit("end_meeting", ["message"=>"لقد إنتهى إجتماع \"".$meeting->name."\"", "meeting_id"=>$meeting->id, "user_id"=>$user->id, "company_id"=>$user->company_id]);
        return redirect()->back()->with("success", "تم إنهاء الإجتماع بنجاح !");
    }

    // Zoom

    public function zoom($c, $id=null){
        init();
        $company    = $this->company;
        $user       = Auth::guard("user")->user();
        $meeting    = Meeting::with("category", "location", "manager", "amin", "users", "clients", "attachments", "votes")->where("id", $id)->first();
        if(!$meeting or !$meeting->company_id==$company->id)
            return back();



        $zoom = new \Fessnik\Zoom\Zoom();
        $data = ["topic"=>$meeting->name, "type"=>1, "duration"=>30, "timezone"=>"Asia/Riyadh", "password"=>"", "agenda"=>$meeting->name,
            "settings"=>["host_video"=>true, "participant_video"=>true, "cn_meeting"=>false, "in_meeting"=>false, "join_before_host"=>false, "mute_upon_entry"=>false, "watermark"=>false, "use_pmi"=>false,
                        "approval_type"=>0, "audio"=>"both", "auto_recording"=> "local", "enforce_login"=>false,]];
        $users = $zoom->users->list();
        $zoom_user = @$users["users"][0];
        if($zoom_user){
            $zoom_meeting = $zoom->meetings->create($zoom_user["id"], $data);
            if($zoom_meeting["code"]!=201)
                return back()->with("error", "لا يوجد حساب مستخدم في zoom !");
            else{
                $zmeeting = $zoom->meetings->meeting($zoom_meeting["id"]);
            }
        }else{
            return back()->with("error", "لا يوجد حساب مستخدم في zoom !");
        }
    }

}
