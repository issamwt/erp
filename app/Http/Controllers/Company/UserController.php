<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Company\Companies;
use App\User;
use App\Company;
use App\Role;

class UserController extends Companies
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $users = User::with("role")->where("type", 0)->where("company_id", @$this->company->id)->with("company")->orderby("id", "desc")->get();
        $company = $this->company;

        return view("company.users.index", compact("users", "company"));
    }

    public function create(){
        if(!permissions_general("add_user"))
            return back();
        $companies = Company::orderby("name", "asc")->get();
        $company = $this->company;
        $roles = Role::where("company_id", $company->id)->orderby("name", "asc")->where("id", "!=", 1)->get();

        return view("company.users.create", compact("companies", "company", "roles"));
    }

    public function store(Request $request){
        if(!permissions_general("add_user"))
            return back();
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
            'company_id' => 'required',
            'image' => 'required|image',
        ]);
        $user              = new User;
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->role_id     = $request->role;
        $user->status      = $request->status;
        $user->type        = 0;
        $user->password    = bcrypt($request->password);
        $user->company_id       = $request->company_id;
        $path = $request->file('image')->store('uploads/users');
        $user->image = $path;
        $user->save();

        return redirect()->back()->with("success", "تم حفظ الموظف الجديد !");
    }

    public function show($c, $id){
        if(!permissions_general("show_user"))
            return back();
        $user = User::with("role")->where("type", 0)->where("company_id", @$this->company->id)->where("id", $id)->first();
        $company = $this->company;

        return view("company.users.show", compact("user", "company"));
    }

    public function edit($c,$id=null){
        if(!permissions_general("update_user"))
            return back();
        $user = User::find($id);
        if(!$id or !$user)
            return redirect()->back();
        $companies = Company::orderby("name", "asc")->get();
        $company = $this->company;
        $roles = Role::where("company_id", $company->id)->orderby("name", "asc")->where("id", "!=", 1)->get();

        return view("company.users.edit", compact("user", "companies", "company", "roles"));
    }

    public function update(Request $request,$c, $id){
        if(!permissions_general("update_user"))
            return back();
        $user = User::find($id);
        if(!$id or !$user)
            return redirect()->back();

        $request->validate(['name' => 'required|string|max:255', 'email' => 'required|string|email|max:255', 'phone' => 'required|string|max:255']);
        if($request->password)
            $request->validate(['password' => 'required|string|min:6']);

        $other = User::where("email", $request->email)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "البريد الإلكتروني مستعمل !");

        $other = User::where("phone", $request->phone)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "رقم الجوال مستعمل !");

        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->role_id     = $request->role;
        $user->status      = $request->status;
        if($request->password)
            $user->password    = bcrypt($request->password);
        if($request->hasFile("image")){
            $request->validate(['image' => 'required|image']);
            $path = $request->file('image')->store('uploads/users');
            $user->image = $path;
        }
        $user->save();

        return redirect()->back()->with("success", "تم تعديل بيانات الموظف !");
    }

    public function destroy($c, $id){
        if(!permissions_general("delete_user"))
            return back();
        $user = User::find($id);
        $user->delete();

        return redirect()->back()->with("success", "تم حذف الموظف !");
    }
}
