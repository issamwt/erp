<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use Auth;
use App\Role;
use App\Website;
use App\SiteUser;
use App\Notif;
use App\User;

class Passwords extends Companies
{
    // Settings

    public function settings(){
        if (!permissions_general("prm_settings"))
            return back();

        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $roles          = Role::where("company_id", $company->id)->orderby("name", "asc")->get();
        $notifs         = Notif::where("company_id", $company->id)->where("daftar", "passwords")->orderby("id", "asc")->get();

        return view("company.passwords.settings", compact("company", "roles", "notifs"));
    }

    public function save_permissions(Request $request){
        $company = $this->company;
        $role = Role::find($request->id);
        if(!$role or $role->company_id != $company->id)
            return back();

        $permissions = [];
        if($request->show_website)
            array_push($permissions,"show_website");
        if($request->add_website)
            array_push($permissions,"add_website");
        if($request->update_website)
            array_push($permissions,"update_website");
        if($request->delete_website)
            array_push($permissions,"delete_website");

        $permissions = serialize($permissions);

        $role->permissions_passwords = $permissions;
        $role->save();

        return back()->with("success", "تم حفظ الصلاحيات !");
    }

    public function savenotif(Request $request){
        $notifs = Notif::where("daftar", "passwords")->get();
        foreach ($notifs as $notif){
            if($request->has("app_".$notif->id))
                $notif->app = 1;
            else
                $notif->app = 0;
            if($request->has("email_".$notif->id))
                $notif->email = 1;
            else
                $notif->email = 0;
            if($request->has("sms_".$notif->id))
                $notif->sms = 1;
            else
                $notif->sms = 0;
            $notif->message = $request->{"message_".$notif->id};
            $notif->save();
        }

        return back()->with("success", "تم حفظ البيانات");
    }

    // Websites

    public function __construct(){
        parent::__construct();

        $this->middleware("daftar:passwords");
    }

    public function websites(){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $websites       = Website::where("company_id", $company->id)->orderby("id", "desc")->get()->keyby("id");
        $users          = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();

        if($user->role_id!=1){
            foreach ($websites as $website){
                $userss = explode(";", $website->userss);
                if(!in_array($user->id, $userss))
                    $websites->forget($website->id);
            }
        }

        return view("company.passwords.websites", compact("company", "websites", "users", "user"));
    }

    public function add_website(){
        if(!permissions_passwords("add_website"))
            return back();

        $company = $this->company;
        $user = Auth::guard("user")->user();
        $users          = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();

        return view("company.passwords.add_website", compact("user", "company", "users", "user"));
    }

    public function save_website(Request $request){
        if(!permissions_passwords("add_website"))
            return back();
        $company            = $this->company;

        $request->validate(["image"=>"required|image"]);

        $site               = new Website;
        $site->company_id   = $company->id;
        $site->name         = $request->name;
        $site->date         = $request->date;
        $site->link         = $request->link;
        $site->dashboard_link = $request->dashboard_link;
        $path               = $request->image->store("uploads/websites");
        $site->image        = $path;
        $site->userss       = implode(";", $request->userss);
        $site->save();

        if(!empty($request->names)){
            $site->users()->delete();
            foreach ($request->names as $i=>$name){
                $u              = new SiteUser;
                $u->name        = $name;
                $u->password    = $request->passwords[$i];
                $u->type        = $request->types[$i];
                $site->users()->save($u);
            }
        }

        $users   = User::where("company_id", @$company->id)->where(["type"=>0])->get();
        foreach ($users as $user)
            notification($user->id, "add_website", $company->id, "passwords");

        return redirect()->route("company.passwords.websites", $company->slug)->with("success", "تم إضافة الموقع بنجاح");
    }

    public function edit_website($c, $id){
        if (!permissions_passwords("show_website"))
            return back();

        $company = $this->company;
        $website = Website::with("users")->where(["id"=>$id, "company_id"=>$company->id])->first();
        $user = Auth::guard("user")->user();
        $users          = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();

        return view("company.passwords.edit_website", compact("website", "company", "users", "user"));
    }

    public function update_website(Request $request){
        $company            = $this->company;

        $site               = Website::find($request->id);
        if(!$site)
            return back();

        $site->company_id   = $company->id;
        $site->name         = $request->name;
        $site->date         = $request->date;
        $site->link         = $request->link;
        $site->dashboard_link = $request->dashboard_link;
        if($request->hasFile("image")){
            $request->validate(["image"=>"required|image"]);
            $path               = $request->image->store("uploads/websites");
            $site->image        = $path;
        }
        $site->userss       = implode(";", $request->userss);
        $site->save();

        if(!empty($request->names)){
            $site->users()->delete();
            $site->users()->detach();
            foreach ($request->names as $i=>$name){
                $u              = new SiteUser;
                $u->name        = $name;
                $u->password    = $request->passwords[$i];
                $u->type        = $request->types[$i];
                $site->users()->save($u);
            }
        }

        $users   = User::where("company_id", @$company->id)->where(["type"=>0])->get();
        foreach ($users as $user)
            notification($user->id, "update_website", $company->id, "passwords");

        return back()->with("success", "تم تعديل الموقع بنجاح");
    }

    public function delete_website($c, $id=null){
        if (!permissions_passwords("delete_website"))
            return back();

        $company = $this->company;
        $website = Website::with("users")->where(["id"=>$id, "company_id"=>$company->id])->first();
        if(!$id or !$website)
            return back();

        $website->users()->delete();
        $website->users()->detach();
        $website->delete();

        $users   = User::where("company_id", @$company->id)->where(["type"=>0])->get();
        foreach ($users as $user)
            notification($user->id, "delete_website", $company->id, "passwords");

        return redirect()->route("company.passwords.websites", $company->slug)->with("success", "تم حذف الموقع بنجاح");
    }

}
