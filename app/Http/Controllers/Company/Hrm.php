<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Company\Companies;
use Auth;
use Carbon\CarbonPeriod;
use Carbon\Carbon as Carbon;
use Validator;
use App\Designation;
use App\VacationCategory;
use App\Violation;
use App\JobLocation;
use App\ItemHrm;
use App\Document;
use App\User;
use App\Department;
use App\Section;
use App\AllowanceCategory;
use App\DeductionCategory;
use App\MedicalCategory;
use App\SocialCategory;
use App\Country;
use App\Role;
use App\UserDetails;
use App\Attendance;
use App\Allowance;
use App\Deduction;
use App\Advance;
use App\Custody;
use App\Vacation;
use App\Training;
use App\Mandate;
use App\Reward;
use App\Subtruction;
use App\EvaluationHrm;
use App\Notif;
use App\Accounting;

class Hrm extends Companies
{

    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->middleware("daftar:hrm");
    }

    // Settings

    public function statistics(){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $users          = User::where("type", 0)->where("company_id", $company->id)->count();
        $departments    = Department::where("company_id", $company->id)->orderby("id", "asc")->count();
        $sections       = Section::where("company_id", $company->id)->orderby("id", "asc")->count();
        $designations   = Designation::where("company_id", $company->id)->orderby("id", "desc")->count();
        $vcategories    = VacationCategory::where("company_id", $company->id)->orderby("id", "desc")->count();
        $violations     = Violation::where("company_id", $company->id)->orderby("id", "desc")->count();
        $locations      = JobLocation::where("company_id", $company->id)->orderby("id", "desc")->count();
        $items          = ItemHrm::where("company_id", $company->id)->orderby("id", "desc")->count();
        $documents      = Document::where("company_id", $company->id)->orderby("id", "desc")->count();

        $violationss    = Violation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $documentss      = Document::where("company_id", $company->id)->orderby("id", "desc")->get();

        return view("company.hrm.statistics", compact("company", "users", "departments", "sections", "designations", "vcategories",
            "violations", "locations", "items", "documents", "violationss", "documentss"));
    }

    public function settings(){
        if(!permissions_general("hrm_settings"))
            return back();

        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        //notification(24, "add_user", $company->id, "hrm");
        //notification(24, "add_allowance", $company->id, "hrm");
        //notification(24, "delete_allowance", $company->id, "hrm");
        //notification(24, "delete_deduction", $company->id, "hrm");
        //notification(24, "add_deduction", $company->id, "hrm");
        //notification(24, "add_advance", $company->id, "hrm");
        //notification(24, "add_custody", $company->id, "hrm");
        //notification(24, "deliver_custody", $company->id, "hrm");
        //notification(24, "delete_custody", $company->id, "hrm");
        //notification(24, "add_vacation", $company->id, "hrm");
        //notification(24, "accept_vacation", $company->id, "hrm");
        //notification(24, "reject_vacation", $company->id, "hrm");
        /*notification(24, "add_training", $company->id, "hrm");
        notification(24, "delete_training", $company->id, "hrm");
        notification(24, "add_mandate", $company->id, "hrm");
        notification(24, "delete_mandate", $company->id, "hrm");*/

        $designations   = Designation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $vcategories    = VacationCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $violations     = Violation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $locations      = JobLocation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $items          = ItemHrm::where("company_id", $company->id)->orderby("id", "desc")->get();
        $documents      = Document::where("company_id", $company->id)->orderby("id", "desc")->get();
        $users          = User::where("type", 0)->where("company_id", $company->id)->get();
        $departments    = Department::where("company_id", $company->id)->orderby("id", "asc")->get();
        $sections       = Section::where("company_id", $company->id)->orderby("id", "asc")->get();
        $roles          = Role::where("company_id", $company->id)->orderby("name", "asc")->get();
        $notifs         = Notif::where("company_id", $company->id)->where("daftar", "hrm")->orderby("id", "asc")->get();


        $children = [];
        foreach($departments as $dep){
            $childrenx = [];
            foreach($sections as $sec){
                if($sec->department_id==$dep->id)
                    array_push($childrenx, ["name"=>"القسم", "title"=>$sec->name, "className"=>"sec"]);
            }
            array_push($children, ["name"=>"الإدارة", "title"=>$dep->name, "className"=>"dep", "children"=>$childrenx]);
        }
        $data = ["name"=>"الشركة", "title"=>$company->name, "children"=>$children];
        $data = json_encode($data);


        return view("company.hrm.settings", compact("company", "designations", "vcategories", "violations", "locations", "items",
            "documents","users", "departments", "sections", "roles", "notifs", "data"));
    }

    function getData(){
        $company        = $this->company;
        $departments    = Department::where("company_id", $company->id)->orderby("id", "asc")->get();
        $sections       = Section::where("company_id", $company->id)->orderby("id", "asc")->get();
        $children = [];
        foreach($departments as $dep){
            $childrenx = [];
            foreach($sections as $sec){
                if($sec->department_id==$dep->id)
                    array_push($childrenx, ["name"=>"القسم", "title"=>$sec->name, "className"=>"sec"]);
            }
            array_push($children, ["name"=>"الإدارة", "title"=>$dep->name, "className"=>"dep", "children"=>$childrenx]);
        }
        $data = ["name"=>"الشركة", "title"=>$company->name, "children"=>$children];
        $data = json_encode($data);
        return $data;
    }

    public function save_permissions(Request $request){
        $company = $this->company;
        $role = Role::find($request->id);
        if(!$role or $role->company_id != $company->id)
            return back();

        $permissions = [];

        if($request->insurance_n_finance_infos)
            array_push($permissions,"insurance_n_finance_infos");
        if($request->add_allowance_cat)
            array_push($permissions,"add_allowance_cat");
        if($request->update_allowance_cat)
            array_push($permissions,"update_allowance_cat");
        if($request->delete_allowance_cat)
            array_push($permissions,"delete_allowance_cat");
        if($request->add_deduction_cat)
            array_push($permissions,"add_deduction_cat");
        if($request->update_deduction_cat)
            array_push($permissions,"update_deduction_cat");
        if($request->delete_deduction_cat)
            array_push($permissions,"delete_deduction_cat");
        if($request->add_medical)
            array_push($permissions,"add_medical");
        if($request->update_medical)
            array_push($permissions,"update_medical");
        if($request->delete_medical)
            array_push($permissions,"delete_medical");
        if($request->add_social)
            array_push($permissions,"add_social");
        if($request->update_social)
            array_push($permissions,"update_social");
        if($request->delete_social)
            array_push($permissions,"delete_social");
        if($request->employees)
            array_push($permissions,"employees");
        if($request->add_employee)
            array_push($permissions,"add_employee");
        if($request->show_employee)
            array_push($permissions,"show_employee");
        if($request->update_employee)
            array_push($permissions,"update_employee");
        if($request->delete_employee)
            array_push($permissions,"delete_employee");

        if($request->finances)
            array_push($permissions,"finances");
        if($request->tab_allowance)
            array_push($permissions,"tab_allowance");
        if($request->add_allowance)
            array_push($permissions,"add_allowance");
        if($request->update_allowance)
            array_push($permissions,"update_allowance");
        if($request->delete_allowance)
            array_push($permissions,"delete_allowance");
        if($request->tab_deduction)
            array_push($permissions,"tab_deduction");
        if($request->add_deduction)
            array_push($permissions,"add_deduction");
        if($request->update_deduction)
            array_push($permissions,"update_deduction");
        if($request->delete_deduction)
            array_push($permissions,"delete_deduction");
        if($request->tab_advance)
            array_push($permissions,"tab_advance");
        if($request->add_advance)
            array_push($permissions,"add_advance");
        if($request->update_advance)
            array_push($permissions,"update_advance");
        if($request->delete_advance)
            array_push($permissions,"delete_advance");
        if($request->tab_custody)
            array_push($permissions,"tab_custody");
        if($request->add_custody)
            array_push($permissions,"add_custody");
        if($request->update_custody)
            array_push($permissions,"update_custody");
        if($request->delete_custody)
            array_push($permissions,"delete_custody");
        if($request->delivered_custody)
            array_push($permissions,"delivered_custody");
        if($request->print_custody)
            array_push($permissions,"print_custody");

        if($request->adminisitrative_affairs)
            array_push($permissions,"adminisitrative_affairs");
        if($request->tab_vacation)
            array_push($permissions,"tab_vacation");
        if($request->add_vacation)
            array_push($permissions,"add_vacation");
        if($request->update_vacation)
            array_push($permissions,"update_vacation");
        if($request->delete_vacation)
            array_push($permissions,"delete_vacation");
        if($request->tab_training)
            array_push($permissions,"tab_training");
        if($request->add_training)
            array_push($permissions,"add_training");
        if($request->update_training)
            array_push($permissions,"update_training");
        if($request->delete_training)
            array_push($permissions,"delete_training");
        if($request->tab_mandate)
            array_push($permissions,"tab_mandate");
        if($request->add_mandate)
            array_push($permissions,"add_mandate");
        if($request->update_mandate)
            array_push($permissions,"update_mandate");
        if($request->delete_mandate)
            array_push($permissions,"delete_mandate");



        if($request->evaluations)
            array_push($permissions,"evaluations");
        if($request->add_evaluation)
            array_push($permissions,"add_evaluation");
        if($request->delete_evaluation)
            array_push($permissions,"delete_evaluation");
        if($request->attendances)
            array_push($permissions,"attendances");
        if($request->vacationsss)
            array_push($permissions,"vacationsss");
        if($request->accept_vacationsss)
            array_push($permissions,"accept_vacationsss");
        if($request->payroll)
            array_push($permissions,"payroll");
        if($request->archive_payroll)
            array_push($permissions,"archive_payroll");
        if($request->print_payroll)
            array_push($permissions,"print_payroll");
        if($request->add_reward)
            array_push($permissions,"add_reward");
        if($request->delete_reward)
            array_push($permissions,"delete_reward");
        if($request->add_substraction)
            array_push($permissions,"add_substraction");
        if($request->delete_substraction)
            array_push($permissions,"delete_substraction");


        if($request->send_accounting)
            array_push($permissions,"send_accounting");

        $permissions = serialize($permissions);

        $role->permissions_hrm = $permissions;
        $role->save();

        return back()->with("success", "تم حفظ الصلاحيات !");
    }

    public function save_designation(Request $request){
        $company        = $this->company;

        $designation                = new Designation;
        $designation->company_id    = $company->id;
        $designation->name          = $request->name;
        $designation->description   = $request->description;
        $designation->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_designation(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $designation = Designation::find($id);
                $designation->name = $v;
                $designation->save();
            }
        }
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'description_') !== false) {
                $k = str_replace("description_", "", $k);
                $id = intval($k);
                $designation = Designation::find($id);
                $designation->description = $v;
                $designation->save();
            }
        }

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_designation($c, $id=null){
        $designation                = Designation::find($id);
        if(!$id or !$designation)
            return;

        $designation->delete();

        //return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_vacation_category(Request $request){
        $company        = $this->company;

        $category               = new VacationCategory;
        $category->company_id   = $company->id;
        $category->name         = $request->name;
        $category->duration     = $request->duration;
        $category->payed        = $request->payed;
        $category->discout      = $request->discout;
        $category->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_vacation_category(Request $request){
        $category               = VacationCategory::find($request->id);
        if(!$category)
            return back();

        $category->name         = $request->name;
        $category->duration     = $request->duration;
        $category->payed        = $request->payed;
        $category->discout      = $request->discout;
        $category->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_vacation_category($c, $id=null){
        $category               = VacationCategory::find($id);
        if(!$id or !$category)
            return;

        $category->delete();

        //return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_violation(Request $request){
        $company        = $this->company;

        $violation                = new Violation;
        $violation->company_id    = $company->id;
        $violation->name          = $request->name;
        $violation->punishment    = $request->punishment;
        $violation->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_violation(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $violation = Violation::find($id);
                $violation->name = $v;
                $violation->save();
            }
        }
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'punishment_') !== false) {
                $k = str_replace("punishment_", "", $k);
                $id = intval($k);
                $violation = Violation::find($id);
                $violation->punishment = $v;
                $violation->save();
            }
        }

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_violation($c, $id=null){
        $violation                = Violation::find($id);
        if(!$id or !$violation)
            return;

        $violation->delete();

        //return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_location(Request $request){
        $company        = $this->company;

        $location                = new JobLocation;
        $location->company_id    = $company->id;
        $location->name          = $request->name;
        $location->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_location(Request $request){
        foreach ($request->all() as $k=>$v){
            if (strpos($k, 'name_') !== false) {
                $k = str_replace("name_", "", $k);
                $id = intval($k);
                $location = JobLocation::find($id);
                $location->name = $v;
                $location->save();
            }
        }
    }

    public function delete_location($c, $id=null){
        $location                = JobLocation::find($id);
        if(!$id or !$location)
            return;

        $location->delete();

        //return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_itemh(Request $request){
        $company        = $this->company;

        $item                = new ItemHrm;
        $item->company_id    = $company->id;
        $item->name          = $request->name;
        $item->designation_id= $request->designation_id;
        $item->description   = $request->description;
        $item->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_itemh(Request $request){
        $item                = ItemHrm::find($request->id);
        if(!$item)
            return;

        $item->name          = $request->name;
        $item->designation_id= $request->designation_id;
        $item->description   = $request->description;
        $item->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_itemh($c, $id=null){
        $item                = ItemHrm::find($id);
        if(!$id or !$item)
            return;

        $item->delete();

        //return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_document(Request $request){
        $company            = $this->company;

        $file               = new Document;
        $file->company_id   = $company->id;
        $file->name         = $request->name;
        $request->validate(["file"=>"required|mimes:jpeg,png,jpg,gif,doc,docx,dot,zip,pdf"]);
        $path               = $request->file->store("uploads/attachments");
        $file->file         = $path;
        $file->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_document(Request $request){
        $file                = Document::find($request->id);
        if(!$file)
            return;

        $file->name          = $request->name;
        $file->save();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_document($c, $id=null){
        $file                = Document::find($id);
        if(!$id or !$file)
            return;

        $file->delete();

        //return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function download_document($c, $id=null){
        $file = Document::find($id);
        if (!$id or !$file)
            return back();

        $filex = base_path("storage/app/".$file->file);

        if(file_exists($filex))
            return response()->download($filex);
        else
            return back();
    }

    public function save_department(Request $request){
        $company            = $this->company;

        $dep               = new Department;
        $dep->company_id   = $company->id;
        $dep->name         = $request->name;
        $dep->user_id      = $request->user_id;
        $dep->save();

        return $this->getData();

        //return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_department(Request $request){
        $dep                = Department::find($request->id);
        if(!$dep)
            return $this->getData();

        $dep->name          = $request->name;
        $dep->user_id       = $request->user_id;
        $dep->save();

        return $this->getData();
    }

    public function delete_department($c, $id=null){
        $dep                = Department::find($id);
        if(!$id or !$dep)
            return $this->getData();

        $dep->delete();

        return $this->getData();
    }

    public function save_section(Request $request){
        $company            = $this->company;

        $sec               = new Section;
        $sec->company_id   = $company->id;
        $sec->name         = $request->name;
        $sec->department_id= $request->department_id;
        $sec->save();

        return $this->getData();
    }

    public function update_section(Request $request){
        $sec                = Section::find($request->id);
        if(!$sec)
            return $this->getData();

        $sec->name          = $request->name;
        $sec->department_id = $request->department_id;
        $sec->save();

        return $this->getData();
    }

    public function delete_section($c, $id=null){
        $sec                = Section::find($id);
        if(!$id or !$sec)
            return $this->getData();

        $sec->delete();

        return $this->getData();
    }

    public function savenotif(Request $request){
        $notifs = Notif::where("daftar", "hrm")->get();
        foreach ($notifs as $notif){
            if($request->has("app_".$notif->id))
                $notif->app = 1;
            else
                $notif->app = 0;
            if($request->has("email_".$notif->id))
                $notif->email = 1;
            else
                $notif->email = 0;
            if($request->has("sms_".$notif->id))
                $notif->sms = 1;
            else
                $notif->sms = 0;
            $notif->message = $request->{"message_".$notif->id};
            $notif->save();
        }

        return back()->with("success", "تم حفظ البيانات");
    }

    // InsurancenFinanceInfos

    public function insurance_n_finance_infos(){
        if(!permissions_hrm("insurance_n_finance_infos"))
            return back();

        $company        = $this->company;
        $allowance_categories = AllowanceCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $deduction_categories = DeductionCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $medical_categories = MedicalCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $social_categories = SocialCategory::where("company_id", $company->id)->orderby("id", "desc")->get();

        return view("company.hrm.insurance_n_finance_infos.index", compact("company", "allowance_categories", "deduction_categories",
            "medical_categories", "social_categories"));
    }

    public function save_allowance_category(Request $request){
        if(!permissions_hrm("add_allowance_cat"))
            return back();

        $company        = $this->company;

        $ac = new AllowanceCategory;
        $ac->company_id = $company->id;
        $ac->name = $request->name;
        $ac->type = $request->type;
        $ac->value = $request->value;
        $ac->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_allowance_category(Request $request){
        if(!permissions_hrm("update_allowance_cat"))
            return back();

        $ac = AllowanceCategory::find($request->id);
        if(!$ac)
            return back();

        $ac->name = $request->name;
        $ac->type = $request->type;
        $ac->value = $request->value;
        $ac->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_allowance_category($c, $id=null){
        if(!permissions_hrm("delete_allowance_cat"))
            return back();

        $ac = AllowanceCategory::find($id);
        if(!$id or !$ac)
            return back();

        $ac->delete();

        return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_deduction_category(Request $request){
        if(!permissions_hrm("add_deduction_cat"))
            return back();

        $company        = $this->company;

        $dc = new DeductionCategory;
        $dc->company_id = $company->id;
        $dc->name = $request->name;
        $dc->type = $request->type;
        $dc->value = $request->value;
        $dc->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_deduction_category(Request $request){
        if(!permissions_hrm("update_deduction_cat"))
            return back();

        $dc = DeductionCategory::find($request->id);
        if(!$dc)
            return back();

        $dc->name = $request->name;
        $dc->type = $request->type;
        $dc->value = $request->value;
        $dc->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_deduction_category($c, $id=null){
        if(!permissions_hrm("delete_deduction_cat"))
            return back();

        $dc = DeductionCategory::find($id);
        if(!$id or !$dc)
            return back();

        $dc->delete();

        return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_medical_category(Request $request){
        if(!permissions_hrm("add_medical"))
            return back();

        $company        = $this->company;

        $dc = new MedicalCategory;
        $dc->company_id = $company->id;
        $dc->name = $request->name;
        $dc->employee = $request->employee;
        $dc->company = $request->companyc;
        $dc->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_medical_category(Request $request){
        if(!permissions_hrm("update_medical"))
            return back();

        $dc = MedicalCategory::find($request->id);
        if(!$dc)
            return back();

        $dc->name = $request->name;
        $dc->employee = $request->employee;
        $dc->company = $request->companyc;
        $dc->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_medical_category($c, $id=null){
        if(!permissions_hrm("delete_medical"))
            return back();

        $dc = MedicalCategory::find($id);
        if(!$id or !$dc)
            return back();

        $dc->delete();

        return back()->with("success", "تم حذف البيانات بنجاح");
    }

    public function save_social_category(Request $request){
        if(!permissions_hrm("add_social"))
            return back();

        $company        = $this->company;

        $sc = new SocialCategory;
        $sc->company_id = $company->id;
        $sc->name = $request->name;
        $sc->employee = $request->employee;
        $sc->company = $request->companyc;
        $sc->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function update_social_category(Request $request){
        if(!permissions_hrm("update_social"))
            return back();

        $sc = SocialCategory::find($request->id);
        if(!$sc)
            return back();

        $sc->name = $request->name;
        $sc->employee = $request->employee;
        $sc->company = $request->companyc;
        $sc->save();

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function delete_social_category($c, $id=null){
        if(!permissions_hrm("delete_social"))
            return back();

        $sc = SocialCategory::find($id);
        if(!$id or !$sc)
            return back();

        $sc->delete();

        return back()->with("success", "تم حذف البيانات بنجاح");
    }

    // Employees

    public function employees(){
        if(!permissions_hrm("employees"))
            return back();

        $company        = $this->company;
        $users          = User::with("details")->where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $departments    = Department::where("company_id", $company->id)->orderby("id", "asc")->get();
        $sections       = Section::where("company_id", $company->id)->orderby("id", "asc")->get();
        $designations   = Designation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $roles = Role::where("company_id", $company->id)->orderby("name", "asc")->get();

        return view("company.hrm.employees.index", compact("company", "users", "departments",
            "sections", "designations", "roles"));
    }

    public function add_employee(){
        if(!permissions_hrm("add_employee"))
            return back();

        $company        = $this->company;
        $users          = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $countries      = Country::orderby("name", "asc")->get();
        $medical_categories = MedicalCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $social_categories = SocialCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $departments     = Department::where("company_id", $company->id)->orderby("id", "asc")->get();
        $locations      = JobLocation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $designations   = Designation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $roles = Role::where("company_id", $company->id)->orderby("name", "asc")->get();

        return view("company.hrm.employees.add", compact("company", "users", "countries",
            "medical_categories", "social_categories", "departments", "locations", "designations", "roles"));
    }

    public function get_sects_by_dep(Request $request){
        $dep_id = $request->dep_id;
        $dep = Department::with("sections")->where("id", $dep_id)->first();
        $html = '<option value="">إختر</option><option value="0">لا يوجد قسم</option>';
        if($dep and count($dep->sections)>0){
            foreach ($dep->sections as $section)
                $html .= '<option value="'.$section->id.'">'.$section->name.'</option>';
        }
        return $html;
    }

    public function save_employee(Request $request){
        $company        = $this->company;

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if($request->hasFile("image"))
            $request->validate(['image' => 'image']);
        if($request->hasFile("cin_photo_path"))
            $request->validate(['cin_photo_path' => 'image']);
        if($request->hasFile("passport_photo_path"))
            $request->validate(['passport_photo_path' => 'image']);
        if($request->hasFile("resume_path"))
            $request->validate(['resume_path' => 'image']);
        if($request->hasFile("contract_paper_path"))
            $request->validate(['contract_paper_path' => 'image']);
        if($request->hasFile("other_document_path"))
            $request->validate(['other_document_path' => 'image']);

        $user              = new User;
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->phone       = $request->phone;
        $user->role_id     = $request->role_id;
        $user->status      = $request->status;
        $user->type        = 0;
        $user->password    = bcrypt($request->password);
        $user->company_id       = $company->id;
        if($request->hasFile("image")){
            $path = $request->file('image')->store('uploads/users');
            $user->image = $path;
        }
        $user->save();

        $user_details = new UserDetails;
        $user_details->user_id = $user->id;
        $user_details->birthday = $request->birthday;
        $user_details->gender = $request->gender;
        $user_details->nationality = $request->nationality;
        $user_details->address = $request->address;
        $user_details->qualification = $request->qualification;
        $user_details->num_passport = $request->num_passport;
        $user_details->date_passport = $request->date_passport;
        $user_details->num_identity = $request->num_identity;
        $user_details->date_identity = $request->date_identity;
        $user_details->social_status = $request->social_status;
        $user_details->partner_name = $request->partner_name;
        $user_details->partner_birthday = $request->partner_birthday;
        $user_details->son_name = implode(";", $request->son_name);
        $user_details->son_birthday = implode(";", $request->son_birthday);
        $user_details->department_id = $request->department_id;
        $user_details->section_id = $request->section_id;
        $user_details->direct_manager_id = $request->direct_manager_id;
        $user_details->joining_date = $request->joining_date;
        $user_details->retirement_date = $request->retirement_date;
        $user_details->job_time = $request->job_time;
        $user_details->job_location_id = $request->job_location_id;
        $user_details->designation_id = $request->designation_id;
        $user_details->salary = $request->salary;
        $user_details->bank_name = $request->bank_name;
        $user_details->branch_name = $request->branch_name;
        $user_details->account_name = $request->account_name;
        $user_details->account_number = $request->account_number;
        $user_details->medical = $request->medical;
        $user_details->social = $request->social;
        if($request->hasFile("cin_photo_path")){
            $path = $request->file('cin_photo_path')->store('uploads/users');
            $user_details->cin_photo_path = $path;
        }
        if($request->hasFile("passport_photo_path")){
            $path = $request->file('passport_photo_path')->store('uploads/users');
            $user_details->passport_photo_path = $path;
        }
        if($request->hasFile("resume_path")){
            $path = $request->file('resume_path')->store('uploads/users');
            $user_details->resume_path = $path;
        }
        if($request->hasFile("contract_paper_path")){
            $path = $request->file('contract_paper_path')->store('uploads/users');
            $user_details->contract_paper_path = $path;
        }
        if($request->hasFile("other_document_path")){
            $path = $request->file('other_document_path')->store('uploads/users');
            $user_details->other_document_path = $path;
        }
        $user_details->save();

        notification($user->id, "add_user", $company->id, "hrm");

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    public function employee($c, $id=null){
        if(!permissions_hrm("show_employee"))
            return back();

        $company        = $this->company;
        $employee = User::with("details")->where("id", $id)->first();
        if(!$id or !$employee or @$employee->company_id!=$company->id)
            return back();

        return view("company.hrm.employees.show", compact("company", "employee"));
    }

    public function delete_employee($c, $id){
        if(!permissions_hrm("delete_employee"))
            return back();

        $company        = $this->company;
        $employee = User::with("details")->where("id", $id)->first();
        if(!$id or !$employee or @$employee->company_id!=$company->id)
            return back();

        notification($employee->id, "delete_user", $company->id, "hrm");

        if($employee->details){
            $employee->details->delete();
        }
        $employee->delete();



        return back()->with("success", "تم حذف الموظف بنجاح");
    }

    public function edit_employee($c, $id){
        if(!permissions_hrm("update_employee"))
            return back();

        $company        = $this->company;
        $employee = User::with("details")->where("id", $id)->first();
        if(!$id or !$employee or @$employee->company_id!=$company->id)
            return back();

        $users          = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $countries      = Country::orderby("name", "asc")->get();
        $medical_categories = MedicalCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $social_categories = SocialCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $departments     = Department::where("company_id", $company->id)->orderby("id", "asc")->get();
        $locations      = JobLocation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $designations   = Designation::where("company_id", $company->id)->orderby("id", "desc")->get();
        $roles = Role::where("company_id", $company->id)->orderby("name", "asc")->get();

        return view("company.hrm.employees.edit", compact("company", "employee", "users", "countries",
            "medical_categories", "social_categories", "departments", "locations", "designations", "roles"));
    }

    public function update_employee(Request $request){
        if(!permissions_hrm("update_employee"))
            return back();

        $company        = $this->company;
        $user = User::with("details")->where("id", $request->id)->first();
        if(!$user or @$user->company_id!=$company->id)
            return back();

        $userx = User::where("id", "!=", $request->id)->where("company_id", $company->id)->where("email", $request->email)->first();

        $usery = User::where("id", "!=", $request->id)->where("company_id", $company->id)->where("phone", $request->phone)->first();

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|max:255',
        ]);
        if($request->hasFile("image"))
            $request->validate(['image' => 'image']);
        if($request->hasFile("cin_photo_path"))
            $request->validate(['cin_photo_path' => 'image']);
        if($request->hasFile("passport_photo_path"))
            $request->validate(['passport_photo_path' => 'image']);
        if($request->hasFile("resume_path"))
            $request->validate(['resume_path' => 'image']);
        if($request->hasFile("contract_paper_path"))
            $request->validate(['contract_paper_path' => 'image']);
        if($request->hasFile("other_document_path"))
            $request->validate(['other_document_path' => 'image']);


        $user->name        = $request->name;
        if(!$userx)
            $user->email       = $request->email;
        if(!$usery)
            $user->phone       = $request->phone;
        $user->role_id     = $request->role_id;
        $user->status      = $request->status;
        $user->type        = 0;
        if($request->password and $request->password!=""){
            $request->validate(['password' => 'string|min:6']);
            $user->password    = bcrypt($request->password);
        }
        $user->company_id       = $company->id;
        if($request->hasFile("image")){
            $path = $request->file('image')->store('uploads/users');
            $user->image = $path;
        }
        $user->save();

        $user_details = UserDetails::where("user_id", $user->id)->first();
        if(!$user_details)
            $user_details = new UserDetails;
        $user_details->user_id = $user->id;
        $user_details->birthday = $request->birthday;
        $user_details->gender = $request->gender;
        $user_details->nationality = $request->nationality;
        $user_details->address = $request->address;
        $user_details->qualification = $request->qualification;
        $user_details->num_passport = $request->num_passport;
        $user_details->date_passport = $request->date_passport;
        $user_details->num_identity = $request->num_identity;
        $user_details->date_identity = $request->date_identity;
        $user_details->social_status = $request->social_status;
        $user_details->partner_name = $request->partner_name;
        $user_details->partner_birthday = $request->partner_birthday;
        $user_details->son_name = implode(";", $request->son_name);
        $user_details->son_birthday = implode(";", $request->son_birthday);
        $user_details->department_id = $request->department_id;
        $user_details->section_id = $request->section_id;
        $user_details->direct_manager_id = $request->direct_manager_id;
        $user_details->joining_date = $request->joining_date;
        $user_details->retirement_date = $request->retirement_date;
        $user_details->job_time = $request->job_time;
        $user_details->job_location_id = $request->job_location_id;
        $user_details->designation_id = $request->designation_id;
        $user_details->salary = $request->salary;
        $user_details->bank_name = $request->bank_name;
        $user_details->branch_name = $request->branch_name;
        $user_details->account_name = $request->account_name;
        $user_details->account_number = $request->account_number;
        $user_details->medical = $request->medical;
        $user_details->social = $request->social;
        if($request->hasFile("cin_photo_path")){
            $path = $request->file('cin_photo_path')->store('uploads/users');
            $user_details->cin_photo_path = $path;
        }
        if($request->hasFile("passport_photo_path")){
            $path = $request->file('passport_photo_path')->store('uploads/users');
            $user_details->passport_photo_path = $path;
        }
        if($request->hasFile("resume_path")){
            $path = $request->file('resume_path')->store('uploads/users');
            $user_details->resume_path = $path;
        }
        if($request->hasFile("contract_paper_path")){
            $path = $request->file('contract_paper_path')->store('uploads/users');
            $user_details->contract_paper_path = $path;
        }
        if($request->hasFile("other_document_path")){
            $path = $request->file('other_document_path')->store('uploads/users');
            $user_details->other_document_path = $path;
        }
        $user_details->save();

        if($userx)
            return back()->with("error", "البريد الإلكتروني مستخدم");

        if($usery)
            return back()->with("error", "رقم الهاتف مستخدم");

        return back()->with("success", "تم حفظ البيانات بنجاح");
    }

    // Attendance

    public function save_attendance($c){
        $company        = $this->company;
        $user           = Auth::guard("user")->user();
        $today = date('Y-m-d');
        $now = date('h:i');

        $att = Attendance::where(["company_id"=>$company->id, "user_id"=>$user->id, "status"=>"open", "thedate"=>$today])->first();
        if($att){
            $att->end_time = $now;
            $att->status = "closed";
            $att->save();
            return back()->with("success", "تم تسجيل الخروج");
        }else{
            $att = new Attendance;
            $att->company_id = $company->id;
            $att->user_id = $user->id;
            $att->thedate = $today;
            $att->start_time = $now;
            $att->status = "open";
            $att->save();
            return back()->with("success", "تم تسجيل الدخول");
        }
    }

    public function attendances(){
        if(!permissions_hrm("attendances"))
            return back();

        $company        = $this->company;
        $today = date('Y-m-d');
        $users          = User::with("details")->where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();

        foreach ($users as $user){
            $att = Attendance::where("company_id",$company->id)->where("user_id", $user->id)->where("thedate", $today)->first();
            $user->att = $att;
            $vac = vacation::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
            $user->vac = $vac;
            $tra = Training::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
            $user->tra = $tra;
            $man = Mandate::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
            $user->man = $man;
        }

        return view("company.hrm.administration.attendances", compact("company", "users", "today"));
    }

    public function filter_attendance(Request $request){
        $company        = $this->company;
        $today = date('Y-m-d');
        $from = $request->from;
        $users_ids = $request->user_id;
        $to = $request->to;
        $users          = User::with("details")->where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get()->keyby("id");

        if($users_ids) {
            foreach ($users as $user){
                if(!in_array($user->id, $users_ids))
                    $users->forget($user->id);
            }
        }


        $html = "";
        if(!$from and !$to){
            foreach($users as $user):
                $att = Attendance::where("company_id",$company->id)->where("user_id", $user->id)->where("thedate", $today)->first();
                $user->att = $att;
                $vac = vacation::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
                $user->vac = $vac;
                $tra = Training::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
                $user->tra = $tra;
                $man = Mandate::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
                $user->man = $man;
                $html .= view("company.hrm.administration.attendance_row", ["user"=> $user, "today"=>$today])->render();
            endforeach;
        }else{

            if($from and !$to){
                $d1 = Carbon::parse($from);
                $d2 = Carbon::parse($from);
                $d2 = $d2->addWeek(1);
            }elseif(!$from and $to){
                $d1 = Carbon::parse($to);
                $d1 = $d1->subWeek(1);
                $d2 = Carbon::parse($to);

            }else{
                $d1 = Carbon::parse($from);
                $d2 = Carbon::parse($to);
            }
            $d1 = $d1->format('Y-m-d');
            $d2 = $d2->format('Y-m-d');
            $period = CarbonPeriod::create($d1, $d2);
            foreach ($period as $date) {
                $date =  $date->format('Y-m-d');
                foreach($users as $user):
                    $att = Attendance::where("company_id",$company->id)->where("user_id", $user->id)->where("thedate", $date)->first();
                    $user->att = $att;
                    $vac = vacation::where("from_date", "<=", $date)->where("to_date", ">=", $date)->where("company_id",$company->id)->where("user_id", $user->id)->first();
                    $user->vac = $vac;
                    $tra = Training::where("from_date", "<=", $date)->where("to_date", ">=", $date)->where("company_id",$company->id)->where("user_id", $user->id)->first();
                    $user->tra = $tra;
                    $man = Mandate::where("from_date", "<=", $date)->where("to_date", ">=", $date)->where("company_id",$company->id)->where("user_id", $user->id)->first();
                    $user->man = $man;
                    $html .= view("company.hrm.administration.attendance_row", ["user"=> $user, "today"=>$date])->render();
                endforeach;
            }
        }


        return $html;
    }

    // Finances

    public function tabsnum($c, $num=1){
        session()->put('tabsnum', $num);
    }

    public function finances($c, $id=null){
        if(!permissions_hrm("finances"))
            return back();

        $company                = $this->company;
        $employee               = User::with("details")->where("id", $id)->first();
        if(!$id or !$employee or @$employee->company_id!=$company->id)
            return back();
        $allowance_categories   = AllowanceCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $allowances             = Allowance::with("category")->where(["company_id"=>$company->id, "user_id"=>$employee->id])->orderby("id", "asc")->get();
        $deduction_categories   = DeductionCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $deductions             = Deduction::with("category")->where(["company_id"=>$company->id, "user_id"=>$employee->id])->orderby("id", "asc")->get();
        $users                  = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $advances               = Advance::with("kafil")->where(["company_id"=>$company->id, "user_id"=>$employee->id])->orderby("id", "asc")->get();
        $custodies              = Custody::where(["user_id"=>$employee->id, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $today = date("Y-m-d");

        return view("company.hrm.finances.index", compact("company", "employee", "allowance_categories", "allowances", "today",
            "deduction_categories", "deductions", "users", "advances", "custodies"));
    }

    public function add_allowance(Request $request){
        if(!permissions_hrm("add_allowance"))
            return;

        $company                            = $this->company;

        $allowance                          = new Allowance;
        $allowance->company_id              = $company->id;
        $allowance->user_id                 = $request->user_id;
        $allowance->allowance_category_id   = $request->allowance_category_id;
        $allowance->from_date               = $request->from_date;
        $allowance->to_date                 = $request->to_date;
        $allowance->save();

        notification($request->user_id, "add_allowance", $company->id, "hrm");
    }

    public function delete_allowance($c, $id){
        if(!permissions_hrm("delete_allowance"))
            return;

        $company = $this->company;
        $allowance = Allowance::where(["id"=>$id, "company_id"=>$company->id])->first();
        if(!$id or !$allowance)
            return;

        notification($allowance->user_id, "delete_allowance", $company->id, "hrm");

        $allowance->delete();
    }

    public function update_allowance(Request $request){
        if(!permissions_hrm("update_allowance"))
            return;

        $company                            = $this->company;

        $allowance                          = Allowance::with("category")->where(["company_id"=>$company->id, "id"=>$request->id])->first();
        if(!$allowance)
            return;

        $allowance->allowance_category_id   = $request->allowance_category_id;
        $allowance->from_date               = $request->from_date;
        $allowance->to_date                 = $request->to_date;
        $allowance->save();
    }

    public function add_deduction(Request $request){
        if(!permissions_hrm("add_deduction"))
            return;

        $company                            = $this->company;

        $deduction                          = new Deduction;
        $deduction->company_id              = $company->id;
        $deduction->user_id                 = $request->user_id;
        $deduction->deduction_category_id   = $request->deduction_category_id;
        $deduction->amount                  = $request->amount;
        $deduction->from_date               = $request->from_date;
        $deduction->to_date                 = $request->to_date;
        $deduction->save();

        notification($request->user_id, "add_deduction", $company->id, "hrm");
    }

    public function delete_deduction($c, $id){
        if(!permissions_hrm("delete_deduction"))
            return;

        $company = $this->company;
        $deduction = Deduction::where(["id"=>$id, "company_id"=>$company->id])->first();
        if(!$id or !$deduction)
            return;

        notification($deduction->user_id, "delete_deduction", $company->id, "hrm");
        $deduction->delete();
    }

    public function update_deduction(Request $request){
        if(!permissions_hrm("update_deduction"))
            return;

        $company                            = $this->company;

        $deduction                          = Deduction::with("category")->where(["company_id"=>$company->id, "id"=>$request->id])->first();
        if(!$deduction)
            return;


        $deduction->deduction_category_id   = $request->deduction_category_id;
        $deduction->amount                  = $request->amount;
        $deduction->from_date               = $request->from_date;
        $deduction->to_date                 = $request->to_date;
        $deduction->save();
    }

    public function add_advance(Request $request){
        if(!permissions_hrm("add_advance"))
            return;

        $company                = $this->company;

        $advance                = new Advance;
        $advance->company_id    = $company->id;
        $advance->user_id       = $request->user_id;
        $advance->amount        = $request->amount;
        $advance->rest          = $request->amount;
        $advance->type          = $request->type;
        $advance->monthly       = $request->monthly;
        $advance->months        = $request->months;
        $advance->thedate       = $request->thedate;
        $advance->kafil_id      = $request->kafil_id;
        $advance->save();

        notification($request->user_id, "add_advance", $company->id, "hrm");
    }

    public function delete_advance($c, $id){
        if(!permissions_hrm("delete_advance"))
            return;

        $company = $this->company;
        $advance = Advance::where(["id"=>$id, "company_id"=>$company->id])->first();
        if(!$id or !$advance)
            return;

        notification($advance->user_id, "delete_advance", $company->id, "hrm");
        $advance->delete();
    }

    public function update_advance(Request $request){
        if(!permissions_hrm("update_advance"))
            return;

        $company                            = $this->company;

        $advance                          = Advance::where(["company_id"=>$company->id, "id"=>$request->id])->first();
        if(!$advance)
            return;

        $advance->rest          = $request->rest;
        $advance->monthly       = $request->monthly;
        $advance->months        = $request->months;
        $advance->save();
    }

    public function add_custody(Request $request){
        if(!permissions_hrm("add_custody"))
            return back();

        $company                = $this->company;
        $custody                = new Custody;
        $custody->company_id    = $company->id;
        $custody->user_id       = $request->user_id;
        $custody->reference     = $request->reference;
        $custody->name          = $request->name;
        $custody->thedate       = $request->thedate;
        $custody->number        = $request->number;
        $custody->description   = $request->description;
        if($request->has("file")) {
            $request->validate(["file"=>"image"]);
            $path = $request->file->store('uploads/attachments');
            $custody->file = $path;
        }
        $custody->save();

        notification($custody->user_id, "add_custody", $company->id, "hrm");

        return back()->with("success", "تم إضافة العهدة بنجاح");

    }

    public function delivered_custody($c, $id=null){
        if(!permissions_hrm("delivered_custody"))
            return;

        $company = $this->company;
        $custody = Custody::where(["id"=>$id, "company_id"=>$company->id])->first();
        if(!$id or !$custody)
            return;

        $custody->delivered = 1;
        $custody->save();

        notification($custody->user_id, "deliver_custody", $company->id, "hrm");
    }

    public function update_custody(Request $request){
        if(!permissions_hrm("update_custody"))
            return;

        $company                            = $this->company;

        $custody                          = Custody::where(["company_id"=>$company->id, "id"=>$request->id])->first();
        if(!$custody)
            return;

        $custody->name          = $request->name;
        $custody->thedate       = $request->thedate;
        $custody->number        = $request->number;
        $custody->description   = $request->description;
        $custody->save();
    }

    public function delete_custody($c, $id=null){
        if(!permissions_hrm("delete_custody"))
            return;

        $company = $this->company;

        $custody = Custody::where(["company_id" => $company->id, "id" => $id])->first();
        if (!$custody)
            return;

        notification($custody->user_id, "delete_custody", $company->id, "hrm");
        $custody->delete();

    }

    // Adminisitrative Affairs

    public function adminisitrative_affairs($c, $id=null){
        if(!permissions_hrm("adminisitrative_affairs"))
            return back();

        $company                = $this->company;
        $employee               = User::with("details")->where("id", $id)->first();
        if(!$id or !$employee or @$employee->company_id!=$company->id)
            return back();

        $users                  = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $vacationCategories     = VacationCategory::where("company_id", $company->id)->orderby("name", "asc")->get();
        $vacations              = Vacation::with("category")->where(["company_id"=>$company->id, "user_id"=>$employee->id])->orderby("id", "desc")->get();
        $trainings              = Training::where(["company_id"=>$company->id, "user_id"=>$employee->id])->orderby("id", "desc")->get();
        $mandates               = Mandate::where(["company_id"=>$company->id, "user_id"=>$employee->id])->orderby("id", "desc")->get();
        $today = date("Y-m-d");

        return view("company.hrm.adminisitrative_affairs.index", compact("company", "employee", "today", "users", "vacationCategories",
            "vacations", "trainings", "mandates"));
    }

    public function add_vacation(Request $request){
        if(!permissions_hrm("add_vacation"))
            return;

        $company                = $this->company;

        $vacation               = new Vacation;
        $vacation->company_id   = $company->id;
        $vacation->user_id      = $request->user_id;
        $vacation->vacation_category_id = $request->vacation_category_id;
        $vacation->from_date      = $request->from_date;
        $vacation->to_date      = $request->to_date;
        $vacation->details      = $request->details;
        if($request->hasFile("file")){
            $validator = Validator::make($request->all(), ["file"=>"image"]);
            if(!$validator->fails()){
                $path = $request->file->store('uploads/attachments');
                $vacation->file = $path;
            }else{
                return response()->json(["error"=>$validator->errors()->first()]);
            }
        }
        $vacation->save();

        notification($vacation->user_id, "add_vacation", $company->id, "hrm");
    }

    public function delete_vacation($c, $id=null){
        if(!permissions_hrm("delete_vacation"))
            return;

        $company = $this->company;

        $vacation = Vacation::where(["company_id" => $company->id, "id" => $id])->first();
        if (!$vacation)
            return;

        $vacation->delete();

    }

    public function update_vacation(Request $request){
        if(!permissions_hrm("update_vacation"))
            return;

        $company                = $this->company;

        $vacation               = Vacation::where(["company_id"=>$company->id, "id"=>$request->id])->first();
        if(!$vacation)
            return;

        $vacation->vacation_category_id = $request->vacation_category_id;
        $vacation->from_date      = $request->from_date;
        $vacation->to_date      = $request->to_date;
        $vacation->details      = $request->details;
        $vacation->save();
    }

    public function add_training(Request $request){
        if(!permissions_hrm("add_training"))
            return;

        $company                = $this->company;

        $training               = new Training;
        $training->company_id   = $company->id;
        $training->user_id      = $request->user_id;
        $training->name         = $request->name;
        $training->price        = $request->price;
        $training->entreprise   = $request->entreprise;
        $training->from_date    = $request->from_date;
        $training->to_date      = $request->to_date;
        $training->details      = $request->details;
        if($request->hasFile("file")){
            $validator = Validator::make($request->all(), ["file"=>"image"]);
            if(!$validator->fails()){
                $path = $request->file->store('uploads/attachments');
                $training->file = $path;
            }else{
                return response()->json(["error"=>$validator->errors()->first()]);
            }
        }
        $training->save();

        notification($training->user_id, "add_training", $company->id, "hrm");
    }

    public function delete_training($c, $id=null){
        if(!permissions_hrm("delete_training"))
            return;

        $company = $this->company;

        $training = Training::where(["company_id" => $company->id, "id" => $id])->first();
        if (!$training)
            return;

        notification($training->user_id, "delete_training", $company->id, "hrm");
        $training->delete();

    }

    public function update_training(Request $request){
        if(!permissions_hrm("update_training"))
            return;

        $company                = $this->company;

        $training = Training::where(["company_id" => $company->id, "id" => $request->id])->first();
        if (!$training)
            return;

        $training->name         = $request->name;
        $training->price        = $request->price;
        $training->entreprise   = $request->entreprise;
        $training->from_date    = $request->from_date;
        $training->to_date      = $request->to_date;
        $training->details      = $request->details;

        $training->save();
    }

    public function add_mandate(Request $request){
        if(!permissions_hrm("add_mandate"))
            return;

        $company                = $this->company;

        $mandate               = new Mandate;
        $mandate->company_id   = $company->id;
        $mandate->user_id      = $request->user_id;
        $mandate->task         = $request->task;
        $mandate->entreprise   = $request->entreprise;
        $mandate->address      = $request->address;
        $mandate->type         = $request->type;
        $mandate->price        = $request->price;
        $mandate->from_date    = $request->from_date;
        $mandate->to_date      = $request->to_date;
        $mandate->details      = $request->details;
        if($request->hasFile("file")){
            $validator = Validator::make($request->all(), ["file"=>"image"]);
            if(!$validator->fails()){
                $path = $request->file->store('uploads/attachments');
                $mandate->file = $path;
            }else{
                return response()->json(["error"=>$validator->errors()->first()]);
            }
        }
        $mandate->save();

        notification($mandate->user_id, "add_mandate", $company->id, "hrm");
    }

    public function delete_mandate($c, $id=null){
        if(!permissions_hrm("delete_mandate"))
            return;

        $company = $this->company;

        $mandate = Mandate::where(["company_id" => $company->id, "id" => $id])->first();
        if (!$mandate)
            return;

        notification($mandate->user_id, "delete_mandate", $company->id, "hrm");
        $mandate->delete();
    }

    public function update_mandate(Request $request){
        if(!permissions_hrm("update_mandate"))
            return;

        $company                = $this->company;

        $mandate = Mandate::where(["company_id" => $company->id, "id" => $request->id])->first();
        if (!$mandate)
            return;

        $mandate->task         = $request->task;
        $mandate->entreprise   = $request->entreprise;
        $mandate->address      = $request->address;
        $mandate->type         = $request->type;
        $mandate->price        = $request->price;
        $mandate->from_date    = $request->from_date;
        $mandate->to_date      = $request->to_date;
        $mandate->details      = $request->details;

        $mandate->save();
    }

    // Payroll

    public function payroll(){
        if(!permissions_hrm("payroll"))
            return back();

        $company        = $this->company;
        $today          = date('Y-m-d');
        $users          = User::with("details", "allowances", "deductions", "rewards", "substractions", "avacations.category")->where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $current_month  = date("m");
        $current_year   = date("Y");
        $first          = Carbon::parse($current_year."-".$current_month."-1")->startOfMonth()->format("Y-m-d");
        $end            = Carbon::parse($current_year."-".$current_month."-1")->endOfMonth()->format("Y-m-d");
        
        foreach ($users as $user){
            $att = Attendance::where("company_id",$company->id)->where("user_id", $user->id)->where("thedate", $today)->first();
            $user->att = $att;
            $vac = vacation::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
            $user->vac = $vac;
            $tra = Training::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
            $user->tra = $tra;
            $man = Mandate::where("from_date", "<=", $today)->where("to_date", ">=", $today)->where("company_id",$company->id)->where("user_id", $user->id)->first();
            $user->man = $man;
        }

        return view("company.hrm.administration.payroll", compact("company", "users", "today", "current_month", "current_year",
            "first", "end"));
    }

    public function filter_pay(Request $request){
        $company        = $this->company;

        $users          = User::with("details", "allowances", "deductions", "rewards", "substractions", "avacations.category")->where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get()->keyby("id");
        if($request->user_id and !empty($request->user_id)){
            foreach ($users as $user)
                if(!in_array($user->id, $request->user_id))
                    $users->forget($user->id);
        }
        $current_year   = $request->year;
        $current_month  = $request->month;
        $first          = Carbon::parse($current_year."-".$current_month."-1")->startOfMonth()->format("Y-m-d");
        $end            = Carbon::parse($current_year."-".$current_month."-1")->endOfMonth()->format("Y-m-d");
        $months = ["جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];

        $html = '';
        foreach ($users as $user){
            $salary = (@$user->details->salary and @$user->details->salary!=0)?@$user->details->salary:0;
            $get = $set = 0;
            $html .= view("company.hrm.administration.payroll_row", compact("user", "company", "first", "end",
                "salary", "get", "set", "current_year", "current_month", "months"))->render();
        }

        return $html;
    }

    public function save_reward(Request $request){
        $company            = $this->company;

        $reward             = new Reward;
        $reward->company_id = $company->id;
        $reward->user_id    = $request->user_id;
        $reward->month      = $request->month;
        $reward->year       = $request->year;
        $reward->name       = $request->name;
        $reward->amount     = $request->amount;
        $reward->save();
    }

    public function delete_reward($c, $id){
        $reward = Reward::find($id);
        $reward->delete();
    }

    public function save_substraction(Request $request){
        $company            = $this->company;

        $substraction            = new Subtruction;
        $substraction->company_id = $company->id;
        $substraction->user_id    = $request->user_id;
        $substraction->month      = $request->month;
        $substraction->year       = $request->year;
        $substraction->name       = $request->name;
        $substraction->amount     = $request->amount;
        $substraction->save();
    }

    public function delete_substraction($c, $id){

        $substraction = Subtruction::find($id);
        $substraction->delete();
    }

    // Vacations

    public function vacations(){
        if(!permissions_hrm("vacationsss"))
            return back();

        $company            = $this->company;

        $vacations          = Vacation::with("user", "category")->where("company_id", $company->id)->orderby("id", "desc")->get();
        $vacationCategories = VacationCategory::where("company_id", $company->id)->orderby("id", "desc")->get();
        $users                  = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();

        return view("company.hrm.administration.vacations", compact("company", "vacations", "vacationCategories", "users"));
    }

    public function accept_vacation($c, $id=null){
        $company = $this->company;

        $vacation = Vacation::where(["company_id" => $company->id, "id" => $id])->first();
        if (!$vacation)
            return;

        $vacation->status = 1;
        $vacation->save();
        notification($vacation->user_id, "accept_vacation", $company->id, "hrm");
    }

    public function reject_vacation($c, $id=null){
        $company = $this->company;

        $vacation = Vacation::where(["company_id" => $company->id, "id" => $id])->first();
        if (!$vacation)
            return;

        $vacation->status = 2;
        $vacation->save();
        notification($vacation->user_id, "reject_vacation", $company->id, "hrm");
    }

    // Evaluation

    public function evaluations(){
        if(!permissions_hrm("evaluations"))
            return back();

        $company    = $this->company;

        $users      = User::with("details")->where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $item_hrms  = ItemHrm::with("designation")->where(["company_id"=>$company->id])->orderby("id", "desc")->get();
        $evaluationHrms = EvaluationHrm::with("user")->where(["company_id"=>$company->id])->orderby("id", "desc")->get();
        foreach ($evaluationHrms as $ev){
            $stars = unserialize($ev->stars);
            $new_stars = [];
            foreach ($stars as $k=>$v){
                $item_hrm = ItemHrm::with("designation")->where("id", $k)->where(["company_id"=>$company->id])->orderby("id", "desc")->first();
                if($item_hrm){
                    array_push($new_stars, ["item"=>$item_hrm, "value"=>$v]);
                }
            }
            $ev->stars = $new_stars;
        }

        return view("company.hrm.administration.evaluations", compact("company", "vacations", "vacationCategories", "users",
            "item_hrms", "evaluationHrms"));
    }

    public function get_items_hrm($c, $user_id,$des_id=null){
        $company    = $this->company;

        $user = User::find($user_id);
        $item_hrms1  = ItemHrm::with("designation")->where(["company_id"=>$company->id])->where("designation_id", 0)->orderby("id", "desc")->get();
        if($des_id and $des_id!=null){
            $item_hrms2  = ItemHrm::with("designation")->where(["company_id"=>$company->id])->orWhere("designation_id", $des_id)->orderby("id", "desc")->get();
            $item_hrms1 = $item_hrms1->merge($item_hrms2);
        }

        return view("company.hrm.administration.evaluation_items", compact("item_hrms1", "user", "company"))->render();
    }

    public function save_evaluation(Request $request){
        if(!permissions_hrm("add_evaluation"))
            return;

        $company    = $this->company;

        $evaluationHrms = new EvaluationHrm;
        $evaluationHrms->company_id = $company->id;
        $evaluationHrms->user_id = $request->user_id;
        $evaluationHrms->notes = $request->notes;
        $evaluationHrms->stars = serialize($request->stars);
        $evaluationHrms->save();
    }

    public function delete_ev($c, $id){
        if(!permissions_hrm("delete_evaluation"))
            return;

        $evaluationHrm = EvaluationHrm::find($id);
        $evaluationHrm->delete();
    }

    // Accounting

    public function accounting(){
        $company    = $this->company;
        $userx       = Auth::guard("user")->user();
        $users      = User::where(["type"=>0, "company_id"=>$company->id])->orderby("id", "desc")->get();
        $accountings= Accounting::with("from", "to")->where("user_from", $userx->id)->Orwhere("user_to", $userx->id)->orderby("id", "desc")->get();

        return view("company.hrm.accounting", compact("company", "users", "accountings", "userx"));
    }

    public function send_accounting(Request $request){
        $company    = $this->company;
        $user           = Auth::guard("user")->user();

        if($request->id)
            $accounting = Accounting::find($request->id);
        else
            $accounting = new Accounting;

        if($request->status==0){
            $accounting->user_to = $request->user_to;
            $accounting->user_from = $user->id;
            $accounting->status = $request->status;
            $accounting->message1 = $request->message1;
            notification($accounting->user_to, "send_accounting", $company->id, "hrm");
        }elseif($request->status==1) {
            $accounting->status = $request->status;
            $accounting->message2 = $request->message2;
        }elseif($request->status==2) {
            $accounting->status = $request->status;
            $accounting->message3 = $request->message3;
        }
        $accounting->save();

        return back()->with("success", "تم حفظ البيانات");
        }

}
