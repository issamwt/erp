<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Company\Companies;
use App\User;
use App\Company;

class ClientController extends Companies
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $clients = User::where("type", 1)->where("company_id", @$this->company->id)->with("company")->orderby("id", "desc")->get();
        $company = $this->company;

        return view("company.clients.index", compact("clients", "company"));
    }

    public function create(){
        if(!permissions_general("add_client"))
            return back();
        $companies = Company::orderby("name", "asc")->get();
        $company = $this->company;

        return view("company.clients.create", compact("companies", "company"));
    }

    public function store(Request $request){
        if(!permissions_general("add_client"))
            return back();
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
            'company_id' => 'required',
            'image' => 'required|image',
        ]);
        $client            = new User;
        $client->name        = $request->name;
        $client->email       = $request->email;
        $client->phone       = $request->phone;
        $client->role_id        = $request->role;
        $client->status      = $request->status;
        $client->type        = 1;
        $client->password    = bcrypt($request->password);
        $client->company_id       = $request->company_id;
        $path = $request->file('image')->store('uploads/clients');
        $client->image = $path;
        $client->save();

        return redirect()->back()->with("success", "تم حفظ العميل الجديد !");
    }

    public function show($c, $id){
        if(!permissions_general("show_client"))
            return back();
        $user = User::with("role")->where("type", 1)->where("company_id", @$this->company->id)->where("id", $id)->first();
        $company = $this->company;

        return view("company.clients.show", compact("user", "company"));
    }

    public function edit($c, $id=null){
        if(!permissions_general("update_client"))
            return back();
        $client = User::find($id);
        if(!$id or !$client)
            return redirect()->back();
        $companies = Company::orderby("name", "asc")->get();
        $company = $this->company;

        return view("company.clients.edit", compact("client", "companies", "company"));
    }

    public function update(Request $request,$c, $id){
        if(!permissions_general("update_client"))
            return back();
        $client = User::find($id);
        if(!$id or !$client)
            return redirect()->back();

        $request->validate(['name' => 'required|string|max:255', 'email' => 'required|string|email|max:255']);
        if($request->password)
            $request->validate(['password' => 'required|string|min:6']);

        $other = User::where("email", $request->email)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "البريد الإلكتروني مستعمل !");

        $other = User::where("phone", $request->phone)->where("id", "!=", $id)->first();
        if($other)
            return redirect()->back()->with("error", "رقم الجوال مستعمل !");

        $client->name        = $request->name;
        $client->email       = $request->email;
        $client->phone       = $request->phone;
        $client->role_id        = $request->role;
        $client->status      = $request->status;
        if($request->password)
            $client->password    = bcrypt($request->password);
        if($request->hasFile("image")){
            $request->validate(['image' => 'required|image']);
            $path = $request->file('image')->store('uploads/clients');
            $client->image = $path;
        }
        $client->save();

        return redirect()->back()->with("success", "تم تعديل بيانات العميل !");
    }

    public function destroy($c, $id){
        if(!permissions_general("delete_client"))
            return back();
        $client = User::find($id);
        $client->delete();

        return redirect()->back()->with("success", "تم حذف العميل !");
    }
}
