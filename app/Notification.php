<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function notif(){
        return $this->belongsTo("App\Notif", "notif_id");
    }
    public function user(){
        return $this->belongsTo("App\User", "user_id");
    }
}
