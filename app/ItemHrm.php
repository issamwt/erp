<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemHrm extends Model
{
    public function designation(){
        return $this->belongsTo("App\Designation", "designation_id");
    }
}
