<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function users(){
        return $this->belongsToMany("App\User", "project_user", "project_id", "user_id")->withPivot('type');
    }

    public function type(){
        return $this->belongsTo("App\CategoryProject", "category_project_id");
    }

    public function city(){
        return $this->belongsTo("App\City");
    }

    public function client(){
        return $this->belongsTo("App\User", "client_id");
    }

    public function manager(){
        return $this->belongsTo("App\User","user_id");
    }

    public function milestones(){
        return $this->hasMany("App\Milestone","daftar_id");
    }

    public function tasks(){
        return $this->hasMany("App\Task","daftar_id")->where("daftar", "prm");
    }

    public function members(){
        return $this->belongsToMany("App\User", "project_user", "project_id", "user_id")->where('type', 0);
    }
}
