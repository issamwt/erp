<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function user(){
        return $this->belongsTo("App\User");
    }

    public function children(){
        return $this->hasMany("App\Comment", "comment_id");
    }

    public function attachments(){
        return $this->morphMany(Attachment::class, 'attachmentable');
    }
}
