<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacation extends Model
{
    public function category(){
        return $this->belongsTo("App\VacationCategory", "vacation_category_id");
    }

    public function user(){
        return $this->belongsTo("App\User", "user_id");
    }
}
