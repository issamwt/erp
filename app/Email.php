<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    public function from(){
        return $this->belongsTo("App\User", "from_id");
    }

    public function to(){
        return $this->belongsTo("App\User", "to_id");
    }
}
