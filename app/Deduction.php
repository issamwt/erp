<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    public function category(){
        return $this->belongsTo("App\DeductionCategory", "deduction_category_id");
    }
}
