<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use App\Company;

class User extends Authenticatable
{
    use Notifiable;

    public function company(){
        return $this->belongsTo("App\Company", "company_id");
    }

    public function role(){
        return $this->belongsTo("App\Role", "role_id");
    }

    public function sendPasswordResetNotification($token)
    {
        $c = Company::find($this->company_id);
        $this->notify(new CustomPassword($token));
    }

    public function details(){
        return $this->hasOne("App\UserDetails", "user_id");
    }

    public function allowances(){
        return $this->hasMany("App\Allowance", "user_id");
    }

    public function deductions(){
        return $this->hasMany("App\Deduction", "user_id");
    }

    public function rewards(){
        return $this->hasMany("App\Reward", "user_id");
    }

    public function substractions(){
        return $this->hasMany("App\Subtruction", "user_id");
    }

    public function vacations(){
        return $this->hasMany("App\Vacation", "user_id");
    }

    public function avacations(){
        return $this->hasMany("App\Vacation", "user_id")->where("status", 1);
    }

}

class CustomPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('إستعادة كلمة المرور')
            ->line('نحن نرسل هذه الرسالة الإلكترونية لأننا تلقينا طلبًا بنسيان كلمة المرور.')
                ->action('إعادة ضبط كلمة المرور', url(route('company.password.reset', $this->token)))
            ->line('إذا لم تطلب إعادة تعيين كلمة المرور ، فلا يلزم اتخاذ أي إجراء آخر. يرجى الاتصال بنا إذا لم ترسل هذا الطلب.');
    }
}
