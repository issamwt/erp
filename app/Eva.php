<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eva extends Model
{
    public function evaluations(){
        return $this->hasMany("App\Evaluation", "eva_id")->orderby("id", "desc");
    }
    public function user(){
        return $this->belongsTo("App\User", "user_id");
    }
}
