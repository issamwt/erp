<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    public function users(){
        return $this->belongsToMany("App\SiteUser", "site_user_website", "website_id", "site_user_id");
    }
}
