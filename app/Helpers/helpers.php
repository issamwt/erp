<?php

use App\Mail\TestMail;
use Illuminate\Support\Facades\Auth as Auth;
use App\Archive;
use App\User;
use App\Notif;
use App\Notification;

function init()
{
    $user = Auth::guard("user")->user();
    $id = $user->company_id;

    $settings                   = DB::table('settings')->get()->keyBy("key");
    $email_sent_from_address    = @$settings["email_sent_from_address".$id]->value;
    $email_sent_from_name       = @$settings["email_sent_from_name".$id]->value;
    $email_smtp_host            = @$settings["email_smtp_host".$id]->value;
    $email_smtp_user            = @$settings["email_smtp_user".$id]->value;
    $email_smtp_password        = @$settings["email_smtp_password".$id]->value;
    $email_smtp_port            = @$settings["email_smtp_port".$id]->value;


    $yamamah_sender             = @$settings["yamamah_sender".$id]->value;
    $yamamah_username           = @$settings["yamamah_username".$id]->value;
    $yamamah_password           = @$settings["yamamah_password".$id]->value;

    $pusher_auth_key = @$settings["pusher_auth_key".$id]->value;
    $pusher_secret = @$settings["pusher_secret".$id]->value;
    $pusher_app_id = @$settings["pusher_app_id".$id]->value;


    $zoom_api_key = @$settings["zoom_api_key".$id]->value;
    $zoom_api_secret = @$settings["zoom_api_secret".$id]->value;

    Config::set("settings", $settings);
    if (\Schema::hasTable('settings')) {
        /*if($email_smtp_host and $email_smtp_host=="smtp.sendgrid.net")
            Config::set('mail.driver', "sendgrid");
        else
            Config::set('mail.driver', "sendmail");
        */
        Config::set('app.name', $email_sent_from_name);
        Config::set('mail.from.name', '"'.$email_sent_from_name.'"');
        Config::set('mail.from.address', $email_sent_from_address);

        if($email_smtp_host and $email_smtp_user and $email_smtp_password and $email_smtp_port){
            Config::set('mail.host', $email_smtp_host);
            Config::set('mail.port', $email_smtp_port);
            Config::set('mail.username', $email_smtp_user);
            Config::set('mail.password', $email_smtp_password);
        }

        if($pusher_auth_key and $pusher_secret and $pusher_app_id){
            Config::set('pusher.connections.main.auth_key', $pusher_auth_key);
            Config::set('pusher.connections.main.secret', $pusher_secret);
            Config::set('pusher.connections.main.app_id', $pusher_app_id);
        }

        if($yamamah_sender and $yamamah_username and $yamamah_password){
            Config::set('yamamah.sender', $yamamah_sender);
            Config::set('yamamah.username', $yamamah_username);
            Config::set('yamamah.password', $yamamah_password);
        }

        if($zoom_api_key and $zoom_api_secret){
            Config::set('zoom.api_key', $zoom_api_key);
            Config::set('zoom.api_secret', $zoom_api_secret);
        }

        //Config::set('nexmo.api_key', $nexmo_api_key);
        //Config::set('nexmo.api_secret', $nexmo_api_secret);
    }

}

if (! function_exists('stars')) {
    function stars($n)
    {
        ?>
        <ul class="stars">
            <?php for($i=1;$i<=5;$i++): ?>
                <li><i class="fa <?=($i<=$n)?"fa-star active":"fa-star-o";?>  fa-2x "></i></li>
            <?php endfor; ?>
        </ul>
        <?php
    }
}

if (! function_exists('datef')) {
    function datef($date){
        if (!$date)
            return null;

        return date("Y-m-d", strtotime($date));
    }
}

if (! function_exists('timef')) {
    function timef($date){
        if (!$date)
            return null;

        return date("H:i:s", strtotime($date));
    }
}

if (! function_exists('get_current_utc_time')) {
    function get_current_utc_time($timezone="UTC", $format = "Y-m-d H:i:s") {
        $d = Carbon\Carbon::now($timezone);
        return $d->format($format);
    }
}

if (! function_exists('sum_evaluations')) {
    function sum_evaluations($evaluations)
    {
        $sum = [];
        $count = count($evaluations);
        foreach ($evaluations as $evaluation)
            array_push($sum, $evaluation->num);
        $res = ($count==0)?0:intval(array_sum($sum)/$count);
        return $res;
    }
}

if (! function_exists('mailit')) {
    function mailit($to, $subject, $body, $company_id=0, $daftar="settings")
    {
        try{
            init();
            Mail::to($to)->sendNow(new TestMail($subject, $body));
            $archive                = new Archive;
            $archive->company_id    = $company_id;
            $archive->daftar        = $daftar;
            $archive->user          = $to;
            $archive->type          = 0;
            $archive->message       = $body;
            $archive->save();
            /*
            \Mail::send([], [], function ($message) use ($to, $subject, $body) {
                $message->to($to)
                    ->subject($subject)
                    ->setBody($body, 'text/html');
            });
            */
        }catch (\Exception $e){
            //dd($e->getMessage());
            //dd(config("mail"));
        }
    }
}

if (! function_exists('notification')) {
    function notification($user_id, $notif_name, $company_id=0, $daftar="settings", $meeting_id=null, $project_id=null)
    {

        try{
            $user = User::find($user_id);
            if($user){
                $email = $user->email;
                $phone = $user->phone;
                $notif = Notif::where("name", $notif_name)->first();
                if($notif){
                    if($notif->app==1){
                        $notification               = new Notification;
                        $notification->user_id      = $user_id;
                        $notification->notif_id     = $notif->id;
                        $notification->daftar       = $daftar;
                        $notification->meeting_id   = $meeting_id;
                        $notification->project_id   = $project_id;
                        $notification->save();
                    }
                    if($notif->email==1){
                        mailit($email, $notif->title, $notif->message, $company_id, $daftar);
                    }
                    if($notif->sms==1){
                        sms_yamamah($phone, $notif->message, $company_id, $daftar);
                    }
                }
            }

        }catch (\Exception $e){
            //dd($e->getMessage());
            //dd(config("mail"));
        }
    }
}

if (! function_exists('sms')) {
    function sms($to, $text){
        try{
            init();
            $to = str_replace("+", "00", $to);
            $x = \Nexmo::message()->send([
                'to'   => $to,
                'from' => "0021699961102",
                'text' => $text,
                'type' => "unicode"
            ]);//dd($x);
        }
        catch (\Exception $e){
            //dd($e->getMessage());
        }
    }
}

if (! function_exists('sms_yamamah')) {
    function sms_yamamah($receiver, $message, $company_id=0, $daftar="settings"){
        try{
            init();
            $receiver = str_replace(" ", "", $receiver);
            $receiver = str_replace("-‎", "", $receiver);
            $receiver = str_replace("(‎", "", $receiver);
            $receiver = str_replace(")‎", "", $receiver);
            $receiver = convert($receiver);

            if (0 === strpos($receiver, '00') or 0 === strpos($receiver, '+'))
                $receiver = $receiver;
            else
                $receiver = "00966" . substr($receiver, -9);

            $login = config("yamamah.username");
            $pass = config("yamamah.password");
            $sender = urlencode(config("yamamah.sender"));
            $data = array(
                'Username' => $login,
                'Password' => $pass,
                'Tagname' => $sender,
                'RecepientNumber' => str_replace('+', '00', $receiver),
                'Message' => $message,
                'SendDateTime' => 0,
                'EnableDR' => false
            );
            $data_string = json_encode($data, JSON_UNESCAPED_UNICODE);
            $ch = curl_init('http://api.yamamah.com/SendSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);

            $archive                = new Archive;
            $archive->company_id    = $company_id;
            $archive->daftar        = $daftar;
            $archive->user           = $receiver;
            $archive->type          = 1;
            $archive->message       = $message;
            $archive->status        = ($response->Status==1)?1:0;
            $archive->error         = ($response->Status==1)?null:$response->StatusDescription;
            $archive->save();
            //dd($archive);
        }
        catch (\Exception $e){
            //dd($e->getMessage());
        }
    }
}

if (! function_exists('sms_yamamah')) {
    function sms_yamamah($receiver, $message, $company_id=0, $daftar="settings"){
        try{
            init();
            $receiver = str_replace(" ", "", $receiver);
            $receiver = str_replace("-‎", "", $receiver);
            $receiver = str_replace("(‎", "", $receiver);
            $receiver = str_replace(")‎", "", $receiver);
            $receiver = convert($receiver);

            if (0 === strpos($receiver, '00') or 0 === strpos($receiver, '+'))
                $receiver = $receiver;
            else
                $receiver = "00966" . substr($receiver, -9);

            $login = config("yamamah.username");
            $pass = config("yamamah.password");
            $sender = urlencode(config("yamamah.sender"));
            $data = array(
                'Username' => $login,
                'Password' => $pass,
                'Tagname' => $sender,
                'RecepientNumber' => str_replace('+', '00', $receiver),
                'Message' => $message,
                'SendDateTime' => 0,
                'EnableDR' => false
            );
            $data_string = json_encode($data, JSON_UNESCAPED_UNICODE);
            $ch = curl_init('http://api.yamamah.com/SendSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);

            $archive                = new Archive;
            $archive->company_id    = $company_id;
            $archive->daftar        = $daftar;
            $archive->user           = $receiver;
            $archive->type          = 1;
            $archive->message       = $message;
            $archive->status        = ($response->Status==1)?1:0;
            $archive->error         = ($response->Status==1)?null:$response->StatusDescription;
            $archive->save();
            //dd($archive);
        }
        catch (\Exception $e){
            //dd($e->getMessage());
        }
    }
}

if (! function_exists('convert')) {
    function convert($string) {
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];
        $num = range(9, 0);
        $englishNumbersOnly = str_replace($arabic, $num, $string);
        return $englishNumbersOnly;
    }
}

if (! function_exists('fcmit')) {
    function fcmit($even, $data){
        try{
            init();
            $x = Pusher::trigger('my-channel', $even, $data);
            //dd($x);
        }
        catch (\Exception $e){
            //dd($e->getMessage());
        }
    }
}

if (! function_exists('zoom')) {
    function zoom(){
        try{
            init();
            dd("zoom");
        }
        catch (\Exception $e){
            //dd($e->getMessage());
        }
    }
}

if (! function_exists('permissions_hrm')) {
    function permissions_hrm($permission)
    {
        if(Auth::guard("user")->user()->role_id==1)
            return true;
        if(Auth::guard("user")->user()->type==1)
            return false;
        $permissions = unserialize(Auth::guard("user")->user()->role->permissions_hrm);
        $permissions = is_array($permissions)?$permissions:[];
        return in_array($permission, $permissions);
    }
}

if (! function_exists('permissions_meetings')) {
    function permissions_meetings($permission)
    {
        if(Auth::guard("user")->user()->role_id==1)
            return true;
        if(Auth::guard("user")->user()->type==1)
            return false;
        $permissions = unserialize(Auth::guard("user")->user()->role->permissions_meetings);
        $permissions = is_array($permissions)?$permissions:[];
        return in_array($permission, $permissions);
    }
}

if (! function_exists('permissions_prm')) {
    function permissions_prm($permission)
    {
        if(Auth::guard("user")->user()->role_id==1)
            return true;
        if(Auth::guard("user")->user()->type==1)
            return false;
        $permissions = unserialize(Auth::guard("user")->user()->role->permissions_prm);
        $permissions = is_array($permissions)?$permissions:[];
        return in_array($permission, $permissions);
    }
}

if (! function_exists('permissions_passwords')) {
    function permissions_passwords($permission)
    {
        if(Auth::guard("user")->user()->role_id==1)
            return true;
        if(Auth::guard("user")->user()->type==1)
            return false;
        $permissions = unserialize(Auth::guard("user")->user()->role->permissions_passwords);
        $permissions = is_array($permissions)?$permissions:[];
        return in_array($permission, $permissions);
    }
}

if (! function_exists('permissions_general')) {
    function permissions_general($permission)
    {
        if(Auth::guard("user")->user()->role_id==1)
            return true;
        if(Auth::guard("user")->user()->type==1)
            return false;
        $permissions = unserialize(Auth::guard("user")->user()->role->permissions_general);
        $permissions = is_array($permissions)?$permissions:[];
        return in_array($permission, $permissions);
    }
}