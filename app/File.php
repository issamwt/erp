<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function user(){
        return $this->belongsTo("App\User", "user_id");
    }

    public function type(){
        return $this->belongsTo("App\FileType", "file_type_id");
    }
}
