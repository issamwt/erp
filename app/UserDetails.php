<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    public function designation(){
        return $this->belongsTo("App\Designation", "designation_id");
    }

    public function department(){
        return $this->belongsTo("App\Department", "department_id");
    }

    public function section(){
        return $this->belongsTo("App\Section", "section_id");
    }

    public function country(){
        return $this->belongsTo("App\Country", "nationality");
    }

    public function manager(){
        return $this->belongsTo("App\User", "direct_manager_id");
    }

    public function job_location(){
        return $this->belongsTo("App\JobLocation", "job_location_id");
    }

    public function medicall(){
        return $this->belongsTo("App\MedicalCategory", "medical");
    }

    public function sociall(){
        return $this->belongsTo("App\SocialCategory", "social");
    }
}
