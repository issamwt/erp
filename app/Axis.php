<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Axis extends Model
{
    public function speaker(){
        return $this->belongsTo("App\User", "speaker_id");
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

}
