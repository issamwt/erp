<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allowance extends Model
{
    public function category(){
        return $this->belongsTo("App\AllowanceCategory", "allowance_category_id");
    }
}
