<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function user(){
        $this->belongsTo("App\User", "user_id");
    }
}
