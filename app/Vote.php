<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public function votes(){
        return $this->hasMany("App\VoteUser", "vote_id");
    }
}
