<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    public function location(){
        return $this->belongsTo("App\Location");
    }

    public function category(){
        return $this->belongsTo("App\CategoryMeeting");
    }

    public function manager(){
        return $this->belongsTo("App\User", "manager_id");
    }

    public function amin(){
        return $this->belongsTo("App\User", "amin_id");
    }

    public function users(){
        return $this->belongsToMany("App\User", "meeting_user");
    }

    public function clients(){
        return $this->belongsToMany("App\User", "meeting_client");
    }

    public function attachments(){
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function votes(){
        return $this->hasMany("App\Vote");
    }

    public function tasks(){
        return $this->hasMany("App\Task");
    }

    public function newtasks(){
        return $this->hasMany("App\Task", "daftar_id")->where("tasks.status", 0);
    }

    public function finishedtasks(){
        return $this->hasMany("App\Task", "daftar_id")->where("tasks.status", 1);
    }
    
}
