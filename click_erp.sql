-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 04 jan. 2019 à 15:02
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `click_erp`
--

-- --------------------------------------------------------

--
-- Structure de la table `accountings`
--

CREATE TABLE `accountings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_to` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `message1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message2` text COLLATE utf8mb4_unicode_ci,
  `message3` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `accountings`
--

INSERT INTO `accountings` (`id`, `user_to`, `user_from`, `message1`, `message2`, `message3`, `status`, `created_at`, `updated_at`) VALUES
(1, 30, 24, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 'erytertert', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على', 2, '2018-12-25 19:35:20', '2018-12-25 19:52:17');

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_sent_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `admins`
--

INSERT INTO `admins` (`id`, `name`, `phone`, `email`, `email_verified_at`, `password`, `remember_token`, `status`, `activation_code`, `code_sent_at`, `created_at`, `updated_at`) VALUES
(5, 'عصام حامد', '0021699961102', 'admin@admin.com', NULL, '$2y$10$mER/V0Mjz5zsMujK0xQmFOFGkQ9BezNtM1.lJGji3686LmZ.7ulEW', 'oZ5BtExi5WXxORUQGhVG5JVD36a0SzSQOVMSJhXi6KAIXbWcZ02fVLhBFhO4', 1, '865713', '2019-01-03 16:48:40', '2018-10-25 16:54:06', '2019-01-03 21:49:01'),
(2, 'أحمد علي', '0021699961103', 'ahmedali@gmail.com', NULL, '$2y$10$dH1GfvwfO8v.d3TtAyCxgeRN.5WzpkHGdxuzRlMf1RaMqeA2JgNCa', NULL, 0, NULL, NULL, '2018-10-25 16:54:06', '2018-10-25 16:54:06'),
(7, 'إبراهيم', '00966502480256', 'ibrahim@gmail.com', NULL, '$2y$10$y2KsztWQaONKS7uonAP8TOl8h80uFU1pQCJTLp61knJkSGAlQ5AJ6', 'pmtgvYOW8lBzd5rWJCjgCeG8JqRMCyQo730b1yrmFQ7mBPp2w9bMqmld5tUw', 1, '173855', '2019-01-04 16:02:23', '2018-11-23 12:12:42', '2019-01-04 21:03:11'),
(8, 'رياض', '00966563421359', 'sa@ccreation.sa', NULL, '$2y$10$swfEa6Y2FPCxSjY.D230z.3A73yBBV5QWYcliQMC3IK3tRjQwUdZm', NULL, 0, '780216', '2018-12-27 00:11:05', '2018-12-24 16:54:17', '2018-12-27 05:11:05');

-- --------------------------------------------------------

--
-- Structure de la table `advances`
--

CREATE TABLE `advances` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `monthly` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `months` int(11) NOT NULL DEFAULT '0',
  `thedate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kafil_id` int(11) DEFAULT NULL,
  `last_date` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `advances`
--

INSERT INTO `advances` (`id`, `company_id`, `user_id`, `amount`, `rest`, `type`, `monthly`, `months`, `thedate`, `kafil_id`, `last_date`, `created_at`, `updated_at`) VALUES
(1, 12, 24, '120', '120', 1, '10.00', 12, '2018-12-03', 32, NULL, '2018-12-19 17:52:01', '2018-12-19 17:52:01'),
(2, 12, 24, '200', '100', 2, '10', 10, '2018-12-17', NULL, NULL, '2018-12-19 18:09:46', '2018-12-19 18:41:25');

-- --------------------------------------------------------

--
-- Structure de la table `allowances`
--

CREATE TABLE `allowances` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `allowance_category_id` int(11) NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `allowances`
--

INSERT INTO `allowances` (`id`, `company_id`, `user_id`, `allowance_category_id`, `from_date`, `to_date`, `created_at`, `updated_at`) VALUES
(1, 12, 24, 2, '2018-10-01', '2018-12-03', '2018-12-19 13:35:50', '2018-12-19 13:35:50'),
(2, 12, 24, 1, '2018-12-03', NULL, '2018-12-19 13:37:23', '2018-12-19 14:39:59');

-- --------------------------------------------------------

--
-- Structure de la table `allowance_categories`
--

CREATE TABLE `allowance_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `allowance_categories`
--

INSERT INTO `allowance_categories` (`id`, `company_id`, `name`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 12, 'بدل سكن', 0, '25', '2018-12-14 12:21:02', '2018-12-14 12:21:02'),
(2, 12, 'بدل مواصلات', 1, '500', '2018-12-14 12:21:22', '2018-12-14 12:21:22');

-- --------------------------------------------------------

--
-- Structure de la table `archives`
--

CREATE TABLE `archives` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `daftar` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `error` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `archives`
--

INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(1, 1, 'settings', 'test@test.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-11-22 10:43:53', '2018-11-22 10:43:53'),
(2, 1, 'meetings', 'employee1@employee.com', 0, 'الإجتماع : أجتماع تجريبي 2<br>هذه رسالة لتذكيرك بالإجتماع.', 0, NULL, '2018-11-22 10:45:13', '2018-11-22 10:45:13'),
(3, 1, 'meetings', 'employee2@employee.com', 0, 'الإجتماع : أجتماع تجريبي 2<br>هذه رسالة لتذكيرك بالإجتماع.', 0, NULL, '2018-11-22 10:45:14', '2018-11-22 10:45:14'),
(4, 1, 'meetings', 'client2@client.com', 0, 'الإجتماع : أجتماع تجريبي 2<br>هذه رسالة لتذكيرك بالإجتماع.', 0, NULL, '2018-11-22 10:45:14', '2018-11-22 10:45:14'),
(5, 1, 'meetings', '002169996110s2', 1, 'drgdfg', 0, 'Invalid Mobile Number', '2018-11-22 10:52:52', '2018-11-22 10:52:52'),
(6, 1, 'meetings', '0021699961102', 1, 'drgdfg', 1, NULL, '2018-11-22 10:53:11', '2018-11-22 10:53:11'),
(7, 1, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2018-11-22 10:56:23', '2018-11-22 10:56:23'),
(8, 1, 'meetings', '00966', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة لتذكيرك بالإجتماع.', 0, 'Invalid Mobile Number', '2018-11-22 10:56:37', '2018-11-22 10:56:37'),
(9, 1, 'meetings', 'issam.web.technologie2@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 2<br>هذه رسالة لتذكيرك بالإجتماع.<br><a href=\'https://ccreation.sa/erp/meeting/9/35\'>https://ccreation.sa/erp/meeting/9/35</a>', 0, NULL, '2018-11-22 12:32:04', '2018-11-22 12:32:04'),
(10, 1, 'meetings', '0021699961101', 1, 'الإجتماع : أجتماع تجريبي 2\nهذه رسالة لتذكيرك بالإجتماع.\n\nhttps://ccreation.sa/erp/meeting/9/35', 1, NULL, '2018-11-22 12:32:04', '2018-11-22 12:32:04'),
(11, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 2<br>هذه رسالة لتذكيرك بالإجتماع.<br><a href=\'https://ccreation.sa/erp/meeting/9/33\'>https://ccreation.sa/erp/meeting/9/33</a>', 0, NULL, '2018-11-22 12:36:31', '2018-11-22 12:36:31'),
(12, 1, 'meetings', '0021699961102', 1, 'الإجتماع : أجتماع تجريبي 2\nهذه رسالة لتذكيرك بالإجتماع.\n\nhttps://ccreation.sa/erp/meeting/9/33', 1, NULL, '2018-11-22 12:36:32', '2018-11-22 12:36:32'),
(13, 1, 'meetings', 'issam.web.technologie2@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/10/35<br><a href=\'https://ccreation.sa/erp/meeting/10/35\'>https://ccreation.sa/erp/meeting/10/35</a>', 0, NULL, '2018-11-22 13:58:05', '2018-11-22 13:58:05'),
(14, 1, 'meetings', '0021699961101', 1, 'الإجتماع : أجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/10/35\n\nhttps://ccreation.sa/erp/meeting/10/35', 1, NULL, '2018-11-22 13:58:05', '2018-11-22 13:58:05'),
(15, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/10/33<br><a href=\'https://ccreation.sa/erp/meeting/10/33\'>https://ccreation.sa/erp/meeting/10/33</a>', 0, NULL, '2018-11-22 13:58:05', '2018-11-22 13:58:05'),
(16, 1, 'meetings', '0021699961102', 1, 'الإجتماع : أجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/10/33\n\nhttps://ccreation.sa/erp/meeting/10/33', 1, NULL, '2018-11-22 13:58:06', '2018-11-22 13:58:06'),
(17, 1, 'meetings', 'issam.web.technologie2@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/11/35<br><a href=\'https://ccreation.sa/erp/meeting/11/35\'>https://ccreation.sa/erp/meeting/11/35</a>', 0, NULL, '2018-11-22 14:00:05', '2018-11-22 14:00:05'),
(18, 1, 'meetings', '0021699961101', 1, 'الإجتماع : أجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/11/35\n\nhttps://ccreation.sa/erp/meeting/11/35', 1, NULL, '2018-11-22 14:00:05', '2018-11-22 14:00:05'),
(19, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/11/33<br><a href=\'https://ccreation.sa/erp/meeting/11/33\'>https://ccreation.sa/erp/meeting/11/33</a>', 0, NULL, '2018-11-22 14:00:05', '2018-11-22 14:00:05'),
(20, 1, 'meetings', '0021699961102', 1, 'الإجتماع : أجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/11/33\n\nhttps://ccreation.sa/erp/meeting/11/33', 1, NULL, '2018-11-22 14:00:06', '2018-11-22 14:00:06'),
(21, 1, 'meetings', 'issam.web.technologie2@gmail.com', 0, 'الإجتماع : إجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/14/35<br><a href=\'https://ccreation.sa/erp/meeting/14/35\'>https://ccreation.sa/erp/meeting/14/35</a>', 0, NULL, '2018-11-22 14:04:03', '2018-11-22 14:04:03'),
(22, 1, 'meetings', '0021699961101', 1, 'الإجتماع : إجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/14/35\n\nhttps://ccreation.sa/erp/meeting/14/35', 1, NULL, '2018-11-22 14:04:03', '2018-11-22 14:04:03'),
(23, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : إجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/14/33<br><a href=\'https://ccreation.sa/erp/meeting/14/33\'>https://ccreation.sa/erp/meeting/14/33</a>', 0, NULL, '2018-11-22 14:04:03', '2018-11-22 14:04:03'),
(24, 1, 'meetings', '0021699961102', 1, 'الإجتماع : إجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/14/33\n\nhttps://ccreation.sa/erp/meeting/14/33', 1, NULL, '2018-11-22 14:04:03', '2018-11-22 14:04:03'),
(25, 1, 'meetings', 'employee2@employee.com', 0, 'الإجتماع : إجتماع تجريبي 3<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/14/34<br><a href=\'https://ccreation.sa/erp/meeting/14/34\'>https://ccreation.sa/erp/meeting/14/34</a>', 0, NULL, '2018-11-22 14:04:04', '2018-11-22 14:04:04'),
(26, 1, 'meetings', '00966123456985', 1, 'الإجتماع : إجتماع تجريبي 3\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/14/34\n\nhttps://ccreation.sa/erp/meeting/14/34', 1, NULL, '2018-11-22 14:04:04', '2018-11-22 14:04:04'),
(27, 1, 'meetings', 'issam.web.technologie2@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 1<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/8/35\'>https://ccreation.sa/erp/meeting/8/35</a>', 0, NULL, '2018-11-22 16:42:03', '2018-11-22 16:42:03'),
(28, 1, 'meetings', '0021699961101', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/8/35', 1, NULL, '2018-11-22 16:42:03', '2018-11-22 16:42:03'),
(29, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 1<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/8/33\'>https://ccreation.sa/erp/meeting/8/33</a>', 0, NULL, '2018-11-22 16:42:04', '2018-11-22 16:42:04'),
(30, 1, 'meetings', '0021699961102', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/8/33', 1, NULL, '2018-11-22 16:42:04', '2018-11-22 16:42:04'),
(31, 1, 'meetings', 'employee2@employee.com', 0, 'الإجتماع : أجتماع تجريبي 1<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/8/34\'>https://ccreation.sa/erp/meeting/8/34</a>', 0, NULL, '2018-11-22 16:42:04', '2018-11-22 16:42:04'),
(32, 1, 'meetings', '00966123456985', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/8/34', 1, NULL, '2018-11-22 16:42:04', '2018-11-22 16:42:04'),
(33, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : أجتماع تجريبي 1<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/8/33\'>https://ccreation.sa/erp/meeting/8/33</a>', 0, NULL, '2018-11-22 16:42:05', '2018-11-22 16:42:05'),
(34, 1, 'meetings', '0021699961102', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/8/33', 1, NULL, '2018-11-22 16:42:05', '2018-11-22 16:42:05'),
(35, 1, 'meetings', 'client1@client.com', 0, 'الإجتماع : أجتماع تجريبي 1<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/8/4\'>https://ccreation.sa/erp/meeting/8/4</a>', 0, NULL, '2018-11-22 16:42:05', '2018-11-22 16:42:05'),
(36, 1, 'meetings', '00966', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/8/4', 0, 'Invalid Mobile Number', '2018-11-22 16:42:05', '2018-11-22 16:42:05'),
(37, 1, 'meetings', 'client2@client.com', 0, 'الإجتماع : أجتماع تجريبي 1<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/8/5\'>https://ccreation.sa/erp/meeting/8/5</a>', 0, NULL, '2018-11-22 16:42:06', '2018-11-22 16:42:06'),
(38, 1, 'meetings', '00966', 1, 'الإجتماع : أجتماع تجريبي 1\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/8/5', 0, 'Invalid Mobile Number', '2018-11-22 16:42:06', '2018-11-22 16:42:06'),
(39, 1, 'meetings', 'issam.web.technologie2@gmail.com', 0, 'الإجتماع : إجتماع تجريبي 4<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/35<br><a href=\'https://ccreation.sa/erp/meeting/18/35\'>https://ccreation.sa/erp/meeting/18/35</a>', 0, NULL, '2018-11-24 13:35:10', '2018-11-24 13:35:10'),
(40, 1, 'meetings', '0021699961101', 1, 'الإجتماع : إجتماع تجريبي 4\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/35\n\nhttps://ccreation.sa/erp/meeting/18/35', 1, NULL, '2018-11-24 13:35:10', '2018-11-24 13:35:10'),
(41, 1, 'meetings', 'isssam.web.technologie@gmail.com', 0, 'الإجتماع : إجتماع تجريبي 4<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/7<br><a href=\'https://ccreation.sa/erp/meeting/18/7\'>https://ccreation.sa/erp/meeting/18/7</a>', 0, NULL, '2018-11-24 13:35:10', '2018-11-24 13:35:10'),
(42, 1, 'meetings', '00966126589712', 1, 'الإجتماع : إجتماع تجريبي 4\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/7\n\nhttps://ccreation.sa/erp/meeting/18/7', 1, NULL, '2018-11-24 13:35:10', '2018-11-24 13:35:10'),
(43, 1, 'meetings', 'devweb218@gmail.com', 0, 'الإجتماع : إجتماع تجريبي 4<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/33<br><a href=\'https://ccreation.sa/erp/meeting/18/33\'>https://ccreation.sa/erp/meeting/18/33</a>', 0, NULL, '2018-11-24 13:35:10', '2018-11-24 13:35:10'),
(44, 1, 'meetings', '0021699961102', 1, 'الإجتماع : إجتماع تجريبي 4\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/33\n\nhttps://ccreation.sa/erp/meeting/18/33', 1, NULL, '2018-11-24 13:35:11', '2018-11-24 13:35:11'),
(45, 1, 'meetings', 'employee2@employee.com', 0, 'الإجتماع : إجتماع تجريبي 4<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/34<br><a href=\'https://ccreation.sa/erp/meeting/18/34\'>https://ccreation.sa/erp/meeting/18/34</a>', 0, NULL, '2018-11-24 13:35:11', '2018-11-24 13:35:11'),
(46, 1, 'meetings', '00966123456985', 1, 'الإجتماع : إجتماع تجريبي 4\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/18/34\n\nhttps://ccreation.sa/erp/meeting/18/34', 1, NULL, '2018-11-24 13:35:11', '2018-11-24 13:35:11'),
(47, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : dfgdfg<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/25/23<br><a href=\'https://ccreation.sa/erp/meeting/25/23\'>https://ccreation.sa/erp/meeting/25/23</a>', 0, NULL, '2018-11-26 18:47:29', '2018-11-26 18:47:29'),
(48, 12, 'meetings', '00966558037380', 1, 'الإجتماع : dfgdfg\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/25/23\n\nhttps://ccreation.sa/erp/meeting/25/23', 1, NULL, '2018-11-26 18:47:30', '2018-11-26 18:47:30'),
(49, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : dfgdfg<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/25/23<br><a href=\'https://ccreation.sa/erp/meeting/25/23\'>https://ccreation.sa/erp/meeting/25/23</a>', 0, NULL, '2018-11-26 18:47:30', '2018-11-26 18:47:30'),
(50, 12, 'meetings', '00966558037380', 1, 'الإجتماع : dfgdfg\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/25/23\n\nhttps://ccreation.sa/erp/meeting/25/23', 1, NULL, '2018-11-26 18:47:30', '2018-11-26 18:47:30'),
(51, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/32<br><a href=\'https://ccreation.sa/erp/meeting/26/32\'>https://ccreation.sa/erp/meeting/26/32</a>', 0, NULL, '2018-11-27 16:10:57', '2018-11-27 16:10:57'),
(52, 12, 'meetings', '00966509193274', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/32\n\nhttps://ccreation.sa/erp/meeting/26/32', 1, NULL, '2018-11-27 16:10:58', '2018-11-27 16:10:58'),
(53, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/20<br><a href=\'https://ccreation.sa/erp/meeting/26/20\'>https://ccreation.sa/erp/meeting/26/20</a>', 0, NULL, '2018-11-27 16:10:58', '2018-11-27 16:10:58'),
(54, 12, 'meetings', '00966563421359', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/20\n\nhttps://ccreation.sa/erp/meeting/26/20', 1, NULL, '2018-11-27 16:10:58', '2018-11-27 16:10:58'),
(55, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/30<br><a href=\'https://ccreation.sa/erp/meeting/26/30\'>https://ccreation.sa/erp/meeting/26/30</a>', 0, NULL, '2018-11-27 16:10:58', '2018-11-27 16:10:58'),
(56, 12, 'meetings', '00966535779245', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/30\n\nhttps://ccreation.sa/erp/meeting/26/30', 1, NULL, '2018-11-27 16:10:59', '2018-11-27 16:10:59'),
(57, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/31<br><a href=\'https://ccreation.sa/erp/meeting/26/31\'>https://ccreation.sa/erp/meeting/26/31</a>', 0, NULL, '2018-11-27 16:10:59', '2018-11-27 16:10:59'),
(58, 12, 'meetings', '00966541009675', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/31\n\nhttps://ccreation.sa/erp/meeting/26/31', 1, NULL, '2018-11-27 16:10:59', '2018-11-27 16:10:59'),
(59, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/27<br><a href=\'https://ccreation.sa/erp/meeting/26/27\'>https://ccreation.sa/erp/meeting/26/27</a>', 0, NULL, '2018-11-27 16:10:59', '2018-11-27 16:10:59'),
(60, 12, 'meetings', '00966582983966', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/27\n\nhttps://ccreation.sa/erp/meeting/26/27', 1, NULL, '2018-11-27 16:11:00', '2018-11-27 16:11:00'),
(61, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/29<br><a href=\'https://ccreation.sa/erp/meeting/26/29\'>https://ccreation.sa/erp/meeting/26/29</a>', 0, NULL, '2018-11-27 16:11:00', '2018-11-27 16:11:00'),
(62, 12, 'meetings', '00966507771868', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/29\n\nhttps://ccreation.sa/erp/meeting/26/29', 1, NULL, '2018-11-27 16:11:00', '2018-11-27 16:11:00'),
(63, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/28<br><a href=\'https://ccreation.sa/erp/meeting/26/28\'>https://ccreation.sa/erp/meeting/26/28</a>', 0, NULL, '2018-11-27 16:11:00', '2018-11-27 16:11:00'),
(64, 12, 'meetings', '00966507187011', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/28\n\nhttps://ccreation.sa/erp/meeting/26/28', 1, NULL, '2018-11-27 16:11:01', '2018-11-27 16:11:01'),
(65, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/20<br><a href=\'https://ccreation.sa/erp/meeting/26/20\'>https://ccreation.sa/erp/meeting/26/20</a>', 0, NULL, '2018-11-27 16:11:01', '2018-11-27 16:11:01'),
(66, 12, 'meetings', '00966563421359', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/20\n\nhttps://ccreation.sa/erp/meeting/26/20', 1, NULL, '2018-11-27 16:11:01', '2018-11-27 16:11:01'),
(67, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : مناقشة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/23<br><a href=\'https://ccreation.sa/erp/meeting/26/23\'>https://ccreation.sa/erp/meeting/26/23</a>', 0, NULL, '2018-11-27 16:11:02', '2018-11-27 16:11:02'),
(68, 12, 'meetings', '00966558037380', 1, 'الإجتماع : مناقشة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/26/23\n\nhttps://ccreation.sa/erp/meeting/26/23', 1, NULL, '2018-11-27 16:11:02', '2018-11-27 16:11:02'),
(69, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/32<br><a href=\'https://ccreation.sa/erp/meeting/27/32\'>https://ccreation.sa/erp/meeting/27/32</a>', 0, NULL, '2018-11-27 16:20:25', '2018-11-27 16:20:25'),
(70, 12, 'meetings', '00966509193274', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/32\n\nhttps://ccreation.sa/erp/meeting/27/32', 1, NULL, '2018-11-27 16:20:26', '2018-11-27 16:20:26'),
(71, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/20<br><a href=\'https://ccreation.sa/erp/meeting/27/20\'>https://ccreation.sa/erp/meeting/27/20</a>', 0, NULL, '2018-11-27 16:20:26', '2018-11-27 16:20:26'),
(72, 12, 'meetings', '00966563421359', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/20\n\nhttps://ccreation.sa/erp/meeting/27/20', 1, NULL, '2018-11-27 16:20:26', '2018-11-27 16:20:26'),
(73, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/30<br><a href=\'https://ccreation.sa/erp/meeting/27/30\'>https://ccreation.sa/erp/meeting/27/30</a>', 0, NULL, '2018-11-27 16:20:26', '2018-11-27 16:20:26'),
(74, 12, 'meetings', '00966535779245', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/30\n\nhttps://ccreation.sa/erp/meeting/27/30', 1, NULL, '2018-11-27 16:20:27', '2018-11-27 16:20:27'),
(75, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/31<br><a href=\'https://ccreation.sa/erp/meeting/27/31\'>https://ccreation.sa/erp/meeting/27/31</a>', 0, NULL, '2018-11-27 16:20:27', '2018-11-27 16:20:27'),
(76, 12, 'meetings', '00966541009675', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/31\n\nhttps://ccreation.sa/erp/meeting/27/31', 1, NULL, '2018-11-27 16:20:27', '2018-11-27 16:20:27'),
(77, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/27<br><a href=\'https://ccreation.sa/erp/meeting/27/27\'>https://ccreation.sa/erp/meeting/27/27</a>', 0, NULL, '2018-11-27 16:20:27', '2018-11-27 16:20:27'),
(78, 12, 'meetings', '00966582983966', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/27\n\nhttps://ccreation.sa/erp/meeting/27/27', 1, NULL, '2018-11-27 16:20:27', '2018-11-27 16:20:27'),
(79, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/29<br><a href=\'https://ccreation.sa/erp/meeting/27/29\'>https://ccreation.sa/erp/meeting/27/29</a>', 0, NULL, '2018-11-27 16:20:28', '2018-11-27 16:20:28'),
(80, 12, 'meetings', '00966507771868', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/29\n\nhttps://ccreation.sa/erp/meeting/27/29', 1, NULL, '2018-11-27 16:20:28', '2018-11-27 16:20:28'),
(81, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/28<br><a href=\'https://ccreation.sa/erp/meeting/27/28\'>https://ccreation.sa/erp/meeting/27/28</a>', 0, NULL, '2018-11-27 16:20:28', '2018-11-27 16:20:28'),
(82, 12, 'meetings', '00966507187011', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/28\n\nhttps://ccreation.sa/erp/meeting/27/28', 1, NULL, '2018-11-27 16:20:29', '2018-11-27 16:20:29'),
(83, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/20<br><a href=\'https://ccreation.sa/erp/meeting/27/20\'>https://ccreation.sa/erp/meeting/27/20</a>', 0, NULL, '2018-11-27 16:20:29', '2018-11-27 16:20:29'),
(84, 12, 'meetings', '00966563421359', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/20\n\nhttps://ccreation.sa/erp/meeting/27/20', 1, NULL, '2018-11-27 16:20:29', '2018-11-27 16:20:29'),
(85, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/22<br><a href=\'https://ccreation.sa/erp/meeting/27/22\'>https://ccreation.sa/erp/meeting/27/22</a>', 0, NULL, '2018-11-27 16:20:30', '2018-11-27 16:20:30'),
(86, 12, 'meetings', '00966502480256', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/22\n\nhttps://ccreation.sa/erp/meeting/27/22', 1, NULL, '2018-11-27 16:20:30', '2018-11-27 16:20:30'),
(87, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : الاجتماع الرابع<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/23<br><a href=\'https://ccreation.sa/erp/meeting/27/23\'>https://ccreation.sa/erp/meeting/27/23</a>', 0, NULL, '2018-11-27 16:20:30', '2018-11-27 16:20:30'),
(88, 12, 'meetings', '00966558037380', 1, 'الإجتماع : الاجتماع الرابع\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/27/23\n\nhttps://ccreation.sa/erp/meeting/27/23', 1, NULL, '2018-11-27 16:20:30', '2018-11-27 16:20:30'),
(89, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/32<br><a href=\'https://ccreation.sa/erp/meeting/28/32\'>https://ccreation.sa/erp/meeting/28/32</a>', 0, NULL, '2018-11-27 16:22:32', '2018-11-27 16:22:32'),
(90, 12, 'meetings', '00966509193274', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/32\n\nhttps://ccreation.sa/erp/meeting/28/32', 1, NULL, '2018-11-27 16:22:32', '2018-11-27 16:22:32'),
(91, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/20<br><a href=\'https://ccreation.sa/erp/meeting/28/20\'>https://ccreation.sa/erp/meeting/28/20</a>', 0, NULL, '2018-11-27 16:22:32', '2018-11-27 16:22:32'),
(92, 12, 'meetings', '00966563421359', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/20\n\nhttps://ccreation.sa/erp/meeting/28/20', 1, NULL, '2018-11-27 16:22:33', '2018-11-27 16:22:33'),
(93, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/30<br><a href=\'https://ccreation.sa/erp/meeting/28/30\'>https://ccreation.sa/erp/meeting/28/30</a>', 0, NULL, '2018-11-27 16:22:33', '2018-11-27 16:22:33'),
(94, 12, 'meetings', '00966535779245', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/30\n\nhttps://ccreation.sa/erp/meeting/28/30', 1, NULL, '2018-11-27 16:22:33', '2018-11-27 16:22:33'),
(95, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/31<br><a href=\'https://ccreation.sa/erp/meeting/28/31\'>https://ccreation.sa/erp/meeting/28/31</a>', 0, NULL, '2018-11-27 16:22:33', '2018-11-27 16:22:33'),
(96, 12, 'meetings', '00966541009675', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/31\n\nhttps://ccreation.sa/erp/meeting/28/31', 1, NULL, '2018-11-27 16:22:33', '2018-11-27 16:22:33'),
(97, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/27<br><a href=\'https://ccreation.sa/erp/meeting/28/27\'>https://ccreation.sa/erp/meeting/28/27</a>', 0, NULL, '2018-11-27 16:22:34', '2018-11-27 16:22:34'),
(98, 12, 'meetings', '00966582983966', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/27\n\nhttps://ccreation.sa/erp/meeting/28/27', 1, NULL, '2018-11-27 16:22:34', '2018-11-27 16:22:34'),
(99, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/29<br><a href=\'https://ccreation.sa/erp/meeting/28/29\'>https://ccreation.sa/erp/meeting/28/29</a>', 0, NULL, '2018-11-27 16:22:34', '2018-11-27 16:22:34'),
(100, 12, 'meetings', '00966507771868', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/29\n\nhttps://ccreation.sa/erp/meeting/28/29', 1, NULL, '2018-11-27 16:22:34', '2018-11-27 16:22:34'),
(101, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/28<br><a href=\'https://ccreation.sa/erp/meeting/28/28\'>https://ccreation.sa/erp/meeting/28/28</a>', 0, NULL, '2018-11-27 16:22:35', '2018-11-27 16:22:35'),
(102, 12, 'meetings', '00966507187011', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/28\n\nhttps://ccreation.sa/erp/meeting/28/28', 1, NULL, '2018-11-27 16:22:35', '2018-11-27 16:22:35'),
(103, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : الخامس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/23<br><a href=\'https://ccreation.sa/erp/meeting/28/23\'>https://ccreation.sa/erp/meeting/28/23</a>', 0, NULL, '2018-11-27 16:22:35', '2018-11-27 16:22:35'),
(104, 12, 'meetings', '00966558037380', 1, 'الإجتماع : الخامس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/28/23\n\nhttps://ccreation.sa/erp/meeting/28/23', 1, NULL, '2018-11-27 16:22:36', '2018-11-27 16:22:36'),
(105, 1, 'prm', 'isssam.web.technologie@gmail.com', 0, '(رسالة من موظف تجريبي ) fghjklmù', 0, NULL, '2018-11-28 22:40:11', '2018-11-28 22:40:11'),
(106, 1, 'prm', 'isssam.web.technologie@gmail.com', 0, '(رسالة من موظف تجريبي ) fghjklmù', 0, NULL, '2018-11-28 22:40:35', '2018-11-28 22:40:35'),
(107, 1, 'prm', 'devweb218@gmail.com', 0, '(رسالة من موظف تجريبي ) rtrtrn tk tkjnetj e jk ejknerjkntef  rre e', 0, NULL, '2018-11-28 22:48:25', '2018-11-28 22:48:25'),
(108, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/32<br><a href=\'https://ccreation.sa/erp/meeting/29/32\'>https://ccreation.sa/erp/meeting/29/32</a>', 0, NULL, '2018-12-01 10:22:58', '2018-12-01 10:22:58'),
(109, 12, 'meetings', '00966509193274', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/32\n\nhttps://ccreation.sa/erp/meeting/29/32', 1, NULL, '2018-12-01 10:22:59', '2018-12-01 10:22:59'),
(110, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/20<br><a href=\'https://ccreation.sa/erp/meeting/29/20\'>https://ccreation.sa/erp/meeting/29/20</a>', 0, NULL, '2018-12-01 10:22:59', '2018-12-01 10:22:59'),
(111, 12, 'meetings', '00966563421359', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/20\n\nhttps://ccreation.sa/erp/meeting/29/20', 1, NULL, '2018-12-01 10:22:59', '2018-12-01 10:22:59'),
(112, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/30<br><a href=\'https://ccreation.sa/erp/meeting/29/30\'>https://ccreation.sa/erp/meeting/29/30</a>', 0, NULL, '2018-12-01 10:22:59', '2018-12-01 10:22:59'),
(113, 12, 'meetings', '00966535779245', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/30\n\nhttps://ccreation.sa/erp/meeting/29/30', 1, NULL, '2018-12-01 10:22:59', '2018-12-01 10:22:59'),
(114, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/31<br><a href=\'https://ccreation.sa/erp/meeting/29/31\'>https://ccreation.sa/erp/meeting/29/31</a>', 0, NULL, '2018-12-01 10:23:00', '2018-12-01 10:23:00'),
(115, 12, 'meetings', '00966541009675', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/31\n\nhttps://ccreation.sa/erp/meeting/29/31', 1, NULL, '2018-12-01 10:23:00', '2018-12-01 10:23:00'),
(116, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/27<br><a href=\'https://ccreation.sa/erp/meeting/29/27\'>https://ccreation.sa/erp/meeting/29/27</a>', 0, NULL, '2018-12-01 10:23:00', '2018-12-01 10:23:00'),
(117, 12, 'meetings', '00966582983966', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/27\n\nhttps://ccreation.sa/erp/meeting/29/27', 1, NULL, '2018-12-01 10:23:00', '2018-12-01 10:23:00'),
(118, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/29<br><a href=\'https://ccreation.sa/erp/meeting/29/29\'>https://ccreation.sa/erp/meeting/29/29</a>', 0, NULL, '2018-12-01 10:23:01', '2018-12-01 10:23:01'),
(119, 12, 'meetings', '00966507771868', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/29\n\nhttps://ccreation.sa/erp/meeting/29/29', 1, NULL, '2018-12-01 10:23:01', '2018-12-01 10:23:01'),
(120, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/28<br><a href=\'https://ccreation.sa/erp/meeting/29/28\'>https://ccreation.sa/erp/meeting/29/28</a>', 0, NULL, '2018-12-01 10:23:01', '2018-12-01 10:23:01'),
(121, 12, 'meetings', '00966507187011', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/28\n\nhttps://ccreation.sa/erp/meeting/29/28', 1, NULL, '2018-12-01 10:23:01', '2018-12-01 10:23:01'),
(122, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/22<br><a href=\'https://ccreation.sa/erp/meeting/29/22\'>https://ccreation.sa/erp/meeting/29/22</a>', 0, NULL, '2018-12-01 10:23:02', '2018-12-01 10:23:02'),
(123, 12, 'meetings', '00966502480256', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/22\n\nhttps://ccreation.sa/erp/meeting/29/22', 1, NULL, '2018-12-01 10:23:02', '2018-12-01 10:23:02'),
(124, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : الاجتماع السادس<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/23<br><a href=\'https://ccreation.sa/erp/meeting/29/23\'>https://ccreation.sa/erp/meeting/29/23</a>', 0, NULL, '2018-12-01 10:23:03', '2018-12-01 10:23:03'),
(125, 12, 'meetings', '00966558037380', 1, 'الإجتماع : الاجتماع السادس\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/29/23\n\nhttps://ccreation.sa/erp/meeting/29/23', 1, NULL, '2018-12-01 10:23:03', '2018-12-01 10:23:03'),
(126, 12, 'meetings', '00966509193274', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/32', 1, NULL, '2018-12-01 10:25:36', '2018-12-01 10:25:36'),
(127, 12, 'meetings', '00966563421359', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/20', 1, NULL, '2018-12-01 10:25:36', '2018-12-01 10:25:36'),
(128, 12, 'meetings', '00966558037380', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/23', 1, NULL, '2018-12-01 10:25:36', '2018-12-01 10:25:36'),
(129, 12, 'meetings', '00966502480256', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/22', 1, NULL, '2018-12-01 10:25:36', '2018-12-01 10:25:36'),
(130, 12, 'meetings', '00966507187011', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/28', 1, NULL, '2018-12-01 10:25:36', '2018-12-01 10:25:36'),
(131, 12, 'meetings', '00966507771868', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/29', 1, NULL, '2018-12-01 10:25:37', '2018-12-01 10:25:37'),
(132, 12, 'meetings', '00966582983966', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/27', 1, NULL, '2018-12-01 10:25:37', '2018-12-01 10:25:37'),
(133, 12, 'meetings', '00966541009675', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/31', 1, NULL, '2018-12-01 10:25:37', '2018-12-01 10:25:37'),
(134, 12, 'meetings', '00966535779245', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/30', 1, NULL, '2018-12-01 10:25:37', '2018-12-01 10:25:37'),
(135, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/20\'>https://ccreation.sa/erp/meeting/29/20</a>', 0, NULL, '2018-12-01 10:38:47', '2018-12-01 10:38:47'),
(136, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/23\'>https://ccreation.sa/erp/meeting/29/23</a>', 0, NULL, '2018-12-01 10:38:47', '2018-12-01 10:38:47'),
(137, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/22\'>https://ccreation.sa/erp/meeting/29/22</a>', 0, NULL, '2018-12-01 10:38:48', '2018-12-01 10:38:48'),
(138, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/28\'>https://ccreation.sa/erp/meeting/29/28</a>', 0, NULL, '2018-12-01 10:38:48', '2018-12-01 10:38:48'),
(139, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/29\'>https://ccreation.sa/erp/meeting/29/29</a>', 0, NULL, '2018-12-01 10:38:48', '2018-12-01 10:38:48'),
(140, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/27\'>https://ccreation.sa/erp/meeting/29/27</a>', 0, NULL, '2018-12-01 10:38:48', '2018-12-01 10:38:48'),
(141, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/31\'>https://ccreation.sa/erp/meeting/29/31</a>', 0, NULL, '2018-12-01 10:38:48', '2018-12-01 10:38:48'),
(142, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : الاجتماع السادس<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/29/30\'>https://ccreation.sa/erp/meeting/29/30</a>', 0, NULL, '2018-12-01 10:38:48', '2018-12-01 10:38:48'),
(143, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : HomePage<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/32<br><a href=\'https://ccreation.sa/erp/meeting/30/32\'>https://ccreation.sa/erp/meeting/30/32</a>', 0, NULL, '2018-12-03 05:54:39', '2018-12-03 05:54:39'),
(144, 12, 'meetings', '00966509193274', 1, 'الإجتماع : HomePage\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/32\n\nhttps://ccreation.sa/erp/meeting/30/32', 1, NULL, '2018-12-03 05:54:39', '2018-12-03 05:54:39'),
(145, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : HomePage<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/20<br><a href=\'https://ccreation.sa/erp/meeting/30/20\'>https://ccreation.sa/erp/meeting/30/20</a>', 0, NULL, '2018-12-03 05:54:39', '2018-12-03 05:54:39'),
(146, 12, 'meetings', '00966563421359', 1, 'الإجتماع : HomePage\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/20\n\nhttps://ccreation.sa/erp/meeting/30/20', 1, NULL, '2018-12-03 05:54:39', '2018-12-03 05:54:39'),
(147, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : HomePage<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/32<br><a href=\'https://ccreation.sa/erp/meeting/30/32\'>https://ccreation.sa/erp/meeting/30/32</a>', 0, NULL, '2018-12-03 05:54:40', '2018-12-03 05:54:40'),
(148, 12, 'meetings', '00966509193274', 1, 'الإجتماع : HomePage\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/32\n\nhttps://ccreation.sa/erp/meeting/30/32', 1, NULL, '2018-12-03 05:54:40', '2018-12-03 05:54:40'),
(149, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : HomePage<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/30<br><a href=\'https://ccreation.sa/erp/meeting/30/30\'>https://ccreation.sa/erp/meeting/30/30</a>', 0, NULL, '2018-12-03 05:54:40', '2018-12-03 05:54:40'),
(150, 12, 'meetings', '00966535779245', 1, 'الإجتماع : HomePage\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/30/30\n\nhttps://ccreation.sa/erp/meeting/30/30', 1, NULL, '2018-12-03 05:54:40', '2018-12-03 05:54:40'),
(151, 12, 'meetings', '00966535779245', 1, 'الإجتماع : HomePage\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/30/30', 1, NULL, '2018-12-03 06:07:25', '2018-12-03 06:07:25'),
(152, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : HomePage<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://ccreation.sa/erp/meeting/30/30\'>https://ccreation.sa/erp/meeting/30/30</a>', 0, NULL, '2018-12-03 06:08:10', '2018-12-03 06:08:10'),
(153, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : test<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/31/30<br><a href=\'https://ccreation.sa/erp/meeting/31/30\'>https://ccreation.sa/erp/meeting/31/30</a>', 0, NULL, '2018-12-03 17:50:59', '2018-12-03 17:50:59'),
(154, 12, 'meetings', '00966535779245', 1, 'الإجتماع : test\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/31/30\n\nhttps://ccreation.sa/erp/meeting/31/30', 1, NULL, '2018-12-03 17:50:59', '2018-12-03 17:50:59'),
(155, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : test<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/31/32<br><a href=\'https://ccreation.sa/erp/meeting/31/32\'>https://ccreation.sa/erp/meeting/31/32</a>', 0, NULL, '2018-12-03 17:50:59', '2018-12-03 17:50:59'),
(156, 12, 'meetings', '00966509193274', 1, 'الإجتماع : test\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/31/32\n\nhttps://ccreation.sa/erp/meeting/31/32', 1, NULL, '2018-12-03 17:50:59', '2018-12-03 17:50:59'),
(157, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : test<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/31/20<br><a href=\'https://ccreation.sa/erp/meeting/31/20\'>https://ccreation.sa/erp/meeting/31/20</a>', 0, NULL, '2018-12-03 17:50:59', '2018-12-03 17:50:59'),
(158, 12, 'meetings', '00966563421359', 1, 'الإجتماع : test\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/31/20\n\nhttps://ccreation.sa/erp/meeting/31/20', 1, NULL, '2018-12-03 17:51:00', '2018-12-03 17:51:00'),
(159, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : test<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/32/30<br><a href=\'https://ccreation.sa/erp/meeting/32/30\'>https://ccreation.sa/erp/meeting/32/30</a>', 0, NULL, '2018-12-03 17:54:56', '2018-12-03 17:54:56'),
(160, 12, 'meetings', '00966535779245', 1, 'الإجتماع : test\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/32/30\n\nhttps://ccreation.sa/erp/meeting/32/30', 1, NULL, '2018-12-03 17:54:57', '2018-12-03 17:54:57'),
(161, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : test<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/32/32<br><a href=\'https://ccreation.sa/erp/meeting/32/32\'>https://ccreation.sa/erp/meeting/32/32</a>', 0, NULL, '2018-12-03 17:54:57', '2018-12-03 17:54:57'),
(162, 12, 'meetings', '00966509193274', 1, 'الإجتماع : test\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/32/32\n\nhttps://ccreation.sa/erp/meeting/32/32', 1, NULL, '2018-12-03 17:54:57', '2018-12-03 17:54:57'),
(163, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : test<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/32/30<br><a href=\'https://ccreation.sa/erp/meeting/32/30\'>https://ccreation.sa/erp/meeting/32/30</a>', 0, NULL, '2018-12-03 17:54:57', '2018-12-03 17:54:57'),
(164, 12, 'meetings', '00966535779245', 1, 'الإجتماع : test\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/32/30\n\nhttps://ccreation.sa/erp/meeting/32/30', 1, NULL, '2018-12-03 17:54:58', '2018-12-03 17:54:58'),
(165, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/32<br><a href=\'https://ccreation.sa/erp/meeting/33/32\'>https://ccreation.sa/erp/meeting/33/32</a>', 0, NULL, '2018-12-08 09:00:01', '2018-12-08 09:00:01'),
(166, 12, 'meetings', '00966509193274', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/32\n\nhttps://ccreation.sa/erp/meeting/33/32', 1, NULL, '2018-12-08 09:00:01', '2018-12-08 09:00:01'),
(167, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/20<br><a href=\'https://ccreation.sa/erp/meeting/33/20\'>https://ccreation.sa/erp/meeting/33/20</a>', 0, NULL, '2018-12-08 09:00:01', '2018-12-08 09:00:01'),
(168, 12, 'meetings', '00966563421359', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/20\n\nhttps://ccreation.sa/erp/meeting/33/20', 1, NULL, '2018-12-08 09:00:02', '2018-12-08 09:00:02'),
(169, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/30<br><a href=\'https://ccreation.sa/erp/meeting/33/30\'>https://ccreation.sa/erp/meeting/33/30</a>', 0, NULL, '2018-12-08 09:00:03', '2018-12-08 09:00:03'),
(170, 12, 'meetings', '00966535779245', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/30\n\nhttps://ccreation.sa/erp/meeting/33/30', 1, NULL, '2018-12-08 09:00:03', '2018-12-08 09:00:03'),
(171, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/31<br><a href=\'https://ccreation.sa/erp/meeting/33/31\'>https://ccreation.sa/erp/meeting/33/31</a>', 0, NULL, '2018-12-08 09:00:04', '2018-12-08 09:00:04'),
(172, 12, 'meetings', '00966541009675', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/31\n\nhttps://ccreation.sa/erp/meeting/33/31', 1, NULL, '2018-12-08 09:00:04', '2018-12-08 09:00:04'),
(173, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/27<br><a href=\'https://ccreation.sa/erp/meeting/33/27\'>https://ccreation.sa/erp/meeting/33/27</a>', 0, NULL, '2018-12-08 09:00:04', '2018-12-08 09:00:04'),
(174, 12, 'meetings', '00966582983966', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/27\n\nhttps://ccreation.sa/erp/meeting/33/27', 1, NULL, '2018-12-08 09:00:05', '2018-12-08 09:00:05'),
(175, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/29<br><a href=\'https://ccreation.sa/erp/meeting/33/29\'>https://ccreation.sa/erp/meeting/33/29</a>', 0, NULL, '2018-12-08 09:00:05', '2018-12-08 09:00:05'),
(176, 12, 'meetings', '00966507771868', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/29\n\nhttps://ccreation.sa/erp/meeting/33/29', 1, NULL, '2018-12-08 09:00:05', '2018-12-08 09:00:05'),
(177, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/28<br><a href=\'https://ccreation.sa/erp/meeting/33/28\'>https://ccreation.sa/erp/meeting/33/28</a>', 0, NULL, '2018-12-08 09:00:05', '2018-12-08 09:00:05'),
(178, 12, 'meetings', '00966507187011', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/28\n\nhttps://ccreation.sa/erp/meeting/33/28', 1, NULL, '2018-12-08 09:00:06', '2018-12-08 09:00:06'),
(179, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/22<br><a href=\'https://ccreation.sa/erp/meeting/33/22\'>https://ccreation.sa/erp/meeting/33/22</a>', 0, NULL, '2018-12-08 09:00:06', '2018-12-08 09:00:06'),
(180, 12, 'meetings', '00966502480256', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/22\n\nhttps://ccreation.sa/erp/meeting/33/22', 1, NULL, '2018-12-08 09:00:06', '2018-12-08 09:00:06'),
(181, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : السابع /مناقشة المحاور المدرجة<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/23<br><a href=\'https://ccreation.sa/erp/meeting/33/23\'>https://ccreation.sa/erp/meeting/33/23</a>', 0, NULL, '2018-12-08 09:00:06', '2018-12-08 09:00:06'),
(182, 12, 'meetings', '00966558037380', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/33/23\n\nhttps://ccreation.sa/erp/meeting/33/23', 1, NULL, '2018-12-08 09:00:06', '2018-12-08 09:00:06'),
(183, 12, 'meetings', '00966509193274', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/32', 1, NULL, '2018-12-08 09:01:07', '2018-12-08 09:01:07'),
(184, 12, 'meetings', '00966563421359', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/20', 1, NULL, '2018-12-08 09:01:07', '2018-12-08 09:01:07'),
(185, 12, 'meetings', '00966535779245', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/30', 1, NULL, '2018-12-08 09:01:08', '2018-12-08 09:01:08'),
(186, 12, 'meetings', '00966558037380', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/23', 1, NULL, '2018-12-08 09:01:08', '2018-12-08 09:01:08'),
(187, 12, 'meetings', '00966502480256', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/22', 1, NULL, '2018-12-08 09:01:08', '2018-12-08 09:01:08'),
(188, 12, 'meetings', '00966507187011', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/28', 1, NULL, '2018-12-08 09:01:08', '2018-12-08 09:01:08'),
(189, 12, 'meetings', '00966507771868', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/29', 1, NULL, '2018-12-08 09:01:09', '2018-12-08 09:01:09'),
(190, 12, 'meetings', '00966582983966', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/27', 1, NULL, '2018-12-08 09:01:09', '2018-12-08 09:01:09'),
(191, 12, 'meetings', '00966541009675', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/31', 1, NULL, '2018-12-08 09:01:09', '2018-12-08 09:01:09'),
(192, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:21:10', '2018-12-10 16:21:10'),
(193, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:47:05', '2018-12-10 16:47:05'),
(194, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:47:59', '2018-12-10 16:47:59'),
(195, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:49:54', '2018-12-10 16:49:54'),
(196, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:50:50', '2018-12-10 16:50:50'),
(197, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:51:56', '2018-12-10 16:51:56'),
(198, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:54:18', '2018-12-10 16:54:18'),
(199, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:54:44', '2018-12-10 16:54:44'),
(200, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:57:24', '2018-12-10 16:57:24'),
(201, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:57:40', '2018-12-10 16:57:40'),
(202, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 16:57:50', '2018-12-10 16:57:50'),
(203, 12, 'meetings', '00966509193274', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/32', 1, NULL, '2018-12-10 17:06:09', '2018-12-10 17:06:09');
INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(204, 12, 'meetings', '00966563421359', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/20', 1, NULL, '2018-12-10 17:06:09', '2018-12-10 17:06:09'),
(205, 12, 'meetings', '00966502480256', 1, 'الإجتماع : الاجتماع السادس\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/29/22', 1, NULL, '2018-12-10 17:06:10', '2018-12-10 17:06:10'),
(206, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:17:06', '2018-12-10 17:17:06'),
(207, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:17:36', '2018-12-10 17:17:36'),
(208, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:23:04', '2018-12-10 17:23:04'),
(209, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:23:17', '2018-12-10 17:23:17'),
(210, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:24:26', '2018-12-10 17:24:26'),
(211, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:31:39', '2018-12-10 17:31:39'),
(212, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:32:06', '2018-12-10 17:32:06'),
(213, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:33:05', '2018-12-10 17:33:05'),
(214, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:33:23', '2018-12-10 17:33:23'),
(215, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:38:09', '2018-12-10 17:38:09'),
(216, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:40:58', '2018-12-10 17:40:58'),
(217, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2018-12-10 17:41:26', '2018-12-10 17:41:26'),
(218, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/32<br><a href=\'https://ccreation.sa/erp/meeting/34/32\'>https://ccreation.sa/erp/meeting/34/32</a>', 0, NULL, '2018-12-17 16:58:29', '2018-12-17 16:58:29'),
(219, 12, 'meetings', '00966509193274', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/32\n\nhttps://ccreation.sa/erp/meeting/34/32', 1, NULL, '2018-12-17 16:58:30', '2018-12-17 16:58:30'),
(220, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/20<br><a href=\'https://ccreation.sa/erp/meeting/34/20\'>https://ccreation.sa/erp/meeting/34/20</a>', 0, NULL, '2018-12-17 16:58:30', '2018-12-17 16:58:30'),
(221, 12, 'meetings', '00966563421359', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/20\n\nhttps://ccreation.sa/erp/meeting/34/20', 1, NULL, '2018-12-17 16:58:30', '2018-12-17 16:58:30'),
(222, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/30<br><a href=\'https://ccreation.sa/erp/meeting/34/30\'>https://ccreation.sa/erp/meeting/34/30</a>', 0, NULL, '2018-12-17 16:58:30', '2018-12-17 16:58:30'),
(223, 12, 'meetings', '00966535779245', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/30\n\nhttps://ccreation.sa/erp/meeting/34/30', 1, NULL, '2018-12-17 16:58:30', '2018-12-17 16:58:30'),
(224, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/31<br><a href=\'https://ccreation.sa/erp/meeting/34/31\'>https://ccreation.sa/erp/meeting/34/31</a>', 0, NULL, '2018-12-17 16:58:31', '2018-12-17 16:58:31'),
(225, 12, 'meetings', '00966541009675', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/31\n\nhttps://ccreation.sa/erp/meeting/34/31', 1, NULL, '2018-12-17 16:58:31', '2018-12-17 16:58:31'),
(226, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/27<br><a href=\'https://ccreation.sa/erp/meeting/34/27\'>https://ccreation.sa/erp/meeting/34/27</a>', 0, NULL, '2018-12-17 16:58:31', '2018-12-17 16:58:31'),
(227, 12, 'meetings', '00966582983966', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/27\n\nhttps://ccreation.sa/erp/meeting/34/27', 1, NULL, '2018-12-17 16:58:31', '2018-12-17 16:58:31'),
(228, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/29<br><a href=\'https://ccreation.sa/erp/meeting/34/29\'>https://ccreation.sa/erp/meeting/34/29</a>', 0, NULL, '2018-12-17 16:58:31', '2018-12-17 16:58:31'),
(229, 12, 'meetings', '00966507771868', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/29\n\nhttps://ccreation.sa/erp/meeting/34/29', 1, NULL, '2018-12-17 16:58:31', '2018-12-17 16:58:31'),
(230, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/28<br><a href=\'https://ccreation.sa/erp/meeting/34/28\'>https://ccreation.sa/erp/meeting/34/28</a>', 0, NULL, '2018-12-17 16:58:32', '2018-12-17 16:58:32'),
(231, 12, 'meetings', '00966507187011', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/28\n\nhttps://ccreation.sa/erp/meeting/34/28', 1, NULL, '2018-12-17 16:58:32', '2018-12-17 16:58:32'),
(232, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/22<br><a href=\'https://ccreation.sa/erp/meeting/34/22\'>https://ccreation.sa/erp/meeting/34/22</a>', 0, NULL, '2018-12-17 16:58:32', '2018-12-17 16:58:32'),
(233, 12, 'meetings', '00966502480256', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/22\n\nhttps://ccreation.sa/erp/meeting/34/22', 1, NULL, '2018-12-17 16:58:32', '2018-12-17 16:58:32'),
(234, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/23<br><a href=\'https://ccreation.sa/erp/meeting/34/23\'>https://ccreation.sa/erp/meeting/34/23</a>', 0, NULL, '2018-12-17 16:58:32', '2018-12-17 16:58:32'),
(235, 12, 'meetings', '00966558037380', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/23\n\nhttps://ccreation.sa/erp/meeting/34/23', 1, NULL, '2018-12-17 16:58:33', '2018-12-17 16:58:33'),
(236, 12, 'meetings', 'bm@ccreation.sa', 0, 'الإجتماع : مناقشة ومتابعة المهام<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/42<br><a href=\'https://ccreation.sa/erp/meeting/34/42\'>https://ccreation.sa/erp/meeting/34/42</a>', 0, NULL, '2018-12-17 16:58:33', '2018-12-17 16:58:33'),
(237, 12, 'meetings', '00966544320099', 1, 'الإجتماع : مناقشة ومتابعة المهام\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/34/42\n\nhttps://ccreation.sa/erp/meeting/34/42', 1, NULL, '2018-12-17 16:58:33', '2018-12-17 16:58:33'),
(238, 12, 'meetings', '00966509193274', 1, 'الإجتماع : السابع /مناقشة المحاور المدرجة\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/33/32', 1, NULL, '2018-12-17 18:23:34', '2018-12-17 18:23:34'),
(239, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/32<br><a href=\'https://ccreation.sa/erp/meeting/35/32\'>https://ccreation.sa/erp/meeting/35/32</a>', 0, NULL, '2018-12-21 21:04:52', '2018-12-21 21:04:52'),
(240, 12, 'meetings', '00966509193274', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/32\n\nhttps://ccreation.sa/erp/meeting/35/32', 0, 'Insufficient Fund', '2018-12-21 21:04:53', '2018-12-21 21:04:53'),
(241, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/20<br><a href=\'https://ccreation.sa/erp/meeting/35/20\'>https://ccreation.sa/erp/meeting/35/20</a>', 0, NULL, '2018-12-21 21:04:53', '2018-12-21 21:04:53'),
(242, 12, 'meetings', '00966563421359', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/20\n\nhttps://ccreation.sa/erp/meeting/35/20', 0, 'Insufficient Fund', '2018-12-21 21:04:53', '2018-12-21 21:04:53'),
(243, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/30<br><a href=\'https://ccreation.sa/erp/meeting/35/30\'>https://ccreation.sa/erp/meeting/35/30</a>', 0, NULL, '2018-12-21 21:04:54', '2018-12-21 21:04:54'),
(244, 12, 'meetings', '00966535779245', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/30\n\nhttps://ccreation.sa/erp/meeting/35/30', 0, 'Insufficient Fund', '2018-12-21 21:04:54', '2018-12-21 21:04:54'),
(245, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/31<br><a href=\'https://ccreation.sa/erp/meeting/35/31\'>https://ccreation.sa/erp/meeting/35/31</a>', 0, NULL, '2018-12-21 21:04:54', '2018-12-21 21:04:54'),
(246, 12, 'meetings', '00966541009675', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/31\n\nhttps://ccreation.sa/erp/meeting/35/31', 0, 'Insufficient Fund', '2018-12-21 21:04:55', '2018-12-21 21:04:55'),
(247, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/27<br><a href=\'https://ccreation.sa/erp/meeting/35/27\'>https://ccreation.sa/erp/meeting/35/27</a>', 0, NULL, '2018-12-21 21:04:55', '2018-12-21 21:04:55'),
(248, 12, 'meetings', '00966582983966', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/27\n\nhttps://ccreation.sa/erp/meeting/35/27', 0, 'Insufficient Fund', '2018-12-21 21:04:56', '2018-12-21 21:04:56'),
(249, 12, 'meetings', 'amidris1985@gmail.com', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/29<br><a href=\'https://ccreation.sa/erp/meeting/35/29\'>https://ccreation.sa/erp/meeting/35/29</a>', 0, NULL, '2018-12-21 21:04:56', '2018-12-21 21:04:56'),
(250, 12, 'meetings', '00966507771868', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/29\n\nhttps://ccreation.sa/erp/meeting/35/29', 0, 'Insufficient Fund', '2018-12-21 21:04:56', '2018-12-21 21:04:56'),
(251, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/28<br><a href=\'https://ccreation.sa/erp/meeting/35/28\'>https://ccreation.sa/erp/meeting/35/28</a>', 0, NULL, '2018-12-21 21:04:56', '2018-12-21 21:04:56'),
(252, 12, 'meetings', '00966507187011', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/28\n\nhttps://ccreation.sa/erp/meeting/35/28', 0, 'Insufficient Fund', '2018-12-21 21:04:57', '2018-12-21 21:04:57'),
(253, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/22<br><a href=\'https://ccreation.sa/erp/meeting/35/22\'>https://ccreation.sa/erp/meeting/35/22</a>', 0, NULL, '2018-12-21 21:04:57', '2018-12-21 21:04:57'),
(254, 12, 'meetings', '00966502480256', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/22\n\nhttps://ccreation.sa/erp/meeting/35/22', 0, 'Insufficient Fund', '2018-12-21 21:04:57', '2018-12-21 21:04:57'),
(255, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/23<br><a href=\'https://ccreation.sa/erp/meeting/35/23\'>https://ccreation.sa/erp/meeting/35/23</a>', 0, NULL, '2018-12-21 21:04:58', '2018-12-21 21:04:58'),
(256, 12, 'meetings', '00966558037380', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/23\n\nhttps://ccreation.sa/erp/meeting/35/23', 0, 'Insufficient Fund', '2018-12-21 21:04:58', '2018-12-21 21:04:58'),
(257, 12, 'meetings', 'bm@ccreation.sa', 0, 'الإجتماع : التاسع مناقشة المحاور<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/42<br><a href=\'https://ccreation.sa/erp/meeting/35/42\'>https://ccreation.sa/erp/meeting/35/42</a>', 0, NULL, '2018-12-21 21:04:58', '2018-12-21 21:04:58'),
(258, 12, 'meetings', '00966544320099', 1, 'الإجتماع : التاسع مناقشة المحاور\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/35/42\n\nhttps://ccreation.sa/erp/meeting/35/42', 0, 'Insufficient Fund', '2018-12-21 21:04:59', '2018-12-21 21:04:59'),
(259, 12, 'meetings', 'AlQurashi@ccreation.sa', 0, 'الإجتماع : hh<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/36/32<br><a href=\'https://ccreation.sa/erp/meeting/36/32\'>https://ccreation.sa/erp/meeting/36/32</a>', 0, NULL, '2018-12-23 12:27:30', '2018-12-23 12:27:30'),
(260, 12, 'meetings', '00966509193274', 1, 'الإجتماع : hh\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/36/32\n\nhttps://ccreation.sa/erp/meeting/36/32', 0, 'Insufficient Fund', '2018-12-23 12:27:31', '2018-12-23 12:27:31'),
(261, 12, 'meetings', 'bm@ccreation.sa', 0, 'الإجتماع : hh<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/36/42<br><a href=\'https://ccreation.sa/erp/meeting/36/42\'>https://ccreation.sa/erp/meeting/36/42</a>', 0, NULL, '2018-12-23 12:27:31', '2018-12-23 12:27:31'),
(262, 12, 'meetings', '00966544320099', 1, 'الإجتماع : hh\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/36/42\n\nhttps://ccreation.sa/erp/meeting/36/42', 0, 'Insufficient Fund', '2018-12-23 12:27:31', '2018-12-23 12:27:31'),
(263, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : مكتب الفريان ( للمحاماة )<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/37/22<br><a href=\'https://ccreation.sa/erp/meeting/37/22\'>https://ccreation.sa/erp/meeting/37/22</a>', 0, NULL, '2018-12-25 13:55:22', '2018-12-25 13:55:22'),
(264, 12, 'meetings', '00966502480256', 1, 'الإجتماع : مكتب الفريان ( للمحاماة )\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/37/22\n\nhttps://ccreation.sa/erp/meeting/37/22', 1, NULL, '2018-12-25 13:55:22', '2018-12-25 13:55:22'),
(265, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : مكتب الفريان ( للمحاماة )<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/37/20<br><a href=\'https://ccreation.sa/erp/meeting/37/20\'>https://ccreation.sa/erp/meeting/37/20</a>', 0, NULL, '2018-12-25 13:55:23', '2018-12-25 13:55:23'),
(266, 12, 'meetings', '00966563421359', 1, 'الإجتماع : مكتب الفريان ( للمحاماة )\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/37/20\n\nhttps://ccreation.sa/erp/meeting/37/20', 1, NULL, '2018-12-25 13:55:23', '2018-12-25 13:55:23'),
(267, 12, 'meetings', 'bm@ccreation.sa', 0, 'الإجتماع : مكتب الفريان ( للمحاماة )<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/37/42<br><a href=\'https://ccreation.sa/erp/meeting/37/42\'>https://ccreation.sa/erp/meeting/37/42</a>', 0, NULL, '2018-12-25 13:55:23', '2018-12-25 13:55:23'),
(268, 12, 'meetings', '00966544320099', 1, 'الإجتماع : مكتب الفريان ( للمحاماة )\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/37/42\n\nhttps://ccreation.sa/erp/meeting/37/42', 1, NULL, '2018-12-25 13:55:23', '2018-12-25 13:55:23'),
(269, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : حملة الجبيل<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/31<br><a href=\'https://ccreation.sa/erp/meeting/38/31\'>https://ccreation.sa/erp/meeting/38/31</a>', 0, NULL, '2018-12-25 14:49:36', '2018-12-25 14:49:36'),
(270, 12, 'meetings', '00966541009675', 1, 'الإجتماع : حملة الجبيل\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/31\n\nhttps://ccreation.sa/erp/meeting/38/31', 1, NULL, '2018-12-25 14:49:36', '2018-12-25 14:49:36'),
(271, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : حملة الجبيل<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/20<br><a href=\'https://ccreation.sa/erp/meeting/38/20\'>https://ccreation.sa/erp/meeting/38/20</a>', 0, NULL, '2018-12-25 14:49:36', '2018-12-25 14:49:36'),
(272, 12, 'meetings', '00966563421359', 1, 'الإجتماع : حملة الجبيل\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/20\n\nhttps://ccreation.sa/erp/meeting/38/20', 1, NULL, '2018-12-25 14:49:36', '2018-12-25 14:49:36'),
(273, 12, 'meetings', 'Tahani_mhmad@outlook.sa', 0, 'الإجتماع : حملة الجبيل<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/28<br><a href=\'https://ccreation.sa/erp/meeting/38/28\'>https://ccreation.sa/erp/meeting/38/28</a>', 0, NULL, '2018-12-25 14:49:37', '2018-12-25 14:49:37'),
(274, 12, 'meetings', '00966507187011', 1, 'الإجتماع : حملة الجبيل\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/28\n\nhttps://ccreation.sa/erp/meeting/38/28', 1, NULL, '2018-12-25 14:49:37', '2018-12-25 14:49:37'),
(275, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : حملة الجبيل<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/22<br><a href=\'https://ccreation.sa/erp/meeting/38/22\'>https://ccreation.sa/erp/meeting/38/22</a>', 0, NULL, '2018-12-25 14:49:37', '2018-12-25 14:49:37'),
(276, 12, 'meetings', '00966502480256', 1, 'الإجتماع : حملة الجبيل\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/38/22\n\nhttps://ccreation.sa/erp/meeting/38/22', 1, NULL, '2018-12-25 14:49:37', '2018-12-25 14:49:37'),
(277, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة موظف جديد', 0, NULL, '2018-12-25 17:45:53', '2018-12-25 17:45:53'),
(278, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة موظف جديد', 1, NULL, '2018-12-25 17:45:53', '2018-12-25 17:45:53'),
(279, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة موظف جديد', 0, NULL, '2018-12-25 17:55:00', '2018-12-25 17:55:00'),
(280, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة موظف جديد', 1, NULL, '2018-12-25 17:55:00', '2018-12-25 17:55:00'),
(281, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف موظف من النظام', 0, NULL, '2018-12-25 18:15:28', '2018-12-25 18:15:28'),
(282, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف موظف من النظام', 1, NULL, '2018-12-25 18:15:28', '2018-12-25 18:15:28'),
(283, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة بدل جديد', 0, NULL, '2018-12-25 18:18:33', '2018-12-25 18:18:33'),
(284, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة بدل جديد', 1, NULL, '2018-12-25 18:18:33', '2018-12-25 18:18:33'),
(285, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة بدل جديد', 0, NULL, '2018-12-25 18:20:41', '2018-12-25 18:20:41'),
(286, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة بدل جديد', 1, NULL, '2018-12-25 18:20:41', '2018-12-25 18:20:41'),
(287, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف بدل من النظام', 0, NULL, '2018-12-25 18:20:41', '2018-12-25 18:20:41'),
(288, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف بدل من النظام', 1, NULL, '2018-12-25 18:20:42', '2018-12-25 18:20:42'),
(289, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تمت حذف إقتطاع جديد', 0, NULL, '2018-12-25 18:23:02', '2018-12-25 18:23:02'),
(290, 12, 'hrm', '0021699961102', 1, 'لقد تمت حذف إقتطاع جديد', 1, NULL, '2018-12-25 18:23:02', '2018-12-25 18:23:02'),
(291, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تمت إضافة إقتطاع جديد', 0, NULL, '2018-12-25 18:23:03', '2018-12-25 18:23:03'),
(292, 12, 'hrm', '0021699961102', 1, 'لقد تمت إضافة إقتطاع جديد', 1, NULL, '2018-12-25 18:23:03', '2018-12-25 18:23:03'),
(293, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة سلفة جديدة', 0, NULL, '2018-12-25 18:25:28', '2018-12-25 18:25:28'),
(294, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة سلفة جديدة', 1, NULL, '2018-12-25 18:25:28', '2018-12-25 18:25:28'),
(295, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف سلفة جديدة', 0, NULL, '2018-12-25 18:25:28', '2018-12-25 18:25:28'),
(296, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف سلفة جديدة', 1, NULL, '2018-12-25 18:25:28', '2018-12-25 18:25:28'),
(297, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة سلفة جديدة', 0, NULL, '2018-12-25 18:28:34', '2018-12-25 18:28:34'),
(298, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة سلفة جديدة', 1, NULL, '2018-12-25 18:28:34', '2018-12-25 18:28:34'),
(299, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف سلفة جديدة', 0, NULL, '2018-12-25 18:28:34', '2018-12-25 18:28:34'),
(300, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف سلفة جديدة', 1, NULL, '2018-12-25 18:28:34', '2018-12-25 18:28:34'),
(301, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة عهدة جديدة', 0, NULL, '2018-12-25 18:29:24', '2018-12-25 18:29:24'),
(302, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة عهدة جديدة', 1, NULL, '2018-12-25 18:29:25', '2018-12-25 18:29:25'),
(303, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم تأكيد إستلام عهدة', 0, NULL, '2018-12-25 18:29:25', '2018-12-25 18:29:25'),
(304, 12, 'hrm', '0021699961102', 1, 'لقد تم تأكيد إستلام عهدة', 1, NULL, '2018-12-25 18:29:25', '2018-12-25 18:29:25'),
(305, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف عهدة', 0, NULL, '2018-12-25 18:29:26', '2018-12-25 18:29:26'),
(306, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف عهدة', 1, NULL, '2018-12-25 18:29:26', '2018-12-25 18:29:26'),
(307, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم تقديم إجازة جديدة', 0, NULL, '2018-12-25 18:31:21', '2018-12-25 18:31:21'),
(308, 12, 'hrm', '0021699961102', 1, 'لقد تم تقديم إجازة جديدة', 1, NULL, '2018-12-25 18:31:21', '2018-12-25 18:31:21'),
(309, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم قبول إجازة جديدة', 0, NULL, '2018-12-25 18:33:09', '2018-12-25 18:33:09'),
(310, 12, 'hrm', '0021699961102', 1, 'لقد تم قبول إجازة جديدة', 1, NULL, '2018-12-25 18:33:10', '2018-12-25 18:33:10'),
(311, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم رفض إجازة', 0, NULL, '2018-12-25 18:33:11', '2018-12-25 18:33:11'),
(312, 12, 'hrm', '0021699961102', 1, 'لقد تم رفض إجازة', 1, NULL, '2018-12-25 18:33:11', '2018-12-25 18:33:11'),
(313, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة دورة تدريبية', 0, NULL, '2018-12-25 18:36:28', '2018-12-25 18:36:28'),
(314, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة دورة تدريبية', 1, NULL, '2018-12-25 18:36:28', '2018-12-25 18:36:28'),
(315, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف دورة تدريبية', 0, NULL, '2018-12-25 18:36:29', '2018-12-25 18:36:29'),
(316, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف دورة تدريبية', 1, NULL, '2018-12-25 18:36:29', '2018-12-25 18:36:29'),
(317, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إنتداب جديد', 0, NULL, '2018-12-25 18:36:30', '2018-12-25 18:36:30'),
(318, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة إنتداب جديد', 1, NULL, '2018-12-25 18:36:30', '2018-12-25 18:36:30'),
(319, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف إنتداب', 0, NULL, '2018-12-25 18:36:30', '2018-12-25 18:36:30'),
(320, 12, 'hrm', '0021699961102', 1, 'لقد تم حذف إنتداب', 1, NULL, '2018-12-25 18:36:31', '2018-12-25 18:36:31'),
(321, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'توجد عندك مسائلة جديدة', 0, NULL, '2018-12-25 19:16:18', '2018-12-25 19:16:18'),
(322, 12, 'hrm', '0021699961102', 1, 'توجد عندك مسائلة جديدة', 1, NULL, '2018-12-25 19:16:18', '2018-12-25 19:16:18'),
(323, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'توجد عندك مسائلة جديدة', 0, NULL, '2018-12-25 19:35:20', '2018-12-25 19:35:20'),
(324, 12, 'hrm', '0021699961102', 1, 'توجد عندك مسائلة جديدة', 1, NULL, '2018-12-25 19:35:20', '2018-12-25 19:35:20'),
(325, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/31<br><a href=\'https://ccreation.sa/erp/meeting/39/31\'>https://ccreation.sa/erp/meeting/39/31</a>', 0, NULL, '2018-12-26 11:06:43', '2018-12-26 11:06:43'),
(326, 12, 'meetings', '00966541009675', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/31\n\nhttps://ccreation.sa/erp/meeting/39/31', 1, NULL, '2018-12-26 11:06:44', '2018-12-26 11:06:44'),
(327, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/20<br><a href=\'https://ccreation.sa/erp/meeting/39/20\'>https://ccreation.sa/erp/meeting/39/20</a>', 0, NULL, '2018-12-26 11:06:44', '2018-12-26 11:06:44'),
(328, 12, 'meetings', '00966563421359', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/20\n\nhttps://ccreation.sa/erp/meeting/39/20', 1, NULL, '2018-12-26 11:06:44', '2018-12-26 11:06:44'),
(329, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/22<br><a href=\'https://ccreation.sa/erp/meeting/39/22\'>https://ccreation.sa/erp/meeting/39/22</a>', 0, NULL, '2018-12-26 11:06:44', '2018-12-26 11:06:44'),
(330, 12, 'meetings', '00966502480256', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/22\n\nhttps://ccreation.sa/erp/meeting/39/22', 1, NULL, '2018-12-26 11:06:45', '2018-12-26 11:06:45'),
(331, 12, 'meetings', 'amidris@gmail.com', 0, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/46<br><a href=\'https://ccreation.sa/erp/meeting/39/46\'>https://ccreation.sa/erp/meeting/39/46</a>', 0, NULL, '2018-12-26 11:06:45', '2018-12-26 11:06:45'),
(332, 12, 'meetings', '00966507771868', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/46\n\nhttps://ccreation.sa/erp/meeting/39/46', 1, NULL, '2018-12-26 11:06:45', '2018-12-26 11:06:45'),
(333, 12, 'meetings', 'k.a.aljabr@gmail.com', 0, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد<br>لقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/49<br><a href=\'https://ccreation.sa/erp/meeting/39/49\'>https://ccreation.sa/erp/meeting/39/49</a>', 0, NULL, '2018-12-26 11:06:45', '2018-12-26 11:06:45'),
(334, 12, 'meetings', '00966505904881', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nلقد تم إنشاء إجتماع جديدhttps://ccreation.sa/erp/meeting/39/49\n\nhttps://ccreation.sa/erp/meeting/39/49', 1, NULL, '2018-12-26 11:06:45', '2018-12-26 11:06:45'),
(335, 12, 'meetings', '00966541009675', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/39/31', 1, NULL, '2018-12-26 11:08:51', '2018-12-26 11:08:51'),
(336, 12, 'meetings', '00966502480256', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/39/22', 1, NULL, '2018-12-26 11:08:51', '2018-12-26 11:08:51'),
(337, 12, 'meetings', '00966507771868', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/39/46', 1, NULL, '2018-12-26 11:08:51', '2018-12-26 11:08:51'),
(338, 12, 'meetings', '00966505904881', 1, 'الإجتماع : مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://ccreation.sa/erp/meeting/39/49', 1, NULL, '2018-12-26 11:08:51', '2018-12-26 11:08:51'),
(339, 12, 'hrm', 'bm@ccreation.sa', 0, 'لقد تم حذف موظف من النظام', 0, NULL, '2018-12-26 21:49:06', '2018-12-26 21:49:06'),
(340, 12, 'hrm', '00966544320099', 1, 'لقد تم حذف موظف من النظام', 1, NULL, '2018-12-26 21:49:06', '2018-12-26 21:49:06'),
(341, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'الإجتماع : hh<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://1click.ws/meeting/36/24\'>https://1click.ws/meeting/36/24</a>', 0, NULL, '2018-12-26 23:46:40', '2018-12-26 23:46:40'),
(342, 12, 'meetings', '0021699961102', 1, 'الإجتماع : hh\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://1click.ws/meeting/36/24', 1, NULL, '2018-12-26 23:46:40', '2018-12-26 23:46:40'),
(343, 12, 'hrm', 'tester@tester.com', 0, 'لقد تم حذف موظف من النظام', 0, NULL, '2018-12-27 04:24:42', '2018-12-27 04:24:42'),
(344, 12, 'hrm', '00966123456789', 1, 'لقد تم حذف موظف من النظام', 1, NULL, '2018-12-27 04:24:42', '2018-12-27 04:24:42'),
(345, 12, 'hrm', 'sa@ccreation.sa', 0, 'لقد تم رفض إجازة', 0, NULL, '2018-12-27 20:17:05', '2018-12-27 20:17:05'),
(346, 12, 'hrm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة موظف جديد', 0, NULL, '2018-12-27 21:46:04', '2018-12-27 21:46:04'),
(347, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:25:46', '2018-12-27 23:25:46'),
(348, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:25:46', '2018-12-27 23:25:46'),
(349, 12, 'hrm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:28:59', '2018-12-27 23:28:59'),
(350, 12, 'hrm', '0021699961102', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:28:59', '2018-12-27 23:28:59'),
(351, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:33:31', '2018-12-27 23:33:31'),
(352, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:33:31', '2018-12-27 23:33:31'),
(353, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/24<br><a href=\'https://1click.ws/meeting/40/24\'>https://1click.ws/meeting/40/24</a>', 0, NULL, '2018-12-27 23:39:29', '2018-12-27 23:39:29'),
(354, 12, 'meetings', '0021699961102', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/24\n\nhttps://1click.ws/meeting/40/24', 1, NULL, '2018-12-27 23:39:29', '2018-12-27 23:39:29'),
(355, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:29', '2018-12-27 23:39:29'),
(356, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:30', '2018-12-27 23:39:30'),
(357, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/32<br><a href=\'https://1click.ws/meeting/40/32\'>https://1click.ws/meeting/40/32</a>', 0, NULL, '2018-12-27 23:39:30', '2018-12-27 23:39:30'),
(358, 12, 'meetings', '00966509193274', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/32\n\nhttps://1click.ws/meeting/40/32', 1, NULL, '2018-12-27 23:39:30', '2018-12-27 23:39:30'),
(359, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:30', '2018-12-27 23:39:30'),
(360, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:31', '2018-12-27 23:39:31'),
(361, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/30<br><a href=\'https://1click.ws/meeting/40/30\'>https://1click.ws/meeting/40/30</a>', 0, NULL, '2018-12-27 23:39:31', '2018-12-27 23:39:31'),
(362, 12, 'meetings', '00966535779245', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/30\n\nhttps://1click.ws/meeting/40/30', 1, NULL, '2018-12-27 23:39:31', '2018-12-27 23:39:31'),
(363, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:31', '2018-12-27 23:39:31'),
(364, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(365, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/31<br><a href=\'https://1click.ws/meeting/40/31\'>https://1click.ws/meeting/40/31</a>', 0, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(366, 12, 'meetings', '00966541009675', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/31\n\nhttps://1click.ws/meeting/40/31', 1, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(367, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(368, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(369, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/27<br><a href=\'https://1click.ws/meeting/40/27\'>https://1click.ws/meeting/40/27</a>', 0, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(370, 12, 'meetings', '00966582983966', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/27\n\nhttps://1click.ws/meeting/40/27', 1, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(371, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(372, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(373, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/20<br><a href=\'https://1click.ws/meeting/40/20\'>https://1click.ws/meeting/40/20</a>', 0, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(374, 12, 'meetings', '00966563421359', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/20\n\nhttps://1click.ws/meeting/40/20', 1, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(375, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(376, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(377, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/22<br><a href=\'https://1click.ws/meeting/40/22\'>https://1click.ws/meeting/40/22</a>', 0, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(378, 12, 'meetings', '00966502480256', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/22\n\nhttps://1click.ws/meeting/40/22', 1, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(379, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(380, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:39:34', '2018-12-27 23:39:34'),
(381, 12, 'meetings', 'a@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/38<br><a href=\'https://1click.ws/meeting/40/38\'>https://1click.ws/meeting/40/38</a>', 0, NULL, '2018-12-27 23:39:34', '2018-12-27 23:39:34'),
(382, 12, 'meetings', '0000000000', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/40/38\n\nhttps://1click.ws/meeting/40/38', 0, 'Invalid Mobile Number', '2018-12-27 23:39:34', '2018-12-27 23:39:34'),
(383, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:39:34', '2018-12-27 23:39:34'),
(384, 12, 'meetings', '0000000000', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-27 23:39:34', '2018-12-27 23:39:34'),
(385, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:32', '2018-12-27 23:43:32'),
(386, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(387, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(388, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(389, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(390, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(391, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(392, 12, 'meetings', '00966541009675', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(393, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(394, 12, 'meetings', '00966582983966', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:34', '2018-12-27 23:43:34'),
(395, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:34', '2018-12-27 23:43:34'),
(396, 12, 'meetings', '00966563421359', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:34', '2018-12-27 23:43:34'),
(397, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:34', '2018-12-27 23:43:34'),
(398, 12, 'meetings', '00966502480256', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:43:35', '2018-12-27 23:43:35'),
(399, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:43:35', '2018-12-27 23:43:35'),
(400, 12, 'meetings', '0000000000', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-27 23:43:35', '2018-12-27 23:43:35'),
(401, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:49:25', '2018-12-27 23:49:25'),
(402, 12, 'meetings', '00966509193274', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:49:26', '2018-12-27 23:49:26'),
(403, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:49:26', '2018-12-27 23:49:26'),
(404, 12, 'meetings', '0021699961102', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(405, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(406, 12, 'meetings', '00966535779245', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(407, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(408, 12, 'meetings', '00966541009675', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(409, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(410, 12, 'meetings', '00966582983966', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(411, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2018-12-27 23:51:02', '2018-12-27 23:51:02'),
(412, 12, 'meetings', '0021699961102', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2018-12-27 23:51:02', '2018-12-27 23:51:02'),
(413, 12, 'meetings', '0021699961102', 1, 'الإجتماع : إجتماع تجريبي\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://1click.ws/meeting/40/24', 1, NULL, '2018-12-27 23:51:03', '2018-12-27 23:51:03'),
(414, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(415, 12, 'meetings', '0021699961102', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(416, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(417, 12, 'meetings', '00966509193274', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(418, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(419, 12, 'meetings', '00966535779245', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(420, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(421, 12, 'meetings', '00966541009675', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(422, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(423, 12, 'meetings', '00966582983966', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(424, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(425, 12, 'meetings', '00966563421359', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(426, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(427, 12, 'meetings', '00966502480256', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(428, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(429, 12, 'meetings', '0000000000', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(430, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:09', '2018-12-27 23:55:09'),
(431, 12, 'meetings', '0021699961102', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(432, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(433, 12, 'meetings', '00966509193274', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(434, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(435, 12, 'meetings', '00966535779245', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(436, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(437, 12, 'meetings', '00966541009675', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(438, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(439, 12, 'meetings', '00966582983966', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(440, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(441, 12, 'meetings', '00966563421359', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(442, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(443, 12, 'meetings', '00966502480256', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(444, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(445, 12, 'meetings', '0000000000', 1, 'لقد تم إلغاء إجتماع في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(446, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(447, 12, 'meetings', '0021699961102', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(448, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(449, 12, 'meetings', '00966509193274', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(450, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(451, 12, 'meetings', '00966535779245', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(452, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(453, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(454, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(455, 12, 'meetings', '00966582983966', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(456, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(457, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(458, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(459, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(460, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:56:55', '2018-12-27 23:56:55'),
(461, 12, 'meetings', '0000000000', 1, 'لقد تم تأجيل إجتماع في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-27 23:56:55', '2018-12-27 23:56:55'),
(462, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(463, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(464, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(465, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(466, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(467, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(468, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(469, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(470, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(471, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(472, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(473, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(474, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(475, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-27 23:58:55', '2018-12-27 23:58:55'),
(476, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-27 23:58:55', '2018-12-27 23:58:55'),
(477, 12, 'meetings', '0000000000', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-27 23:58:55', '2018-12-27 23:58:55'),
(478, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33');
INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(479, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(480, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(481, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(482, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(483, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(484, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(485, 12, 'meetings', '00966541009675', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(486, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(487, 12, 'meetings', '00966582983966', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(488, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(489, 12, 'meetings', '00966563421359', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(490, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(491, 12, 'meetings', '00966502480256', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(492, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(493, 12, 'meetings', '0000000000', 1, 'لقد تم تعديل محور في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:01:35', '2018-12-28 00:01:35'),
(494, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:21', '2018-12-28 00:02:21'),
(495, 12, 'meetings', '0021699961102', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:21', '2018-12-28 00:02:21'),
(496, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:21', '2018-12-28 00:02:21'),
(497, 12, 'meetings', '00966509193274', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:21', '2018-12-28 00:02:21'),
(498, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(499, 12, 'meetings', '00966535779245', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(500, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(501, 12, 'meetings', '00966541009675', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(502, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(503, 12, 'meetings', '00966582983966', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(504, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(505, 12, 'meetings', '00966563421359', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(506, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(507, 12, 'meetings', '00966502480256', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:23', '2018-12-28 00:02:23'),
(508, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:23', '2018-12-28 00:02:23'),
(509, 12, 'meetings', '0000000000', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:02:23', '2018-12-28 00:02:23'),
(510, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:59', '2018-12-28 00:02:59'),
(511, 12, 'meetings', '0021699961102', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:02:59', '2018-12-28 00:02:59'),
(512, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:02:59', '2018-12-28 00:02:59'),
(513, 12, 'meetings', '00966509193274', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(514, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(515, 12, 'meetings', '00966535779245', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(516, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(517, 12, 'meetings', '00966541009675', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(518, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(519, 12, 'meetings', '00966582983966', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(520, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(521, 12, 'meetings', '00966563421359', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:03:01', '2018-12-28 00:03:01'),
(522, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:03:01', '2018-12-28 00:03:01'),
(523, 12, 'meetings', '00966502480256', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:03:01', '2018-12-28 00:03:01'),
(524, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:03:01', '2018-12-28 00:03:01'),
(525, 12, 'meetings', '0000000000', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:03:01', '2018-12-28 00:03:01'),
(526, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(527, 12, 'meetings', '0021699961102', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(528, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(529, 12, 'meetings', '00966509193274', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(530, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(531, 12, 'meetings', '00966535779245', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(532, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(533, 12, 'meetings', '00966541009675', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(534, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(535, 12, 'meetings', '00966582983966', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(536, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(537, 12, 'meetings', '00966563421359', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(538, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(539, 12, 'meetings', '00966502480256', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:06:14', '2018-12-28 00:06:14'),
(540, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:06:14', '2018-12-28 00:06:14'),
(541, 12, 'meetings', '0000000000', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:06:14', '2018-12-28 00:06:14'),
(542, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:11', '2018-12-28 00:07:11'),
(543, 12, 'meetings', '00966509193274', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:11', '2018-12-28 00:07:11'),
(544, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(545, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(546, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(547, 12, 'meetings', '00966558037380', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(548, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(549, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(550, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(551, 12, 'meetings', '00966507187011', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(552, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(553, 12, 'meetings', '00966582983966', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(554, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(555, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(556, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(557, 12, 'meetings', '00966535779245', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(558, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(559, 12, 'meetings', '00966509193274', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(560, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(561, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(562, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(563, 12, 'meetings', '0021699961102', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(564, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(565, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(566, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(567, 12, 'meetings', '00966507187011', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(568, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(569, 12, 'meetings', '00966582983966', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(570, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(571, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(572, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(573, 12, 'meetings', '00966535779245', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:09:48', '2018-12-28 00:09:48'),
(574, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:01', '2018-12-28 00:11:01'),
(575, 12, 'meetings', '0021699961102', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:01', '2018-12-28 00:11:01'),
(576, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:01', '2018-12-28 00:11:01'),
(577, 12, 'meetings', '00966509193274', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(578, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(579, 12, 'meetings', '00966535779245', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(580, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(581, 12, 'meetings', '00966541009675', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(582, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(583, 12, 'meetings', '00966582983966', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(584, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(585, 12, 'meetings', '00966563421359', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:03', '2018-12-28 00:11:03'),
(586, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:03', '2018-12-28 00:11:03'),
(587, 12, 'meetings', '00966502480256', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:11:03', '2018-12-28 00:11:03'),
(588, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم حذف محور في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:11:03', '2018-12-28 00:11:03'),
(589, 12, 'meetings', '0000000000', 1, 'لقد تم حذف محور في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:11:03', '2018-12-28 00:11:03'),
(590, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(591, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(592, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(593, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(594, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(595, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(596, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(597, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(598, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(599, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(600, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(601, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(602, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(603, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(604, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(605, 12, 'meetings', '0000000000', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:12:14', '2018-12-28 00:12:14'),
(606, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(607, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(608, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(609, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(610, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(611, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(612, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(613, 12, 'meetings', '00966541009675', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(614, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(615, 12, 'meetings', '00966582983966', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(616, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(617, 12, 'meetings', '00966563421359', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(618, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(619, 12, 'meetings', '00966502480256', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(620, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(621, 12, 'meetings', '0000000000', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:12:47', '2018-12-28 00:12:47'),
(622, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(623, 12, 'meetings', '0021699961102', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(624, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(625, 12, 'meetings', '00966509193274', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(626, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(627, 12, 'meetings', '00966535779245', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(628, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(629, 12, 'meetings', '00966541009675', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(630, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(631, 12, 'meetings', '00966582983966', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(632, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(633, 12, 'meetings', '00966563421359', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(634, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(635, 12, 'meetings', '00966502480256', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(636, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(637, 12, 'meetings', '0000000000', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:13:24', '2018-12-28 00:13:24'),
(638, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:14', '2018-12-28 00:14:14'),
(639, 12, 'meetings', '0021699961102', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:14', '2018-12-28 00:14:14'),
(640, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:14', '2018-12-28 00:14:14'),
(641, 12, 'meetings', '00966509193274', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(642, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(643, 12, 'meetings', '00966535779245', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(644, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(645, 12, 'meetings', '00966541009675', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(646, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(647, 12, 'meetings', '00966582983966', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(648, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(649, 12, 'meetings', '00966563421359', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(650, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:16', '2018-12-28 00:14:16'),
(651, 12, 'meetings', '00966502480256', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:16', '2018-12-28 00:14:16'),
(652, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:16', '2018-12-28 00:14:16'),
(653, 12, 'meetings', '0000000000', 1, 'لقد تم حذف مهمة في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:14:16', '2018-12-28 00:14:16'),
(654, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(655, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(656, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(657, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(658, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(659, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(660, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(661, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(662, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(663, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(664, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(665, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(666, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(667, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 1, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(668, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, NULL, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(669, 12, 'meetings', '0000000000', 1, 'لقد تم إضافة ملف في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 00:14:45', '2018-12-28 00:14:45'),
(670, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 00:59:17', '2018-12-28 00:59:17'),
(671, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 00:59:17', '2018-12-28 00:59:17'),
(672, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:10:57', '2018-12-28 01:10:57'),
(673, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:10:57', '2018-12-28 01:10:57'),
(674, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:10:57', '2018-12-28 01:10:57'),
(675, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:10:57', '2018-12-28 01:10:57'),
(676, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:37:33', '2018-12-28 01:37:33'),
(677, 12, 'prm', '0021699961102', 1, 'لقد تم تعديل مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:37:33', '2018-12-28 01:37:33'),
(678, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم تعديل مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:37:33', '2018-12-28 01:37:33'),
(679, 12, 'prm', '00966563421359', 1, 'لقد تم تعديل مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:37:33', '2018-12-28 01:37:33'),
(680, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:39:56', '2018-12-28 01:39:56'),
(681, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:39:57', '2018-12-28 01:39:57'),
(682, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:39:57', '2018-12-28 01:39:57'),
(683, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:39:57', '2018-12-28 01:39:57'),
(684, 12, 'prm', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:39:57', '2018-12-28 01:39:57'),
(685, 12, 'prm', '00966582983966', 1, 'لقد تم إضافة مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:39:57', '2018-12-28 01:39:57'),
(686, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:40:38', '2018-12-28 01:40:38'),
(687, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:40:38', '2018-12-28 01:40:38'),
(688, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:40:38', '2018-12-28 01:40:38'),
(689, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:40:39', '2018-12-28 01:40:39'),
(690, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:43:01', '2018-12-28 01:43:01'),
(691, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:43:01', '2018-12-28 01:43:01'),
(692, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:43:01', '2018-12-28 01:43:01'),
(693, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مساعد في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:43:02', '2018-12-28 01:43:02'),
(694, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:43:39', '2018-12-28 01:43:39'),
(695, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:43:39', '2018-12-28 01:43:39'),
(696, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:43:39', '2018-12-28 01:43:39'),
(697, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:43:40', '2018-12-28 01:43:40'),
(698, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:44:58', '2018-12-28 01:44:58'),
(699, 12, 'prm', '00966563421359', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:44:59', '2018-12-28 01:44:59'),
(700, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:44:59', '2018-12-28 01:44:59'),
(701, 12, 'prm', '0021699961102', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:44:59', '2018-12-28 01:44:59'),
(702, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:45:43', '2018-12-28 01:45:43'),
(703, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:45:43', '2018-12-28 01:45:43'),
(704, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:45:43', '2018-12-28 01:45:43'),
(705, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:45:43', '2018-12-28 01:45:43'),
(706, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:47:50', '2018-12-28 01:47:50'),
(707, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:47:50', '2018-12-28 01:47:50'),
(708, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:47:50', '2018-12-28 01:47:50'),
(709, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:47:50', '2018-12-28 01:47:50'),
(710, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مرحلة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:48:23', '2018-12-28 01:48:23'),
(711, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مرحلة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:48:24', '2018-12-28 01:48:24'),
(712, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مرحلة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:48:24', '2018-12-28 01:48:24'),
(713, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مرحلة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:48:24', '2018-12-28 01:48:24'),
(714, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مرحلة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:48:48', '2018-12-28 01:48:48'),
(715, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مرحلة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:48:48', '2018-12-28 01:48:48'),
(716, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مرحلة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:48:48', '2018-12-28 01:48:48'),
(717, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مرحلة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:48:49', '2018-12-28 01:48:49'),
(718, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة نقاش في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:50:35', '2018-12-28 01:50:35'),
(719, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة نقاش في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:50:35', '2018-12-28 01:50:35'),
(720, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة نقاش في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:50:35', '2018-12-28 01:50:35'),
(721, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة نقاش في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:50:36', '2018-12-28 01:50:36'),
(722, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(723, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(724, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(725, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(726, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(727, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(728, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(729, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة ملف في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(730, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:07', '2018-12-28 02:19:07'),
(731, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(732, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(733, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(734, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(735, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(736, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(737, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(738, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(739, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(740, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(741, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(742, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(743, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(744, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(745, 12, 'meetings', '0000000000', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2018-12-28 02:19:10', '2018-12-28 02:19:10'),
(746, 12, 'settings', '00966563421359', 1, 'هذه رسالة تجربة من ERP', 0, 'Invalid UserName and Password', '2018-12-28 02:20:59', '2018-12-28 02:20:59'),
(747, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2018-12-28 02:22:51', '2018-12-28 02:22:51'),
(748, 12, 'meetings', '0021699961102', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, 'Invalid UserName and Password', '2018-12-28 02:22:52', '2018-12-28 02:22:52'),
(749, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'الإجتماع : إجتماع تجريبي<br>هذه رسالة للتذكير بالإجتماع لجميع المدعويين<br><a href=\'https://1click.ws/meeting/40/24\'>https://1click.ws/meeting/40/24</a>', 0, NULL, '2018-12-28 02:22:52', '2018-12-28 02:22:52'),
(750, 12, 'meetings', '0021699961102', 1, 'الإجتماع : إجتماع تجريبي\nهذه رسالة للتذكير بالإجتماع لجميع المدعويين\n\nhttps://1click.ws/meeting/40/24', 0, 'Invalid UserName and Password', '2018-12-28 02:22:52', '2018-12-28 02:22:52'),
(751, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 0, 'Invalid UserName and Password', '2018-12-28 02:36:46', '2018-12-28 02:36:46'),
(752, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2018-12-28 02:41:37', '2018-12-28 02:41:37'),
(753, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(754, 12, 'passwords', '00966509193274', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(755, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(756, 12, 'passwords', '00966535779245', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(757, 12, 'passwords', 'rasha@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(758, 12, 'passwords', '00966541009675', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(759, 12, 'passwords', 'ismail@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(760, 12, 'passwords', '00966582983966', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(761, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(762, 12, 'passwords', '0021699961102', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(763, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(764, 12, 'passwords', '00966599464878', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(765, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(766, 12, 'passwords', '00966507187011', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(767, 12, 'passwords', 'sa@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(768, 12, 'passwords', '00966563421359', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(769, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(770, 12, 'passwords', '00966502480256', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(771, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(772, 12, 'passwords', '00966558037380', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(773, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(774, 12, 'passwords', '00966544320099', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(775, 12, 'passwords', 'amidris@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(776, 12, 'passwords', '00966507771868', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(777, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:10', '2018-12-28 05:00:10'),
(778, 12, 'passwords', '00966509193274', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(779, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(780, 12, 'passwords', '00966535779245', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(781, 12, 'passwords', 'rasha@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(782, 12, 'passwords', '00966541009675', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(783, 12, 'passwords', 'ismail@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(784, 12, 'passwords', '00966582983966', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(785, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(786, 12, 'passwords', '0021699961102', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(787, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(788, 12, 'passwords', '00966599464878', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(789, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(790, 12, 'passwords', '00966507187011', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(791, 12, 'passwords', 'sa@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(792, 12, 'passwords', '00966563421359', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(793, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(794, 12, 'passwords', '00966502480256', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(795, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(796, 12, 'passwords', '00966558037380', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(797, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(798, 12, 'passwords', '00966544320099', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(799, 12, 'passwords', 'amidris@gmail.com', 0, 'لقد تم إضافة موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(800, 12, 'passwords', '00966507771868', 1, 'لقد تم إضافة موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:00:14', '2018-12-28 05:00:14'),
(801, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(802, 12, 'passwords', '00966509193274', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(803, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(804, 12, 'passwords', '00966535779245', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(805, 12, 'passwords', 'rasha@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(806, 12, 'passwords', '00966541009675', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(807, 12, 'passwords', 'ismail@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(808, 12, 'passwords', '00966582983966', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(809, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(810, 12, 'passwords', '0021699961102', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(811, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(812, 12, 'passwords', '00966599464878', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(813, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(814, 12, 'passwords', '00966507187011', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(815, 12, 'passwords', 'sa@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(816, 12, 'passwords', '00966563421359', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(817, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(818, 12, 'passwords', '00966502480256', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(819, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(820, 12, 'passwords', '00966558037380', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(821, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(822, 12, 'passwords', '00966544320099', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(823, 12, 'passwords', 'amidris@gmail.com', 0, 'لقد تم تعديل موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(824, 12, 'passwords', '00966507771868', 1, 'لقد تم تعديل موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(825, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(826, 12, 'passwords', '00966509193274', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(827, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(828, 12, 'passwords', '00966535779245', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(829, 12, 'passwords', 'rasha@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:37', '2018-12-28 05:03:37');
INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(830, 12, 'passwords', '00966541009675', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(831, 12, 'passwords', 'ismail@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(832, 12, 'passwords', '00966582983966', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(833, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(834, 12, 'passwords', '0021699961102', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(835, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(836, 12, 'passwords', '00966599464878', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(837, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(838, 12, 'passwords', '00966507187011', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(839, 12, 'passwords', 'sa@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(840, 12, 'passwords', '00966563421359', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(841, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(842, 12, 'passwords', '00966502480256', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(843, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(844, 12, 'passwords', '00966558037380', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(845, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(846, 12, 'passwords', '00966544320099', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:40', '2018-12-28 05:03:40'),
(847, 12, 'passwords', 'amidris@gmail.com', 0, 'لقد تم حذف موقع في نظام باسوورداتي', 0, NULL, '2018-12-28 05:03:40', '2018-12-28 05:03:40'),
(848, 12, 'passwords', '00966507771868', 1, 'لقد تم حذف موقع في نظام باسوورداتي', 1, NULL, '2018-12-28 05:03:40', '2018-12-28 05:03:40'),
(849, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:12', '2018-12-28 19:42:12'),
(850, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(851, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(852, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(853, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(854, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(855, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(856, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(857, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(858, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(859, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(860, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(861, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(862, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(863, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(864, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مشروع جديد في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:42:15', '2018-12-28 19:42:15'),
(865, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:52', '2018-12-28 19:52:52'),
(866, 12, 'prm', '00966544320099', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(867, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(868, 12, 'prm', '00966502480256', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(869, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(870, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(871, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(872, 12, 'prm', '00966507187011', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(873, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(874, 12, 'prm', '00966541009675', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(875, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(876, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(877, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(878, 12, 'prm', '00966544320099', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(879, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(880, 12, 'prm', '00966509193274', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(881, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:58', '2018-12-28 19:52:58'),
(882, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(883, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(884, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(885, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(886, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(887, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(888, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(889, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(890, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(891, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(892, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(893, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(894, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(895, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(896, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(897, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:23', '2018-12-28 19:57:23'),
(898, 12, 'prm', '00966544320099', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(899, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(900, 12, 'prm', '00966502480256', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(901, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(902, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(903, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(904, 12, 'prm', '00966507187011', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(905, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(906, 12, 'prm', '00966541009675', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(907, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(908, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(909, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(910, 12, 'prm', '00966544320099', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(911, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(912, 12, 'prm', '00966509193274', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(913, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:28', '2018-12-28 19:57:28'),
(914, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(915, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(916, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(917, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(918, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(919, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(920, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(921, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(922, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(923, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(924, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(925, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(926, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(927, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(928, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(929, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:53', '2018-12-28 20:28:53'),
(930, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(931, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(932, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(933, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(934, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(935, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(936, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(937, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(938, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(939, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(940, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(941, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(942, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(943, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(944, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(945, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/32<br><a href=\'https://1click.ws/meeting/41/32\'>https://1click.ws/meeting/41/32</a>', 0, NULL, '2018-12-29 19:24:15', '2018-12-29 19:24:15'),
(946, 12, 'meetings', '00966509193274', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/32\n\nhttps://1click.ws/meeting/41/32', 1, NULL, '2018-12-29 19:24:15', '2018-12-29 19:24:15'),
(947, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:15', '2018-12-29 19:24:15'),
(948, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:15', '2018-12-29 19:24:15'),
(949, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/20<br><a href=\'https://1click.ws/meeting/41/20\'>https://1click.ws/meeting/41/20</a>', 0, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(950, 12, 'meetings', '00966563421359', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/20\n\nhttps://1click.ws/meeting/41/20', 1, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(951, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(952, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(953, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/30<br><a href=\'https://1click.ws/meeting/41/30\'>https://1click.ws/meeting/41/30</a>', 0, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(954, 12, 'meetings', '00966535779245', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/30\n\nhttps://1click.ws/meeting/41/30', 1, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(955, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(956, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(957, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/31<br><a href=\'https://1click.ws/meeting/41/31\'>https://1click.ws/meeting/41/31</a>', 0, NULL, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(958, 12, 'meetings', '00966541009675', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/31\n\nhttps://1click.ws/meeting/41/31', 1, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(959, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(960, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(961, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/27<br><a href=\'https://1click.ws/meeting/41/27\'>https://1click.ws/meeting/41/27</a>', 0, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(962, 12, 'meetings', '00966582983966', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/27\n\nhttps://1click.ws/meeting/41/27', 1, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(963, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(964, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(965, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/28<br><a href=\'https://1click.ws/meeting/41/28\'>https://1click.ws/meeting/41/28</a>', 0, NULL, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(966, 12, 'meetings', '00966507187011', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/28\n\nhttps://1click.ws/meeting/41/28', 1, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(967, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(968, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(969, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/20<br><a href=\'https://1click.ws/meeting/41/20\'>https://1click.ws/meeting/41/20</a>', 0, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(970, 12, 'meetings', '00966563421359', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/20\n\nhttps://1click.ws/meeting/41/20', 1, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(971, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(972, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(973, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/22<br><a href=\'https://1click.ws/meeting/41/22\'>https://1click.ws/meeting/41/22</a>', 0, NULL, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(974, 12, 'meetings', '00966502480256', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/22\n\nhttps://1click.ws/meeting/41/22', 1, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(975, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(976, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(977, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/23<br><a href=\'https://1click.ws/meeting/41/23\'>https://1click.ws/meeting/41/23</a>', 0, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(978, 12, 'meetings', '00966558037380', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/23\n\nhttps://1click.ws/meeting/41/23', 1, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(979, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(980, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(981, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/50<br><a href=\'https://1click.ws/meeting/41/50\'>https://1click.ws/meeting/41/50</a>', 0, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(982, 12, 'meetings', '00966544320099', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/50\n\nhttps://1click.ws/meeting/41/50', 1, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(983, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(984, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:20', '2018-12-29 19:24:20'),
(985, 12, 'meetings', 'amidris@gmail.com', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/46<br><a href=\'https://1click.ws/meeting/41/46\'>https://1click.ws/meeting/41/46</a>', 0, NULL, '2018-12-29 19:24:20', '2018-12-29 19:24:20'),
(986, 12, 'meetings', '00966507771868', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/41/46\n\nhttps://1click.ws/meeting/41/46', 1, NULL, '2018-12-29 19:24:20', '2018-12-29 19:24:20'),
(987, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 19:24:20', '2018-12-29 19:24:20'),
(988, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 19:24:20', '2018-12-29 19:24:20'),
(989, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/32<br><a href=\'https://1click.ws/meeting/42/32\'>https://1click.ws/meeting/42/32</a>', 0, NULL, '2018-12-29 20:38:10', '2018-12-29 20:38:10'),
(990, 12, 'meetings', '00966509193274', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/32\n\nhttps://1click.ws/meeting/42/32', 1, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(991, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(992, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(993, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/20<br><a href=\'https://1click.ws/meeting/42/20\'>https://1click.ws/meeting/42/20</a>', 0, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(994, 12, 'meetings', '00966563421359', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/20\n\nhttps://1click.ws/meeting/42/20', 1, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(995, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(996, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(997, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/30<br><a href=\'https://1click.ws/meeting/42/30\'>https://1click.ws/meeting/42/30</a>', 0, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(998, 12, 'meetings', '00966535779245', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/30\n\nhttps://1click.ws/meeting/42/30', 1, NULL, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(999, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1000, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1001, 12, 'meetings', 'rasha@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/31<br><a href=\'https://1click.ws/meeting/42/31\'>https://1click.ws/meeting/42/31</a>', 0, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1002, 12, 'meetings', '00966541009675', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/31\n\nhttps://1click.ws/meeting/42/31', 1, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1003, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1004, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1005, 12, 'meetings', 'ismail@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/27<br><a href=\'https://1click.ws/meeting/42/27\'>https://1click.ws/meeting/42/27</a>', 0, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1006, 12, 'meetings', '00966582983966', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/27\n\nhttps://1click.ws/meeting/42/27', 1, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1007, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(1008, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1009, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/28<br><a href=\'https://1click.ws/meeting/42/28\'>https://1click.ws/meeting/42/28</a>', 0, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1010, 12, 'meetings', '00966507187011', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/28\n\nhttps://1click.ws/meeting/42/28', 1, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1011, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1012, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1013, 12, 'meetings', 'sa@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/20<br><a href=\'https://1click.ws/meeting/42/20\'>https://1click.ws/meeting/42/20</a>', 0, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1014, 12, 'meetings', '00966563421359', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/20\n\nhttps://1click.ws/meeting/42/20', 1, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1015, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(1016, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1017, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/22<br><a href=\'https://1click.ws/meeting/42/22\'>https://1click.ws/meeting/42/22</a>', 0, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1018, 12, 'meetings', '00966502480256', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/22\n\nhttps://1click.ws/meeting/42/22', 1, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1019, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1020, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1021, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/23<br><a href=\'https://1click.ws/meeting/42/23\'>https://1click.ws/meeting/42/23</a>', 0, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1022, 12, 'meetings', '00966558037380', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/23\n\nhttps://1click.ws/meeting/42/23', 1, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1023, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(1024, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1025, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/50<br><a href=\'https://1click.ws/meeting/42/50\'>https://1click.ws/meeting/42/50</a>', 0, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1026, 12, 'meetings', '00966544320099', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/50\n\nhttps://1click.ws/meeting/42/50', 1, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1027, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1028, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1029, 12, 'meetings', 'amidris@gmail.com', 0, 'الإجتماع : العاشر - متابعات<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/46<br><a href=\'https://1click.ws/meeting/42/46\'>https://1click.ws/meeting/42/46</a>', 0, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1030, 12, 'meetings', '00966507771868', 1, 'الإجتماع : العاشر - متابعات\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/42/46\n\nhttps://1click.ws/meeting/42/46', 1, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1031, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1032, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(1033, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:19', '2018-12-29 20:42:19'),
(1034, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1035, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1036, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1037, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1038, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1039, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1040, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1041, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(1042, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1043, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1044, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1045, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1046, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1047, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1048, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1049, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(1050, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(1051, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(1052, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(1053, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(1054, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(1055, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1056, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1057, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1058, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1059, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1060, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1061, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(1062, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1063, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1064, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1065, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1066, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1067, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1068, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1069, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(1070, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1071, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1072, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1073, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1074, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1075, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1076, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(1077, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(1078, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(1079, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(1080, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(1081, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(1082, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1083, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1084, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1085, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1086, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1087, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1088, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(1089, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:33', '2018-12-29 20:43:33'),
(1090, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:33', '2018-12-29 20:43:33'),
(1091, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:33', '2018-12-29 20:43:33'),
(1092, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:33', '2018-12-29 20:43:33'),
(1093, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:33', '2018-12-29 20:43:33'),
(1094, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(1095, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(1096, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(1097, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(1098, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(1099, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1100, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1101, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1102, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1103, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1104, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1105, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(1106, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1107, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1108, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1109, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1110, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1111, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1112, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1113, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(1114, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1115, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1116, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1117, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1118, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1119, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1120, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(1121, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(1122, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(1123, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(1124, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(1125, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(1126, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1127, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1128, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1129, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1130, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1131, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1132, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1133, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(1134, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(1135, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(1136, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(1137, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(1138, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(1139, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(1140, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:02', '2018-12-29 20:45:02'),
(1141, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:45:03', '2018-12-29 20:45:03');
INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(1142, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:45:03', '2018-12-29 20:45:03'),
(1143, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:55', '2018-12-29 20:54:55'),
(1144, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:55', '2018-12-29 20:54:55'),
(1145, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:55', '2018-12-29 20:54:55'),
(1146, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(1147, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(1148, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(1149, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(1150, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(1151, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(1152, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1153, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1154, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1155, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1156, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1157, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1158, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1159, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(1160, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(1161, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(1162, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(1163, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 0, NULL, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(1164, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة محور جديد في نظام إجتماعاتي', 1, NULL, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(1165, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:40', '2018-12-29 21:17:40'),
(1166, 12, 'meetings', '00966509193274', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1167, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1168, 12, 'meetings', '00966563421359', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1169, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1170, 12, 'meetings', '00966582983966', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1171, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1172, 12, 'meetings', '00966541009675', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1173, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(1174, 12, 'meetings', '00966535779245', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1175, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1176, 12, 'meetings', '00966507187011', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1177, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1178, 12, 'meetings', '00966563421359', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1179, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1180, 12, 'meetings', '00966502480256', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1181, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(1182, 12, 'meetings', '00966558037380', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(1183, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(1184, 12, 'meetings', '00966544320099', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(1185, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(1186, 12, 'meetings', '00966507771868', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(1187, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:07', '2018-12-29 21:18:07'),
(1188, 12, 'meetings', '00966509193274', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1189, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1190, 12, 'meetings', '00966563421359', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1191, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1192, 12, 'meetings', '00966582983966', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1193, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1194, 12, 'meetings', '00966541009675', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1195, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(1196, 12, 'meetings', '00966535779245', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1197, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1198, 12, 'meetings', '00966507187011', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1199, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1200, 12, 'meetings', '00966563421359', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1201, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1202, 12, 'meetings', '00966502480256', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1203, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(1204, 12, 'meetings', '00966558037380', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(1205, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(1206, 12, 'meetings', '00966544320099', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(1207, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(1208, 12, 'meetings', '00966507771868', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(1209, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:48', '2018-12-29 21:27:48'),
(1210, 12, 'meetings', '00966509193274', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1211, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1212, 12, 'meetings', '00966563421359', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1213, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1214, 12, 'meetings', '00966582983966', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1215, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1216, 12, 'meetings', '00966541009675', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1217, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(1218, 12, 'meetings', '00966535779245', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1219, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1220, 12, 'meetings', '00966507187011', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1221, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1222, 12, 'meetings', '00966563421359', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1223, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1224, 12, 'meetings', '00966502480256', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1225, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(1226, 12, 'meetings', '00966558037380', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(1227, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(1228, 12, 'meetings', '00966544320099', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(1229, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(1230, 12, 'meetings', '00966507771868', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(1231, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(1232, 12, 'meetings', '00966509193274', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(1233, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(1234, 12, 'meetings', '00966563421359', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(1235, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(1236, 12, 'meetings', '00966582983966', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(1237, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(1238, 12, 'meetings', '00966541009675', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(1239, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(1240, 12, 'meetings', '00966535779245', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(1241, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(1242, 12, 'meetings', '00966507187011', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1243, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1244, 12, 'meetings', '00966563421359', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1245, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1246, 12, 'meetings', '00966502480256', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1247, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1248, 12, 'meetings', '00966558037380', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1249, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(1250, 12, 'meetings', '00966544320099', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:07', '2018-12-29 21:28:07'),
(1251, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم بدء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:28:07', '2018-12-29 21:28:07'),
(1252, 12, 'meetings', '00966507771868', 1, 'لقد تم بدء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:28:07', '2018-12-29 21:28:07'),
(1253, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:34', '2018-12-29 21:33:34'),
(1254, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1255, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1256, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1257, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1258, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1259, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1260, 12, 'meetings', '00966507187011', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1261, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(1262, 12, 'meetings', '00966507771868', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:36', '2018-12-29 21:33:36'),
(1263, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(1264, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(1265, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(1266, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(1267, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(1268, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(1269, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(1270, 12, 'meetings', '00966507187011', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(1271, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(1272, 12, 'meetings', '00966507771868', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(1273, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(1274, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(1275, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(1276, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(1277, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(1278, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(1279, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(1280, 12, 'meetings', '00966507187011', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(1281, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(1282, 12, 'meetings', '00966507771868', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(1283, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1284, 12, 'meetings', '00966541009675', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1285, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1286, 12, 'meetings', '00966563421359', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1287, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1288, 12, 'meetings', '00966502480256', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1289, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(1290, 12, 'meetings', '00966507187011', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:35', '2018-12-29 21:34:35'),
(1291, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم تأجيل محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:35', '2018-12-29 21:34:35'),
(1292, 12, 'meetings', '00966507771868', 1, 'لقد تم تأجيل محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:35', '2018-12-29 21:34:35'),
(1293, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1294, 12, 'meetings', '00966509193274', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1295, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1296, 12, 'meetings', '00966563421359', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1297, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1298, 12, 'meetings', '00966582983966', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1299, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(1300, 12, 'meetings', '00966541009675', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1301, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1302, 12, 'meetings', '00966535779245', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1303, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1304, 12, 'meetings', '00966507187011', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1305, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1306, 12, 'meetings', '00966563421359', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1307, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1308, 12, 'meetings', '00966502480256', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(1309, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(1310, 12, 'meetings', '00966558037380', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(1311, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(1312, 12, 'meetings', '00966544320099', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(1313, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إنهاء محور في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(1314, 12, 'meetings', '00966507771868', 1, 'لقد تم إنهاء محور في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(1315, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1316, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1317, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1318, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1319, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1320, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1321, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(1322, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1323, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1324, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1325, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1326, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1327, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1328, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1329, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(1330, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1331, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1332, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1333, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1334, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1335, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1336, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(1337, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(1338, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(1339, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(1340, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(1341, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1342, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1343, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1344, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1345, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1346, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1347, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1348, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1349, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(1350, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1351, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1352, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1353, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1354, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1355, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1356, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1357, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(1358, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:38:28', '2018-12-29 21:38:28'),
(1359, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:39:58', '2018-12-29 21:39:58'),
(1360, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:39:58', '2018-12-29 21:39:58'),
(1361, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:39:58', '2018-12-29 21:39:58'),
(1362, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1363, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1364, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1365, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1366, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1367, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1368, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1369, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(1370, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1371, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1372, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1373, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1374, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1375, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1376, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1377, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1378, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(1379, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:01', '2018-12-29 21:40:01'),
(1380, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:01', '2018-12-29 21:40:01'),
(1381, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:53', '2018-12-29 21:40:53'),
(1382, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1383, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1384, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1385, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1386, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1387, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1388, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1389, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(1390, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1391, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1392, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1393, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1394, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1395, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1396, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1397, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(1398, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(1399, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(1400, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(1401, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(1402, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(1403, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:50', '2018-12-29 21:41:50'),
(1404, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:50', '2018-12-29 21:41:50'),
(1405, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:50', '2018-12-29 21:41:50'),
(1406, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1407, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1408, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1409, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1410, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1411, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1412, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1413, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(1414, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1415, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1416, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1417, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1418, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1419, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1420, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1421, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(1422, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:53', '2018-12-29 21:41:53'),
(1423, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:41:53', '2018-12-29 21:41:53'),
(1424, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:41:53', '2018-12-29 21:41:53'),
(1425, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1426, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1427, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1428, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1429, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1430, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1431, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(1432, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1433, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1434, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1435, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1436, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1437, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1438, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1439, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(1440, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1441, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1442, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1443, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1444, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1445, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1446, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(1447, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1448, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1449, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1450, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1451, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1452, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1453, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(1454, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1455, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1456, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1457, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1458, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1459, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1460, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1461, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(1462, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1463, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1464, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1465, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1466, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1467, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1468, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(1469, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:49', '2018-12-29 21:44:49'),
(1470, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1471, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1472, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1473, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1474, 12, 'meetings', '00966582983966', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1475, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1476, 12, 'meetings', '00966541009675', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1477, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(1478, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1479, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1480, 12, 'meetings', '00966507187011', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1481, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1482, 12, 'meetings', '00966563421359', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1483, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1484, 12, 'meetings', '00966502480256', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1485, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(1486, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(1487, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(1488, 12, 'meetings', '00966544320099', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(1489, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(1490, 12, 'meetings', '00966507771868', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(1491, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(1492, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(1493, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(1494, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(1495, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(1496, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(1497, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(1498, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:29', '2018-12-31 19:07:29');
INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(1499, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(1500, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(1501, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(1502, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(1503, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(1504, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(1505, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(1506, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(1507, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(1508, 12, 'prm', '00966544320099', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(1509, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(1510, 12, 'prm', '00966502480256', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(1511, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(1512, 12, 'prm', '0021699961102', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1513, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1514, 12, 'prm', '00966507187011', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1515, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1516, 12, 'prm', '00966541009675', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1517, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1518, 12, 'prm', '00966563421359', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1519, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(1520, 12, 'prm', '00966544320099', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:29', '2018-12-31 19:11:29'),
(1521, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:11:29', '2018-12-31 19:11:29'),
(1522, 12, 'prm', '00966509193274', 1, 'لقد تم حذف مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:11:29', '2018-12-31 19:11:29'),
(1523, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(1524, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(1525, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(1526, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(1527, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(1528, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1529, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1530, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1531, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1532, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1533, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1534, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1535, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1536, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(1537, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:12:50', '2018-12-31 19:12:50'),
(1538, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:12:50', '2018-12-31 19:12:50'),
(1539, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(1540, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(1541, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(1542, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(1543, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(1544, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1545, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1546, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1547, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1548, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1549, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1550, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1551, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(1552, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:23', '2018-12-31 19:18:23'),
(1553, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:18:23', '2018-12-31 19:18:23'),
(1554, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:18:23', '2018-12-31 19:18:23'),
(1555, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(1556, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(1557, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(1558, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(1559, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(1560, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1561, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1562, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1563, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1564, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1565, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1566, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1567, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(1568, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:17', '2018-12-31 19:21:17'),
(1569, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:21:17', '2018-12-31 19:21:17'),
(1570, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:21:17', '2018-12-31 19:21:17'),
(1571, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:57', '2018-12-31 19:23:57'),
(1572, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:57', '2018-12-31 19:23:57'),
(1573, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:57', '2018-12-31 19:23:57'),
(1574, 12, 'prm', '00966502480256', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1575, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1576, 12, 'prm', '0021699961102', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1577, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1578, 12, 'prm', '00966507187011', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1579, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1580, 12, 'prm', '00966541009675', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1581, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(1582, 12, 'prm', '00966563421359', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(1583, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(1584, 12, 'prm', '00966544320099', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(1585, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(1586, 12, 'prm', '00966509193274', 1, 'لقد تم إضافة مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(1587, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:30', '2018-12-31 19:27:30'),
(1588, 12, 'prm', '00966544320099', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1589, 12, 'prm', 'alsalo@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1590, 12, 'prm', '00966502480256', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1591, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1592, 12, 'prm', '0021699961102', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1593, 12, 'prm', 'Tahania@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1594, 12, 'prm', '00966507187011', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1595, 12, 'prm', 'rasha@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1596, 12, 'prm', '00966541009675', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(1597, 12, 'prm', 'sa@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(1598, 12, 'prm', '00966563421359', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(1599, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(1600, 12, 'prm', '00966544320099', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(1601, 12, 'prm', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 0, NULL, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(1602, 12, 'prm', '00966509193274', 1, 'لقد تم تعديل مهمة في مشروع في نظام إدارة المشاريع', 1, NULL, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(1603, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:24', '2019-01-02 04:34:24'),
(1604, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:24', '2019-01-02 04:34:24'),
(1605, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:24', '2019-01-02 04:34:24'),
(1606, 12, 'meetings', '00966563421359', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1607, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1608, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1609, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1610, 12, 'meetings', '00966541009675', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1611, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1612, 12, 'meetings', '00966582983966', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1613, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(1614, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1615, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1616, 12, 'meetings', '00966507187011', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1617, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1618, 12, 'meetings', '00966563421359', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1619, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1620, 12, 'meetings', '00966502480256', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1621, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1622, 12, 'meetings', '00966558037380', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(1623, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:27', '2019-01-02 04:34:27'),
(1624, 12, 'meetings', '00966544320099', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:27', '2019-01-02 04:34:27'),
(1625, 12, 'meetings', 'amidris@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:34:27', '2019-01-02 04:34:27'),
(1626, 12, 'meetings', '00966507771868', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:34:27', '2019-01-02 04:34:27'),
(1627, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:04', '2019-01-02 04:36:04'),
(1628, 12, 'meetings', '0021699961102', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1629, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1630, 12, 'meetings', '00966509193274', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1631, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1632, 12, 'meetings', '00966535779245', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1633, 12, 'meetings', 'rasha@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1634, 12, 'meetings', '00966541009675', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1635, 12, 'meetings', 'ismail@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(1636, 12, 'meetings', '00966582983966', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1637, 12, 'meetings', 'sa@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1638, 12, 'meetings', '00966563421359', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1639, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1640, 12, 'meetings', '00966502480256', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1641, 12, 'meetings', 'a@ccreation.sa', 0, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1642, 12, 'meetings', '0000000000', 1, 'لقد تم حذف إجتماع في نظام إجتماعاتي', 0, 'Invalid Mobile Number', '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(1643, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/24<br><a href=\'https://1click.ws/meeting/43/24\'>https://1click.ws/meeting/43/24</a>', 0, NULL, '2019-01-02 04:36:43', '2019-01-02 04:36:43'),
(1644, 12, 'meetings', '0021699961102', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/24\n\nhttps://1click.ws/meeting/43/24', 1, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1645, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1646, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1647, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/23<br><a href=\'https://1click.ws/meeting/43/23\'>https://1click.ws/meeting/43/23</a>', 0, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1648, 12, 'meetings', '00966558037380', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/23\n\nhttps://1click.ws/meeting/43/23', 1, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1649, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1650, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1651, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/32<br><a href=\'https://1click.ws/meeting/43/32\'>https://1click.ws/meeting/43/32</a>', 0, NULL, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(1652, 12, 'meetings', '00966509193274', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/32\n\nhttps://1click.ws/meeting/43/32', 1, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1653, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1654, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1655, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : إجتماع تجريبي<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/30<br><a href=\'https://1click.ws/meeting/43/30\'>https://1click.ws/meeting/43/30</a>', 0, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1656, 12, 'meetings', '00966535779245', 1, 'الإجتماع : إجتماع تجريبي\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/43/30\n\nhttps://1click.ws/meeting/43/30', 1, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1657, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1658, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة إجتماع جديد في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(1659, 12, 'hrm', 'test@test.com', 0, 'لقد تم إضافة موظف جديد', 0, NULL, '2019-01-02 04:38:55', '2019-01-02 04:38:55'),
(1660, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1661, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1662, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1663, 12, 'meetings', '00966558037380', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1664, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1665, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1666, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(1667, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:04', '2019-01-02 04:41:04'),
(1668, 12, 'meetings', 'test@test.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:04', '2019-01-02 04:41:04'),
(1669, 12, 'meetings', '0096699961102', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:04', '2019-01-02 04:41:04'),
(1670, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:48', '2019-01-02 04:41:48'),
(1671, 12, 'meetings', '0021699961102', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:52', '2019-01-02 04:41:52'),
(1672, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:52', '2019-01-02 04:41:52'),
(1673, 12, 'meetings', '00966558037380', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:52', '2019-01-02 04:41:52'),
(1674, 12, 'meetings', 'test@test.com', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:52', '2019-01-02 04:41:52'),
(1675, 12, 'meetings', '0096699961102', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(1676, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(1677, 12, 'meetings', '00966535779245', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(1678, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد بدء إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(1679, 12, 'meetings', '00966509193274', 1, 'لقد بدء إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(1680, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:19:17', '2019-01-02 05:19:17'),
(1681, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:19:17', '2019-01-02 05:19:17'),
(1682, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:19:17', '2019-01-02 05:19:17'),
(1683, 12, 'meetings', '00966558037380', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1684, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1685, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1686, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1687, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1688, 12, 'meetings', 'test@test.com', 0, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1689, 12, 'meetings', '0096699961102', 1, 'لقد تم تعديل إجتماع في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(1690, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:01', '2019-01-02 05:20:01'),
(1691, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:01', '2019-01-02 05:20:01'),
(1692, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1693, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1694, 12, 'meetings', 'test@test.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1695, 12, 'meetings', '0096699961102', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1696, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1697, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1698, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1699, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(1700, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1701, 12, 'meetings', '0021699961102', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1702, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1703, 12, 'meetings', '00966558037380', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1704, 12, 'meetings', 'test@test.com', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1705, 12, 'meetings', '0096699961102', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1706, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1707, 12, 'meetings', '00966535779245', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(1708, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:20:17', '2019-01-02 05:20:17'),
(1709, 12, 'meetings', '00966509193274', 1, 'لقد تم إضافة مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:20:17', '2019-01-02 05:20:17'),
(1710, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(1711, 12, 'meetings', '0021699961102', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(1712, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(1713, 12, 'meetings', '00966558037380', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(1714, 12, 'meetings', 'test@test.com', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(1715, 12, 'meetings', '0096699961102', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(1716, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(1717, 12, 'meetings', '00966535779245', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(1718, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 0, NULL, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(1719, 12, 'meetings', '00966509193274', 1, 'لقد تم تعديل مهمة في نظام إجتماعاتي', 1, NULL, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(1720, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1721, 12, 'meetings', '0021699961102', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1722, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1723, 12, 'meetings', '00966558037380', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1724, 12, 'meetings', 'test@test.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1725, 12, 'meetings', '0096699961102', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1726, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(1727, 12, 'meetings', '00966535779245', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:12:19', '2019-01-02 06:12:19'),
(1728, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:12:19', '2019-01-02 06:12:19'),
(1729, 12, 'meetings', '00966509193274', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:12:19', '2019-01-02 06:12:19'),
(1730, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(1731, 12, 'meetings', '00966509193274', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(1732, 12, 'meetings', 'sa@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(1733, 12, 'meetings', '00966563421359', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(1734, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(1735, 12, 'meetings', '00966535779245', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1736, 12, 'meetings', 'rasha@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1737, 12, 'meetings', '00966541009675', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1738, 12, 'meetings', 'ismail@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1739, 12, 'meetings', '00966582983966', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1740, 12, 'meetings', 'Tahania@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1741, 12, 'meetings', '00966507187011', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1742, 12, 'meetings', 'sa@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(1743, 12, 'meetings', '00966563421359', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1744, 12, 'meetings', 'alsalo@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1745, 12, 'meetings', '00966502480256', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1746, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1747, 12, 'meetings', '00966558037380', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1748, 12, 'meetings', 'pmm.norah@ccreation.sa', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1749, 12, 'meetings', '00966544320099', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1750, 12, 'meetings', 'amidris@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(1751, 12, 'meetings', '00966507771868', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:52', '2019-01-02 06:14:52'),
(1752, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 0, NULL, '2019-01-02 06:14:52', '2019-01-02 06:14:52'),
(1753, 12, 'meetings', '0021699961102', 1, 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', 1, NULL, '2019-01-02 06:14:52', '2019-01-02 06:14:52'),
(1754, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-02 23:07:14', '2019-01-02 23:07:14'),
(1755, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-02 23:07:51', '2019-01-02 23:07:51'),
(1756, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-02 23:08:11', '2019-01-02 23:08:11'),
(1757, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-02 23:13:10', '2019-01-02 23:13:10'),
(1758, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-02 23:28:29', '2019-01-02 23:28:29'),
(1759, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-02 23:28:34', '2019-01-02 23:28:34'),
(1760, 12, 'prm', 'sa@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:13', '2019-01-03 09:45:13'),
(1761, 12, 'prm', '00966563421359', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:13', '2019-01-03 09:45:13'),
(1762, 12, 'prm', 'rasha@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:13', '2019-01-03 09:45:13'),
(1763, 12, 'prm', '00966541009675', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1764, 12, 'prm', 'Tahania@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1765, 12, 'prm', '00966507187011', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1766, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1767, 12, 'prm', '0021699961102', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1768, 12, 'prm', 'ismail@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1769, 12, 'prm', '00966582983966', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1770, 12, 'prm', 'alsalo@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1771, 12, 'prm', '00966502480256', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(1772, 12, 'prm', 'sa@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:25', '2019-01-03 09:45:25'),
(1773, 12, 'prm', '00966563421359', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:25', '2019-01-03 09:45:25'),
(1774, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:25', '2019-01-03 09:45:25'),
(1775, 12, 'prm', '0021699961102', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:26', '2019-01-03 09:45:26'),
(1776, 12, 'prm', 'sa@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:48', '2019-01-03 09:45:48'),
(1777, 12, 'prm', '00966563421359', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:48', '2019-01-03 09:45:48'),
(1778, 12, 'prm', 'rasha@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:48', '2019-01-03 09:45:48'),
(1779, 12, 'prm', '00966541009675', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1780, 12, 'prm', 'Tahania@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1781, 12, 'prm', '00966507187011', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1782, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1783, 12, 'prm', '0021699961102', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1784, 12, 'prm', 'ismail@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1785, 12, 'prm', '00966582983966', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1786, 12, 'prm', 'alsalo@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(1787, 12, 'prm', '00966502480256', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:50', '2019-01-03 09:45:50'),
(1788, 12, 'prm', 'sa@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:58', '2019-01-03 09:45:58'),
(1789, 12, 'prm', '00966563421359', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1790, 12, 'prm', 'rasha@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1791, 12, 'prm', '00966541009675', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1792, 12, 'prm', 'Tahania@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1793, 12, 'prm', '00966507187011', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1794, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1795, 12, 'prm', '0021699961102', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1796, 12, 'prm', 'ismail@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(1797, 12, 'prm', '00966582983966', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:46:00', '2019-01-03 09:46:00'),
(1798, 12, 'prm', 'alsalo@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-03 09:46:00', '2019-01-03 09:46:00'),
(1799, 12, 'prm', '00966502480256', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-03 09:46:00', '2019-01-03 09:46:00'),
(1800, 12, 'prm', 'SALAH@CCREATION.SA', 0, 'تعديل مشروع', 0, NULL, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(1801, 12, 'prm', '00966535779245', 1, 'تعديل مشروع', 1, NULL, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(1802, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(1803, 12, 'prm', '00966502480256', 1, 'تعديل مشروع', 1, NULL, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(1804, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(1805, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(1806, 12, 'prm', 'sa@ccreation.sa', 0, '(رسالة من ابراهيم السلو ) تست', 0, NULL, '2019-01-03 09:55:35', '2019-01-03 09:55:35'),
(1807, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(1808, 12, 'passwords', '00966509193274', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(1809, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(1810, 12, 'passwords', '00966535779245', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(1811, 12, 'passwords', 'rasha@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(1812, 12, 'passwords', '00966541009675', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1813, 12, 'passwords', 'ismail@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1814, 12, 'passwords', '00966582983966', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1815, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1816, 12, 'passwords', '0021699961102', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1817, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1818, 12, 'passwords', '00966599464878', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1819, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(1820, 12, 'passwords', '00966507187011', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1821, 12, 'passwords', 'sa@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1822, 12, 'passwords', '00966563421359', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1823, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1824, 12, 'passwords', '00966502480256', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1825, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1826, 12, 'passwords', '00966558037380', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1827, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(1828, 12, 'passwords', '00966544320099', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(1829, 12, 'passwords', 'amidris@gmail.com', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(1830, 12, 'passwords', '00966507771868', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(1831, 12, 'passwords', 'test@test.com', 0, 'إضافة موقع', 0, NULL, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(1832, 12, 'passwords', '0096699961102', 1, 'إضافة موقع', 1, NULL, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(1833, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:50', '2019-01-03 22:41:50'),
(1834, 12, 'passwords', '00966509193274', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1835, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1836, 12, 'passwords', '00966535779245', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1837, 12, 'passwords', 'rasha@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1838, 12, 'passwords', '00966541009675', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1839, 12, 'passwords', 'ismail@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1840, 12, 'passwords', '00966582983966', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1841, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(1842, 12, 'passwords', '0021699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1843, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1844, 12, 'passwords', '00966599464878', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1845, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1846, 12, 'passwords', '00966507187011', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1847, 12, 'passwords', 'sa@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1848, 12, 'passwords', '00966563421359', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1849, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(1850, 12, 'passwords', '00966502480256', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1851, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1852, 12, 'passwords', '00966558037380', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1853, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1854, 12, 'passwords', '00966544320099', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1855, 12, 'passwords', 'amidris@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53');
INSERT INTO `archives` (`id`, `company_id`, `daftar`, `user`, `type`, `message`, `status`, `error`, `created_at`, `updated_at`) VALUES
(1856, 12, 'passwords', '00966507771868', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1857, 12, 'passwords', 'test@test.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(1858, 12, 'passwords', '0096699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:41:54', '2019-01-03 22:41:54'),
(1859, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:43', '2019-01-03 22:45:43'),
(1860, 12, 'passwords', '00966509193274', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1861, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1862, 12, 'passwords', '00966535779245', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1863, 12, 'passwords', 'rasha@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1864, 12, 'passwords', '00966541009675', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1865, 12, 'passwords', 'ismail@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1866, 12, 'passwords', '00966582983966', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1867, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(1868, 12, 'passwords', '0021699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1869, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1870, 12, 'passwords', '00966599464878', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1871, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1872, 12, 'passwords', '00966507187011', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1873, 12, 'passwords', 'sa@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1874, 12, 'passwords', '00966563421359', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1875, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1876, 12, 'passwords', '00966502480256', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(1877, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1878, 12, 'passwords', '00966558037380', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1879, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1880, 12, 'passwords', '00966544320099', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1881, 12, 'passwords', 'amidris@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1882, 12, 'passwords', '00966507771868', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1883, 12, 'passwords', 'test@test.com', 0, 'تعديل موقع', 0, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1884, 12, 'passwords', '0096699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(1885, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'الإجتماع : تست<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/44/32<br><a href=\'https://1click.ws/meeting/44/32\'>https://1click.ws/meeting/44/32</a>', 0, NULL, '2019-01-04 01:16:26', '2019-01-04 01:16:26'),
(1886, 12, 'meetings', '00966509193274', 1, 'الإجتماع : تست\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/44/32\n\nhttps://1click.ws/meeting/44/32', 1, NULL, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(1887, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة إجتماع جديد', 0, NULL, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(1888, 12, 'meetings', '00966509193274', 1, 'إضافة إجتماع جديد', 1, NULL, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(1889, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'الإجتماع : تست<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/44/30<br><a href=\'https://1click.ws/meeting/44/30\'>https://1click.ws/meeting/44/30</a>', 0, NULL, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(1890, 12, 'meetings', '00966535779245', 1, 'الإجتماع : تست\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/44/30\n\nhttps://1click.ws/meeting/44/30', 1, NULL, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(1891, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إضافة إجتماع جديد', 0, NULL, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(1892, 12, 'meetings', '00966535779245', 1, 'إضافة إجتماع جديد', 1, NULL, '2019-01-04 01:16:28', '2019-01-04 01:16:28'),
(1893, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'الإجتماع : تست<br>لقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/44/32<br><a href=\'https://1click.ws/meeting/44/32\'>https://1click.ws/meeting/44/32</a>', 0, NULL, '2019-01-04 01:16:28', '2019-01-04 01:16:28'),
(1894, 12, 'meetings', '00966509193274', 1, 'الإجتماع : تست\nلقد تم إنشاء إجتماع جديدhttps://1click.ws/meeting/44/32\n\nhttps://1click.ws/meeting/44/32', 1, NULL, '2019-01-04 01:16:28', '2019-01-04 01:16:28'),
(1895, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة إجتماع جديد', 0, NULL, '2019-01-04 01:16:28', '2019-01-04 01:16:28'),
(1896, 12, 'meetings', '00966509193274', 1, 'إضافة إجتماع جديد', 1, NULL, '2019-01-04 01:16:28', '2019-01-04 01:16:28'),
(1897, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(1898, 12, 'meetings', '00966509193274', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(1899, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(1900, 12, 'meetings', '00966535779245', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(1901, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(1902, 12, 'meetings', '00966509193274', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 01:17:43', '2019-01-04 01:17:43'),
(1903, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 01:17:59', '2019-01-04 01:17:59'),
(1904, 12, 'meetings', '00966509193274', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 01:17:59', '2019-01-04 01:17:59'),
(1905, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 01:17:59', '2019-01-04 01:17:59'),
(1906, 12, 'meetings', '00966535779245', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 01:18:00', '2019-01-04 01:18:00'),
(1907, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 01:18:00', '2019-01-04 01:18:00'),
(1908, 12, 'meetings', '00966509193274', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 01:18:00', '2019-01-04 01:18:00'),
(1909, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'بدء إجتماع', 0, NULL, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(1910, 12, 'meetings', '00966509193274', 1, 'بدء إجتماع', 1, NULL, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(1911, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'بدء إجتماع', 0, NULL, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(1912, 12, 'meetings', '00966535779245', 1, 'بدء إجتماع', 1, NULL, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(1913, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'بدء إجتماع', 0, NULL, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(1914, 12, 'meetings', '00966509193274', 1, 'بدء إجتماع', 1, NULL, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(1915, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'بدء محور', 0, NULL, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(1916, 12, 'meetings', '00966509193274', 1, 'بدء محور', 1, NULL, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(1917, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'بدء محور', 0, NULL, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(1918, 12, 'meetings', '00966535779245', 1, 'بدء محور', 1, NULL, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(1919, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'بدء محور', 0, NULL, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(1920, 12, 'meetings', '00966509193274', 1, 'بدء محور', 1, NULL, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(1921, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إنهاء محور', 0, NULL, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(1922, 12, 'meetings', '00966509193274', 1, 'إنهاء محور', 1, NULL, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(1923, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إنهاء محور', 0, NULL, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(1924, 12, 'meetings', '00966535779245', 1, 'إنهاء محور', 1, NULL, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(1925, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إنهاء محور', 0, NULL, '2019-01-04 01:20:54', '2019-01-04 01:20:54'),
(1926, 12, 'meetings', '00966509193274', 1, 'إنهاء محور', 1, NULL, '2019-01-04 01:20:54', '2019-01-04 01:20:54'),
(1927, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'بدء محور', 0, NULL, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(1928, 12, 'meetings', '00966509193274', 1, 'بدء محور', 1, NULL, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(1929, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'بدء محور', 0, NULL, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(1930, 12, 'meetings', '00966535779245', 1, 'بدء محور', 1, NULL, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(1931, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'بدء محور', 0, NULL, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(1932, 12, 'meetings', '00966509193274', 1, 'بدء محور', 1, NULL, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(1933, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إنهاء محور', 0, NULL, '2019-01-04 01:21:49', '2019-01-04 01:21:49'),
(1934, 12, 'meetings', '00966509193274', 1, 'إنهاء محور', 1, NULL, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(1935, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إنهاء محور', 0, NULL, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(1936, 12, 'meetings', '00966535779245', 1, 'إنهاء محور', 1, NULL, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(1937, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إنهاء محور', 0, NULL, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(1938, 12, 'meetings', '00966509193274', 1, 'إنهاء محور', 1, NULL, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(1939, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة مهمة في إجتماع', 0, NULL, '2019-01-04 01:22:31', '2019-01-04 01:22:31'),
(1940, 12, 'meetings', '00966509193274', 1, 'إضافة مهمة في إجتماع', 1, NULL, '2019-01-04 01:22:31', '2019-01-04 01:22:31'),
(1941, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إضافة مهمة في إجتماع', 0, NULL, '2019-01-04 01:22:31', '2019-01-04 01:22:31'),
(1942, 12, 'meetings', '00966535779245', 1, 'إضافة مهمة في إجتماع', 1, NULL, '2019-01-04 01:22:32', '2019-01-04 01:22:32'),
(1943, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة مهمة في إجتماع', 0, NULL, '2019-01-04 01:22:32', '2019-01-04 01:22:32'),
(1944, 12, 'meetings', '00966509193274', 1, 'إضافة مهمة في إجتماع', 1, NULL, '2019-01-04 01:22:32', '2019-01-04 01:22:32'),
(1945, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'تعديل محور', 0, NULL, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(1946, 12, 'meetings', '00966509193274', 1, 'تعديل محور', 1, NULL, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(1947, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'تعديل محور', 0, NULL, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(1948, 12, 'meetings', '00966535779245', 1, 'تعديل محور', 1, NULL, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(1949, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'تعديل محور', 0, NULL, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(1950, 12, 'meetings', '00966509193274', 1, 'تعديل محور', 1, NULL, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(1951, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(1952, 12, 'prm', '00966544320099', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(1953, 12, 'prm', 'ismail@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(1954, 12, 'prm', '00966582983966', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(1955, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(1956, 12, 'prm', '0021699961102', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1957, 12, 'prm', 'rasha@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1958, 12, 'prm', '00966541009675', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1959, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1960, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1961, 12, 'prm', 'amr.sabry.designer@gmail.com', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1962, 12, 'prm', '00966558037380', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(1963, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1964, 12, 'prm', '00966544320099', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1965, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1966, 12, 'prm', '00966502480256', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1967, 12, 'prm', 'ismail@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1968, 12, 'prm', '00966582983966', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1969, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(1970, 12, 'prm', '0021699961102', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(1971, 12, 'prm', 'rasha@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(1972, 12, 'prm', '00966541009675', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(1973, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(1974, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(1975, 12, 'prm', 'amr.sabry.designer@gmail.com', 0, 'تعديل مشروع', 0, NULL, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(1976, 12, 'prm', '00966558037380', 1, 'تعديل مشروع', 1, NULL, '2019-01-04 01:58:38', '2019-01-04 01:58:38'),
(1977, 12, 'prm', 'sa@ccreation.sa', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:22', '2019-01-04 02:00:22'),
(1978, 12, 'prm', '00966563421359', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:22', '2019-01-04 02:00:22'),
(1979, 12, 'prm', 'rasha@ccreation.sa', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:22', '2019-01-04 02:00:22'),
(1980, 12, 'prm', '00966541009675', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1981, 12, 'prm', 'issam.web.technologie@gmail.com', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1982, 12, 'prm', '0021699961102', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1983, 12, 'prm', 'ismail@ccreation.sa', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1984, 12, 'prm', '00966582983966', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1985, 12, 'prm', 'alsalo@ccreation.sa', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1986, 12, 'prm', '00966502480256', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1987, 12, 'prm', 'pmm.norah@ccreation.sa', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1988, 12, 'prm', '00966544320099', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1989, 12, 'prm', 'amr.sabry.designer@gmail.com', 0, 'إضافة مهمة في مشروع', 0, NULL, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(1990, 12, 'prm', '00966558037380', 1, 'إضافة مهمة في مشروع', 1, NULL, '2019-01-04 02:00:24', '2019-01-04 02:00:24'),
(1991, 12, 'settings', 'issam.web.technologie@gmail.com', 0, 'هذه رسالة إختبار من نظام ERP', 0, NULL, '2019-01-04 21:22:34', '2019-01-04 21:22:34'),
(1992, 12, 'settings', '0021699961102', 1, 'هذه رسالة تجربة من ERP', 1, NULL, '2019-01-04 21:26:34', '2019-01-04 21:26:34'),
(1993, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(1994, 12, 'meetings', '0021699961102', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(1995, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(1996, 12, 'meetings', '00966558037380', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(1997, 12, 'meetings', 'test@test.com', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(1998, 12, 'meetings', '0096699961102', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(1999, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(2000, 12, 'meetings', '00966535779245', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 22:21:09', '2019-01-04 22:21:09'),
(2001, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'إضافة محور جديد', 0, NULL, '2019-01-04 22:21:09', '2019-01-04 22:21:09'),
(2002, 12, 'meetings', '00966509193274', 1, 'إضافة محور جديد', 1, NULL, '2019-01-04 22:21:09', '2019-01-04 22:21:09'),
(2003, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2004, 12, 'meetings', '0021699961102', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2005, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2006, 12, 'meetings', '00966558037380', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2007, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2008, 12, 'meetings', '00966509193274', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2009, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(2010, 12, 'meetings', '00966535779245', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:21:30', '2019-01-04 22:21:30'),
(2011, 12, 'meetings', 'test@test.com', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:21:30', '2019-01-04 22:21:30'),
(2012, 12, 'meetings', '0096699961102', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:21:30', '2019-01-04 22:21:30'),
(2013, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2014, 12, 'meetings', '0021699961102', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2015, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2016, 12, 'meetings', '00966558037380', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2017, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2018, 12, 'meetings', '00966509193274', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2019, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(2020, 12, 'meetings', '00966535779245', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:50:35', '2019-01-04 22:50:35'),
(2021, 12, 'meetings', 'test@test.com', 0, 'تعديل إجتماع', 0, NULL, '2019-01-04 22:50:35', '2019-01-04 22:50:35'),
(2022, 12, 'meetings', '0096699961102', 1, 'تعديل إجتماع', 1, NULL, '2019-01-04 22:50:35', '2019-01-04 22:50:35'),
(2023, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(2024, 12, 'meetings', '00966509193274', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(2025, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(2026, 12, 'meetings', '00966535779245', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(2027, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(2028, 12, 'meetings', '00966509193274', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(2029, 12, 'meetings', 'issam.web.technologie@gmail.com', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2030, 12, 'meetings', '0021699961102', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2031, 12, 'meetings', 'amr.sabry.designer@gmail.com', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2032, 12, 'meetings', '00966558037380', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2033, 12, 'meetings', 'test@test.com', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2034, 12, 'meetings', '0096699961102', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2035, 12, 'meetings', 'SALAH@CCREATION.SA', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2036, 12, 'meetings', '00966535779245', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2037, 12, 'meetings', 'enjaz@ccreation.sa', 0, 'حذف إجتماع', 0, NULL, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(2038, 12, 'meetings', '00966509193274', 1, 'حذف إجتماع', 1, NULL, '2019-01-05 00:19:11', '2019-01-05 00:19:11'),
(2039, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:26:39', '2019-01-05 00:26:39'),
(2040, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:26:39', '2019-01-05 00:26:39'),
(2041, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:26:39', '2019-01-05 00:26:39'),
(2042, 12, 'prm', '00966502480256', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:26:39', '2019-01-05 00:26:39'),
(2043, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:26:55', '2019-01-05 00:26:55'),
(2044, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:26:55', '2019-01-05 00:26:55'),
(2045, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:26:55', '2019-01-05 00:26:55'),
(2046, 12, 'prm', '00966502480256', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:26:55', '2019-01-05 00:26:55'),
(2047, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:27:14', '2019-01-05 00:27:14'),
(2048, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:27:15', '2019-01-05 00:27:15'),
(2049, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:27:15', '2019-01-05 00:27:15'),
(2050, 12, 'prm', '00966502480256', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:27:15', '2019-01-05 00:27:15'),
(2051, 12, 'prm', 'alsalo@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-05 00:31:20', '2019-01-05 00:31:20'),
(2052, 12, 'prm', '00966502480256', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-05 00:31:20', '2019-01-05 00:31:20'),
(2053, 12, 'prm', 'sa@ccreation.sa', 0, 'حذف مهمة في مشروع', 0, NULL, '2019-01-05 00:31:20', '2019-01-05 00:31:20'),
(2054, 12, 'prm', '00966563421359', 1, 'حذف مهمة في مشروع', 1, NULL, '2019-01-05 00:31:20', '2019-01-05 00:31:20'),
(2055, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:31:29', '2019-01-05 00:31:29'),
(2056, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:31:29', '2019-01-05 00:31:29'),
(2057, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:31:29', '2019-01-05 00:31:29'),
(2058, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:31:29', '2019-01-05 00:31:29'),
(2059, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:32:35', '2019-01-05 00:32:35'),
(2060, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:32:35', '2019-01-05 00:32:35'),
(2061, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:32:35', '2019-01-05 00:32:35'),
(2062, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:32:35', '2019-01-05 00:32:35'),
(2063, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:33:08', '2019-01-05 00:33:08'),
(2064, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:33:09', '2019-01-05 00:33:09'),
(2065, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:33:09', '2019-01-05 00:33:09'),
(2066, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:33:09', '2019-01-05 00:33:09'),
(2067, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:35:52', '2019-01-05 00:35:52'),
(2068, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:35:52', '2019-01-05 00:35:52'),
(2069, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:35:52', '2019-01-05 00:35:52'),
(2070, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:35:53', '2019-01-05 00:35:53'),
(2071, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:36:10', '2019-01-05 00:36:10'),
(2072, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:36:10', '2019-01-05 00:36:10'),
(2073, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:36:10', '2019-01-05 00:36:10'),
(2074, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:36:10', '2019-01-05 00:36:10'),
(2075, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:36:45', '2019-01-05 00:36:45'),
(2076, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:36:45', '2019-01-05 00:36:45'),
(2077, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:36:45', '2019-01-05 00:36:45'),
(2078, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:36:46', '2019-01-05 00:36:46'),
(2079, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:38:41', '2019-01-05 00:38:41'),
(2080, 12, 'prm', '00966502480256', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:38:41', '2019-01-05 00:38:41'),
(2081, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مهمة في مشروع', 0, NULL, '2019-01-05 00:38:42', '2019-01-05 00:38:42'),
(2082, 12, 'prm', '00966563421359', 1, 'تعديل مهمة في مشروع', 1, NULL, '2019-01-05 00:38:42', '2019-01-05 00:38:42'),
(2083, 12, 'prm', 'SALAH@CCREATION.SA', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(2084, 12, 'prm', '00966535779245', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(2085, 12, 'prm', 'alsalo@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(2086, 12, 'prm', '00966502480256', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(2087, 12, 'prm', 'sa@ccreation.sa', 0, 'تعديل مشروع', 0, NULL, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(2088, 12, 'prm', '00966563421359', 1, 'تعديل مشروع', 1, NULL, '2019-01-05 00:51:42', '2019-01-05 00:51:42'),
(2089, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:05', '2019-01-05 00:55:05'),
(2090, 12, 'passwords', '00966509193274', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:05', '2019-01-05 00:55:05'),
(2091, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:05', '2019-01-05 00:55:05'),
(2092, 12, 'passwords', '00966535779245', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2093, 12, 'passwords', 'rasha@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2094, 12, 'passwords', '00966541009675', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2095, 12, 'passwords', 'ismail@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2096, 12, 'passwords', '00966582983966', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2097, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2098, 12, 'passwords', '0021699961102', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2099, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(2100, 12, 'passwords', '00966599464878', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2101, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2102, 12, 'passwords', '00966507187011', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2103, 12, 'passwords', 'sa@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2104, 12, 'passwords', '00966563421359', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2105, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2106, 12, 'passwords', '00966502480256', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2107, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(2108, 12, 'passwords', '00966558037380', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2109, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2110, 12, 'passwords', '00966544320099', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2111, 12, 'passwords', 'amidris@gmail.com', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2112, 12, 'passwords', '00966507771868', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2113, 12, 'passwords', 'test@test.com', 0, 'حذف موقع', 0, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2114, 12, 'passwords', '0096699961102', 1, 'حذف موقع', 1, NULL, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(2115, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2116, 12, 'passwords', '00966509193274', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2117, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2118, 12, 'passwords', '00966535779245', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2119, 12, 'passwords', 'rasha@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2120, 12, 'passwords', '00966541009675', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2121, 12, 'passwords', 'ismail@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(2122, 12, 'passwords', '00966582983966', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2123, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2124, 12, 'passwords', '0021699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2125, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2126, 12, 'passwords', '00966599464878', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2127, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2128, 12, 'passwords', '00966507187011', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2129, 12, 'passwords', 'sa@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2130, 12, 'passwords', '00966563421359', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(2131, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2132, 12, 'passwords', '00966502480256', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2133, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2134, 12, 'passwords', '00966558037380', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2135, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2136, 12, 'passwords', '00966544320099', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2137, 12, 'passwords', 'amidris@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2138, 12, 'passwords', '00966507771868', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2139, 12, 'passwords', 'test@test.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(2140, 12, 'passwords', '0096699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:55:47', '2019-01-05 00:55:47'),
(2141, 12, 'passwords', 'enjaz@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:31', '2019-01-05 00:56:31'),
(2142, 12, 'passwords', '00966509193274', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:31', '2019-01-05 00:56:31'),
(2143, 12, 'passwords', 'SALAH@CCREATION.SA', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:31', '2019-01-05 00:56:31'),
(2144, 12, 'passwords', '00966535779245', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2145, 12, 'passwords', 'rasha@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2146, 12, 'passwords', '00966541009675', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2147, 12, 'passwords', 'ismail@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2148, 12, 'passwords', '00966582983966', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2149, 12, 'passwords', 'issam.web.technologie@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2150, 12, 'passwords', '0021699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(2151, 12, 'passwords', 'mohdesign92@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2152, 12, 'passwords', '00966599464878', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2153, 12, 'passwords', 'Tahania@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2154, 12, 'passwords', '00966507187011', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2155, 12, 'passwords', 'sa@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2156, 12, 'passwords', '00966563421359', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2157, 12, 'passwords', 'alsalo@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(2158, 12, 'passwords', '00966502480256', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2159, 12, 'passwords', 'amr.sabry.designer@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2160, 12, 'passwords', '00966558037380', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2161, 12, 'passwords', 'pmm.norah@ccreation.sa', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2162, 12, 'passwords', '00966544320099', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2163, 12, 'passwords', 'amidris@gmail.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2164, 12, 'passwords', '00966507771868', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2165, 12, 'passwords', 'test@test.com', 0, 'تعديل موقع', 0, NULL, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(2166, 12, 'passwords', '0096699961102', 1, 'تعديل موقع', 1, NULL, '2019-01-05 00:56:35', '2019-01-05 00:56:35'),
(2167, 12, 'emails', 'issam.web.technologie@gmail.com', 0, 'gdfgdfg', 0, NULL, '2019-01-05 02:59:04', '2019-01-05 02:59:04');

-- --------------------------------------------------------

--
-- Structure de la table `attachments`
--

CREATE TABLE `attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `user_id` int(12) DEFAULT NULL,
  `attachmentable_id` int(10) UNSIGNED NOT NULL,
  `attachmentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `attachments`
--

INSERT INTO `attachments` (`id`, `name`, `path`, `url`, `user_id`, `attachmentable_id`, `attachmentable_type`, `created_at`, `updated_at`) VALUES
(1, 'sgsdg', 'uploads/attachments/Op3M85WzMKcS2tOqtIisuBuUMZA5tMrIn436zQQP.jpeg', NULL, 33, 9, 'App\\Meeting', '2018-11-23 19:40:36', '2018-11-23 19:40:36'),
(2, 'sdf', NULL, 'http://www.pdf995.com/samples/pdf.pdf', 35, 9, 'App\\Meeting', '2018-11-24 13:59:51', '2018-11-24 13:59:51'),
(7, NULL, 'uploads/attachments/1543500167_Misc11.jpg', NULL, NULL, 50, 'App\\Task', '2018-11-29 13:02:47', '2018-11-29 13:02:47'),
(5, NULL, 'uploads/attachments/1543499789_Nature32.jpg', NULL, NULL, 48, 'App\\Task', '2018-11-29 12:56:29', '2018-11-29 12:56:29'),
(6, NULL, 'uploads/attachments/1543499789_LandWater13.jpg', NULL, NULL, 48, 'App\\Task', '2018-11-29 12:56:29', '2018-11-29 12:56:29'),
(8, NULL, 'uploads/attachments/1543500167_LandWater12.jpg', NULL, NULL, 50, 'App\\Task', '2018-11-29 13:02:47', '2018-11-29 13:02:47'),
(9, NULL, 'uploads/attachments/1543500167_Nature31.jpg', NULL, NULL, 50, 'App\\Task', '2018-11-29 13:02:47', '2018-11-29 13:02:47'),
(11, NULL, 'uploads/attachments/1543518900_Water8.jpg', NULL, 33, 2, 'App\\Comment', '2018-11-29 18:15:00', '2018-11-29 18:15:00'),
(12, NULL, 'uploads/attachments/1543518900_Nature23.jpg', NULL, 33, 2, 'App\\Comment', '2018-11-29 18:15:00', '2018-11-29 18:15:00'),
(13, NULL, 'uploads/attachments/1543600272_Nature23.jpg', NULL, 33, 2, 'App\\Discussion', '2018-11-30 19:51:12', '2018-11-30 19:51:12'),
(16, NULL, 'uploads/attachments/1544238906_لقطة الشاشة ١٤٤٠-٠٤-٠١ في ١٠.٤٩.٠٣ ص.png', NULL, 22, 4, 'App\\Comment', '2018-12-08 02:15:06', '2018-12-08 02:15:06'),
(17, NULL, 'uploads/attachments/1544240779_لقطة الشاشة ١٤٤٠-٠٤-٠١ في ١١.٤٣.٢٧ ص.png', NULL, 22, 8, 'App\\Discussion', '2018-12-08 05:46:19', '2018-12-08 05:46:19'),
(18, NULL, 'uploads/attachments/1544284455_Water8.jpg', NULL, 24, 11, 'App\\Comment', '2018-12-08 14:54:15', '2018-12-08 14:54:15'),
(19, NULL, 'uploads/attachments/1544284455_Nature23.jpg', NULL, 24, 11, 'App\\Comment', '2018-12-08 14:54:15', '2018-12-08 14:54:15'),
(20, NULL, 'uploads/attachments/1544284455_Mountains12.jpg', NULL, 24, 11, 'App\\Comment', '2018-12-08 14:54:15', '2018-12-08 14:54:15'),
(21, NULL, 'uploads/attachments/1544284455_City2.jpg', NULL, 24, 11, 'App\\Comment', '2018-12-08 14:54:15', '2018-12-08 14:54:15'),
(58, NULL, 'uploads/attachments/1545565023_العقد.pdf', NULL, NULL, 132, 'App\\Task', '2018-12-23 13:37:04', '2018-12-23 13:37:04'),
(59, NULL, 'uploads/attachments/1545756908_منصة الأرقان معدل3.pdf', NULL, NULL, 143, 'App\\Task', '2018-12-25 18:55:08', '2018-12-25 18:55:08'),
(60, NULL, 'uploads/attachments/1545756908_ملخص التعديلات المعتمد من قبل العميل عدا القسم المالي..docx', NULL, NULL, 143, 'App\\Task', '2018-12-25 18:55:08', '2018-12-25 18:55:08'),
(31, NULL, 'uploads/attachments/1544453954_Water8.jpg', NULL, 24, 12, 'App\\Discussion', '2018-12-10 16:59:14', '2018-12-10 16:59:14'),
(32, NULL, 'uploads/attachments/1544453954_Nature23.jpg', NULL, 24, 12, 'App\\Discussion', '2018-12-10 16:59:14', '2018-12-10 16:59:14'),
(33, NULL, 'uploads/attachments/1544453954_Mountains12.jpg', NULL, 24, 12, 'App\\Discussion', '2018-12-10 16:59:14', '2018-12-10 16:59:14'),
(34, NULL, 'uploads/attachments/1544453954_City2.jpg', NULL, 24, 12, 'App\\Discussion', '2018-12-10 16:59:14', '2018-12-10 16:59:14'),
(35, NULL, 'uploads/attachments/1544454381_Water8.jpg', NULL, 24, 15, 'App\\Discussion', '2018-12-10 17:06:21', '2018-12-10 17:06:21'),
(36, NULL, 'uploads/attachments/1544454381_Nature23.jpg', NULL, 24, 15, 'App\\Discussion', '2018-12-10 17:06:21', '2018-12-10 17:06:21'),
(37, NULL, 'uploads/attachments/1544454381_Mountains12.jpg', NULL, 24, 15, 'App\\Discussion', '2018-12-10 17:06:21', '2018-12-10 17:06:21'),
(38, NULL, 'uploads/attachments/1544455462_Nature23.jpg', NULL, 24, 16, 'App\\Discussion', '2018-12-10 17:24:22', '2018-12-10 17:24:22'),
(39, NULL, 'uploads/attachments/1544455462_Colour13.jpg', NULL, 24, 16, 'App\\Discussion', '2018-12-10 17:24:22', '2018-12-10 17:24:22'),
(40, NULL, 'uploads/attachments/1544455462_Space4.jpg', NULL, 24, 16, 'App\\Discussion', '2018-12-10 17:24:22', '2018-12-10 17:24:22'),
(41, NULL, 'uploads/attachments/1544455761_Colour11.jpg', NULL, 24, 17, 'App\\Discussion', '2018-12-10 17:29:21', '2018-12-10 17:29:21'),
(42, NULL, 'uploads/attachments/1544455761_Colour13.jpg', NULL, 24, 17, 'App\\Discussion', '2018-12-10 17:29:21', '2018-12-10 17:29:21'),
(43, NULL, 'uploads/attachments/1544455761_Nature23.jpg', NULL, 24, 17, 'App\\Discussion', '2018-12-10 17:29:21', '2018-12-10 17:29:21'),
(44, NULL, 'uploads/attachments/1544455834_City2.jpg', NULL, 24, 18, 'App\\Discussion', '2018-12-10 17:30:34', '2018-12-10 17:30:34'),
(45, NULL, 'uploads/attachments/1544455834_Colour13.jpg', NULL, 24, 18, 'App\\Discussion', '2018-12-10 17:30:34', '2018-12-10 17:30:34'),
(46, NULL, 'uploads/attachments/1544455896_Mountains13.jpg', NULL, 24, 19, 'App\\Discussion', '2018-12-10 17:31:36', '2018-12-10 17:31:36'),
(47, NULL, 'uploads/attachments/1544455896_Misc1.jpg', NULL, 24, 19, 'App\\Discussion', '2018-12-10 17:31:36', '2018-12-10 17:31:36'),
(48, NULL, 'uploads/attachments/1544455896_Colour11.jpg', NULL, 24, 19, 'App\\Discussion', '2018-12-10 17:31:36', '2018-12-10 17:31:36'),
(57, NULL, 'uploads/attachments/1544457397_Colour13.jpg', NULL, 24, 21, 'App\\Discussion', '2018-12-10 17:56:37', '2018-12-10 17:56:37'),
(61, NULL, 'uploads/attachments/1545756908_صلاحية الشراء من الموردين ونقاط البيع(1).docx', NULL, NULL, 143, 'App\\Task', '2018-12-25 18:55:08', '2018-12-25 18:55:08'),
(62, NULL, 'uploads/attachments/1545756908_التقارير المحاسبية المعتمدة.docx', NULL, NULL, 143, 'App\\Task', '2018-12-25 18:55:08', '2018-12-25 18:55:08'),
(63, NULL, 'uploads/attachments/1545756908_تطبيق أرجان باكيج (1).docx', NULL, NULL, 143, 'App\\Task', '2018-12-25 18:55:08', '2018-12-25 18:55:08'),
(64, NULL, 'uploads/attachments/1545756908_صلاحية الشراء من الموردين ونقاط البيع.docx', NULL, NULL, 143, 'App\\Task', '2018-12-25 18:55:08', '2018-12-25 18:55:08'),
(65, NULL, 'uploads/attachments/1545762550_تعديلات المقراءة.pdf', NULL, NULL, 144, 'App\\Task', '2018-12-25 20:29:10', '2018-12-25 20:29:10');

-- --------------------------------------------------------

--
-- Structure de la table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thedate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `attendances`
--

INSERT INTO `attendances` (`id`, `company_id`, `user_id`, `thedate`, `start_time`, `end_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 24, '2018-12-20', '04:27', '05:48', 'closed', '2018-12-18 16:48:53', '2018-12-18 16:48:56'),
(2, 12, 30, '2018-12-20', '01:30', '03:15', 'closed', '2018-12-18 16:48:53', '2018-12-18 16:48:56'),
(3, 12, 42, '2018-12-20', '02:30', '03:15', 'closed', '2018-12-18 16:48:53', '2018-12-18 16:48:56'),
(4, 12, 42, '2018-12-20', '01:30', '03:15', 'closed', '2018-12-18 16:48:53', '2018-12-18 16:48:56'),
(5, 12, 32, '2018-12-20', '10:37', NULL, 'open', '2018-12-18 21:37:09', '2018-12-18 21:37:09'),
(6, 12, 24, '2018-12-20', '01:28', '07:14', 'closed', '2018-12-19 12:28:33', '2018-12-20 18:14:21'),
(7, 12, 24, '2018-12-20', '04:37', '06:34', 'closed', '2018-12-20 15:37:58', '2018-12-20 17:34:27'),
(8, 12, 24, '2018-12-21', '08:31', '08:31', 'closed', '2018-12-21 19:31:12', '2018-12-21 19:31:19'),
(9, 12, 32, '2018-12-22', '12:51', '12:51', 'closed', '2018-12-22 11:51:08', '2018-12-22 11:51:20'),
(10, 12, 32, '2018-12-22', '04:56', '08:56', 'closed', '2018-12-22 15:56:40', '2018-12-22 19:56:04'),
(11, 12, 22, '2018-12-23', '02:12', '02:13', 'closed', '2018-12-23 13:12:50', '2018-12-23 13:13:04'),
(12, 12, 20, '2018-12-23', '02:13', '02:14', 'closed', '2018-12-23 13:13:53', '2018-12-23 13:14:02'),
(13, 12, 32, '2018-12-23', '08:54', '10:00', 'closed', '2018-12-23 19:54:14', '2018-12-23 21:00:43'),
(14, 12, 32, '2018-12-24', '08:22', NULL, 'open', '2018-12-24 19:22:29', '2018-12-24 19:22:29'),
(15, 12, 24, '2018-12-25', '08:54', NULL, 'open', '2018-12-25 19:54:03', '2018-12-25 19:54:03'),
(16, 12, 20, '2018-12-26', '02:16', '02:16', 'closed', '2018-12-26 01:16:39', '2018-12-26 01:16:49'),
(17, 12, 32, '2018-12-27', '09:11', NULL, 'open', '2018-12-28 02:11:34', '2018-12-28 02:11:34'),
(18, 12, 32, '2018-12-29', '04:16', '04:48', 'closed', '2018-12-29 21:16:40', '2018-12-29 21:48:27'),
(19, 12, 32, '2018-12-30', '08:12', '08:59', 'closed', '2018-12-31 01:12:52', '2018-12-31 01:59:27'),
(20, 12, 32, '2018-12-30', '08:59', '10:08', 'closed', '2018-12-31 01:59:37', '2018-12-31 03:08:09'),
(21, 12, 32, '2018-12-31', '08:11', '10:01', 'closed', '2019-01-01 01:11:03', '2019-01-01 03:01:38'),
(22, 12, 32, '2019-01-01', '08:41', '09:40', 'closed', '2019-01-02 01:41:20', '2019-01-02 02:40:32'),
(23, 12, 32, '2019-01-02', '08:46', NULL, 'open', '2019-01-03 01:46:11', '2019-01-03 01:46:11'),
(24, 12, 32, '2019-01-03', '08:18', '10:06', 'closed', '2019-01-04 01:18:44', '2019-01-04 03:06:22');

-- --------------------------------------------------------

--
-- Structure de la table `axes`
--

CREATE TABLE `axes` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderx` int(222) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `speaker_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `hours` int(12) NOT NULL,
  `minutes` int(12) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `start_at` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `category_meetings`
--

CREATE TABLE `category_meetings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(12) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category_meetings`
--

INSERT INTO `category_meetings` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 12, 'الإجتماع الأسبوعي', '2018-11-19 12:42:49', '2019-01-04 00:02:11'),
(3, 1, 'سلسلة 1', '2018-11-21 14:01:03', '2018-11-21 14:01:03'),
(4, 1, 'سلسة 2', '2018-11-21 14:01:09', '2018-11-21 14:01:09'),
(5, 12, 'اجتماع آخر', '2018-12-25 13:54:15', '2019-01-04 00:02:01'),
(6, 12, 'اجتماعات تحفيظ الجبيل', '2019-01-03 09:34:06', '2019-01-04 00:02:01'),
(7, 12, 'اجتماعات قسم التقنية', '2019-01-03 09:35:49', '2019-01-04 00:02:01'),
(8, 12, 'اجتماعات قسم الحملات التسويقية', '2019-01-03 09:36:08', '2019-01-04 00:02:01');

-- --------------------------------------------------------

--
-- Structure de la table `category_projects`
--

CREATE TABLE `category_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category_projects`
--

INSERT INTO `category_projects` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'النوع الأول', '2018-11-27 16:49:23', '2018-11-27 16:49:23'),
(2, 1, 'النوع الثاني', '2018-11-27 16:56:11', '2018-11-27 17:01:55'),
(4, 1, 'النوع الثالث', '2018-11-27 17:02:06', '2018-11-27 17:02:06'),
(5, 12, 'تسويق', '2018-12-07 09:57:20', '2019-01-04 03:07:39'),
(6, 12, 'تقنية', '2018-12-08 01:42:55', '2018-12-08 01:42:55'),
(7, 12, 'إعلام', '2018-12-08 01:43:13', '2018-12-08 01:43:13'),
(8, 12, 'داخلي للشركة', '2018-12-08 01:43:41', '2018-12-08 01:43:41');

-- --------------------------------------------------------

--
-- Structure de la table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `cities`
--

INSERT INTO `cities` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'مدينة 1', '2018-11-28 15:41:59', '2018-11-28 15:41:59'),
(2, 1, 'مدينة 2', '2018-11-28 15:42:05', '2018-11-28 15:42:05'),
(3, 1, 'مدينة 3', '2018-11-28 16:09:07', '2018-11-28 16:09:07'),
(4, 1, 'مدينة 4', '2018-11-28 17:02:10', '2018-11-28 17:02:10');

-- --------------------------------------------------------

--
-- Structure de la table `clients_olddd`
--

CREATE TABLE `clients_olddd` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `comment_id` int(12) NOT NULL DEFAULT '0',
  `texte` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_id` int(10) UNSIGNED NOT NULL,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `parent_id`, `comment_id`, `texte`, `commentable_id`, `commentable_type`, `created_at`, `updated_at`) VALUES
(1, 33, 50, 0, 'sdjhgs fjkds sd', 0, '', '2018-11-29 18:14:51', '2018-11-29 18:14:51'),
(2, 33, 50, 0, 'sdfsdfsdf', 0, '', '2018-11-29 18:15:00', '2018-11-29 18:15:00'),
(3, 22, 77, 0, 'استفسار', 0, '', '2018-12-08 02:13:04', '2018-12-08 02:13:04'),
(4, 22, 77, 0, 'ا', 0, '', '2018-12-08 02:15:05', '2018-12-08 02:15:05'),
(5, 24, 78, 0, 'يسقالقل', 0, '', '2018-12-08 13:17:05', '2018-12-08 13:17:05'),
(9, 24, 78, 0, '@المبرمج_عصام QSQS', 0, '', '2018-12-08 14:39:31', '2018-12-08 14:39:31'),
(8, 24, 78, 0, 'zsegze  @رياض_المحمدي zsefjgzebjfze fzefgzekf jzekjf zkejfz kefhk @اسماعيل_السلو', 0, '', '2018-12-08 14:06:23', '2018-12-08 14:06:23'),
(10, 24, 0, 5, 'sdgrergerg @رشا_العتيبي', 0, '', '2018-12-08 14:44:14', '2018-12-08 14:44:14'),
(15, 22, 82, 0, 'ilikgk\r\n\r\n @رشا_العتيبي  @المبرمج_عصام  @اسماعيل_السلو', 0, '', '2018-12-23 12:34:12', '2018-12-23 12:34:12'),
(13, 22, 83, 0, 'تم\r\n @ابراهيم_السلو', 0, '', '2018-12-14 09:12:32', '2018-12-14 09:12:32'),
(14, 22, 0, 13, 'اوك\r\n @اسماعيل_السلو', 0, '', '2018-12-14 09:12:48', '2018-12-14 09:12:48'),
(16, 22, 0, 15, 'ugh', 0, '', '2018-12-23 12:34:32', '2018-12-23 12:34:32'),
(18, 22, 174, 0, 'تم التنسيق بين عمرو ومحمد جمال \r\n @عمرو_صبري', 0, '', '2019-01-04 02:03:35', '2019-01-04 02:03:35'),
(19, 22, 132, 0, 'امل التحديث @ابراهيم_السلو', 0, '', '2019-01-05 00:40:36', '2019-01-05 00:40:36'),
(20, 22, 132, 0, 'امل التحديث\r\n @رياض_المحمدي', 0, '', '2019-01-05 00:40:50', '2019-01-05 00:40:50');

-- --------------------------------------------------------

--
-- Structure de la table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meetings` tinyint(1) NOT NULL DEFAULT '0',
  `crm` tinyint(1) NOT NULL DEFAULT '0',
  `prm` tinyint(1) NOT NULL DEFAULT '0',
  `hrm` tinyint(1) NOT NULL DEFAULT '0',
  `cards` tinyint(1) NOT NULL DEFAULT '0',
  `passwords` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `companies`
--

INSERT INTO `companies` (`id`, `name`, `slug`, `image`, `meetings`, `crm`, `prm`, `hrm`, `cards`, `passwords`, `created_at`, `updated_at`) VALUES
(1, 'شركة محلات تجارية كبرى', 'thecompany1', 'uploads/companies/neqNcCcy5EazhxeVAoOrqC3fWz8zieO1u3r0Z0DR.jpeg', 1, 0, 1, 1, 0, 1, '2018-10-24 21:50:47', '2018-12-12 12:52:39'),
(2, 'الشركة الثانية', 'company2', 'uploads/companies/yMZxnxMcKZzOITdCJxwlGYHk23qXWAURfi8FcMZR.jpeg', 1, 0, 0, 0, 0, 0, '2018-10-24 21:50:52', '2018-10-26 19:52:19'),
(3, 'الشركة رقم 3', 'شركة', 'uploads/companies/fK33caqpLaTOepbkuAse53C00ACmdfUYKMEr5DGr.jpeg', 1, 0, 0, 0, 0, 0, '2018-10-24 21:51:00', '2018-10-26 19:47:03'),
(4, 'الشركة الرابعة', 'company4', 'uploads/companies/ELeViHIynerWsBCup0sO88gylJhqH4Ga65HOfj0h.jpeg', 0, 0, 0, 0, 0, 0, '2018-10-26 19:39:40', '2018-10-26 19:39:40'),
(10, 'شركة 1', 'company1', 'uploads/companies/89OZCAszEsxLz29YmYxkQZxACnwkCCXG01kWLEiy.jpeg', 1, 0, 0, 0, 0, 0, '2018-11-12 17:16:37', '2018-11-12 17:16:37'),
(9, 'aaaaaaaaaaa', 'aaaaaaaaaaa', NULL, 1, 0, 0, 0, 0, 0, '2018-11-07 10:39:47', '2018-11-07 10:39:47'),
(11, 'صناعة المحتوى ـ متابعات', 'aaa', NULL, 1, 0, 0, 0, 0, 0, '2018-11-17 12:25:23', '2018-11-18 18:14:28'),
(12, 'شركة صناعة المحتوى', 'ccreation', 'uploads/companies/wkehhv5w9NVcn69RBSK2FocY57l2CL9vpEjGECj7.jpeg', 1, 0, 1, 1, 0, 1, '2018-11-18 18:17:58', '2018-12-20 12:32:14'),
(13, 'شركة فنوني', '8ofn', 'uploads/companies/RJIGsEhLUNbQU1o0sq7zv6DI7hzQHLkLy1iKZYRm.jpeg', 1, 0, 0, 0, 0, 0, '2018-11-18 18:43:01', '2018-11-18 18:43:01'),
(14, 'شركة x', 'x', 'uploads/companies/DS7fTi4vjSE4voxSXIQWnHgDkIBnkZ6TdrvqbXj4.png', 1, 0, 1, 1, 0, 1, '2018-12-25 22:26:41', '2018-12-25 22:26:41');

-- --------------------------------------------------------

--
-- Structure de la table `countries`
--

CREATE TABLE `countries` (
  `id` int(5) NOT NULL,
  `code` char(2) NOT NULL DEFAULT '',
  `name` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AD', 'أندورا'),
(2, 'AE', 'الإمارات'),
(3, 'AF', 'أفغانستان'),
(4, 'AG', 'بربودا و أنتيقا'),
(5, 'AI', 'أنغويلا'),
(6, 'AL', 'ألبانيا'),
(7, 'AM', 'أرمينيا'),
(8, 'AO', 'أنغولا'),
(9, 'AQ', 'أنترانتيكا'),
(10, 'AR', 'الأرجنتين'),
(11, 'AS', 'ساموا'),
(12, 'AT', 'النمسا'),
(13, 'AU', 'أستراليا'),
(14, 'AW', 'أروبا'),
(16, 'AZ', 'أذربيجان'),
(17, 'BA', 'البوسنة والهرسك'),
(18, 'BB', 'بربادوسا'),
(19, 'BD', 'بنغلاديش'),
(20, 'BE', 'بلجيكيا'),
(21, 'BF', 'بوركينا فاسو'),
(22, 'BG', 'بلغاريا'),
(23, 'BH', 'البحرين'),
(24, 'BI', 'بوراندي'),
(25, 'BJ', 'البينين'),
(27, 'BM', 'برمودا'),
(28, 'BN', 'بروناي'),
(29, 'BO', 'بوليفيا'),
(30, 'BQ', 'بونار'),
(31, 'BR', 'البرازيل'),
(32, 'BS', 'الباهاما'),
(33, 'BT', 'بوتان'),
(34, 'BV', 'جزيرة بوفيت'),
(35, 'BW', 'بوتسوانا'),
(36, 'BY', 'بيلاروسيا'),
(37, 'BZ', 'بيليز'),
(38, 'CA', 'كندا'),
(39, 'CC', 'جزر كوكوس'),
(40, 'CD', 'الكونغو الديمقراطية'),
(41, 'CF', 'جمهورية إفريقيا الوسطى'),
(42, 'CG', 'الكنغو'),
(43, 'CH', 'سويسرا'),
(46, 'CL', 'شيلي'),
(47, 'CM', 'كاميرون'),
(48, 'CN', 'الصين'),
(49, 'CO', 'كولومبيا'),
(50, 'CR', 'كوستاريكا'),
(51, 'CU', 'كوبا'),
(52, 'CV', 'الرأس الأخضر'),
(53, 'CW', 'كوراكاو'),
(55, 'CY', 'قبرص'),
(56, 'CZ', 'جمهورية التشيك'),
(57, 'DE', 'ألمانيا'),
(58, 'DJ', 'جيبوتي'),
(59, 'DK', 'دنمارك'),
(60, 'DM', 'دومينيكا'),
(61, 'DO', 'جمهورية الدومنيكان'),
(62, 'DZ', 'الجزائر'),
(63, 'EC', 'إكوادور'),
(64, 'EE', 'إستونيا'),
(65, 'EG', 'مصر'),
(66, 'EH', 'الصحراء الغربية'),
(67, 'ER', 'إيريتريا'),
(68, 'ES', 'إسبانيا'),
(69, 'ET', 'أثيوبيا'),
(70, 'FI', 'فنلاندا'),
(71, 'FJ', 'فوجي'),
(72, 'FK', 'جزر فوكلاند'),
(73, 'FM', 'ميكرونيزيا'),
(75, 'FR', 'فرنسا'),
(76, 'GA', 'الغابون'),
(77, 'GB', 'المملكة المتحدة'),
(78, 'GD', 'غرينادا'),
(79, 'GE', 'جيورجيا'),
(80, 'GF', 'فارو جويانا الفرنسية'),
(82, 'GH', 'غانا'),
(84, 'GL', 'غرينلاند'),
(85, 'GM', 'غامبيا'),
(86, 'GN', 'غينيا'),
(87, 'GP', 'قوادالوبي'),
(88, 'GQ', 'غينيا الإستوائية'),
(89, 'GR', 'اليونان'),
(90, 'GS', 'جورجيا الجنوبية وجزر ساندويتش الجنوبية'),
(91, 'GT', 'غواتيمالا'),
(92, 'GU', 'غوام'),
(93, 'GW', 'غينيا بيساو'),
(94, 'GY', 'غينيا'),
(95, 'HK', 'هونج كونج'),
(96, 'HM', 'جزيرة هيرد وجزر ماكدونالد'),
(97, 'HN', 'هندوراس'),
(98, 'HR', 'كرواتيا'),
(99, 'HT', 'هايتي'),
(100, 'HU', 'هنغاريا'),
(101, 'ID', 'أندونيسيا'),
(102, 'IE', 'إيرلندا'),
(104, 'IM', 'جزيرة آيل أوف مان'),
(105, 'IN', 'الهند'),
(106, 'IO', 'إقليم المحيط البريطاني الهندي'),
(107, 'IQ', 'العراق'),
(108, 'IR', 'إيران'),
(109, 'IS', 'إيزلاندا'),
(110, 'IT', 'إيطاليا'),
(111, 'JE', 'جيريسي'),
(112, 'JM', 'جامايكا'),
(113, 'JO', 'الأردن'),
(114, 'JP', 'اليابان'),
(115, 'KE', 'كينيا'),
(116, 'KG', 'قيرغستان'),
(117, 'KH', 'كمبوديا'),
(118, 'KI', 'كيريباتي'),
(119, 'KM', 'جزر القمر'),
(120, 'KN', 'سانت كيتس ونيفيس'),
(121, 'KP', 'كوريا الشمالية'),
(122, 'KR', 'كوريا الجنوبية'),
(123, 'KW', 'الكويت'),
(124, 'KY', 'جزر كايمان'),
(125, 'KZ', 'كزخستان'),
(126, 'LA', 'لاووس'),
(127, 'LB', 'لبنان'),
(128, 'LC', 'سانت لوسيا'),
(129, 'LI', 'ليختنشتاين'),
(130, 'LK', 'سيريلنكا'),
(131, 'LR', 'ليبيريا'),
(132, 'LS', 'ليسوتو'),
(133, 'LT', 'ليتوانيا'),
(134, 'LU', 'لوكسيمبورغ'),
(135, 'LV', 'لاتفيا'),
(136, 'LY', 'ليبيا'),
(137, 'MA', 'المغرب'),
(138, 'MC', 'موناكو'),
(139, 'MD', 'مولدوفا'),
(140, 'ME', 'مونتينيغرو'),
(141, 'MF', 'سانت مارتين'),
(142, 'MG', 'مدغشقر'),
(143, 'MH', 'جزر مارشال'),
(144, 'MK', 'مقدونيا'),
(145, 'ML', 'مالي'),
(146, 'MM', 'ميامار بورما'),
(147, 'MN', 'منغوليا'),
(148, 'MO', 'ماكاو'),
(149, 'MP', 'جزر مريانا الشمالية'),
(150, 'MQ', 'مارتينيك'),
(151, 'MR', 'موريطانيا'),
(152, 'MS', 'مونتسيرات'),
(153, 'MT', 'مالطا'),
(154, 'MU', 'موريتيوس'),
(155, 'MV', 'المالديف'),
(156, 'MW', 'ملاوي'),
(157, 'MX', 'المكسيك'),
(158, 'MY', 'ماليزيا'),
(159, 'MZ', 'الموزمبيق'),
(160, 'NA', 'نمبيا'),
(161, 'NC', 'كلدونيا الجديدة'),
(162, 'NE', 'النيجر'),
(163, 'NF', 'جزيرة نورفولك'),
(164, 'NG', 'نيجيريا'),
(165, 'NI', 'نيكراقواي'),
(166, 'NL', 'هولندا'),
(167, 'NO', 'النرويج'),
(168, 'NP', 'نيبال'),
(169, 'NR', 'ناورو'),
(170, 'NU', 'نيوي'),
(171, 'NZ', 'نيوزيلندا'),
(172, 'OM', 'عمان'),
(173, 'PA', 'بنما'),
(174, 'PE', 'البيرو'),
(175, 'PF', 'بولينيزيا الفرنسية'),
(176, 'PG', 'بابوا غينيا الجديدة'),
(177, 'PH', 'فيليبين'),
(178, 'PK', 'باكستان'),
(179, 'PL', 'بولندا'),
(180, 'PM', 'سان بيار وميكلون'),
(181, 'PN', 'جزر بيتكيرن'),
(182, 'PR', 'بويرتو ريكتو'),
(183, 'PS', 'فلسطين'),
(184, 'PT', 'البرتغال'),
(185, 'PW', 'بالاو'),
(186, 'PY', 'بوروغواي'),
(187, 'QA', 'قطر'),
(189, 'RO', 'رومانيا'),
(190, 'RS', 'صربيا'),
(191, 'RU', 'روسيا'),
(192, 'RW', 'رواندا'),
(193, 'SA', 'المملكة العربية السعودية'),
(194, 'SB', 'جزر سليمان'),
(195, 'SC', 'سيشل'),
(196, 'SD', 'السودان'),
(197, 'SE', 'السويد'),
(198, 'SG', 'سينغافورة'),
(199, 'SH', 'سانت هيلينا'),
(200, 'SI', 'سلوفينيا'),
(201, 'SJ', 'سفالبارد وجان مايان'),
(202, 'SK', 'سلوفاكيا'),
(203, 'SL', 'سييرا ليون'),
(204, 'SM', 'سان مارينو'),
(205, 'SN', 'سينيغال'),
(206, 'SO', 'الصومال'),
(207, 'SR', 'سورينام'),
(208, 'SS', 'السودان الجنوبية'),
(210, 'SV', 'السلفادور'),
(211, 'SX', 'سينت مارتن'),
(212, 'SY', 'سوريا'),
(213, 'SZ', 'سوازيلاند'),
(214, 'TC', 'جزر تركس وكايكوس'),
(215, 'TD', 'تشاد'),
(216, 'TF', 'المناطق الجنوبية لفرنسا'),
(217, 'TG', 'طوغو'),
(218, 'TH', 'تايلندا'),
(219, 'TJ', 'طاجيكستان'),
(220, 'TK', 'توكيلو'),
(221, 'TL', 'تيمور الشرقية'),
(222, 'TM', 'تركمنستان'),
(223, 'TN', 'تونس'),
(224, 'TO', 'تونغا'),
(225, 'TR', 'تركيا'),
(226, 'TT', 'ترينداد وتوباغو'),
(227, 'TV', 'توفالو'),
(228, 'TW', 'تايوان'),
(229, 'TZ', 'تنزانيا'),
(230, 'UA', 'أكرانيا'),
(231, 'UG', 'أوغندا'),
(232, 'UM', 'جزر الولايات المتحدة البعيدة الصغيرة'),
(233, 'US', 'الولايات المتحدة الأمريكية'),
(234, 'UY', 'أوروغواي'),
(235, 'UZ', 'أزبكستان'),
(236, 'VA', 'الفاتيكان'),
(237, 'VC', 'سانت فنسنت وجزر غرينادين'),
(238, 'VE', 'فنزويلا'),
(239, 'VG', 'جزر فيرجن البريطانية'),
(240, 'VI', 'جزر العذراء الأمريكية'),
(241, 'VN', 'فيتنام'),
(242, 'VU', 'فانواتو'),
(243, 'WF', 'واليس وفوتونا'),
(244, 'WS', 'ساموا'),
(245, 'XK', 'كوسوفو'),
(246, 'YE', 'اليمن'),
(247, 'YT', 'مايوت'),
(248, 'ZA', 'جنوب إفريقيا'),
(249, 'ZM', 'زمبيا'),
(250, 'ZW', 'زمبابوي');

-- --------------------------------------------------------

--
-- Structure de la table `custodies`
--

CREATE TABLE `custodies` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thedate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivered` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `custodies`
--

INSERT INTO `custodies` (`id`, `company_id`, `user_id`, `reference`, `name`, `thedate`, `number`, `description`, `file`, `delivered`, `created_at`, `updated_at`) VALUES
(1, 12, 24, '3256582', 'العهدة اﻷولى', '2018-12-10', '4', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما.', 'uploads/attachments/4gFO8beSTmSB8vPzpTSBX1Nn244UHNBiChHRVrTc.jpeg', 1, '2018-12-19 20:41:23', '2018-12-20 12:50:16'),
(2, 12, 24, '6827740', 'العهدة الثانية', '2018-12-22', '89', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص.', NULL, 0, '2018-12-20 12:57:54', '2018-12-20 12:58:15'),
(4, 12, 24, '47575757', 'dhgdfg', '2018-12-18', '4', NULL, NULL, 0, '2018-12-20 16:03:02', '2018-12-20 16:03:02');

-- --------------------------------------------------------

--
-- Structure de la table `deductions`
--

CREATE TABLE `deductions` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deduction_category_id` int(11) NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `deductions`
--

INSERT INTO `deductions` (`id`, `company_id`, `user_id`, `deduction_category_id`, `amount`, `from_date`, `to_date`, `created_at`, `updated_at`) VALUES
(1, 12, 24, 2, '90', '2018-12-18', '2018-12-30', '2018-12-19 15:03:04', '2018-12-19 15:03:04'),
(7, 12, 24, 1, '180', '2018-12-02', NULL, '2018-12-19 15:10:05', '2018-12-19 15:10:05');

-- --------------------------------------------------------

--
-- Structure de la table `deduction_categories`
--

CREATE TABLE `deduction_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `deduction_categories`
--

INSERT INTO `deduction_categories` (`id`, `company_id`, `name`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 12, 'عن بدل المواصلات', 1, '180', '2018-12-14 13:05:23', '2018-12-14 13:05:23'),
(2, 12, 'خطية', 0, '2', '2018-12-14 13:05:34', '2018-12-14 13:05:34');

-- --------------------------------------------------------

--
-- Structure de la table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `departments`
--

INSERT INTO `departments` (`id`, `company_id`, `name`, `user_id`, `created_at`, `updated_at`) VALUES
(6, 12, 'الادارة', 22, '2018-12-27 19:30:14', '2018-12-27 19:30:14');

-- --------------------------------------------------------

--
-- Structure de la table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `designations`
--

INSERT INTO `designations` (`id`, `company_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 12, 'المدير التنفيذي', 'القيام بأداء جميع المهمات و المسئوليات المنوطة بالمدير التنفيذي ، و المشاركة الفعالة فى تحديد و صياغة الأهداف، و كذلك تخطيط و تنظيم سير العمل بالمنشأة بما يضمن تحقيق الأهداف المحددة', '2018-12-12 13:23:58', '2019-01-04 20:04:28'),
(2, 12, 'مسؤول الموارد البشرية', 'يتولى مسؤولية جميع الأنشطة المرتبطة بكافة الإجراءات النظامية لشؤون الموظفين بداية من التعين حتى التقاعد أو إنهاء الخدمة', '2018-12-12 13:24:18', '2018-12-12 13:24:18'),
(3, 12, 'محاسب', 'التاكد من تقييد العمليات المحاسبية وترحيلها واستخراج موازين المراجعة اللازمة واجراء قيود التسويات واعداد التقارير المالية والمحاسبية وإتمام الحسابات الختامية والميزانية السنوية .', '2018-12-12 13:24:40', '2018-12-12 13:24:40'),
(4, 12, 'منسق مشاريع', 'يقوم بعمل التصاميم الهندسية للمنشاءات.', '2018-12-12 13:24:53', '2018-12-27 02:33:19'),
(7, 12, 'منسق اداري', NULL, '2018-12-27 02:33:07', '2018-12-27 02:33:07'),
(8, 12, 'منسق تسويق', NULL, '2018-12-27 02:33:42', '2018-12-27 02:33:42'),
(9, 12, 'منسق تقني', NULL, '2018-12-27 02:35:38', '2018-12-27 02:35:38'),
(10, 12, 'سكرتير تنفيذي', NULL, '2018-12-27 02:35:47', '2018-12-27 02:35:47'),
(11, 12, 'مصمم', 'مصمم', '2018-12-27 04:23:41', '2018-12-27 04:23:41'),
(12, 12, 'كاتبة محتوى', 'كاتبة محتوى', '2018-12-27 04:29:17', '2018-12-27 04:29:17'),
(13, 12, 'مسؤول حسابات التواصل الإجتماعي', 'مسؤول حسابات التواصل الإجتماعي', '2018-12-27 05:25:12', '2019-01-04 20:04:28');

-- --------------------------------------------------------

--
-- Structure de la table `discussions`
--

CREATE TABLE `discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(12) NOT NULL,
  `daftar_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `discussion_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `for_users` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `discussions`
--

INSERT INTO `discussions` (`id`, `company_id`, `daftar_id`, `type`, `discussion_id`, `user_id`, `for_users`, `name`, `text`, `created_at`, `updated_at`) VALUES
(2, 12, 6, 0, 0, 20, NULL, 'zetre', 'zerzer', '2018-12-10 18:23:39', '2018-12-10 18:23:39'),
(3, 12, 6, 0, 0, 24, '20', 'zerzer', 'zerzer', '2018-12-10 18:23:50', '2018-12-10 18:23:50'),
(4, 12, 6, 0, 0, 20, '31,28,24', 'zerzerzer', 'zerzer', '2018-12-10 18:24:02', '2018-12-10 18:24:02'),
(5, 12, 14, 0, 0, 24, NULL, 'qsd', 'qsdf', '2018-12-28 01:50:35', '2018-12-28 01:50:35');

-- --------------------------------------------------------

--
-- Structure de la table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `documents`
--

INSERT INTO `documents` (`id`, `company_id`, `name`, `file`, `created_at`, `updated_at`) VALUES
(4, 12, 'qsdqsd', 'uploads/attachments/6ZGSdvQh7x2CncGWqh4DNBRFmQEKbtSNLR9ZVxZf.jpeg', '2019-01-03 01:30:44', '2019-01-04 20:38:19');

-- --------------------------------------------------------

--
-- Structure de la table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `emails`
--

INSERT INTO `emails` (`id`, `from_id`, `to_id`, `subject`, `message`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 24, 22, 'بصدد إنشاء قسم الرسائل', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 'uploads/companies/8U5pOF3Rl9n6ohjdQhZlwF6iNhG0MkpB2OsjAsyH.jpeg', 0, '2019-01-05 00:32:08', '2019-01-05 00:32:08'),
(2, 22, 27, 'hh', 'hhhhhh', 'uploads/companies/S4945rHL9TWOKv67hcBk0Ln8a2wKckTTNgysXsAk.png', 0, '2019-01-05 00:58:31', '2019-01-05 00:58:31'),
(3, 24, 27, 'رسالة ثانية', 'ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.', NULL, 0, '2019-01-05 01:34:56', '2019-01-05 01:34:56'),
(4, 24, 24, 'شسيشسي صثق ثق', 'العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.', 'uploads/companies/TOdTGop7LuUSHQH8DozDK4tJbH258SLFu200aEGU.jpeg', 0, '2019-01-05 01:37:23', '2019-01-05 01:37:23'),
(5, 24, 24, 'qsdqsd', 'qsdqsdqsd', NULL, 0, '2019-01-05 02:51:01', '2019-01-05 02:51:01'),
(6, 24, 24, 'xcvxcvxcv', 'sfesfdsdf', NULL, 0, '2019-01-05 02:55:05', '2019-01-05 02:55:05'),
(7, 24, 24, 'dfgdf', 'gdfgdfg', NULL, 0, '2019-01-05 02:55:44', '2019-01-05 02:55:44'),
(8, 24, 24, 'dfgdf', 'gdfgdfg', NULL, 0, '2019-01-05 02:56:03', '2019-01-05 02:56:03'),
(9, 24, 24, 'dfgdf', 'gdfgdfg', NULL, 0, '2019-01-05 02:58:49', '2019-01-05 02:58:49'),
(10, 24, 24, 'dfgdf', 'gdfgdfg', NULL, 0, '2019-01-05 02:59:04', '2019-01-05 02:59:04');

-- --------------------------------------------------------

--
-- Structure de la table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(10) UNSIGNED NOT NULL,
  `eva_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `num` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evaluations`
--

INSERT INTO `evaluations` (`id`, `eva_id`, `item_id`, `num`, `created_at`, `updated_at`) VALUES
(17, 5, 4, 5, '2018-12-22 16:04:40', '2018-12-22 16:04:40'),
(18, 5, 3, 4, '2018-12-22 16:04:40', '2018-12-22 16:04:40'),
(19, 5, 2, 4, '2018-12-22 16:04:40', '2018-12-22 16:04:40'),
(20, 5, 1, 5, '2018-12-22 16:04:40', '2018-12-22 16:04:40');

-- --------------------------------------------------------

--
-- Structure de la table `evaluation_hrms`
--

CREATE TABLE `evaluation_hrms` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stars` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evaluation_hrms`
--

INSERT INTO `evaluation_hrms` (`id`, `company_id`, `user_id`, `stars`, `notes`, `created_at`, `updated_at`) VALUES
(1, 12, 32, 'a:1:{i:1;s:1:\"2\";}', 'إذ أدنى الدمج المتساقطة، تحت, أسيا اتّجة الإطلاق و وتم, قام وصغار إحكام ما. دون إذ كثيرة اوروبا. إجلاء بالفشل بحق عل. مدينة الجنوب بالسيطرة ولم تم. و دون ليتسنّى وبولندا الإقتصادي, كردة الشتاء مسؤولية لكل أي. اكتوبر مقاومة بمحاولة تلك أم, هُزم الوراء عرض أم. بـ تمهيد واُسدل ولم, الأولى وتتحمّل الفرنسي بل تحت.', '2018-12-14 14:32:03', '2018-12-24 14:32:03'),
(4, 12, 24, 'a:2:{i:1;s:1:\"3\";i:2;s:1:\"4\";}', 'ان سابق كُلفة بلا, البرية الأمريكية بها من, مئات إستيلاء الشّعبين قد حتى. وفي قد بزمام أعلنت, المواد البرية الإنذار، ٣٠ جهة. حالية لإنعدام كان من, كل المضي ليتسنّى والنرويج بحق. خطّة القوى على هو. الخاطفة واستمرت انتصارهم لكل بل, فمرّ الشهير الأوروبية مكن مع.', '2018-12-24 15:06:59', '2018-12-24 15:06:59');

-- --------------------------------------------------------

--
-- Structure de la table `evas`
--

CREATE TABLE `evas` (
  `id` int(10) UNSIGNED NOT NULL,
  `daftar` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `daftar_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evas`
--

INSERT INTO `evas` (`id`, `daftar`, `daftar_id`, `user_id`, `created_at`, `updated_at`) VALUES
(5, 'meeting', 35, 32, '2018-12-22 16:04:40', '2018-12-22 16:04:40');

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `expense_categories`
--

CREATE TABLE `expense_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `expense_categories`
--

INSERT INTO `expense_categories` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'الفئة الأولى', '2018-11-27 17:14:02', '2018-11-27 17:28:39'),
(2, 1, 'الفئة الثانية', '2018-11-27 17:26:22', '2018-11-28 14:27:59'),
(5, 12, 'eeeeeeeeeeeeeee', '2019-01-04 03:12:11', '2019-01-04 03:13:09');

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `daftar_id` int(11) NOT NULL,
  `file_type_id` int(11) NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `files`
--

INSERT INTO `files` (`id`, `user_id`, `company_id`, `daftar_id`, `file_type_id`, `name`, `path`, `size`, `created_at`, `updated_at`) VALUES
(2, 33, 1, 1, 1, 'Nature23.jpg', 'uploads/files/1543659233_Nature23.jpg', '79.14 KB', '2018-12-01 09:13:53', '2018-12-01 09:13:53'),
(3, 33, 1, 1, 1, 'Mountains12.jpg', 'uploads/files/1543659234_Mountains12.jpg', '88.21 KB', '2018-12-01 09:13:54', '2018-12-01 09:13:54'),
(4, 33, 1, 1, 1, 'City2.jpg', 'uploads/files/1543659235_City2.jpg', '102.64 KB', '2018-12-01 09:13:55', '2018-12-01 09:13:55'),
(7, 33, 1, 1, 2, 'LandWater12.jpg', 'uploads/files/1543659711_LandWater12.jpg', '253.6 KB', '2018-12-01 09:21:51', '2018-12-01 09:21:51'),
(6, 33, 1, 1, 2, 'Colour2.jpg', 'uploads/files/1543659678_Colour2.jpg', '275.87 KB', '2018-12-01 09:21:18', '2018-12-01 09:21:18'),
(11, 22, 12, 8, 6, 'لقطة الشاشة ١٤٤٠-٠٤-٠١ في ١٠.٤٩.٠٣ ص.png', 'uploads/files/1544782710_لقطة الشاشة ١٤٤٠-٠٤-٠١ في ١٠.٤٩.٠٣ ص.png', '28.82 KB', '2018-12-14 09:18:30', '2018-12-14 09:18:30'),
(9, 33, 1, 1, 2, 'Misc10.jpg', 'uploads/files/1543659724_Misc10.jpg', '182.76 KB', '2018-12-01 09:22:04', '2018-12-01 09:22:04'),
(12, 22, 12, 8, 6, 'لقطة الشاشة ١٤٤٠-٠٤-٠١ في ١١.٤٣.٢٧ ص.png', 'uploads/files/1544782711_لقطة الشاشة ١٤٤٠-٠٤-٠١ في ١١.٤٣.٢٧ ص.png', '3.84 KB', '2018-12-14 09:18:31', '2018-12-14 09:18:31'),
(13, 30, 12, 13, 8, 'تعديلات المقراءة.pdf', 'uploads/files/1545762412_تعديلات المقراءة.pdf', '626.09 KB', '2018-12-25 20:26:52', '2018-12-25 20:26:52'),
(14, 24, 12, 14, 9, 'Water8.jpg', 'uploads/files/1545933119_Water8.jpg', '77.25 KB', '2018-12-28 01:51:59', '2018-12-28 01:51:59'),
(15, 24, 12, 14, 9, 'Nature23.jpg', 'uploads/files/1545933119_Nature23.jpg', '79.14 KB', '2018-12-28 01:51:59', '2018-12-28 01:51:59'),
(16, 24, 12, 14, 9, 'Mountains12.jpg', 'uploads/files/1545933164_Mountains12.jpg', '88.21 KB', '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(17, 24, 12, 14, 9, 'City2.jpg', 'uploads/files/1545933164_City2.jpg', '102.64 KB', '2018-12-28 01:52:44', '2018-12-28 01:52:44');

-- --------------------------------------------------------

--
-- Structure de la table `file_types`
--

CREATE TABLE `file_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `daftar_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `file_types`
--

INSERT INTO `file_types` (`id`, `company_id`, `daftar_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'kjdghjkfdh', '2018-11-30 19:05:22', '2018-11-30 19:05:22'),
(2, 1, 1, 'ddddddddd', '2018-11-30 19:06:36', '2018-11-30 19:06:36'),
(3, 1, 1, 'ssssssssss', '2018-11-30 19:15:47', '2018-11-30 19:15:47'),
(5, 12, 6, 're(yh', '2018-12-10 14:31:46', '2018-12-10 14:31:46'),
(6, 12, 8, 'ملفات المشروع', '2018-12-14 09:18:10', '2018-12-14 09:18:10'),
(7, 12, 6, 'النظامية', '2018-12-23 12:38:18', '2018-12-23 12:38:18'),
(8, 12, 13, 'المقراءة', '2018-12-25 20:26:17', '2018-12-25 20:26:17'),
(9, 12, 14, 'qsd', '2018-12-28 01:51:44', '2018-12-28 01:51:44');

-- --------------------------------------------------------

--
-- Structure de la table `inviteds`
--

CREATE TABLE `inviteds` (
  `id` int(10) UNSIGNED NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attendance` tinyint(4) NOT NULL DEFAULT '0',
  `attendance2` tinyint(1) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `inviteds`
--

INSERT INTO `inviteds` (`id`, `meeting_id`, `user_id`, `attendance`, `attendance2`, `note`, `created_at`, `updated_at`) VALUES
(70, 35, 27, 0, 5, NULL, '2018-12-22 13:15:19', '2018-12-22 16:02:07'),
(69, 35, 20, 0, 5, NULL, '2018-12-22 13:08:24', '2018-12-22 16:01:57'),
(68, 35, 32, 0, 5, NULL, '2018-12-21 21:05:26', '2018-12-22 16:01:18'),
(67, 33, 20, 0, 0, NULL, '2018-12-08 10:50:03', '2018-12-08 10:50:03'),
(66, 33, 28, 0, 0, NULL, '2018-12-08 09:27:05', '2018-12-08 09:27:05'),
(65, 33, 32, 0, 0, NULL, '2018-12-08 09:05:50', '2018-12-08 09:05:50'),
(61, 29, 32, 0, 0, NULL, '2018-12-03 17:32:47', '2018-12-03 17:32:47'),
(60, 29, 31, 0, 0, NULL, '2018-12-01 11:24:00', '2018-12-01 11:24:00'),
(59, 29, 28, 0, 0, NULL, '2018-12-01 10:27:18', '2018-12-01 10:27:18'),
(49, 9, 35, 0, 2, NULL, '2018-11-22 12:23:49', '2018-11-22 15:21:04'),
(50, 9, 33, 0, 1, NULL, '2018-11-22 12:42:10', '2018-11-22 15:21:00'),
(51, 14, 33, 2, 3, 'بيفاقفا', '2018-11-22 14:05:22', '2018-11-22 14:37:45'),
(52, 14, 34, 1, 2, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', '2018-11-22 14:06:30', '2018-11-22 14:37:51'),
(53, 14, 35, 0, 1, NULL, '2018-11-22 14:06:35', '2018-11-22 14:36:28'),
(54, 14, 5, 3, 1, NULL, '2018-11-22 14:12:43', '2018-11-22 14:39:04'),
(55, 9, 34, 3, 0, NULL, '2018-11-22 14:49:23', '2018-11-22 14:49:23'),
(56, 9, 5, 3, 5, NULL, '2018-11-22 14:49:23', '2018-11-22 15:20:34'),
(57, 9, 10, 3, 3, NULL, '2018-11-22 14:49:23', '2018-11-22 15:21:09'),
(58, 9, 11, 3, 4, NULL, '2018-11-22 14:49:23', '2018-11-22 15:21:16'),
(71, 35, 23, 3, 2, NULL, '2018-12-22 15:26:03', '2018-12-22 16:02:44'),
(72, 35, 22, 3, 5, NULL, '2018-12-22 15:26:03', '2018-12-22 16:02:29'),
(73, 35, 28, 3, 5, NULL, '2018-12-22 15:26:03', '2018-12-22 16:02:56'),
(74, 35, 29, 3, 5, NULL, '2018-12-22 15:26:03', '2018-12-22 16:03:05'),
(75, 35, 42, 3, 5, NULL, '2018-12-22 15:26:03', '2018-12-22 16:03:16'),
(76, 35, 31, 3, 5, NULL, '2018-12-22 15:26:03', '2018-12-22 16:03:27'),
(77, 35, 30, 3, 5, NULL, '2018-12-22 15:26:03', '2018-12-22 16:03:36'),
(78, 37, 42, 3, 0, NULL, '2018-12-25 14:33:40', '2018-12-25 14:33:40'),
(79, 37, 20, 3, 0, NULL, '2018-12-25 14:33:40', '2018-12-25 14:33:40'),
(80, 38, 31, 3, 0, NULL, '2018-12-25 14:56:34', '2018-12-25 14:56:34'),
(81, 38, 20, 3, 0, NULL, '2018-12-25 14:56:34', '2018-12-25 14:56:34'),
(82, 38, 22, 3, 0, NULL, '2018-12-25 14:56:34', '2018-12-25 14:56:34'),
(83, 38, 28, 3, 0, NULL, '2018-12-25 14:56:34', '2018-12-25 14:56:34'),
(84, 38, 46, 3, 0, NULL, '2018-12-25 14:56:34', '2018-12-25 14:56:34'),
(85, 39, 49, 0, 0, NULL, '2018-12-26 09:00:27', '2018-12-26 09:00:27'),
(86, 39, 31, 3, 0, NULL, '2018-12-26 13:02:40', '2018-12-26 13:02:40'),
(87, 39, 20, 3, 0, NULL, '2018-12-26 13:02:40', '2018-12-26 13:02:40'),
(88, 39, 22, 3, 0, NULL, '2018-12-26 13:02:40', '2018-12-26 13:02:40'),
(89, 39, 46, 3, 0, NULL, '2018-12-26 13:02:40', '2018-12-26 13:02:40'),
(103, 41, 50, 2, 2, 'اعتذر لكن انا الان متوجهة لمنطقه ضعيفة الأبراج اذا في مجال راح اكون متواجده', '2018-12-29 16:27:39', '2018-12-29 21:47:03'),
(104, 41, 20, 0, 1, NULL, '2018-12-29 19:30:04', '2018-12-29 21:45:33'),
(105, 42, 20, 0, 0, NULL, '2018-12-29 17:38:59', '2018-12-29 17:38:59'),
(106, 41, 32, 0, 1, NULL, '2018-12-29 20:52:47', '2018-12-29 21:45:15'),
(107, 42, 28, 0, 0, NULL, '2018-12-29 18:11:50', '2018-12-29 18:11:50'),
(108, 41, 27, 3, 2, NULL, '2018-12-29 21:17:43', '2018-12-29 21:45:47'),
(109, 41, 31, 3, 2, NULL, '2018-12-29 21:17:43', '2018-12-29 21:46:00'),
(110, 41, 30, 3, 1, NULL, '2018-12-29 21:17:43', '2018-12-29 21:46:10'),
(111, 41, 28, 3, 1, NULL, '2018-12-29 21:17:43', '2018-12-29 21:46:22'),
(112, 41, 22, 3, 2, NULL, '2018-12-29 21:17:43', '2018-12-29 21:46:35'),
(113, 41, 23, 3, 2, NULL, '2018-12-29 21:17:43', '2018-12-29 21:46:49'),
(114, 41, 46, 3, 1, NULL, '2018-12-29 21:17:43', '2018-12-29 21:47:13');

-- --------------------------------------------------------

--
-- Structure de la table `invoice_categories`
--

CREATE TABLE `invoice_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `invoice_categories`
--

INSERT INTO `invoice_categories` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'فاتورة 1', '2018-11-28 10:48:45', '2018-11-28 10:48:45'),
(2, 1, 'فاتورة 2', '2018-11-28 11:03:58', '2018-11-28 11:05:25'),
(6, 12, 'sfgrsefr', '2019-01-04 03:32:20', '2019-01-04 03:32:20');

-- --------------------------------------------------------

--
-- Structure de la table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `items`
--

INSERT INTO `items` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'جودة الصوت', '2018-11-01 00:14:03', '2019-01-03 09:39:22'),
(9, 12, 'eztrertet', '2019-01-04 00:10:09', '2019-01-04 00:10:09'),
(2, 1, 'أهمية المحاور', '2018-11-01 00:19:41', '2019-01-03 09:40:00'),
(3, 1, 'ادارة الاجتماع', '2018-11-01 00:19:57', '2019-01-03 09:40:14'),
(4, 1, 'فائدة الاجتماع بشكل عام من وجه نظرك', '2018-11-01 00:20:14', '2019-01-03 09:39:08');

-- --------------------------------------------------------

--
-- Structure de la table `item_hrms`
--

CREATE TABLE `item_hrms` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `item_hrms`
--

INSERT INTO `item_hrms` (`id`, `company_id`, `name`, `designation_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 12, 'البند الأول', 0, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', '2018-12-13 16:10:05', '2018-12-13 16:10:05'),
(2, 12, 'البند الثاني', 2, 'شسيب', '2018-12-13 16:15:00', '2018-12-13 16:15:07');

-- --------------------------------------------------------

--
-- Structure de la table `job_locations`
--

CREATE TABLE `job_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `job_locations`
--

INSERT INTO `job_locations` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(4, 12, 'المقر الرئيسي', '2018-12-13 16:27:29', '2019-01-04 20:26:22'),
(3, 12, 'عن بعد', '2018-12-13 16:05:27', '2018-12-27 19:49:44');

-- --------------------------------------------------------

--
-- Structure de la table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `locations`
--

INSERT INTO `locations` (`id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'مكان إجتماع 2', '2018-10-29 17:57:50', '2018-10-29 17:57:50'),
(2, 1, 'مكان إجتماع 1', '2018-10-29 18:09:34', '2018-10-29 18:09:34'),
(3, 1, 'مكان إجتماع 3', '2018-10-29 20:42:37', '2018-10-29 20:42:37'),
(5, 9, 'dfgdfg', '2018-11-07 10:41:11', '2018-11-07 10:41:11'),
(6, 10, 'مكان الإجتماع نيبتلسيبس بسيبس', '2018-11-12 17:23:09', '2018-11-12 17:23:09'),
(7, 11, 'أون لاين', '2018-11-17 12:29:56', '2018-11-17 12:29:56'),
(9, 12, 'أون لاين - عن بعد', '2018-11-19 11:43:56', '2019-01-03 09:36:31'),
(11, 12, 'فرع الرياض', '2018-11-21 10:00:01', '2019-01-03 09:43:49');

-- --------------------------------------------------------

--
-- Structure de la table `mandates`
--

CREATE TABLE `mandates` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `task` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entreprise` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `mandates`
--

INSERT INTO `mandates` (`id`, `company_id`, `user_id`, `task`, `entreprise`, `address`, `type`, `from_date`, `to_date`, `price`, `details`, `file`, `created_at`, `updated_at`) VALUES
(1, 12, 27, 'مهمة 1', 'الجهة 1', 'موقع 1', 0, '2018-12-18', '2018-12-25', '789', 'يبنلتايبتنليبلىن يبتنﻻل يتنبل يتبنلب يتنلينمبال منيبتلنم', 'uploads/attachments/XGSFwL58O0fKRxVw38UHSzn9iJA3IhFoRj1q7evl.jpeg', '2018-12-20 17:13:08', '2018-12-20 17:13:08'),
(2, 12, 24, 'مهمة 2', 'الجهة 2', 'موقع 2', 1, '2018-12-12', '2018-12-30', '5465', 'الجهة 2', NULL, '2018-12-20 17:18:00', '2018-12-20 17:28:44');

-- --------------------------------------------------------

--
-- Structure de la table `medical_categories`
--

CREATE TABLE `medical_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `medical_categories`
--

INSERT INTO `medical_categories` (`id`, `company_id`, `name`, `employee`, `company`, `created_at`, `updated_at`) VALUES
(1, 12, 'كامل المبلغ', '0', '100', '2018-12-14 14:40:47', '2018-12-14 14:40:47'),
(2, 12, 'جزء من المبلغ', '4', '5', '2018-12-14 14:41:07', '2018-12-14 14:48:32');

-- --------------------------------------------------------

--
-- Structure de la table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(12) NOT NULL,
  `category_id` int(10) NOT NULL,
  `location_id` int(11) NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_time` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `record` tinyint(1) NOT NULL DEFAULT '0',
  `gmap_url` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `repeats` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `manager_id` int(11) NOT NULL,
  `amin_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `zoom_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `start_at` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `meetings`
--

INSERT INTO `meetings` (`id`, `name`, `company_id`, `category_id`, `location_id`, `from_date`, `from_time`, `duration`, `online`, `record`, `gmap_url`, `repeats`, `type`, `manager_id`, `amin_id`, `status`, `zoom_id`, `created_at`, `updated_at`, `start_at`) VALUES
(9, 'أجتماع تجريبي 2', 1, 3, 1, '2018-11-28', '17:31', '120', 0, 0, NULL, 0, 0, 33, 33, 2, NULL, '2018-11-21 14:05:24', '2018-11-24 17:58:17', '2018-11-22 15:49:23'),
(29, 'الاجتماع السادس', 12, 1, 9, '2018-12-01', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 0, NULL, '2018-12-01 10:22:57', '2018-12-28 00:14:56', NULL),
(8, 'أجتماع تجريبي 1', 1, 3, 2, '2018-11-22', '17:45', '0', 0, 0, NULL, 0, 0, 35, 33, 0, NULL, '2018-11-21 14:02:44', '2018-11-22 16:38:39', NULL),
(14, 'إجتماع تجريبي 3', 1, 3, 2, '2018-11-13', '17:31', '0', 0, 0, NULL, 0, 1, 35, 33, 2, NULL, '2018-11-22 14:04:02', '2018-11-23 11:54:48', '2018-11-23 10:52:06'),
(15, 'الحملات', 12, 1, 9, '2018-11-22', '15:45', '0', 0, 1, NULL, 0, 2, 22, 20, 0, NULL, '2018-11-22 15:42:43', '2018-11-28 16:29:53', NULL),
(37, 'مكتب الفريان ( للمحاماة )', 12, 5, 9, '2018-12-25', '14:54', '20', 0, 1, NULL, 0, 0, 42, 20, 2, NULL, '2018-12-25 13:55:21', '2018-12-25 14:48:10', '2018-12-25 15:33:40'),
(18, 'إجتماع تجريبي 4', 1, 4, 3, '2018-11-06', '15:34', '0', 0, 0, NULL, 0, 2, 35, 7, 0, NULL, '2018-11-24 13:35:09', '2018-11-28 16:49:38', NULL),
(28, 'الخامس', 12, 1, 9, '2018-11-24', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 0, NULL, '2018-11-27 16:22:32', '2018-12-17 18:16:21', NULL),
(35, 'التاسع مناقشة المحاور', 12, 1, 9, '2018-12-22', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 2, NULL, '2018-12-21 21:04:51', '2018-12-31 01:34:50', '2018-12-22 16:26:03'),
(33, 'السابع /مناقشة المحاور المدرجة', 12, 1, 9, '2018-12-08', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 0, NULL, '2018-12-08 09:00:00', '2018-12-17 18:21:41', NULL),
(34, 'الثامن مناقشة ومتابعة المهام', 12, 1, 9, '2018-12-15', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 0, NULL, '2018-12-17 16:58:29', '2018-12-17 18:44:09', NULL),
(38, 'حملة الجبيل', 12, 5, 9, '2018-12-25', '15:48', '18', 0, 0, NULL, 0, 0, 31, 20, 2, NULL, '2018-12-25 14:49:36', '2018-12-31 01:36:35', '2018-12-25 15:56:34'),
(39, 'مناقشة الافكار الرئيسية للحملة التسويقية للرقم الموحد لجمعية تحفيظ الجبيل', 12, 5, 9, '2018-12-26', '13:00', '0', 0, 1, NULL, 0, 0, 31, 20, 2, NULL, '2018-12-26 11:06:43', '2018-12-29 20:39:21', '2018-12-26 14:02:40'),
(41, 'العاشر - متابعات', 12, 1, 9, '2018-12-29', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 2, NULL, '2018-12-29 19:24:15', '2019-01-03 19:52:32', '2018-12-29 16:17:43'),
(42, 'العاشر - متابعات', 12, 1, 9, '2018-12-29', '16:00', '0', 0, 1, NULL, 0, 0, 32, 20, 0, NULL, '2018-12-29 20:38:10', '2018-12-29 20:38:10', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `meeting_client`
--

CREATE TABLE `meeting_client` (
  `id` int(10) UNSIGNED NOT NULL,
  `meeting_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `meeting_client`
--

INSERT INTO `meeting_client` (`id`, `meeting_id`, `user_id`) VALUES
(3, 8, 4),
(4, 8, 5),
(5, 9, 5),
(6, 9, 10),
(7, 9, 11),
(12, 14, 5),
(13, 18, 4),
(14, 18, 5),
(15, 39, 49);

-- --------------------------------------------------------

--
-- Structure de la table `meeting_user`
--

CREATE TABLE `meeting_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `meeting_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `meeting_user`
--

INSERT INTO `meeting_user` (`id`, `meeting_id`, `user_id`) VALUES
(49, 15, 29),
(110, 33, 30),
(105, 29, 23),
(104, 29, 22),
(103, 29, 28),
(102, 29, 29),
(101, 29, 27),
(100, 29, 31),
(48, 15, 31),
(47, 15, 32),
(46, 14, 34),
(127, 35, 27),
(40, 8, 34),
(39, 8, 33),
(50, 15, 28),
(131, 35, 23),
(130, 35, 22),
(129, 35, 28),
(128, 35, 29),
(135, 38, 22),
(134, 38, 28),
(133, 37, 42),
(132, 35, 42),
(59, 18, 33),
(60, 18, 34),
(126, 35, 31),
(124, 34, 42),
(123, 34, 23),
(122, 34, 22),
(121, 34, 28),
(120, 34, 29),
(119, 34, 27),
(118, 34, 31),
(117, 34, 30),
(116, 33, 23),
(97, 28, 28),
(96, 28, 29),
(95, 28, 27),
(94, 28, 31),
(93, 28, 30),
(99, 29, 30),
(115, 33, 22),
(114, 33, 28),
(113, 33, 29),
(112, 33, 27),
(111, 33, 31),
(125, 35, 30),
(98, 28, 23),
(136, 38, 46),
(137, 39, 22),
(138, 39, 46),
(139, 39, 20),
(150, 41, 27),
(149, 41, 31),
(148, 41, 30),
(151, 41, 28),
(152, 41, 20),
(153, 41, 22),
(154, 41, 23),
(155, 41, 50),
(156, 41, 46),
(157, 42, 30),
(158, 42, 31),
(159, 42, 27),
(160, 42, 28),
(161, 42, 20),
(162, 42, 22),
(163, 42, 23),
(164, 42, 50),
(165, 42, 46),
(166, 42, 24);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_24_172812_create_companies_table', 2),
(4, '2018_10_25_132645_create_users_table', 3),
(5, '2018_10_25_132708_create_clients_table', 3),
(6, '2018_10_25_171126_create_messages_table', 4),
(7, '2018_10_29_135025_create_locations_table', 5),
(8, '2018_10_29_144102_create_meetings_table', 6),
(9, '2018_10_20_150315_create_meeting_client_table', 7),
(10, '2018_10_20_150315_create_meeting_user_table', 7),
(11, '2018_10_29_210152_create_category_meetings_table', 8),
(12, '2018_10_30_153052_create_axes_table', 9),
(13, '2018_10_30_161733_create_comments_table', 10),
(14, '2018_10_30_163736_create_attachments_table', 11),
(15, '2018_10_31_131653_create_tasks_table', 12),
(16, '2018_10_20_150315_create_task_user_table', 13),
(17, '2018_10_31_170325_create_votes_table', 14),
(18, '2018_10_31_200835_create_items_table', 15),
(19, '2018_10_31_205709_create_evaluations_table', 16),
(20, '2018_10_31_210851_create_evas_table', 17),
(21, '2018_11_02_134337_create_inviteds_table', 18),
(22, '2018_11_02_154132_create_settings_table', 19),
(23, '2018_11_04_172644_create_roles_table', 20),
(24, '2018_11_07_180750_create_vote_users_table', 21),
(25, '2018_11_19_210011_create_archives_table', 22),
(26, '2018_11_27_164250_create_task_statuses_table', 23),
(27, '2018_11_27_174638_create_category_projects_table', 24),
(28, '2018_11_27_181134_create_expense_categories_table', 25),
(29, '2018_11_27_183932_create_taxes_table', 26),
(30, '2018_11_27_190133_create_percentages_table', 27),
(31, '2018_11_28_114555_create_invoice_categories_table', 28),
(32, '2018_11_28_151151_create_payment_methods_table', 29),
(33, '2018_11_28_155618_create_cities_table', 30),
(34, '2018_11_28_164553_create_projects_table', 31),
(35, '2018_07_15_185303_create_project_user_table', 32),
(36, '2018_11_28_235826_create_timers_table', 33),
(37, '2018_11_29_105307_create_milestones_table', 34),
(38, '2018_11_29_165818_create_subtasks_table', 35),
(39, '2018_11_30_174142_create_discussions_table', 36),
(40, '2018_11_30_194926_create_file_types_table', 37),
(41, '2018_11_30_201911_create_files_table', 38),
(42, '2018_12_07_170504_create_websites_table', 39),
(43, '2018_12_07_174056_create_site_users_table', 40),
(44, '2018_07_15_185303_create_site_user_website_table', 41),
(45, '2018_12_12_141544_create_designations_table', 42),
(46, '2018_12_12_144937_create_vacation_categories_table', 43),
(47, '2018_12_12_152431_create_violations_table', 44),
(48, '2018_12_12_154011_create_job_locations_table', 45),
(49, '2018_12_12_154915_create_item_hrms_table', 46),
(50, '2018_12_13_172109_create_documents_table', 47),
(51, '2018_12_13_174636_create_departments_table', 48),
(52, '2018_12_13_174703_create_sections_table', 48),
(53, '2018_12_14_131738_create_allowance_categories_table', 49),
(54, '2018_12_14_134939_create_deduction_categories_table', 50),
(55, '2018_12_14_140718_create_medical_categories_table', 51),
(56, '2018_12_14_140734_create_social_categories_table', 51),
(57, '2018_12_17_123558_create_client_details_table', 52),
(58, '2018_12_18_142621_create_attendances_table', 53),
(59, '2018_12_19_113010_create_allowances_table', 54),
(60, '2018_12_19_125856_create_deductions_table', 55),
(61, '2018_12_19_154658_create_advances_table', 56),
(62, '2018_12_19_182829_create_custodies_table', 57),
(63, '2018_12_20_125226_create_vacations_table', 58),
(64, '2018_12_20_135037_create_trainings_table', 59),
(65, '2018_12_20_150739_create_mandates_table', 60),
(66, '2018_12_21_161620_create_rewards_table', 61),
(67, '2018_12_21_164151_create_subtructions_table', 62),
(68, '2018_12_24_122228_create_evaluation_hrms_table', 63),
(69, '2018_12_25_124743_create_notifs_table', 64),
(70, '2018_12_25_152834_create_notifications_table', 65),
(71, '2018_12_25_172451_create_accountings_table', 66),
(72, '2018_12_31_165111_create_events_table', 67),
(73, '2019_01_04_162009_create_emails_table', 68);

-- --------------------------------------------------------

--
-- Structure de la table `milestones`
--

CREATE TABLE `milestones` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `daftar_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `milestones`
--

INSERT INTO `milestones` (`id`, `company_id`, `daftar_id`, `name`, `score`, `from_date`, `to_date`, `details`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'مرحلة 1', 20, '2018-11-14', '2018-11-28', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', '2018-11-29 10:17:27', '2018-11-29 10:17:27'),
(2, 1, 1, 'مرحلة 2', 59, '2018-11-27', '2018-11-30', 'fdqsdqsdqsdqsd', '2018-11-29 10:19:55', '2018-11-29 10:40:42'),
(4, 1, 1, 'مرحلة 3', 0, '2018-11-26', '2018-11-28', 'شصسثبقصثب صثقب صثقص قصثق', '2018-11-29 11:29:48', '2018-11-29 11:29:48'),
(5, 1, 1, 'sefzsef', 0, '2018-11-12', '2018-11-19', 'zef', '2018-11-29 12:35:50', '2018-11-29 12:35:50'),
(6, 12, 7, 'الإنشاء', 50, '2018-12-08', '2019-01-22', 'هي مرحل انشاء المشروع', '2018-12-08 02:07:23', '2019-01-04 02:00:22'),
(7, 12, 6, 'ألمرحلة الأولى', 100, NULL, NULL, NULL, '2018-12-08 10:25:35', '2019-01-03 09:45:13'),
(8, 12, 8, 'الدراسة', 0, NULL, NULL, NULL, '2018-12-14 09:11:05', '2018-12-14 09:11:05'),
(9, 12, 9, 'الدعم الفني والشات', 0, '2018-12-14', '2018-12-17', 'تذاكر الدعم ، الشات.', '2018-12-25 19:45:26', '2018-12-25 19:45:26'),
(10, 12, 9, 'إدارة العملاء', 0, '2018-12-17', '2018-12-21', 'العملاء ، مجموعة العملاء ، تقارير العملاء، البريد ، مركز التواصل، محفظة العميل ( مدفوعات العملاء)', '2018-12-25 19:46:23', '2018-12-25 19:46:23'),
(11, 12, 9, 'إدارة التسويق', 0, '2018-12-21', '2018-12-24', 'المعلين ، قنوات الإعلان ، الكوبون ، العروض ، حملة الرسائل القصيرة ، حملة البريد الالكتروينة .', '2018-12-25 19:47:12', '2018-12-25 19:47:12'),
(12, 12, 9, 'تحديث القائمة', 0, '2018-12-25', '2018-12-27', 'صفحة لوحة التحك\r\nمقائمة المتجر:\r\nتحديث قائمة المتجر\r\nقسم الطلبيات : \r\nاضافة الفلاتر ، اضافة الطلب، المرتجعات، الاستبدالات\r\nإدار المنتجات:\r\nإنشاء تبويبات لكل اقسام ادارة المنتج', '2018-12-25 19:52:30', '2018-12-25 19:52:30'),
(13, 12, 9, 'إدارة العملاء', 0, '2018-12-27', '2018-12-30', ':\r\nالعملاء ، مجموعة العملاء ، تقارير العملاء، البريد ، مركز التواصل، محفظة العميل ( مدفوعات العملاء)', '2018-12-25 19:54:09', '2018-12-25 19:54:09'),
(14, 12, 9, 'القسم المالي', 0, '2018-12-31', '2019-01-04', 'الحسابات، المعاملات، التقارير، قيد يومي، فاتورة الشراء ، سندات ، مرتجع مبيعات.:', '2018-12-25 19:56:05', '2018-12-25 19:56:05'),
(15, 12, 9, 'الإدراة الفرعية', 0, '2018-12-25', '2018-12-27', '\" تحديث جميع العناصر في بند الادارة الفرعية\"\r\nالدفع ، الضرائب، المحفظة ، الشحن ، التواصل ، وسائل الإتصال، بوبات رسائل الجوال، الملفات ، رسائل مخصصة ، صفحات مخصصة ، سجل القائمة البريدية.', '2018-12-25 20:03:08', '2018-12-25 20:03:49'),
(16, 12, 9, 'التقارير', 0, '2018-12-31', '2019-01-02', 'جميع تقارير المتجر', '2018-12-25 20:04:48', '2018-12-25 20:05:16'),
(17, 12, 9, 'المخزون', 0, '2019-01-02', '2019-01-05', 'المخزون ، مخزون المنتج، مخزون المورد ، المستودع.', '2018-12-25 20:06:07', '2018-12-25 20:06:07'),
(18, 12, 14, 'qsdqsd', 0, NULL, NULL, NULL, '2018-12-28 01:47:50', '2018-12-28 01:47:50'),
(20, 12, 15, 'المرحله الاولى', 0, '2018-12-28', '2019-01-31', NULL, '2018-12-28 19:52:52', '2018-12-28 19:52:52'),
(21, 12, 15, 'المرحله النهايه في نسخه مكتب ال فريان', 0, '2018-12-01', '2018-12-31', NULL, '2018-12-28 19:57:23', '2018-12-28 19:57:23'),
(22, 12, 10, 'الاتفاقية', 0, NULL, NULL, NULL, '2019-01-05 00:31:20', '2019-01-05 00:31:20');

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `notif_id` int(11) NOT NULL,
  `daftar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meeting_id` int(20) DEFAULT NULL,
  `project_id` int(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `notif_id`, `daftar`, `meeting_id`, `project_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 24, 1, 'hrm', NULL, NULL, 1, '2018-12-25 17:33:46', '2018-12-25 18:15:44'),
(11, 24, 6, 'hrm', NULL, NULL, 1, '2018-12-25 18:23:02', '2018-12-25 18:23:17'),
(10, 24, 4, 'hrm', NULL, NULL, 1, '2018-12-25 18:20:41', '2018-12-25 18:20:58'),
(9, 24, 3, 'hrm', NULL, NULL, 1, '2018-12-25 18:20:40', '2018-12-25 18:20:45'),
(8, 24, 3, 'hrm', NULL, NULL, 1, '2018-12-25 18:18:32', '2018-12-25 18:19:31'),
(7, 24, 2, 'hrm', NULL, NULL, 1, '2018-12-25 18:15:27', '2018-12-25 18:17:16'),
(12, 24, 5, 'hrm', NULL, NULL, 1, '2018-12-25 18:23:02', '2018-12-25 18:23:13'),
(13, 24, 7, 'hrm', NULL, NULL, 1, '2018-12-25 18:25:27', '2018-12-25 18:26:29'),
(14, 24, 8, 'hrm', NULL, NULL, 1, '2018-12-25 18:25:28', '2018-12-25 18:26:32'),
(15, 24, 7, 'hrm', NULL, NULL, 1, '2018-12-25 18:28:33', '2018-12-25 18:28:45'),
(16, 24, 8, 'hrm', NULL, NULL, 1, '2018-12-25 18:28:34', '2018-12-25 18:28:43'),
(17, 24, 9, 'hrm', NULL, NULL, 1, '2018-12-25 18:29:24', '2018-12-25 18:29:40'),
(18, 24, 10, 'hrm', NULL, NULL, 1, '2018-12-25 18:29:25', '2018-12-25 18:29:36'),
(19, 24, 11, 'hrm', NULL, NULL, 1, '2018-12-25 18:29:25', '2018-12-25 18:29:30'),
(20, 24, 12, 'hrm', NULL, NULL, 1, '2018-12-25 18:31:20', '2018-12-25 18:31:23'),
(21, 24, 13, 'hrm', NULL, NULL, 1, '2018-12-25 18:33:09', '2018-12-25 18:33:20'),
(22, 24, 14, 'hrm', NULL, NULL, 1, '2018-12-25 18:33:10', '2018-12-25 18:33:16'),
(23, 24, 15, 'hrm', NULL, NULL, 1, '2018-12-25 18:36:28', '2018-12-25 18:36:43'),
(24, 24, 16, 'hrm', NULL, NULL, 1, '2018-12-25 18:36:28', '2018-12-25 18:36:40'),
(25, 24, 17, 'hrm', NULL, NULL, 1, '2018-12-25 18:36:29', '2018-12-25 18:36:37'),
(26, 24, 18, 'hrm', NULL, NULL, 1, '2018-12-25 18:36:30', '2018-12-25 18:36:35'),
(27, 24, 20, 'hrm', NULL, NULL, 1, '2018-12-25 19:16:17', '2018-12-25 19:17:30'),
(28, 24, 20, 'hrm', NULL, NULL, 1, '2018-12-25 19:35:20', '2018-12-25 19:35:27'),
(29, 42, 2, 'hrm', NULL, NULL, 0, '2018-12-26 21:49:06', '2018-12-26 21:49:06'),
(30, 39, 2, 'hrm', NULL, NULL, 0, '2018-12-27 04:24:42', '2018-12-27 04:24:42'),
(36, 24, 22, 'meetings', 40, NULL, 1, '2018-12-27 23:39:29', '2018-12-28 04:26:32'),
(37, 32, 22, 'meetings', 40, NULL, 1, '2018-12-27 23:39:30', '2018-12-28 02:21:01'),
(38, 30, 22, 'meetings', 40, NULL, 0, '2018-12-27 23:39:31', '2018-12-27 23:39:31'),
(39, 31, 22, 'meetings', 40, NULL, 0, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(40, 27, 22, 'meetings', 40, NULL, 0, '2018-12-27 23:39:32', '2018-12-27 23:39:32'),
(41, 20, 22, 'meetings', 40, NULL, 0, '2018-12-27 23:39:33', '2018-12-27 23:39:33'),
(42, 22, 22, 'meetings', 40, NULL, 1, '2018-12-27 23:39:33', '2019-01-03 21:25:20'),
(43, 38, 22, 'meetings', 40, NULL, 0, '2018-12-27 23:39:34', '2018-12-27 23:39:34'),
(44, 24, 23, 'meetings', 40, NULL, 1, '2018-12-27 23:43:32', '2018-12-28 04:26:32'),
(45, 32, 23, 'meetings', 40, NULL, 0, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(46, 30, 23, 'meetings', 40, NULL, 0, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(47, 31, 23, 'meetings', 40, NULL, 0, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(48, 27, 23, 'meetings', 40, NULL, 0, '2018-12-27 23:43:33', '2018-12-27 23:43:33'),
(49, 20, 23, 'meetings', 40, NULL, 0, '2018-12-27 23:43:34', '2018-12-27 23:43:34'),
(50, 22, 23, 'meetings', 40, NULL, 1, '2018-12-27 23:43:34', '2019-01-03 21:25:20'),
(51, 38, 23, 'meetings', 40, NULL, 0, '2018-12-27 23:43:35', '2018-12-27 23:43:35'),
(52, 32, 24, 'meetings', 36, NULL, 0, '2018-12-27 23:49:24', '2018-12-27 23:49:24'),
(53, 24, 24, 'meetings', 36, NULL, 1, '2018-12-27 23:49:26', '2018-12-28 04:26:32'),
(54, 30, 24, 'meetings', 36, NULL, 0, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(55, 31, 24, 'meetings', 36, NULL, 0, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(56, 27, 24, 'meetings', 36, NULL, 0, '2018-12-27 23:49:27', '2018-12-27 23:49:27'),
(57, 24, 25, 'meetings', 40, NULL, 1, '2018-12-27 23:51:02', '2018-12-28 04:26:32'),
(58, 24, 26, 'meetings', 40, NULL, 1, '2018-12-27 23:52:49', '2018-12-28 04:26:32'),
(59, 32, 26, 'meetings', 40, NULL, 0, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(60, 30, 26, 'meetings', 40, NULL, 0, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(61, 31, 26, 'meetings', 40, NULL, 0, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(62, 27, 26, 'meetings', 40, NULL, 0, '2018-12-27 23:52:50', '2018-12-27 23:52:50'),
(63, 20, 26, 'meetings', 40, NULL, 0, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(64, 22, 26, 'meetings', 40, NULL, 1, '2018-12-27 23:52:51', '2019-01-03 21:25:20'),
(65, 38, 26, 'meetings', 40, NULL, 0, '2018-12-27 23:52:51', '2018-12-27 23:52:51'),
(66, 24, 27, 'meetings', 40, NULL, 1, '2018-12-27 23:55:09', '2018-12-28 04:26:32'),
(67, 32, 27, 'meetings', 40, NULL, 0, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(68, 30, 27, 'meetings', 40, NULL, 0, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(69, 31, 27, 'meetings', 40, NULL, 0, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(70, 27, 27, 'meetings', 40, NULL, 0, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(71, 20, 27, 'meetings', 40, NULL, 0, '2018-12-27 23:55:10', '2018-12-27 23:55:10'),
(72, 22, 27, 'meetings', 40, NULL, 1, '2018-12-27 23:55:11', '2019-01-03 21:25:20'),
(73, 38, 27, 'meetings', 40, NULL, 0, '2018-12-27 23:55:11', '2018-12-27 23:55:11'),
(74, 24, 28, 'meetings', 40, NULL, 1, '2018-12-27 23:56:53', '2018-12-28 04:26:32'),
(75, 32, 28, 'meetings', 40, NULL, 0, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(76, 30, 28, 'meetings', 40, NULL, 0, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(77, 31, 28, 'meetings', 40, NULL, 0, '2018-12-27 23:56:53', '2018-12-27 23:56:53'),
(78, 27, 28, 'meetings', 40, NULL, 0, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(79, 20, 28, 'meetings', 40, NULL, 0, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(80, 22, 28, 'meetings', 40, NULL, 1, '2018-12-27 23:56:54', '2019-01-03 21:25:20'),
(81, 38, 28, 'meetings', 40, NULL, 0, '2018-12-27 23:56:54', '2018-12-27 23:56:54'),
(82, 24, 29, 'meetings', 40, NULL, 1, '2018-12-27 23:58:53', '2018-12-28 04:26:32'),
(83, 32, 29, 'meetings', 40, NULL, 0, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(84, 30, 29, 'meetings', 40, NULL, 0, '2018-12-27 23:58:53', '2018-12-27 23:58:53'),
(85, 31, 29, 'meetings', 40, NULL, 0, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(86, 27, 29, 'meetings', 40, NULL, 0, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(87, 20, 29, 'meetings', 40, NULL, 0, '2018-12-27 23:58:54', '2018-12-27 23:58:54'),
(88, 22, 29, 'meetings', 40, NULL, 1, '2018-12-27 23:58:54', '2019-01-03 21:25:20'),
(89, 38, 29, 'meetings', 40, NULL, 0, '2018-12-27 23:58:55', '2018-12-27 23:58:55'),
(90, 24, 30, 'meetings', 40, NULL, 1, '2018-12-28 00:01:33', '2018-12-28 04:26:32'),
(91, 32, 30, 'meetings', 40, NULL, 0, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(92, 30, 30, 'meetings', 40, NULL, 0, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(93, 31, 30, 'meetings', 40, NULL, 0, '2018-12-28 00:01:33', '2018-12-28 00:01:33'),
(94, 27, 30, 'meetings', 40, NULL, 0, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(95, 20, 30, 'meetings', 40, NULL, 0, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(96, 22, 30, 'meetings', 40, NULL, 1, '2018-12-28 00:01:34', '2019-01-03 21:25:20'),
(97, 38, 30, 'meetings', 40, NULL, 0, '2018-12-28 00:01:34', '2018-12-28 00:01:34'),
(98, 24, 26, 'meetings', 40, NULL, 1, '2018-12-28 00:02:21', '2018-12-28 00:02:39'),
(99, 32, 26, 'meetings', 40, NULL, 0, '2018-12-28 00:02:21', '2018-12-28 00:02:21'),
(100, 30, 26, 'meetings', 40, NULL, 0, '2018-12-28 00:02:21', '2018-12-28 00:02:21'),
(101, 31, 26, 'meetings', 40, NULL, 0, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(102, 27, 26, 'meetings', 40, NULL, 0, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(103, 20, 26, 'meetings', 40, NULL, 0, '2018-12-28 00:02:22', '2018-12-28 00:02:22'),
(104, 22, 26, 'meetings', 40, NULL, 1, '2018-12-28 00:02:22', '2019-01-03 21:25:20'),
(105, 38, 26, 'meetings', 40, NULL, 0, '2018-12-28 00:02:23', '2018-12-28 00:02:23'),
(106, 24, 33, 'meetings', 40, NULL, 1, '2018-12-28 00:02:59', '2018-12-28 00:14:51'),
(107, 32, 33, 'meetings', 40, NULL, 0, '2018-12-28 00:02:59', '2018-12-28 00:02:59'),
(108, 30, 33, 'meetings', 40, NULL, 0, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(109, 31, 33, 'meetings', 40, NULL, 0, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(110, 27, 33, 'meetings', 40, NULL, 0, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(111, 20, 33, 'meetings', 40, NULL, 0, '2018-12-28 00:03:00', '2018-12-28 00:03:00'),
(112, 22, 33, 'meetings', 40, NULL, 1, '2018-12-28 00:03:01', '2019-01-03 21:25:20'),
(113, 38, 33, 'meetings', 40, NULL, 0, '2018-12-28 00:03:01', '2018-12-28 00:03:01'),
(114, 24, 34, 'meetings', 40, NULL, 1, '2018-12-28 00:06:12', '2018-12-28 00:14:55'),
(115, 32, 34, 'meetings', 40, NULL, 0, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(116, 30, 34, 'meetings', 40, NULL, 0, '2018-12-28 00:06:12', '2018-12-28 00:06:12'),
(117, 31, 34, 'meetings', 40, NULL, 0, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(118, 27, 34, 'meetings', 40, NULL, 0, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(119, 20, 34, 'meetings', 40, NULL, 0, '2018-12-28 00:06:13', '2018-12-28 00:06:13'),
(120, 22, 34, 'meetings', 40, NULL, 1, '2018-12-28 00:06:13', '2019-01-03 21:25:20'),
(121, 38, 34, 'meetings', 40, NULL, 0, '2018-12-28 00:06:14', '2018-12-28 00:06:14'),
(122, 32, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:11', '2018-12-28 00:07:11'),
(123, 20, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:11', '2018-12-28 00:07:11'),
(124, 23, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(125, 22, 32, 'meetings', 29, NULL, 1, '2018-12-28 00:07:12', '2019-01-03 21:25:20'),
(126, 28, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(127, 27, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:12', '2018-12-28 00:07:12'),
(128, 31, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(129, 30, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:07:13', '2018-12-28 00:07:13'),
(130, 32, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(131, 20, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:09:46', '2018-12-28 00:09:46'),
(132, 24, 32, 'meetings', 29, NULL, 1, '2018-12-28 00:09:46', '2018-12-28 00:14:56'),
(133, 22, 32, 'meetings', 29, NULL, 1, '2018-12-28 00:09:46', '2019-01-03 21:25:20'),
(134, 28, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(135, 27, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(136, 31, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(137, 30, 32, 'meetings', 29, NULL, 0, '2018-12-28 00:09:47', '2018-12-28 00:09:47'),
(138, 24, 31, 'meetings', 40, NULL, 1, '2018-12-28 00:11:01', '2018-12-28 00:14:57'),
(139, 32, 31, 'meetings', 40, NULL, 0, '2018-12-28 00:11:01', '2018-12-28 00:11:01'),
(140, 30, 31, 'meetings', 40, NULL, 0, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(141, 31, 31, 'meetings', 40, NULL, 0, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(142, 27, 31, 'meetings', 40, NULL, 0, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(143, 20, 31, 'meetings', 40, NULL, 0, '2018-12-28 00:11:02', '2018-12-28 00:11:02'),
(144, 22, 31, 'meetings', 40, NULL, 1, '2018-12-28 00:11:03', '2019-01-03 21:25:20'),
(145, 38, 31, 'meetings', 40, NULL, 0, '2018-12-28 00:11:03', '2018-12-28 00:11:03'),
(146, 24, 35, 'meetings', 40, NULL, 1, '2018-12-28 00:12:12', '2018-12-28 00:14:58'),
(147, 32, 35, 'meetings', 40, NULL, 0, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(148, 30, 35, 'meetings', 40, NULL, 0, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(149, 31, 35, 'meetings', 40, NULL, 0, '2018-12-28 00:12:12', '2018-12-28 00:12:12'),
(150, 27, 35, 'meetings', 40, NULL, 0, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(151, 20, 35, 'meetings', 40, NULL, 0, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(152, 22, 35, 'meetings', 40, NULL, 1, '2018-12-28 00:12:13', '2019-01-03 21:25:20'),
(153, 38, 35, 'meetings', 40, NULL, 0, '2018-12-28 00:12:13', '2018-12-28 00:12:13'),
(154, 24, 36, 'meetings', 40, NULL, 1, '2018-12-28 00:12:45', '2018-12-28 00:14:58'),
(155, 32, 36, 'meetings', 40, NULL, 0, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(156, 30, 36, 'meetings', 40, NULL, 0, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(157, 31, 36, 'meetings', 40, NULL, 0, '2018-12-28 00:12:45', '2018-12-28 00:12:45'),
(158, 27, 36, 'meetings', 40, NULL, 0, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(159, 20, 36, 'meetings', 40, NULL, 0, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(160, 22, 36, 'meetings', 40, NULL, 1, '2018-12-28 00:12:46', '2019-01-03 21:25:20'),
(161, 38, 36, 'meetings', 40, NULL, 0, '2018-12-28 00:12:46', '2018-12-28 00:12:46'),
(162, 24, 37, 'meetings', 40, NULL, 1, '2018-12-28 00:13:22', '2018-12-28 00:15:00'),
(163, 32, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(164, 30, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(165, 31, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:13:22', '2018-12-28 00:13:22'),
(166, 27, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(167, 20, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(168, 22, 37, 'meetings', 40, NULL, 1, '2018-12-28 00:13:23', '2019-01-03 21:25:20'),
(169, 38, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:13:23', '2018-12-28 00:13:23'),
(170, 24, 37, 'meetings', 40, NULL, 1, '2018-12-28 00:14:14', '2018-12-28 00:15:23'),
(171, 32, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:14:14', '2018-12-28 00:14:14'),
(172, 30, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(173, 31, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(174, 27, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(175, 20, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:14:15', '2018-12-28 00:14:15'),
(176, 22, 37, 'meetings', 40, NULL, 1, '2018-12-28 00:14:15', '2019-01-03 21:25:20'),
(177, 38, 37, 'meetings', 40, NULL, 0, '2018-12-28 00:14:16', '2018-12-28 00:14:16'),
(178, 24, 38, 'meetings', 40, NULL, 1, '2018-12-28 00:14:42', '2018-12-28 00:15:19'),
(179, 32, 38, 'meetings', 40, NULL, 1, '2018-12-28 00:14:43', '2018-12-28 02:18:35'),
(180, 30, 38, 'meetings', 40, NULL, 0, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(181, 31, 38, 'meetings', 40, NULL, 0, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(182, 27, 38, 'meetings', 40, NULL, 0, '2018-12-28 00:14:43', '2018-12-28 00:14:43'),
(183, 20, 38, 'meetings', 40, NULL, 0, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(184, 22, 38, 'meetings', 40, NULL, 1, '2018-12-28 00:14:44', '2019-01-03 21:25:20'),
(185, 38, 38, 'meetings', 40, NULL, 0, '2018-12-28 00:14:44', '2018-12-28 00:14:44'),
(186, 24, 39, 'prm', NULL, 6, 1, '2018-12-28 00:59:17', '2018-12-28 01:02:08'),
(187, 24, 39, 'prm', NULL, 14, 1, '2018-12-28 01:10:57', '2018-12-28 04:26:32'),
(188, 20, 39, 'prm', NULL, 14, 0, '2018-12-28 01:10:57', '2018-12-28 01:10:57'),
(189, 24, 40, 'prm', NULL, 14, 1, '2018-12-28 01:37:33', '2018-12-28 04:26:32'),
(190, 20, 40, 'prm', NULL, 14, 0, '2018-12-28 01:37:33', '2018-12-28 01:37:33'),
(191, 20, 42, 'prm', NULL, 14, 0, '2018-12-28 01:39:56', '2018-12-28 01:39:56'),
(192, 24, 42, 'prm', NULL, 14, 1, '2018-12-28 01:39:57', '2018-12-28 04:26:32'),
(193, 27, 42, 'prm', NULL, 14, 0, '2018-12-28 01:39:57', '2018-12-28 01:39:57'),
(194, 20, 43, 'prm', NULL, 14, 0, '2018-12-28 01:40:38', '2018-12-28 01:40:38'),
(195, 24, 43, 'prm', NULL, 14, 1, '2018-12-28 01:40:38', '2018-12-28 04:26:32'),
(196, 20, 43, 'prm', NULL, 14, 0, '2018-12-28 01:43:01', '2018-12-28 01:43:01'),
(197, 24, 43, 'prm', NULL, 14, 1, '2018-12-28 01:43:01', '2018-12-28 01:43:20'),
(198, 20, 44, 'prm', NULL, 14, 0, '2018-12-28 01:43:39', '2018-12-28 01:43:39'),
(199, 24, 44, 'prm', NULL, 14, 1, '2018-12-28 01:43:39', '2018-12-28 04:26:32'),
(200, 20, 45, 'prm', NULL, 14, 0, '2018-12-28 01:44:58', '2018-12-28 01:44:58'),
(201, 24, 45, 'prm', NULL, 14, 1, '2018-12-28 01:44:59', '2018-12-28 04:26:32'),
(202, 20, 46, 'prm', NULL, 14, 0, '2018-12-28 01:45:42', '2018-12-28 01:45:42'),
(203, 24, 46, 'prm', NULL, 14, 1, '2018-12-28 01:45:43', '2018-12-28 01:49:07'),
(204, 20, 46, 'prm', NULL, 14, 0, '2018-12-28 01:47:50', '2018-12-28 01:47:50'),
(205, 24, 46, 'prm', NULL, 14, 1, '2018-12-28 01:47:50', '2018-12-28 01:47:56'),
(206, 20, 47, 'prm', NULL, 14, 0, '2018-12-28 01:48:23', '2018-12-28 01:48:23'),
(207, 24, 47, 'prm', NULL, 14, 1, '2018-12-28 01:48:24', '2018-12-28 01:49:00'),
(208, 20, 48, 'prm', NULL, 14, 0, '2018-12-28 01:48:48', '2018-12-28 01:48:48'),
(209, 24, 48, 'prm', NULL, 14, 1, '2018-12-28 01:48:48', '2018-12-28 01:48:55'),
(210, 20, 49, 'prm', NULL, 14, 0, '2018-12-28 01:50:35', '2018-12-28 01:50:35'),
(211, 24, 49, 'prm', NULL, 14, 1, '2018-12-28 01:50:35', '2018-12-28 04:26:32'),
(212, 20, 50, 'prm', NULL, 14, 0, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(213, 24, 50, 'prm', NULL, 14, 1, '2018-12-28 01:52:44', '2018-12-28 04:26:32'),
(214, 20, 50, 'prm', NULL, 14, 0, '2018-12-28 01:52:44', '2018-12-28 01:52:44'),
(215, 24, 50, 'prm', NULL, 14, 1, '2018-12-28 01:52:44', '2018-12-28 01:52:47'),
(216, 24, 35, 'meetings', 40, NULL, 1, '2018-12-28 02:19:07', '2018-12-28 04:26:32'),
(217, 32, 35, 'meetings', 40, NULL, 0, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(218, 30, 35, 'meetings', 40, NULL, 0, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(219, 31, 35, 'meetings', 40, NULL, 0, '2018-12-28 02:19:08', '2018-12-28 02:19:08'),
(220, 27, 35, 'meetings', 40, NULL, 0, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(221, 20, 35, 'meetings', 40, NULL, 0, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(222, 22, 35, 'meetings', 40, NULL, 1, '2018-12-28 02:19:09', '2019-01-03 21:25:20'),
(223, 38, 35, 'meetings', 40, NULL, 0, '2018-12-28 02:19:09', '2018-12-28 02:19:09'),
(224, 24, 25, 'meetings', 40, NULL, 1, '2018-12-28 02:22:51', '2018-12-28 04:26:32'),
(225, 32, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(226, 30, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(227, 31, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:06', '2018-12-28 04:59:06'),
(228, 27, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(229, 24, 22, 'passwords', NULL, NULL, 1, '2018-12-28 04:59:07', '2018-12-28 04:59:32'),
(230, 25, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(231, 28, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:07', '2018-12-28 04:59:07'),
(232, 20, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(233, 22, 22, 'passwords', NULL, NULL, 1, '2018-12-28 04:59:08', '2019-01-03 21:25:20'),
(234, 23, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:08', '2018-12-28 04:59:08'),
(235, 50, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(236, 46, 22, 'passwords', NULL, NULL, 0, '2018-12-28 04:59:09', '2018-12-28 04:59:09'),
(237, 32, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:10', '2018-12-28 05:00:10'),
(238, 30, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(239, 31, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(240, 27, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:11', '2018-12-28 05:00:11'),
(241, 24, 51, 'passwords', NULL, NULL, 1, '2018-12-28 05:00:12', '2018-12-28 05:01:51'),
(242, 25, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(243, 28, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(244, 20, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:12', '2018-12-28 05:00:12'),
(245, 22, 51, 'passwords', NULL, NULL, 1, '2018-12-28 05:00:13', '2019-01-03 21:25:20'),
(246, 23, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(247, 50, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(248, 46, 51, 'passwords', NULL, NULL, 0, '2018-12-28 05:00:13', '2018-12-28 05:00:13'),
(249, 32, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:47', '2018-12-28 05:02:47'),
(250, 30, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(251, 31, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(252, 27, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:48', '2018-12-28 05:02:48'),
(253, 24, 52, 'passwords', NULL, NULL, 1, '2018-12-28 05:02:48', '2018-12-28 05:02:57'),
(254, 25, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(255, 28, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(256, 20, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:49', '2018-12-28 05:02:49'),
(257, 22, 52, 'passwords', NULL, NULL, 1, '2018-12-28 05:02:49', '2019-01-03 21:25:20'),
(258, 23, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(259, 50, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(260, 46, 52, 'passwords', NULL, NULL, 0, '2018-12-28 05:02:50', '2018-12-28 05:02:50'),
(261, 32, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(262, 30, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(263, 31, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:37', '2018-12-28 05:03:37'),
(264, 27, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(265, 24, 53, 'passwords', NULL, NULL, 1, '2018-12-28 05:03:38', '2018-12-28 05:03:49'),
(266, 25, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(267, 28, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:38', '2018-12-28 05:03:38'),
(268, 20, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(269, 22, 53, 'passwords', NULL, NULL, 1, '2018-12-28 05:03:39', '2019-01-03 21:25:20'),
(270, 23, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(271, 50, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:39', '2018-12-28 05:03:39'),
(272, 46, 53, 'passwords', NULL, NULL, 0, '2018-12-28 05:03:40', '2018-12-28 05:03:40'),
(273, 50, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:12', '2018-12-28 19:42:12'),
(274, 22, 39, 'prm', NULL, 15, 1, '2018-12-28 19:42:13', '2019-01-03 21:25:20'),
(275, 24, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(276, 28, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:13', '2018-12-28 19:42:13'),
(277, 31, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(278, 20, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(279, 50, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(280, 32, 39, 'prm', NULL, 15, 0, '2018-12-28 19:42:14', '2018-12-28 19:42:14'),
(281, 50, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:52', '2018-12-28 19:52:52'),
(282, 22, 46, 'prm', NULL, 15, 1, '2018-12-28 19:52:53', '2019-01-03 21:25:20'),
(283, 24, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(284, 28, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(285, 31, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:53', '2018-12-28 19:52:53'),
(286, 20, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(287, 50, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(288, 32, 46, 'prm', NULL, 15, 0, '2018-12-28 19:52:54', '2018-12-28 19:52:54'),
(289, 50, 44, 'prm', NULL, 15, 0, '2018-12-28 19:52:58', '2018-12-28 19:52:58'),
(290, 22, 44, 'prm', NULL, 15, 1, '2018-12-28 19:52:59', '2019-01-03 21:25:20'),
(291, 24, 44, 'prm', NULL, 15, 0, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(292, 28, 44, 'prm', NULL, 15, 1, '2018-12-28 19:52:59', '2018-12-29 03:35:26'),
(293, 31, 44, 'prm', NULL, 15, 0, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(294, 20, 44, 'prm', NULL, 15, 0, '2018-12-28 19:52:59', '2018-12-28 19:52:59'),
(295, 50, 44, 'prm', NULL, 15, 0, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(296, 32, 44, 'prm', NULL, 15, 0, '2018-12-28 19:53:00', '2018-12-28 19:53:00'),
(297, 50, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:23', '2018-12-28 19:57:23'),
(298, 22, 46, 'prm', NULL, 15, 1, '2018-12-28 19:57:24', '2019-01-03 21:25:20'),
(299, 24, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(300, 28, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(301, 31, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:24', '2018-12-28 19:57:24'),
(302, 20, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(303, 50, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(304, 32, 46, 'prm', NULL, 15, 0, '2018-12-28 19:57:25', '2018-12-28 19:57:25'),
(305, 50, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:28', '2018-12-28 19:57:28'),
(306, 22, 44, 'prm', NULL, 15, 1, '2018-12-28 19:57:29', '2019-01-03 21:25:20'),
(307, 24, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(308, 28, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(309, 31, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:29', '2018-12-28 19:57:29'),
(310, 20, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(311, 50, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(312, 32, 44, 'prm', NULL, 15, 0, '2018-12-28 19:57:30', '2018-12-28 19:57:30'),
(313, 50, 44, 'prm', NULL, 15, 0, '2018-12-28 20:28:53', '2018-12-28 20:28:53'),
(314, 22, 44, 'prm', NULL, 15, 1, '2018-12-28 20:28:54', '2018-12-28 22:51:56'),
(315, 24, 44, 'prm', NULL, 15, 0, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(316, 28, 44, 'prm', NULL, 15, 1, '2018-12-28 20:28:54', '2018-12-29 03:34:36'),
(317, 31, 44, 'prm', NULL, 15, 0, '2018-12-28 20:28:54', '2018-12-28 20:28:54'),
(318, 20, 44, 'prm', NULL, 15, 0, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(319, 50, 44, 'prm', NULL, 15, 0, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(320, 32, 44, 'prm', NULL, 15, 0, '2018-12-28 20:28:55', '2018-12-28 20:28:55'),
(321, 32, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:15', '2018-12-29 19:24:15'),
(322, 20, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(323, 30, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:16', '2018-12-29 19:24:16'),
(324, 31, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(325, 27, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:17', '2018-12-29 19:24:17'),
(326, 28, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(327, 20, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:18', '2018-12-29 19:24:18'),
(328, 22, 22, 'meetings', 41, NULL, 1, '2018-12-29 19:24:19', '2019-01-03 21:25:20'),
(329, 23, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(330, 50, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:19', '2018-12-29 19:24:19'),
(331, 46, 22, 'meetings', 41, NULL, 0, '2018-12-29 19:24:20', '2018-12-29 19:24:20'),
(332, 32, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(333, 20, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(334, 30, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:11', '2018-12-29 20:38:11'),
(335, 31, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(336, 27, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:12', '2018-12-29 20:38:12'),
(337, 28, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(338, 20, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:13', '2018-12-29 20:38:13'),
(339, 22, 22, 'meetings', 42, NULL, 1, '2018-12-29 20:38:14', '2019-01-03 21:25:20'),
(340, 23, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:14', '2018-12-29 20:38:14'),
(341, 50, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(342, 46, 22, 'meetings', 42, NULL, 0, '2018-12-29 20:38:15', '2018-12-29 20:38:15'),
(343, 32, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:19', '2018-12-29 20:42:19'),
(344, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(345, 27, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(346, 31, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(347, 30, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:20', '2018-12-29 20:42:20'),
(348, 28, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(349, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(350, 22, 29, 'meetings', 41, NULL, 1, '2018-12-29 20:42:21', '2019-01-03 21:25:20'),
(351, 23, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:21', '2018-12-29 20:42:21'),
(352, 50, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(353, 46, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:42:22', '2018-12-29 20:42:22'),
(354, 32, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(355, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(356, 27, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(357, 31, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:01', '2018-12-29 20:43:01'),
(358, 30, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(359, 28, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(360, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:02', '2018-12-29 20:43:02'),
(361, 22, 29, 'meetings', 41, NULL, 1, '2018-12-29 20:43:02', '2019-01-03 21:25:20'),
(362, 23, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(363, 50, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(364, 46, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:03', '2018-12-29 20:43:03'),
(365, 32, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(366, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(367, 27, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:31', '2018-12-29 20:43:31'),
(368, 31, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(369, 30, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(370, 28, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(371, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:32', '2018-12-29 20:43:32'),
(372, 22, 29, 'meetings', 41, NULL, 1, '2018-12-29 20:43:33', '2019-01-03 21:25:20'),
(373, 23, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:33', '2018-12-29 20:43:33'),
(374, 50, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(375, 46, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:43:34', '2018-12-29 20:43:34'),
(376, 32, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(377, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(378, 27, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(379, 31, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:24', '2018-12-29 20:44:24'),
(380, 30, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(381, 28, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(382, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:25', '2018-12-29 20:44:25'),
(383, 22, 29, 'meetings', 41, NULL, 1, '2018-12-29 20:44:25', '2019-01-03 21:25:20'),
(384, 23, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(385, 50, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(386, 46, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:26', '2018-12-29 20:44:26'),
(387, 32, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(388, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(389, 27, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:44:59', '2018-12-29 20:44:59'),
(390, 31, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(391, 30, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(392, 28, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(393, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:00', '2018-12-29 20:45:00'),
(394, 22, 29, 'meetings', 41, NULL, 1, '2018-12-29 20:45:01', '2019-01-03 21:25:20'),
(395, 23, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(396, 50, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:01', '2018-12-29 20:45:01'),
(397, 46, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:45:02', '2018-12-29 20:45:02'),
(398, 32, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:55', '2018-12-29 20:54:55'),
(399, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:55', '2018-12-29 20:54:55'),
(400, 27, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(401, 31, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(402, 30, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:56', '2018-12-29 20:54:56'),
(403, 28, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(404, 20, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(405, 22, 29, 'meetings', 41, NULL, 1, '2018-12-29 20:54:57', '2019-01-03 21:25:20'),
(406, 23, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:57', '2018-12-29 20:54:57'),
(407, 50, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(408, 46, 29, 'meetings', 41, NULL, 0, '2018-12-29 20:54:58', '2018-12-29 20:54:58'),
(409, 32, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:40', '2018-12-29 21:17:40'),
(410, 20, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(411, 27, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(412, 31, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(413, 30, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:41', '2018-12-29 21:17:41'),
(414, 28, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(415, 20, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(416, 22, 26, 'meetings', 41, NULL, 1, '2018-12-29 21:17:42', '2019-01-03 21:25:20'),
(417, 23, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:42', '2018-12-29 21:17:42'),
(418, 50, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(419, 46, 26, 'meetings', 41, NULL, 0, '2018-12-29 21:17:43', '2018-12-29 21:17:43'),
(420, 32, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:07', '2018-12-29 21:18:07'),
(421, 20, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(422, 27, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(423, 31, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(424, 30, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:08', '2018-12-29 21:18:08'),
(425, 28, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(426, 20, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(427, 22, 33, 'meetings', 41, NULL, 1, '2018-12-29 21:18:09', '2019-01-03 21:25:20'),
(428, 23, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:09', '2018-12-29 21:18:09'),
(429, 50, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(430, 46, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:18:10', '2018-12-29 21:18:10'),
(431, 32, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:48', '2018-12-29 21:27:48'),
(432, 20, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(433, 27, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(434, 31, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(435, 30, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:49', '2018-12-29 21:27:49'),
(436, 28, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(437, 20, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(438, 22, 34, 'meetings', 41, NULL, 1, '2018-12-29 21:27:50', '2019-01-03 21:25:20'),
(439, 23, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:50', '2018-12-29 21:27:50'),
(440, 50, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(441, 46, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:27:51', '2018-12-29 21:27:51'),
(442, 32, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(443, 20, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(444, 27, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:04', '2018-12-29 21:28:04'),
(445, 31, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(446, 30, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(447, 28, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:05', '2018-12-29 21:28:05'),
(448, 20, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(449, 22, 33, 'meetings', 41, NULL, 1, '2018-12-29 21:28:06', '2019-01-03 21:25:20'),
(450, 23, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(451, 50, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:06', '2018-12-29 21:28:06'),
(452, 46, 33, 'meetings', 41, NULL, 0, '2018-12-29 21:28:07', '2018-12-29 21:28:07'),
(453, 31, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:34', '2018-12-29 21:33:34'),
(454, 20, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(455, 22, 32, 'meetings', 38, NULL, 1, '2018-12-29 21:33:35', '2019-01-03 21:25:20'),
(456, 28, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(457, 46, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:35', '2018-12-29 21:33:35'),
(458, 31, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(459, 20, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:57', '2018-12-29 21:33:57'),
(460, 22, 32, 'meetings', 38, NULL, 1, '2018-12-29 21:33:57', '2019-01-03 21:25:20'),
(461, 28, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(462, 46, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:33:58', '2018-12-29 21:33:58'),
(463, 31, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(464, 20, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:16', '2018-12-29 21:34:16'),
(465, 22, 32, 'meetings', 38, NULL, 1, '2018-12-29 21:34:16', '2019-01-03 21:25:20'),
(466, 28, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(467, 46, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:17', '2018-12-29 21:34:17'),
(468, 31, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(469, 20, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(470, 22, 32, 'meetings', 38, NULL, 1, '2018-12-29 21:34:34', '2019-01-03 21:25:20'),
(471, 28, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:34', '2018-12-29 21:34:34'),
(472, 46, 32, 'meetings', 38, NULL, 0, '2018-12-29 21:34:35', '2018-12-29 21:34:35'),
(473, 32, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(474, 20, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(475, 27, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(476, 31, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:50', '2018-12-29 21:34:50'),
(477, 30, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(478, 28, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(479, 20, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(480, 22, 34, 'meetings', 41, NULL, 1, '2018-12-29 21:34:51', '2019-01-03 21:25:20'),
(481, 23, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:51', '2018-12-29 21:34:51'),
(482, 50, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(483, 46, 34, 'meetings', 41, NULL, 0, '2018-12-29 21:34:52', '2018-12-29 21:34:52'),
(484, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(485, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(486, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(487, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(488, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(489, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(490, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:47', '2018-12-29 21:36:47'),
(491, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:36:47', '2019-01-03 21:25:20'),
(492, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(493, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(494, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:36:48', '2018-12-29 21:36:48'),
(495, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(496, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(497, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(498, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(499, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(500, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(501, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:26', '2018-12-29 21:38:26'),
(502, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:38:27', '2019-01-03 21:25:20'),
(503, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(504, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(505, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:38:27', '2018-12-29 21:38:27'),
(506, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:39:58', '2018-12-29 21:39:58'),
(507, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:39:58', '2018-12-29 21:39:58'),
(508, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(509, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(510, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(511, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:39:59', '2018-12-29 21:39:59'),
(512, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(513, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:40:00', '2019-01-03 21:25:20'),
(514, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(515, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(516, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:00', '2018-12-29 21:40:00'),
(517, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:53', '2018-12-29 21:40:53'),
(518, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(519, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(520, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(521, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:54', '2018-12-29 21:40:54'),
(522, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(523, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(524, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:40:55', '2019-01-03 21:25:20'),
(525, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:55', '2018-12-29 21:40:55'),
(526, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(527, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:40:56', '2018-12-29 21:40:56'),
(528, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:50', '2018-12-29 21:41:50'),
(529, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:50', '2018-12-29 21:41:50'),
(530, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(531, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(532, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(533, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:51', '2018-12-29 21:41:51'),
(534, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(535, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:41:52', '2019-01-03 21:25:20'),
(536, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(537, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:52', '2018-12-29 21:41:52'),
(538, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:41:53', '2018-12-29 21:41:53'),
(539, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(540, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(541, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(542, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(543, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(544, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(545, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:52', '2018-12-29 21:42:52'),
(546, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:42:52', '2019-01-03 21:25:20'),
(547, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(548, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(549, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:42:53', '2018-12-29 21:42:53'),
(550, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(551, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(552, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(553, 31, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(554, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(555, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(556, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:51', '2018-12-29 21:43:51'),
(557, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:43:51', '2019-01-03 21:25:20'),
(558, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(559, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(560, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:43:52', '2018-12-29 21:43:52'),
(561, 32, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:49', '2018-12-29 21:44:49'),
(562, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(563, 27, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(564, 31, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:44:50', '2018-12-29 21:49:15'),
(565, 30, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:50', '2018-12-29 21:44:50'),
(566, 28, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(567, 20, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(568, 22, 35, 'meetings', 41, NULL, 1, '2018-12-29 21:44:51', '2019-01-03 21:25:20'),
(569, 23, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:51', '2018-12-29 21:44:51'),
(570, 50, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(571, 46, 35, 'meetings', 41, NULL, 0, '2018-12-29 21:44:52', '2018-12-29 21:44:52'),
(572, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(573, 22, 44, 'prm', NULL, 15, 1, '2018-12-31 19:07:28', '2019-01-03 21:25:20'),
(574, 24, 44, 'prm', NULL, 15, 0, '2018-12-31 19:07:28', '2018-12-31 19:07:28'),
(575, 28, 44, 'prm', NULL, 15, 1, '2018-12-31 19:07:29', '2019-01-02 11:23:42'),
(576, 31, 44, 'prm', NULL, 15, 0, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(577, 20, 44, 'prm', NULL, 15, 0, '2018-12-31 19:07:29', '2018-12-31 19:07:29'),
(578, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(579, 32, 44, 'prm', NULL, 15, 0, '2018-12-31 19:07:30', '2018-12-31 19:07:30'),
(580, 50, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(581, 22, 46, 'prm', NULL, 15, 1, '2018-12-31 19:11:27', '2019-01-03 21:25:20'),
(582, 24, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:27', '2018-12-31 19:11:27'),
(583, 28, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(584, 31, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(585, 20, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(586, 50, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:28', '2018-12-31 19:11:28'),
(587, 32, 46, 'prm', NULL, 15, 0, '2018-12-31 19:11:29', '2018-12-31 19:11:29'),
(588, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(589, 22, 44, 'prm', NULL, 15, 1, '2018-12-31 19:12:48', '2019-01-03 21:25:20'),
(590, 24, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(591, 28, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(592, 31, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(593, 20, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(594, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(595, 32, 44, 'prm', NULL, 15, 0, '2018-12-31 19:12:49', '2018-12-31 19:12:49'),
(596, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(597, 22, 44, 'prm', NULL, 15, 1, '2018-12-31 19:18:21', '2019-01-03 21:25:20'),
(598, 24, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(599, 28, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(600, 31, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(601, 20, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(602, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:22', '2018-12-31 19:18:22'),
(603, 32, 44, 'prm', NULL, 15, 0, '2018-12-31 19:18:23', '2018-12-31 19:18:23'),
(604, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(605, 22, 44, 'prm', NULL, 15, 1, '2018-12-31 19:21:15', '2019-01-03 21:25:20'),
(606, 24, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(607, 28, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(608, 31, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(609, 20, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(610, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:16', '2018-12-31 19:21:16'),
(611, 32, 44, 'prm', NULL, 15, 0, '2018-12-31 19:21:17', '2018-12-31 19:21:17');
INSERT INTO `notifications` (`id`, `user_id`, `notif_id`, `daftar`, `meeting_id`, `project_id`, `status`, `created_at`, `updated_at`) VALUES
(612, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:57', '2018-12-31 19:23:57'),
(613, 22, 44, 'prm', NULL, 15, 1, '2018-12-31 19:23:57', '2019-01-03 21:25:20'),
(614, 24, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(615, 28, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(616, 31, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(617, 20, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:58', '2018-12-31 19:23:58'),
(618, 50, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(619, 32, 44, 'prm', NULL, 15, 0, '2018-12-31 19:23:59', '2018-12-31 19:23:59'),
(620, 50, 45, 'prm', NULL, 15, 0, '2018-12-31 19:27:30', '2018-12-31 19:27:30'),
(621, 22, 45, 'prm', NULL, 15, 1, '2018-12-31 19:27:31', '2019-01-03 21:25:20'),
(622, 24, 45, 'prm', NULL, 15, 0, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(623, 28, 45, 'prm', NULL, 15, 1, '2018-12-31 19:27:31', '2019-01-02 11:23:19'),
(624, 31, 45, 'prm', NULL, 15, 0, '2018-12-31 19:27:31', '2018-12-31 19:27:31'),
(625, 20, 45, 'prm', NULL, 15, 0, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(626, 50, 45, 'prm', NULL, 15, 0, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(627, 32, 45, 'prm', NULL, 15, 0, '2018-12-31 19:27:32', '2018-12-31 19:27:32'),
(628, 32, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:24', '2019-01-02 04:34:24'),
(629, 20, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:24', '2019-01-02 04:34:24'),
(630, 30, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(631, 31, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(632, 27, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(633, 24, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:25', '2019-01-02 04:34:25'),
(634, 28, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(635, 20, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(636, 22, 23, 'meetings', 42, NULL, 1, '2019-01-02 04:34:26', '2019-01-03 21:25:20'),
(637, 23, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(638, 50, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:26', '2019-01-02 04:34:26'),
(639, 46, 23, 'meetings', 42, NULL, 0, '2019-01-02 04:34:27', '2019-01-02 04:34:27'),
(640, 24, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:04', '2019-01-02 04:36:04'),
(641, 32, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(642, 30, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(643, 31, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(644, 27, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:05', '2019-01-02 04:36:05'),
(645, 20, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(646, 22, 24, 'meetings', 40, NULL, 1, '2019-01-02 04:36:06', '2019-01-03 21:25:20'),
(647, 38, 24, 'meetings', 40, NULL, 0, '2019-01-02 04:36:06', '2019-01-02 04:36:06'),
(648, 24, 22, 'meetings', 43, NULL, 0, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(649, 23, 22, 'meetings', 43, NULL, 0, '2019-01-02 04:36:44', '2019-01-02 04:36:44'),
(650, 32, 22, 'meetings', 43, NULL, 0, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(651, 30, 22, 'meetings', 43, NULL, 0, '2019-01-02 04:36:45', '2019-01-02 04:36:45'),
(652, 51, 1, 'hrm', NULL, NULL, 0, '2019-01-02 04:38:55', '2019-01-02 04:38:55'),
(653, 24, 23, 'meetings', 43, NULL, 0, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(654, 23, 23, 'meetings', 43, NULL, 0, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(655, 32, 23, 'meetings', 43, NULL, 0, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(656, 30, 23, 'meetings', 43, NULL, 0, '2019-01-02 04:41:03', '2019-01-02 04:41:03'),
(657, 51, 23, 'meetings', 43, NULL, 0, '2019-01-02 04:41:04', '2019-01-02 04:41:04'),
(658, 24, 26, 'meetings', 43, NULL, 0, '2019-01-02 04:41:48', '2019-01-02 04:41:48'),
(659, 23, 26, 'meetings', 43, NULL, 0, '2019-01-02 04:41:52', '2019-01-02 04:41:52'),
(660, 51, 26, 'meetings', 43, NULL, 0, '2019-01-02 04:41:52', '2019-01-02 04:41:52'),
(661, 30, 26, 'meetings', 43, NULL, 0, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(662, 32, 26, 'meetings', 43, NULL, 0, '2019-01-02 04:41:53', '2019-01-02 04:41:53'),
(663, 24, 23, 'meetings', 43, NULL, 0, '2019-01-02 05:19:17', '2019-01-02 05:19:17'),
(664, 23, 23, 'meetings', 43, NULL, 0, '2019-01-02 05:19:17', '2019-01-02 05:19:17'),
(665, 32, 23, 'meetings', 43, NULL, 0, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(666, 30, 23, 'meetings', 43, NULL, 0, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(667, 51, 23, 'meetings', 43, NULL, 0, '2019-01-02 05:19:18', '2019-01-02 05:19:18'),
(668, 24, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:01', '2019-01-02 05:20:01'),
(669, 23, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:01', '2019-01-02 05:20:01'),
(670, 51, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(671, 30, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(672, 32, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:02', '2019-01-02 05:20:02'),
(673, 24, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:15', '2019-01-02 05:20:15'),
(674, 23, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(675, 51, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(676, 30, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(677, 32, 35, 'meetings', 43, NULL, 0, '2019-01-02 05:20:16', '2019-01-02 05:20:16'),
(678, 24, 36, 'meetings', 43, NULL, 0, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(679, 23, 36, 'meetings', 43, NULL, 0, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(680, 51, 36, 'meetings', 43, NULL, 0, '2019-01-02 05:54:00', '2019-01-02 05:54:00'),
(681, 30, 36, 'meetings', 43, NULL, 0, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(682, 32, 36, 'meetings', 43, NULL, 0, '2019-01-02 05:54:01', '2019-01-02 05:54:01'),
(683, 24, 25, 'meetings', 43, NULL, 0, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(684, 23, 25, 'meetings', 43, NULL, 0, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(685, 51, 25, 'meetings', 43, NULL, 0, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(686, 30, 25, 'meetings', 43, NULL, 0, '2019-01-02 06:12:18', '2019-01-02 06:12:18'),
(687, 32, 25, 'meetings', 43, NULL, 0, '2019-01-02 06:12:19', '2019-01-02 06:12:19'),
(688, 32, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(689, 20, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(690, 30, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:49', '2019-01-02 06:14:49'),
(691, 31, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(692, 27, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(693, 28, 25, 'meetings', 42, NULL, 1, '2019-01-02 06:14:50', '2019-01-02 11:22:33'),
(694, 20, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:50', '2019-01-02 06:14:50'),
(695, 22, 25, 'meetings', 42, NULL, 1, '2019-01-02 06:14:51', '2019-01-03 09:18:20'),
(696, 23, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(697, 50, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(698, 46, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:51', '2019-01-02 06:14:51'),
(699, 24, 25, 'meetings', 42, NULL, 0, '2019-01-02 06:14:52', '2019-01-02 06:14:52'),
(700, 20, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:13', '2019-01-03 09:45:13'),
(701, 31, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:13', '2019-01-03 09:45:13'),
(702, 28, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(703, 24, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(704, 27, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:14', '2019-01-03 09:45:14'),
(705, 22, 46, 'prm', NULL, 6, 1, '2019-01-03 09:45:14', '2019-01-03 21:25:20'),
(706, 20, 46, 'prm', NULL, 14, 0, '2019-01-03 09:45:25', '2019-01-03 09:45:25'),
(707, 24, 46, 'prm', NULL, 14, 0, '2019-01-03 09:45:25', '2019-01-03 09:45:25'),
(708, 20, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:48', '2019-01-03 09:45:48'),
(709, 31, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:48', '2019-01-03 09:45:48'),
(710, 28, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(711, 24, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(712, 27, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:49', '2019-01-03 09:45:49'),
(713, 22, 46, 'prm', NULL, 6, 1, '2019-01-03 09:45:49', '2019-01-03 21:25:20'),
(714, 20, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:58', '2019-01-03 09:45:58'),
(715, 31, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(716, 28, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(717, 24, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(718, 27, 46, 'prm', NULL, 6, 0, '2019-01-03 09:45:59', '2019-01-03 09:45:59'),
(719, 22, 46, 'prm', NULL, 6, 1, '2019-01-03 09:46:00', '2019-01-03 21:25:20'),
(720, 30, 40, 'prm', NULL, 13, 0, '2019-01-03 09:49:47', '2019-01-03 09:49:47'),
(721, 22, 40, 'prm', NULL, 13, 1, '2019-01-03 09:49:48', '2019-01-03 21:25:20'),
(722, 20, 40, 'prm', NULL, 13, 0, '2019-01-03 09:49:48', '2019-01-03 09:49:48'),
(723, 32, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(724, 30, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(725, 31, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:54', '2019-01-03 22:39:54'),
(726, 27, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(727, 24, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(728, 25, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(729, 28, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:55', '2019-01-03 22:39:55'),
(730, 20, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(731, 22, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(732, 23, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(733, 50, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:56', '2019-01-03 22:39:56'),
(734, 46, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(735, 51, 51, 'passwords', NULL, NULL, 0, '2019-01-03 22:39:57', '2019-01-03 22:39:57'),
(736, 32, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:50', '2019-01-03 22:41:50'),
(737, 30, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(738, 31, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(739, 27, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(740, 24, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:51', '2019-01-03 22:41:51'),
(741, 25, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(742, 28, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(743, 20, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(744, 22, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:52', '2019-01-03 22:41:52'),
(745, 23, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(746, 50, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(747, 46, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(748, 51, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:41:53', '2019-01-03 22:41:53'),
(749, 32, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:43', '2019-01-03 22:45:43'),
(750, 30, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(751, 31, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(752, 27, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(753, 24, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:44', '2019-01-03 22:45:44'),
(754, 25, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(755, 28, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(756, 20, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(757, 22, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(758, 23, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:45', '2019-01-03 22:45:45'),
(759, 50, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(760, 46, 52, 'passwords', NULL, NULL, 0, '2019-01-03 22:45:46', '2019-01-03 22:45:46'),
(761, 51, 52, 'passwords', NULL, NULL, 1, '2019-01-03 22:45:46', '2019-01-03 23:32:17'),
(762, 32, 22, 'meetings', 44, NULL, 0, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(763, 30, 22, 'meetings', 44, NULL, 0, '2019-01-04 01:16:27', '2019-01-04 01:16:27'),
(764, 32, 22, 'meetings', 44, NULL, 0, '2019-01-04 01:16:28', '2019-01-04 01:16:28'),
(765, 32, 29, 'meetings', 44, NULL, 0, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(766, 30, 29, 'meetings', 44, NULL, 0, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(767, 32, 29, 'meetings', 44, NULL, 0, '2019-01-04 01:17:42', '2019-01-04 01:17:42'),
(768, 32, 29, 'meetings', 44, NULL, 0, '2019-01-04 01:17:59', '2019-01-04 01:17:59'),
(769, 30, 29, 'meetings', 44, NULL, 0, '2019-01-04 01:17:59', '2019-01-04 01:17:59'),
(770, 32, 29, 'meetings', 44, NULL, 0, '2019-01-04 01:18:00', '2019-01-04 01:18:00'),
(771, 32, 26, 'meetings', 44, NULL, 0, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(772, 30, 26, 'meetings', 44, NULL, 0, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(773, 32, 26, 'meetings', 44, NULL, 0, '2019-01-04 01:18:44', '2019-01-04 01:18:44'),
(774, 32, 33, 'meetings', 44, NULL, 0, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(775, 30, 33, 'meetings', 44, NULL, 0, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(776, 32, 33, 'meetings', 44, NULL, 0, '2019-01-04 01:20:09', '2019-01-04 01:20:09'),
(777, 32, 34, 'meetings', 44, NULL, 0, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(778, 30, 34, 'meetings', 44, NULL, 0, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(779, 32, 34, 'meetings', 44, NULL, 0, '2019-01-04 01:20:53', '2019-01-04 01:20:53'),
(780, 32, 33, 'meetings', 44, NULL, 0, '2019-01-04 01:21:03', '2019-01-04 01:21:03'),
(781, 30, 33, 'meetings', 44, NULL, 0, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(782, 32, 33, 'meetings', 44, NULL, 0, '2019-01-04 01:21:04', '2019-01-04 01:21:04'),
(783, 32, 34, 'meetings', 44, NULL, 0, '2019-01-04 01:21:49', '2019-01-04 01:21:49'),
(784, 30, 34, 'meetings', 44, NULL, 0, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(785, 32, 34, 'meetings', 44, NULL, 0, '2019-01-04 01:21:50', '2019-01-04 01:21:50'),
(786, 32, 35, 'meetings', 44, NULL, 0, '2019-01-04 01:22:31', '2019-01-04 01:22:31'),
(787, 30, 35, 'meetings', 44, NULL, 0, '2019-01-04 01:22:31', '2019-01-04 01:22:31'),
(788, 32, 35, 'meetings', 44, NULL, 0, '2019-01-04 01:22:32', '2019-01-04 01:22:32'),
(789, 32, 30, 'meetings', 44, NULL, 0, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(790, 30, 30, 'meetings', 44, NULL, 0, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(791, 32, 30, 'meetings', 44, NULL, 0, '2019-01-04 01:26:34', '2019-01-04 01:26:34'),
(792, 50, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(793, 27, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(794, 24, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:25', '2019-01-04 01:58:25'),
(795, 31, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(796, 20, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(797, 23, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:26', '2019-01-04 01:58:26'),
(798, 50, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:35', '2019-01-04 01:58:35'),
(799, 22, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(800, 27, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(801, 24, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:36', '2019-01-04 01:58:36'),
(802, 31, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(803, 20, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(804, 23, 40, 'prm', NULL, 7, 0, '2019-01-04 01:58:37', '2019-01-04 01:58:37'),
(805, 20, 44, 'prm', NULL, 7, 0, '2019-01-04 02:00:22', '2019-01-04 02:00:22'),
(806, 31, 44, 'prm', NULL, 7, 0, '2019-01-04 02:00:22', '2019-01-04 02:00:22'),
(807, 24, 44, 'prm', NULL, 7, 0, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(808, 27, 44, 'prm', NULL, 7, 0, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(809, 22, 44, 'prm', NULL, 7, 1, '2019-01-04 02:00:23', '2019-01-04 02:11:04'),
(810, 50, 44, 'prm', NULL, 7, 0, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(811, 23, 44, 'prm', NULL, 7, 0, '2019-01-04 02:00:23', '2019-01-04 02:00:23'),
(812, 24, 29, 'meetings', 43, NULL, 0, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(813, 23, 29, 'meetings', 43, NULL, 0, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(814, 51, 29, 'meetings', 43, NULL, 0, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(815, 30, 29, 'meetings', 43, NULL, 0, '2019-01-04 22:21:08', '2019-01-04 22:21:08'),
(816, 32, 29, 'meetings', 43, NULL, 0, '2019-01-04 22:21:09', '2019-01-04 22:21:09'),
(817, 24, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(818, 23, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(819, 32, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(820, 30, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:21:29', '2019-01-04 22:21:29'),
(821, 51, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:21:30', '2019-01-04 22:21:30'),
(822, 24, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(823, 23, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(824, 32, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(825, 30, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:50:34', '2019-01-04 22:50:34'),
(826, 51, 23, 'meetings', 43, NULL, 0, '2019-01-04 22:50:35', '2019-01-04 22:50:35'),
(827, 32, 24, 'meetings', 44, NULL, 0, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(828, 30, 24, 'meetings', 44, NULL, 0, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(829, 32, 24, 'meetings', 44, NULL, 0, '2019-01-05 00:18:51', '2019-01-05 00:18:51'),
(830, 24, 24, 'meetings', 43, NULL, 0, '2019-01-05 00:19:09', '2019-01-05 00:19:09'),
(831, 23, 24, 'meetings', 43, NULL, 0, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(832, 51, 24, 'meetings', 43, NULL, 0, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(833, 30, 24, 'meetings', 43, NULL, 0, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(834, 32, 24, 'meetings', 43, NULL, 0, '2019-01-05 00:19:10', '2019-01-05 00:19:10'),
(835, 20, 40, 'prm', NULL, 10, 0, '2019-01-05 00:26:39', '2019-01-05 00:26:39'),
(836, 22, 40, 'prm', NULL, 10, 0, '2019-01-05 00:26:39', '2019-01-05 00:26:39'),
(837, 20, 40, 'prm', NULL, 10, 0, '2019-01-05 00:26:55', '2019-01-05 00:26:55'),
(838, 22, 40, 'prm', NULL, 10, 0, '2019-01-05 00:26:55', '2019-01-05 00:26:55'),
(839, 20, 40, 'prm', NULL, 10, 0, '2019-01-05 00:27:14', '2019-01-05 00:27:14'),
(840, 22, 40, 'prm', NULL, 10, 0, '2019-01-05 00:27:15', '2019-01-05 00:27:15'),
(841, 22, 46, 'prm', NULL, 10, 0, '2019-01-05 00:31:20', '2019-01-05 00:31:20'),
(842, 20, 46, 'prm', NULL, 10, 0, '2019-01-05 00:31:20', '2019-01-05 00:31:20'),
(843, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:31:29', '2019-01-05 00:31:29'),
(844, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:31:29', '2019-01-05 00:31:29'),
(845, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:32:35', '2019-01-05 00:32:35'),
(846, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:32:35', '2019-01-05 00:32:35'),
(847, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:33:08', '2019-01-05 00:33:08'),
(848, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:33:09', '2019-01-05 00:33:09'),
(849, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:35:52', '2019-01-05 00:35:52'),
(850, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:35:52', '2019-01-05 00:35:52'),
(851, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:36:10', '2019-01-05 00:36:10'),
(852, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:36:10', '2019-01-05 00:36:10'),
(853, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:36:45', '2019-01-05 00:36:45'),
(854, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:36:45', '2019-01-05 00:36:45'),
(855, 22, 45, 'prm', NULL, 10, 0, '2019-01-05 00:38:41', '2019-01-05 00:38:41'),
(856, 20, 45, 'prm', NULL, 10, 0, '2019-01-05 00:38:41', '2019-01-05 00:38:41'),
(857, 30, 40, 'prm', NULL, 9, 0, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(858, 22, 40, 'prm', NULL, 9, 0, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(859, 20, 40, 'prm', NULL, 9, 0, '2019-01-05 00:51:41', '2019-01-05 00:51:41'),
(860, 32, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:05', '2019-01-05 00:55:05'),
(861, 30, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:05', '2019-01-05 00:55:05'),
(862, 31, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(863, 27, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(864, 24, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(865, 25, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:06', '2019-01-05 00:55:06'),
(866, 28, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(867, 20, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(868, 22, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(869, 23, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:07', '2019-01-05 00:55:07'),
(870, 50, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(871, 46, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(872, 51, 53, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:08', '2019-01-05 00:55:08'),
(873, 32, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:43', '2019-01-05 00:55:43'),
(874, 30, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(875, 31, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(876, 27, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:44', '2019-01-05 00:55:44'),
(877, 24, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(878, 25, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(879, 28, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(880, 20, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(881, 22, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:45', '2019-01-05 00:55:45'),
(882, 23, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(883, 50, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(884, 46, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(885, 51, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:55:46', '2019-01-05 00:55:46'),
(886, 32, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:31', '2019-01-05 00:56:31'),
(887, 30, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:31', '2019-01-05 00:56:31'),
(888, 31, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(889, 27, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(890, 24, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(891, 25, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:32', '2019-01-05 00:56:32'),
(892, 28, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(893, 20, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(894, 22, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:33', '2019-01-05 00:56:33'),
(895, 23, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(896, 50, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(897, 46, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:34', '2019-01-05 00:56:34'),
(898, 51, 52, 'passwords', NULL, NULL, 0, '2019-01-05 00:56:34', '2019-01-05 00:56:34');

-- --------------------------------------------------------

--
-- Structure de la table `notifs`
--

CREATE TABLE `notifs` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `daftar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meeting_id` int(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app` tinyint(4) NOT NULL DEFAULT '0',
  `email` tinyint(4) NOT NULL DEFAULT '0',
  `sms` tinyint(4) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `notifs`
--

INSERT INTO `notifs` (`id`, `company_id`, `daftar`, `meeting_id`, `name`, `title`, `app`, `email`, `sms`, `message`, `created_at`, `updated_at`) VALUES
(1, 12, 'hrm', NULL, 'add_user', 'إضافة موظف', 1, 1, 1, 'إضافة موظف', '2018-12-25 14:53:52', '2019-01-04 21:18:21'),
(2, 12, 'hrm', NULL, 'delete_user', 'حذف موظف', 1, 1, 1, 'حذف موظف', '2018-12-25 16:44:08', '2019-01-04 21:18:21'),
(3, 12, 'hrm', NULL, 'add_allowance', 'إضافة بدل', 1, 1, 1, 'إضافة بدل', '2018-12-25 16:46:43', '2019-01-04 21:18:21'),
(4, 12, 'hrm', NULL, 'delete_allowance', 'حذف بدل', 1, 1, 1, 'حذف بدل', '2018-12-25 16:47:34', '2019-01-04 21:18:21'),
(5, 12, 'hrm', NULL, 'add_deduction', 'إضافة إقتطاع', 1, 1, 1, 'إضافة إقتطاع', '2018-12-25 16:48:10', '2019-01-03 01:22:30'),
(6, 12, 'hrm', NULL, 'delete_deduction', 'حذف إقتطاع', 1, 1, 1, 'حذف إقتطاع', '2018-12-25 16:48:35', '2019-01-03 01:22:16'),
(7, 12, 'hrm', NULL, 'add_advance', 'إضافة سلفة', 1, 1, 1, 'إضافة سلفة', '2018-12-25 16:49:33', '2019-01-03 01:22:16'),
(8, 12, 'hrm', NULL, 'delete_advance', 'حذف سلفة', 1, 1, 1, 'حذف سلفة', '2018-12-25 16:50:08', '2019-01-03 01:22:16'),
(9, 12, 'hrm', NULL, 'add_custody', 'إضافة عهدة', 1, 1, 1, 'إضافة عهدة', '2018-12-25 16:51:39', '2019-01-03 01:22:16'),
(10, 12, 'hrm', NULL, 'deliver_custody', 'إستلام عهدة', 1, 1, 1, 'إستلام عهدة', '2018-12-25 16:52:11', '2019-01-03 01:22:16'),
(11, 12, 'hrm', NULL, 'delete_custody', 'حذف عهدة', 1, 1, 1, 'حذف عهدة', '2018-12-25 16:52:46', '2019-01-03 01:22:16'),
(12, 12, 'hrm', NULL, 'add_vacation', 'إجازة جديدة', 1, 1, 1, 'إجازة جديدة', '2018-12-25 16:53:39', '2019-01-03 01:22:16'),
(13, 12, 'hrm', NULL, 'accept_vacation', 'قبول إجازة', 1, 1, 1, 'قبول إجازة', '2018-12-25 16:54:05', '2019-01-03 01:22:16'),
(14, 12, 'hrm', NULL, 'reject_vacation', 'رفض إجازة', 1, 1, 1, 'رفض إجازة', '2018-12-25 16:54:34', '2019-01-03 01:22:16'),
(15, 12, 'hrm', NULL, 'add_training', 'إضافة دورة تدريبية', 1, 1, 1, 'إضافة دورة تدريبية', '2018-12-25 16:55:15', '2019-01-03 01:22:16'),
(16, 12, 'hrm', NULL, 'delete_training', 'حذف دورة تدريبية', 1, 1, 1, 'حذف دورة تدريبية', '2018-12-25 16:55:50', '2019-01-03 01:21:30'),
(17, 12, 'hrm', NULL, 'add_mandate', 'إضافة إنتداب', 1, 1, 1, 'إضافة إنتداب', '2018-12-25 16:56:25', '2019-01-03 01:21:30'),
(18, 12, 'hrm', NULL, 'delete_mandate', 'حذف إنتداب', 1, 1, 1, 'حذف إنتداب', '2018-12-25 16:57:04', '2019-01-03 01:21:30'),
(20, 12, 'hrm', NULL, 'send_accounting', 'مسائلة جديدة', 1, 1, 1, 'مسائلة جديدة', '2018-12-25 16:57:04', '2019-01-03 01:21:30'),
(22, 12, 'meetings', NULL, 'add_meeting', 'إضافة إجتماع جديد', 1, 1, 1, 'إضافة إجتماع جديد', '2018-12-27 20:52:26', '2019-01-04 00:36:04'),
(23, 12, 'meetings', NULL, 'update_meeting', 'تعديل إجتماع', 1, 1, 1, 'تعديل إجتماع', '2018-12-27 20:54:15', '2019-01-04 00:35:53'),
(24, 12, 'meetings', NULL, 'delete_meeting', 'حذف إجتماع', 1, 1, 1, 'حذف إجتماع', '2018-12-27 20:56:18', '2019-01-04 00:35:53'),
(25, 12, 'meetings', NULL, 'remind_meeting', 'تذكير بإجتماع', 1, 1, 1, 'تذكير بإجتماع', '2018-12-27 20:57:55', '2019-01-04 00:35:53'),
(26, 12, 'meetings', NULL, 'accept_meeting', 'بدء إجتماع', 1, 1, 1, 'بدء إجتماع', '2018-12-27 21:00:17', '2019-01-04 00:35:53'),
(27, 12, 'meetings', NULL, 'cancel_meeting', 'إلغاء إجتماع', 1, 1, 1, 'إلغاء إجتماع', '2018-12-27 21:01:05', '2019-01-04 00:35:53'),
(28, 12, 'meetings', NULL, 'delay_meeting', 'تأجيل الإجتماع', 1, 1, 1, 'تأجيل الإجتماع', '2018-12-27 21:01:51', '2019-01-04 00:35:53'),
(29, 12, 'meetings', NULL, 'save_axis', 'إضافة محور جديد', 1, 1, 1, 'إضافة محور جديد', '2018-12-27 21:05:37', '2019-01-04 00:35:53'),
(30, 12, 'meetings', NULL, 'update_axis', 'تعديل محور', 1, 1, 1, 'تعديل محور', '2018-12-27 23:04:20', '2019-01-04 00:35:53'),
(31, 12, 'meetings', NULL, 'delete_axis', 'حذف محور', 1, 1, 1, 'حذف محور', '2018-12-27 23:06:01', '2019-01-04 00:35:53'),
(32, 12, 'meetings', NULL, 'delay_axis', 'تأجيل محور', 1, 1, 1, 'تأجيل محور', '2018-12-27 23:08:14', '2019-01-04 00:35:53'),
(33, 12, 'meetings', NULL, 'start_axis', 'بدء محور', 1, 1, 1, 'بدء محور', '2018-12-27 23:10:03', '2019-01-04 00:35:53'),
(34, 12, 'meetings', NULL, 'finish_axis', 'إنهاء محور', 1, 1, 1, 'إنهاء محور', '2018-12-27 23:10:47', '2019-01-04 00:35:53'),
(35, 12, 'meetings', NULL, 'add_meeting_task', 'إضافة مهمة في إجتماع', 1, 1, 1, 'إضافة مهمة في إجتماع', '2018-12-27 23:12:34', '2019-01-04 00:35:53'),
(36, 12, 'meetings', NULL, 'update_meeting_task', 'تعديل مهمة في إجتماع', 1, 1, 1, 'تعديل مهمة في إجتماع', '2018-12-27 23:13:15', '2019-01-04 00:35:53'),
(37, 12, 'meetings', NULL, 'delete_meeting_task', 'حذف مهمة في إجتماع', 1, 1, 1, 'حذف مهمة في إجتماع', '2018-12-27 23:14:29', '2019-01-04 00:35:58'),
(38, 12, 'meetings', NULL, 'add_file_meeting', 'إضافة ملف في إجتماع', 1, 1, 1, 'إضافة ملف في إجتماع', '2018-12-27 23:16:27', '2019-01-04 00:35:58'),
(39, 12, 'prm', NULL, 'add_project', 'إضافة مشروع جديد', 1, 1, 1, 'إضافة مشروع جديد', '2018-12-28 00:38:46', '2019-01-04 03:42:45'),
(40, 12, 'prm', NULL, 'update_project', 'تعديل مشروع', 1, 1, 1, 'تعديل مشروع', '2018-12-28 00:39:30', '2019-01-03 00:53:09'),
(41, 12, 'prm', NULL, 'delete_project', 'حذف مشروع', 1, 1, 1, 'حذف مشروع', '2018-12-28 00:40:23', '2019-01-03 00:53:09'),
(42, 12, 'prm', NULL, 'add_contributer', 'إضافة مساعد في مشروع', 1, 1, 1, 'إضافة مساعد في مشروع', '2018-12-28 00:41:41', '2019-01-04 03:42:45'),
(43, 12, 'prm', NULL, 'delete_contributer', 'حذف مساعد في مشروع', 1, 1, 1, 'حذف مساعد في مشروع', '2018-12-28 00:42:12', '2019-01-04 03:42:45'),
(44, 12, 'prm', NULL, 'add_task_prm', 'إضافة مهمة في مشروع', 1, 1, 1, 'إضافة مهمة في مشروع', '2018-12-28 00:44:11', '2019-01-03 00:53:09'),
(45, 12, 'prm', NULL, 'update_task_prm', 'تعديل مهمة في مشروع', 1, 1, 1, 'تعديل مهمة في مشروع', '2018-12-28 00:44:49', '2019-01-03 00:53:09'),
(46, 12, 'prm', NULL, 'delete_task_prm', 'حذف مهمة في مشروع', 1, 1, 1, 'حذف مهمة في مشروع', '2018-12-28 00:46:03', '2019-01-03 00:53:09'),
(47, 12, 'prm', NULL, 'add_milestone_prm', 'إضافة مرحلة في مشروع', 1, 1, 1, 'إضافة مرحلة في مشروع', '2018-12-28 00:47:11', '2019-01-03 00:53:09'),
(48, 12, 'prm', NULL, 'delete_milestone_prm', 'حذف مرحلة في مشروع', 1, 1, 1, 'حذف مرحلة في مشروع', '2018-12-28 00:47:43', '2019-01-03 00:53:09'),
(49, 12, 'prm', NULL, 'add_discussion_prm', 'إضافة نقاش في مشروع', 1, 1, 1, 'إضافة نقاش في مشروع', '2018-12-28 00:49:13', '2019-01-03 00:53:09'),
(50, 12, 'prm', NULL, 'add_file_prm', 'إضافة ملف في مشروع', 1, 1, 1, 'إضافة ملف في مشروع', '2018-12-28 00:50:44', '2019-01-04 03:42:45'),
(51, 12, 'passwords', NULL, 'add_website', 'إضافة موقع', 1, 1, 1, 'إضافة موقع', '2018-12-28 04:47:11', '2019-01-04 03:45:33'),
(52, 12, 'passwords', NULL, 'update_website', 'تعديل موقع', 1, 1, 1, 'تعديل موقع', '2018-12-28 04:48:44', '2019-01-04 03:45:33'),
(53, 12, 'passwords', NULL, 'delete_website', 'حذف موقع', 1, 1, 1, 'حذف موقع', '2018-12-28 04:49:40', '2019-01-04 03:45:33');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('user1@user.com', '$2y$10$otyeRF02WFW8wFPX0NumAONNbkxjBXNDM.oHAAEFENduDQ0w4Oi3S', '2018-10-26 21:09:36'),
('ENJAZ@ccreation.sa', '$2y$10$O/wN1LDcmKOWDPeK9vBZl..dpNX919NdEIHUfpxAZhdVs84ZgUzg.', '2018-11-20 16:21:29'),
('sa@ccreation.sa', '$2y$10$9GpG98uRQCBUUgHpnPQ0zODO2UQzAxjriuhfmaHmyLBDnGYrGbPji', '2018-12-25 17:58:38'),
('devweb218@gmail.com', '$2y$10$Y0yeZkq5/S7mtbicVo1HI.Cwz//jhPwPUb4ZaqHyd647HwyxbLvTq', '2018-12-01 17:31:04');

-- --------------------------------------------------------

--
-- Structure de la table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(12) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_on_invoice` tinyint(4) NOT NULL DEFAULT '0',
  `minimum_payment_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `company_id`, `title`, `description`, `available_on_invoice`, `minimum_payment_amount`, `created_at`, `updated_at`) VALUES
(1, 1, 'PayPal Payments Standard', 'PayPal Payments Standard Online Payments', 0, '0', '2018-11-28 14:17:42', '2018-11-28 14:17:42'),
(2, 1, 'Stripe', 'Stripe online payments', 1, '0', '2018-11-28 14:17:51', '2018-11-28 14:29:08'),
(3, 1, 'إيداع بنكي', 'حساب الشركة بمصرف الإنماء', 1, '100', '2018-11-28 14:29:25', '2018-11-28 14:29:25'),
(4, 1, 'تحويل بنكي', 'حساب الشركة في مصرف الإنماء', 0, '0', '2018-11-28 14:29:40', '2018-11-28 14:29:40'),
(6, 12, 'edgryer', 'ertert', 0, '10', '2019-01-04 03:32:28', '2019-01-04 03:37:34'),
(7, 12, 'qsdq', 'qsdqsd', 0, '77', '2019-01-04 03:37:22', '2019-01-04 03:37:22');

-- --------------------------------------------------------

--
-- Structure de la table `percentages`
--

CREATE TABLE `percentages` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `percentage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `percentages`
--

INSERT INTO `percentages` (`id`, `company_id`, `percentage`, `created_at`, `updated_at`) VALUES
(1, 1, '20', '2018-11-27 18:03:28', '2018-11-27 18:12:37'),
(2, 1, '46.1', '2018-11-27 18:11:45', '2018-11-27 18:11:45'),
(3, 1, '88', '2018-11-27 18:12:50', '2018-11-27 18:12:50'),
(8, 12, '87', '2019-01-04 03:37:56', '2019-01-04 03:37:56');

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_project_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brief` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_from` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_to` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `projects`
--

INSERT INTO `projects` (`id`, `company_id`, `name`, `category_project_id`, `client_id`, `city_id`, `user_id`, `price`, `brief`, `notes`, `status`, `date_from`, `date_to`, `created_at`, `updated_at`) VALUES
(1, 1, 'مشروع تجريبي 1', 1, 4, 1, 7, '456', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 'ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.', 0, NULL, NULL, '2018-11-28 15:58:44', '2018-11-28 15:58:44'),
(2, 1, 'مشروع تجريبي 2', 2, 5, 2, 35, '698', NULL, NULL, 1, NULL, NULL, '2018-11-28 16:08:39', '2018-11-28 16:08:39'),
(3, 1, 'مشروع تجريبي 3', 4, 10, 3, 33, '852', 'صسثقلصثقل', 'صثبقصثبق', 2, NULL, NULL, '2018-11-28 16:09:19', '2018-11-28 16:09:19'),
(5, 1, 'مشروع تجريبي 4', 1, 4, 1, 7, '101', 'سنا لسثصنىلص لاىصنثباثصه بىةثسنبتنص ثبىوةسيبى سي سينتباسيتن بىسينم سينتسبث', 'صسثبصثب سنيتلاسين ت صحخثقهصثحخعءؤ ثتال بءؤ  سثنتب ىسيمن ب تسيةوىبنم سثب', 0, NULL, NULL, '2018-11-28 17:02:17', '2018-11-28 17:10:41'),
(7, 12, 'منصة اعلانات', 8, 38, 0, 50, '0', 'منصة اعلانات ( برمجة - تسويق - تشغيل )', NULL, 4, '2018-12-08', '2018-12-31', '2018-12-08 01:47:48', '2019-01-04 02:11:04'),
(9, 12, 'متجر ارقان', 6, 47, 0, 30, '0', 'متجر الكتروني لبيع ادوات الزيوت و المرطبات.', NULL, 4, '2018-01-01', '2018-12-31', '2018-12-16 05:02:31', '2019-01-05 00:51:41'),
(10, 12, 'تطبيق محاضرة', 6, 38, 0, 20, '0', 'بناء تطبيق إلكتروني على نظامي (Android وIOS  ) يواكب التطور التكنلوجي بطريقة ذكية وفعالة بحيث يقدم هذه الفعاليات الدينية والاجتماعية للمهتمين بها في جميع مدن المملكة .', NULL, 1, '2018-12-23', '2019-01-31', '2018-12-23 13:28:24', '2019-01-05 00:31:29'),
(11, 12, 'صيت', 8, 38, 0, 31, '0', NULL, NULL, 0, '2018-12-24', '2019-02-28', '2018-12-24 12:19:04', '2018-12-24 12:19:04'),
(13, 12, 'مقرأة الحرمين', 6, 38, 0, 30, '0', NULL, NULL, 0, '2019-01-01', '2019-01-15', '2018-12-25 20:25:18', '2019-01-03 09:49:47'),
(15, 12, 'نظام إدارة مكاتب المحاماة', 6, 38, 0, 50, '0000', 'المشروع مقسم الى مسارين مسار نسخه خاصه لمكتب الدكتور عبدالعزيز ال فريان \r\nوالمسار الاخر منتج للشركة يباع', NULL, 4, '2018-09-06', '2018-12-31', '2018-12-28 19:42:12', '2019-01-04 01:52:41');

-- --------------------------------------------------------

--
-- Structure de la table `project_user`
--

CREATE TABLE `project_user` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `project_user`
--

INSERT INTO `project_user` (`project_id`, `user_id`, `type`) VALUES
(1, 7, 1),
(2, 35, 1),
(3, 33, 1),
(5, 7, 1),
(1, 35, 0),
(1, 33, 0),
(7, 20, 0),
(7, 31, 0),
(7, 24, 0),
(7, 27, 0),
(7, 22, 0),
(7, 50, 1),
(9, 20, 0),
(9, 22, 0),
(9, 30, 1),
(10, 22, 0),
(10, 20, 1),
(11, 31, 1),
(11, 22, 0),
(11, 31, 0),
(11, 20, 0),
(11, 23, 0),
(11, 42, 0),
(11, 32, 0),
(13, 22, 0),
(13, 30, 1),
(7, 23, 0),
(13, 20, 0),
(15, 50, 1),
(15, 22, 0),
(15, 24, 0),
(15, 28, 0),
(15, 31, 0),
(15, 20, 0),
(15, 50, 0),
(15, 32, 0);

-- --------------------------------------------------------

--
-- Structure de la table `rewards`
--

CREATE TABLE `rewards` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `rewards`
--

INSERT INTO `rewards` (`id`, `company_id`, `user_id`, `month`, `year`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 12, 24, '12', '2018', 'مكافئة 1', '455', '2018-12-21 18:29:22', '2018-12-21 18:29:22'),
(2, 12, 24, '11', '2018', 'مكافئة 2', '666', '2018-12-21 18:29:22', '2018-12-21 18:29:22'),
(4, 12, 24, '12', '2018', 'مكافئة 2', '100', '2018-12-21 18:39:49', '2018-12-21 18:39:49'),
(7, 12, 28, '12', '2018', 'مكافأة انجاز ملف المشاهير', '1500', '2018-12-26 01:20:21', '2018-12-26 01:20:21'),
(6, 12, 24, '07', '2018', 'قثلثقل', '88', '2018-12-21 18:40:32', '2018-12-21 18:40:32');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permissions_general` text COLLATE utf8mb4_unicode_ci,
  `permissions_meetings` text COLLATE utf8mb4_unicode_ci,
  `permissions_prm` text COLLATE utf8mb4_unicode_ci,
  `permissions_passwords` text CHARACTER SET utf8,
  `permissions_hrm` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `company_id`, `created_at`, `updated_at`, `permissions_general`, `permissions_meetings`, `permissions_prm`, `permissions_passwords`, `permissions_hrm`) VALUES
(1, 'أدمن', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'موظف', 1, NULL, '2018-12-07 15:49:54', 'a:0:{}', 'a:25:{i:0;s:11:\"add_meeting\";i:1;s:14:\"update_meeting\";i:2;s:14:\"delete_meeting\";i:3;s:14:\"accept_meeting\";i:4;s:14:\"cancel_meeting\";i:5;s:13:\"delay_meeting\";i:6;s:14:\"notify_meeting\";i:7;s:14:\"finish_meeting\";i:8;s:8:\"add_axis\";i:9;s:11:\"update_axis\";i:10;s:11:\"delete_axis\";i:11;s:10:\"order_axis\";i:12;s:17:\"start_finish_axis\";i:13;s:16:\"add_comment_axis\";i:14;s:13:\"add_file_axis\";i:15;s:8:\"add_task\";i:16;s:11:\"update_task\";i:17;s:11:\"delete_task\";i:18;s:10:\"order_task\";i:19;s:8:\"add_file\";i:20;s:11:\"delete_file\";i:21;s:8:\"add_vote\";i:22;s:11:\"update_vote\";i:23;s:11:\"delete_vote\";i:24;s:14:\"add_evaluation\";}', 'a:13:{i:0;s:11:\"add_project\";i:1;s:14:\"update_project\";i:2;s:14:\"delete_project\";i:3;s:8:\"add_task\";i:4;s:11:\"update_task\";i:5;s:11:\"delete_task\";i:6;s:13:\"add_milestone\";i:7;s:16:\"update_milestone\";i:8;s:16:\"delete_milestone\";i:9;s:14:\"add_discussion\";i:10;s:12:\"add_filetype\";i:11;s:8:\"add_file\";i:12;s:11:\"delete_file\";}', 'a:4:{i:0;s:12:\"show_website\";i:1;s:11:\"add_website\";i:2;s:14:\"update_website\";i:3;s:14:\"delete_website\";}', NULL),
(3, 'موظف مميز', 1, '2018-11-08 15:11:54', '2018-12-07 15:49:56', 'a:3:{i:0;s:13:\"delete_client\";i:1;s:13:\"update_client\";i:2;s:12:\"prm_settings\";}', 'a:24:{i:0;s:14:\"update_meeting\";i:1;s:14:\"delete_meeting\";i:2;s:14:\"accept_meeting\";i:3;s:14:\"cancel_meeting\";i:4;s:13:\"delay_meeting\";i:5;s:14:\"notify_meeting\";i:6;s:14:\"finish_meeting\";i:7;s:8:\"add_axis\";i:8;s:11:\"update_axis\";i:9;s:11:\"delete_axis\";i:10;s:10:\"order_axis\";i:11;s:17:\"start_finish_axis\";i:12;s:16:\"add_comment_axis\";i:13;s:13:\"add_file_axis\";i:14;s:8:\"add_task\";i:15;s:11:\"update_task\";i:16;s:11:\"delete_task\";i:17;s:10:\"order_task\";i:18;s:8:\"add_file\";i:19;s:11:\"delete_file\";i:20;s:8:\"add_vote\";i:21;s:11:\"update_vote\";i:22;s:11:\"delete_vote\";i:23;s:14:\"add_evaluation\";}', 'a:13:{i:0;s:11:\"add_project\";i:1;s:14:\"update_project\";i:2;s:14:\"delete_project\";i:3;s:8:\"add_task\";i:4;s:11:\"update_task\";i:5;s:11:\"delete_task\";i:6;s:13:\"add_milestone\";i:7;s:16:\"update_milestone\";i:8;s:16:\"delete_milestone\";i:9;s:14:\"add_discussion\";i:10;s:12:\"add_filetype\";i:11;s:8:\"add_file\";i:12;s:11:\"delete_file\";}', 'a:4:{i:0;s:12:\"show_website\";i:1;s:11:\"add_website\";i:2;s:14:\"update_website\";i:3;s:14:\"delete_website\";}', NULL),
(4, 'موظف فرعي', 10, '2018-11-12 17:18:31', '2018-11-12 18:05:48', 'a:7:{i:0;s:8:\"add_user\";i:1;s:11:\"delete_user\";i:2;s:11:\"update_user\";i:3;s:9:\"show_user\";i:4;s:10:\"add_client\";i:5;s:13:\"update_client\";i:6;s:11:\"show_client\";}', 'a:6:{i:0;s:11:\"add_meeting\";i:1;s:14:\"update_meeting\";i:2;s:14:\"delete_meeting\";i:3;s:14:\"accept_meeting\";i:4;s:13:\"delay_meeting\";i:5;s:14:\"add_evaluation\";}', NULL, NULL, NULL),
(5, 'عضو', 12, '2018-11-18 18:31:57', '2019-01-03 22:24:23', 'a:5:{i:0;s:15:\"meetings_system\";i:1;s:10:\"prm_system\";i:2;s:16:\"passwords_system\";i:3;s:10:\"hrm_system\";i:4;s:15:\"calendar_system\";}', NULL, 'a:8:{i:0;s:4:\"tab1\";i:1;s:4:\"tab2\";i:2;s:4:\"tab3\";i:3;s:4:\"tab4\";i:4;s:4:\"tab5\";i:5;s:4:\"tab6\";i:6;s:4:\"tab7\";i:7;s:4:\"tab8\";}', 'a:4:{i:0;s:12:\"show_website\";i:1;s:11:\"add_website\";i:2;s:14:\"update_website\";i:3;s:14:\"delete_website\";}', 'a:62:{i:0;s:25:\"insurance_n_finance_infos\";i:1;s:17:\"add_allowance_cat\";i:2;s:20:\"update_allowance_cat\";i:3;s:20:\"delete_allowance_cat\";i:4;s:17:\"add_deduction_cat\";i:5;s:20:\"update_deduction_cat\";i:6;s:20:\"delete_deduction_cat\";i:7;s:11:\"add_medical\";i:8;s:14:\"update_medical\";i:9;s:14:\"delete_medical\";i:10;s:10:\"add_social\";i:11;s:13:\"update_social\";i:12;s:13:\"delete_social\";i:13;s:9:\"employees\";i:14;s:12:\"add_employee\";i:15;s:13:\"show_employee\";i:16;s:15:\"update_employee\";i:17;s:15:\"delete_employee\";i:18;s:8:\"finances\";i:19;s:13:\"tab_allowance\";i:20;s:13:\"add_allowance\";i:21;s:16:\"update_allowance\";i:22;s:16:\"delete_allowance\";i:23;s:13:\"tab_deduction\";i:24;s:13:\"add_deduction\";i:25;s:16:\"update_deduction\";i:26;s:16:\"delete_deduction\";i:27;s:11:\"tab_advance\";i:28;s:11:\"add_advance\";i:29;s:14:\"update_advance\";i:30;s:14:\"delete_advance\";i:31;s:11:\"tab_custody\";i:32;s:11:\"add_custody\";i:33;s:14:\"update_custody\";i:34;s:14:\"delete_custody\";i:35;s:17:\"delivered_custody\";i:36;s:13:\"print_custody\";i:37;s:23:\"adminisitrative_affairs\";i:38;s:12:\"tab_vacation\";i:39;s:12:\"add_vacation\";i:40;s:15:\"update_vacation\";i:41;s:15:\"delete_vacation\";i:42;s:12:\"tab_training\";i:43;s:12:\"add_training\";i:44;s:15:\"update_training\";i:45;s:15:\"delete_training\";i:46;s:11:\"tab_mandate\";i:47;s:11:\"add_mandate\";i:48;s:14:\"update_mandate\";i:49;s:14:\"delete_mandate\";i:50;s:11:\"evaluations\";i:51;s:14:\"add_evaluation\";i:52;s:17:\"delete_evaluation\";i:53;s:11:\"attendances\";i:54;s:11:\"vacationsss\";i:55;s:18:\"accept_vacationsss\";i:56;s:7:\"payroll\";i:57;s:10:\"add_reward\";i:58;s:13:\"delete_reward\";i:59;s:16:\"add_substraction\";i:60;s:19:\"delete_substraction\";i:61;s:15:\"send_accounting\";}'),
(6, 'عضو مميز', 12, '2018-12-16 19:59:40', '2019-01-04 22:05:04', NULL, 'a:4:{i:0;s:8:\"add_axis\";i:1;s:11:\"update_axis\";i:2;s:8:\"add_vote\";i:3;s:11:\"update_vote\";}', NULL, NULL, NULL),
(7, 'منسق', 12, '2018-12-16 20:03:33', '2019-01-04 22:10:11', 'a:7:{i:0;s:8:\"add_user\";i:1;s:11:\"delete_user\";i:2;s:11:\"update_user\";i:3;s:9:\"show_user\";i:4;s:13:\"update_client\";i:5;s:15:\"meetings_system\";i:6;s:10:\"prm_system\";}', 'a:20:{i:0;s:11:\"add_meeting\";i:1;s:14:\"update_meeting\";i:2;s:14:\"accept_meeting\";i:3;s:14:\"cancel_meeting\";i:4;s:13:\"delay_meeting\";i:5;s:14:\"notify_meeting\";i:6;s:14:\"finish_meeting\";i:7;s:8:\"add_axis\";i:8;s:11:\"update_axis\";i:9;s:10:\"order_axis\";i:10;s:17:\"start_finish_axis\";i:11;s:16:\"add_comment_axis\";i:12;s:13:\"add_file_axis\";i:13;s:8:\"add_task\";i:14;s:11:\"update_task\";i:15;s:10:\"order_task\";i:16;s:8:\"add_file\";i:17;s:8:\"add_vote\";i:18;s:11:\"update_vote\";i:19;s:14:\"add_evaluation\";}', 'a:9:{i:0;s:11:\"add_project\";i:1;s:14:\"update_project\";i:2;s:8:\"add_task\";i:3;s:11:\"update_task\";i:4;s:13:\"add_milestone\";i:5;s:16:\"update_milestone\";i:6;s:14:\"add_discussion\";i:7;s:12:\"add_filetype\";i:8;s:8:\"add_file\";}', 'a:2:{i:0;s:12:\"show_website\";i:1;s:11:\"add_website\";}', NULL),
(8, 'موظف', 12, '2018-12-24 19:37:08', '2019-01-02 23:58:11', 'a:12:{i:0;s:8:\"add_user\";i:1;s:11:\"delete_user\";i:2;s:11:\"update_user\";i:3;s:9:\"show_user\";i:4;s:10:\"add_client\";i:5;s:13:\"delete_client\";i:6;s:13:\"update_client\";i:7;s:11:\"show_client\";i:8;s:15:\"meetings_system\";i:9;s:10:\"prm_system\";i:10;s:16:\"passwords_system\";i:11;s:10:\"hrm_system\";}', 'a:2:{i:0;s:8:\"add_vote\";i:1;s:11:\"update_vote\";}', 'a:13:{i:0;s:11:\"add_project\";i:1;s:14:\"update_project\";i:2;s:14:\"delete_project\";i:3;s:8:\"add_task\";i:4;s:11:\"update_task\";i:5;s:11:\"delete_task\";i:6;s:13:\"add_milestone\";i:7;s:16:\"update_milestone\";i:8;s:16:\"delete_milestone\";i:9;s:14:\"add_discussion\";i:10;s:12:\"add_filetype\";i:11;s:8:\"add_file\";i:12;s:11:\"delete_file\";}', 'a:1:{i:0;s:14:\"delete_website\";}', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sections`
--

INSERT INTO `sections` (`id`, `company_id`, `name`, `department_id`, `created_at`, `updated_at`) VALUES
(5, 12, 'التقنية', 6, '2018-12-27 19:31:16', '2018-12-27 19:31:16'),
(6, 12, 'التسويق', 6, '2018-12-27 19:31:34', '2018-12-27 19:31:34'),
(7, 12, 'الاداري', 6, '2018-12-27 19:32:01', '2018-12-27 19:32:01');

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(12) DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `company_id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'email_sent_from_address1', 'devweb218@gmail.com', '2018-11-05 15:47:39', '2018-11-05 15:47:39'),
(2, 1, 'email_sent_from_name1', 'شركة محلات تجارية كبرى', '2018-11-05 15:47:39', '2018-11-05 15:47:39'),
(3, 1, 'email_smtp_host1', 'smtp.gmail.com', '2018-11-05 15:47:39', '2018-11-05 15:47:39'),
(4, 1, 'email_smtp_user1', 'devweb218@gmail.com', '2018-11-05 15:47:39', '2018-11-05 15:47:39'),
(5, 1, 'email_smtp_password1', 'azerty123AZERTY', '2018-11-05 15:47:39', '2018-11-05 15:47:39'),
(6, 1, 'email_smtp_port1', '465', '2018-11-05 15:47:39', '2018-11-22 10:43:53'),
(7, 1, 'send_test_mail_to', NULL, '2018-11-05 15:47:39', '2019-01-04 21:24:15'),
(36, 1, 'countdown11', '10', '2018-11-22 14:59:29', '2018-11-22 14:59:29'),
(8, 0, 'sms_default', '1', '2018-11-05 18:17:34', '2018-11-05 18:20:45'),
(9, 1, 'yamamah_url1', 'https://www.yamamah.com', '2018-11-05 18:17:34', '2018-11-05 18:17:34'),
(10, 1, 'yamamah_sender1', 'ERP', '2018-11-05 18:17:34', '2018-11-05 18:17:34'),
(11, 1, 'yamamah_username1', '966502480256', '2018-11-05 18:17:34', '2018-11-05 18:47:07'),
(12, 1, 'yamamah_password1', 'Aa@0502480256', '2018-11-05 18:17:34', '2018-11-05 18:47:07'),
(13, 1, 'pusher_auth_key1', '3fa55074add159637058', '2018-11-05 19:55:01', '2018-11-05 19:55:01'),
(14, 1, 'pusher_secret1', '5e64bdeb748cf3fa4ec4', '2018-11-05 19:55:01', '2018-11-05 19:55:01'),
(15, 1, 'pusher_app_id1', '638953', '2018-11-05 19:55:01', '2018-11-05 19:55:01'),
(16, 1, 'zoom_api_key1', 'kEyZuvuxRcuA1S8Jbqkg8Q', '2018-11-06 21:43:45', '2018-11-06 21:43:45'),
(17, 1, 'zoom_api_secret1', '6X9fHLxGS7ydB8YDqCFKenou3VytvEyfGI7g', '2018-11-06 21:43:45', '2018-11-06 21:43:45'),
(18, 12, 'countdown112', '11', '2018-11-19 15:43:18', '2018-11-19 16:06:06'),
(19, 12, 'countdown212', '5', '2018-11-19 15:43:18', '2018-11-19 16:06:06'),
(20, 12, 'reminder_message12', 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', '2018-11-19 18:33:52', '2018-11-19 18:33:52'),
(21, 12, 'pusher_auth_key12', '3fa55074add159637058', '2018-11-19 18:36:52', '2018-11-19 18:36:52'),
(22, 12, 'pusher_secret12', '5e64bdeb748cf3fa4ec4', '2018-11-19 18:36:52', '2018-11-19 18:36:52'),
(23, 12, 'pusher_app_id12', '638953', '2018-11-19 18:36:52', '2019-01-04 21:28:54'),
(24, 12, 'email_sent_from_address12', 'devweb218@gmail.com', '2018-11-19 18:56:40', '2018-11-19 18:56:40'),
(25, 12, 'email_sent_from_name12', 'ccreation', '2018-11-19 18:56:40', '2018-11-19 18:56:40'),
(26, 12, 'email_smtp_host12', 'smtp.sparkpostmail.com', '2018-11-19 18:56:40', '2018-12-10 17:40:58'),
(27, 12, 'email_smtp_user12', 'SMTP_Injection', '2018-11-19 18:56:40', '2018-12-10 17:41:22'),
(28, 12, 'email_smtp_password12', 'azerty123AZERTY', '2018-11-19 18:56:40', '2018-12-10 17:40:58'),
(29, 12, 'email_smtp_port12', '465', '2018-11-19 18:56:40', '2018-12-10 16:54:18'),
(30, 12, 'yamamah_url12', 'https://www.yamamah.com', '2018-11-19 19:00:25', '2018-11-19 19:00:25'),
(31, 12, 'yamamah_sender12', '1click', '2018-11-19 19:00:25', '2018-12-28 02:20:59'),
(32, 12, 'yamamah_username12', '966502480256', '2018-11-19 19:00:25', '2019-01-02 23:13:10'),
(33, 12, 'yamamah_password12', 'Aa@0502480256', '2018-11-19 19:00:25', '2018-12-28 02:41:36'),
(34, 12, 'zoom_api_key12', 'kEyZuvuxRcuA1S8Jbqkg8Q', '2018-11-20 15:05:49', '2019-01-04 21:32:37'),
(35, 12, 'zoom_api_secret12', '6X9fHLxGS7ydB8YDqCFKenou3VytvEyfGI7g', '2018-11-20 15:05:49', '2018-11-21 08:40:58'),
(37, 1, 'countdown21', '5', '2018-11-22 14:59:29', '2018-11-22 14:59:29'),
(38, 1, 'reminder_message1', 'هذه رسالة للتذكير بالإجتماع لجميع المدعويين', '2018-11-22 14:59:29', '2018-11-22 14:59:29'),
(39, 1, 'partialAttendance1', '25', '2018-11-22 15:04:47', '2018-11-22 15:19:06'),
(40, 1, 'reminder_day1', '4', '2018-11-22 15:51:55', '2018-11-22 15:51:55'),
(41, 1, 'reminder_hour1', '7', '2018-11-22 15:51:55', '2018-11-22 15:51:55'),
(42, 1, 'reminder_minute1', '55', '2018-11-22 15:51:55', '2018-11-22 15:51:55'),
(43, 12, 'reminder_day12', '0', '2019-01-03 23:46:48', '2019-01-03 23:46:48'),
(44, 12, 'reminder_hour12', '0', '2019-01-03 23:46:48', '2019-01-03 23:46:48'),
(45, 12, 'reminder_minute12', '1', '2019-01-03 23:46:48', '2019-01-03 23:46:48'),
(46, 12, 'partialAttendance12', '80', '2019-01-03 23:46:48', '2019-01-03 23:47:02'),
(47, 12, 'name', 'ertgergt', '2019-01-03 23:49:16', '2019-01-03 23:49:16');

-- --------------------------------------------------------

--
-- Structure de la table `site_users`
--

CREATE TABLE `site_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `site_users`
--

INSERT INTO `site_users` (`id`, `name`, `password`, `type`, `created_at`, `updated_at`) VALUES
(1, 'sdf', 'zerzer', 'zerzer', '2018-12-07 16:53:24', '2018-12-07 16:53:24'),
(2, 'zerzer', 'zerzer', 'zerzer', '2018-12-07 16:53:24', '2018-12-07 16:53:24'),
(32, 'admin2@admin.com', 'password', 'أدمن', '2019-01-05 00:55:43', '2019-01-05 00:55:43'),
(33, 'admin@admin.com', 'password', 'أدمن', '2019-01-05 00:56:31', '2019-01-05 00:56:31'),
(22, 'سيبييسب', 'سيبسيبسي', 'بسيبسيب', '2018-12-07 17:36:24', '2018-12-07 17:36:24'),
(23, 'erterte re tert', 'ertertert', 'ertertertert', '2018-12-07 17:36:24', '2018-12-07 17:36:24'),
(24, 'eeeeeeee', 'eeeeeeeeeee', 'eeeeeeeeeeeeeee', '2018-12-07 17:36:24', '2018-12-07 17:36:24');

-- --------------------------------------------------------

--
-- Structure de la table `site_user_website`
--

CREATE TABLE `site_user_website` (
  `site_user_id` int(10) UNSIGNED NOT NULL,
  `website_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `site_user_website`
--

INSERT INTO `site_user_website` (`site_user_id`, `website_id`) VALUES
(1, 1),
(2, 1),
(32, 4),
(33, 3),
(24, 2),
(23, 2),
(22, 2);

-- --------------------------------------------------------

--
-- Structure de la table `social_categories`
--

CREATE TABLE `social_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `social_categories`
--

INSERT INTO `social_categories` (`id`, `company_id`, `name`, `employee`, `company`, `created_at`, `updated_at`) VALUES
(1, 12, 'سعودي (قابل للخصم)', '10', '12', '2018-12-14 14:48:15', '2018-12-14 14:48:15'),
(2, 12, 'سعودي (غير قابل للخصم)', '0', '22', '2018-12-14 14:48:45', '2018-12-14 14:48:45'),
(3, 12, 'غير سعودي', '0', '2', '2018-12-14 14:49:00', '2018-12-14 14:49:00');

-- --------------------------------------------------------

--
-- Structure de la table `subtasks`
--

CREATE TABLE `subtasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `subtasks`
--

INSERT INTO `subtasks` (`id`, `user_id`, `task_id`, `name`, `from_date`, `to_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 33, 50, 'المهمة الفرعية الأولى', '2018-11-01', '2018-11-05', 1, '2018-11-29 16:25:05', '2018-11-29 16:58:05'),
(4, 33, 50, 'المهمة الفرعية الثانية', '2018-11-06', '2018-11-10', 1, '2018-11-29 16:50:59', '2018-11-29 17:00:23'),
(6, 33, 50, 'صثقبصثبق', '2018-11-28', '2018-11-30', 0, '2018-11-29 17:01:31', '2018-11-29 17:50:17'),
(7, 22, 77, 'اختيار الاسم', '2018-12-08', '2018-12-20', 1, '2018-12-08 02:10:58', '2019-01-04 01:56:32'),
(8, 22, 77, 'اعتماد الاسم', '2018-12-08', '2018-12-09', 0, '2018-12-08 02:12:26', '2019-01-03 22:02:23'),
(20, 28, 78, 'dherg', '2018-12-03', '2018-12-05', 1, '2018-12-08 12:55:00', '2018-12-10 12:23:49'),
(21, 24, 78, 'edrh', '2019-01-01', '2018-12-19', 1, '2018-12-08 13:03:30', '2018-12-08 15:11:48'),
(22, 22, 78, 'zerzer', NULL, NULL, 0, '2018-12-08 13:05:04', '2018-12-08 13:05:14'),
(23, 28, 80, 'مهام فرعية', '2018-11-28', '2018-12-01', 1, '2018-12-10 11:41:25', '2018-12-10 12:41:19'),
(24, 27, 83, 'سين', '2018-12-14', '2018-12-16', 1, '2018-12-14 09:12:02', '2018-12-14 09:13:47'),
(25, 20, 83, 'صاد', '2018-12-15', '2018-12-17', 1, '2018-12-14 09:12:16', '2018-12-14 09:16:08'),
(26, 20, 82, 'مهمة فرعية 1', '2018-12-05', '2018-12-10', 1, '2018-12-14 17:45:38', '2018-12-23 12:32:45'),
(27, 22, 82, 'مهمة فرعية 2', '2018-12-11', '2018-12-25', 1, '2018-12-14 18:28:23', '2018-12-14 18:57:06'),
(28, 28, 82, 'مهمة فرعية 3', '2018-12-12', '2018-12-18', 1, '2018-12-14 18:50:58', '2018-12-14 18:57:10'),
(29, 27, 81, 'dfghjk', '2018-12-24', '2018-12-16', 0, '2018-12-14 19:07:41', '2018-12-14 19:07:41'),
(30, 28, 82, 'hjhkk', '2018-12-23', '2018-12-24', 0, '2018-12-23 12:32:29', '2018-12-23 12:32:29'),
(31, 20, 82, 'dddd', '2018-12-23', '2018-12-28', 1, '2018-12-23 12:33:19', '2018-12-23 12:33:31'),
(32, 22, 155, 'اختيار اسم للنظام', '2018-12-28', '2018-12-31', 0, '2018-12-28 19:53:51', '2018-12-28 19:53:51'),
(33, 22, 155, 'حجز الدومين', '2019-01-01', '2019-01-11', 0, '2018-12-28 19:54:21', '2018-12-28 19:54:21'),
(34, 24, 156, 'التعديلات على النظام', '2018-12-27', '2018-12-31', 0, '2018-12-28 20:23:44', '2018-12-28 20:23:44'),
(35, 24, 156, 'تركيب نظام الموارد البشريه', '2018-12-28', '2018-12-31', 0, '2018-12-28 20:26:27', '2018-12-28 20:26:27'),
(36, 50, 157, 'تصميم البراند للنظام', '2019-01-01', '2019-01-31', 0, '2018-12-28 20:30:12', '2018-12-28 20:30:12'),
(37, 50, 157, 'فيديو تسويقي للنظام', '2019-01-01', '2019-01-31', 0, '2018-12-28 20:30:52', '2018-12-28 20:30:52'),
(38, 50, 157, 'ملف تعريفي للنظام', '2019-01-01', '2019-01-31', 0, '2018-12-28 20:34:32', '2018-12-28 20:34:32'),
(40, 28, 168, 'اعداد المحتوى', '2018-12-31', '2019-01-17', 0, '2018-12-31 19:18:58', '2018-12-31 19:18:58'),
(41, 50, 168, 'تصميم الملف', '2019-01-09', '2019-01-18', 0, '2018-12-31 19:19:27', '2018-12-31 19:19:27'),
(42, 28, 169, 'كتابة سيناريو الفيديو', '2018-12-31', '2019-01-17', 0, '2018-12-31 19:22:16', '2018-12-31 19:22:16'),
(43, 50, 169, 'تصميم الفيديو', '2019-01-17', '2019-01-20', 0, '2018-12-31 19:22:47', '2018-12-31 19:22:47'),
(44, 22, 170, 'تخصيص موظف للمبيعات', '2019-01-01', '2019-01-31', 0, '2018-12-31 19:24:36', '2018-12-31 19:24:36'),
(45, 50, 170, 'توفير قاعد بيانات للمكاتب المحاماة', '2019-01-01', '2019-01-31', 0, '2018-12-31 19:25:05', '2018-12-31 19:25:05'),
(46, 50, 170, 'تواصل مع هيئة المحامين والوصول الى اتفاقيه', '2019-01-01', '2019-01-31', 0, '2018-12-31 19:25:45', '2018-12-31 19:25:45'),
(47, 50, 170, 'الاعلان في مواقع مختصه بالمحاماة', '2019-01-01', '2019-01-31', 0, '2018-12-31 19:26:20', '2018-12-31 19:26:20'),
(48, 22, 170, 'الاعلان في مكتب ال عاظف والهزاع بالشرقيه', '2019-01-01', '2019-01-31', 0, '2018-12-31 19:27:00', '2018-12-31 19:27:00'),
(49, 22, 144, 'ا', '2019-01-03', '2019-01-07', 0, '2019-01-03 10:00:47', '2019-01-03 10:01:25'),
(50, 22, 174, 'ربط بين عمرو ومحمد جمال', '2019-01-03', '2019-01-03', 1, '2019-01-04 02:01:17', '2019-01-04 02:04:11'),
(51, 23, 174, 'تصميم صفحات الموقع الاولية', '2019-01-01', '2019-01-03', 0, '2019-01-04 02:01:43', '2019-01-04 02:01:43'),
(52, 23, 174, 'تسليم صفحة ( المشروع )', '2019-01-03', '2019-01-03', 0, '2019-01-04 02:03:05', '2019-01-04 02:03:05'),
(53, 20, 132, 'توقيع الاتفاقية من مكتب الروضة', '2019-01-05', '2019-01-10', 0, '2019-01-05 00:39:43', '2019-01-05 00:39:43'),
(54, 20, 132, 'توقيع الاتفاقية من مؤسسة ....', '2019-01-05', '2019-01-10', 0, '2019-01-05 00:40:14', '2019-01-05 00:40:14');

-- --------------------------------------------------------

--
-- Structure de la table `subtructions`
--

CREATE TABLE `subtructions` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `subtructions`
--

INSERT INTO `subtructions` (`id`, `company_id`, `user_id`, `month`, `year`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 12, 24, '12', '2018', 'خصم 1', '1000', '2018-12-21 18:51:05', '2018-12-21 18:51:05');

-- --------------------------------------------------------

--
-- Structure de la table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `daftar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `daftar_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thedate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(22) DEFAULT '0',
  `milestone_id` int(99) DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `oldstatus` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tasks`
--

INSERT INTO `tasks` (`id`, `daftar`, `daftar_id`, `company_id`, `user_id`, `name`, `thedate`, `from_date`, `to_date`, `num_days`, `priority`, `milestone_id`, `note`, `status`, `oldstatus`, `created_at`, `updated_at`) VALUES
(23, 'meeting', 15, 12, 29, 'حملة حفر الباطن', '2018-11-22', NULL, NULL, NULL, 0, NULL, 'تبدأ الحملة يوم 27-11', 2, 0, '2018-11-27 17:28:44', '2018-12-17 18:13:22'),
(21, 'meeting', 15, 12, 31, 'خطة الحملات الشهرية', '2018-11-22', NULL, NULL, NULL, 0, NULL, 'تتضمن تجهيز (1خطة الحملات الشهرية -خطة حملة رمضانة -خطة حملة ذو الحجة -خطة الخدمات العامة )', 2, 0, '2018-11-27 17:25:03', '2018-12-22 20:31:08'),
(12, 'meeting', 8, 1, 35, 'المهمة الأولى 1', '2018-11-15', NULL, NULL, NULL, 0, NULL, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 2, 0, '2018-11-21 14:03:34', '2018-11-21 17:39:55'),
(13, 'meeting', 8, 1, 33, 'المهمة الثانية 1', '2018-11-20', NULL, NULL, NULL, 0, NULL, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 0, 0, '2018-11-21 14:04:02', '2018-12-07 12:36:09'),
(14, 'meeting', 8, 1, 34, 'المهمة الثالثة 1', '2018-11-25', NULL, NULL, NULL, 0, NULL, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 1, 0, '2018-11-21 14:04:28', '2018-11-21 17:40:00'),
(15, 'meeting', 9, 1, 5, 'المهمة الأولى 2', '2018-11-14', NULL, NULL, NULL, 0, NULL, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 0, 0, '2018-11-21 14:07:27', '2018-11-21 17:53:54'),
(16, 'meeting', 9, 1, 35, 'المهمة الثانية 2', '2018-11-30', NULL, NULL, NULL, 0, NULL, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 1, 0, '2018-11-21 14:07:46', '2018-11-21 17:39:50'),
(17, 'meeting', 14, 1, 35, 'ثيقفثقلف', '2018-11-15', NULL, NULL, NULL, 0, NULL, 'ثقفثقفثقف', 1, 0, '2018-11-24 12:44:26', '2018-12-07 12:36:28'),
(19, 'meeting', 18, 1, 35, 'شسيشسي', '2018-11-09', NULL, NULL, NULL, 0, NULL, 'شسيشسي', 0, 0, '2018-11-24 13:40:29', '2018-11-24 13:40:29'),
(20, 'meeting', 18, 1, 35, 'zerzer', '2018-11-06', NULL, NULL, NULL, 0, NULL, 'zer', 1, 0, '2018-11-24 13:41:35', '2018-12-07 12:41:39'),
(22, 'meeting', 15, 12, 20, 'حملة تحفيظ الجبيل', '2018-11-22', NULL, NULL, NULL, 0, NULL, 'الحملة مدتها من 40 إلى 60 يوم لدينا 20 يوم للتنفيذ', 1, 0, '2018-11-27 17:26:45', '2018-12-17 18:14:11'),
(24, 'meeting', 15, 12, 29, 'خطة الخمسة أشهر', '2018-11-22', NULL, NULL, NULL, 0, NULL, 'تجهيز الخطة الخاصة بحفر الباطن', 2, 0, '2018-11-27 17:42:20', '2018-12-17 18:12:53'),
(25, 'meeting', 15, 12, 28, 'تجهيز المحتوى', '2018-11-22', NULL, NULL, NULL, 0, NULL, 'تجهيز محتوى الموشن والسيانريو والاعلان عاجلا لحفر الباطن', 2, 0, '2018-11-27 17:46:47', '2018-12-17 18:14:48'),
(26, 'meeting', 15, 12, 20, 'التنسيق مع المشاهير', '2018-11-22', NULL, NULL, NULL, 0, NULL, 'التنسيق مع المشاهير المرتبطين بحملة الحفر', 2, 0, '2018-11-27 17:54:20', '2018-12-17 18:15:03'),
(27, 'meeting', 28, 12, 20, 'تسجيل الصوت لحملة الحفر', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'متابعة الثنيان لتسجيل الصوت', 2, 0, '2018-11-28 16:35:51', '2018-12-21 13:35:23'),
(28, 'meeting', 28, 12, 20, 'ارسال النصوص المصورة الخاصة بالحفر', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'ارسال النصوص المصورة لحفر الباطن لمحمد جمال', 0, 0, '2018-11-28 16:37:03', '2018-11-28 16:37:03'),
(29, 'meeting', 28, 12, 20, 'الاعلان الرئيسي والموشن', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'التواصل مع عمرو لتصميم الاعلان الرئيسي والموشن', 2, 0, '2018-11-28 16:38:41', '2018-12-03 19:42:52'),
(30, 'meeting', 28, 12, 20, 'التحويل لحملة الجبيل', '2018-11-25', NULL, NULL, NULL, 0, NULL, 'التحويل من رياض', 2, 0, '2018-11-28 16:39:27', '2018-12-21 13:35:03'),
(31, 'meeting', 28, 12, 20, 'تسليم الاعلان لحفر الباطن', '2018-11-25', NULL, NULL, NULL, 0, NULL, 'تسليم الاعلان بعد تجهيزه', 2, 0, '2018-11-28 16:40:12', '2018-12-21 13:34:33'),
(32, 'meeting', 28, 12, 20, 'متابعة المبرمجين', '2018-11-26', NULL, NULL, NULL, 0, NULL, 'بخصوص ما تم الاتفاق عليه باجتماع الارجان التحويل البنكي ومشغلات الرسائل البديلة لليمامة خارج المملكة', 2, 0, '2018-11-28 16:41:57', '2018-12-21 13:36:13'),
(33, 'meeting', 28, 12, 20, 'اوقاف الضحيان', '2018-11-27', NULL, NULL, NULL, 0, NULL, 'متابعة اوقاف الضحيان لتوقيع عقد المحاضرة', 1, 0, '2018-11-28 16:43:13', '2018-12-21 13:37:00'),
(34, 'meeting', 28, 12, 20, 'تطبيق محاضرة', '2018-11-27', NULL, NULL, NULL, 0, NULL, 'يرفع تطبيق المحاضرة عن طريق شاكر', 1, 0, '2018-11-28 16:44:25', '2018-12-21 13:36:37'),
(35, 'meeting', 28, 12, 29, 'اطلاق حملة حفر الباطن', '2018-11-27', NULL, NULL, NULL, 0, NULL, 'اطلاق الحملة', 0, 0, '2018-11-28 16:45:14', '2018-11-28 16:45:14'),
(36, 'meeting', 28, 12, 31, 'خطة الخمسة أشهر', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'تجهيز خطة الخمسة اشهر الخاصة بحفر الباطن', 1, 0, '2018-11-28 16:46:10', '2018-12-22 20:31:54'),
(37, 'meeting', 28, 12, 29, 'تجهيز المنتج الخاص بحفر الباطن', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'تجهيز المنتج خلال 6 ايام', 0, 0, '2018-11-28 16:46:56', '2018-11-28 16:46:56'),
(38, 'meeting', 28, 12, 23, 'تطبيق سريع', '2018-11-21', NULL, NULL, NULL, 0, NULL, 'الانتهاء من تصميم التطبيق', 0, 0, '2018-11-28 16:48:09', '2018-11-28 16:48:09'),
(39, 'meeting', 28, 12, 23, 'تصميم تطبيق ثري دي', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'الانتهاء من التصميم', 0, 0, '2018-11-28 16:48:54', '2018-11-28 16:48:54'),
(40, 'meeting', 28, 12, 23, 'تصاميم الارقان', '2018-11-26', NULL, NULL, NULL, 0, NULL, 'الانتهاء من تصاميم الارقان ايام 24-25-26', 0, 0, '2018-11-28 16:49:37', '2018-11-28 16:49:37'),
(41, 'meeting', 28, 12, 30, 'استلام التعديلات لتطبيق الارقان', '2018-11-27', NULL, NULL, NULL, 0, NULL, 'استلام التعديلات من العميل والبد بالعمل عليها', 0, 0, '2018-11-28 16:51:00', '2018-11-28 16:51:00'),
(42, 'meeting', 28, 12, 30, 'تحديد موعد نهائي لتسليم تطبيقات الارقان', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'تحديد موعد لتسليم العميل', 0, 0, '2018-11-28 16:52:09', '2018-11-28 16:52:09'),
(43, 'meeting', 28, 12, 30, 'عمل دليل استخدام لموقع الارقان', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'عمل دليل استخدام من قبل صلاح وخالد', 0, 0, '2018-11-28 16:53:05', '2018-11-28 16:53:05'),
(44, 'meeting', 28, 12, 30, 'تحديث بيانات العملاء في موقع الارقان', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'تحديث البيانات الخاصة بعملاء الموقع- صلاح وخالد بـ10ايام عمل', 0, 0, '2018-11-28 16:54:16', '2018-11-28 16:55:00'),
(45, 'meeting', 28, 12, 30, 'لوحة التحكم بموقع الارقان', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'العمل على الملاحظات والتعديلات المطلوبة من العميل الخاصة بلوحة التحكم', 0, 0, '2018-11-28 16:56:07', '2018-11-28 16:56:07'),
(46, 'meeting', 28, 12, 28, 'محتوى حملة الحفر', '2018-11-24', NULL, NULL, NULL, 0, NULL, 'تجهيز محتوى السيناريو للجرافيك والاعلان ومحتوى الموشن', 0, 0, '2018-11-28 16:57:14', '2018-11-28 16:57:14'),
(47, 'meeting', 28, 12, 27, 'دليل الشركات التجارية', '2018-11-17', NULL, NULL, NULL, 0, NULL, 'اخذ نسخة من دليل الشركات التجارية من الغرفة', 2, 0, '2018-11-28 16:58:27', '2018-12-22 13:15:53'),
(48, 'prm', 1, 1, 35, 'المهمة الأول', NULL, '2018-11-26', '2018-12-01', '7', 1, 5, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', 2, 0, '2018-11-29 11:41:48', '2018-12-15 13:47:11'),
(49, 'prm', 1, 1, 7, 'المهمة الثانية', NULL, '2018-11-26', '2018-11-30', '1', 3, NULL, 'ثقلثقلثقل', 1, 0, '2018-11-29 13:02:06', '2018-12-07 13:00:06'),
(50, 'prm', 1, 1, 35, 'المهمة الثالثة', NULL, '2018-11-22', '2018-11-25', '5', 4, 2, 'ثقلصثقفثص', 2, 0, '2018-11-29 13:02:47', '2018-12-07 12:52:28'),
(52, 'prm', 2, 1, 35, 'فيققففق', NULL, '2018-12-20', '2019-01-01', '12', 2, NULL, 'فق', 2, 0, '2018-12-01 11:10:36', '2019-01-03 06:57:21'),
(53, 'prm', 2, 1, 35, 'قثثق', NULL, '2018-12-20', '2018-12-31', '11', 1, NULL, 'ثقثقث', 2, 0, '2018-12-01 11:11:33', '2019-01-03 06:57:21'),
(54, 'meeting', 29, 12, 20, 'تحديد مهام عمرو', '2018-12-01', NULL, NULL, NULL, 0, NULL, 'تحديد اولويات مهام عمرو صبري', 2, 0, '2018-12-02 15:52:01', '2018-12-06 18:38:35'),
(55, 'meeting', 29, 12, 20, 'فتح جروب', '2018-12-01', NULL, NULL, NULL, 0, NULL, 'فتح جروب خاص بحملة الجبيل', 2, 0, '2018-12-02 15:52:45', '2018-12-06 18:38:23'),
(56, 'meeting', 29, 12, 20, 'ميساء', '2018-12-01', NULL, NULL, NULL, 0, NULL, 'ارسال رقم ميساء لرشا', 2, 0, '2018-12-02 15:53:37', '2018-12-06 18:38:13'),
(57, 'meeting', 29, 12, 20, 'مشغلي الرسائل', '2018-12-02', NULL, NULL, NULL, 0, NULL, 'البحث عن مشغلي رسائل دوليين', 2, 0, '2018-12-02 15:54:28', '2018-12-06 18:38:03'),
(58, 'meeting', 29, 12, 20, 'تنسيق اجتماع تعريفي', '2018-12-02', NULL, NULL, NULL, 0, NULL, 'تنسيق اجتماع للموظفين لتعليمهم على نظام اجتماعاتي', 1, 0, '2018-12-02 15:56:04', '2018-12-06 18:38:49'),
(59, 'meeting', 29, 12, 20, 'عقد الروضة', '2018-12-02', NULL, NULL, NULL, 0, NULL, 'ارسال عقد المحاضرة لتوقيعه', 1, 0, '2018-12-02 15:57:16', '2018-12-21 13:38:35'),
(60, 'meeting', 29, 12, 20, 'حملة الجبيل', '2018-12-03', NULL, NULL, NULL, 0, NULL, 'اخذ الافكار الرئيسية والتفاصيل من تحفيظ الجبيل', 1, 0, '2018-12-02 15:58:48', '2018-12-06 18:37:53'),
(61, 'meeting', 29, 12, 20, 'تسلم العقد', '2018-12-04', NULL, NULL, NULL, 0, NULL, 'تسلم عقد تطبيق محاضرة من العميل', 1, 0, '2018-12-02 16:00:09', '2018-12-06 18:37:41'),
(62, 'meeting', 29, 12, 20, 'فتح حسابات', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'فتح حسابات في جوجل بلاي وآب استور لتطبيق محاضرة', 2, 0, '2018-12-02 16:01:05', '2018-12-12 16:24:38'),
(63, 'meeting', 29, 12, 29, 'مناقشة ميزانية حملة الحفرdfhd', '2018-12-02', NULL, NULL, NULL, 0, NULL, 'منافشة الميزانية الخاصة بالحملة', 1, 0, '2018-12-02 16:02:00', '2018-12-27 00:21:52'),
(64, 'meeting', 29, 12, 29, 'انتهاء حملة الحفر', '2018-12-03', NULL, NULL, NULL, 0, NULL, 'متابعة الحملة ومتعلقاتها حتى الانتهاء منها', 2, 0, '2018-12-02 16:03:09', '2018-12-12 16:24:06'),
(65, 'meeting', 29, 12, 29, 'رفع تقرير عن الحملة', '2018-12-04', NULL, NULL, NULL, 0, NULL, 'رفع تقرير بعد انتهاء الحملة الخاصة بالحفر', 1, 0, '2018-12-02 16:03:59', '2018-12-06 18:37:24'),
(66, 'meeting', 29, 12, 31, 'خطة حملة الجبيل', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'تجهيز الخطة الخاصة بالحملة', 2, 0, '2018-12-02 16:04:53', '2018-12-06 18:37:01'),
(67, 'meeting', 29, 12, 31, 'ارسال قائمة المشاهير', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'التنسيق مع المشاهير المرشحين لحملة الجبيل مع رياض', 2, 0, '2018-12-02 16:05:45', '2018-12-22 21:11:59'),
(68, 'meeting', 29, 12, 31, 'استلام الافكار وتفاصيل الحملة', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'استلام الافكار وتفاصيل الحملة من رياض', 2, 0, '2018-12-02 16:06:50', '2018-12-22 20:30:30'),
(69, 'meeting', 29, 12, 30, 'تصاميم الارقان', '2018-12-01', NULL, NULL, NULL, 0, NULL, 'الانتهاء من التعديل على التصاميم لمتجر الارقان', 1, 0, '2018-12-02 16:10:12', '2018-12-06 18:36:22'),
(70, 'meeting', 29, 12, 30, 'رفع التصاميم الخاصة بالارقان', '2018-12-02', NULL, NULL, NULL, 0, NULL, 'رفع التصمايم لمتجر الارقان', 1, 0, '2018-12-02 16:10:39', '2018-12-06 18:35:22'),
(71, 'meeting', 29, 12, 30, 'اجتماع مع ماجد', '2018-12-03', NULL, NULL, NULL, 0, NULL, 'ترتيب اجتماع مع ماجد ايوب', 1, 0, '2018-12-02 16:11:21', '2018-12-06 18:35:05'),
(72, 'meeting', 29, 12, 30, 'تطبيقات المتجر', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'رفع التطبيقات بعد الانتهاء من المتجر واستلام التعديلات من العميل', 0, 0, '2018-12-02 16:11:55', '2018-12-02 16:11:55'),
(73, 'meeting', 29, 12, 28, 'تجهيز محتوى', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'محتوى السيناريو للجرافيك والاعلان والموشن لحملة الجبيل', 1, 0, '2018-12-02 16:12:49', '2018-12-06 18:35:57'),
(74, 'meeting', 29, 12, 28, 'فكرة ابداعية', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'تطوير فكرة ابداعية لموشن تعريفي لخير تك مع رشا ورياض', 1, 0, '2018-12-02 16:13:35', '2018-12-23 20:56:41'),
(75, 'meeting', 29, 12, 20, 'متابعة المبرمجين', '2018-12-02', NULL, NULL, NULL, 0, NULL, 'متابعة المبرمجين بخصوص التعديل على التحويل البنكي وبطاقات الفيزا ومدى كما تم الاتفاق عليه في الاجتماع قبل الماضي مع ماجد ايوب وتفعيل خاصية الدفع عند الاستلام لمنطقة الرياض فقط', 2, 0, '2018-12-02 16:22:12', '2018-12-21 13:37:55'),
(77, 'prm', 7, 12, 22, 'اختيار اسم المنصة', NULL, '2018-12-06', '2018-12-08', '1', 1, 6, 'يتم اعتماد اسم المنصة', 1, 1, '2018-12-08 02:07:38', '2019-01-03 21:04:33'),
(174, 'prm', 7, 12, 23, 'تصميم واجهة المنصة', NULL, '2019-01-01', '2019-01-05', '4', 2, 6, 'تم الشرح لعمرو', 0, 0, '2019-01-04 02:00:22', '2019-01-04 02:00:22'),
(79, 'meeting', 33, 12, 23, 'sdf', '2018-12-13', NULL, NULL, NULL, 0, NULL, 'iou', 1, 0, '2018-12-08 11:48:13', '2018-12-08 11:48:28'),
(80, 'prm', 6, 12, 22, 'ثقلثقلثقل', NULL, '2018-12-01', '2018-12-02', '1', 4, 7, NULL, 1, 1, '2018-12-08 15:13:35', '2018-12-15 13:48:31'),
(83, 'prm', 8, 12, 22, 'دراسة المشروع', NULL, '2018-12-14', '2018-12-21', '7', 1, 8, 'نص نص', 2, 0, '2018-12-14 09:11:11', '2018-12-21 22:00:01'),
(84, 'meeting', 34, 12, 20, 'الاتصال بمسئول المكتب التعاوني بعزبزية الخبر', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'التواصل مع فهد العقيل لطلب نبذة عن المكتب التعاوني بالعزيزية بالخبر', 2, 0, '2018-12-17 17:02:30', '2018-12-21 13:40:13'),
(85, 'meeting', 34, 12, 20, 'اعتماد المشاهير', '2018-12-16', NULL, NULL, NULL, 0, NULL, 'اعتماد قائمة الشاهير من ابراهيم كحد اقصى يوم الاحد', 0, 0, '2018-12-17 17:03:37', '2018-12-17 17:03:37'),
(86, 'meeting', 34, 12, 20, 'استاند الجاليات', '2018-12-16', NULL, NULL, NULL, 0, NULL, 'متابعة التصميم الخاص باستاند الجاليات والباركود عن الاعمال الخاصة بالمكتب مع محمد جمال', 1, 0, '2018-12-17 17:04:27', '2018-12-21 13:41:10'),
(87, 'meeting', 34, 12, 20, 'تجهيز الفيديو', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'تجهيز الفيديو الخاص بالارقان', 0, 0, '2018-12-17 17:05:13', '2018-12-17 17:05:13'),
(88, 'meeting', 34, 12, 20, 'اعتماد عقد الثنيان', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'اعتماد عقد الثنيان مع منصة اعلاناتي من ابراهيم', 0, 0, '2018-12-17 17:05:57', '2018-12-17 17:05:57'),
(89, 'meeting', 34, 12, 20, 'تحديد موعد', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'تحديد موعدمن ابراهيم  مع ثنيان السبيعي', 0, 0, '2018-12-17 17:06:36', '2018-12-17 17:06:36'),
(90, 'meeting', 34, 12, 20, 'عقد تطبيق المحاضرة', '2018-12-18', NULL, NULL, NULL, 0, NULL, 'ارسال عقد تطبيق محاضرة  لتوقيعه////متأخر', 0, 0, '2018-12-17 17:07:26', '2018-12-17 17:07:26'),
(91, 'meeting', 34, 12, 20, 'خير تك', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'مناقشة تسليم خير تك لسمير بدلا من اسماء مع ابراهيم', 0, 0, '2018-12-17 17:08:08', '2018-12-17 17:08:08'),
(92, 'meeting', 34, 12, 20, 'تنسيق اجتماع', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'تنسيق اجتماع بين ابراهيم ورياض ونورة', 0, 0, '2018-12-17 17:08:49', '2018-12-17 17:08:49'),
(93, 'meeting', 34, 12, 20, 'كول سنتر', '2018-12-18', NULL, NULL, NULL, 0, NULL, 'شراء شرائح الاتصال الخاصة بكول سنتر', 0, 0, '2018-12-17 17:09:34', '2018-12-17 17:09:34'),
(94, 'meeting', 34, 12, 20, 'اضافة موظفة', '2018-12-18', NULL, NULL, NULL, 0, NULL, 'اضافة موظفة من مكتب الثريان في جروب المحاماة من ابراهيم', 0, 0, '2018-12-17 17:10:16', '2018-12-17 17:10:16'),
(95, 'meeting', 34, 12, 20, 'استلام الدفعة الاخيرة من العقد', '2018-12-20', NULL, NULL, NULL, 0, NULL, 'استلام الدفعة الاخيرة لنظام ادارة المحاماة', 1, 0, '2018-12-17 17:11:03', '2018-12-21 14:48:13'),
(96, 'meeting', 34, 12, 20, 'تسلم عقد المحاضرة', '2018-12-20', NULL, NULL, NULL, 0, NULL, 'تسلم عقد تطبيق المحاضرة من العميل', 0, 0, '2018-12-17 17:11:46', '2018-12-17 17:11:46'),
(97, 'meeting', 34, 12, 42, 'اعتماد الوان', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'اعتماد الوان منصة اعلاناتي من ابراهيم', 0, 0, '2018-12-17 17:12:26', '2018-12-17 17:12:26'),
(98, 'meeting', 34, 12, 42, 'استضلفة موقع اعلاناتي', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'الاستضافة والدومين بالتنسيق مع رياض', 0, 0, '2018-12-17 17:13:14', '2018-12-17 17:13:14'),
(99, 'meeting', 34, 12, 42, 'تركيب صفحات منصة اعلاناتي', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'اعتماد تركيب الصفحات كل صفحة على حدة لمنصة اعلناتي', 0, 0, '2018-12-17 17:14:11', '2018-12-17 17:14:11'),
(100, 'meeting', 34, 12, 42, 'مراجعة المحتوى', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'مراجعة المحتوى الخاص بمنصة اعلاناتي كاملا', 0, 0, '2018-12-17 17:15:01', '2018-12-17 17:15:01'),
(101, 'meeting', 34, 12, 42, 'المراجعة القانونية', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'مراجعة الجوانب القانونية للمنصة', 0, 0, '2018-12-17 17:15:50', '2018-12-17 17:15:50'),
(102, 'meeting', 34, 12, 42, 'تطبيق الماء', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'مناقشة الانتهاء من تطبيق الماء مع ابراهيم', 0, 0, '2018-12-17 17:16:27', '2018-12-17 17:16:27'),
(103, 'meeting', 34, 12, 29, 'اعداد دليل', '2018-12-17', NULL, NULL, NULL, 0, NULL, 'اعداد دليل حملة ريالك بركة', 0, 0, '2018-12-17 17:17:59', '2018-12-17 17:17:59'),
(104, 'meeting', 34, 12, 29, 'تقرير حملة الحفر', '2018-12-19', NULL, NULL, NULL, 0, NULL, 'متابعة رياض بتسلم تقرير حملةحفر الباطن من محمد جمال', 0, 0, '2018-12-17 17:18:55', '2018-12-17 17:18:55'),
(105, 'meeting', 34, 12, 31, 'حملة الجبيل', '2018-12-16', NULL, NULL, NULL, 0, NULL, 'استلام الافكار الرئيسية وتفاصيل الحملة من رياض لحملة الجبيل//// متاخرة', 2, 0, '2018-12-17 17:19:56', '2018-12-22 21:11:06'),
(106, 'meeting', 34, 12, 23, 'تصميم الثري دي', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'تسليم تصميم الثري دي الي رياض', 0, 0, '2018-12-17 17:20:42', '2018-12-17 17:20:42'),
(107, 'meeting', 34, 12, 30, 'النسخة الثانية للارقان', '2018-12-30', NULL, NULL, NULL, 0, NULL, 'متابعة العمل للنسخة الثانية للارقان لكافة الاقسام في الموقع', 0, 0, '2018-12-17 17:24:02', '2018-12-17 17:24:02'),
(108, 'meeting', 34, 12, 30, 'المقرأة النسائية', '2019-01-12', NULL, NULL, NULL, 0, NULL, 'متابعة العمل في المقرأة النسائية والتي ستنتهي منتصف يناير////التأخير من  بن عاشور', 0, 0, '2018-12-17 17:25:23', '2018-12-17 17:25:23'),
(109, 'meeting', 34, 12, 30, 'دليل المستخدم للارقان', '2018-12-16', NULL, NULL, NULL, 0, NULL, 'تجهيز المحتوى الخاص بدليل المستخدم للارقان', 0, 0, '2018-12-17 17:26:07', '2018-12-17 17:26:07'),
(110, 'meeting', 34, 12, 30, 'تطبيقات الارقان', '2019-01-15', NULL, NULL, NULL, 0, NULL, 'تأجيل العمل على التعديلات بتطبيقات الارقان حتى الانتهاء من الموقع', 0, 0, '2018-12-17 17:27:01', '2018-12-17 17:27:01'),
(111, 'meeting', 34, 12, 30, 'رفع الحسابات', '2019-01-16', NULL, NULL, NULL, 0, NULL, 'متابعة شاكر في رفع الحسابات الخاصة بتطبيق محاضرة / على جوجل بلاي وآب استور وكذلك الحسابات المسلمة له سابقا من صلاح', 0, 0, '2018-12-17 17:28:07', '2018-12-17 17:28:07'),
(112, 'meeting', 34, 12, 28, 'محتوى حملة الجبيل', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'محتوى السيناريو للجرافيك والاعلان والموشن لحملة الجبيل', 0, 0, '2018-12-17 17:29:21', '2018-12-17 17:29:21'),
(113, 'meeting', 34, 12, 28, 'موشن تعريفي', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'تطوير فكرة موشن تعريفي لخير تك مع رشا ورياض', 2, 0, '2018-12-17 17:30:04', '2018-12-17 17:32:46'),
(114, 'meeting', 34, 12, 28, 'قائمة مشاهير كاملة', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'اعداد قائمة كاملة للمشاهير بشكل عام بالتنسيق مع رشا', 2, 0, '2018-12-17 17:30:54', '2018-12-17 17:33:25'),
(115, 'meeting', 34, 12, 28, 'دليل تسويقي لمكتب العزيزية بالخبر', '2018-12-15', NULL, NULL, NULL, 0, NULL, 'عمل دليل تسويقي لمكتب العزيزة التعاوني بالخبر بالتنسيق مع رياض', 2, 0, '2018-12-17 17:31:30', '2018-12-22 14:50:30'),
(116, 'meeting', 35, 12, 42, 'توصية نظام اعلاناتي', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'انشاء مشروع اعلاناتي على نظام المشاريع وادخال كل المهام المتعلقة به', 0, 0, '2018-12-22 19:22:52', '2018-12-22 19:22:52'),
(117, 'meeting', 35, 12, 42, 'توصية بخصوص تسويق متجر الارقان', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'انشاء المشروع على نظام المشاريع وادخال كل المهام المتعلقة به', 0, 0, '2018-12-22 19:27:32', '2018-12-22 19:27:32'),
(118, 'meeting', 35, 12, 42, 'تنسيق اجتماع', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'تنسيق اجتماع بين ابراهيم وصلاح ونورةبخصوص تسويق الارقان', 0, 0, '2018-12-22 19:28:42', '2018-12-22 19:29:13'),
(119, 'meeting', 35, 12, 31, 'توصية بخصوص حملة الجبيل', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'انشاء مشروع للحملة على نظام المشاريع ونقل جميع المهام المتعلقة بها في النظام', 0, 0, '2018-12-22 19:31:50', '2018-12-22 19:31:50'),
(120, 'meeting', 35, 12, 31, 'تنسيق اجتماع حملة الجبيل', '2018-12-24', NULL, NULL, NULL, 0, NULL, 'تنسيق اجتماع خاص لمناقشة افكار لحملة الجبيل', 0, 0, '2018-12-22 19:34:01', '2018-12-22 19:34:01'),
(121, 'meeting', 35, 12, 29, 'توصية بخصوص حملة الحفر', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'ادخال المشروع على نظام المشاريع وادخال جميع المهام المتعلقة به', 0, 0, '2018-12-22 19:35:08', '2018-12-22 19:35:08'),
(122, 'meeting', 35, 12, 29, 'توصية بتحديد يوم اجتماع ثابت للحفر', '2018-12-29', NULL, NULL, NULL, 0, NULL, 'التوصية بحديد يوم ثابت اسبوعي للاجتماع مع حفر الباطن', 0, 0, '2018-12-22 19:37:39', '2018-12-22 19:37:39'),
(123, 'meeting', 35, 12, 29, 'مناقشة افكار جملة الحفر وخطة رمضان الخاصة', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'تجهيز الافكار والخطة الخاصة برمضان ومناقشتها الاجتماع القادم', 0, 0, '2018-12-22 19:38:51', '2018-12-22 19:38:51'),
(124, 'meeting', 35, 12, 30, 'توصية بانشاء مشروع الارقان هاى النظام', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'انشاء مشروع الالرقان على نظام المشاريع وادخال جميع المهام المتعلقة به ورفع الملفات المرتبطة', 0, 0, '2018-12-22 19:40:50', '2018-12-22 19:40:50'),
(125, 'meeting', 35, 12, 20, 'انشاء مشروع محاضرة على نظام المشاريع', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'ااشاء المشروع وادخال جميع المهام المتعلقة به في المشروع والجانب التقني يكون بالتنسيق مع صلاح', 2, 0, '2018-12-22 19:42:28', '2018-12-23 16:08:10'),
(126, 'meeting', 35, 12, 20, 'عمل فيديو مصور عن نظام المشاريع', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'عمل فيديو مصور مدته 5 دقائق وتوزيعه على الموظفين', 2, 0, '2018-12-22 19:43:38', '2018-12-23 16:07:52'),
(127, 'meeting', 35, 12, 32, 'عمل فيديو مصور عن نظام اجتماعاتي', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'عمل فيديو مصور وتوزيعه على الموظفين', 2, 0, '2018-12-22 19:44:35', '2018-12-31 01:35:25'),
(128, 'meeting', 35, 12, 31, 'توصية بانشاء مشروع صيت', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'انشاء المشروع وادخاله بنظام المشاريع وادخال المهام المرتبطة به وتسجيل الخطة والهوية والتفعيل \r\nومتابعة ابراهيم بخصوص الدومين ومتابعة رياض بخصوص الاستضافة', 0, 0, '2018-12-22 19:46:36', '2018-12-22 19:46:36'),
(129, 'meeting', 35, 12, 20, 'متابعة محمد جمال', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'متابعة محمد جمال بخصوص الانتهاء من التقرير الخاص بحفر الباطن', 0, 0, '2018-12-22 19:51:30', '2018-12-22 19:51:30'),
(130, 'meeting', 35, 12, 29, 'رفع ملف عن المحتوى المتأخر تصميمه لحملة الحفر', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'رفع ملف كامل عن المحتوى المتأخر والذي لم يتم تصميمه الى الان', 0, 0, '2018-12-22 19:53:02', '2018-12-22 19:53:02'),
(131, 'meeting', 35, 12, 20, 'متابعة رفع الحسابات', '2018-12-22', NULL, NULL, NULL, 0, NULL, 'متابعة رفع الحسابات عن طريق شاكر', 2, 0, '2018-12-22 19:54:16', '2018-12-23 22:10:04'),
(132, 'prm', 10, 12, 20, 'توقيع الاتفاقية', NULL, '2019-01-01', '2019-01-10', '9', 3, 22, 'توقيع العقد المبرم بين كل من : \r\n١- شركة صناعة المحتوى\r\n٢- مؤسسة ........ \r\n٣- المكتب التعاوني بالروضة', 6, 6, '2018-12-23 13:37:03', '2019-01-05 00:39:43'),
(133, 'prm', 11, 12, 20, 'متابعة تسجيل السجل التجاري', NULL, '2018-12-24', '2018-12-31', '7', 1, NULL, NULL, 2, 0, '2018-12-24 12:20:18', '2019-01-03 06:57:21'),
(134, 'prm', 11, 12, 23, 'تصميم شعار البراند', NULL, '2018-12-24', '2019-01-10', '17', 2, NULL, NULL, 0, 0, '2018-12-24 12:21:25', '2018-12-24 12:21:25'),
(135, 'prm', 11, 12, 22, 'حجز الدومين', NULL, '2018-12-24', '2018-12-31', '7', 2, NULL, 'seet.com متاح\r\nseet.sa مستخدم', 2, 0, '2018-12-24 12:24:33', '2019-01-03 06:57:21'),
(136, 'prm', 11, 12, 20, 'حجز الإستضافة للموقع', NULL, '2018-12-31', '2019-01-06', '6', 1, NULL, NULL, 0, 0, '2018-12-24 12:25:49', '2018-12-24 12:25:49'),
(137, 'meeting', 37, 12, 22, 'اختيار أسم للنظام', '2018-12-25', NULL, NULL, NULL, 0, NULL, 'اختيار مسمى مناسب لنظام المحاماة', 1, 0, '2018-12-25 13:58:24', '2019-01-03 09:32:30'),
(138, 'meeting', 37, 12, 42, 'تجهيز محضر إجتماع مكتب الفريان', '2018-12-25', NULL, NULL, NULL, 0, NULL, 'تجهيز محضر إجتماع مكتب الفريان المنعقد يوم أمس الإثنين', 0, 0, '2018-12-25 13:59:46', '2018-12-25 13:59:46'),
(139, 'meeting', 37, 12, 20, 'وضع المحضر على ملف بي دي إف', '2018-12-26', NULL, NULL, NULL, 0, NULL, 'وضع المحضر على ملف بي دي إف وإعادة إرساله إلى الأستاذة نورة بعد اعتماد الأستاذ ابراهيم على المحضر', 0, 0, '2018-12-25 14:00:36', '2018-12-25 14:00:36'),
(140, 'meeting', 37, 12, 20, 'التأكد من وجود النظام المالي في عقد الفريان', '2018-12-25', NULL, NULL, NULL, 0, NULL, 'التأكد من وجود عنصر النظام المالي في عقد الفريان وإبلاغ الأستاذة نورة بذلك', 0, 0, '2018-12-25 14:03:59', '2018-12-25 14:03:59'),
(141, 'meeting', 38, 12, 20, 'ارسال الإعلان بعدة لغات', '2018-12-25', NULL, NULL, NULL, 0, NULL, 'ارسال الإعلان الرئيسي للحملة بعدة لغات ( أوردو + انجليزي + فلبيني ) لشركة آي دكس', 1, 0, '2018-12-25 15:09:17', '2019-01-02 05:41:24'),
(142, 'meeting', 38, 12, 31, 'تصميم قالب الافكار', '2018-12-25', NULL, NULL, NULL, 0, NULL, 'تصميم قالب من قبل محمد جمال للأفكار الرئيسية', 1, 0, '2018-12-25 15:34:32', '2019-01-02 05:48:42'),
(143, 'prm', 9, 12, 30, 'الاصدار الثاني من المتجر ارقان', NULL, '2018-12-14', '2019-01-18', '35', 3, NULL, NULL, 6, 6, '2018-12-25 18:55:08', '2018-12-25 20:07:39'),
(144, 'prm', 13, 12, 30, 'التحسينات على المقراءة', NULL, '2019-01-01', '2019-01-15', '14', 2, NULL, 'التحسينات على التطبيقات و المقراءة', 0, 0, '2018-12-25 20:29:10', '2019-01-03 10:01:25'),
(145, 'meeting', 39, 12, 49, 'التأكد ن الأرقام والإحصائيات', '2018-12-26', NULL, NULL, NULL, 0, NULL, 'التأكد من الرقام والإحصائيات للحملة', 0, 0, '2018-12-26 13:22:17', '2018-12-26 13:22:17'),
(146, 'meeting', 39, 12, 49, 'اعتماد الفكرة الرئيسية', '2018-12-26', NULL, NULL, NULL, 0, NULL, '#_لا تستكثرها', 0, 0, '2018-12-26 13:22:41', '2018-12-26 13:22:41'),
(147, 'meeting', 39, 12, 49, 'تقرير الجمعية', '2018-12-26', NULL, NULL, NULL, 0, NULL, 'ارسال التقرير الخاص بالإحصائيات والأرقام للجمعية', 0, 0, '2018-12-26 13:24:13', '2018-12-26 13:24:13'),
(155, 'prm', 15, 12, 22, 'المستلزمات التقنيه', NULL, '2018-12-28', '2019-01-11', '14', 1, 20, '1- اختيار اسم للنظام \r\n2- حجز الدومين', 0, 0, '2018-12-28 19:52:58', '2018-12-28 19:52:58'),
(150, 'meeting', 29, 12, 30, 'hhhhhhhhhhhhتطبيقات المتجر', '2018-12-06', NULL, NULL, NULL, 0, NULL, 'رفع التطبيقات بعد الانتهاء من المتجر واستلام التعديلات من العميل', 1, 0, '2018-12-02 16:11:55', '2018-12-02 16:11:55'),
(156, 'prm', 15, 12, 24, 'المهام المتعلقه بنسخة مكتب ال فريان', NULL, '2018-12-28', '2019-01-09', '12', 2, 21, 'سيتم رفع كافة التعديلات التي يحتاجها المكتب \r\nبالاضافه الى تركيب الموارد البشريه', 0, 0, '2018-12-28 19:57:28', '2018-12-28 19:57:28'),
(167, 'prm', 15, 12, 50, 'براند للنظام', NULL, '2018-12-31', '2019-01-31', '31', 1, 20, NULL, 0, 0, '2018-12-31 19:12:48', '2018-12-31 19:12:48'),
(158, 'meeting', 41, 12, 46, 'اجتماع مع حفر الباطن', '2019-01-01', NULL, NULL, NULL, 0, NULL, 'اجتماع مع متب الحفر ومناقشة الافكار والخطة الرمضانية وتحديد لقاء اسبوعي دوري', 0, 0, '2018-12-29 21:36:46', '2018-12-29 21:36:46'),
(159, 'meeting', 41, 12, 20, 'حجز الاستضافة والدومين لاعلاناتي', '2018-12-31', NULL, NULL, NULL, 0, NULL, 'المهمة متاخرة', 0, 0, '2018-12-29 21:38:25', '2018-12-29 21:38:25'),
(160, 'meeting', 41, 12, 20, 'رفع الحسابات', '2018-12-29', NULL, NULL, NULL, 0, NULL, 'لم يتم بسبب عدم تفعيل الاشعارات ورمز الحماية ( تنسيق اجتماع ثلاثي رياض وصلاح وشاكر )', 0, 0, '2018-12-29 21:39:58', '2018-12-29 21:39:58'),
(161, 'meeting', 41, 12, 20, 'خير تك', '2018-12-29', NULL, NULL, NULL, 0, NULL, 'نقل المشروع لسمير بعد التنسيق مع ابراهيم', 0, 0, '2018-12-29 21:40:53', '2018-12-29 21:40:53'),
(162, 'meeting', 41, 12, 20, 'اضافة موظفة في جروب مكتب المحاماة', '2018-12-29', NULL, NULL, NULL, 0, NULL, 'بعد التنسيق مع ابراهيم', 0, 0, '2018-12-29 21:41:50', '2018-12-29 21:41:50'),
(163, 'meeting', 41, 12, 20, 'سداد الدفعة المالية من مكتب المحاماة', '2018-12-30', NULL, NULL, NULL, 0, NULL, 'متابعة السداد بعد رفع المطالبة', 0, 0, '2018-12-29 21:42:51', '2018-12-29 21:42:51'),
(164, 'meeting', 41, 12, 20, 'المكود', '2018-12-29', NULL, NULL, NULL, 0, NULL, 'بانتظار تسليم مهام لانجازها للمكود', 0, 0, '2018-12-29 21:43:50', '2018-12-29 21:43:50'),
(165, 'meeting', 41, 12, 30, 'تأخر بعض الاعمال للارقان', '2018-12-29', NULL, NULL, NULL, 0, NULL, 'التاخر بسبب حملة تمت في الكويت من قبل العميل والعمل قائم بحسب الخطة', 1, 0, '2018-12-29 21:44:49', '2019-01-02 06:01:08'),
(166, 'prm', 15, 12, 50, 'اعداد ماستر بيج للبيع النظام', NULL, '2018-12-31', '2019-01-17', '17', 1, 20, 'تتكون من ( هيدر + عن النظام- فيديو-+ الاسعار + عن البرامج+ الاسئله الشائعه+ شركاء + اطلب النظام + فوتر)', 8, 8, '2018-12-31 19:07:28', '2019-01-04 01:49:25'),
(168, 'prm', 15, 12, 50, 'الملف التعريفي للنظام', NULL, '2018-12-31', '2019-01-31', '31', 1, 20, 'يتكون الملف التعريفي من التالي\r\n١- اسم النظام \r\n٢- عباره تسويقيه تجذب القاري ان يتعرف على النظام \r\n٣- ماهو النظام ادارة مكاتب المحاماة\r\n٤- لماذا نحن \r\n٥- كيف نعمل \r\n٦- الانظمة التي يتكون منها النظام\r\n٧- مميزات النظام \r\n٨- الباقات \r\n٨- ميزة ادارة النظام \r\n٩- اسئله شائعه\r\n١٠- \r\n١١- \r\n١٢ - حسابات النظام', 0, 0, '2018-12-31 19:18:21', '2018-12-31 19:18:21'),
(169, 'prm', 15, 12, 50, 'الفيديو التسويقي للنظام', NULL, '2018-12-31', '2019-01-31', '31', 1, 20, NULL, 0, 0, '2018-12-31 19:21:15', '2018-12-31 19:21:15'),
(170, 'prm', 15, 12, 50, 'مهام تسويقيه فرعيه', NULL, '2019-01-01', '2019-01-31', '30', 1, 20, NULL, 0, 0, '2018-12-31 19:23:57', '2018-12-31 19:27:30');

-- --------------------------------------------------------

--
-- Structure de la table `task_statuses`
--

CREATE TABLE `task_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `task_statuses`
--

INSERT INTO `task_statuses` (`id`, `company_id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(9, 1, 'مهام جديدة', '#ffbe33', '2018-11-27 16:15:04', '2018-11-27 16:15:04'),
(10, 1, 'مهام قيد العمل', '#0f98cf', '2018-11-27 16:20:55', '2018-11-27 16:30:55'),
(4, 1, 'مهام منجزة', '#27d166', '2018-11-27 16:31:06', '2018-11-27 16:31:22'),
(5, 12, 'موقفة مؤقتًا', '#8f0000', '2018-12-08 01:42:03', '2018-12-08 01:42:03'),
(8, 12, 'انتظار العميل', '#919191', '2018-12-08 02:02:52', '2018-12-23 13:01:50'),
(1, 0, 'منجزة', 'green', NULL, NULL),
(2, 0, 'متأخرة', 'red', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `task_user`
--

CREATE TABLE `task_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `task_user`
--

INSERT INTO `task_user` (`id`, `task_id`, `user_id`) VALUES
(27, 21, 20),
(16, 12, 34),
(15, 12, 33),
(18, 13, 33),
(17, 13, 35),
(20, 13, 5),
(19, 13, 4),
(21, 14, 33),
(25, 15, 34),
(24, 15, 33),
(23, 16, 34),
(22, 16, 33),
(26, 17, 33),
(28, 21, 29),
(29, 22, 29),
(30, 23, 20),
(31, 24, 31),
(32, 25, 31),
(33, 28, 28),
(34, 29, 23),
(35, 30, 31),
(36, 31, 29),
(37, 35, 31),
(38, 36, 29),
(39, 37, 20),
(40, 37, 31),
(41, 38, 20),
(42, 40, 20),
(43, 42, 20),
(44, 44, 20),
(45, 45, 20),
(48, 48, 7),
(47, 48, 33),
(49, 50, 33),
(51, 54, 23),
(52, 60, 31),
(53, 63, 20),
(54, 68, 20),
(55, 71, 20),
(56, 73, 31),
(57, 74, 20),
(59, 75, 30),
(60, 77, 31),
(61, 83, 20),
(62, 85, 22),
(63, 87, 30),
(64, 88, 22),
(65, 91, 22),
(66, 92, 42),
(67, 92, 22),
(68, 93, 27),
(69, 94, 22),
(70, 97, 22),
(71, 98, 20),
(72, 102, 22),
(73, 104, 20),
(74, 106, 20),
(75, 108, 20),
(76, 110, 20),
(77, 112, 31),
(78, 113, 20),
(79, 114, 31),
(80, 115, 20),
(81, 117, 30),
(82, 120, 20),
(83, 122, 20),
(84, 125, 30),
(85, 127, 20),
(86, 128, 20),
(87, 128, 22),
(88, 130, 20),
(89, 131, 30),
(90, 132, 22),
(91, 133, 31),
(92, 134, 31),
(93, 134, 42),
(94, 135, 31),
(95, 136, 31),
(96, 139, 42),
(97, 140, 42),
(98, 142, 20),
(99, 143, 22),
(100, 143, 20),
(101, 144, 22),
(105, 155, 50),
(106, 155, 32),
(107, 156, 50),
(108, 156, 32),
(120, 170, 22),
(119, 167, 32),
(111, 158, 20),
(112, 159, 50),
(113, 160, 30),
(114, 161, 22),
(115, 162, 50),
(116, 164, 30),
(117, 166, 22),
(118, 166, 20),
(121, 170, 20);

-- --------------------------------------------------------

--
-- Structure de la table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `taxes`
--

INSERT INTO `taxes` (`id`, `company_id`, `name`, `percentage`, `created_at`, `updated_at`) VALUES
(1, 1, 'الضريبة الأولى', '5', '2018-11-27 17:42:56', '2018-11-27 17:42:56'),
(2, 1, 'الضريبة الثانية', '14', '2018-11-27 17:52:56', '2018-11-27 17:54:45'),
(5, 12, 'zerzer', '7', '2019-01-04 03:37:49', '2019-01-04 03:37:49');

-- --------------------------------------------------------

--
-- Structure de la table `timers`
--

CREATE TABLE `timers` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(12) NOT NULL,
  `daftar_id` int(11) NOT NULL,
  `task_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `timers`
--

INSERT INTO `timers` (`id`, `company_id`, `daftar_id`, `task_id`, `user_id`, `start_time`, `end_time`, `status`, `note`, `price`, `created_at`, `updated_at`) VALUES
(6, 12, 7, NULL, 22, '2018-12-08 04:57:59', '2018-12-08 04:59:15', 'closed', 'ا', 0, '2018-12-08 01:57:59', '2018-12-08 01:59:15'),
(7, 12, 6, NULL, 24, '2018-12-08 13:06:28', '2018-12-08 13:10:52', 'closed', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', NULL, '2018-12-08 10:06:28', '2018-12-08 10:10:52'),
(2, 1, 1, 48, 33, '2018-12-11 10:43:52', '2018-12-19 16:44:00', 'closed', 'sdfsdfsdqfqfqsdf', 520, '2018-12-01 09:47:56', '2018-12-01 10:22:51'),
(4, 1, 1, 48, 33, '2018-12-18 11:25:23', '2018-12-24 11:25:26', 'closed', 'qsdqsd', 4, '2018-12-01 10:25:32', '2018-12-01 10:25:32'),
(8, 12, 6, 78, 24, '2018-12-08 13:13:30', '2018-12-08 13:13:58', 'closed', 'qsdqsd', NULL, '2018-12-08 10:13:30', '2018-12-08 10:13:58'),
(9, 12, 6, 78, 24, '2018-12-08 13:39:53', '2018-12-08 13:42:56', 'closed', NULL, NULL, '2018-12-08 10:39:53', '2018-12-08 10:42:56'),
(10, 12, 8, 83, 22, '2018-12-14 12:19:03', '2018-12-14 12:22:08', 'closed', NULL, NULL, '2018-12-14 09:19:17', '2018-12-14 09:19:17'),
(11, 12, 6, 80, 22, '2018-12-23 12:28:59', '2018-12-23 12:29:25', 'closed', 'hhhh', NULL, '2018-12-23 12:28:59', '2018-12-23 12:29:25'),
(12, 12, 7, 77, 22, '2019-01-04 01:54:18', '2019-01-04 01:54:30', 'closed', NULL, NULL, '2019-01-04 01:54:18', '2019-01-04 01:54:30');

-- --------------------------------------------------------

--
-- Structure de la table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entreprise` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `trainings`
--

INSERT INTO `trainings` (`id`, `company_id`, `user_id`, `status`, `name`, `entreprise`, `from_date`, `to_date`, `price`, `details`, `file`, `created_at`, `updated_at`) VALUES
(1, 12, 39, 0, 'تدريب 1', 'مؤسسة 1', '2018-12-01', '2018-12-23', '52', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي', 'uploads/attachments/mvUw6v41ccf701zdT4sGVVYISgStqgaQ7RQlC7oV.jpeg', '2018-12-20 16:15:27', '2018-12-20 16:15:27'),
(2, 12, 24, 0, 'تدريب 2', 'مؤسسة 2', '2018-12-01', '2018-12-02', '100', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي', 'uploads/attachments/MQ6L2EfUk8ki0sXGfk2N7QGgx24DEA3f4tPChMUC.jpeg', '2018-12-20 16:15:37', '2018-12-20 16:53:40');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `company_id`, `type`, `role_id`, `name`, `email`, `phone`, `password`, `image`, `status`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(32, 12, 0, 7, 'ياسر القرشي', 'enjaz@ccreation.sa', '0509193274', '$2y$10$5Fu9Nn51Hv.kzQU65XU9yO45DrVQGdfgBtVVzaT371D365COJl1y2', 'uploads/users/P2l2TSYptgIhxZEJb1fwYo3vtIcrsRxuOc9yN6gn.jpeg', 1, NULL, '68dUaC3bIUO7zMIIR8rQY932RNF5rcRSH9aIrJjl9mBuKk8LfOJPjZSIxZXO', '2018-11-20 12:16:57', '2018-12-28 01:56:11'),
(30, 12, 0, 7, 'صلاح علوي الشرفي', 'SALAH@CCREATION.SA', '535779245', '$2y$10$fOOvBU5DjKJXgLMzEExoP.o08gLDEcSq4WyV5tjFoTLcLJq38fCIK', 'uploads/users/iONCAA3d6JRrfP6aLMrCshbyQTLrzEbTbCjiCvhv.jpeg', 1, NULL, 'wpkYhlnjV4ESL3RHQ8shWGTNESzRqCbNF4getQjmJJKmy4XB1LcwthODeXOy', '2018-11-20 12:09:51', '2018-12-27 20:00:14'),
(31, 12, 0, 7, 'رشا العتيبي', 'rasha@ccreation.sa', '541009675', '$2y$10$rXcCZHd783yL8xgqQlitIexSKeKrYYpgOtNJ2A5HYKnoQjugRGFSG', 'uploads/users/lciYct2Ki4W3MXuROMncIuAjYyhcDtHe2HQSAn8O.jpeg', 1, NULL, '3kPoOr6gA2rHNcwF8Yzbxj2WTVMvQE5Fi3vjhlXnqPIlGWm7dRcJY79AZfqG', '2018-11-20 12:10:44', '2018-12-27 19:59:14'),
(47, 12, 1, 1, 'مؤسسة الأرقان', 'majedayoub@live.com', '00', '$2y$10$u4ww0CErJynyzbbxEJa1dOVWxuY9vokYZDRPEQAw5UlQxLFYg1xju', 'uploads/clients/qDlcv87562omDpgBxsjly2yLoxojvuMckFhaiW2S.png', 1, NULL, NULL, '2018-12-25 17:17:55', '2019-01-05 00:54:14'),
(4, 1, 1, 1, 'العميل الأول', 'client1@client.com', NULL, '$2y$10$bTGqtM0kBwe85XqD4mvUVeeY/5xKtQfxmVX1umblGognF8c5fA/7G', 'uploads/clients/lVkhLkl1aOlG1Mxb4mGVdo5WsOKHZQCpD3PAU4x1.jpeg', 1, NULL, NULL, '2018-11-02 15:38:10', '2018-11-02 15:38:10'),
(5, 1, 1, 1, 'العميل الثاني', 'client2@client.com', NULL, '$2y$10$yx.3GT/DQ9eeyqZDXVbBlO7OeY3GN.6eoSIjIlRKN..TZfqDNbOs6', 'uploads/clients/XYZ1mATChdgGHohie9AZLwQcvrizgwFRhID3esW2.jpeg', 1, NULL, NULL, '2018-11-02 15:38:52', '2018-11-02 15:38:52'),
(7, 1, 0, 1, 'Hamed Issam', 'isssam.web.technologie@gmail.com', '3126589712', '$2y$10$FMh5sl1Vk49gIwoR20dbl.Rw6cRq2yGJmZuaTCuOkSGd726PmIMn2', 'uploads/clients/l7Y539WezKIkX4xLcLkQTTOsXbCocDTmwq5S6Toy.jpeg', 1, NULL, NULL, '2018-11-02 15:53:46', '2018-11-23 19:22:10'),
(27, 12, 0, 7, 'اسماعيل السلو', 'ismail@ccreation.sa', '582983966', '$2y$10$tEqXxbRSTuupcrOG0HtYbu9.lvdYU5cKqaYqxYH8epABTjcdI7zXS', 'uploads/users/zkXDEbx2tBTwqJXnJyoZebN7DxNHVvCysPlLku6I.jpeg', 1, NULL, 'hqwJi1vmTWgCDDIAAxP6c4dDdoHEgssF7ep0Qf8kHbjbpxTCdwIuv2CnO5AF', '2018-11-20 12:03:11', '2018-12-27 20:04:42'),
(10, 1, 1, 1, 'العميل الثالث', 'employee3@employee.com', '123645987654', '$2y$10$KXfYogzPr3A5YRW.29VU5eufc5oLbBjiZ3nOAZJFx6pRV8XWle7e.', 'uploads/clients/IwL3cKr8xb9UwdWUu8U5Kk4VWXRvuL4BwYy5M7Om.jpeg', 0, NULL, NULL, '2018-11-05 12:12:42', '2018-11-05 12:12:42'),
(11, 1, 1, 1, 'العميل الرابع', 'client4@client.com', '7984652136554', '$2y$10$HrFLR.IWRgTpUg14dmTnC.vOiht8fYrG.pki58f0yboEkCcrczF06', 'uploads/clients/ebcQvpOrC7TuedWZBVNoDVvawMvbFLeUbob17LNl.jpeg', 1, NULL, NULL, '2018-11-05 12:17:32', '2018-11-05 12:22:30'),
(33, 1, 0, 3, 'موظف تجريبي', 'devweb218@gmail.com', '0021699961106', '$2y$10$Q3Tu3IHv1Rvdwp80dQ0xUuwFIQlQ3Hpr.1Bv0vKMGZTGhTxVt/5Yy', 'uploads/users/yeoZWTanLBEHAqGQUJMwtBOLyjLk37qQJMvkq6FZ.jpeg', 1, NULL, 'UZ6uFN3PQPRSBMCUWmaNZBrOY6YPDx4WND54pcIcdpIIWsPeOqo6j1xCxYn8', '2018-11-21 13:51:56', '2018-11-23 19:15:50'),
(24, 12, 0, 1, 'المبرمج عصام', 'issam.web.technologie@gmail.com', '0021699961102', '$2y$10$ZD2DSIymH28RxSzSL/RLqu40DAflGyetO6gWqwD8aLgRuLdEAUSZu', 'uploads/users/W1jgl9wRHQo95EZrksqjflNITH2cR1AwjDbZZ6LE.jpeg', 1, NULL, 'qsk5H0Gh022PmC4rsrzbwktQraqSBjpnNWjIBOWHIMbGVdzs8STTrhgMmqI7', '2018-11-19 10:51:57', '2018-12-07 15:02:10'),
(25, 12, 0, 7, 'محمد جمال المصمم', 'mohdesign92@gmail.com', '972599464878', '$2y$10$4nLAi0jVYvNhBPW42ULlletZBh5PcAjWhqa9nFoNHDzBOsO5iC6JK', 'uploads/users/kH4AL1MQi5Xthr8S3Oatw8SPw1fF7ynNmoJ2pAgA.jpeg', 1, NULL, NULL, '2018-11-19 10:58:18', '2018-12-27 20:07:32'),
(28, 12, 0, 8, 'تهاني با بكر', 'Tahania@ccreation.sa', '507187011', '$2y$10$PZehT/w488/3SkqDleqQreQ5JOOg5lAw0nQFEgh.CRdx/2qmrl2AW', 'uploads/users/QQqZRZXcX4rwtGRSH1zmVAu577mkMMwG2aQupz9w.jpeg', 1, NULL, '6m0s4CHpleFWBlAHEpMmPq542ur0GjKe4HwUI2VhnzwxndVhvUYN32YFpkPu', '2018-11-20 12:04:31', '2018-12-29 03:28:50'),
(17, 11, 0, 1, 'ياسر القرشي', 'ENJAZ@ccreation.sa', '509193274', '$2y$10$0B/0gjLP5ao9OefttCzJn.YpsNCzh9xdG4QbSRftFHqKvjqD8c0z6', NULL, 1, NULL, 'tXKkr9Fn7CcJiAfy1K5pZLaBPul7k9ACkJc4cjaV4oqsagXNNfyXG8PJXxMT', '2018-11-17 12:25:23', '2018-11-17 18:19:23'),
(20, 12, 0, 1, 'رياض المحمدي', 'sa@ccreation.sa', '563421359', '$2y$10$WIrc/SeghYPoCvBnBv3kz.rl4NYR9NsMGSqTrGnPFQR2ej7SFBjZO', NULL, 1, NULL, 'GkZlOyK0UbIye87moQKScY3JJWNEwyzyXtExuHyVfjgPNAjY5yI3xQ2Aqj2B', '2018-11-18 18:17:58', '2018-12-27 04:23:03'),
(19, 11, 0, 2, 'نورة الرشود', 'pm.norah@ccreation.sa', '0544320099', '$2y$10$nh7/9qzUkO758jnah8V2HO//qEU0I5yjKbA0oshvAwGAylLrXM8bW', 'uploads/users/O71ZKT3biamZ1u17JvmsC33tgWw1c4x4bVxcxuAO.jpeg', 1, NULL, NULL, '2018-11-17 14:55:56', '2018-11-17 14:55:56'),
(22, 12, 0, 1, 'ابراهيم السلو', 'alsalo@ccreation.sa', '0502480256', '$2y$10$g.f0JGMWA0wK2sLU/bfRS.ZPUE7WiLo.z5kJ2pGj02zjVUocqGjy6', 'uploads/users/18PlpyNBGW4Ymh6V1shW2uEjnEXudMq6jrQk6Djm.gif', 1, NULL, 'gINitzYHRJtXgCO7MnMmhRZbyWT8EDXmSp4SMfGgPRMTr2MAyP9hhzZgrzxj', '2018-11-19 07:30:35', '2018-11-19 07:30:35'),
(23, 12, 0, 7, 'عمرو صبري', 'amr.sabry.designer@gmail.com', '558037380', '$2y$10$0WrvbabXYn2M00KiijILVemDXC0ASWLURxj.8ZUafqxiOj40V8VEK', 'uploads/users/U2uYzscQZmHJJfxVFprXJClA6OyPBAZ42U0AXVVU.jpeg', 1, NULL, NULL, '2018-11-19 10:11:00', '2018-12-27 20:08:49'),
(34, 1, 0, 2, 'موظف تجريبي 2', 'employee2@employee.com', '123456985', '$2y$10$tsYZEoAHDuyPTbAeRg1.kOfQcbz/Tif6QsqIhncEeHKygopFh6xnm', 'uploads/users/VRRctqnXzrXKaNlLwfSTy6yEZdMKEXmJ56VAiOyS.jpeg', 1, NULL, NULL, '2018-11-21 13:52:41', '2018-11-21 13:57:20'),
(35, 1, 0, 1, 'عصام حامد', 'issam.web.technologie2@gmail.com', '0021699961101', '$2y$10$jo3BnNtHxdZQE.bF07LIPe2By5PYRZDmK/6n8D5AIGA0gXdPzntOG', 'uploads/users/ixCoYiRdKRo5HRwXoX0U1Xo7bhwlNddktNroHrlM.jpeg', 1, NULL, 'OymBgouIMyPJUFGL9s3hXVKXYPOo00p3x6H7b0toPAQgY0UBiJ2rt6fJPDMn', '2018-11-21 13:53:31', '2018-12-01 11:17:59'),
(38, 12, 1, 1, 'شركة صناعة المحتوى', 'a@ccreation.sa', '0000000000', '$2y$10$PPkOQIJhFgyvE6qVv59e8uGI1V/lApPeiif1BrNc7fJRRv48CybXy', 'uploads/clients/FkukDlTRWbtfvl1yGsLPb6c50iZmiPhHiTIrlyBn.png', 1, NULL, NULL, '2018-12-07 09:58:25', '2018-12-08 01:51:02'),
(50, 12, 0, 8, 'نورة آل رشود', 'pmm.norah@ccreation.sa', '544320099', '$2y$10$cBsdPDPwrzfjFtY8Nb7mCONTKMfgObzAfqpm6PcCy7uhasyp9qqvO', NULL, 1, NULL, 'Fe1QLhwPoS6Lt5bHcHuYh2cVonRjYVccB14No70EUARof9FLT2N1dM3ayos7', '2018-12-27 21:46:04', '2018-12-27 21:48:30'),
(45, 12, 1, 1, 'المعمل الفني', 'sa@TT.sa', '0563421359', '$2y$10$7y2pChh9Yu7iI61.IfRac.FtRVCPZIsN06PqIisFRHiF4p8n3a1h.', 'uploads/clients/mZOF80cp4k868LDWT4PnqwSUyXSQUrJ6sxLTXMWz.jpeg', 1, NULL, 'L5vqLiGPpioMJfO5S44exRyEpQRP4JF9JlyLGsBy2ABc78FWZUqADRkGZfMe', '2018-12-24 22:34:22', '2018-12-24 22:34:22'),
(46, 12, 0, 8, 'عبد الرحمن أدريس', 'amidris@gmail.com', '0507771868', '$2y$10$uzd9cRMKFbs8vJFikn3OveMYx0utPxTmOlzdij8/8GsE47zBuSNfO', NULL, 1, NULL, 'a6fHdk92zUPwLIKeFn1vzjmZf41XtyQYsvLhw7x7fDGddYlOIftFaxaAERBL', '2018-12-25 14:55:40', '2018-12-29 21:38:14'),
(48, 14, 0, 1, 'ابراهيم', 'x@x.com', '0500000000', '$2y$10$nvAz6frkP.EYdFIAhQwRXu.MRlbcHljvCbB2D9s6EydmzVD1Mg2GC', NULL, 1, NULL, NULL, '2018-12-25 22:26:41', '2018-12-25 22:26:41'),
(49, 12, 1, 1, 'جمعية تحفيظ القرآن بالجبيل', 'k.a.aljabr@gmail.com', '0', '$2y$10$m5dNzq87YM09jTcykNq56eg80ar3rrW2EvP0jRt4ngvfTKbsv7K3e', 'uploads/clients/tgSX522FIX15rI4SAInVMIS9BqZI2HiWQi2BoR9E.png', 1, NULL, NULL, '2018-12-26 10:57:43', '2019-01-05 00:53:33'),
(51, 12, 0, 5, 'test test', 'test@test.com', '99961102', '$2y$10$0skBKo805q5c.oqujYrA..j4bDklh.x8EwNTVSAUYRdZpCHRmY6vm', NULL, 1, NULL, '446Zve0PeTfVzaTRHrtZ5j9HZIgLR8iqP36gooTzHQGxj8yCgX8mSUtbTPRT', '2019-01-02 04:38:55', '2019-01-03 00:07:47');

-- --------------------------------------------------------

--
-- Structure de la table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(12) NOT NULL DEFAULT '0',
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_passport` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_passport` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_identity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_identity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `son_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `son_birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `direct_manager_id` int(11) DEFAULT NULL,
  `joining_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `retirement_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_location_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical` int(11) DEFAULT NULL,
  `social` int(11) DEFAULT NULL,
  `cin_photo_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_photo_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resume_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_paper_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_document_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `birthday`, `gender`, `nationality`, `address`, `qualification`, `num_passport`, `date_passport`, `num_identity`, `date_identity`, `social_status`, `partner_name`, `partner_birthday`, `son_name`, `son_birthday`, `department_id`, `section_id`, `direct_manager_id`, `joining_date`, `retirement_date`, `job_time`, `job_location_id`, `designation_id`, `salary`, `bank_name`, `branch_name`, `account_name`, `account_number`, `medical`, `social`, `cin_photo_path`, `passport_photo_path`, `resume_path`, `contract_paper_path`, `other_document_path`, `created_at`, `updated_at`) VALUES
(1, 24, '1995-05-12', 'male', '69', 'نستاب سينتباسنتيس ميبتمنسيب', 'هندسة حسابات', '978654321654231', '2024-11-13', '978654321654231', '2024-11-13', 'married', 'آصال الصباغ', '1994-06-07', 'سيلبسثيب;صسثقلصثسلب;شسيشسي;eryhertetr;', '2015-07-20;2018-04-10;2016-11-29;2018-12-31;', 1, 1, 24, '2018-12-17', '2020-12-17', 'Full', 4, 3, '4500', 'بنك الجزيرة', 'فرع الرياض', 'ACC-IUYGH-EZAMR', '978654321645879', 2, 3, 'uploads/users/iB0J80ABZwtzfGBC7uDmAWV9luDQ8Ea9zYVKmgQs.jpeg', 'uploads/users/pnJVfla0W7ULvDpBBz6crKgBYAIcEMT9JGsTTINg.jpeg', 'uploads/users/5OT4p9uIbPKhklSx9bQEXJ047CKSdO09aZ2hv2Ce.jpeg', 'uploads/users/6cbS2MCKZ7M3diUGNaaTPIXfWjexbuRPNHEsLJFP.jpeg', 'uploads/users/3W1ZmuJdfpIlreZmzpMx5Py5t5999xsw2UcBRXoH.jpeg', '2018-12-17 12:02:25', '2018-12-17 15:58:31'),
(3, 30, '2018-12-02', 'male', '246', 'جدة', 'ماجستير', NULL, NULL, NULL, NULL, 'married', NULL, NULL, ';;;;', ';;;;', 6, 5, 22, '2018-12-02', NULL, 'Part', 3, 4, '2000', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-24 19:41:47', '2018-12-27 20:00:14'),
(4, 46, '2016-12-05', 'male', '183', 'الأحساء', 'ب', NULL, NULL, NULL, NULL, 'married', NULL, NULL, ';;;;', ';;;;', 6, 6, 22, '2018-11-26', '2018-12-29', 'Part', 3, 4, '2000', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-25 14:55:40', '2018-12-29 21:29:00'),
(5, 32, '2018-12-25', 'male', '246', 'الدمام', 'ثانوية عامة', NULL, NULL, NULL, NULL, 'married', NULL, NULL, ';;;;', ';;;;', 6, 7, 22, '2018-12-24', '2018-12-27', 'Part', 3, 7, '1000', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 02:58:26', '2018-12-28 01:56:11'),
(6, 27, '2018-12-26', 'male', '193', 'الدمام', 'ثانوية عامة', NULL, NULL, NULL, NULL, 'celibate', NULL, NULL, ';;;;', ';;;;', 6, 7, 22, '2018-12-04', '2018-12-19', 'Full', 4, 7, '2000', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 04:11:33', '2018-12-27 20:04:42'),
(7, 23, '2018-12-26', 'male', '193', 'Riyadh, Exit 5', 'بي', NULL, NULL, NULL, NULL, 'married', NULL, NULL, ';;;;', ';;;;', 6, 7, 22, '2018-12-12', '2018-12-12', 'Part', 4, 11, '3000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 04:20:57', '2018-12-27 20:08:23'),
(8, 20, '16-08-08', 'male', '193', 'Riyadh, Exit 5', 'بك', NULL, NULL, NULL, NULL, 'married', NULL, NULL, ';;;;', ';;;;', 1, 1, 22, '2018-12-05', '2018-12-31', 'Full', 4, 10, '5000', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 04:23:03', '2018-12-27 04:23:03'),
(9, 28, '2018-12-05', 'female', '193', 'الرياض', 'ثانوية عامة', NULL, NULL, NULL, NULL, 'celibate', NULL, NULL, ';;;;', ';;;;', 6, 6, 22, '2018-12-05', '2018-12-05', 'Part', 3, 12, '1500', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 04:30:41', '2018-12-29 03:27:53'),
(10, 31, '2018-12-12', 'female', '193', 'الطائف', 'بكالوريوس', NULL, NULL, NULL, NULL, 'celibate', NULL, NULL, ';;;;', ';;;;', 6, 6, 22, '2018-12-19', '2018-12-30', 'Full', 3, 13, '4000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 05:26:20', '2018-12-27 19:59:14'),
(11, 25, '2018-12-01', 'male', '13', 'h', 'h', NULL, NULL, NULL, NULL, 'celibate', NULL, NULL, ';;;;', ';;;;', 6, 6, 22, '2018-12-03', NULL, 'Part', 3, 11, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 20:07:32', '2018-12-27 20:07:32'),
(12, 50, '2018-12-12', 'female', '193', 'Riyadh, Exit 5', 'بك', NULL, NULL, NULL, NULL, 'celibate', NULL, NULL, ';;;;', ';;;;', 1, 1, 22, '2018-12-06', '2018-12-31', 'Full', 3, 4, '4000', NULL, NULL, 'sa@ccreation.sa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-27 21:46:04', '2018-12-27 21:46:04'),
(13, 51, '2019-01-08', 'male', '69', 'test', 'drhdrg', NULL, NULL, NULL, NULL, 'married', NULL, NULL, ';;;;', ';;;;', 1, 1, 46, '2019-01-07', NULL, 'Full', 4, 11, '4000', NULL, NULL, 'issam.web.technologie@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-02 04:38:55', '2019-01-03 00:06:47');

-- --------------------------------------------------------

--
-- Structure de la table `vacations`
--

CREATE TABLE `vacations` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vacation_category_id` int(11) NOT NULL,
  `from_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `vacations`
--

INSERT INTO `vacations` (`id`, `company_id`, `user_id`, `vacation_category_id`, `from_date`, `to_date`, `details`, `file`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 30, 4, '2018-11-01', '2018-12-03', 'هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص.', 'uploads/attachments/UsE5jQif4IsO8fHYMtvnQQTUoEcKoHxcdupwlKaN.jpeg', 1, '2018-12-20 15:03:24', '2018-12-20 15:03:24'),
(2, 12, 24, 2, '2018-11-01', '2018-12-12', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ.', NULL, 1, '2018-12-20 15:05:09', '2018-12-20 15:05:09'),
(6, 12, 23, 4, '2018-12-17', '2018-12-25', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ.', NULL, 2, '2018-12-24 12:00:39', '2018-12-24 12:31:16'),
(7, 12, 24, 2, '2018-12-11', '2018-12-13', 'هناك rthy منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ.', 'uploads/attachments/xnmCLjdRQepT5n7I8fusFmRRdhNI7AFcQTYB4A4M.jpeg', 1, '2018-12-24 12:00:57', '2018-12-24 12:30:11'),
(9, 12, 20, 4, '2018-12-12', '2018-12-31', 'ثفغثقلف', NULL, 2, '2018-12-24 12:39:52', '2018-12-27 20:17:05');

-- --------------------------------------------------------

--
-- Structure de la table `vacation_categories`
--

CREATE TABLE `vacation_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '1',
  `payed` tinyint(4) NOT NULL DEFAULT '1',
  `discout` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `vacation_categories`
--

INSERT INTO `vacation_categories` (`id`, `company_id`, `name`, `duration`, `payed`, `discout`, `created_at`, `updated_at`) VALUES
(1, 12, 'إجازة سنوية', 1, 1, 0, '2018-12-12 13:55:19', '2018-12-12 13:55:19'),
(2, 12, 'إجازة إستثنائية', 3, 0, 2, '2018-12-12 13:55:39', '2018-12-12 13:55:39'),
(3, 12, 'إجازة مرضية', 7, 1, 0, '2018-12-12 13:56:03', '2018-12-12 13:56:03'),
(4, 12, 'إجازة أمومة', 40, 1, 0, '2018-12-12 13:56:15', '2018-12-12 13:56:15');

-- --------------------------------------------------------

--
-- Structure de la table `violations`
--

CREATE TABLE `violations` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(12) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `punishment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `violations`
--

INSERT INTO `violations` (`id`, `company_id`, `name`, `punishment`, `created_at`, `updated_at`) VALUES
(1, 12, 'مخالفة', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', '2018-12-12 14:29:23', '2018-12-12 14:29:23'),
(2, 12, 'مخالفة 2', 'ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.', '2018-12-12 14:29:38', '2018-12-12 14:29:38');

-- --------------------------------------------------------

--
-- Structure de la table `votes`
--

CREATE TABLE `votes` (
  `id` int(10) UNSIGNED NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minutes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `open` tinyint(4) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `start_at` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `votes`
--

INSERT INTO `votes` (`id`, `meeting_id`, `name`, `hours`, `minutes`, `open`, `note`, `status`, `created_at`, `updated_at`, `type`, `options`, `start_at`) VALUES
(1, 14, 'تصويت 1', '0', '56', 0, NULL, 2, '2018-11-22 17:06:44', '2018-11-23 11:20:20', 0, NULL, '2018-11-23 11:23:50'),
(2, 14, 'تصويت 2', '0', '55', 0, NULL, 2, '2018-11-22 17:07:14', '2018-11-23 11:33:20', 1, 'a:4:{i:0;s:10:\"خيار 1\";i:1;s:10:\"خيار 2\";i:2;s:10:\"خيار 3\";i:3;s:10:\"خيار 4\";}', '2018-11-23 11:37:51'),
(3, 14, 'تصويت 3', '0', '57', 1, NULL, 2, '2018-11-22 17:08:09', '2018-11-23 11:37:20', 2, 'a:4:{i:0;s:10:\"خيار 4\";i:1;s:10:\"خيار 5\";i:2;s:10:\"خيار 5\";i:3;s:10:\"خيار 6\";}', '2018-11-23 11:40:02'),
(4, 14, 'تصويت 4', '0', '40', 0, NULL, 2, '2018-11-22 17:09:03', '2018-11-23 11:23:20', 3, 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', '2018-11-23 11:42:49'),
(13, 33, 'dhrergt', '0', '0', 0, NULL, 0, '2018-12-08 11:50:43', '2018-12-08 11:51:58', 1, 'a:2:{i:0;s:6:\"sdfsdf\";i:1;s:6:\"sdfsdf\";}', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `vote_users`
--

CREATE TABLE `vote_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `vote_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `result` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `vote_users`
--

INSERT INTO `vote_users` (`id`, `vote_id`, `user_id`, `result`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 35, '0', NULL, '2018-11-23 10:37:42', '2018-11-23 10:37:42'),
(2, 2, 35, '2', NULL, '2018-11-23 10:39:49', '2018-11-23 10:39:49'),
(4, 3, 35, '0;1;3', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.', '2018-11-23 10:42:32', '2018-11-23 10:42:32'),
(5, 4, 35, 'ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.', NULL, '2018-11-23 10:43:10', '2018-11-23 10:43:10');

-- --------------------------------------------------------

--
-- Structure de la table `websites`
--

CREATE TABLE `websites` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userss` text COLLATE utf8mb4_unicode_ci,
  `company_id` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dashboard_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `websites`
--

INSERT INTO `websites` (`id`, `name`, `userss`, `company_id`, `date`, `image`, `link`, `dashboard_link`, `created_at`, `updated_at`) VALUES
(1, 'sdgf', NULL, 1, '2018-12-11', 'uploads/websites/cAEiTwiD9JfxJc6zxnoNAxo1EPWaMIFlm58xTkl2.jpeg', 'https://facebook.com', 'https://facebook.com/sdg', '2018-12-07 16:53:24', '2018-12-07 16:53:24'),
(2, 'zefrzer', NULL, 1, '2018-12-25', 'uploads/websites/nT5q7EZFBBO1ooYJrgq9FRrsGI3XvSrttkDZgCsZ.jpeg', 'https://google.com', 'https://google.com/sedfhskjdf', '2018-12-07 16:54:54', '2018-12-07 17:33:56'),
(3, 'إعلاناتي', '50;22;20', 12, NULL, 'uploads/websites/Ouo0qCWXkJ95ny7ylWFpBf8TaPCCBEgrd1MwQyrM.png', 'https://ccreation.co/ads2', 'https://ccreation.co/ads2/login', '2018-12-07 17:42:03', '2019-01-05 00:56:31'),
(4, 'نظام المحاماة', '50;22;20', 12, NULL, 'uploads/websites/iXJlw5w7XL3JETPZpUxduCMO6sBDsyZr0P6tZVHg.png', 'http://ccreation.co/mouhamat.test/login', 'http://ccreation.co/mouhamat.test/login', '2018-12-08 09:09:00', '2019-01-05 00:55:43');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `accountings`
--
ALTER TABLE `accountings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Index pour la table `advances`
--
ALTER TABLE `advances`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `allowances`
--
ALTER TABLE `allowances`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `allowance_categories`
--
ALTER TABLE `allowance_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `archives`
--
ALTER TABLE `archives`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `axes`
--
ALTER TABLE `axes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category_meetings`
--
ALTER TABLE `category_meetings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `category_projects`
--
ALTER TABLE `category_projects`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `clients_olddd`
--
ALTER TABLE `clients_olddd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clients_email_unique` (`email`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Index pour la table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `custodies`
--
ALTER TABLE `custodies`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `deductions`
--
ALTER TABLE `deductions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `deduction_categories`
--
ALTER TABLE `deduction_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evaluation_hrms`
--
ALTER TABLE `evaluation_hrms`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `evas`
--
ALTER TABLE `evas`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `file_types`
--
ALTER TABLE `file_types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `inviteds`
--
ALTER TABLE `inviteds`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `invoice_categories`
--
ALTER TABLE `invoice_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `item_hrms`
--
ALTER TABLE `item_hrms`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `job_locations`
--
ALTER TABLE `job_locations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mandates`
--
ALTER TABLE `mandates`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `medical_categories`
--
ALTER TABLE `medical_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `meeting_client`
--
ALTER TABLE `meeting_client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_client_meeting_id_foreign` (`meeting_id`),
  ADD KEY `meeting_client_client_id_foreign` (`user_id`);

--
-- Index pour la table `meeting_user`
--
ALTER TABLE `meeting_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_user_meeting_id_foreign` (`meeting_id`),
  ADD KEY `meeting_user_user_id_foreign` (`user_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `milestones`
--
ALTER TABLE `milestones`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notifs`
--
ALTER TABLE `notifs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `percentages`
--
ALTER TABLE `percentages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `project_user`
--
ALTER TABLE `project_user`
  ADD KEY `project_user_project_id_foreign` (`project_id`),
  ADD KEY `project_user_user_id_foreign` (`user_id`);

--
-- Index pour la table `rewards`
--
ALTER TABLE `rewards`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `site_users`
--
ALTER TABLE `site_users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `site_user_website`
--
ALTER TABLE `site_user_website`
  ADD KEY `site_user_website_site_user_id_foreign` (`site_user_id`),
  ADD KEY `site_user_website_website_id_foreign` (`website_id`);

--
-- Index pour la table `social_categories`
--
ALTER TABLE `social_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subtasks`
--
ALTER TABLE `subtasks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `subtructions`
--
ALTER TABLE `subtructions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `task_statuses`
--
ALTER TABLE `task_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `task_user`
--
ALTER TABLE `task_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_user_task_id_foreign` (`task_id`),
  ADD KEY `task_user_user_id_foreign` (`user_id`);

--
-- Index pour la table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `timers`
--
ALTER TABLE `timers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vacations`
--
ALTER TABLE `vacations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vacation_categories`
--
ALTER TABLE `vacation_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `violations`
--
ALTER TABLE `violations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vote_users`
--
ALTER TABLE `vote_users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `websites`
--
ALTER TABLE `websites`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `accountings`
--
ALTER TABLE `accountings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `advances`
--
ALTER TABLE `advances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `allowances`
--
ALTER TABLE `allowances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `allowance_categories`
--
ALTER TABLE `allowance_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `archives`
--
ALTER TABLE `archives`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2168;

--
-- AUTO_INCREMENT pour la table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT pour la table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `axes`
--
ALTER TABLE `axes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT pour la table `category_meetings`
--
ALTER TABLE `category_meetings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `category_projects`
--
ALTER TABLE `category_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `clients_olddd`
--
ALTER TABLE `clients_olddd`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT pour la table `custodies`
--
ALTER TABLE `custodies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `deductions`
--
ALTER TABLE `deductions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `deduction_categories`
--
ALTER TABLE `deduction_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `evaluation_hrms`
--
ALTER TABLE `evaluation_hrms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `evas`
--
ALTER TABLE `evas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `expense_categories`
--
ALTER TABLE `expense_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `file_types`
--
ALTER TABLE `file_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `inviteds`
--
ALTER TABLE `inviteds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT pour la table `invoice_categories`
--
ALTER TABLE `invoice_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `item_hrms`
--
ALTER TABLE `item_hrms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `job_locations`
--
ALTER TABLE `job_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `mandates`
--
ALTER TABLE `mandates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `medical_categories`
--
ALTER TABLE `medical_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT pour la table `meeting_client`
--
ALTER TABLE `meeting_client`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `meeting_user`
--
ALTER TABLE `meeting_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT pour la table `milestones`
--
ALTER TABLE `milestones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=899;

--
-- AUTO_INCREMENT pour la table `notifs`
--
ALTER TABLE `notifs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT pour la table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `percentages`
--
ALTER TABLE `percentages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `rewards`
--
ALTER TABLE `rewards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT pour la table `site_users`
--
ALTER TABLE `site_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT pour la table `social_categories`
--
ALTER TABLE `social_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `subtasks`
--
ALTER TABLE `subtasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT pour la table `subtructions`
--
ALTER TABLE `subtructions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT pour la table `task_statuses`
--
ALTER TABLE `task_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `task_user`
--
ALTER TABLE `task_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT pour la table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `timers`
--
ALTER TABLE `timers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT pour la table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `vacations`
--
ALTER TABLE `vacations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `vacation_categories`
--
ALTER TABLE `vacation_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `violations`
--
ALTER TABLE `violations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `vote_users`
--
ALTER TABLE `vote_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `websites`
--
ALTER TABLE `websites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
