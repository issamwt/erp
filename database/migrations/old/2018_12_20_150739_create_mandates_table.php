<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mandates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("company_id");
            $table->integer("user_id");
            $table->string("task");
            $table->string("entreprise");
            $table->string("address");
            $table->tinyInteger("type")->default(0);
            $table->string("from_date");
            $table->string("to_date");
            $table->string("price");
            $table->text("details")->nullable();
            $table->string("file")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mandates');
    }
}
