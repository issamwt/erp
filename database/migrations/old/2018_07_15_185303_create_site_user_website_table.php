<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteUserWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_user_website', function (Blueprint $table) {
            $table->integer('site_user_id')->unsigned();
            $table->integer('website_id')->unsigned();

            $table->foreign('site_user_id')->references('id')->on('site_users')->onDelete('cascade');
            $table->foreign('website_id')->references('id')->on('websites')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('a_case_user', function (Blueprint $table) {
            //
        });
    }
}