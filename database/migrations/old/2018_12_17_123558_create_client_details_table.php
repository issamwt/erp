<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string("birthday")->nullable();
            $table->string("gender")->nullable();
            $table->string("nationality")->nullable();
            $table->string("address")->nullable();
            $table->string("qualification")->nullable();
            $table->string("num_passport")->nullable();
            $table->string("date_passport")->nullable();
            $table->string("num_identity")->nullable();
            $table->string("date_identity")->nullable();
            $table->string("social_status")->nullable();
            $table->string("partner_name")->nullable();
            $table->string("partner_birthday")->nullable();
            $table->string("son_name")->nullable();
            $table->string("son_birthday")->nullable();
            $table->integer("department_id")->nullable();
            $table->integer("section_id")->nullable();
            $table->integer("direct_manager_id")->nullable();
            $table->string("joining_date")->nullable();
            $table->string("retirement_date")->nullable();
            $table->string("job_time")->nullable();
            $table->integer("job_location_id")->nullable();
            $table->integer("designation_id")->nullable();
            $table->string("salary")->nullable();
            $table->string("bank_name")->nullable();
            $table->string("branch_name")->nullable();
            $table->string("account_name")->nullable();
            $table->string("account_number")->nullable();
            $table->integer("medical")->nullable();
            $table->integer("social")->nullable();
            $table->string("cin_photo_path")->nullable();
            $table->string("passport_photo_path")->nullable();
            $table->string("resume_path")->nullable();
            $table->string("contract_paper_path")->nullable();
            $table->string("other_document_path")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_details');
    }
}
