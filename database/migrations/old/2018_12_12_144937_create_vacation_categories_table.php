<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("company_id");
            $table->string("name");
            $table->integer("duration")->default(1);
            $table->tinyInteger("payed")->default(1);
            $table->integer("discout")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacation_categories');
    }
}
