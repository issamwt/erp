<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>نظام إجتماعاتي</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{asset("public/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("public/dist/css/AdminLTE.min.css")}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page" style="background-image: url('{{asset("public/uploads/meetings.jpg")}}'); background-size:cover;">
<div class="login-box" style="margin-top: 70px">
    <div class="login-logo">
        <img src="{{asset("public/uploads/logo2.png")}}" style="display: block; margin: 10px auto; width: 50%; border: 10px solid #fff;">
    </div>
    <div class="login-box-body">
        <h4 class="login-box-msg">قم بتسجيل الدخول من هنا</h4>
        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="form-group has-feedback">
                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} text-right" name="phone" value="{{ old('phone') }}" required autofocus placeholder="رقم الجوال">
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>
            @if ($errors->has('phone'))
                <div style="margin-bottom: 20px" class="text-right red invalid-feedback" role="alert"><strong>{{ $errors->first('phone') }}</strong></div>
            @endif
            <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} text-right" name="password" required placeholder="كلمة المرور">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if ($errors->has('password'))
                <div style="margin-bottom: 20px" class="text-right red invalid-feedback" role="alert"><strong>{{ $errors->first('password') }}</strong></div>
            @endif
            <div class="row">
                <div class="col-xs-7">
                    <div class="checkbox icheck">
                        <label>
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            تذكرني
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-5">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">تسجيل الدخول</button>
                </div><!-- /.col -->
            </div>
        </form>

        {{--<a href="{{ route('password.request') }}">نسيت كلمة المرور ؟</a><br>--}}

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{asset("public/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{asset("public/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- iCheck -->
<script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<style>
    .red{
        color: red;
    }
    .icheck>label{
        padding-left: 10px;
        padding-right: 0px;
    }
</style>
</body>
</html>
