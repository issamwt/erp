<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>نظام إجتماعاتي</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{asset("public/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("public/dist/css/AdminLTE.min.css")}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-page" style="background-image: url('{{asset("public/uploads/meetings.jpg")}}'); background-size:cover;">

    <div class="authentication login-box" style="margin-top: 70px;">
        <div class="login-logo">
            <img src="{{asset("public/uploads/logo2.png")}}" style="display: block; margin: 10px auto; width: 50%; border: 10px solid #fff;">
        </div>
        <div class="login-box-body">
            <h4 class="login-box-msg" style="padding: 10px 0px 30px 0px;font-size: 17px;">ادخل الكود المرسل على رقم جوالك</h4>
            <form method="POST" action="{{ route('verification', ['id' => $user->id]) }}">
                @csrf

                <div class="card-plain">

                    <div class="form-group">
                        <div class="clock" style="margin: 20px auto 40px !important;width: 230px !important;"></div>
                        <div class="message"></div>
                    </div>

                    @include('admin.layouts.messages')

                    <div class="form-group has-feedback">
                        <input type="text" name="verification_code" id="verification_code" class="form-control" placeholder="ادخل الكـود" autofocus required>
                        <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                    </div>

                    <div class="footer">
                        <button type="submit" id="verify" class="btn btn-primary btn-round btn-block">تفعيل</button>
                        <a href="{{route('resend', ['id' => $user->id])}}" id="resend" class="btn btn-primary btn-round btn-block">إعادة إرسال الكود</a>
                        <br>
                        <a href="{{ route('login') }}" class="link">تسجيل الدخول</a><br>
                    </div>
                </div>

            </form>
        </div>
        <div id="particles-js"></div>
    </div>


<link rel="stylesheet" href="{{asset('public/dist/flipclock/flipclock.css')}}">
<script src="{{asset("public/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<script src="{{asset('public/dist/flipclock/flipclock.js')}}"></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script src="https://momentjs.com/downloads/moment-timezone.min.js"></script>
<script src="{{asset('public/dist/node-waves/waves.js')}}"></script>
<script src="{{asset('public/dist/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{asset('public/dist/jquery-countto/jquery.countTo.js')}}"></script>

<script type="text/javascript">
    var clock;

    $(document).ready(function() {
        var clock;
        clock = $('.clock').FlipClock({
            clockFace: 'MinuteCounter',
            countdown: true,
            autoStart: false,
            callbacks: {
                stop: function() {
                    $('.message').html('إنتهى الوقت المسموح');
                    $('#verify').attr('disabled',true);
                    $('#verification_code').val('');
                    $('#verification_code').attr('disabled',true);
                    $('#resend').hide();
                    $('#sendNew').removeClass('hide');
                }
            }
        });
        var sentAt = moment('{{$user->code_sent_at}}').add(5, 'minutes');
        sentAt = sentAt.format("YYYY-MM-DD HH:mm:ss");
        moment.tz.add('Asia/Riyadh|LMT +03|-36.Q -30|01|-TvD6.Q|57e5');
        var now = moment().tz("Asia/Riyadh").format("YYYY-MM-DD HH:mm:ss");
        var duration = moment.duration(moment(sentAt).diff(moment(now)));
        var diff = duration.asMinutes();
        if(diff>=0){
            clock.setTime(diff*60);
            clock.start();
        } else{
            clock.setTime(0);
            clock.stop();
            $('.message').html('إنتهى الوقت المسموح');
            $('#verify').attr('disabled',true);
            $('#resend').hide();
            $('#verification_code').val('');
            $('#verification_code').attr('disabled',true);
            $('#sendNew').removeClass('hide');
        }


    });
</script>

<script src="{{asset('public/dist/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script> <!-- Input Mask Plugin Js -->
<script src="{{asset('public/dist/particles-js/particles.min.js')}}"></script>
<script src="{{asset('public/dist/particles-js/particles.js')}}"></script>
<script src="{{asset("public/bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<style>
    .red{
        color: red;
    }
    .icheck>label{
        padding-left: 10px;
        padding-right: 0px;
    }
    .hide {
        display: none;
    }
</style>

</body>
</html>
