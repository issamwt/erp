<aside class="main-sidebar">

    <section class="sidebar">
        <ul class="sidebar-menu">
            <li><img src="{{asset("public/uploads/logo2.png")}}" style="display: block; margin: 10px auto;  width: 90px; height:90px; border-radius: 50%;border: 3px solid #fff;" ></li>
            <li class="@if(Request::url()==route("admin.home")) active @endif">
                <a href="{{route("admin.home")}}">
                    <i class="fa fa-dashboard"></i> <span>الصفحة الرئيسية</span></i>
                </a>
            </li>
            <li class="@if(Request::url()==route("admin.admins.index")) active @endif">
                <a href="{{route("admin.admins.index")}}">
                    <i class="fa fa-user"></i> <span>قائمة المستخدمين</span></i>
                </a>
            </li>
            <li class="@if(Request::url()==route("admin.companies")) active @endif">
                <a href="{{route("admin.companies")}}">
                    <i class="fa fa-building"></i> <span>قائمة الشركات</span></i>
                </a>
            </li>
            <li class="@if(Request::url()==route("admin.users.index")) active @endif">
                <a href="{{route("admin.users.index")}}">
                    <i class="fa fa-users"></i> <span>قائمة الموظفين</span></i>
                </a>
            </li>
            <li class="@if(Request::url()==route("admin.clients.index")) active @endif">
                <a href="{{route("admin.clients.index")}}">
                    <i class="fa fa-star"></i> <span>قائمة العملاء</span></i>
                </a>
            </li>
            <li class="@if(Request::url()==route("admin.messages.index")) active @endif">
                <a href="{{route("admin.messages.index")}}">
                    <i class="fa fa-envelope"></i> <span>رسائل الزوار</span></i>
                </a>
            </li>
            <li class="@if(Request::url()==route("admin.settings.index")) active @endif">
                <a href="{{route("admin.settings.index")}}">
                    <i class="fa fa-gears"></i> <span>الإعدادات</span></i>
                </a>
            </li>
            <!--
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Layout Options</span>
                    <span class="label label-primary pull-right">4</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                    <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                    <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                    <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                </ul>
            </li>
            -->
        </ul>
    </section>

</aside>