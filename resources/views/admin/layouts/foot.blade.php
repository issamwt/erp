<!-- jQuery 2.1.4 -->
<script src="{{asset("public/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{asset("public/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- DataTables -->
<script src="{{asset("public/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("public/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- SlimScroll -->
<script src="{{asset("public/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("public/plugins/fastclick/fastclick.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{asset("public/dist/js/app.min.js")}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset("public/dist/js/demo.js")}}"></script>
<!-- page script -->

<script>
    $(function () {
        $("#dataTable").dataTable(
            {
                language: {
                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر  _MENU_  مدخلات ",
                    "sZeroRecords": "لم يعثر على أية سجلات",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخلات",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث : ",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
                , "pageLength": 25
            }
        );

        setTimeout(function () {
            $(".alert-msg").fadeOut();
        }, 3000);
    });
</script>

<style>
    .dataTables_length, .dataTables_info{
        /*float: left;*/
    }
    #dataTable th{
        /*text-align: right;*/
    }
    .dataTables_filter, .dataTables_paginate{
        float: left;
    }
    .panel, .panel-heading, .panel-body, .btn{
        border-radius: 0px;
    }
</style>

@section("scripts")@show