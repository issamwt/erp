<!DOCTYPE html>
<html>
@include("admin.layouts.head")
<body class="skin-blue sidebar-mini"  dir="rtl">
<div class="wrapper">

    @include("admin.layouts.header")

    @include("admin.layouts.sidebar")

    <div class="content-wrapper">

        @include("admin.layouts.messages")

        @section("content")@show

    </div>

    @include("admin.layouts.footer")

</div>

@include("admin.layouts.foot")

</body>
</html>