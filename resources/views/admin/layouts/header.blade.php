<header class="main-header">
    <!-- Logo -->
    <a href="{{route("admin.home")}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b style="font-size: 0.8em">ERP</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>نظام ERP</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset("public/dist/img/user2-160x160.jpg")}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{asset("public/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
                            <p>
                                {{Auth::user()->name}}
                                <small>إدارة نظام إجتماعاتي</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route("admin.settings.index")}}" class="btn btn-default btn-flat">الإعدادات</a>
                            </div>
                            <div class="pull-right">
                                <a href="javascript:void(0)" class="btn btn-default btn-flat"
                                   onclick="document.getElementById('logout').submit()">تسجيل الخروج</a>
                                <form method="post" action="{{route("logout")}}" id="logout">@csrf</form>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{url('/')}}" target="_blank"><i class="fa fa-globe" style="font-size: 18px;"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>