@extends("admin.layouts.app", ["title"=>"قائمة الرسائل"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة الرسائل
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة الرسائل</b>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>التاريخ</th>
                                <th>الإسم</th>
                                <th>البريد الإلكتروني</th>
                                <th>الهاتف</th>
                                <th>الموضوع</th>
                                <th>الأوامر</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($messages as $message)
                                <tr>
                                    <td class="text-center">{{$message->created_at->diffForhumans()}}</td>
                                    <td>{{$message->name}}</td>
                                    <td>{{$message->email}}</td>
                                    <td>{{$message->phone}}</td>
                                    <td>{{$message->subject}}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target="#myModal{{$message->id}}"><i class="fa fa-eye"></i> <span>نص الرسالة</span></a>
                                        <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="document.getElementById('deleteform{{$message->id}}').submit()"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                        <form id="deleteform{{$message->id}}" action="{{route("admin.messages.destroy", $message->id)}}" method="post">@csrf {{method_field("DELETE")}}</form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @foreach($messages as $message)

        <div id="myModal{{$message->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">نص الرسالة</h4>
                    </div>
                    <div class="modal-body">
                        <p style="text-align: justify">{{$message->message}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                    </div>
                </div>

            </div>
        </div>

    @endforeach

@endsection