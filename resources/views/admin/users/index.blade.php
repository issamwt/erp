@extends("admin.layouts.app", ["title"=>"قائمة الموظفين"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة الموظفين
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة الموظفين</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("admin.users.create")}}"><i class="fa fa-plus"></i> <span>إضافة موظف</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px"></th>
                                    <th>الصورة</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الشركة</th>
                                    <th>الأوامر</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>
                                            @if($user->image)
                                                <img src="{{asset("storage/app/".$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                            @else
                                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                            @endif
                                        </td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{@$user->company->name}}</td>
                                        <td>
                                            <a class="btn btn-success btn-xs" href="{{route("admin.users.edit", $user->id)}}"><i class="fa fa-edit"></i> <span>تعديل</span></a>
                                            <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="document.getElementById('deleteform{{$user->id}}').submit()"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                            <form id="deleteform{{$user->id}}" action="{{route("admin.users.destroy", $user->id)}}" method="post">@csrf {{method_field("DELETE")}}</form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection