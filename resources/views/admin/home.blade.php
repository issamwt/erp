@extends("admin.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content-header">
        <h1>
            الرئيسية
            <small>الصفحة الرئيسية</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #EAA737;border:1px solid  #EAA737;">
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام إجتماعاتي</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/meeting.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b><i class="fa fa-gears"></i> الإعدادات</b></a><div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #006EEB;border:1px solid  #006EEB;">
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام إدارة المشاريع</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/group.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b><i class="fa fa-gears"></i> الإعدادات</b></a><div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #913CC1;border:1px solid  #913CC1;">
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام باسوورداتي</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/password.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b><i class="fa fa-gears"></i> الإعدادات</b></a><div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #92DD66;border:1px solid  #92DD66;">
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام شؤون الموظفين</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/projects.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted text-center">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b><i class="fa fa-gears"></i> الإعدادات</b></a><div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #DC0028;border:1px solid  #DC0028;">
                            <div class="coming-soon">Coming Soon</div>
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام إدارة علاقات العملاء</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/crm.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b><i class="fa fa-gears"></i> الإعدادات</b></a><div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #29CEFC;border:1px solid  #29CEFC;">
                            <div class="coming-soon">Coming Soon</div>
                            <div class="card-body">
                                <h4 class="card-title text-center">بطاقة إهداءاتي</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/gift.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b><i class="fa fa-gears"></i> الإعدادات</b></a><div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
                </div>
                
            </div>
        </div>
    </section>

    <style>
        .card-body img{
            margin: 10px auto 30px;
            display: block;
            border: 1px solid #d2d2d2;
            width: 100px;
        }
        .card{
            clear: both;
            background: #fff;
            margin-top: 20px;
            padding: 20px;
        }
        .card-title{
            margin-top: 5px;
            margin-bottom: 20px;
        }
        .card{
            position: relative;
            overflow: hidden;
        }
        .card .coming-soon{
            position: absolute;
            background: #d00202;
            padding: 2px 10px;
            width: 150px;
            text-align: center;
            color: #fff;
            font-size: 0.8em;
            top: 25px;
            left: -40px;
            transform: rotate(-45deg);
        }
    </style>

@endsection
