@extends("admin.layouts.app", ["title"=>"قائمة المستخدمين"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة المستخدمين
            <small>(الأدمن)</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة المستخدمين</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("admin.admins.create")}}"><i class="fa fa-plus"></i> <span>إضافة مستخدم</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px"></th>
                                    <th>الإسم</th>
                                    <th>رقم الجوال</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الأوامر</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($admins as $admin)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>{{$admin->name}}</td>
                                        <td>{{$admin->phone}}</td>
                                        <td>{{$admin->email}}</td>
                                        <td>
                                            <a class="btn btn-success btn-xs" href="{{route("admin.admins.edit", $admin->id)}}"><i class="fa fa-edit"></i> <span>تعديل</span></a>
                                            <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="document.getElementById('deleteform{{$admin->id}}').submit()"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                            <form id="deleteform{{$admin->id}}" action="{{route("admin.admins.destroy", $admin->id)}}" method="post">@csrf {{method_field("DELETE")}}</form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection