@extends("admin.layouts.app", ["title"=>"إضافة مستخدم"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة المستخدمين
            <small>(الأدمن)</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>إضافة مستخدم</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("admin.admins.index")}}"><i class="fa fa-list"></i> <span>قائمة المستخدمين</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-7">

                                <form action="{{route("admin.admins.store")}}" method="post">
                                    @csrf

                                    <div class="form-group">
                                        <label for="name">الإسم</label>
                                        <input type="text" name="name" id="name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">رقم الجوال</label>
                                        <input type="text" name="phone" id="phone" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input type="email" name="email" id="email" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">كلمة المرور</label>
                                        <input type="password" name="password" id="password" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection