@extends("admin.layouts.app", ["title"=>"الشركات"])

@section("content")

    <section class="content-header">
        <h1>
            الشركات
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة الشركات</b>
                        <a class="btn btn-success btn-xs pull-left" href="{{route("admin.add_company")}}"><i class="fa fa-plus"></i> <span>إضافة شركة</span></a>
                    </div>
                    <div class="panel-body">

                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th width="10"></th>
                                    <th>الشعار</th>
                                    <th>الإسم</th>
                                    <th>الرابط</th>
                                    <th>الدفاتر</th>
                                    <th>الأوامر</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>
                                            @if($company->image)
                                                <img src="{{asset("storage/app/".$company->image)}}" style="width: 80px; height: 80px; border: 1px solid #c3c3c3; border-radius: 50%; display: block; margin: 0px auto;">
                                            @else
                                                <img src="{{asset("public/uploads/logo2.png")}}" style="width: 80px; height: 80px; border: 1px solid #c3c3c3; border-radius: 50%; display: block; margin: 0px auto;">
                                            @endif
                                        </td>
                                        <td>{{$company->name}}</td>
                                        <td><a href="{{route("company.login", $company->slug)}}" target="_blank">{{route("company.login", $company->slug)}}</a></td>
                                        <td>
                                            @if($company->meetings) <strong style="display: block;">نظام إجتماعاتي</strong> @endif
                                            @if($company->crm) <strong style="display: block;">نظام إدارة علاقات العملاء</strong> @endif
                                            @if($company->prm) <strong style="display: block;">نظام إدارة المشاريع</strong> @endif
                                            @if($company->hrm) <strong style="display: block;">نظام شؤون الموظفين</strong> @endif
                                            @if($company->cards) <strong style="display: block;">بطاقة إهداءاتي</strong> @endif
                                            @if($company->passwords) <strong style="display: block;">نظام باسوورداتي</strong> @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-xs btn-success" href="{{route("admin.edit_company", $company->id)}}"><i class="fa fa-edit"></i> <span>تعديل</span></a>
                                            <a class="btn btn-xs btn-danger" href="{{route("admin.delete_company", $company->id)}}"
                                            onclick="return confirm('هل أنت متأكد ؟')"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection