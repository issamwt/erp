@extends("admin.layouts.app", ["title"=>"إضافة شركة"])

@section("content")

    <section class="content-header">
        <h1>
            الشركات
            <small>إضافة شركة</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>إضافة شركة</b>
                        <a class="btn btn-success btn-xs pull-left" href="{{route("admin.companies")}}"><i class="fa fa-reply"></i> <span>العودة للقائمة</span></a>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <form action="{{route("admin.save_company")}}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <label>الدفاتر المسموح بها لهذه الشركة</label>
                                        <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="meetings"> <strong>نظام إجتماعاتي</strong></label></div>
                                        <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="crm"> <strong>نظام إدارة علاقات العملاء</strong></label></div>
                                        <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="prm"> <strong>نظام إدارة المشاريع</strong></label></div>
                                        <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="hrm"> <strong>نظام شؤون الموظفين</strong></label></div>
                                        <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="cards"> <strong>بطاقة إهداءاتي</strong></label></div>
                                        <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="passwords"> <strong>نظام باسوورداتي</strong></label></div>
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <h4>بيانات الشركة</h4>
                                    </div>

                                    <div class="form-group">
                                        <label>الإسم</label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label>الرابط</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="slug" required placeholder="thelink" style="text-align: left">
                                            <span class="input-group-addon" style="border-left: 1px solid #d2d6de;direction: initial;padding: 6px 30px 6px 30px;">{{url('/')}}/</span>
                                        </div>
                                        <small>يجب أن يكون أحرف لاتينية فقط</small>
                                    </div>

                                    <div class="form-group">
                                        <label>الشعار</label>
                                        <input type="file" name="image" accept="image/*" class="form-control">
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group">
                                        <h4>بيانات الأدمن</h4>
                                    </div>

                                    <div class="form-group">
                                        <label for="namex">الإسم</label>
                                        <input type="text" value="{{old('namex')}}" name="namex" id="namex" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">رقم الجوال</label>
                                        <input type="text" value="{{old('phone')}}" name="phone" id="phone" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input type="email" value="{{old('email')}}" name="email" id="email" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">كلمة المرور</label>
                                        <input type="password" name="password" id="password" class="form-control" required>
                                    </div>

                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group mt-20">
                                        <hr>
                                        <button type="submit" class="btn btn-primary" style="width: 120px;">حفظ</button>
                                    </div>
                                </div>

                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">
    <script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@endsection