@extends("admin.layouts.app", ["title"=>"إضافة شركة"])

@section("content")

    <section class="content-header">
        <h1>
            الشركات
            <small>تعديل الشركة</small>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>تعديل الشركة</b>
                        <a class="btn btn-success btn-xs pull-left" href="{{route("admin.companies")}}"><i class="fa fa-reply"></i> <span>العودة للقائمة</span></a>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                                <form action="{{route("admin.update_company")}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$company->id}}">

                                    <div class="col-sm-5">

                                        <div class="form-group">
                                            <label>الدفاتر المسموح بها لهذه الشركة</label>
                                            <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="meetings" @if($company->meetings) checked @endif> <strong>نظام إجتماعاتي</strong></label></div>
                                            <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="crm" @if($company->crm) checked @endif> <strong>نظام إدارة علاقات العملاء</strong></label></div>
                                            <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="prm" @if($company->prm) checked @endif> <strong>نظام إدارة المشاريع</strong></label></div>
                                            <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="hrm" @if($company->hrm) checked @endif> <strong>نظام شؤون الموظفين</strong></label></div>
                                            <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="cards" @if($company->cards) checked @endif> <strong>بطاقة إهداءاتي</strong></label></div>
                                            <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="passwords" @if($company->passwords) checked @endif> <strong>نظام باسوورداتي</strong></label></div>
                                        </div>

                                    </div>

                                    <div class="col-sm-7">

                                        <div class="form-group">
                                            <label>الإسم</label>
                                            <input type="text" name="name" value="{{$company->name}}" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label>الرابط</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="{{$company->slug}}" name="slug" maxlength="20" required placeholder="thelink" style="text-align: left">
                                                <span class="input-group-addon" style="border-left: 1px solid #d2d6de;direction: initial;padding: 6px 30px 6px 30px;">{{url('/')}}/</span>
                                            </div>
                                            <small>يجب أن يكون أحرف لاتينية فقط</small>
                                        </div>

                                        <div class="form-group">
                                            <label>الشعار</label>
                                            <input type="file" name="image" accept="image/*" class="form-control">
                                            <br>
                                            @if($company->image)
                                                <img src="{{asset("storage/app/".$company->image)}}" style="width: 120px; height: 120px; border: 1px solid #c3c3c3;">
                                            @else
                                                <img src="{{asset("public/uploads/logo2.png")}}" style="width: 120px; height: 120px; border: 1px solid #c3c3c3;">
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" style="width: 120px;">حفظ</button>
                                        </div>

                                    </div>

                                </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">
    <script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@endsection