@extends("admin.layouts.app", ["title"=>"إضافة عميل"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة العملاء
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>إضافة عميل</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("admin.clients.index")}}"><i class="fa fa-list"></i> <span>قائمة العملاء</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-7">

                                <form action="{{route("admin.clients.store")}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group">
                                        <label for="name">الإسم</label>
                                        <input type="text" value="{{old('name')}}" name="name" id="name" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">رقم الجوال</label>
                                        <input type="text" value="{{old('phone')}}" name="phone" id="phone" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input type="email" value="{{old('email')}}" name="email" id="email" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">كلمة المرور</label>
                                        <input type="password" name="password" id="password" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="company_id">الشركة</label>
                                        <select name="company_id" id="company_id" class="form-control" required>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="image">الصورة</label>
                                        <input type="file" accept="image/*" name="image" id="image" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    </div>

                                </form>

                            </div>

                            <div class="col-sm-5"></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection