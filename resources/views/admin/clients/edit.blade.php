@extends("admin.layouts.app", ["title"=>"تعديل عميل"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة العملاء
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>تعديل عميل</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("admin.clients.index")}}"><i class="fa fa-list"></i> <span>قائمة العملاء</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-7">

                                <form action="{{route("admin.clients.update", $client->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    {{method_field("PUT")}}

                                    <div class="form-group">
                                        <label for="name">الإسم</label>
                                        <input type="text" name="name" id="name" value="{{$client->name}}" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">رقم الجوال</label>
                                        <input type="text" value="{{$client->phone}}" name="phone" id="phone" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">البريد الإلكتروني</label>
                                        <input type="email" name="email" id="email" value="{{$client->email}}" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">كلمة المرور</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                        <small>دع الحقل فارغا إذا لم تكن تريد تغيير كلمة المرور</small>
                                    </div>

                                    <div class="form-group">
                                        <label for="company_id">الشركة</label>
                                        <select name="company_id" id="company_id" class="form-control" required>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}" @if($client->company_id==$company->id) selected @endif>{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="image">الصورة</label>
                                        <input type="file" accept="image/*" name="image" id="image" class="form-control">
                                        <br>
                                        @if($client->image)
                                            <img src="{{asset("storage/app/".$client->image)}}" style="width: 120px; height: 120px; border: 1px solid #eee;">
                                        @else
                                            <img src="{{asset("public/noimage.png")}}" style="width: 120px; height: 120px; border: 1px solid #eee;">
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    </div>

                                </form>

                            </div>

                            <div class="col-sm-5"></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection