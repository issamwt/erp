@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', "خطأ")

@section('image')
    <div style="background-image: url('{{asset("public/svg/500.svg")}}');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', "عفوًا ، حدث خطأ ما على السرفر")