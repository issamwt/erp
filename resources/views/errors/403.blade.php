@extends('errors::illustrated-layout')

@section('code', '403')
@section('title', "غير ممكن")

@section('image')
<div style="background-image: url('{{asset("public/svg/403.svg")}}');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', 'عذرًا ، أنت غير مصرح لك بالوصول إلى هذه الصفحة')
