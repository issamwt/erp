@extends('errors::illustrated-layout')

@section('code', '404')
@section('title', "الصفحة غير موجودة")

@section('image')
<div style="background-image: url('{{asset("/public/svg/404.svg")}}');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', "عفوًا ، تعذر العثور على الصفحة التي تبحث عنها")
