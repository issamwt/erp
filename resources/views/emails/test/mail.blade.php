<link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

<div style="direction: rtl; text-align: right;font-family: 'Reem Kufi', sans-serif !important;">

@component('mail::message')

<div style="text-align: center!important;"><h3 style="font-size: 30px;">{{$subject}}</h3></div>

<p style="font-size: 20px; color: #333;text-align: right;margin-top: 20px;padding: 20px 20px 100px;background: #f7f7f7;">{!! $body !!}</p>

@endcomponent


</div>