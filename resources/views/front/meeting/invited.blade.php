@extends("front.parts.app")

@section("content")

    <section style="background: #fff; min-height: 620px">
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>حالة الإجتماعات</label></div>
                                <div class="col-sm-8">
                                    @if($meeting->status==0)
                                        <strong class="alert alert-success alert-xs">جديد</strong>
                                    @elseif($meeting->status==1)
                                        <strong class="alert alert-info alert-xs">جاري</strong>
                                    @elseif($meeting->status==2)
                                        <strong class="alert alert-warning alert-xs">منته</strong>
                                    @elseif($meeting->status==3)
                                        <strong class="alert alert-danger alert-xs">ملغى</strong>
                                    @else
                                        <span class="alert alert-default">coming soon</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>سلسلة إجتماعات</label></div>
                                <div class="col-sm-8">{{@$meeting->category->name}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>موضوع الإجتماع</label></div>
                                <div class="col-sm-8">{{$meeting->name}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 0px">
                                <div class="col-sm-4"><label>مكان الإجتماع</label></div>
                                <div class="col-sm-8">{{@$meeting->location->name}}</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3"><label>التاريخ</label></div>
                                <div class="col-sm-9">{{$meeting->from_date}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3"><label>الوقت</label></div>
                                <div class="col-sm-9">{{$meeting->from_time}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3"><label>المدة</label></div>
                                <div class="col-sm-9"><span>{{floor($meeting->duration/60)}}</span> <span>ساعة</span> : <span>{{floor($meeting->duration%60)}}</span> <span>دقيقة</span></div>
                            </div>
                            <div class="row" style="margin-bottom: 0px">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9"></div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4 a">
                                    <label class="choice @if($meeting->type==0) active @endif">
                                        <i class="fa fa-check"></i><span>إجتماع عادي</span>
                                    </label>
                                </div>
                                <div class="col-sm-4 b">
                                    <label class="choice @if($meeting->type==1) active @endif">
                                        <i class="fa fa-times"></i><span>إجتماع مهم</span>
                                    </label>
                                </div>
                                <div class="col-sm-4 c">
                                    <label class="choice @if($meeting->type==2) active @endif">
                                        <i class="fa fa-exclamation"></i><span>إجتماع طارئ</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix" style="margin-bottom: 10px"></div>
                        <div class="col-sm-8">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3"><label>طريقة الإجتماع</label></div>
                                <div class="col-sm-9">
                                    @if($meeting->online==0)
                                        <div>عن بعد</div>
                                    @elseif($meeting->online==1)
                                        <div>حضوريا</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if($meeting->online==1)
                            <div class="col-sm-8">
                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-sm-3"><label>رابط الغوغل ماب</label></div>
                                    <div class="col-sm-9"><a style="text-decoration: underline !important;" href="{{$meeting->gmap_url}}" target="_blank">{{$meeting->gmap_url}}</a></div>
                                </div>
                            </div>
                        @endif
                        <div class="col-sm-8">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3"><label>تسجيل الإجتماع</label></div>
                                <div class="col-sm-9">
                                    @if($meeting->record==0)
                                        <div>لا</div>
                                    @else
                                        <div>نعم</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>مدير الإجتماع</label></div>
                                <div class="col-sm-8">@if(@$meeting->manager){{@$meeting->manager->name}}@else<span>لا يوجد</span>@endif</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>أمين الإجتماع</label></div>
                                <div class="col-sm-8">@if(@$meeting->amin){{@$meeting->amin->name}}@else<span>لا يوجد</span>@endif</div>
                            </div>
                        </div>
                        <div class="col-sm-2"><div style="height: 80px; width: 2px; background: red;margin: 0px auto"></div></div>
                        <div class="col-sm-5">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>الموظفين المدعوين</label></div>
                                <div class="col-sm-8">
                                    @if(count(@$meeting->users)>0)
                                        @foreach(@$meeting->users as $user) <span style="margin-left: 10px;"><b>[</b>{{@$user->name}}<b>]</b> </span>@endforeach
                                    @else
                                        <span>لا يوجد</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-4"><label>العملاء المدعوين</label></div>
                                <div class="col-sm-8">
                                    @if(count(@$meeting->clients)>0)
                                        @foreach(@$meeting->clients as $client) <span style="margin-left: 10px;"><b>[</b>{{@$client->name}}<b>]</b> </span>@endforeach
                                    @else
                                        <span>لا يوجد</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-sm-2">حالة الحضور</div>
                        <div class="col-sm-10">
                            @if($invited)
                                @if(@$invited->attendance==0)
                                    <span>سأحضر</span>
                                @elseif(@$invited->attendance==1)
                                    <span>لن أحضر</span>
                                @elseif(@$invited->attendance==2)
                                    <span>ربما أحضر</span>
                                @else
                                    <span>غائب</span>
                                @endif
                            @else
                                <span>بإنتظار الرد</span>
                            @endif
                            @if(@$invited->note)
                                <br><p>{{@$invited->note}}</p>
                            @endif
                        </div>
                    </div><hr style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-sm-2">تغيير حالة الحضور</div>
                        <div class="col-sm-10">

                            <form action="{{route("save_invited")}}" method="post">
                                @csrf

                                <input type="hidden" name="meeting_id" value="{{$meeting->id}}">
                                <input type="hidden" name="user_id" value="{{$userx->id}}">

                                <label class="containerx"> سأحضر
                                    <input type="radio" name="attendance" value="0" @if(!$invited or @$invited->attendance==0) checked="checked" @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="containerx">لن أحضر
                                    <input type="radio" name="attendance" value="1" @if(@$invited->attendance==1) checked="checked" @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="containerx">ربما أحضر
                                    <input type="radio" name="attendance" value="2" @if(@$invited->attendance==2) checked="checked" @endif>
                                    <span class="checkmark"></span>
                                </label>

                                <textarea class="form-control" name="note" rows="4" style="border-radius: 0px; margin-bottom: 20px;">{{@$invited->note}}</textarea>

                                <button type="submit" class="btn btn-primary" style="width: 120px; border-radius: 0px; background: #337ab7;border-color: #337ab7;">حفظ</button>

                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>

        </div>
    </section>

    <style>
        #mainNav {
            background: #212529;
        }

        .choice {
            width: 90px;
            height: 90px;
            border: 1px solid #b9b9b9;
            border-radius: 4px;
            text-align: center;
            line-height: 35px;
            display: block;
            margin: 0px auto;
            padding-top: 15px;
        }

        .col-sm-4.a .choice {
            background: #60bd03;
        }

        .col-sm-4.a .choice.active {
            box-shadow: 0px 0px 3px 3px #60bd03;
            border-color: #458802;
        }

        .col-sm-4.b .choice {
            background: hotpink;
        }

        .col-sm-4.b .choice.active {
            box-shadow: 0px 0px 3px 3px hotpink;
            border-color: #b74b81;
        }

        .col-sm-4.c .choice {
            background: crimson;
        }

        .col-sm-4.c .choice.active {
            box-shadow: 0px 0px 3px 3px crimson;
            border-color: #a80f2e;
        }

        .choice .fa {
            font-size: 1.5em;
            background: #fff;
            padding: 4px;
            border-radius: 50%;
            border: 1px solid #b9b9b9;
            width: 31px;
            height: 31px;
            text-align: center;
            line-height: 21px;
        }

        .choice span {
            font-size: 0.6em;
            background: #fff;
            display: block;
            height: 17px;
            line-height: 18px;
            width: 70%;
            margin: 0px auto;
        }

        .containerx {
            display: inline-block;
            position: relative;
            padding-right: 35px;
            margin-left: 20px;
            cursor: pointer;
            font-size: 20px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .containerx input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        .checkmark {
            position: absolute;
            top: 0;
            right: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 50%;
        }

        .containerx:hover input ~ .checkmark {
            background-color: #ccc;
        }

        .containerx input:checked ~ .checkmark {
            background-color: #337ab7;
        }

        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        .containerx input:checked ~ .checkmark:after {
            display: block;
        }

        .containerx .checkmark:after {
            top: 9px;
            left: 9px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
        }

        .btn-primary:hover{
            background: #337ab7 !important;
            border-color: #337ab7 !important;
        }

        .alert-xs {
            font-size: 0.9em;
            padding: 1px 3px !important;
            display: inline-block;
            width: 70px;
            margin: 0px 10px 0px 0px;
            border-radius: 0px !important;
            text-align: center;
        }

    </style>

@endsection