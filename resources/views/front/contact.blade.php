<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">إتصل بنا</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form id="contactForm" name="sentMessage" novalidate="novalidate">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" type="text" placeholder="إسم الكريم *" required="required" data-validation-required-message="من فضلك اكتب إسمك.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" type="email" placeholder="البريد الإلكتروني *" required="required" data-validation-required-message="الرجاء كتابة عنوان البريد الإلكتروني الخاص بك.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="phone" type="tel" placeholder="رقم الهاتف *" required="required" data-validation-required-message="يرجى كتابة رقم الهاتف الخاص بك.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="subject" type="text" placeholder="الموضوع *" required="required" data-validation-required-message="يرجى كتابة موضوع الرسالة.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="message" placeholder="التفاصيل *" required="required" data-validation-required-message="من فضلك اكتب نص رسالة."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                            <input type="hidden" id="_token" value="{{csrf_token()}}">
                        </div>
                        <div class="col-sm-6">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d83565.2099921537!2d46.588607708320176!3d24.698497672449896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2stn!4v1540486954845" style="width: 100%; min-height: 545px; border-radius: 5px" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">أرسل الآن</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    var urll = '{{route("contact")}}';
</script>

<style>
    section#contact .form-group input, section#contact .form-group textarea{
        padding: 12px !important;
    }
</style>