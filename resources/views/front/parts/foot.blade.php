<!-- Bootstrap core JavaScript -->
<script src="{{asset("public/front/vendor/jquery/jquery.min.js")}}"></script>
<script src="{{asset("public/front/vendor/bootstrap/js/bootstrap.bundle.min.js")}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset("public/front/vendor/jquery-easing/jquery.easing.min.js")}}"></script>

<!-- Contact form JavaScript -->
<script src="{{asset("public/front/js/jqBootstrapValidation.js")}}"></script>
<script src="{{asset("public/front/js/contact_me.js")}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset("public/front/js/agency.min.js")}}"></script>

<script>

    $(function () {
       $(".btn-portfolio").on("click", function (e) {
           e.preventDefault();
           var val = $(this).data("id");
           $(".btn-portfolio").removeClass("active");
           $(this).addClass("active");
           $(".portfolio-item").fadeOut(200);
           setTimeout(function () {
               $( ".portfolio-item" ).each(function( index ) {
                   if($( this ).data("id")==val){
                       $( this ).fadeIn();
                       console.log(val);
                   }
               });
               console.log("-------------");
           }, 500);

       })
    });

</script>