<!DOCTYPE html>
<html lang="en">

@include("front.parts.head")

<body id="page-top">

    @include("front.parts.navbar")

    @section("content")@show

    @include("front.parts.footer")

    @include("front.parts.foot")

</body>

</html>