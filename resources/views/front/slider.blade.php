<header class="masthead" id="slider">
    <div class="container">
        <div class="intro-text">
            <div class="intro-heading text-uppercase">نظام ERP</div>
            <div class="intro-lead-in">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</div>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">أحصل على نسخة تجريبية</a>
        </div>
    </div>
</header>