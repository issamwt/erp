<section id="aboutus" style="background-color: #f8f9fa !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase" style="margin-bottom: 80px;">لماذا ERP</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <iframe style="width: 100%; min-height: 300px; margin-top: 10px" src="https://www.youtube.com/embed/t1NjQuXTUSU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-sm-6">
                <p style="line-height: 40px;font-size: 22px;text-align: justify;">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو وكأنها نص مقروء.</p>
            </div>
        </div>
    </div>
</section>