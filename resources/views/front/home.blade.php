@extends("front.parts.app")

@section("content")

    @include("front.slider")

    @include("front.aboutus")

    @include("front.portfolio")

    @include("front.services")

    @include("front.partners")

    @include("front.testimonials")

    @include("front.contact")

@endsection