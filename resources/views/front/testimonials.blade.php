<section class="bg-light" id="team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">قالو عنا</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="{{asset("public/front/img/team/1.jpg")}}" alt="">
                    <h4>ما هو "لوريم إيبسوم" ؟</h4>
                    <p class="text-muted">هناك حقيقة مثبتة منذ زمن طويل</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="{{asset("public/front/img/team/2.jpg")}}" alt="">
                    <h4>ما فائدته ؟</h4>
                    <p class="text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="{{asset("public/front/img/team/3.jpg")}}" alt="">
                    <h4>ما أصله ؟</h4>
                    <p class="text-muted">هو ببساطة نص شكلي</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <p class="large text-muted">هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات.</p>
            </div>
        </div>
    </div>
</section>