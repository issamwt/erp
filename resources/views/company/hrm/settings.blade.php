@extends("company.layouts.app", ["title"=>"الإعدادات"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة الموارد البشرية</h1>
    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>المسميات الوظيفية</strong></div>
                            <div class="panel-body">

                                <div id="div11">
                                    <div id="div1">

                                        <form action="{{route("company.hrm.save_designation", $company->slug)}}" class="form1" method="post">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم <span class="red">*</span></label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <label>الوصف</label>
                                                <textarea type="text" class="form-control" name="description"
                                                          rows="4"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                            class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3"><b>الإسم</b></div>
                                            <div class="col-sm-7"><b>الوصف</b></div>
                                            <div class="col-sm-2"><b>الحذف</b></div>
                                        </div>

                                        <form action="{{route("company.hrm.update_designation", $company->slug)}}"
                                              method="post" class="row form11" style="margin-bottom: 15px">
                                            @csrf

                                            @foreach($designations as $designation)

                                                    <div class="col-sm-3 mb-10">
                                                        <input type="text" class="form-control" name="name_{{$designation->id}}"
                                                               value="{{$designation->name}}" required>
                                                    </div>
                                                    <div class="col-sm-7 mb-10">
                                                <textarea type="text" class="form-control" name="description_{{$designation->id}}"
                                                          rows="4">{{$designation->description}}</textarea>
                                                    </div>
                                                    <div class="col-sm-2 mb-10">
                                                        <a href="{{route("company.hrm.delete_designation", [$company->slug, $designation->id])}}"
                                                           class="btn btn-danger btn-block btn-delete1">حذف</a>
                                                    </div>

                                            @endforeach

                                            <div class="col-sm-12">

                                                <div class="form-group mt-20">
                                                    <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                                class="fa fa-save"></i> <span>إضافة</span></button>
                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أصناف الإجازات</strong></div>
                            <div class="panel-body">

                                <div id="div22">
                                    <div id="div2">

                                        <form action="{{route("company.hrm.save_vacation_category", $company->slug)}}" class="form2" method="post">
                                            @csrf

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>الإسم <span class="red">*</span></label>
                                                        <input type="text" class="form-control" name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>المدة المسموح بها (باليوم) <span class="red">*</span></label>
                                                        <input type="number" value="1" min="1" class="form-control"
                                                               name="duration" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>مدفوعة الأجر <span class="red">*</span></label>
                                                        <select class="form-control" name="payed" id="payed" required>
                                                            <option value="1" selected>نعم</option>
                                                            <option value="0">لا</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group discout">
                                                        <label>نسبة % الخصم من الراتب (باليوم) <span
                                                                    class="red">*</span></label>
                                                        <input type="number" value="0" min="0" class="form-control"
                                                               name="discout" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                                    class="fa fa-save"></i> <span>إضافة</span></button>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3"><b>الإسم</b></div>
                                            <div class="col-sm-2"><b>المدة</b></div>
                                            <div class="col-sm-2"><b>مدفوعة</b></div>
                                            <div class="col-sm-2"><b>الخصم</b></div>
                                            <div class="col-sm-3"><b>الأوامر</b></div>
                                        </div>

                                        @foreach($vcategories as $vcategory)
                                            <form action="{{route("company.hrm.update_vacation_category", $company->slug)}}"
                                                  method="post" class="row form22" style="margin-bottom: 15px">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$vcategory->id}}">
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="name"
                                                           value="{{$vcategory->name}}" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="number" min="1" class="form-control"
                                                           value="{{$vcategory->duration}}" name="duration" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <select class="form-control payed" name="payed" required>
                                                        <option value="1" @if($vcategory->payed==1) selected @endif>نعم</option>
                                                        <option value="0" @if($vcategory->payed==0) selected @endif>لا</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="number" value="{{$vcategory->discout}}"
                                                           @if($vcategory->payed==1) readonly @endif min="0"
                                                           class="form-control discoutt" name="discout" required>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button type="submit" class="btn btn-primary btn-block">تعديل</button>
                                                    <a href="{{route("company.hrm.delete_vacation_category", [$company->slug, $vcategory->id])}}"
                                                       class="btn btn-danger btn-block btn-delete2">حذف</a>
                                                </div>
                                            </form>
                                        @endforeach

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أصناف المخالفات</strong></div>
                            <div class="panel-body">

                                <div id="div33">
                                    <div id="div3">

                                        <form action="{{route("company.hrm.save_violation", $company->slug)}}" class="form3" method="post">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم <span class="red">*</span></label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <label>العقوبة <span class="red">*</span></label>
                                                <textarea type="text" class="form-control" name="punishment" rows="4"
                                                          required></textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                            class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3"><b>الإسم</b></div>
                                            <div class="col-sm-7"><b>العقوبة</b></div>
                                            <div class="col-sm-2"><b>الأوامر</b></div>
                                        </div>

                                        <form action="{{route("company.hrm.update_violation", $company->slug)}}" method="post" class="row form33" style="margin-bottom: 15px">
                                            @csrf

                                            @foreach($violations as $violation)

                                                    <div class="col-sm-3 mb-15">
                                                        <input type="text" class="form-control" name="name_{{$violation->id}}"
                                                               value="{{$violation->name}}" required>
                                                    </div>
                                                    <div class="col-sm-7 mb-15">
                                                <textarea type="text" class="form-control" name="punishment_{{$violation->id}}" rows="4"
                                                          required>{{$violation->punishment}}</textarea>
                                                    </div>
                                                    <div class="col-sm-2 mb-15">
                                                        <a href="{{route("company.hrm.delete_violation", [$company->slug, $violation->id])}}"
                                                           class="btn btn-danger btn-block btn-delete3">حذف</a>
                                                    </div>

                                            @endforeach

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                            <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                        class="fa fa-save"></i> <span>إضافة</span></button>
                                        </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أماكن العمل</strong></div>
                            <div class="panel-body">

                                <div id="div44">
                                    <div id="div4">

                                        <form action="{{route("company.hrm.save_location", $company->slug)}}" method="post" class="form4">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم <span class="red">*</span></label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                            class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-10"><b>الإسم</b></div>
                                            <div class="col-sm-2"><b>الأوامر</b></div>
                                        </div>

                                        <form action="{{route("company.hrm.update_location", $company->slug)}}"
                                              method="post" class="row form44" style="margin-bottom: 15px">
                                            @csrf

                                            @foreach($locations as $location)

                                                <div class="col-sm-10 mb-15">
                                                    <input type="text" class="form-control" name="name_{{$location->id}}"
                                                           value="{{$location->name}}" required>
                                                </div>
                                                <div class="col-sm-2 mb-15">
                                                    <a href="{{route("company.hrm.delete_location", [$company->slug, $location->id])}}"
                                                       class="btn btn-danger btn-delete4">حذف</a>
                                                </div>

                                            @endforeach

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                                class="fa fa-save"></i> <span>إضافة</span></button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>بنود التقييم</strong></div>
                            <div class="panel-body">

                                <div id="div55">
                                    <div id="div5">

                                        <form action="{{route("company.hrm.save_itemh", $company->slug)}}" method="post" class="form5">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم <span class="red">*</span></label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <label>المسمى الوظيفي <span class="red">*</span></label>
                                                <select class="form-control" name="designation_id" required>
                                                    <option value="0">الكل</option>
                                                    @foreach($designations as $d)
                                                        <option value="{{$d->id}}">{{$d->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>الوصف</label>
                                                <textarea class="form-control" name="description" rows="3"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                            class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3"><b>الإسم</b></div>
                                            <div class="col-sm-3"><b>المسمى الوظيفي</b></div>
                                            <div class="col-sm-3"><b>الوصف</b></div>
                                            <div class="col-sm-3"><b>الأوامر</b></div>
                                        </div>

                                        @foreach($items as $item)
                                            <form action="{{route("company.hrm.update_itemh", $company->slug)}}" method="post"
                                                  class="row form55" style="margin-bottom: 15px">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$item->id}}">
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="name" value="{{$item->name}}"
                                                           required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <select class="form-control" name="designation_id" required>
                                                        <option value="0">الكل</option>
                                                        @foreach($designations as $d)
                                                            <option value="{{$d->id}}"
                                                                    @if($item->designation_id==$d->id) selected @endif>{{$d->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-5">
                                                    <textarea class="form-control" name="description"
                                                              rows="3">{{$item->description}}</textarea>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="submit" class="btn btn-primary btn-block">تعديل</button>
                                                    <a href="{{route("company.hrm.delete_itemh", [$company->slug, $item->id])}}"
                                                       class="btn btn-danger btn-block btn-delete5" style="margin-top: 3px">حذف</a>
                                                </div>
                                            </form>
                                        @endforeach

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>الوثائق الرسمية</strong></div>
                            <div class="panel-body">

                                <div id="div66">
                                    <div id="div6">

                                    <form action="{{route("company.hrm.save_document", $company->slug)}}" method="post" class="form6"
                                          enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label>الإسم <span class="red">*</span></label>
                                            <input type="text" class="form-control" name="name" required>
                                        </div>

                                        <div class="form-group">
                                            <label>الملف <span class="red">*</span></label>
                                            <input type="file" class="form-control" name="file" required>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                        class="fa fa-save"></i> <span>إضافة</span></button>
                                        </div>
                                        <hr>

                                    </form>

                                    <div class="row" style="margin-bottom: 15px">
                                        <div class="col-sm-6"><b>الإسم</b></div>
                                        <div class="col-sm-6"><b>الأوامر</b></div>
                                    </div>

                                    @foreach($documents as $document)
                                        <form action="{{route("company.hrm.update_document", $company->slug)}}"
                                              method="post" class="row form66" style="margin-bottom: 15px">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$document->id}}">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" name="name"
                                                       value="{{$document->name}}" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="{{route("company.hrm.download_document", [$company->slug, $document->id])}}"
                                                   class="btn btn-success">تنزيل الملف</a>
                                                <button type="submit" class="btn btn-primary">تعديل</button>
                                                <a href="{{route("company.hrm.delete_document", [$company->slug, $document->id])}}"
                                                   class="btn btn-danger btn-delete6">حذف</a>
                                            </div>
                                        </form>
                                    @endforeach

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>الإدارات و الأقسام</strong></div>
                            <div class="panel-body">

                                <div id="div77">
                                    <div id="div7">

                                        <div class="row">

                                            <div class="col-sm-4">
                                                <h4>الإدارات</h4>

                                                <form action="{{route("company.hrm.save_department", $company->slug)}}" method="post" class="form7">
                                                    @csrf

                                                    <div class="form-group">
                                                        <label>الإسم <span class="red">*</span></label>
                                                        <input type="text" class="form-control" name="name" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>المدير <span class="red">*</span></label>
                                                        <select class="form-control" name="user_id" required>
                                                            <option value="">إختر المدير</option>
                                                            @foreach($users as $user)
                                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                                    class="fa fa-save"></i> <span>إضافة</span></button>
                                                    </div>
                                                    <hr>
                                                </form>

                                                <div class="row" style="margin-bottom: 15px">
                                                    <div class="col-sm-4"><b>الإسم</b></div>
                                                    <div class="col-sm-4"><b>المدير</b></div>
                                                    <div class="col-sm-4"><b>الأوامر</b></div>
                                                </div>

                                                @foreach($departments as $department)
                                                    <form action="{{route("company.hrm.update_department", $company->slug)}}"
                                                          method="post" class="row form77" style="margin-bottom: 15px">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$department->id}}">
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" name="name"
                                                                   value="{{$department->name}}" required>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <select class="form-control" name="user_id" required>
                                                                <option value="">إختر المدير</option>
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}"
                                                                            @if($department->user_id==$user->id) selected @endif>{{$user->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                                    title="تعديل"><i class="fa fa-edit"></i></button>
                                                            <a href="{{route("company.hrm.delete_department", [$company->slug, $department->id])}}"
                                                               class="btn btn-danger btn-delete7" data-toggle="tooltip" title="حذف"><i
                                                                        class="fa fa-remove"></i></a>
                                                        </div>
                                                    </form>
                                                @endforeach
                                            </div>

                                            <div class="col-sm-4">
                                                <h4>الأقسام</h4>

                                                <form action="{{route("company.hrm.save_section", $company->slug)}}"
                                                      method="post" class="form72" enctype="multipart/form-data">
                                                    @csrf

                                                    <div class="form-group">
                                                        <label>الإسم <span class="red">*</span></label>
                                                        <input type="text" class="form-control" name="name" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>الإدارة <span class="red">*</span></label>
                                                        <select class="form-control" name="department_id" required>
                                                            <option value="">إختر الإدارة</option>
                                                            @foreach($departments as $department)
                                                                <option value="{{$department->id}}">{{$department->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary" style="width: 120px"><i
                                                                    class="fa fa-save"></i> <span>إضافة</span></button>
                                                    </div>
                                                    <hr>
                                                </form>

                                                <div class="row" style="margin-bottom: 15px">
                                                    <div class="col-sm-4"><b>الإسم</b></div>
                                                    <div class="col-sm-4"><b>الإدارة</b></div>
                                                    <div class="col-sm-4"><b>الأوامر</b></div>
                                                </div>

                                                @foreach($sections as $section)
                                                    <form action="{{route("company.hrm.update_section", $company->slug)}}"
                                                          method="post" class="row form772" style="margin-bottom: 15px">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$section->id}}">
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" name="name"
                                                                   value="{{$section->name}}" required>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <select class="form-control" name="department_id" required>
                                                                <option value="">إختر الإدارة</option>
                                                                @foreach($departments as $department)
                                                                    <option value="{{$department->id}}"
                                                                            @if($section->department_id==$department->id) selected @endif>{{$department->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button type="submit" class="btn btn-primary" data-toggle="tooltip"
                                                                    title="تعديل"><i class="fa fa-edit"></i></button>
                                                            <a href="{{route("company.hrm.delete_section", [$company->slug, $section->id])}}"
                                                               class="btn btn-danger btn-delete72" data-toggle="tooltip" title="حذف"><i
                                                                        class="fa fa-remove"></i></a>
                                                        </div>
                                                    </form>
                                                @endforeach
                                            </div>

                                            <div class="col-sm-4">
                                                <h4>الهيكل التنظيمي</h4>
                                                <div id="chart-container"></div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>الإشعارات</strong></div>
                            <div class="panel-body">

                                <div id="div88">
                                    <div id="div8">

                                        {{--
                                <form action="{{route("company.hrm.savenotif", $company->slug)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="company_id" value="{{$company->id}}">

                                    <input type="text" name="name" placeholder="name" required class="form-control">
                                    <br>

                                    <input type="text" name="title" placeholder="title" required class="form-control">
                                    <br>

                                    <div class="checkbox">
                                        <label><input type="checkbox" name="app" value="1" checked required>على البرنامج</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="email" value="1" checked required>عبر ال Eamil</label>
                                    </div>
                                    <div class="checkbox disabled">
                                        <label><input type="checkbox" name="sms" value="1" checked required>عبر ال Sms</label>
                                    </div>
                                    <br>

                                    <textarea name="message" placeholder="message" required class="form-control"></textarea>
                                    <br>

                                    <button type="submit">أضف</button>
                                    <br><br><br>

                                </form>
                                --}}

                                        <br>
                                        <div class="row mb-20">
                                            <div class="col-sm-2">نوع الإشعار</div>
                                            <div class="col-sm-2">على البرنامج</div>
                                            <div class="col-sm-2">عبر البريد الإلكتروني</div>
                                            <div class="col-sm-2">عبر الرسائل القصيرة</div>
                                            <div class="col-sm-4">الرسالة</div>
                                        </div>

                                        <form action="{{route("company.hrm.savenotif", $company->slug)}}" method="post" class="mb-10 form8">
                                            @csrf

                                            @foreach($notifs as $notif)

                                                <div class="row mb-10">

                                                    <div class="col-sm-2">{{$notif->title}}{{--<br>{{$notif->name}}--}}</div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="app_{{$notif->id}}" value="1"
                                                                          @if($notif->app==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="email_{{$notif->id}}" value="1"
                                                                          @if($notif->email==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="sms_{{$notif->id}}" value="1"
                                                                          @if($notif->sms==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="text-justify col-sm-4">
                                            <textarea name="message_{{$notif->id}}" class="form-control"
                                                      maxlength="250" >{{$notif->message}}</textarea>
                                                    </div>

                                                </div>

                                            @endforeach

                                            <center>
                                                <button type="submit" class="btn btn-primary mt-20 mb-30" style="width: 120px"><i
                                                            class="fa fa-save"></i> <span>حفظ</span></button>
                                            </center>

                                        </form>

                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <style>
        .panel-heading {
            cursor: pointer;
        }

        .panel-body {
            /*display: none;*/
        }

        .discout {
            display: none;
        }

    </style>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/css/jquery.orgchart.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/js/jquery.orgchart.min.js"></script>

    <style>
        #chart-container {
            position: relative;
            display: inline-block;
            top: 10px;
            left: 10px;
            height: 382px;
            width: 100%;
            border: 2px solid #2c699e;
            background: #337ab7;
            border-radius: 3px;
            overflow: auto;
            text-align: center;
        }

        .orgchart {
            background-image: none !important;
            background: #337ab7;
            direction: ltr !important;
            display: block !important;
            margin: 0px auto !important;
            /*transform: translateX(-50%);*/
            cursor: move;
            min-height: 100%;
        }

        .orgchart .lines .leftLine, .orgchart .lines .rightLine, .orgchart .lines .topLine {
            border-color: #aaa;
        }

        .orgchart .lines .downLine {
            background-color: #aaa;
        }

        .orgchart table {
            cursor: move;
        }

        .node .content {
            height: inherit !important;
            min-height: inherit !important;
            padding: 2px;
        }

        .node i.fa.fa-info-circle.chartform-icon {
            position: absolute;
            top: 0px;
            right: 0px;
            color: #2C3B41;
            box-shadow: 0px 0px 2px 1px #000;
            border-radius: 50%;
            padding: 1px;
            background: #fff;
            display: none;
            cursor: pointer;
        }

        .node:hover i.fa.fa-info-circle.chartform-icon {
            display: block;
        }

        .orgchart .node .title {
            background-color: #2C3B41 !important;
            direction: rtl;
        }

        .orgchart .node .content {
            border: 1px solid #222D32;
            background-color: #fff;
            color: #333;
        }

        .orgchart .dep .title {
            background-color: #cc0066 !important;
        }

        .orgchart .dep .content {
            border: 1px solid #cc0066;
        }

        .orgchart .sec .title {
            background-color: #009933 !important;
        }

        .orgchart .sec .content {
            border: 1px solid #009933;
        }
    </style>
    <script>
        $(function () {

            function jsonIt(val) {
                val = val.replace(/\\n/g, "\\n")
                    .replace(/\\'/g, "\\'")
                    .replace(/\\"/g, '\\"')
                    .replace(/\\&/g, "\\&")
                    .replace(/\\r/g, "\\r")
                    .replace(/\\t/g, "\\t")
                    .replace(/\\b/g, "\\b")
                    .replace(/\\f/g, "\\f");
                val = val.replace(/[\u0000-\u0019]+/g,"");

                val = JSON.parse(val);
                return val;
            }

            function chartIt(val){
                var datascource = jsonIt(val);
                $('#chart-container').orgchart({
                    'data': datascource,
                    'parentNodeSymbol': 'fa-th-large',
                    'toggleSiblingsResp': true,
                    'nodeContent': 'title',
                    'pan': true,
                    'zoom': true,
                });
            }

            chartIt('{!! $data !!}');

            $(document).on("submit", ".form1", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div11").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_designation", $company->slug)}}', $(form).serialize(), function () {
                    $("#div11").load(window.location + " #div1", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("click", ".btn-delete1", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div11").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div11").load(window.location + " #div1", function () {
                            goNotif("success", "تم حفظ البيانات !");

                        });
                    });
                }
            });

            $(document).on("submit", ".form11", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div11").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_designation", $company->slug)}}', $(form).serialize(), function () {
                    $("#div11").load(window.location + " #div1", function () {
                        goNotif("success", "تم حفظ البيانات !");

                    });
                });
            });

            $(document).on("submit", ".form2", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div22").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_vacation_category", $company->slug)}}', $(form).serialize(), function () {
                    $("#div22").load(window.location + " #div2", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("click", ".btn-delete2", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div22").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div22").load(window.location + " #div2", function () {
                            goNotif("success", "تم حفظ البيانات !");

                        });
                    });
                }
            });

            $(document).on("submit", ".form22", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div22").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_vacation_category", $company->slug)}}', $(form).serialize(), function () {
                    $("#div22").load(window.location + " #div2", function () {
                        goNotif("success", "تم حفظ البيانات !");

                    });
                });
            });

            $(document).on("submit", ".form3", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div33").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_violation", $company->slug)}}', $(form).serialize(), function () {
                    $("#div33").load(window.location + " #div3", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("click", ".btn-delete3", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div33").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div33").load(window.location + " #div3", function () {
                            goNotif("success", "تم حفظ البيانات !");

                        });
                    });
                }
            });

            $(document).on("submit", ".form33", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div33").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_violation", $company->slug)}}', $(form).serialize(), function () {
                    $("#div33").load(window.location + " #div3", function () {
                        goNotif("success", "تم حفظ البيانات !");

                    });
                });
            });

            $(document).on("submit", ".form4", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div44").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_location", $company->slug)}}', $(form).serialize(), function () {
                    $("#div44").load(window.location + " #div4", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("click", ".btn-delete4", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div44").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div44").load(window.location + " #div4", function () {
                            goNotif("success", "تم حفظ البيانات !");

                        });
                    });
                }
            });

            $(document).on("submit", ".form44", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div44").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_location", $company->slug)}}', $(form).serialize(), function () {
                    $("#div44").load(window.location + " #div4", function () {
                        goNotif("success", "تم حفظ البيانات !");

                    });
                });
            });

            $(document).on("submit", ".form5", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div55").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_itemh", $company->slug)}}', $(form).serialize(), function () {
                    $("#div55").load(window.location + " #div5", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("click", ".btn-delete5", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div55").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div55").load(window.location + " #div5", function () {
                            goNotif("success", "تم حفظ البيانات !");

                        });
                    });
                }
            });

            $(document).on("submit", ".form55", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div55").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_itemh", $company->slug)}}', $(form).serialize(), function () {
                    $("#div55").load(window.location + " #div5", function () {
                        goNotif("success", "تم حفظ البيانات !");

                    });
                });
            });

            $(document).on("click", ".btn-delete6", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div66").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div66").load(window.location + " #div6", function () {
                            goNotif("success", "تم حفظ البيانات !");

                        });
                    });
                }
            });

            $(document).on("submit", ".form66", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div66").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_document", $company->slug)}}', $(form).serialize(), function () {
                    $("#div66").load(window.location + " #div6", function () {
                        goNotif("success", "تم حفظ البيانات !");

                    });
                });
            });

            $(document).on("submit", ".form7", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div77").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_department", $company->slug)}}', $(form).serialize(), function (data) {
                    $("#div77").load(window.location + " #div7", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        console.log(data);
                        chartIt(data);
                    });
                });
            });

            $(document).on("click", ".btn-delete7", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div77").html($("#loading-wrapper").html());
                    $.get(href, function (data) {
                        $("#div77").load(window.location + " #div7", function () {
                            goNotif("success", "تم حفظ البيانات !");
                            console.log(data);
                            chartIt(data);
                        });
                    });
                }
            });

            $(document).on("submit", ".form77", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div77").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_department", $company->slug)}}', $(form).serialize(), function (data) {
                    $("#div77").load(window.location + " #div7", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        console.log(data);
                        chartIt(data);
                    });
                });
            });

            $(document).on("submit", ".form72", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div77").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_section", $company->slug)}}', $(form).serialize(), function (data) {
                    $("#div77").load(window.location + " #div7", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        console.log(data);
                        chartIt(data);
                    });
                });
            });

            $(document).on("click", ".btn-delete72", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div77").html($("#loading-wrapper").html());
                    $.get(href, function (data) {
                        $("#div77").load(window.location + " #div7", function () {
                            goNotif("success", "تم حفظ البيانات !");
                            console.log(data);
                            chartIt(data);
                        });
                    });
                }
            });

            $(document).on("submit", ".form772", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div77").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.update_section", $company->slug)}}', $(form).serialize(), function (data) {
                    $("#div77").load(window.location + " #div7", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        console.log(data);
                        chartIt(data);
                    });
                });
            });

            $(document).on("submit", ".form8", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div88").html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.savenotif", $company->slug)}}', $(form).serialize(), function (data) {
                    $("#div88").load(window.location + " #div8", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });
            
            $(document).on("click", ".panel-heading", function (e) {
                e.preventDefault();
                $(this).parent().find(".panel-body").slideToggle();
            });

            $(document).on("change", "#payed", function (e) {
                var val = $(this).val();
                if (val == 0)
                    $(".discout").slideDown();
                else
                    $(".discout").slideUp();
            });

            $(document).on("change", ".payed", function (e) {
                var val = $(this).val();
                if (val == 0)
                    $(this).closest("form").find(".discoutt").prop("readonly", false);
                else
                    $(this).closest("form").find(".discoutt").prop("readonly", true);
            });

        });
    </script>

    <style>

        div.bhoechie-tab-container {
            z-index: 10;
            background-color: #ffffff;
            padding: 0 !important;
            border: 1px solid #ddd;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            -moz-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            background-clip: padding-box;
            opacity: 0.97;
            filter: alpha(opacity=97);
        }

        div.bhoechie-tab-menu {
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group {
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group > a {
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group > a .glyphicon,
        div.bhoechie-tab-menu div.list-group > a .fa {
            color: #5A55A3;
        }

        div.bhoechie-tab-menu div.list-group > a:first-child {
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group > a:last-child {
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group > a.active,
        div.bhoechie-tab-menu div.list-group > a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group > a.active .fa {
            background-color: #337ab7;
            border-radius: 0px;
            color: #ffffff;
        }

        div.bhoechie-tab-menu div.list-group > a.active:after {
            content: '';
            position: absolute;
            right: 100%;
            top: 50%;
            margin-top: -13px;
            border-left: 0;
            border-bottom: 13px solid transparent;
            border-top: 13px solid transparent;
            border-right: 10px solid #337ab7;
        }

        div.bhoechie-tab-content {
            background-color: #ffffff;
            /* border: 1px solid #eeeeee; */
            padding-left: 20px;
            padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active) {
            display: none;
        }

        .bhoechie-tab {
            padding-top: 10px;
            padding-bottom: 120px;
            min-height: 400px;
            border-right: 1px solid #ddd;
        }

        .icheckbox_square-blue {
            margin-left: 10px;
        }

    </style>

    <script>
        $(document).ready(function () {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>


@endsection