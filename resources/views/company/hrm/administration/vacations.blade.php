@extends("company.layouts.app", ["title"=>"إدارة الإجازات"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>إدارة الإجازات</strong>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#vacations_filter').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                <form id="vacations_filter" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datee date1" autocomplete="off" name="from" placeholder="من تاريخ">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datee date2" autocomplete="off" name="to" placeholder="إلى تاريخ">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x userss" autocomplete="off">
                                            <option value="0">جميع الموظفين</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x status" autocomplete="off">
                                            <option value="all">جميع الحالات</option>
                                            <option value="0">قيد الإنتظار</option>
                                            <option value="1">موافق</option>
                                            <option value="2">مرفوض</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block">فلتر</button>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="reset" class="btn btn-info btn-block">مسح</button>
                                    </div>

                                    <div class="clearfix"></div>

                                </form>
                                <div class="clearfix"></div>

                                <div class="clearfix"><hr></div>

                                <div id="menu5">
                                    <div id="menu5-content">

                                        <table class="table table-bordered table-hover grd_view_tasks">
                                            <thead>
                                            <tr>
                                                <th class="text-center">م</th>
                                                <th>الموظف</th>
                                                <th>من تاريخ</th>
                                                <th>إلى تاريخ</th>
                                                <th>نوع الإجازة</th>
                                                <th width="35%">التفاصيل</th>
                                                <th>الملف المرفق</th>
                                                <th>حالة الإجازة</th>
                                                @if(permissions_hrm("accept_vacationsss"))
                                                <th>الأوامر</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($vacations as $vacation)
                                                <tr class="filter" data-date0="{{$vacation->from_date}}" data-date00="{{$vacation->to_date}}" data-user="{{$vacation->user_id}}" data-status="{{$vacation->status}}">
                                                    <td width="35px" class="text-center">{{$loop->iteration}}</td>
                                                    <td>
                                                        <a href="{{route("company.hrm.employee", [$company->slug, @$vacation->user->id])}}">
                                                            @if(@$vacation->user->image)
                                                                <img src="{{asset("storage/app/".@$vacation->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                            @else
                                                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                            @endif
                                                            <span style="display: inline-block">{{@$vacation->user->name}}</span>
                                                        </a>
                                                    </td>
                                                    <td>{{$vacation->from_date}}</td>
                                                    <td>{{$vacation->to_date}}</td>
                                                    <td>{{@$vacation->category->name}}</td>
                                                    <td>{{$vacation->details}}</td>
                                                    <td class="text-center">
                                                        @if($vacation->file)
                                                            <a href="{{asset("storage/app/".$vacation->file)}}" data-toggle="tooltip" title="تحميل الملف" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                                        @else
                                                            <span>لا يوجد</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if($vacation->status==0)
                                                            <span class="label label-warning block">قيد الإنتظار</span>
                                                        @elseif($vacation->status==1)
                                                            <span class="label label-success block">موافق</span>
                                                        @else
                                                            <span class="label label-danger block">مرفوض</span>
                                                        @endif
                                                    </td>
                                                    @if(permissions_hrm("accept_vacationsss"))
                                                    <td class="text-center">
                                                        @if($vacation->status==0)
                                                            <a href="#" data-id="{{$vacation->id}}" class="btn btn-info btn-xs btn-accept" data-toggle="tooltip" title="موافقة"><i class="fa fa-thumbs-up"></i></a>
                                                            <a href="#" data-id="{{$vacation->id}}" class="btn btn-warning btn-xs btn-reject" data-toggle="tooltip" title="رفض"><i class="fa fa-thumbs-down"></i></a>
                                                        @else
                                                            <a href="#" class="btn btn-info btn-xs disabled" disabled data-toggle="tooltip" title="موافقة"><i class="fa fa-thumbs-up"></i></a>
                                                            <a href="#" class="btn btn-warning btn-xs disabled" disabled data-toggle="tooltip" title="رفض"><i class="fa fa-thumbs-down"></i></a>
                                                        @endif
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>


    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <script>
        $(function () {

            function reloadish(){
                $('.datee').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true
                });
            }
            reloadish();

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("click", "#vacation_category_id", function (e) {
                e.preventDefault();
                if($(this).find("option:selected").val()){
                    var duration    = $(this).closest("form").find("option:selected").data("duration");
                    var payed       = $(this).closest("form").find("option:selected").data("payed");
                    var discout     = $(this).closest("form").find("option:selected").data("discout");
                    $(this).closest("form").find("#vacation_duration").slideDown();
                    $(this).closest("form").find("#vacation_duration .variable").html(duration+ " أيام");
                    $(this).closest("form").find("#vacation_payed").slideDown();
                    if(payed==1){
                        $(this).closest("form").find("#vacation_payed .variable").html("نعم");
                        $(this).closest("form").find("#vacation_discout").slideUp();
                        $(this).closest("form").find("#vacation_discout .variable").html("");
                    }else{
                        $(this).closest("form").find("#vacation_payed .variable").html("لا");
                        $(this).closest("form").find("#vacation_discout").slideDown();
                        $(this).closest("form").find("#vacation_discout .variable").html("%"+discout);
                    }
                }else{
                    $(this).closest("form").find("#vacation_duration").slideUp();
                    $(this).closest("form").find("#vacation_payed").slideUp();
                    $(this).closest("form").find("#vacation_discout").slideUp();
                    $(this).closest("form").find("#vacation_duration .variable").html("");
                    $(this).closest("form").find("#vacation_payed .variable").html("");
                    $(this).closest("form").find("#vacation_discout .variable").html("");
                }
            });

            // Vacation

            $(document).on("submit", "#form_add_vacation", function (e) {
                e.preventDefault();
                $("#menu5").html($("#loading-wrapper").html());
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{route("company.hrm.add_vacation", $company->slug)}}',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result.error)
                            goNotif("error", result.error);

                    }, error: function (data) {
                        goNotif("error", data);
                    }
                }).always(function () {
                    $("#menu5").load(window.location + " #menu5-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-vacation", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu5").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_vacation", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu5").load(window.location + " #menu5-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_vacation", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu5").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_vacation", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu5").load(window.location + " #menu5-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-accept", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu5").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.accept_vacation", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu5").load(window.location + " #menu5-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("click", ".btn-reject", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu5").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.reject_vacation", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu5").load(window.location + " #menu5-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            // Filters


            $(document).on("submit", "#vacations_filter", function (e){
                var date1   = $("#vacations_filter .date1").val();
                var date2   = $("#vacations_filter .date2").val();
                var userss = $("#vacations_filter .userss").select2('val')+"";
                var status = $("#vacations_filter .status").select2('val')+"";

                $('.grd_view_tasks tbody tr.filter').hide();
                $('.grd_view_tasks tbody tr.filter').filter(function () {
                    var datex = true;
                    var date0 = $(this).data("date0");
                    var date00 = $(this).data("date00");
                    if(date0 && date00){
                        if(date1 && !date2) {
                            if(new Date(date1) > new Date(date00))
                                datex = false;
                        }else if(!date1 && date2){
                            if(new Date(date0) > new Date(date2))
                                datex = false;
                        }else if(date1 && date2) {
                            if(new Date(date1)>new Date(date0) || new Date(date2)<new Date(date00))
                                datex = false;
                        }
                    }

                    var userx = true;
                    var user0 = $(this).data("user").toString();
                    if(userss){
                        if(userss!="0"){
                            if(user0!=userss)
                                userx=false;
                        }
                    }

                    var statusx = true;
                    var status0 = $(this).data("status").toString();
                    if(status){
                        if(status!="all")
                            if(status!=status0)
                                statusx = false;
                    }

                    return  (datex && userx && statusx);

                }).show();
            });

            $(document).on("reset", "#vacations_filter", function (e){
                $('.grd_view_tasks tbody tr.filter').show();
                $("#vacations_filter .date1").val("");
                $("#vacations_filter .date2").val("");
                $('#vacations_filter .userss').val(0).trigger('change');
                $('#vacations_filter .status').val("all").trigger('change');
            });



        });
    </script>

@endsection