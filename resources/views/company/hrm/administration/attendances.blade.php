@extends("company.layouts.app", ["title"=>"إدارة الحضور و الإنصراف"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>إدارة الحضور و الإنصراف</strong>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">
                                <form action="#" method="post" id="filter_attendance" class="row">
                                    @csrf
                                    <div class="col-sm-3">
                                        <label>من تاريخ</label>
                                        <input  type="text" class="form-control datee" name="from" autocomplete="off">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>إلى تاريخ</label>
                                        <input  type="text" class="form-control datee" name="to" autocomplete="off">
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>إختر الموظف</label>
                                            <select name="user_id[]" class="form-control select2x" multiple>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label style="opacity: 0;">عرض</label>
                                        <button type="submit" class="btn btn-primary btn-block">عرض</button>
                                    </div>
                                </form>
                                <div id="html"></div>
                                <hr style="margin-bottom: 35px">
                            </div>

                            <div class="col-sm-12">
                                <table class="table table-bordered"  id="tableAttendances">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>التاريخ</th>
                                        <th>الموضف</th>
                                        <th width="150px">حالة الحضور</th>
                                        <th>توقيت الدخول</th>
                                        <th>توقيت الخروج</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            @include("company.hrm.administration.attendance_row", ["user"=> $user, "today"=>$today])
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="green" style="display: inline-block; height: 20px;"></div> <strong>حضور</strong>
                                <div class="gold" style="display: inline-block; height: 20px;"></div> <strong>إجازة</strong>
                                <div class="blue" style="display: inline-block; height: 20px;"></div> <strong>تدريب</strong>
                                <div class="purple" style="display: inline-block; height: 20px;"></div> <strong>إنتداب</strong>
                                <div class="red" style="display: inline-block; height: 20px;"></div> <strong>غياب</strong>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .green{
            background: green;
            width: 20px;
            height: 25px;
        }
        .red{
            background: red;
            width: 20px;
            height: 25px;
        }
        .gold{
            background: gold;
            width: 20px;
            height: 25px;
        }
        .blue{
            background: #346bff;
            width: 20px;
            height: 25px;
        }
        .purple{
            background: #ba00ba;
            width: 20px;
            height: 25px;
        }
        .block{
            text-align: center;
            display: block;
            width: 100%;
            line-height: 25px;
        }
        .block:not(.gold){
            color: #fff;
        }
    </style>

    <script>
        $(function () {

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on("submit", "#filter_attendance", function (e) {
                e.preventDefault();
                $("#tableAttendances tbody").html("");
                var data = $(this).serialize()
                $.post("{{route("company.hrm.filter_attendance", $company->slug)}}", data, function (html) {
                    $("#tableAttendances tbody").html(html);
                });
                return;
            });

        });
    </script>

@endsection