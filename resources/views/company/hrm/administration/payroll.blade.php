@extends("company.layouts.app", ["title"=>"إدارة الحضور و الإنصراف"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>إدارة حساب الراتب (payroll)</strong>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">
                                <form action="#" method="post" id="filter_pay" class="row">
                                    @csrf
                                    <div class="col-sm-3">
                                        <label>الشهر</label>
                                        <?php $months = ["جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"]; ?>
                                        <select name="month" class="form-control select2x">
                                            @for($i=1; $i<=12; $i++)
                                                <option value="{{sprintf("%02d", $i)}}" @if(intval($current_month)==$i) selected @endif>{{$months[$i-1]}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>السنة</label>
                                        <select name="year" class="form-control select2x">
                                            @for($j=(intval($current_year)-10); $j<=(intval($current_year)+10); $j++)
                                                <option value="{{$j}}" @if(intval($current_year)==$j) selected @endif>{{$j}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label>إختر الموظف</label>
                                                <select name="user_id[]" class="form-control select2x" multiple>
                                                    @foreach($users as $user)
                                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label style="opacity: 0;">عرض</label>
                                        <button type="submit" class="btn btn-primary btn-block">عرض</button>
                                    </div>
                                </form>
                                <div id="html"></div>
                                <hr style="margin-bottom: 35px">
                            </div>

                            <div class="col-sm-12">
                                <table class="table table-bordered"  id="tablePay">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">الموظف</th>
                                            <th rowspan="2">المسمى الوظيفي</th>
                                            <th rowspan="2">الإدارة</th>
                                            <th rowspan="2">القسم</th>
                                            <th colspan="3">الإستحقاقات</th>
                                            <th rowspan="2">إجمالي الإستحقاقات</th>
                                            <th colspan="3">الخصم</th>
                                            <th rowspan="2">إجمالي الخصم</th>
                                            <th rowspan="2">الصافي المستحق</th>
                                        </tr>
                                        <tr>
                                            <th>الراتب الأساسي</th>
                                            <th>البدلات</th>
                                            <th>المكافآت</th>
                                            <th>الإقتطاعات</th>
                                            <th>الخصومات</th>
                                            <th>الإجازات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <?php
                                                $salary = (@$user->details->salary and @$user->details->salary!=0)?@$user->details->salary:0;
                                                $get = $set = 0;
                                            ?>
                                            @include("company.hrm.administration.payroll_row", ["user"=>$user])
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <script>
        $(function () {

            $(".sidebar-toggle").trigger("click");

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on("submit", "#filter_pay", function (e) {
                e.preventDefault();
                $("#tablePay tbody").html('<tr><td class="text-center" colspan="13">'+$("#loading-wrapper").html()+'</td></tr>');
                var data = $(this).serialize()
                $.post("{{route("company.hrm.filter_pay", $company->slug)}}", data, function (html) {
                    $("#tablePay tbody").html(html);
                });
                return;
            });

            $(document).on("click", ".add-reward-btn", function (e) {
                $(this).next(".add-reward-div").slideToggle();
            });

            $(document).on("submit", ".add_Reward", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                $("#"+id).modal('hide');
                var data = $(this).serialize();
                setTimeout(function () {
                    $.post("{{route("company.hrm.save_reward", $company->slug)}}", data, function (html) {
                        $("#filter_pay").trigger("submit");
                    });
                    return;
                }, 500);

                return;
            });

            $(document).on("click", ".remove-reward-btn", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    var idd = $(this).data("idd");
                    $("#"+id).modal('hide');
                    setTimeout(function () {
                        $.get("{{route("company.hrm.delete_reward", $company->slug)}}/"+idd, function (html) {
                            $("#filter_pay").trigger("submit");
                        });
                        return;
                    }, 500);

                    return;
                }
            });

            $(document).on("click", ".add-substraction-btn", function (e) {
                $(this).next(".add-substraction-div").slideToggle();
            });

            $(document).on("submit", ".add_Substraction", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                $("#"+id).modal('hide');
                var data = $(this).serialize();
                setTimeout(function () {
                    $.post("{{route("company.hrm.save_substraction", $company->slug)}}", data, function (html) {
                        $("#filter_pay").trigger("submit");
                    });
                    return;
                }, 500);

                return;
            });

            $(document).on("click", ".remove-substruction-btn", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    var idd = $(this).data("idd");
                    $("#"+id).modal('hide');
                    setTimeout(function () {
                        $.get("{{route("company.hrm.delete_substraction", $company->slug)}}/"+idd, function (html) {
                            $("#filter_pay").trigger("submit");
                        });
                        return;
                    }, 500);

                    return;
                }
            });



        });
    </script>

    <style>
        #tablePay{
            font-size: 0.8em;
        }
        #tablePay th{
            background: #f5f5f6;
        }
        #tablePay th, #tablePay td{
            text-align: center;
            border: 1px solid #ddd;
            vertical-align: middle;
            color: #515151;
        }
        #tablePay td.blue{
            color: royalblue;
        }
        #tablePay td.red{
            color: red;
        }
        #tablePay td.green{
            color: green;
        }
        #tablePay td{
            position: relative;
        }
        #tablePay td .btn{
            position: absolute;
            font-size: 0.9em;
            padding: 1px 4px;
            top: 0px;
            right: 0px;
            display: none;
        }
        #tablePay td:hover .btn{
            display: block;
        }
        #tablePay .black{
            color: #515151 !important;
        }
    </style>

@endsection