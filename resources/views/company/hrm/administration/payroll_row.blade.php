<tr>
    <td>
        <a href="{{route("company.hrm.employee", [$company->slug, $user->id])}}" target="_blank">{{$user->name}}</a>
    </td>
    <td style="vertical-align: middle;">
        @if(@$user->details->designation)
            {{@$user->details->designation->name}}
        @else
            <span>غير محدد</span> <span class="red">*</span>
        @endif
    </td>
    <td>
        {{(@$user->details->department)?@$user->details->department->name:"لا يتبع إدارة"}}<br>
    </td>
    <td>{{(@$user->details->section)?@$user->details->section->name:"لا يتبع قسم"}}</td>
    <td class="blue">
        <?php $get+=$salary; ?>
        {{$salary}} <span>ريال</span>
    </td>
    <td class="blue">
        <a href="#" data-toggle="modal" data-target="#showAllowance{{$user->id}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
        <div id="showAllowance{{$user->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title black">البدلات</h4>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center mt-0 mb-20 black">الموظف : {{$user->name}}</h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>رمز البدل</th>
                                <th>قيمة البدل</th>
                                <th>من تاريخ</th>
                                <th>إلى تاريخ</th>
                                <th>الحالة</th>
                                <th>المجموع</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_allowance=0; ?>
                            @foreach($user->allowances as $allowance)
                                <?php
                                $active = true;
                                $to = ($allowance->to_date)?$allowance->to_date:"9999-12-30";
                                $from=$allowance->from_date;
                                if(($from<$first and $to<$first) or ($from>$end and $to>$end))
                                    $active=false;
                                ?>
                                <tr>
                                    <td>{{@$allowance->category->name}}</td>
                                    <td>
                                        <span>{{@$allowance->category->value}}</span>
                                        @if(@$allowance->category->type==0)
                                            <span> (%)</span>
                                        @else
                                            <span> ريال</span>
                                        @endif
                                    </td>
                                    <td>{{$allowance->from_date}}</td>
                                    <td>{{$allowance->to_date}}</td>
                                    <td>
                                        @if ($active)
                                            <span class="label label-success label-block">فعال</span>
                                        @else
                                            <span class="label label-warning label-block">غير فعال</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$allowance->category->type==0)
                                            <?php $val = $salary/100*@$allowance->category->value; ?>
                                        @else
                                            <?php $val = @$allowance->category->value;?>
                                        @endif
                                        @if ($active) @else <?php $val=0; ?> @endif
                                        <?php $total_allowance+=$val; ?>
                                        <span>{{number_format($val, 2)}}</span> <span>ريال</span>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5"></td>
                                <td>
                                    <div id="total_allowance" data-val="{{$total_allowance}} "></div>
                                    <span>{{number_format($total_allowance, 2)}}</span> <span>ريال</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <?php $get+=$total_allowance; ?>
        <span>{{$total_allowance}}</span> <span>ريال</span>
    </td>
    <td class="blue">
        <a href="#" data-toggle="modal" data-target="#showRewards{{$user->id}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
        <div id="showRewards{{$user->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title black">المكافآة</h4>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center mt-0 mb-20 black">الموظف : {{$user->name}}</h5>
                        <h5 class="text-center mt-0 mb-20 black"><span>الشهر : </span><span>{{@$months[$current_month-1]." ".$current_year}}</span></h5>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>إسم المكافأة</th>
                                <th>الشهر</th>
                                <th>قيمة المكافأة</th>
                                @if(permissions_hrm("delete_reward"))
                                <th>الحذف</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_rewards = 0; ?>
                                @foreach($user->rewards as $reward)
                                    @if($reward->month==$current_month and $reward->year==$current_year)
                                        <tr>
                                            <td>{{$reward->name}}</td>
                                            <td>{{@$months[$reward->month-1]." ".$reward->year}}</td>
                                            <td>{{$reward->amount}}</td>
                                            <?php $total_rewards+=floatval($reward->amount); ?>
                                            @if(permissions_hrm("delete_reward"))
                                            <td><a class="btn btn-xs btn-danger btn-block remove-reward-btn" data-id="showRewards{{$user->id}}" data-idd="{{$reward->id}}"  style="display: block;position: inherit;"><i class="fa fa-remove"></i> <span>حذف</span></a></td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td>
                                    <span>{{$total_rewards}}</span> <span>ريال</span>
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        @if(permissions_hrm("add_reward"))
                        <a href="#" class="text-center mt-0 mb-20 blue add-reward-btn" style="display: block"><i class="fa fa-plus"></i> <span>إضافة مكافئة</span></a>

                        <form action="{{route("company.hrm.save_reward", $company->slug)}}" class="add_Reward add-reward-div col-sm-12 mb-20" style="display: none" data-id="showRewards{{$user->id}}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input type="hidden" name="month" value="{{$current_month}}">
                            <input type="hidden" name="year" value="{{$current_year}}">

                            <div class="form-group row">
                                <label class="col-sm-3 text-right black">إسم المكافئة <span class="red">*</span></label>
                                <input type="text" class="form-control col-sm-9" required name="name" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right black">قيمة المكافئة <span class="red">*</span></label>
                                <input type="number" class="form-control col-sm-9" required name="amount" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <button type="submit" class="btn btn-block btn-primary" style="font-size: 1em;display: block;padding: 7px;"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                </div>
                            </div>
                        </form>
                        @endif
                        <div class="clearfix"></div>

                    </div>
                </div>

            </div>
        </div>
        <?php $get+=$total_rewards; ?>
        <span>{{$total_rewards}}</span> <span>ريال</span>
    </td>
    <td class="blue">
        <span>{{$get}}</span> <span>ريال</span>
    </td>
    <td class="red">
        <a href="#" data-toggle="modal" data-target="#showDeduction{{$user->id}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
        <div id="showDeduction{{$user->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title black">الإقتطاعات</h4>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center mt-0 mb-20 black">الموظف : {{$user->name}}</h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>رمز الإقتطاع</th>
                                <th>قيمة الإقتطاع</th>
                                <th>القيمة النهائية</th>
                                <th>من تاريخ</th>
                                <th>إلى تاريخ</th>
                                <th>الحالة</th>
                                <th>المجموع</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_deduction=0; ?>
                            @foreach($user->deductions as $deduction)
                                <?php
                                $active = true;
                                $to = ($deduction->to_date)?$deduction->to_date:"9999-12-30";
                                $from=$deduction->from_date;
                                if(($from<$first and $to<$first) or ($from>$end and $to>$end))
                                    $active=false;
                                ?>
                                <tr>
                                    <td>{{@$deduction->category->name}}</td>
                                    <td>
                                        <span>{{@$deduction->category->value}}</span>
                                        @if(@$deduction->category->type==0)
                                            <span> (%)</span>
                                        @else
                                            <span> ريال</span>
                                        @endif
                                    </td>
                                    <td>{{$deduction->amount}}</td>
                                    <td>{{$deduction->from_date}}</td>
                                    <td>{{$deduction->to_date}}</td>
                                    <td>
                                        @if ($active)
                                            <span class="label label-success label-block">فعال</span>
                                        @else
                                            <span class="label label-warning label-block">غير فعال</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $val = intval($deduction->amount);?>
                                        @if ($active) @else <?php $val=0; ?> @endif
                                        <?php $total_deduction += $val; ?>
                                        <span>{{number_format($val, 2)}}</span> <span>ريال</span>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="6"></td>
                                <td>
                                    <div id="total_deduction" data-val="{{$total_deduction}} "></div>
                                    <span>{{number_format($total_deduction, 2)}}</span> <span>ريال</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <?php $set+=$total_deduction; ?>
        <span>{{$total_deduction}}</span> <span>ريال</span>
    </td>
    <td class="red">
        <a href="#" data-toggle="modal" data-target="#showSubtruction{{$user->id}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
        <div id="showSubtruction{{$user->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title black">الخصومات</h4>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center mt-0 mb-20 black">الموظف : {{$user->name}}</h5>
                        <h5 class="text-center mt-0 mb-20 black"><span>الشهر : </span><span>{{@$months[$current_month-1]." ".$current_year}}</span></h5>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>إسم الخصم</th>
                                <th>الشهر</th>
                                <th>قيمة الخصم</th>
                                @if(permissions_hrm("delete_substraction"))
                                <th>الحذف</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_substractions = 0; ?>
                            @foreach($user->substractions as $substraction)
                                @if($substraction->month==$current_month and $substraction->year==$current_year)
                                    <tr>
                                        <td>{{$substraction->name}}</td>
                                        <td>{{@$months[$substraction->month-1]." ".$substraction->year}}</td>
                                        <td>{{$substraction->amount}}</td>
                                        <?php $total_substractions+=floatval($substraction->amount); ?>
                                        @if(permissions_hrm("delete_substraction"))
                                        <td><a class="btn btn-xs btn-danger btn-block remove-substruction-btn" data-id="showSubtruction{{$user->id}}" data-idd="{{$substraction->id}}"  style="display: block;position: inherit;"><i class="fa fa-remove"></i> <span>حذف</span></a></td>
                                        @endif
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td>
                                    <span>{{$total_substractions}}</span> <span>ريال</span>
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        @if(permissions_hrm("add_substraction"))
                        <a href="#" class="text-center mt-0 mb-20 blue add-substraction-btn" style="display: block"><i class="fa fa-plus"></i> <span>إضافة خصم</span></a>

                        <form action="{{route("company.hrm.save_substraction", $company->slug)}}" class="add_Substraction add-substraction-div col-sm-12 mb-20" style="display: none" data-id="showSubtruction{{$user->id}}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input type="hidden" name="month" value="{{$current_month}}">
                            <input type="hidden" name="year" value="{{$current_year}}">

                            <div class="form-group row">
                                <label class="col-sm-3 text-right black">إسم الخصم <span class="red">*</span></label>
                                <input type="text" class="form-control col-sm-9" required name="name" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right black">قيمة الخصم <span class="red">*</span></label>
                                <input type="number" class="form-control col-sm-9" required name="amount" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <button type="submit" class="btn btn-block btn-primary" style="font-size: 1em;display: block;padding: 7px;"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                </div>
                            </div>
                        </form>
                        @endif
                        <div class="clearfix"></div>

                    </div>
                </div>

            </div>
        </div>
        <?php $set+=$total_substractions; ?>
        <span>{{$total_substractions}}</span> <span>ريال</span>
    </td>
    <td class="red">
        <a href="#" data-toggle="modal" data-target="#showVacation{{$user->id}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>
        <div id="showVacation{{$user->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title black">الإجازات غير مدفوعة الأجر</h4>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-center mt-0 mb-20 black">الموظف : {{$user->name}}</h5>
                        <h5 class="text-center mt-0 mb-20 black"><span>الشهر : </span><span>{{@$months[$current_month-1]." ".$current_year}}</span></h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>من تاريخ</th>
                                <th>إلى تاريخ</th>
                                <th>عدد الأيام في هذا الشهر</th>
                                <th>قيمة اليوم</th>
                                <th>نوع الإجازة</th>
                                <th width="35%">التفاصيل</th>
                                <th>الملف المرفق</th>
                                <th>المجموع</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_vacations=0; ?>

                            @foreach($user->avacations as $vacation)
                                <?php
                                $active = true;
                                $to = ($vacation->to_date)?$vacation->to_date:"9999-12-30";
                                $from=$vacation->from_date;
                                if(($from<$first and $to<$first) or ($from>$end and $to>$end) and @$vacation->category->payed==0)
                                    $active=false;
                                $period1 = \Carbon\CarbonPeriod::create($first, $end);
                                $p1 = []; foreach ($period1 as $d)array_push($p1, $d->format('Y-m-d'));
                                $period2 = \Carbon\CarbonPeriod::create($from, $to);
                                $p2 = []; foreach ($period2 as $d)array_push($p2, $d->format('Y-m-d'));
                                $days = count(array_intersect($p1, $p2));
                                ?>
                                @if($active)
                                    <tr>
                                        <td>{{$vacation->from_date}}</td>
                                        <td>{{$vacation->to_date}}</td>
                                        <td>{{$days}}</td>
                                        <td><span>{{intval($salary/count($p1))}}</span> <span>ريال</span></td>
                                        <td>{{@$vacation->category->name}}</td>
                                        <td>{{$vacation->details}}</td>
                                        <td class="text-center">
                                            @if($vacation->file)
                                                <a href="{{asset("storage/app/".$vacation->file)}}" data-toggle="tooltip" title="تحميل الملف" class="btn btn-info btn-xs" target="_blank"
                                                style="position:inherit"><i class="fa fa-download"></i></a>
                                            @else
                                                <span>لا يوجد</span>
                                            @endif
                                        </td>
                                        <td><?php $tot = ($salary==0)?0:intval($days*($salary/count($p1))); $total_vacations+=$tot;?><span>{{$tot}}</span> <span>ريال</span></td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td colspan="7"></td>
                                <td>
                                    <div id="total_deduction" data-val="{{$total_vacations}} "></div>
                                    <span>{{number_format($total_vacations, 2)}}</span> <span>ريال</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <?php $set+=$total_vacations; ?>
        <span>{{$total_vacations}}</span> <span>ريال</span>
    </td>
    <td class="red">
        <span>{{$set}}</span> <span>ريال</span>
    </td>
    <td class="green">
        <span>{{$get-$set}}</span> <span>ريال</span>
    </td>
</tr>