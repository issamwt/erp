@extends("company.layouts.app", ["title"=>"إدارة التقييمات"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>إدارة التقييمات</strong>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#evaluations_filter').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                @if(permissions_hrm("add_evaluation"))
                                <a href="javascript:void(0)" class="btn btn-primary pull-right btn-open" data-target="#addEvaluation" style="width: 105px"> إضافة تقييم </a>

                                <div class="clearfix"></div>

                                <div class="col-sm-12" style="padding: 0px">
                                    <form action="{{route("company.hrm.save_evaluation", $company->slug)}}" method="post" class="formi" id="addEvaluation">
                                        @csrf

                                        <div class="form-group">
                                            <label>الموظف</label>
                                            <select name="user_id" class="form-control select2x" id="user_id">
                                                <option value="">إختر الموظف</option>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}" data-designation="{{@$user->details->designation_id}}">{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div id="table-items"></div>

                                    </form>
                                </div>
                                @endif

                                <form id="evaluations_filter" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datee date1" autocomplete="off" name="from" placeholder="من تاريخ">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datee date2" autocomplete="off" name="to" placeholder="إلى تاريخ">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x userss" autocomplete="off">
                                            <option value="0">جميع الموظفين</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block">فلتر</button>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="reset" class="btn btn-info btn-block">مسح</button>
                                    </div>

                                    <div class="clearfix"></div>

                                </form>
                                <div class="clearfix"></div>

                                <div class="clearfix"><hr></div>

                                <div id="menu5">
                                    <div id="menu5-content">

                                        <table class="table table-bordered table-hover grd_view_tasks">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="25px">م</th>
                                                <th>الموظف</th>
                                                <th>التاريخ</th>
                                                <th width="35%">الملاحظات</th>
                                                <th>التقييمات</th>
                                                @if(permissions_hrm("delete_evaluation"))
                                                <th>الأوامر</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody class="tbody">
                                                @foreach($evaluationHrms as $ev)
                                                    <tr class="tr" data-date0="{{$ev->created_at->format("Y-m-d")}}" data-user0="{{$ev->user_id}}">
                                                        <td>{{$loop->iteration}}</td>
                                                        <td>
                                                            <a href="{{route("company.hrm.employee", [$company->slug, @$ev->user->id])}}">
                                                                @if(@$ev->user->image)
                                                                    <img src="{{asset("storage/app/".@$ev->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                                @else
                                                                    <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                                @endif
                                                                <span style="display: inline-block">{{@$ev->user->name}}</span>
                                                            </a>
                                                        </td>
                                                        <td class="text-center">{{$ev->created_at->format("Y-m-d")}}</td>
                                                        <td width="30%">{{$ev->notes}}</td>
                                                        <td width="30%">
                                                            @foreach($ev->stars as $stars)
                                                                <div class="row">
                                                                    <div class="col-sm-7">
                                                                        <a href="#" data-toggle="modal" data-target="#myModal{{@$stars["item"]->id}}">{{@$stars["item"]->name}}</a>
                                                                        <div id="myModal{{@$stars["item"]->id}}" class="modal fade" role="dialog">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                        <h4 class="modal-title">بند تقييم</h4>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <table class="table table-bordered">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th>الإسم</th>
                                                                                                <th>المسمى الوظيفي</th>
                                                                                                <th width="50%">الوصف</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>{{@$stars["item"]->name}}</td>
                                                                                                <td>
                                                                                                    @if(@$stars["item"]->designation)
                                                                                                        <span>{{@$stars["item"]->designation->name}}</span>
                                                                                                    @else
                                                                                                        <span>الكل</span>
                                                                                                    @endif
                                                                                                </td>
                                                                                                <td>{{@$stars["item"]->description}}</td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5">{{stars(@$stars["value"])}}</div>
                                                                </div>
                                                            @endforeach
                                                        </td>
                                                        @if(permissions_hrm("delete_evaluation"))
                                                        <td class="text-center">
                                                            <a data-id="{{$ev->id}}" class="btn btn-danger btn-xs btn-delete-ev" href="{{route("company.hrm.delete_ev", [$company->slug, $ev->id])}}"
                                                               data-toggle="tooltip" title="حذف التقييم"><i class="fa fa-remove"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>


    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.5/css/star-rating.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.5/js/star-rating.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.5/js/locales/ar.js"></script>

    <script>
        $(function () {

            function reloadish(){
                $('.datee').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true
                });
            }
            reloadish();

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("change", "#user_id", function () {
                var des_id  = $("#user_id option:selected").data("designation");
                var user_id = $(this).val();
                $("#table-items").html("");
                $.get("{{route("company.hrm.get_items_hrm", $company->slug)}}/"+user_id+"/"+des_id, function (data) {
                    $("#table-items").html(data);
                    $(".ratingg").rating({language:"ar"});
                });
            });

            $(document).on("submit", "#addEvaluation", function (e) {
                e.preventDefault();

                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{route("company.hrm.save_evaluation", $company->slug)}}',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        $("#addEvaluation").trigger("reset");
                        $("#addEvaluation").slideUp();
                        goNotif("success", "تم حفظ التقييم بنجاح");
                    }, error: function (data) {
                        goNotif("error", "لقد وقع خطأ ما, الرجاء المحاولة من جديد");
                    }
                }).always(function () {
                    $("#menu5").html($("#loading-wrapper").html());
                    $("#menu5").load(window.location + " #menu5-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-ev", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu5").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_ev", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu5").load(window.location + " #menu5-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#evaluations_filter", function (e){
                var date1   = $("#evaluations_filter .date1").val();
                var date2   = $("#evaluations_filter .date2").val();
                var userss = $("#evaluations_filter .userss").select2('val')+"";

                $('.grd_view_tasks>tbody.tbody tr.tr').hide();
                $('.grd_view_tasks>tbody.tbody tr.tr').filter(function () {
                    var datex = true;
                    var date0 = $(this).data("date0");
                    if(date0){
                        if(date1 && !date2) {
                            if(new Date(date1) > new Date(date0))
                                datex = false;
                        }else if(!date1 && date2){
                            if(new Date(date0) > new Date(date2))
                                datex = false;
                        }else if(date1 && date2) {
                            if(new Date(date1)>new Date(date0) || new Date(date2)<new Date(date0))
                                datex = false;
                        }
                    }

                    var userx = true;
                    var user0 = $(this).data("user0");
                    if(userss){
                        if(userss!="0"){
                            if(user0!=userss)
                                userx=false;
                        }
                    }

                    console.log("--------------");
                    console.log(userss);
                    console.log(user0);
                    console.log(userx);
                    console.log("--------------");

                    return  (datex && userx);

                }).show();
            });

            $(document).on("reset", "#evaluations_filter", function (e){
                $('.grd_view_tasks>tbody.tbody tr.tr').show();
                $("#evaluations_filter .date1").val("");
                $("#evaluations_filter .date2").val("");
                $('#evaluations_filter .userss').val(0).trigger('change');
            });


        });
    </script>

@endsection