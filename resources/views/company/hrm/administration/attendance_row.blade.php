<tr>
    <td width="18px" style="padding: 2px">
        @if($user->vac)
            <div class="gold"></div>
        @elseif($user->tra)
            <div class="blue"></div>
        @elseif($user->man)
            <div class="purple"></div>
        @else
            @if($user->att)
                <div class="green"></div>
            @else
                <div class="red"></div>
            @endif
        @endif
    </td>
    <td>{{$today}}</td>
    <td>{{$user->name}}</td>
    <td>
        @if($user->vac)
            <span class="gold block">إجازة</span>
        @elseif($user->tra)
            <span class="blue block">تدريب</span>
        @elseif($user->man)
            <div class="purple block">إنتداب</div>
        @else
            @if($user->att)
                <span class="green block">حضور</span>
            @else
                <span class="red block">غياب</span>
            @endif
        @endif
    </td>
    <td>
        @if($user->vac)
        @elseif($user->tra)
        @elseif($user->man)
        @else
            @if($user->att)
                <span class="green block">{{@$user->att->start_time}}</span>
            @endif
        @endif
    </td>
    <td>
        @if($user->vac)
        @elseif($user->tra)
        @elseif($user->man)
        @else
            @if($user->att)
                <span class="green block">{{@$user->att->end_time}}</span>
            @endif
        @endif
    </td>
</tr>