<div class="form-group" >
    <a href="{{route("company.hrm.employee", [$company->slug, @$user->id])}}">
        @if(@$user->image)
            <img src="{{asset("storage/app/".@$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
        @else
            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
        @endif
        <span style="display: inline-block">{{@$user->name}}</span>
    </a>
</div>

<div class="form-group" >
    <label>التقييمات</label>
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th>البند</th>
            <th>المسمى الوظيفي</th>
            <th width="45%">الوصف</th>
            <th>التقييم</th>
        </tr>
        @foreach($item_hrms1 as $item)
            <tr data-des="{{@$item->designation_id}}">
                <td>{{$item->name}}</td>
                <td>
                    @if(@$item->designation)
                        <span>{{@$item->designation->name}}</span>
                    @else
                        <span>الكل</span>
                    @endif
                </td>
                <td>{{$item->description}}</td>
                <td>
                    <div style="direction: ltr !important;">
                        <input name="stars[{{@$item->id}}]" value="1" type="text" class="rating ratingg" data-min=0 data-max=5 data-step=1 data-size="sm" data-language="ar" required title="sdf">
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>

<div class="form-group">
    <label>الملاحظات</label>
    <textarea name="notes" rows="5" class="form-control"></textarea>
</div>

<center>
    <button type="submit" class="btn btn-primary" style="display: block"><i class="fa fa-save"></i> <span>حفظ التقييم</span></button>
</center>