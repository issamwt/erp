@extends("company.layouts.app", ["title"=>"الشؤون الإدارية"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>الشؤون الإدارية</strong>
                        <a href="{{route("company.hrm.employees", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>قائمة الموظفين</span></a>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="boxino">
                                    <table class="table">
                                        <tr><td width="50%">الإسم : </td><td>{{$employee->name}}</td></tr>
                                        <tr><td width="50%">المسمى الوظيفي : </td><td>@if(@$employee->details->designation){{@$employee->details->designation->name}}@else<span>غير محدد</span> <span class="red">*</span>@endif</td></tr>
                                        <tr><td width="50%">نوع الدوام : </td><td>@if(@$employee->details->job_time=="Full")<span>كامل</span>@elseif(@$employee->details->job_time=="Part")<span>جزئي</span>@else<span>غير محدد</span> <span class="red">*</span>@endif</td></tr>
                                        <tr><td width="50%">الإدارة : </td><td>{{(@$employee->details->department)?@$employee->details->department->name:"لا يتبع إدارة"}}</td></tr>
                                        <tr><td width="50%">المدير المباشر : </td><td>{{(@$employee->details->manager)?@$employee->details->manager->name:"لا يوجد"}}</td></tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>

                            <div class="col-sm-12 mt-30">

                                <ul class="nav nav-tabs">

                                    @if(permissions_hrm("tab_vacation"))
                                        <li class="@if(!session("tabsnum") or in_array(session("tabsnum"), [1, 2, 3, 4, 5])) active @endif">
                                        <a data-toggle="tab" href="#menu5" data-num="5">الإجازات</a>
                                    </li>
                                    @endif

                                    @if(permissions_hrm("tab_training"))
                                        <li class="@if(session("tabsnum")==6) active @endif">
                                        <a data-toggle="tab" href="#menu6" data-num="6">الدورات التدريبية</a>
                                    </li>
                                    @endif

                                    @if(permissions_hrm("tab_mandate"))
                                        <li class="@if(session("tabsnum")==7) active @endif">
                                        <a data-toggle="tab" href="#menu7" data-num="7">الإنتدابات</a>
                                    </li>
                                    @endif

                                </ul>

                                <div class="tab-content">

                                    @if(permissions_hrm("tab_vacation"))
                                    <div id="menu5" class="tab-pane in @if(!session("tabsnum") or in_array(session("tabsnum"), [1, 2, 3, 4, 5])) active @endif">
                                        <div id="menu5-content">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">م</th>
                                                                <th>من تاريخ</th>
                                                                <th>إلى تاريخ</th>
                                                                <th>نوع الإجازة</th>
                                                                <th width="35%">التفاصيل</th>
                                                                <th>الملف المرفق</th>
                                                                <th>حالة الإجازة</th>
                                                                <th>الأوامر</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($vacations as $vacation)
                                                                <tr>
                                                                    <td width="35px" class="text-center">{{$loop->iteration}}</td>
                                                                    <td>{{$vacation->from_date}}</td>
                                                                    <td>{{$vacation->to_date}}</td>
                                                                    <td>{{@$vacation->category->name}}</td>
                                                                    <td>{{$vacation->details}}</td>
                                                                    <td class="text-center">
                                                                        @if($vacation->file)
                                                                            <a href="{{asset("storage/app/".$vacation->file)}}" data-toggle="tooltip" title="تحميل الملف" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                                                        @else
                                                                            <span>لا يوجد</span>
                                                                        @endif
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if($vacation->status==0)
                                                                            <span class="label label-warning block">قيد الإنتظار</span>
                                                                        @elseif($vacation->status==1)
                                                                            <span class="label label-success block">موافق</span>
                                                                        @else
                                                                            <span class="label label-danger block">مرفوض</span>
                                                                        @endif
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if(permissions_hrm("update_vacation"))
                                                                            @if($vacation->status==0)
                                                                                <a data-target="#editVacation{{$vacation->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل الإجازة"><i class="fa fa-edit"></i></a>
                                                                            @else
                                                                                <a class="btn btn-success btn-xs disabled" disabled href="#" data-toggle="tooltip" title="تعديل الإجازة"><i class="fa fa-edit"></i></a>
                                                                            @endif
                                                                        @endif
                                                                        @if(permissions_hrm("delete_vacation"))
                                                                            <a data-id="{{$vacation->id}}" class="btn btn-danger btn-xs btn-delete-vacation" href="{{route("company.hrm.delete_vacation", [$company->slug, $vacation->id])}}"
                                                                           data-toggle="tooltip" title="حذف الإجازة"><i class="fa fa-remove"></i></a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @if(permissions_hrm("update_vacation"))
                                                                <tr>
                                                                    @if($vacation->status==0)
                                                                        <td colspan="7" class="formi" id="editVacation{{$vacation->id}}">
                                                                            <div class="col-sm-4 col-sm-offset-4">
                                                                                <h5>تعديل الإجازة</h5>
                                                                                <form id="form_update_vacation" method="post" enctype="multipart/form-data">
                                                                                    @csrf
                                                                                    <input type="hidden" name="id" value="{{$vacation->id}}">

                                                                                    <div class="form-group row">
                                                                                        <label class="col-sm-4">نوع الإجازة <span class="red">*</span></label>
                                                                                        <select class="form-control col-sm-8" name="vacation_category_id" id="vacation_category_id" required>
                                                                                            <option value="">إختر البدل</option>
                                                                                            @foreach($vacationCategories as $cat)
                                                                                                <option @if($cat->id==$vacation->vacation_category_id) selected @endif value="{{$cat->id}}" data-duration="{{$cat->duration}}" data-payed="{{$cat->payed}}" data-discout="{{$cat->discout}}">{{$cat->name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="form-group row" id="vacation_duration" style="display: none;">
                                                                                        <label class="col-sm-4">المدة المسموح بها</label>
                                                                                        <div class="form-control col-sm-8 variable" disabled></div>
                                                                                    </div>

                                                                                    <div class="form-group row" id="vacation_payed" style="display: none;">
                                                                                        <label class="col-sm-4">مدفوعة الأجر</label>
                                                                                        <div class="form-control col-sm-8 variable" disabled></div>
                                                                                    </div>

                                                                                    <div class="form-group row" id="vacation_discout" style="display: none;">
                                                                                        <label class="col-sm-4">نسبة الخصم من الراتب</label>
                                                                                        <div class="form-control col-sm-8 variable" disabled></div>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                                                        <input value="{{$vacation->from_date}}" type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-sm-4">إلى تاريخ <span class="red">*</span></label>
                                                                                        <input value="{{$vacation->to_date}}" type="text" class="form-control col-sm-8 datee" name="to_date" required autocomplete="off">
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <label class="col-sm-4">التفاصيل </label>
                                                                                        <textarea class="form-control col-sm-8" name="details" autocomplete="off" rows="4">{{$vacation->details}}</textarea>
                                                                                    </div>

                                                                                    <div class="form-group row">
                                                                                        <div class="col-sm-4">
                                                                                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </td>
                                                                    @endif
                                                                </tr>
                                                                @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_vacation"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة إجازة : </h5>
                                                    <form id="form_add_vacation" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">نوع الإجازة <span class="red">*</span></label>
                                                            <select class="form-control col-sm-8" name="vacation_category_id" id="vacation_category_id" required>
                                                                <option value="">إختر البدل</option>
                                                                @foreach($vacationCategories as $cat)
                                                                    <option value="{{$cat->id}}" data-duration="{{$cat->duration}}" data-payed="{{$cat->payed}}" data-discout="{{$cat->discout}}">{{$cat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group row" id="vacation_duration" style="display: none;">
                                                            <label class="col-sm-4">المدة المسموح بها</label>
                                                            <div class="form-control col-sm-8 variable" disabled></div>
                                                        </div>

                                                        <div class="form-group row" id="vacation_payed" style="display: none;">
                                                            <label class="col-sm-4">مدفوعة الأجر</label>
                                                            <div class="form-control col-sm-8 variable" disabled></div>
                                                        </div>

                                                        <div class="form-group row" id="vacation_discout" style="display: none;">
                                                            <label class="col-sm-4">نسبة الخصم من الراتب</label>
                                                            <div class="form-control col-sm-8 variable" disabled></div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إلى تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="to_date" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">التفاصيل </label>
                                                            <textarea class="form-control col-sm-8" name="details" autocomplete="off" rows="4"></textarea>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الملف المرفق </label>
                                                            <input type="file" accept="image/*" class="form-control col-sm-8" name="file" autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                    @endif

                                    @if(permissions_hrm("tab_training"))
                                    <div id="menu6" class="tab-pane @if(session("tabsnum")==6) active @endif">
                                        <div id="menu6-content">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">م</th>
                                                            <th>إسم التدريب</th>
                                                            <th>إسم المؤسسة</th>
                                                            <th>من تاريخ</th>
                                                            <th>إلى تاريخ</th>
                                                            <th>ميزانية التدريب</th>
                                                            <th width="35%">التفاصيل</th>
                                                            <th>الملف المرفق</th>
                                                            <th>الأوامر</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($trainings as $training)
                                                            <tr>
                                                                <td width="35px" class="text-center">{{$loop->iteration}}</td>
                                                                <td>{{$training->name}}</td>
                                                                <td>{{$training->entreprise}}</td>
                                                                <td>{{$training->from_date}}</td>
                                                                <td>{{$training->to_date}}</td>
                                                                <td><span>{{$training->price}}</span> <span>ريال</span></td>
                                                                <td>{{$training->details}}</td>
                                                                <td class="text-center">
                                                                    @if($training->file)
                                                                        <a href="{{asset("storage/app/".$training->file)}}" data-toggle="tooltip" title="تحميل الملف" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                                                    @else
                                                                        <span>لا يوجد</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">
                                                                    @if(permissions_hrm("update_training"))
                                                                        <a data-target="#editTraining{{$training->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل الإجازة"><i class="fa fa-edit"></i></a>
                                                                    @endif
                                                                    @if(permissions_hrm("delete_training"))
                                                                        <a data-id="{{$training->id}}" class="btn btn-danger btn-xs btn-delete-training" href="{{route("company.hrm.delete_training", [$company->slug, $vacation->id])}}"
                                                                       data-toggle="tooltip" title="حذف الإجازة"><i class="fa fa-remove"></i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @if(permissions_hrm("update_training"))
                                                            <tr>
                                                                <td colspan="7" class="formi" id="editTraining{{$training->id}}">
                                                                    <div class="col-sm-6 col-sm-offset-3">
                                                                        <h5>تعديل التدريب</h5>
                                                                        <form id="form_update_training" method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <input type="hidden" name="id" value="{{$training->id}}">

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">إسم التدريب <span class="red">*</span></label>
                                                                                <input type="text" value="{{$training->name}}" class="form-control col-sm-8" name="name" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">إسم المؤسسة <span class="red">*</span></label>
                                                                                <input type="text" value="{{$training->entreprise}}" class="form-control col-sm-8" name="entreprise" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                                                <input value="{{$training->from_date}}" type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">إلى تاريخ <span class="red">*</span></label>
                                                                                <input value="{{$training->to_date}}" type="text" class="form-control col-sm-8 datee" name="to_date" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">الميزانية <span class="red">*</span></label>
                                                                                <input type="number" value="{{$training->price}}" class="form-control col-sm-8" name="price" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">التفاصيل </label>
                                                                                <textarea class="form-control col-sm-8" name="details" autocomplete="off" rows="4">{{$training->details}}</textarea>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div class="col-sm-4">
                                                                                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_training"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة تدريب : </h5>
                                                    <form id="form_add_training" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">
                                                        <input type="hidden" name="status" value="0">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إسم التدريب <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" name="name" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إسم المؤسسة <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" name="entreprise" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إلى تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="to_date" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الميزانية <span class="red">*</span></label>
                                                            <input type="number" class="form-control col-sm-8" name="price" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">التفاصيل </label>
                                                            <textarea class="form-control col-sm-8" name="details" autocomplete="off" rows="4"></textarea>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الملف المرفق </label>
                                                            <input type="file" accept="image/*" class="form-control col-sm-8" name="file" autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                    @endif

                                    @if(permissions_hrm("tab_mandate"))
                                    <div id="menu7" class="tab-pane @if(session("tabsnum")==7) active @endif">
                                        <div id="menu7-content">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">م</th>
                                                            <th>مهمة التدريب</th>
                                                            <th>الجهة الطالبة</th>
                                                            <th>موقع الإنتداب</th>
                                                            <th>نوع الإنتداب</th>
                                                            <th>من تاريخ</th>
                                                            <th>إلى تاريخ</th>
                                                            <th>ميزانية التدريب</th>
                                                            <th width="25%">التفاصيل</th>
                                                            <th>الملف المرفق</th>
                                                            <th>الأوامر</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($mandates as $mandate)
                                                            <tr>
                                                                <td width="35px" class="text-center">{{$loop->iteration}}</td>
                                                                <td>{{$mandate->task}}</td>
                                                                <td>{{$mandate->entreprise}}</td>
                                                                <td>{{$mandate->address}}</td>
                                                                <td>@if($mandate->type==0) <span>داخلي</span> @else <span>خارجي</span> @endif</td>
                                                                <td>{{$mandate->from_date}}</td>
                                                                <td>{{$mandate->to_date}}</td>
                                                                <td><span>{{$mandate->price}}</span> <span>ريال</span></td>
                                                                <td>{{$mandate->details}}</td>
                                                                <td class="text-center">
                                                                    @if($mandate->file)
                                                                        <a href="{{asset("storage/app/".$mandate->file)}}" data-toggle="tooltip" title="تحميل الملف" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                                                    @else
                                                                        <span>لا يوجد</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">
                                                                    @if(permissions_hrm("update_mandate"))
                                                                        <a data-target="#editMandate{{$mandate->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل الإجازة"><i class="fa fa-edit"></i></a>
                                                                    @endif
                                                                    @if(permissions_hrm("delete_mandate"))
                                                                        <a data-id="{{$mandate->id}}" class="btn btn-danger btn-xs btn-delete-mandate" href="{{route("company.hrm.delete_mandate", [$company->slug, $vacation->id])}}"
                                                                       data-toggle="tooltip" title="حذف الإجازة"><i class="fa fa-remove"></i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @if(permissions_hrm("update_mandate"))
                                                            <tr>
                                                                <td colspan="11" class="formi" id="editMandate{{$mandate->id}}">
                                                                    <div class="col-sm-6 col-sm-offset-3">
                                                                        <h5>تعديل الإنتداب</h5>
                                                                        <form id="form_update_mandate" method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <input type="hidden" name="id" value="{{$mandate->id}}">

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">مهمة التدريب <span class="red">*</span></label>
                                                                                <input type="text" value="{{$mandate->task}}" class="form-control col-sm-8" name="task" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">الجهة الطالبة <span class="red">*</span></label>
                                                                                <input type="text" value="{{$mandate->entreprise}}" class="form-control col-sm-8" name="entreprise" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">موقع الإنتداب <span class="red">*</span></label>
                                                                                <input type="text" value="{{$mandate->address}}" class="form-control col-sm-8" name="address" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">نوع الإنتداب <span class="red">*</span></label>
                                                                                <select class="form-control col-sm-8" name="type" required autocomplete="off">
                                                                                    <option value="">إختر</option>
                                                                                    <option value="0" @if($mandate->type==0) selected @endif>داخلي</option>
                                                                                    <option value="1" @if($mandate->type==1) selected @endif>خارجي</option>
                                                                                </select>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                                                <input type="text" value="{{$mandate->from_date}}" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">إلى تاريخ <span class="red">*</span></label>
                                                                                <input type="text" value="{{$mandate->to_date}}" class="form-control col-sm-8 datee" name="to_date" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">الميزانية <span class="red">*</span></label>
                                                                                <input type="number" value="{{$mandate->price}}" class="form-control col-sm-8" name="price" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">التفاصيل </label>
                                                                                <textarea class="form-control col-sm-8" name="details" autocomplete="off" rows="4">{{$mandate->entreprise}}</textarea>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div class="col-sm-4">
                                                                                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_mandate"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة إنتداب : </h5>
                                                    <form id="form_add_mandate" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">
                                                        <input type="hidden" name="status" value="0">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">مهمة التدريب <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" name="task" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الجهة الطالبة <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" name="entreprise" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">موقع الإنتداب <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" name="address" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">نوع الإنتداب <span class="red">*</span></label>
                                                            <select class="form-control col-sm-8" name="type" required autocomplete="off">
                                                                <option value="">إختر</option>
                                                                <option value="0">داخلي</option>
                                                                <option value="1">خارجي</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إلى تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="to_date" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الميزانية <span class="red">*</span></label>
                                                            <input type="number" class="form-control col-sm-8" name="price" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">التفاصيل </label>
                                                            <textarea class="form-control col-sm-8" name="details" autocomplete="off" rows="4"></textarea>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الملف المرفق </label>
                                                            <input type="file" accept="image/*" class="form-control col-sm-8" name="file" autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>

                                    </div>
                                    @endif

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <script>
        $(function () {

            function reloadish(){
                $('.datee').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true
                });
            }
            reloadish();

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("click", ".nav-tabs li a", function (e) {
                e.preventDefault();
                var num = $(this).data("num");
                $.get("{{route("company.hrm.tabsnum", $company->slug)}}/"+num);
            });

            $(document).on("click", "#vacation_category_id", function (e) {
                e.preventDefault();
                if($(this).find("option:selected").val()){
                    var duration    = $(this).closest("form").find("option:selected").data("duration");
                    var payed       = $(this).closest("form").find("option:selected").data("payed");
                    var discout     = $(this).closest("form").find("option:selected").data("discout");
                    $(this).closest("form").find("#vacation_duration").slideDown();
                    $(this).closest("form").find("#vacation_duration .variable").html(duration+ " أيام");
                    $(this).closest("form").find("#vacation_payed").slideDown();
                    if(payed==1){
                        $(this).closest("form").find("#vacation_payed .variable").html("نعم");
                        $(this).closest("form").find("#vacation_discout").slideUp();
                        $(this).closest("form").find("#vacation_discout .variable").html("");
                    }else{
                        $(this).closest("form").find("#vacation_payed .variable").html("لا");
                        $(this).closest("form").find("#vacation_discout").slideDown();
                        $(this).closest("form").find("#vacation_discout .variable").html("%"+discout);
                    }
                }else{
                    $(this).closest("form").find("#vacation_duration").slideUp();
                    $(this).closest("form").find("#vacation_payed").slideUp();
                    $(this).closest("form").find("#vacation_discout").slideUp();
                    $(this).closest("form").find("#vacation_duration .variable").html("");
                    $(this).closest("form").find("#vacation_payed .variable").html("");
                    $(this).closest("form").find("#vacation_discout .variable").html("");
                }
            });

            // Vacation

            $(document).on("submit", "#form_add_vacation", function (e) {
                e.preventDefault();
                $("#menu5").html($("#loading-wrapper").html());
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{route("company.hrm.add_vacation", $company->slug)}}',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result.error)
                            goNotif("error", result.error);

                    }, error: function (data) {
                        goNotif("error", data);
                    }
                }).always(function () {
                    $("#menu5").load(window.location + " #menu5-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-vacation", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu5").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_vacation", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu5").load(window.location + " #menu5-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_vacation", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu5").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_vacation", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu5").load(window.location + " #menu5-content", function () {
                        reloadish();
                    });
                });
            });

            // Training

            $(document).on("submit", "#form_add_training", function (e) {
                e.preventDefault();
                $("#menu6").html($("#loading-wrapper").html());
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{route("company.hrm.add_training", $company->slug)}}',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result.error)
                            goNotif("error", result.error);

                    }, error: function (data) {
                        goNotif("error", data);
                    }
                }).always(function () {
                    $("#menu6").load(window.location + " #menu6-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-training", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu6").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_training", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu6").load(window.location + " #menu6-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_training", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu6").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_training", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu6").load(window.location + " #menu6-content", function () {
                        reloadish();
                    });
                });
            });

            // Mandate

            $(document).on("submit", "#form_add_mandate", function (e) {
                e.preventDefault();
                $("#menu7").html($("#loading-wrapper").html());
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '{{route("company.hrm.add_mandate", $company->slug)}}',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result.error)
                            goNotif("error", result.error);

                    }, error: function (data) {
                        goNotif("error", data);
                    }
                }).always(function () {
                    $("#menu7").load(window.location + " #menu7-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-mandate", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu7").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_mandate", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu7").load(window.location + " #menu7-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_mandate", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu7").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_mandate", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu7").load(window.location + " #menu7-content", function () {
                        reloadish();
                    });
                });
            });





        });
    </script>

    <style>
        .panel-body{
            min-height: 1000px;
        }
        .boxino td{border: none !important;}
        .boxino{
            background: #277AB3;
            color: #fff;
            padding: 5px 5px 5px 5px;
            box-shadow: 3px 3px 2px #b5b5b5;
        }
        .boxino .table{
            margin-bottom: 0px;
        }
        .tab-content{
            padding-top: 15px;
        }
        .label-block{
            display: inline-block;
            width: 100%;
            border-radius: 0px;
            padding: 7px !important;
        }
        .formi{
            display: none;
            border: 1px solid #2d6a8d;
            padding: 15px 20px;
            margin: 15px auto;
        }
        .print{
            display: none;
        }
    </style>

@endsection