@extends("company.layouts.app", ["title"=>"الشؤون المالية"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>الشؤون المالية</strong>
                        <a href="{{route("company.hrm.employees", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>قائمة الموظفين</span></a>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="boxino">
                                    <table class="table">
                                        <tr><td width="50%">الإسم : </td><td>{{$employee->name}}</td></tr>
                                        <tr><td width="50%">المسمى الوظيفي : </td><td>@if(@$employee->details->designation){{@$employee->details->designation->name}}@else<span>غير محدد</span> <span class="red">*</span>@endif</td></tr>
                                        <tr><td width="50%">نوع الدوام : </td><td>@if(@$employee->details->job_time=="Full")<span>كامل</span>@elseif(@$employee->details->job_time=="Part")<span>جزئي</span>@else<span>غير محدد</span> <span class="red">*</span>@endif</td></tr>
                                        <tr><td width="50%">الإدارة : </td><td>{{(@$employee->details->department)?@$employee->details->department->name:"لا يتبع إدارة"}}</td></tr>
                                        <tr><td width="50%">المدير المباشر : </td><td>{{(@$employee->details->manager)?@$employee->details->manager->name:"لا يوجد"}}</td></tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="boxino">
                                    <table class="table">
                                        <tr><td width="50%">الراتب الاساسي : </td><td>{{(@$employee->details->salary)?@$employee->details->salary." ريال":"0 ريال"}}</td></tr>
                                        <tr><td width="50%">الراتب الإجمالي : </td><td style="font-weight: bold;" id="final_salary">0 ريال</td></tr>
                                    </table>
                                </div>
                            </div>
                            <?php $salary = (@$employee->details->salary and @$employee->details->salary!=0)?@$employee->details->salary:0;?>

                            <div class="col-sm-12 mt-30">

                                <ul class="nav nav-tabs">

                                    @if(permissions_hrm("tab_allowance"))
                                        <li class="@if(!session("tabsnum") or in_array(session("tabsnum"), [1, 5, 6, 7, 8])) active @endif">
                                        <a data-toggle="tab" href="#menu1" data-num="1">البدلات</a>
                                    </li>
                                    @endif
                                    @if(permissions_hrm("tab_deduction"))
                                        <li class="@if(session("tabsnum")==2) active @endif">
                                        <a data-toggle="tab" href="#menu2" data-num="2">الإقتطاعات</a>
                                    </li>
                                    @endif
                                    @if(permissions_hrm("tab_advance"))
                                        <li class="@if(session("tabsnum")==3) active @endif">
                                        <a data-toggle="tab" href="#menu3" data-num="3">السلف</a>
                                    </li>
                                    @endif
                                    @if(permissions_hrm("tab_custody"))
                                        <li class="@if(session("tabsnum")==4) active @endif">
                                        <a data-toggle="tab" href="#menu4" data-num="4">العهدات</a>
                                    </li>
                                    @endif
                                </ul>

                                <div class="tab-content">

                                    @if(permissions_hrm("tab_allowance"))
                                    <div id="menu1" class="tab-pane in @if(!session("tabsnum") or in_array(session("tabsnum"), [1, 5, 6, 7, 8])) active @endif">
                                        <div id="menu1-content">

                                            <?php $salary = (@$employee->details->salary and @$employee->details->salary!=0)?@$employee->details->salary:0;?>

                                            <div class="row">
                                                <div class="col-sm-12 mt-30">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>رمز البدل</th>
                                                                <th>قيمة البدل</th>
                                                                <th>من تاريخ</th>
                                                                <th>إلى تاريخ</th>
                                                                <th>الحالة</th>
                                                                <th>المجموع</th>
                                                                <th>الأوامر</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $total_allowance=0; ?>
                                                            @foreach($allowances as $allowance)
                                                                <?php $active = false; $to = ($allowance->to_date)?$allowance->to_date:"9999-12-30"; $from=$allowance->from_date; ?>
                                                                <tr>
                                                                    <td>{{@$allowance->category->name}}</td>
                                                                    <td>
                                                                        <span>{{@$allowance->category->value}}</span>
                                                                        @if(@$allowance->category->type==0)
                                                                            <span> (%)</span>
                                                                        @else
                                                                            <span> ريال</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{$allowance->from_date}}</td>
                                                                    <td>{{$allowance->to_date}}</td>
                                                                    <td>
                                                                        @if ($from <= $today and $today <= $to)
                                                                            <span class="label label-success label-block">فعال</span>
                                                                        @else
                                                                            <span class="label label-warning label-block">غير فعال</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if(@$allowance->category->type==0)
                                                                            <?php $val = $salary/100*@$allowance->category->value; ?>
                                                                        @else
                                                                            <?php $val = @$allowance->category->value;?>
                                                                        @endif
                                                                        @if ($from <= $today and $today <= $to) @else <?php $val=0; ?> @endif
                                                                        <?php $total_allowance+=$val; ?>
                                                                        <span>{{number_format($val, 2)}}</span> <span>ريال</span>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if(permissions_hrm("update_allowance"))
                                                                            <a data-target="#editAllownace{{$allowance->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل البدل"><i class="fa fa-edit"></i></a>
                                                                        @endif
                                                                        @if(permissions_hrm("delete_allowance"))
                                                                            <a data-id="{{$allowance->id}}" class="btn btn-danger btn-xs btn-delete-allowance" href="{{route("company.hrm.delete_allowance", [$company->slug, $allowance->id])}}"
                                                                        data-toggle="tooltip" title="حذف البدل"><i class="fa fa-remove"></i></a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @if(permissions_hrm("update_allowance"))
                                                                <tr>
                                                                    <td id="editAllownace{{$allowance->id}}" class="formi" colspan="7">
                                                                        <div class="col-sm-4 col-sm-offset-4">
                                                                            <h5>تعديل البدل</h5>
                                                                            <form id="form_update_allowance" method="post">
                                                                                @csrf
                                                                                <input type="hidden" name="id" value="{{$allowance->id}}">

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-4">البدل <span class="red">*</span></label>
                                                                                    <select class="form-control col-sm-8" name="allowance_category_id" id="allowance_category_id" required>
                                                                                        <option value="">إختر البدل</option>
                                                                                        @foreach($allowance_categories as $cat)
                                                                                            <option value="{{$cat->id}}" data-type="{{$cat->type}}" data-value="{{$cat->value}}" @if($cat->id==@$allowance->category->id) selected @endif>{{$cat->name}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-4">نوع البدل </label>
                                                                                    <input type="text" disabled value="{{(@$allowance->category->type==0)?"نسبة من الراتب الأساسي":"قيمة ثابتة"}}" class="form-control col-sm-8" id="allowance_type">
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-4">قيمة البدل </label>
                                                                                    <input type="text" disabled value="{{@$allowance->category->value}} {{(@$allowance->category->type==0)?"%":"ريال"}}" class="form-control col-sm-8" id="allowance_value">
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                                                    <input type="text" value="{{$allowance->from_date}}" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-4">إلى تاريخ</label>
                                                                                    <input type="text" value="{{$allowance->to_date}}" class="form-control col-sm-8 datee" name="to_date" autocomplete="off">
                                                                                </div>
                                                                                <div class="form-group row">
                                                                                    <div class="col-sm-4">
                                                                                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                            @endforeach
                                                            <tr>
                                                                <td colspan="5"></td>
                                                                <td>
                                                                    <div id="total_allowance" data-val="{{$total_allowance}} "></div>
                                                                    <span>{{number_format($total_allowance, 2)}}</span> <span>ريال</span>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_allowance"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة بدل : </h5>
                                                    <form id="form_add_allowance" method="post">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">البدل <span class="red">*</span></label>
                                                            <select class="form-control col-sm-8" name="allowance_category_id" id="allowance_category_id" required>
                                                                <option value="">إختر البدل</option>
                                                                @foreach($allowance_categories as $cat)
                                                                    <option value="{{$cat->id}}" data-type="{{$cat->type}}" data-value="{{$cat->value}}">{{$cat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">نوع البدل </label>
                                                            <input type="text" disabled class="form-control col-sm-8" id="allowance_type">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">قيمة البدل </label>
                                                            <input type="text" disabled class="form-control col-sm-8" id="allowance_value">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إلى تاريخ</label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="to_date" autocomplete="off">
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                    @endif

                                    @if(permissions_hrm("tab_deduction"))
                                    <div id="menu2" class="tab-pane @if(session("tabsnum")==2) active @endif">
                                        <div id="menu2-content">

                                            <?php $salary = (@$employee->details->salary and @$employee->details->salary!=0)?@$employee->details->salary:0;?>

                                            <div class="row">
                                                <div class="col-sm-12 mt-30">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>رمز الإقتطاع</th>
                                                            <th>قيمة الإقتطاع</th>
                                                            <th>القيمة النهائية</th>
                                                            <th>من تاريخ</th>
                                                            <th>إلى تاريخ</th>
                                                            <th>الحالة</th>
                                                            <th>المجموع</th>
                                                            <th>الأوامر</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $total_deduction=0; ?>
                                                        @foreach($deductions as $deduction)
                                                            <?php $active = false; $to = ($deduction->to_date)?$deduction->to_date:"9999-12-30"; $from=$deduction->from_date; ?>
                                                            <tr>
                                                                <td>{{@$deduction->category->name}}</td>
                                                                <td>
                                                                    <span>{{@$deduction->category->value}}</span>
                                                                    @if(@$deduction->category->type==0)
                                                                        <span> (%)</span>
                                                                    @else
                                                                        <span> ريال</span>
                                                                    @endif
                                                                </td>
                                                                <td>{{$deduction->amount}}</td>
                                                                <td>{{$deduction->from_date}}</td>
                                                                <td>{{$deduction->to_date}}</td>
                                                                <td>
                                                                    @if ($from <= $today and $today <= $to)
                                                                        <span class="label label-success label-block">فعال</span>
                                                                    @else
                                                                        <span class="label label-warning label-block">غير فعال</span>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <?php $val = intval($deduction->amount);?>
                                                                    @if ($from <= $today and $today <= $to) @else <?php $val=0; ?> @endif
                                                                    <?php $total_deduction += $val; ?>
                                                                    <span>{{number_format($val, 2)}}</span> <span>ريال</span>
                                                                </td>
                                                                <td class="text-center">
                                                                    @if(permissions_hrm("update_deduction"))
                                                                        <a data-target="#editDeduction{{$deduction->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل الإقتطاع"><i class="fa fa-edit"></i></a>
                                                                    @endif
                                                                    @if(permissions_hrm("delete_deduction"))
                                                                        <a data-id="{{$deduction->id}}" class="btn btn-danger btn-xs btn-delete-deduction" href="{{route("company.hrm.delete_deduction", [$company->slug, $deduction->id])}}"
                                                                       data-toggle="tooltip" title="حذف الإقتطاع"><i class="fa fa-remove"></i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @if(permissions_hrm("update_deduction"))
                                                            <tr>
                                                                <td id="editDeduction{{$deduction->id}}" class="formi" colspan="7">
                                                                    <div class="col-sm-4 col-sm-offset-4">
                                                                        <h5>تعديل الإقتطاع</h5>
                                                                        <form id="form_update_deduction" method="post">
                                                                            @csrf
                                                                            <input type="hidden" name="id" value="{{$deduction->id}}">

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">الإقتطاع <span class="red">*</span></label>
                                                                                <select class="form-control col-sm-8" name="deduction_category_id" id="deduction_category_id" required>
                                                                                    <option value="">إختر الإقتطاع</option>
                                                                                    @foreach($deduction_categories as $cat)
                                                                                        <option value="{{$cat->id}}" data-type="{{$cat->type}}" data-value="{{$cat->value}}" @if($cat->id==@$deduction->category->id) selected @endif>{{$cat->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">نوع الإقتطاع </label>
                                                                                <input type="text" disabled value="{{(@$deduction->category->type==0)?"نسبة من الراتب الأساسي":"قيمة ثابتة"}}" class="form-control col-sm-8" id="deduction_type">
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">قيمة الإقتطاع </label>
                                                                                <input type="text" disabled value="{{@$deduction->category->value}} {{(@$deduction->category->type==0)?"%":"ريال"}}" class="form-control col-sm-8" id="deduction_value">
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">القيمة النهائية(ريال) <span class="red">*</span></label>
                                                                                <input type="text" value="{{$deduction->amount}}" class="form-control col-sm-8" id="final_deduction_value" name="amount" required>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                                                <input type="text" value="{{$deduction->from_date}}" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">إلى تاريخ</label>
                                                                                <input type="text" value="{{$deduction->to_date}}" class="form-control col-sm-8 datee" name="to_date" autocomplete="off">
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div class="col-sm-4">
                                                                                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="6"></td>
                                                            <td>
                                                                <div id="total_deduction" data-val="{{$total_deduction}} "></div>
                                                                <span>{{number_format($total_deduction, 2)}}</span> <span>ريال</span>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_deduction"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة إقتطاع : </h5>
                                                    <form id="form_add_deduction" method="post">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الإقتطاع <span class="red">*</span></label>
                                                            <select class="form-control col-sm-8" name="deduction_category_id" id="deduction_category_id" required>
                                                                <option value="">إختر الإقتطاع</option>
                                                                @foreach($deduction_categories as $ded)
                                                                    <option value="{{$ded->id}}" data-type="{{$ded->type}}" data-value="{{$ded->value}}">{{$ded->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">نوع الإقتطاع </label>
                                                            <input type="text" disabled class="form-control col-sm-8" id="deduction_type">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">قيمة الإقتطاع </label>
                                                            <input type="text" disabled class="form-control col-sm-8" id="deduction_value">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">القيمة النهائية(ريال) <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" id="final_deduction_value" name="amount" required>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">من تاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="from_date" required autocomplete="off">
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إلى تاريخ</label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="to_date" autocomplete="off">
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                    @endif

                                    @if(permissions_hrm("tab_advance"))
                                    <div id="menu3" class="tab-pane @if(session("tabsnum")==3) active @endif">
                                        <div id="menu3-content">

                                            <div class="row">
                                                <div class="col-sm-12 mt-30">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>التاريخ</th>
                                                            <th>الكفيل</th>
                                                            <th>قيمة القرض</th>
                                                            <th>المبلغ المتبقي</th>
                                                            <th>طريقة السداد</th>
                                                            <th>قيمة القسط الشهري</th>
                                                            <th>عدد الأقساط الشهرية</th>
                                                            <th>تاريخ آخر عملية دفع</th>
                                                            <th>الأوامر</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $total_advance=0; ?>
                                                            @foreach($advances as $advance)
                                                                <tr>
                                                                    <td>{{$advance->thedate}}</td>
                                                                    <td>{{@$advance->kafil->name}}</td>
                                                                    <td><span>{{number_format($advance->amount, 2)}}</span> <span>ريال</span></td>
                                                                    <td><span>{{number_format($advance->rest, 2)}}</span> <span>ريال</span></td>
                                                                    <td>
                                                                        @if($advance->type==0)
                                                                            <span>على سنة</span>
                                                                        @else
                                                                            <span>على مبلغ محدد</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <span>{{number_format((floatval($advance->rest)>0)?floatval($advance->monthly):0, 2)}}</span> <span>ريال</span>
                                                                        <?php $total_advance += (floatval($advance->rest)>0)?floatval($advance->monthly):0; ?>
                                                                    </td>
                                                                    <td>{{$advance->months}}</td>
                                                                    <td>
                                                                        @if($advance->last_date)
                                                                            <span>{{$advance->last_date}}</span>
                                                                        @else
                                                                            <span>لا يوجد</span>
                                                                        @endif
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if(permissions_hrm("update_advance"))
                                                                            <a data-target="#editAdvance{{$advance->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل الإقتطاع"><i class="fa fa-edit"></i></a>
                                                                        @endif
                                                                        @if(permissions_hrm("delete_advance"))
                                                                            <a data-id="{{$advance->id}}" class="btn btn-danger btn-xs btn-delete-advance" href="{{route("company.hrm.delete_advance", [$company->slug, $advance->id])}}"
                                                                           data-toggle="tooltip" title="حذف السلف"><i class="fa fa-remove"></i></a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @if(permissions_hrm("update_advance"))
                                                                <tr>
                                                                    <td id="editAdvance{{$advance->id}}" class="formi" colspan="9">
                                                                        <div class="col-sm-6 col-sm-offset-3">
                                                                            <h5>تعديل السلف</h5>
                                                                            <form id="form_update_advance" method="post">
                                                                                @csrf
                                                                                <input type="hidden" name="id" value="{{$advance->id}}">

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">قيمة السلف </label>
                                                                                    <div class="col-sm-7">{{$advance->advance_amount}} <span>ريال</span></div>
                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">طريقة السداد <span class="red">*</span></label>
                                                                                    <div class="col-sm-7">
                                                                                        @if($advance->type==1)
                                                                                            <span>على سنة</span>
                                                                                        @else
                                                                                            <span>على مبلغ محدد</span>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">المبلغ المتبقي <span class="red">*</span></label>
                                                                                    <input type="number" value="{{$advance->rest}}" class="form-control col-sm-7" id="rest" name="rest" required autocomplete="off">
                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">قيمة القسط الشهري <span class="red">*</span></label>
                                                                                    <input type="number" value="{{$advance->monthly}}" class="form-control col-sm-7" id="monthly" name="monthly" required autocomplete="off">
                                                                                </div>


                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">عدد الأقساط الشهرية <span class="red">*</span></label>
                                                                                    <input type="text" value="{{$advance->months}}" class="form-control col-sm-7" id="months" name="months" required autocomplete="off">
                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">التاريخ </label>
                                                                                    <div class="col-sm-7">{{$advance->thedate}}</div>

                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <label class="col-sm-5">الكفيل</label>
                                                                                    <div class="col-sm-7">
                                                                                        @foreach($users as $user)
                                                                                            @if($advance->kafil_id==$user->id) <span>{{$user->name}}</span>@endif
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group row">
                                                                                    <div class="col-sm-4">
                                                                                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endif
                                                            @endforeach
                                                            <tr>
                                                                <td colspan="5"></td>
                                                                <td>
                                                                    <div id="total_advance" data-val="{{$total_advance}}"></div>
                                                                    <span>{{number_format($total_advance, 2)}}</span> <span>ريال</span>
                                                                </td>
                                                                <td colspan="3"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_advance"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة سلفة : </h5>
                                                    <form id="form_add_advance" method="post">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">قيمة السلف <span class="red">*</span></label>
                                                            <input type="number" class="form-control col-sm-8" name="amount" id="advance_amount" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">طريقة السداد <span class="red">*</span></label>
                                                            <select readonly class="form-control col-sm-8" name="type" id="advance_type" required>
                                                                <option value="">إختر</option>
                                                                <option value="1">على سنة</option>
                                                                <option value="2">على مبلغ محدد</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">قيمة القسط الشهري <span class="red">*</span></label>
                                                            <input type="number" readonly class="form-control col-sm-8" id="advance_monthly" name="monthly" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">عدد الأقساط الشهرية <span class="red">*</span></label>
                                                            <input readonly="" type="text" class="form-control col-sm-8" id="advance_months" name="months" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">التاريخ <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="thedate" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الكفيل</label>
                                                            <select type="text" class="form-control col-sm-8" name="kafil_id">
                                                                <option value="">إختر</option>
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>


                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>

                                    </div>
                                    @endif

                                    @if(permissions_hrm("tab_custody"))
                                    <div id="menu4" class="tab-pane @if(session("tabsnum")==4) active @endif">
                                        <div id="menu4-content">

                                            <div class="row">
                                                <div class="col-sm-12 mt-30">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>رقم العهدة</th>
                                                            <th>تاريخ التسليم</th>
                                                            <th>العدد</th>
                                                            <th>الإسم</th>
                                                            <th width="30%">الوصف</th>
                                                            <th>الملف</th>
                                                            <th>تأكيد الإستلام</th>
                                                            <th>الأوامر</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($custodies as $custody)
                                                            <tr>
                                                                <td>{{$custody->reference}}</td>
                                                                <td>{{$custody->thedate}}</td>
                                                                <td>{{$custody->number}}</td>
                                                                <td>{{$custody->name}}</td>
                                                                <td>{{$custody->description}}</td>
                                                                <td class="text-center">
                                                                    @if($custody->file)
                                                                        <a class="btn btn-info btn-xs" href="{{asset("storage/app/".$custody->file)}}" data-toggle="tooltip" title="تنزيل الملف"><i class="fa fa-download"></i></a>
                                                                    @else
                                                                        <span>لا يوجد</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">
                                                                    @if($custody->delivered==0)
                                                                        <i class="fa fa-remove" style="color: red"></i>
                                                                    @else
                                                                        <i class="fa fa-check green" style="color: #4cae4c"></i>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">

                                                                    @if(permissions_hrm("delivered_custody"))
                                                                        @if($custody->delivered==0)
                                                                            <a data-id="{{$custody->id}}" class="btn btn-warning btn-xs btn-delivered" href="#" data-toggle="tooltip" title="تأكيد الإستلام"><i class="fa fa-star"></i></a>
                                                                        @else
                                                                            <a disabled class="btn btn-warning btn-xs btn-delivered disabled" href="#" data-toggle="tooltip" title="تأكيد الإستلام"><i class="fa fa-star"></i></a>
                                                                        @endif
                                                                    @endif

                                                                    @if(permissions_hrm("update_custody"))
                                                                        <a data-target="#editCustody{{$custody->id}}" class="btn btn-success btn-xs btn-open" href="#" data-toggle="tooltip" title="تعديل العهدة"><i class="fa fa-edit"></i></a>
                                                                    @endif

                                                                    @if(permissions_hrm("print_custody"))
                                                                        <a data-id="{{$custody->id}}" class="btn btn-primary btn-xs btn-print-custody" href="#" data-toggle="tooltip" title="طباعة تفاصيل العهدة"><i class="fa fa-print"></i></a>
                                                                    @endif

                                                                    @if(permissions_hrm("delete_custody"))
                                                                        <a data-id="{{$custody->id}}" class="btn btn-danger btn-xs btn-delete-custody" href="{{route("company.hrm.delete_custody", [$company->slug, $custody->id])}}"
                                                                       data-toggle="tooltip" title="حذف العهدة"><i class="fa fa-remove"></i></a>
                                                                    @endif

                                                                </td>
                                                            </tr>
                                                            @if(permissions_hrm("update_custody"))
                                                            <tr>
                                                                <td id="editCustody{{$custody->id}}" class="formi" colspan="9">
                                                                    <div class="col-sm-6 col-sm-offset-3">
                                                                        <h5>تعديل العهدة</h5>
                                                                        <form class="form_update_custody" method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <input type="hidden" name="id" value="{{$custody->id}}">

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">رقم العهدة<span class="red">*</span></label>
                                                                                <div class="col-sm-8">{{$custody->reference}}</div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">إسم العهدة<span class="red">*</span></label>
                                                                                <input type="text" value="{{$custody->name}}" class="form-control col-sm-8" name="name" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">تاريخ التسليم <span class="red">*</span></label>
                                                                                <input type="text" value="{{$custody->thedate}}" class="form-control col-sm-8 datee" name="thedate" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">العدد <span class="red">*</span></label>
                                                                                <input type="number" value="{{$custody->number}}" class="form-control col-sm-8" name="number" required autocomplete="off">
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <label class="col-sm-4">الوصف </label>
                                                                                <textarea class="form-control col-sm-8" rows="3" name="description"  autocomplete="off">{{$custody->description}}</textarea>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div class="col-sm-4">
                                                                                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                    @foreach($custodies as $custody)
                                                        <div class="print" id="custody_{{$custody->id}}">
                                                            <style type="text/css">
                                                                .print {
                                                                    border: 1px solid gray;
                                                                    min-height: 200px;
                                                                    margin-bottom: 100px;
                                                                    padding: 100px;
                                                                }

                                                                table {
                                                                    width: 100%
                                                                }
                                                            </style>
                                                            <table style="margin-top: 50px">
                                                                <tr>
                                                                    <td width="10%"></td>
                                                                    <td style="vertical-align: top">
                                                                        <h3 style="margin: 0px;">تفاصيل عهدة</h3>
                                                                    </td>
                                                                    <td width="160px" style="text-align: center">
                                                                        @if(@$company->image)
                                                                            <img src="{{asset("storage/app/".@$company->image)}}" style="display: block; margin: 0px auto; width: 100px; height: 100px;border: 1px solid #eee;">
                                                                        @else
                                                                            <img src="{{asset("public/uploads/logo2.png")}}" style="display: block; margin: 0px auto; width: 100px; height: 100px;border: 1px solid #eee;">
                                                                        @endif
                                                                        <br>
                                                                        <h6 style="margin: 0px;">{{$company->name}}</h6>
                                                                    </td>
                                                                    <td width="10%"></td>
                                                                </tr>
                                                            </table>
                                                            <br><br><br>
                                                            <table>
                                                                <tr>
                                                                    <td width="10%"></td>
                                                                    <td>
                                                                        <table class="table table-bordered" style="">
                                                                            <tr>
                                                                                <td width="20%">إسم الموظف</td>
                                                                                <td>{{$employee->name}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>الإسم</td>
                                                                                <td>{{$custody->name}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>رقم العهدة</td>
                                                                                <td>{{$custody->reference}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>تاريخ التسليم</td>
                                                                                <td>{{$custody->thedate}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>العدد</td>
                                                                                <td>{{$custody->number}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>الوصف</td>
                                                                                <td style="max-height: 140px">
                                                                                    @if($custody->description and $custody->description!="")
                                                                                        <span>{{$custody->description}}</span>
                                                                                    @else
                                                                                        <span>لا يوجد</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>تأكيد الإستلام</td>
                                                                                <td>
                                                                                    @if($custody->delivered==0)
                                                                                        <span>لا</span>
                                                                                    @else
                                                                                        <span>لا</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="10%"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table>
                                                                <tr>
                                                                    <td width="65%"></td>
                                                                    <td>
                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <td class="text-center">إمضاء الموظف</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 100px"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="10%"></td>
                                                                </tr>
                                                            </table>
                                                            <br>
                                                            <table>
                                                                <tr>
                                                                    <td width="65%"></td>
                                                                    <td>
                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <td class="text-center">إمضاء المدير</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 140px"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="10%"></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>

                                            @if(permissions_hrm("add_custody"))
                                            <div class="row">
                                                <div class="col-sm-6 mt-30">
                                                    <h5>إضافة عهدة : </h5>
                                                    <form action="{{route("company.hrm.add_custody", $company->slug)}}" id="form_add_custody" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$employee->id}}">

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">رقم العهدة<span class="red">*</span></label>
                                                            <input type="number" class="form-control col-sm-6" name="reference" id="custody_reference" required autocomplete="off">
                                                            <button type="button" class="col-sm-2 btn btn-primary btn-block" id="custody_reference_btn">توليد تلقائي</button>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">إسم العهدة<span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8" name="name" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">تاريخ التسليم <span class="red">*</span></label>
                                                            <input type="text" class="form-control col-sm-8 datee" name="thedate" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">العدد <span class="red">*</span></label>
                                                            <input type="number" class="form-control col-sm-8" name="number" required autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الملف المرفق </label>
                                                            <input type="file" class="form-control col-sm-8" accept="image/*" name="file" autocomplete="off">
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-4">الوصف </label>
                                                            <textarea class="form-control col-sm-8" rows="3" name="description"  autocomplete="off"></textarea>
                                                        </div>

                                                        <div class="form-group row">
                                                            <div class="col-sm-4">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                    @endif

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <style>
        .panel-body{
            min-height: 1000px;
        }
        .boxino td{border: none !important;}
        .boxino{
            background: #277AB3;
            color: #fff;
            padding: 5px 5px 5px 5px;
            box-shadow: 3px 3px 2px #b5b5b5;
        }
        .boxino .table{
            margin-bottom: 0px;
        }
        .tab-content{
            padding-top: 15px;
        }
        .label-block{
            display: inline-block;
            width: 100%;
            border-radius: 0px;
            padding: 7px !important;
        }
        .formi{
            display: none;
            border: 1px solid #2d6a8d;
            padding: 15px 20px;
            margin: 15px auto;
        }
        .print{
            display: none;
        }
    </style>

    <script>
        $(function () {

            function calculate_salary(){
                var salary          = {{$salary}};
                var total_allowance = parseFloat($("#total_allowance").data("val")) || 0;
                var total_deduction = parseFloat($("#total_deduction").data("val")) || 0;
                var total_advance   = parseFloat($("#total_advance").data("val")) || 0;

                salary += total_allowance;
                salary -= total_deduction;
                salary -= total_advance;
                $("#final_salary").html(salary+" ريال");
            }

            function reloadish(){
                $('.datee').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true
                });
                calculate_salary();
            }
            reloadish();

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("click", ".nav-tabs li a", function (e) {
               e.preventDefault();
               var num = $(this).data("num");
               $.get("{{route("company.hrm.tabsnum", $company->slug)}}/"+num);
           });

            // Allowances

            $(document).on("change", "#allowance_category_id", function (e) {
                if(!$(this).val()){
                    $(this).closest("form").find("#allowance_type").val("");
                    $(this).closest("form").find("#allowance_value").val("");
                    return;
                }
                var type    = $(this).closest("form").find("option:selected").data("type");
                var value   = $(this).closest("form").find("option:selected").data("value");
                if(type==0){
                    $(this).closest("form").find("#allowance_value").val(value+" %");
                    $(this).closest("form").find("#allowance_type").val("نسبة من الراتب الأساسي");
                }else{
                    $(this).closest("form").find("#allowance_value").val(value+" ريال");
                    $(this).closest("form").find("#allowance_type").val("قيمة ثابتة");
                }
            });

            $(document).on("submit", "#form_add_allowance", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu1").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.add_allowance", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu1").load(window.location + " #menu1-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-allowance", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu1").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_allowance", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu1").load(window.location + " #menu1-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_allowance", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu1").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_allowance", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu1").load(window.location + " #menu1-content", function () {
                        reloadish();
                    });
                });
            });

            // Deductions

            $(document).on("change", "#deduction_category_id", function (e) {
                if(!$(this).val()){
                    $(this).closest("form").find("#deduction_type").val("");
                    $(this).closest("form").find("#deduction_value").val("");
                    $(this).closest("form").find("#final_deduction_value").val("");
                    return;
                }
                var type    = $(this).closest("form").find("option:selected").data("type");
                var value   = $(this).closest("form").find("option:selected").data("value");
                var salary  = {{$salary}};
                if(type==0){
                    $(this).closest("form").find("#deduction_value").val(value+" %");
                    $(this).closest("form").find("#deduction_type").val("نسبة من الراتب الأساسي");
                    $(this).closest("form").find("#final_deduction_value").val(parseFloat(salary/100*value));
                } else{

                    $(this).closest("form").find("#deduction_value").val(value+" ريال");
                    $(this).closest("form").find("#deduction_type").val("قيمة ثابتة");
                    $(this).closest("form").find("#final_deduction_value").val(value);
                }


            });

            $(document).on("submit", "#form_add_deduction", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu1").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.add_deduction", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu1").load(window.location + " #menu1-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-deduction", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu2").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_deduction", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu2").load(window.location + " #menu2-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_deduction", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu2").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_deduction", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu2").load(window.location + " #menu2-content", function () {
                        reloadish();
                    });
                });
            });

            // Advance

            $(document).on('change keyup', "#advance_amount",  function () {
                if ($(this).val()) {
                    $(this).closest("#form_add_advance").find("#advance_type").attr('readonly', false);
                    $(this).closest("#form_add_advance").find("#advance_monthly").attr('readonly', false);
                    payementmethod($(this).closest("#form_add_advance").find("#advance_type"));
                }
                else {
                    $(this).closest("#form_add_advance").find("#advance_type").attr('readonly', true);
                    $(this).closest("#form_add_advance").find("#advance_monthly").attr('readonly', true);
                }
            });
            
            $(document).on("change textInput input", "#advance_monthly", function () {
                pm = $(this).closest("#form_add_advance").find("#advance_type").val();
                av = $(this).closest("#form_add_advance").find("#advance_amount").val();
                mi = $(this).val();

                if (!mi || mi == 0 || !$.isNumeric(mi)) {
                    $(this).closest("#form_add_advance").find("#advance_months").val(0);
                    $(this).closest("#form_add_advance").find("#advance_monthly").val(0);
                    return;
                }
                if (pm == 2) {
                    if (av < eval(mi)) {
                        $(this).closest("#form_add_advance").find("#advance_monthly").val(0);
                    }
                    else {
                        var nummonths = Math.ceil(av / parseFloat(mi, 10));
                        $(this).closest("#form_add_advance").find("#advance_months").val(nummonths);
                    }
                }

            });

            $(document).on("change", "#advance_type", function () {
                payementmethod($(this));
            });

            function payementmethod(s) {
                pm = s.val();
                av = s.closest("#form_add_advance").find("#advance_amount").val();
                if (pm == 1) {
                    var res = av / 12;
                    s.closest("#form_add_advance").find("#advance_monthly").val(res.toFixed(2));
                    s.closest("#form_add_advance").find("#advance_months").val(12);
                }
                else if (pm == 3) {
                    /*var nummonths = 10;
                    var res = av / nummonths;
                    s.closest("#form_add_advance").find("#advance_monthly").val(res.toFixed(2));
                    s.closest("#form_add_advance").find("#advance_months").val(nummonths);
                    */
                }
                else if (pm == 2) {
                    if (s.closest("#form_add_advance").find("#advance_monthly").val() != 0) {
                        var res = av / s.closest("#form_add_advance").find("#advance_monthly").val();
                        s.closest("#form_add_advance").find("#advance_months").val(Math.ceil(res));
                    }
                    else {
                        s.closest("#form_add_advance").find("#advance_months").val(0);
                    }
                }
                else {
                    s.closest("#form_add_advance").find("#advance_monthly").val();
                    s.closest("#form_add_advance").find("#advance_months").val(0);
                }
            }

            $(document).on("submit", "#form_add_advance", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu3").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.add_advance", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu3").load(window.location + " #menu3-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-advance", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu3").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_advance", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu3").load(window.location + " #menu3-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("submit", "#form_update_advance", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu3").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_advance", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu3").load(window.location + " #menu3-content", function () {
                        reloadish();
                    });
                });
            });

            // Custody

            $(document).on("click", "#custody_reference_btn", function (e) {
                e.preventDefault();
                var text = "";
                var possible = "0123456789";
                for (var i = 0; i < 7; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                $("#custody_reference").val(text);
            });

            $(document).on("click", ".btn-delivered", function (e) {
               e.preventDefault();
               if(confirm('هل أنت متأكد ؟')){
                   var id = $(this).data("id");
                   $("#menu4").html($("#loading-wrapper").html());
                   $.get("{{route("company.hrm.delivered_custody", $company->slug)}}/"+id, function (data) {}).always(function() {
                       $("#menu4").load(window.location + " #menu4-content", function () {
                           reloadish();
                       });
                   });
               }
            });

            $(document).on("submit", ".form_update_custody", function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                $("#menu4").html($("#loading-wrapper").html());
                $.post("{{route("company.hrm.update_custody", $company->slug)}}", data, function (data) {}).always(function() {
                    $("#menu4").load(window.location + " #menu4-content", function () {
                        reloadish();
                    });
                });
            });

            $(document).on("click", ".btn-delete-custody", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $("#menu4").html($("#loading-wrapper").html());
                    $.get("{{route("company.hrm.delete_custody", $company->slug)}}/"+id, function (data) {}).always(function() {
                        $("#menu4").load(window.location + " #menu4-content", function () {
                            reloadish();
                        });
                    });
                }
            });

            $(document).on("click", ".btn-print-custody", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                print_custody(id);
            });


            function print_custody(id) {
                var headstr = "<html><head><title></title></head><body>";
                var footstr = "</body>";
                var newstr = document.all.item("custody_"+id).innerHTML;
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + newstr + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                return false;
            }



        });
    </script>

@endsection