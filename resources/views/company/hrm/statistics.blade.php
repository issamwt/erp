
@extends("company.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">


                    <section class="content-header">
                        <h1>نظام إدارة الموارد البشرية</h1>
                    </section>

                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12" style="margin-bottom: 40px; padding: 20px;">

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 mb-20">
                                        <div class="tiles purple added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> الوثائق الرسمية </div>
                                                <span class="fa fa-3x fa-check tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$documents}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description"><a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6-sm mb-20">
                                        <div class="tiles blue added-margin">

                                            <div class="tiles-body">
                                                <div class="tiles-title"> مجموع الموظفين </div>
                                                <span class="fa fa-3x fa-users tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$users}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description">
                                                    <a href="{{route("company.hrm.employees", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 mb-20">
                                        <div class="tiles red added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> مجموع الإدارات </div>
                                                <span class="fa fa-3x fa-chain tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$departments}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description">
                                                    <a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 mb-20">
                                        <div class="tiles purple added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> مجموع الأقسام </div>
                                                <span class="fa fa-3x fa-table tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="80" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description">
                                                    <a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6-sm mb-20">
                                        <div class="tiles blue added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> تصنيف الإجازات </div>
                                                <span class="fa fa-3x fa-files-o tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$vcategories}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description"><a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 mb-20">
                                        <div class="tiles green added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> تصنيف المخالفات </div>
                                                <span class="fa fa-3x fa-file-text tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$violations}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description"><a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6-sm mb-20">
                                        <div class="tiles purple added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> المسميات الوظيفية </div>
                                                <span class="fa fa-3x fa-list tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$designations}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description"><a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 mb-20">
                                        <div class="tiles red added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> أماكن العمل </div>
                                                <span class="fa fa-3x fa-location-arrow tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$locations}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description"><a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6-sm mb-20">
                                        <div class="tiles green added-margin">
                                            <div class="tiles-body">
                                                <div class="tiles-title"> بنود التقييم </div>
                                                <span class="fa fa-3x fa-star-o tiles-ico"></span>
                                                <div class="heading"> <span class="animate-number" data-value="{{$items}}" data-animation-duration="5000">0</span> </div>
                                                <hr class="bg-white">
                                                <div class="description"><a href="{{route("company.hrm.settings", $company->slug)}}"><i class="fa fa-arrow-circle-o-right"></i><span class="text-white mini-description ">&nbsp;المزيد </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-12" style="margin-bottom: 100px; padding: 20px;">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">الوثائق الرسمية</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            @foreach($documentss as $d)
                                                <div class="col-sm-3 text-center">
                                                    <a href="{{route("company.hrm.download_document", [$company->slug, $d->id])}}" style="height: 300px; border: 3px solid #333; display: block; color: #333; padding: 15px; border-radius:22px 22px 0px 0px;">
                                                        <i class="fa fa-file-o" style="font-size: 10em; margin-top: 10px"></i>
                                                        <h4 class="mt-20">{{$d->name}}</h4>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">المخالفات</div>
                                    <div class="panel-body">
                                        <table class="table table-bordered table-striped table-hovered">
                                            <thead>
                                            <tr>
                                                <th>م</th>
                                                <th>المخالفة</th>
                                                <th>العقوبة</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($violationss as $v)
                                                    <tr>
                                                        <td class="text-center" width="25px">{{$loop->iteration}}</td>
                                                        <td width="35%">{{$v->name}}</td>
                                                        <td>{{$v->punishment}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .spacing-bottom {
            margin-bottom: 30px;
        }
        .tiles.blue {
            background-color: #0090d9;
        }
        .tiles.added-margin {
            /* margin-right: -10px; */
            margin-left: -5px;
            margin-right: -5px;
        }
        .tiles {
            background-color: #bcbcbc;
            color: #ffffff;
            position: relative;
        }
        .tiles .tiles-body {
            padding: 19px 18px 15px 24px;
            overflow: hidden;
        }
        .tiles .tiles-title {
            font-size: 14px;
            letter-spacing: 0.5px;
            font-weight: 600;
        }
        .tiles-ico {
            position: absolute;
            left: 10px !important;
            right: auto;
            top: 30%;
            color: rgba(0, 0, 0, 0.2);
            -webkit-transition: all 200ms ease-in;
            -ms-transition: all 200ms ease-in;
            -moz-transition: all 200ms ease-in;
            transition: all 200ms ease-in;
        }
        .tiles > .tiles-body:hover .tiles-ico {
            font-size: 500%;
        }
        .tiles .heading {
            font-size: 25px;
            display: block;
            font-family: 'Open Sans';
            font-weight: 600;
            /* margin: 4px 0px; */
            margin: 10px 0px;
        }
        .tiles hr {
            margin: 10px 0px;
            height: 1px;
            border: none;
            background-color: #2b3038;
        }
        .bg-white:not(i) {
            background-color: #fff !important;
            color: black !important;
        }
        .tiles > .tiles-body > .description {
            float: left;
        }
        .tiles > .tiles-body > .description {
            font-size: 12px;
            display: block;
            color: #ffffff;
            display: table-cell;
            vertical-align: middle;
            -webkit-font-smoothing: antialiased;
        }
        .tiles > .tiles-body > .description a {
            color: rgba(255, 255, 255, 0.6);
            font-size: 16px;
        }
        .tiles .description .mini-description {
            position: relative;
            top: -5px;
        }

        .tiles.red {
            background-color: #f35958;
        }
        .tiles.red .button {
            background: #bf3938;
            color: #f7bebe;
        }
        .tiles.purple {
            background-color: #735f87;
        }
        .tiles.purple .button {
            background: #736086;
            color: #d7d5d7;
        }
        .tiles.blue {
            background-color: #0090d9;
        }
        .tiles.green {
            background-color: #0aa699;
        }
        .tiles.black {
            background-color: #22262e;
        }
        .tiles.black .blend {
            color: #8b91a0;
        }
        .tiles.black input {
            background-color: rgba(0, 0, 0, 0.35);
            border: 0;
        }
        .tiles.dark-blue {
            background-color: #365d98;
        }
        .tiles.light-blue {
            background-color: #00abea;
        }
        .tiles.light-red {
            background-color: #f96773;
        }
        .tiles.grey {
            background-color: #e9ecee;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-animateNumber/0.0.14/jquery.animateNumber.min.js"></script>
    <script>
        $(function () {
            $('.animate-number').each(function () {
                $(this).animateNumber({number : $(this).attr("data-value")});
            });
        });
    </script>

@endsection