@extends("company.layouts.app", ["title"=>"تفاصيل الموظف"])

@section("content")

    <section class="content-header">
        <h1>
            نظام إدارة شؤون الموظفين
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>عرض الموظف</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("company.hrm.employees", @$company->slug)}}"><i class="fa fa-list"></i> <span>قائمة الموظفين</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                @if($employee->image)
                                    <img src="{{asset("storage/app/".$employee->image)}}" style="width: 150px; height: 150px; border-radius: 50%; border: 1px solid #eee; margin: 10px auto; display: block;">
                                @else
                                    <img src="{{asset("public/noimage.png")}}" style="width: 150px; height: 150px; border-radius: 50%; border: 1px solid #eee; margin: 10px auto; display: block;">
                                @endif
                                <h4 class="mt-20 text-center">{{@$employee->name}}</h4>
                                <h5 class="mt-10 text-center" style="color: #000;"><i class="fa fa-at"></i> {{@$employee->email}}</h5>
                                <h5 class="mt-10 text-center" style="color: #000;"><i class="fa fa-phone"></i> {{@$employee->phone}}</h5>

                            </div>

                            <div class="col-sm-12">

                                <div class="col-sm-12 mt-20 mb-20"><hr></div>

                                <div class="col-sm-6">

                                    <h5 class="mt-20 mb-20"><i class="fa fa-lock"></i> <span>بيانات الحساب :</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="email">البريد الإلكتروني </label>
                                            <div class="col-sm-8">{{@$employee->email}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="phone">رقم الجوال </label>
                                            <div class="col-sm-8">{{@$employee->phone}}</div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <h5 class="mt-50 mb-20"><i class="fa fa-user"></i> <span>التفاصيل الشخصية</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="birthday">تاريخ الولادة </label>
                                            <div class="col-sm-4">{{@$employee->details->birthday}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="gender">الجنس </label>
                                            <div class="col-sm-8">
                                                @if($employee->gender=="male")
                                                    <span>ذكر</span>
                                                @else
                                                    <span>أنثى</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="nationality">الجنسية </label>
                                            <div class="col-sm-8">{{@$employee->details->country->name}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="address">العنوان الحالي </label>
                                            <div class="col-sm-8">{{@$employee->details->address}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="qualification">المؤهل العلمي </label>
                                            <div class="col-sm-8">{{@$employee->details->qualification}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="num_passport">رقم جواز السفر </label>
                                            <div class="col-sm-8">{{@$employee->details->num_passport}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="date_passport">تاريخ نهاية جواز السفر </label>
                                            <div class="col-sm-4">{{@$employee->details->date_passport}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="num_identity">رقم الهوية </label>
                                            <div class="col-sm-8">{{@$employee->details->num_identity}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="date_identity">تاريخ نهاية الهوية </label>
                                            <div class="col-sm-4">{{@$employee->details->date_identity}}</div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <h5 class="mt-50 mb-20"><i class="fa fa-users"></i> <span>العائلة</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="social_status">الحالة الاجتماعية </label>
                                            <div class="col-sm-8">
                                                @switch(@$employee->details->social_status)
                                                    @case("married") <span>متزوج</span> @break
                                                    @case("celibate") <span>أعزب</span> @break
                                                    @case("widowed") <span>أرمل</span> @break
                                                    @case("divorcee") <span>مطلق</span> @break
                                                    @default <span>غير محدد</span> @break
                                                @endswitch
                                            </div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="partner_name">إسم الشريك </label>
                                            <div class="col-sm-8">{{@$employee->details->partner_name}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="partner_birthday">تاريخ ميلاد الشريك </label>
                                            <div class="col-sm-8">{{@$employee->details->partner_birthday}}</div>
                                        </div>
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4">التابعين</label>
                                            <div class="col-sm-8">
                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>إسم الإبن / الإبنة</th>
                                                        <th>تاريخ الميلاد</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $son_name = explode(";", @$employee->details->son_name); $son_birthday = explode(";", @$employee->details->son_birthday); ?>
                                                    @for($i=0; $i<=4;$i++)
                                                        @if(isset($son_name[$i]) and $son_name[$i]!="")
                                                            <tr>
                                                                <td width="60%">{{$son_name[$i]}}</td>
                                                                <td>{{$son_birthday[$i]}}</td>
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6">
                                    <h5 class="mt-20 mb-20"><i class="fa fa-suitcase"></i> <span>الصفة الرسمية</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="status"> الحالة </label>
                                            <div class="col-sm-8">
                                                @if($employee->status==1)
                                                    <span>فعال</span>
                                                @elseif($employee->status==0)
                                                    <span>موقوف</span>
                                                @else
                                                    <span>في تجربة</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="department_id"> الإدارة </label>
                                            <div class="col-sm-8">{{@$employee->details->department->name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="section_id"> القسم </label>
                                            <div class="col-sm-8" id="section_id_wrapper"><div class="col-sm-8">{{@$employee->details->section->name}}</div></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="direct_manager_id"> المدير المباشر </label>
                                            <div class="col-sm-8">{{@$employee->details->manager->name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="joining_date"> تاريخ التعيين </label>
                                            <div class="col-sm-4">{{@$employee->details->joining_date}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="retirement_date"> تاريخ إنتهاء العقد </label>
                                            <div class="col-sm-4">{{@$employee->details->retirement_date}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="job_time"> نوع الدوام </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->job_time=="Full")
                                                    <span>كامل</span>
                                                @elseif(@$employee->details->job_time=="Part")
                                                    <span>جزئي</span>
                                                @else
                                                    <span>غير محدد</span> <span class="red">*</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="job_location_id"> موقع العمل </label>
                                            <div class="col-sm-8">{{@$employee->details->job_location->name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="role_id"> الدور </label>
                                            <div class="col-sm-8">{{@$employee->role->name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="designation_id"> المسمى الوظيفي </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->designation)
                                                    {{@$employee->details->designation->name}}
                                                @else
                                                    <span>غير محدد</span> <span class="red">*</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="salary"> الراتب </label>
                                            <div class="col-sm-4"><span>{{@$employee->details->salary}}</span> <span>ريال</span></div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <h5 class="mt-50 mb-20"><i class="fa fa-bank"></i> <span>البيانات البنكية</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="bank_name"> اسم البنك </label>
                                            <div class="col-sm-8">{{@$employee->details->bank_name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="branch_name"> اسم الفرع </label>
                                            <div class="col-sm-8">{{@$employee->details->bank_name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="account_name"> إسم الحساب </label>
                                            <div class="col-sm-8">{{@$employee->details->account_name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="account_number"> رقم الحساب </label>
                                            <div class="col-sm-8">{{@$employee->details->account_number}}</div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <h5 class="mt-50 mb-20"><i class="fa fa-files"></i> <span>الوثائق المرفقة</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="cin_photo_path"> صورة بطاقة الهوية </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->cin_photo_path)
                                                    <a href="{{asset("storage/app/".$employee->details->cin_photo_path)}}" target="_blank">تحميل الملف</a>
                                                @else
                                                    <span>لا يوجد ملف</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="passport_photo_path"> صورة جواز السفر </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->passport_photo_path)
                                                    <a href="{{asset("storage/app/".$employee->details->passport_photo_path)}}" target="_blank">تحميل الملف</a>
                                                @else
                                                    <span>لا يوجد ملف</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="resume_path"> سيرة ذاتية </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->resume_path)
                                                    <a href="{{asset("storage/app/".$employee->details->resume_path)}}" target="_blank">تحميل الملف</a>
                                                @else
                                                    <span>لا يوجد ملف</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="contract_paper_path"> ورقة العقد </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->contract_paper_path)
                                                    <a href="{{asset("storage/app/".$employee->details->contract_paper_path)}}" target="_blank">تحميل الملف</a>
                                                @else
                                                    <span>لا يوجد ملف</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4" for="other_document_path"> وثيقة أخرى </label>
                                            <div class="col-sm-8">
                                                @if(@$employee->details->other_document_path)
                                                    <a href="{{asset("storage/app/".$employee->details->other_document_path)}}" target="_blank">تحميل الملف</a>
                                                @else
                                                    <span>لا يوجد ملف</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <h5 class="mt-50 mb-20"><i class="fa fa-h-square"></i> <span>التأمين الطبي</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="medical">نوع التأمين الطبي </label>
                                            <div class="col-sm-8">{{@$employee->details->medicall->name}}</div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <h5 class="mt-50 mb-20"><i class="fa fa-gears"></i> <span>الضمان الإجتماعي</span></h5>
                                    <div class="col-sm-12">
                                        <div class="form-group mt-10 row">
                                            <label class="col-sm-4" for="social">نوع الضمان الإجتماعي </label>
                                            <div class="col-sm-8">{{@$employee->details->sociall->name}}</div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section("scripts")

<style>
    .panel-body h5{
        color: #337ab7;
    }
</style>

@endsection