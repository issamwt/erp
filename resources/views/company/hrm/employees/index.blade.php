@extends("company.layouts.app", ["title"=>"قائمة الموظفين"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>قائمة الموظفين</strong>
                        @if(permissions_hrm("add_employee"))
                            <a href="{{route("company.hrm.add_employee", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-plus"></i> <span>إضافة موظف</span></a>
                        @endif
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#finances_filter0').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                <form id="finances_filter0" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control names" autocomplete="off" name="from" placeholder="الإسم">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x roles" autocomplete="off">
                                            <option value="0">كل المسميات</option>
                                            <option value="1">أدمن</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x designations" autocomplete="off">
                                            <option value="0">كل المسميات الوظيفية</option>
                                            @foreach($designations as $designation)
                                                <option value="{{$designation->id}}">{{$designation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x departments" autocomplete="off">
                                            <option value="0">كل الإدارات</option>
                                            @foreach($departments as $department)
                                                <option value="{{$department->id}}">{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x job_times" autocomplete="off">
                                            <option value="0">كل أنواع الدوام</option>
                                            <option value="Full">كامل</option>
                                            <option value="Part">جزئي</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x statuses" autocomplete="off">
                                            <option value="all">كل الحالات</option>
                                            <option value="1">فعال</option>
                                            <option value="0">موقوف</option>
                                            <option value="2">في تجربة</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block">فلتر</button>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="reset" class="btn btn-info btn-block">مسح</button>
                                    </div>

                                    <div class="clearfix"></div>

                                </form>
                                <div class="clearfix"></div>
                                <div class="clearfix"><hr></div>

                            </div>

                            <div class="col-sm-12">
                                <table class="table table-hover table-bordered"  id="tableEmployees">
                                    <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>الصورة</th>
                                        <th>الإسم</th>
                                        <th>المسمى</th>
                                        <th>المسمى الوظيفي</th>
                                        <th>التعيين</th>
                                        <th>نوع الدوام</th>
                                        <th>الحالة</th>
                                        <th>الأوامر</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr data-name0="{{$user->name}}" data-role0="{{@$user->role_id}}" data-designation0="{{@$user->details->designation_id}}"
                                            data-department0="{{@$user->details->department_id}}" data-job_time0="{{@$user->details->job_time}}" data-statuse0="{{$user->status}}">
                                                <td width="20px">{{$loop->iteration}}</td>
                                                <td>
                                                    @if($user->image)
                                                        <img src="{{asset("storage/app/".$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                                    @else
                                                        <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                                    @endif
                                                </td>
                                                <td>{{$user->name}}</td>
                                                <td style="vertical-align: middle;">{{@$user->role->name}}</td>
                                                <td style="vertical-align: middle;">
                                                    @if(@$user->details->designation)
                                                        {{@$user->details->designation->name}}
                                                    @else
                                                        <span>غير محدد</span> <span class="red">*</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{(@$user->details->department)?@$user->details->department->name:"لا يتبع إدارة"}}<br>
                                                    {{(@$user->details->section)?@$user->details->section->name:"لا يتبع قسم"}}
                                                </td>
                                                <td>
                                                    @if(@$user->details->job_time=="Full")
                                                        <span>كامل</span>
                                                    @elseif(@$user->details->job_time=="Part")
                                                        <span>جزئي</span>
                                                    @else
                                                        <span>غير محدد</span> <span class="red">*</span>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    @if($user->status==1)
                                                        <strong class="alert alert-success alert-xs" style="width: 90%;">فعال</strong>
                                                    @elseif($user->status==0)
                                                        <strong class="alert alert-danger alert-xs" style="width: 90%;">موقوف</strong>
                                                    @else
                                                        <strong class="alert alert-primary alert-xs" style="width: 90%;">في تجربة</strong>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;" class="text-center">
                                                    @if(permissions_hrm("show_employee"))
                                                        <a class="btn btn-success btn-xs" href="{{route("company.hrm.employee", [@$company->slug, $user->id])}}" data-toggle="tooltip" title="التفاصيل الشخصية"><i class="fa fa-eye"></i></a>
                                                    @else
                                                        <a class="btn btn-success btn-xs disabled" disabled href="javascript:void(0)" data-toggle="tooltip" title="التفاصيل الشخصية"><i class="fa fa-eye"></i></a>
                                                    @endif

                                                    @if(permissions_hrm("update_employee"))
                                                        <a class="btn btn-info btn-xs" href="{{route("company.hrm.edit_employee", [@$company->slug, $user->id])}}" data-toggle="tooltip" title="التعديل"><i class="fa fa-edit"></i></a>
                                                    @else
                                                        <a class="btn btn-info btn-xs disabled" disabled href="javascript:void(0)" data-toggle="tooltip" title="التعديل"><i class="fa fa-edit"></i></a>
                                                    @endif

                                                    @if(permissions_hrm("delete_employee"))
                                                        <a class="btn btn-danger btn-xs" onclick="return confirm('هل أنت متأكد ؟')" href="{{route("company.hrm.delete_employee", [@$company->slug, $user->id])}}" data-toggle="tooltip" title="الحذف"><i class="fa fa-remove"></i></a>
                                                    @else
                                                        <a class="btn btn-danger btn-xs disabled" disabled href="javascript:void(0)" data-toggle="tooltip" title="الحذف"><i class="fa fa-remove"></i></a>
                                                    @endif

                                                    @if(permissions_hrm("finances"))
                                                        <a class="btn btn-primary btn-xs" href="{{route("company.hrm.finances", [@$company->slug, $user->id])}}" data-toggle="tooltip" title="الشؤون المالية"><i class="fa fa-money"></i></a>
                                                    @else
                                                        <a class="btn btn-primary btn-xs disabled" disabled href="javascript:void(0)" data-toggle="tooltip" title="الشؤون المالية"><i class="fa fa-money"></i></a>
                                                    @endif

                                                    @if(permissions_hrm("adminisitrative_affairs"))
                                                        <a class="btn btn-warning btn-xs" href="{{route("company.hrm.adminisitrative_affairs", [@$company->slug, $user->id])}}" data-toggle="tooltip" title="الشؤون الإدارية"><i class="fa fa-briefcase"></i></a>
                                                    @else
                                                        <a class="btn btn-warning btn-xs disabled" disabled href="javascript:void(0)" data-toggle="tooltip" title="الشؤون الإدارية"><i class="fa fa-briefcase"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <script>
        $(function () {

            $(document).on("submit", "#finances_filter0", function (e){
                var names   = $("#finances_filter0 .names").val();
                var roles   = $("#finances_filter0 .roles").select2('val')+"";
                var designations = $("#finances_filter0 .designations").select2('val')+"";
                var departments = $("#finances_filter0 .departments").select2('val')+"";
                var job_times = $("#finances_filter0 .job_times").select2('val')+"";
                var statuses = $("#finances_filter0 .statuses").select2('val')+"";
                console.log("-----");console.log(statuses);console.log("-----");

                $('#tableEmployees tbody tr').hide();
                $('#tableEmployees tbody tr').filter(function () {
                    var namex = true;
                    var name0 = $(this).data("name0");
                    if(names!=""){
                        if(!name0.includes(names))
                            namex = false;
                    }

                    var rolex = true;
                    var role0 = $(this).data("role0");
                    if(roles!="0"){
                        if(role0!=roles)
                            rolex = false;
                    }

                    var designationx = true;
                    var designation0 = $(this).data("designation0");
                    if(designations!="0"){
                        if(designation0!=designations)
                            designationx = false;
                    }

                    var departmentx = true;
                    var department0 = $(this).data("department0");
                    if(departments!="0"){
                        if(department0!=departments)
                            departmentx = false;
                    }

                    var job_timex = true;
                    var job_time0 = $(this).data("job_time0");
                    if(job_times!="0"){
                        if(job_time0!=job_times)
                            job_timex = false;
                    }

                    var statusex = true;
                    var statuse0 = $(this).data("statuse0");
                    if(statuses!="all"){
                        if(statuse0!=statuses)
                            statusex = false;
                    }
                    console.log(statuse0);
                    console.log(statusex);

                    return (namex && rolex && designationx && departmentx && job_timex && statusex);
                    
                }).show();
            });

            $(document).on("reset", "#finances_filter0", function (e){
                $('#tableEmployees tbody tr').show();
                $("#finances_filter0 .names").val("");
                $('#finances_filter0 .roles').val(0).trigger('change');
                $('#finances_filter0 .designations').val(0).trigger('change');
                $('#finances_filter0 .departments').val(0).trigger('change');
                $('#finances_filter0 .job_times').val(0).trigger('change');
                $('#finances_filter0 .statuses').val("all").trigger('change');
            });

        });
    </script>

@endsection