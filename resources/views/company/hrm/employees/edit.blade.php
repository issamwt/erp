@extends("company.layouts.app", ["title"=>"تعديل الموظف"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة شؤون الموظفين</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>تعديل الموظف</strong>
                        <a href="{{route("company.hrm.employees", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>قائمة الموظفين</span></a>
                    </div>
                    <div class="panel-body">

                        <form action="{{route("company.hrm.update_employee", @$company->slug)}}" class="row" id="add_employee_form" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$employee->id}}">

                            <div class="col-sm-6">

                                <h5 class="mt-20 mb-20"><i class="fa fa-lock"></i> <span>بيانات الحساب :</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="email">البريد الإلكتروني <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="email" value="{{$employee->email}}" name="email" id="email" class="form-control" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="phone">رقم الجوال <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{$employee->phone}}" name="phone" id="phone" class="form-control" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="password">كلمة المرور </label>
                                        <div class="col-sm-8">
                                            <input type="password" name="password" id="password" class="form-control" autocomplete="off">
                                            <small>إذا لم تكن تريد تغيير كلمة المرور دع هذا الحقل فارغا.</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <h5 class="mt-50 mb-20"><i class="fa fa-user"></i> <span>التفاصيل الشخصية</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="name">الإسم <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{$employee->name}}" name="name" id="name" class="form-control" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="birthday">تاريخ الولادة <span class="red">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" value="{{@$employee->details->birthday}}" name="birthday" id="birthday" class="form-control datee" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="gender">الجنس <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="gender" id="gender" class="form-control select2x" required="" autocomplete="off">
                                                <option value="">إختر</option>
                                                <option value="male" @if(@$employee->details->gender=="male") selected @endif>ذكر</option>
                                                <option value="female" @if(@$employee->details->gender=="female") selected @endif>أنثى</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="nationality">الجنسية <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="nationality" id="nationality" class="form-control select2x" required="" autocomplete="off">
                                                <option value="">إختر</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}" @if(@$employee->details->nationality==$country->id) selected @endif>{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="address">العنوان الحالي <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->address}}" name="address" id="address" class="form-control" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="qualification">المؤهل العلمي <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->qualification}}" name="qualification" id="qualification" class="form-control" required="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="num_passport">رقم جواز السفر </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->num_passport}}" name="num_passport" id="num_passport" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="date_passport">تاريخ نهاية جواز السفر </label>
                                        <div class="col-sm-4">
                                            <input type="text" value="{{@$employee->details->date_passport}}" name="date_passport" id="date_passport" class="form-control datee" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="num_identity">رقم الهوية </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->num_identity}}" name="num_identity" id="num_identity" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="date_identity">تاريخ نهاية الهوية </label>
                                        <div class="col-sm-4">
                                            <input type="text" value="{{@$employee->details->date_identity}}" name="date_identity" id="date_identity" class="form-control datee" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="image">الصورة </label>
                                        <div class="col-sm-8">
                                            @if($employee->image)
                                                <img src="{{asset("storage/app/".$employee->image)}}" style="width: 100px; height: 100px; border-radius: 1px; border: 1px solid #d2d6de; margin-bottom: 10px; display: block;">
                                            @else
                                                <img src="{{asset("public/noimage.png")}}" style="width: 100px; height: 100px; border-radius: 1px; border: 1px solid #d2d6de; margin-bottom: 10px; display: block;">
                                            @endif
                                            <input type="file" value="" name="image" accept="image/*" id="image" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <h5 class="mt-50 mb-20"><i class="fa fa-users"></i> <span>العائلة</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="social_status">الحالة الاجتماعية <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="social_status" id="social_status" class="form-control select2x" required="" autocomplete="off">
                                                <option value="">إختر</option>
                                                <option value="married" @if(@$employee->details->social_status=="married") selected @endif>متزوج</option>
                                                <option value="celibate" @if(@$employee->details->social_status=="celibate") selected @endif>أعزب</option>
                                                <option value="widowed" @if(@$employee->details->social_status=="widowed") selected @endif>أرمل</option>
                                                <option value="divorcee" @if(@$employee->details->social_status=="divorcee") selected @endif>مطلق</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="partner_name">إسم الشريك </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->partner_name}}" name="partner_name" id="partner_name" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="partner_birthday">تاريخ ميلاد الشريك </label>
                                        <div class="col-sm-4">
                                            <input type="text" value="{{@$employee->details->partner_birthday}}" name="partner_birthday" id="partner_birthday" class="form-control datee" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4">التابعين</label>
                                        <div class="col-sm-8">
                                            <button type="button" class="btn btn-success btn-ooz">إظهار / إخفاء</button>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="ooz">
                                        <div class="col-sm-4 mt-10"></div>
                                        <div class="col-sm-8" style="padding: 0px">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>إسم الإبن / الإبنة</th>
                                                    <th>تاريخ الميلاد</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $son_name = explode(";", @$employee->details->son_name); $son_birthday = explode(";", @$employee->details->son_birthday); ?>
                                                @for($i=0; $i<=4;$i++)
                                                    <tr>
                                                        <td width="60%">
                                                            <input type="text" value="{{@$son_name[$i]}}" name="son_name[]" class="form-control" autocomplete="off">
                                                        </td>
                                                        <td>
                                                            <input type="text" value="{{@$son_birthday[$i]}}" name="son_birthday[]" class="form-control datee" autocomplete="off">
                                                        </td>
                                                    </tr>
                                                @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <h5 class="mt-20 mb-20"><i class="fa fa-suitcase"></i> <span>الصفة الرسمية</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="status"> الحالة <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="status" id="status" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                <option value="1" @if($employee->status==1) selected @endif>فعال</option>
                                                <option value="0" @if($employee->status==0) selected @endif>موقوف</option>
                                                <option value="2" @if($employee->status==2) selected @endif>في تجربة</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="department_id"> الإدارة <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="department_id" id="department_id" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                @foreach($departments as $dep)
                                                    <option value="{{$dep->id}}" @if(@$employee->details->department_id==$dep->id) selected @endif>{{$dep->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="section_id"> القسم <span class="red">*</span></label>
                                        <div class="col-sm-8" id="section_id_wrapper">
                                            <select name="section_id" id="section_id" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                <option value="0">لا يوجد قسم</option>
                                            </select>
                                            <i class="fa fa-refresh fa-spin fa-2x fa-fw" style="display: none; color:#337ab7;"></i>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="direct_manager_id"> المدير المباشر <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="direct_manager_id" id="direct_manager_id" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}" @if(@$employee->details->direct_manager_id==$user->id) selected @endif>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="joining_date"> تاريخ التعيين <span class="red">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" value="{{@$employee->details->joining_date}}" class="form-control datee" name="joining_date" id="joining_date" required autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="retirement_date"> تاريخ إنتهاء العقد </label>
                                        <div class="col-sm-4">
                                            <input type="text" value="{{@$employee->details->retirement_date}}" class="form-control datee" name="retirement_date" id="retirement_date" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="job_time"> نوع الدوام <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="job_time" id="job_time" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                <option value="Full" @if(@$employee->details->job_time=="Full") selected @endif>كامل</option>
                                                <option value="Part" @if(@$employee->details->job_time=="Part") selected @endif>جزئي</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="job_location_id"> موقع العمل <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="job_location_id" id="job_location_id" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                @foreach($locations as $location)
                                                    <option value="{{$location->id}}" @if(@$employee->details->job_location_id==$location->id) selected @endif>{{$location->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="role_id"> الدور <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="role_id" id="role_id" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                <option value="1" @if($employee->role_id==1) selected @endif>الأدمن</option>
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}" @if($employee->role_id==$role->id) selected @endif>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="designation_id"> المسمى الوظيفي <span class="red">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="designation_id" id="designation_id" class="form-control select2x" required="">
                                                <option value="">إختر</option>
                                                @foreach($designations as $designation)
                                                    <option value="{{$designation->id}}" @if(@$employee->details->designation_id==$designation->id) selected @endif>{{$designation->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="salary"> الراتب <span class="red">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="number" value="{{@$employee->details->salary}}" class="form-control" name="salary" id="salary" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <h5 class="mt-50 mb-20"><i class="fa fa-bank"></i> <span>البيانات البنكية</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="bank_name"> اسم البنك </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->bank_name}}" class="form-control" name="bank_name" id="bank_name" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="branch_name"> اسم الفرع </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->branch_name}}" class="form-control" name="branch_name" id="branch_name" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="account_name"> إسم الحساب </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->account_name}}" class="form-control" name="account_name" id="account_name" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="account_number"> رقم الحساب </label>
                                        <div class="col-sm-8">
                                            <input type="text" value="{{@$employee->details->account_number}}" class="form-control" name="account_number" id="account_number" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <h5 class="mt-50 mb-20"><i class="fa fa-files"></i> <span>الوثائق المرفقة</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="cin_photo_path"> صورة بطاقة الهوية </label>
                                        <div class="col-sm-8">
                                            <input type="file" accept="image/*" class="form-control" name="cin_photo_path" id="cin_photo_path" autocomplete="off">
                                            <br>
                                            @if(@$employee->details->cin_photo_path)
                                                <a href="{{asset("storage/app/".$employee->details->cin_photo_path)}}" target="_blank">تحميل الملف</a>
                                            @else
                                                <span>لا يوجد ملف</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="passport_photo_path"> صورة جواز السفر </label>
                                        <div class="col-sm-8">
                                            <input type="file" accept="image/*" class="form-control" name="passport_photo_path" id="passport_photo_path" autocomplete="off">
                                            <br>
                                            @if(@$employee->details->passport_photo_path)
                                                <a href="{{asset("storage/app/".$employee->details->passport_photo_path)}}" target="_blank">تحميل الملف</a>
                                            @else
                                                <span>لا يوجد ملف</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="resume_path"> سيرة ذاتية </label>
                                        <div class="col-sm-8">
                                            <input type="file" accept="image/*" class="form-control" name="resume_path" id="resume_path" autocomplete="off">
                                            <br>
                                            @if(@$employee->details->resume_path)
                                                <a href="{{asset("storage/app/".$employee->details->resume_path)}}" target="_blank">تحميل الملف</a>
                                            @else
                                                <span>لا يوجد ملف</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="contract_paper_path"> ورقة العقد </label>
                                        <div class="col-sm-8">
                                            <input type="file" accept="image/*" class="form-control" name="contract_paper_path" id="contract_paper_path" autocomplete="off">
                                            <br>
                                            @if(@$employee->details->contract_paper_path)
                                                <a href="{{asset("storage/app/".$employee->details->contract_paper_path)}}" target="_blank">تحميل الملف</a>
                                            @else
                                                <span>لا يوجد ملف</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4" for="other_document_path"> وثيقة أخرى </label>
                                        <div class="col-sm-8">
                                            <input type="file" accept="image/*" class="form-control" name="other_document_path" id="other_document_path" autocomplete="off">
                                            <br>
                                            @if(@$employee->details->other_document_path)
                                                <a href="{{asset("storage/app/".$employee->details->other_document_path)}}" target="_blank">تحميل الملف</a>
                                            @else
                                                <span>لا يوجد ملف</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <h5 class="mt-50 mb-20"><i class="fa fa-h-square"></i> <span>التأمين الطبي</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="medical">نوع التأمين الطبي </label>
                                        <div class="col-sm-8">
                                            <select name="medical" id="medical" class="form-control select2x" autocomplete="off">
                                                <option value="">إختر</option>
                                                <option value="0">لا يوجد تأمين</option>
                                                @foreach($medical_categories as $medical)
                                                    <option value="{{$medical->id}}" @if(@$employee->details->medical==$medical->id) selected @endif>{{$medical->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <h5 class="mt-50 mb-20"><i class="fa fa-gears"></i> <span>الضمان الإجتماعي</span></h5>
                                <div class="col-sm-12">
                                    <div class="form-group mt-10 row">
                                        <label class="col-sm-4" for="social">نوع الضمان الإجتماعي </label>
                                        <div class="col-sm-8">
                                            <select name="social" id="social" class="form-control select2x" autocomplete="off">
                                                <option value="">إختر</option>
                                                <option value="0">لا يوجد الضمان</option>
                                                @foreach($social_categories as $social)
                                                    <option value="{{$social->id}}" @if(@$employee->details->social==$social->id) selected @endif>{{$social->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 text-center mt-50 mb-20">
                                <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .datepicker {
            direction: rtl;
        }
        .datepicker.dropdown-menu {
            right: initial;
        }
        #ooz{
            display: none;
        }
        .panel-body h5{
            color: #337ab7;
        }
        form-control.error{
            border-color: crimson;
        }
        label.error{
            color: crimson;
            font-size: 0.8em;
        }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/localization/messages_ar.min.js"></script>

    <script>
        $(function () {

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on("click", ".btn-ooz", function (e) {
                e.preventDefault();
                $("#ooz").slideToggle();
            });

            $(document).on("change.select2", "#department_id", function (e) {
                var dep_id = $(this).val();
                $("#section_id_wrapper .fa").css("display", "block");
                $("#section_id_wrapper #section_id, #section_id_wrapper .select2").css("display", "none");
                $.post("{{route("company.hrm.get_sects_by_dep", $company->slug)}}", { _token:"{{csrf_token()}}", dep_id:dep_id }, function (data) {
                    $("#section_id").html(data);
                    $("#section_id").select2({dir: "rtl", language: "ar", placeholder : "إختر"});
                    $("#section_id_wrapper .fa").css("display", "none");
                    $("#section_id_wrapper #section_id, #section_id_wrapper .select2").css("display", "block");
                });
            });

            @if(@$employee->details->department_id)
            function getsec(){
                var dep_id = {{@$employee->details->department_id}};
                var sect_id = {{@$employee->details->section_id}};
                $("#section_id_wrapper .fa").css("display", "block");
                $("#section_id_wrapper #section_id, #section_id_wrapper .select2").css("display", "none");
                $.post("{{route("company.hrm.get_sects_by_dep", $company->slug)}}", { _token:"{{csrf_token()}}", dep_id:dep_id }, function (data) {
                    $("#section_id").html(data);
                    $("#section_id").select2({dir: "rtl", language: "ar", placeholder : "إختر"});
                    $("#section_id_wrapper .fa").css("display", "none");
                    $("#section_id_wrapper #section_id, #section_id_wrapper .select2").css("display", "block");
                    @if(@$employee->details->section_id)
                        $("#section_id").val(sect_id);
                        $("#section_id").trigger('change');
                    @endif
                });
            }
            getsec();
            @endif

            $("#add_employee_form").validate();

        })
    </script>

@endsection