@extends("company.layouts.app", ["title"=>"المسائلات"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة الموارد البشرية</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="panel panel-primary">
                        <div class="panel-heading">المسائلات</div>
                        <div class="panel-body">

                            @if(permissions_hrm("send_accounting"))
                                <form action="{{route("company.hrm.send_accounting", $company->slug)}}" method="post" class="mb-20">
                                    @csrf
                                    <input type="hidden" name="status" value="0">

                                    <h4>إرسال مسائلة لموظف :</h4>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>الموظف</label>
                                                <select class="form-control select2x" required name="user_to">
                                                    <option value=""></option>
                                                    @foreach($users as $user)
                                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>نص المسائلة</label>
                                                <textarea class="form-control" rows="4" required name="message1"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-reply"></i> <span>إرسال</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endif

                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>التاريخ</th>
                                        <th>المرسل</th>
                                        <th>المرسل إليه</th>
                                        <th>الرسالة</th>
                                        <th>رد الموظف</th>
                                        <th>القرار النهائي</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($accountings))
                                        @foreach($accountings as $acc)
                                            <tr>
                                                <td class="text-center">{{$loop->iteration}}</td>
                                                <td>{{@$acc->created_at->format("Y-m-d")}}</td>
                                                <td>{{@$acc->from->name}}</td>
                                                <td>{{@$acc->to->name}}</td>
                                                <td width="20%" class="text-justify">{{$acc->message1}}</td>
                                                <td width="20%" class="text-justify">{{$acc->message2}}</td>
                                                <td width="20%" class="text-justify">{{$acc->message3}}</td>
                                            </tr>
                                            @if($acc->status==0 and $acc->user_to==$userx->id)
                                                <tr>
                                                    <td colspan="7">
                                                        <form action="{{route("company.hrm.send_accounting", $company->slug)}}" method="post" class="mb-20">
                                                            @csrf
                                                            <input type="hidden" name="status" value="1">
                                                            <input type="hidden" name="id" value="{{$acc->id}}">

                                                            <h4>الرد على المسائلة :</h4>

                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <textarea class="form-control" rows="4" required name="message2"></textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-reply"></i> <span>إرسال</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($acc->status==1 and $acc->user_from==$userx->id)
                                                <tr>
                                                    <td colspan="7">
                                                        <form action="{{route("company.hrm.send_accounting", $company->slug)}}" method="post" class="mb-20">
                                                            @csrf
                                                            <input type="hidden" name="status" value="2">
                                                            <input type="hidden" name="id" value="{{$acc->id}}">

                                                            <h4>القرار النهائي :</h4>

                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <textarea class="form-control" rows="4" required name="message3"></textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-reply"></i> <span>إرسال</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">لا توجد مراسلات جديدة حاليا</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

@endsection