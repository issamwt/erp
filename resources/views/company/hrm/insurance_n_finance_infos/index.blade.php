@extends("company.layouts.app", ["title"=>"التأمينات و المعلومات المالية"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة الموارد البشرية</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-12"><h5 class="mt-20 mb-15">تهيئة المعلومات المالية :</h5></div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أصناف البدلات</strong></div>
                            <div class="panel-body">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#div1"><i class="fa fa-list"></i> <span>قائمة البدلات</span></a></li>
                                    @if(permissions_hrm("add_allowance_cat"))
                                        <li><a data-toggle="tab" href="#div2"><i class="fa fa-plus"></i> <span>إضافة بدل</span></a></li>
                                    @endif
                                </ul>

                                <div class="tab-content" style="background: #fff">
                                    <div id="div1" class="tab-pane fade in active">
                                        <table class="table table-bordered table-striped mt-20">
                                            <thead>
                                                <tr>
                                                    <th>الإسم</th>
                                                    <th>نوع البدل</th>
                                                    <th>قيمة البدل</th>
                                                    <th>الأوامر</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($allowance_categories as $ac)
                                                    <tr>
                                                        <td>{{$ac->name}}</td>
                                                        <td>
                                                            @if($ac->type==0)
                                                                <span>نسبة من الراتب الأساسي</span>
                                                            @else
                                                                <span>قيمة ثابتة</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($ac->type==0)
                                                                <span>{{$ac->value}}</span><span> %</span>
                                                            @else
                                                                <span>{{$ac->value}}</span><span>ريال</span>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if(permissions_hrm("update_allowance_cat"))
                                                                <a class="btn btn-success btn-xs btn-open" data-target="#editAC{{$ac->id}}" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                                                            @endif
                                                            @if(permissions_hrm("delete_allowance_cat"))
                                                                <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="حذف"
                                                                   href="{{route("company.hrm.delete_allowance_category", [$company->slug, $ac->id])}}"
                                                                   onclick="return confirm('هل أنت متأكد ؟')"><i class="fa fa-trash"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if(permissions_hrm("update_allowance_cat"))
                                                        <tr>
                                                            <td colspan="4" style="padding: 0px">
                                                                <form class="formi" id="editAC{{$ac->id}}"action="{{route("company.hrm.update_allowance_category", $company->slug)}}" method="post">
                                                                    @csrf

                                                                    <h5 class="mt-0">تعديل البدل</h5>

                                                                    <input type="hidden" name="id" value="{{$ac->id}}">

                                                                    <div class="form-group mt-15">
                                                                        <label>الإسم <span class="red">*</span></label>
                                                                        <input type="text" class="form-control" name="name" required value="{{$ac->name}}">
                                                                    </div>

                                                                    <div class="form-group mt-15">
                                                                        <label>نوع البدل <span class="red">*</span></label>
                                                                        <select class="form-control" name="type" required>
                                                                            <option value="0" @if($ac->type==0) selected @endif>نسبة من الراتب الأساسي</option>
                                                                            <option value="1" @if($ac->type==1) selected @endif>قيمة ثابتة</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group mt-15">
                                                                        <label>قيمة البدل <span class="red">*</span></label>
                                                                        <input type="number" class="form-control" name="value" value="{{$ac->value}}" required>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> تعديل</button>
                                                                    </div>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @if(permissions_hrm("add_allowance_cat"))
                                        <div id="div2" class="tab-pane fade">
                                            <form action="{{route("company.hrm.save_allowance_category", $company->slug)}}" method="post">
                                                @csrf

                                                <div class="form-group mt-15">
                                                    <label>الإسم <span class="red">*</span></label>
                                                    <input type="text" class="form-control" name="name" required>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>نوع البدل <span class="red">*</span></label>
                                                    <select class="form-control" name="type" required>
                                                        <option value="0">نسبة من الراتب الأساسي</option>
                                                        <option value="1">قيمة ثابتة</option>
                                                    </select>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>قيمة البدل <span class="red">*</span></label>
                                                    <input type="number" class="form-control" name="value" required>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                                                </div>

                                            </form>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أصناف الإقتطاعات المالية</strong></div>
                            <div class="panel-body">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#div3"><i class="fa fa-list"></i> <span>قائمة الإقتطاعات</span></a></li>
                                    @if(permissions_hrm("add_deduction_cat"))
                                        <li><a data-toggle="tab" href="#div4"><i class="fa fa-plus"></i> <span>إضافة إقتطاع</span></a></li>
                                    @endif
                                </ul>

                                <div class="tab-content" style="background: #fff">
                                    <div id="div3" class="tab-pane fade in active">
                                        <table class="table table-bordered table-striped mt-20">
                                            <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>نوع الإقتطاع</th>
                                                <th>قيمة الإقتطاع</th>
                                                <th>الأوامر</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($deduction_categories as $dc)
                                                <tr>
                                                    <td>{{$dc->name}}</td>
                                                    <td>
                                                        @if($dc->type==0)
                                                            <span>نسبة من الراتب الأساسي</span>
                                                        @else
                                                            <span>قيمة ثابتة</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($dc->type==0)
                                                            <span>{{$dc->value}}</span><span> %</span>
                                                        @else
                                                            <span>{{$dc->value}}</span><span>ريال</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if(permissions_hrm("update_deduction_cat"))
                                                            <a class="btn btn-success btn-xs btn-open" data-target="#editDC{{$dc->id}}" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                                                        @endif
                                                        @if(permissions_hrm("delete_deduction_cat"))
                                                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="حذف"
                                                               href="{{route("company.hrm.delete_deduction_category", [$company->slug, $dc->id])}}"
                                                               onclick="return confirm('هل أنت متأكد ؟')"><i class="fa fa-trash"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if(permissions_hrm("update_deduction_cat"))
                                                    <tr>
                                                        <td colspan="4" style="padding: 0px">
                                                            <form class="formi" id="editDC{{$dc->id}}"action="{{route("company.hrm.update_deduction_category", $company->slug)}}" method="post">
                                                                @csrf

                                                                <h5 class="mt-0">تعديل الإقتطاع</h5>

                                                                <input type="hidden" name="id" value="{{$dc->id}}">

                                                                <div class="form-group mt-15">
                                                                    <label>الإسم <span class="red">*</span></label>
                                                                    <input type="text" class="form-control" name="name" required value="{{$dc->name}}">
                                                                </div>

                                                                <div class="form-group mt-15">
                                                                    <label>نوع البدل <span class="red">*</span></label>
                                                                    <select class="form-control" name="type" required>
                                                                        <option value="0" @if($dc->type==0) selected @endif>نسبة من الراتب الأساسي</option>
                                                                        <option value="1" @if($dc->type==1) selected @endif>قيمة ثابتة</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group mt-15">
                                                                    <label>قيمة البدل <span class="red">*</span></label>
                                                                    <input type="number" class="form-control" name="value" value="{{$dc->value}}" required>
                                                                </div>

                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> تعديل</button>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @if(permissions_hrm("add_deduction_cat"))
                                        <div id="div4" class="tab-pane fade">
                                            <form action="{{route("company.hrm.save_deduction_category", $company->slug)}}" method="post">
                                                @csrf

                                                <div class="form-group mt-15">
                                                    <label>الإسم <span class="red">*</span></label>
                                                    <input type="text" class="form-control" name="name" required>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>نوع الإقتطاع <span class="red">*</span></label>
                                                    <select class="form-control" name="type" required>
                                                        <option value="0">نسبة من الراتب الأساسي</option>
                                                        <option value="1">قيمة ثابتة</option>
                                                    </select>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>قيمة الإقتطاع <span class="red">*</span></label>
                                                    <input type="number" class="form-control" name="value" required>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                                                </div>

                                            </form>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-12"><h5 class="mt-20 mb-15">تهيئة التأمينات المختلفة :</h5></div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>التأمين الطبي</strong></div>
                            <div class="panel-body">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#div5"><i class="fa fa-list"></i> <span>قائمة التأمين الطبي</span></a></li>
                                    @if(permissions_hrm("add_medical"))
                                        <li><a data-toggle="tab" href="#div6"><i class="fa fa-plus"></i> <span>إضافة تأمين الطبي</span></a></li>
                                    @endif
                                </ul>

                                <div class="tab-content" style="background: #fff">
                                    <div id="div5" class="tab-pane fade in active">
                                        <table class="table table-bordered table-striped mt-20">
                                            <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>نسبة الموظف</th>
                                                <th>نسبة الشركة</th>
                                                <th>الأوامر</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($medical_categories as $mc)
                                                <tr>
                                                    <td>{{$mc->name}}</td>
                                                    <td><span>{{$mc->employee}}</span> <span>%</span></td>
                                                    <td><span>{{$mc->company}}</span> <span>%</span></td>
                                                    <td class="text-center">
                                                        @if(permissions_hrm("update_medical"))
                                                            <a class="btn btn-success btn-xs btn-open" data-target="#editMC{{$mc->id}}" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                                                        @endif
                                                        @if(permissions_hrm("delete_medical"))
                                                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="حذف"
                                                               href="{{route("company.hrm.delete_medical_category", [$company->slug, $mc->id])}}"
                                                               onclick="return confirm('هل أنت متأكد ؟')"><i class="fa fa-trash"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if(permissions_hrm("update_medical"))
                                                    <tr>
                                                        <td colspan="4" style="padding: 0px">
                                                            <form class="formi" id="editMC{{$mc->id}}"action="{{route("company.hrm.update_medical_category", $company->slug)}}" method="post">
                                                                @csrf

                                                                <h5 class="mt-0">تعديل التأمين</h5>

                                                                <input type="hidden" name="id" value="{{$mc->id}}">

                                                                <div class="form-group mt-15">
                                                                    <label>الإسم <span class="red">*</span></label>
                                                                    <input type="text" class="form-control" name="name" required value="{{$mc->name}}">
                                                                </div>

                                                                <div class="form-group mt-15">
                                                                    <label>نسبة الموظف <span class="red">*</span></label>
                                                                    <input type="number" class="form-control" name="employee" required value="{{$mc->employee}}">
                                                                </div>

                                                                <div class="form-group mt-15">
                                                                    <label>نسبة الشركة <span class="red">*</span></label>
                                                                    <input type="number" class="form-control" name="companyc" required value="{{$mc->company}}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> تعديل</button>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @if(permissions_hrm("add_medical"))
                                        <div id="div6" class="tab-pane fade">
                                            <form action="{{route("company.hrm.save_medical_category", $company->slug)}}" method="post">
                                                @csrf

                                                <div class="form-group mt-15">
                                                    <label>الإسم <span class="red">*</span></label>
                                                    <input type="text" class="form-control" name="name" required>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>نسبة الموظف <span class="red">*</span></label>
                                                    <input type="number" class="form-control" name="employee" required>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>نسبة الشركة <span class="red">*</span></label>
                                                    <input type="number" class="form-control" name="companyc" required>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                                                </div>

                                            </form>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>الضمان الإجتماعي</strong></div>
                            <div class="panel-body">

                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#div7"><i class="fa fa-list"></i> <span>قائمة الضمان الإجتماعي</span></a></li>
                                    @if(permissions_hrm("add_social"))
                                        <li><a data-toggle="tab" href="#div8"><i class="fa fa-plus"></i> <span>إضافة ضمان الإجتماعي</span></a></li>
                                    @endif
                                </ul>

                                <div class="tab-content" style="background: #fff">
                                    <div id="div7" class="tab-pane fade in active">
                                        <table class="table table-bordered table-striped mt-20">
                                            <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>نسبة الموظف</th>
                                                <th>نسبة الشركة</th>
                                                <th>الأوامر</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($social_categories as $sc)
                                                <tr>
                                                    <td>{{$sc->name}}</td>
                                                    <td><span>{{$sc->employee}}</span> <span>%</span></td>
                                                    <td><span>{{$sc->company}}</span> <span>%</span></td>
                                                    <td class="text-center">
                                                        @if(permissions_hrm("update_social"))
                                                            <a class="btn btn-success btn-xs btn-open" data-target="#editSC{{$sc->id}}" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                                                        @endif
                                                        @if(permissions_hrm("delete_social"))
                                                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="حذف"
                                                               href="{{route("company.hrm.delete_social_category", [$company->slug, $sc->id])}}"
                                                               onclick="return confirm('هل أنت متأكد ؟')"><i class="fa fa-trash"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if(permissions_hrm("update_social"))
                                                    <tr>
                                                        <td colspan="4" style="padding: 0px">
                                                            <form class="formi" id="editSC{{$sc->id}}"action="{{route("company.hrm.update_social_category", $company->slug)}}" method="post">
                                                                @csrf

                                                                <h5 class="mt-0">تعديل الضمان</h5>

                                                                <input type="hidden" name="id" value="{{$sc->id}}">

                                                                <div class="form-group mt-15">
                                                                    <label>الإسم <span class="red">*</span></label>
                                                                    <input type="text" class="form-control" name="name" required value="{{$sc->name}}">
                                                                </div>

                                                                <div class="form-group mt-15">
                                                                    <label>نسبة الموظف <span class="red">*</span></label>
                                                                    <input type="number" class="form-control" name="employee" required value="{{$sc->employee}}">
                                                                </div>

                                                                <div class="form-group mt-15">
                                                                    <label>نسبة الشركة <span class="red">*</span></label>
                                                                    <input type="number" class="form-control" name="companyc" required value="{{$sc->company}}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> تعديل</button>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @if(permissions_hrm("add_social"))
                                        <div id="div8" class="tab-pane fade">
                                            <form action="{{route("company.hrm.save_social_category", $company->slug)}}" method="post">
                                                @csrf

                                                <div class="form-group mt-15">
                                                    <label>الإسم <span class="red">*</span></label>
                                                    <input type="text" class="form-control" name="name" required>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>نسبة الموظف <span class="red">*</span></label>
                                                    <input type="number" class="form-control" name="employee" required>
                                                </div>

                                                <div class="form-group mt-15">
                                                    <label>نسبة الشركة <span class="red">*</span></label>
                                                    <input type="number" class="form-control" name="companyc" required>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                                                </div>

                                            </form>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .panel-body{
            min-height: 300px;
        }
    </style>

    <script>
        $(function () {

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

        });
    </script>

@endsection