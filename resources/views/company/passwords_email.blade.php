<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>إستعادة كلمة المرور</title>
    <link rel="icon" type="image/png" href="{{asset("public/uploads/favicon.png")}}" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{asset("public/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("public/dist/css/AdminLTE.min.css")}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page" style="background-image: url('{{asset("public/uploads/meetings.jpg")}}'); background-size:cover;">
<div class="login-box" style="margin-top: 70px">
    <div class="login-logo">
        @if($company->image)
            <img src="{{asset("storage/app/".$company->image)}}" style="display: block; margin: 10px auto; width: 50%; border: 10px solid #fff;">
        @else
            <img src="{{asset("public/uploads/logo2.png")}}" style="display: block; margin: 10px auto; width: 50%; border: 10px solid #fff;">
        @endif
    </div>
    <div class="login-box-body">
        <h4 class="login-box-msg">{{$company->name}}</h4>
        <h4 class="text-center" style="margin-top: -10px;margin-bottom: 20px;">إستعادة كلمة المرور</h4>
        <form action="{{ route('company.password.email', $company->slug) }}" method="post">
            @csrf

            @if (session('status'))
                <div class="green" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} text-right" name="email" value="{{ old('email') }}" required autofocus placeholder="البريد الإلكتروني">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            @if ($errors->has('email'))
                <div style="margin-bottom: 20px" class="text-right red invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></div>
            @endif

            <div class="row">
                <div class="col-xs-4"></div>
                <div class="col-xs-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">أرسل رابط إستعادة كلمة المرور</button>
                </div>
            </div>
        </form>

    </div>
</div>

<!-- jQuery 2.1.4 -->
<script src="{{asset("public/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{asset("public/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- iCheck -->
<script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<style>
    .red{
        color: red;
    }
    .green{
        text-align: center;
        color: #2fa360;
        font-size: 1.2em;
        margin-bottom: 20px;
    }
</style>
</body>
</html>
