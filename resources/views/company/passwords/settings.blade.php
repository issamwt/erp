@extends("company.layouts.app", ["title"=>"الإعدادات"])

@section("content")

    <section class="content-header">
        <h1>نظام باسوورداتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-12">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>الإشعارات</strong></div>
                            <div class="panel-body" style="display: block;">

                                <div id="div1-wrapper">
                                    <div id="div1">

                                        {{--
                                        <form action="{{route("company.passwords.savenotif", $company->slug)}}" method="post">
                                            @csrf
                                            <br><br>
                                            <input type="hidden" name="company_id" value="{{$company->id}}">

                                            <input type="text" name="name" placeholder="name" required class="form-control">
                                            <br>

                                            <input type="text" name="title" placeholder="title" required class="form-control">
                                            <br>

                                            <div class="checkbox">
                                                <label><input type="checkbox" name="app" value="1" checked required>على البرنامج</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="email" value="1" checked required>عبر ال
                                                    Eamil</label>
                                            </div>
                                            <div class="checkbox disabled">
                                                <label><input type="checkbox" name="sms" value="1" checked required>عبر ال
                                                    Sms</label>
                                            </div>
                                            <br>

                                            <textarea name="message" placeholder="message" required
                                                      class="form-control"></textarea>
                                            <br>

                                            <button type="submit">أضف</button>
                                            <br><br><br>
                                        </form>
                                        --}}

                                        <br>
                                        <div class="row mb-30">
                                            <div class="col-sm-2">نوع الإشعار</div>
                                            <div class="col-sm-2">على البرنامج</div>
                                            <div class="col-sm-2">عبر البريد الإلكتروني</div>
                                            <div class="col-sm-2">عبر الرسائل القصيرة</div>
                                            <div class="col-sm-4">الرسالة</div>
                                        </div>


                                        <form action="{{route("company.passwords.savenotif", $company->slug)}}" method="post" class="mb-10 form1">
                                            @csrf

                                            @foreach($notifs as $notif)

                                                <div class="row mb-10">

                                                    <div class="col-sm-2">{{$notif->title}}{{--<br>{{$notif->name}}--}}</div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="app_{{$notif->id}}" value="1"
                                                                          @if($notif->app==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="email_{{$notif->id}}" value="1"
                                                                          @if($notif->email==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="sms_{{$notif->id}}" value="1"
                                                                          @if($notif->sms==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="text-justify col-sm-4">
                                            <textarea name="message_{{$notif->id}}" class="form-control"
                                                      maxlength="250" required>{{$notif->message}}</textarea>
                                                    </div>

                                                </div>

                                            @endforeach

                                            <center>
                                                <button type="submit" class="btn btn-primary mt-20 mb-30" style="width: 120px"><i
                                                            class="fa fa-save"></i> <span>حفظ</span></button>
                                            </center>
                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .panel-heading {
            cursor: pointer;
        }

        .panel-body {
            display: none;
        }
    </style>

    <script>
        $(function () {

            $(document).on("click", ".panel-heading", function (e) {
                e.preventDefault();
                $(this).parent().find(".panel-body").slideToggle();
            });

            $(document).on("submit", ".form1", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div1-wrapper").html($("#loading-wrapper").html());
                $.post('{{route("company.passwords.savenotif", $company->slug)}}', $(form).serialize(), function () {
                    $("#div1-wrapper").load(window.location + " #div1", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });
        });
    </script>

    <style>

        div.bhoechie-tab-container {
            z-index: 10;
            background-color: #ffffff;
            padding: 0 !important;
            border: 1px solid #ddd;
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            -moz-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            background-clip: padding-box;
            opacity: 0.97;
            filter: alpha(opacity=97);
        }

        div.bhoechie-tab-menu {
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group {
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group > a {
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group > a .glyphicon,
        div.bhoechie-tab-menu div.list-group > a .fa {
            color: #5A55A3;
        }

        div.bhoechie-tab-menu div.list-group > a:first-child {
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group > a:last-child {
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group > a.active,
        div.bhoechie-tab-menu div.list-group > a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group > a.active .fa {
            background-color: #337ab7;
            border-radius: 0px;
            color: #ffffff;
        }

        div.bhoechie-tab-menu div.list-group > a.active:after {
            content: '';
            position: absolute;
            right: 100%;
            top: 50%;
            margin-top: -13px;
            border-left: 0;
            border-bottom: 13px solid transparent;
            border-top: 13px solid transparent;
            border-right: 10px solid #337ab7;
        }

        div.bhoechie-tab-content {
            background-color: #ffffff;
            /* border: 1px solid #eeeeee; */
            padding-left: 20px;
            padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active) {
            display: none;
        }

        .bhoechie-tab {
            padding-top: 10px;
            padding-bottom: 120px;
            min-height: 400px;
            border-right: 1px solid #ddd;
        }

        .icheckbox_square-blue {
            margin-left: 10px;
        }

    </style>

    <script>
        $(document).ready(function () {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>

@endsection