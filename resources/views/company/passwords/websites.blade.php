
@extends("company.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content-header">
        <h1>نظام باسوورداتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>قائمة المواقع والتطبيقات</strong>
                        @if(permissions_passwords("add_website"))
                            <a href="{{route("company.passwords.add_website", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-plus"></i> <span>إضافة موقع</span></a>
                        @endif
                    </div>
                    <div class="panel-body">

                        <div class="row mt-5 mb-30">
                            @if(count($websites)>0)
                                @foreach($websites as $website)
                                    <div class="col-sm-3">
                                        <div class="boxx">
                                            @if(!permissions_passwords("show_website"))
                                                <?php $url = "javascript:void(0);" ?>
                                            @else
                                                <?php $url = route("company.passwords.edit_website", [$company->slug, $website->id]); ?>
                                            @endif
                                            <a href="{{$url}}" class="imglink"><img src="{{asset("storage/app/".$website->image)}}"></a>
                                            <h4>{{$website->name}}</h4>
                                            <div class="col-sm-12 buttonss">
                                                <div class="row">
                                                    <div class="col-md-6 mb-5">
                                                        <a href="{{$url}}" class="btn btn-block btn-danger" style="font-size: 0.8em;"><i class="fa fa-eye"></i> <span>التفاصيل</span></a>
                                                    </div>
                                                    <div class="col-md-6 mb-5">
                                                        <a href="{{$website->link}}" target="_blank" class="btn btn-block btn-primary" style="font-size: 0.8em;"><i class="fa fa-link"></i> <span>زيارة الرابط</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-sm-12">لا توجد مواقع حاليا</div>
                            @endif
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

<style>
    .boxx{
        display: block;
        position: relative;
        width: 100%;
        min-height: 320px;
        border: 1px solid #eee;
        border-radius: 13px;
        box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.15);
    }
    .boxx .imglink{
        transition: all ease 0.2s;
    }
    .boxx .imglink:hover{
        box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.93);
    }
    .boxx img{
        border: 1px solid #c3c3c3;
    }
    .boxx .imglink, .boxx img{
        display: block;
        margin: 13px auto;
        border-radius: 50%;
        height: 130px;
        width: 130px;

    }
    .boxx h4{
        margin: 14px auto 10px;
        display: block;
        text-align: center;
    }
    .boxx .buttonss{
        position: absolute;
        bottom: 15px;
        width: 100%;
    }
    .boxx .buttonss a{
        border-radius: 11px !important;
    }
</style>

@endsection