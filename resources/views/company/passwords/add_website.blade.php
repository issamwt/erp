@extends("company.layouts.app", ["title"=>"إضافة موقع"])

@section("content")

    <section class="content-header">
        <h1>نظام باسوورداتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>إضافة موقع</strong> <a href="{{route("company.passwords.websites", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>العودة للقائمة</span></a></div>
                    <div class="panel-body">

                        <form action="{{route("company.passwords.save_website", @$company->slug)}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>إسم الموقع <span class="red">*</span></label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>شعار الموقع <span class="red">*</span></label>
                                        <input type="file" accept="image/*" class="form-control" name="image" required>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>رابط الموقع <span class="red">*</span></label>
                                        <input type="text" class="form-control" name="link" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>رابط لوحة التحكم</label>
                                        <input type="text" class="form-control" name="dashboard_link">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>لمن يظهر هذا الموقع <span class="red">*</span></label>
                                        <select class="form-control select2x" name="userss[]" multiple required>
                                            @foreach($users as $u)
                                                <option value="{{$u->id}}" @if($u->id==$user->id) selected @endif>{{$u->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>المستخدمين</h4>
                                    <button type="button" class="btn btn-xs btn-info btn-add"><i class="fa fa-plus"></i> <span>إضافة مستخدم</span></button>
                                </div>
                                <div class="col-sm-12" id="users"></div>
                            </div>

                            <div class="form-group mt-50">
                                <button type="submit" class="btn btn-primary" style="width: 120px;margin: 0px auto; display: inline-block"><i class="fa fa-save"></i> <span>إضافة</span></button>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <script>
        $(function () {

            $('.datee').datepicker({format: 'yyyy-mm-dd', rtl: true, language: 'ar', autoclose: true});

            $(document).on("click", ".btn-add", function (e) {
                var html = '<div class="row mt-10">\n' +
                    '        <div class="col-sm-4">\n' +
                    '            <div class="form-group">\n' +
                    '                <label>إسم المستخدم <span class="red">*</span></label>\n' +
                    '                <input type="text" class="form-control" required name="names[]">\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '        <div class="col-sm-3">\n' +
                    '            <div class="form-group">\n' +
                    '                <label>كلمة سر المستخدم <span class="red">*</span></label>\n' +
                    '                <input type="text" class="form-control" required name="passwords[]">\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '        <div class="col-sm-4">\n' +
                    '            <div class="form-group">\n' +
                    '                <label>نوع المستخدم <span class="red">*</span></label>\n' +
                    '                <input type="text" class="form-control" required name="types[]">\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '        <div class="col-sm-1">\n' +
                    '            <div class="form-group">\n' +
                    '                <label style="opacity: 0">حذف</label>\n' +
                    '                <button type="button" class="btn btn-danger btn-block btn-delete"><i class="fa fa-remove"></i> <span>حذف</span></button>\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>';
                $("#users").append(html);
            });

            $(document).on("click", ".btn-delete", function (e) {
                $(this).closest(".row").remove();
            });

        });
    </script>

@endsection