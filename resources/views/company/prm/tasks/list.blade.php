<table class="table table-bordered table-striped dt-responsive last-col grd_view grd_view_tasks" id="dataTable">
    <thead>
    <tr>
        <th>عنوان المهمة</th>
        <th>المرحلة</th>
        <th>المشروع</th>
        <th>تاريخ البدء</th>
        <th>الموعد النهائى</th>
        <th>الأولوية</th>
        <th>المكلف</th>
        <th>المساعدون</th>
        <th width="200">التفاصيل</th>
    </tr>
    </thead>
    <tbody>
    @if(count($tasks)<=0)

    @else
        @foreach($tasks as $task)
            <?php $uss=[]; foreach($task->users as $user) array_push($uss, $user->id); $uss = implode(",", $uss);?>
            <tr class="odd gradeX" data-date0="{{$task->from_date}}" data-date00="{{$task->to_date}}" data-employees="{{$uss}}" data-status="{{$task->status}}" data-priority="{{$task->priority}}"
            data-user="{{$task->user_id}}" data-milestone="{{@$task->milestone->id}}" data-project="{{@$task->daftar_id}}" data-client="{{@$task->project->client->id}}">
                <td style="border-right: 7px solid #8aeeff; border-right-color: {{@$task->taskStatus->color}};">
                    <a href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" data-name="{{$task->name}}">{{$task->name}}</a>
                </td>
                <td>
                    @if($task->milestone)
                        <a href="{{route("company.prm.editMilestone", [$company->slug, @$task->milestone->id])}}">{{@$task->milestone->name}}</a>
                    @else
                        <span>بدون مرحلة</span>
                    @endif
                </td>
                <td>
                    <a href="{{route('company.prm.project', [$company->slug, @$task->project->id])}}">{{@$task->project->name}} <br><small class="tagg">{{@$task->project->client->name}}</small></a>
                </td>
                <td class="case-serial-td" style="vertical-align: top;">{{$task->from_date}}</td>
                <td style="vertical-align: top">@if(\Carbon\Carbon::now()>$task->to_date)<span style="color: #e20000; text-decoration: line-through;">{{$task->to_date}}</span>@else<span>{{$task->to_date}}</span>@endif</td>
                <td class="text-center">
                    @if($task->priority==1)
                        <label class="label label-info" style="width:100%; display: block; padding: 5px 6px;">عادى</label>
                    @elseif($task->priority==2)
                        <label class="label label-warning" style="width:100%; display: block; padding: 5px 6px;">عاجل</label>
                    @elseif($task->priority==3)
                        <label class="label label-danger" style="width:100%; display: block; padding: 5px 6px;">عاجل جدا</label>
                    @elseif($task->priority==4)
                        <label class="label label-success" style="width:100%; display: block; padding: 5px 6px;">غير عاجل</label>
                    @endif
                </td>
                <td style="vertical-align: top">
                    @if(@$task->user)
                        @if(@$task->user->image)
                            <img src="{{asset('storage/app/'.@$task->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                        @else
                            <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                        @endif
                        <a href="{{route('company.users.show',[$company->slug, @$task->user->id])}}">{{@$task->user->name}}</a>
                    @endif
                </td>
                <td>
                    <ul style="list-style: none; margin: 0px; padding: 0px;">
                        @foreach($task->users as $user)
                            @if(@$user)
                                <li class="mb-10">
                                    @if(@$user->image)
                                        <img src="{{asset('storage/app/'.@$user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                    @else
                                        <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                    @endif
                                    <a href="{{route('company.users.show',[$company->slug, @$user->id])}}">{{@$user->name}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </td>
                <td class="text-center" style="vertical-align: top; position: relative">
                    <a href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" data-name="{{$task->name}}" class="btn btn-default" data-toggle="tooltip" title="مشاهدة"><i class="fa fa-eye"></i></a>
                    @if(permissions_prm("update_task"))
                        <a href="#choose{{$task->id}}" class="btn btn-primary btn-choose" data-toggle="tooltip" title="الحالة"><i class="fa fa-star"></i></a>
                        <a href="{{route("company.prm.editTask", [$company->slug, $task->id])}}" class="btn btn-success" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                    @endif
                    @if(permissions_prm("delete_task"))
                        <a href="{{route("company.prm.deleteTask", [$company->slug, $task->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-danger" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                    @endif
                    <ul id="choose{{$task->id}}" class="div-choose" style="display: none; list-style: none; margin: 0px; padding: 0px;">
                        @if($task->status!=0)
                            <li>
                                <a href="{{route("company.prm.statusTask", [$company->slug, $task->id, 0])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-primary btn-block" style="color:#fff;">جديدة</a>
                            </li>
                        @else
                            <li>
                                <a href="javascript:void(0)" class="btn btn-primary btn-block disabled heree" style="color:#fff;">جديدة</a>
                            </li>
                        @endif
                        
                        @foreach($taskStatus as $status)
                            @if($status->id!=$task->status)
                                <li>
                                    <a href="{{route("company.prm.statusTask", [$company->slug, $task->id, $status->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-default btn-block" style="color:#fff; background: {{$status->color}}">{{$status->name}}</a>
                                </li>
                            @else
                                <li>
                                    <a href="javascript:void(0)" class="btn btn-default btn-block disabled heree" style="color:#fff; background: {{$status->color}}">{{$status->name}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
