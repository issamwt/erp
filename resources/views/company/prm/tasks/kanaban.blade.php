<div class="row">
    <div class="col-sm-12" style="padding: 15px; padding-bottom: 7px">
        <div class="col-sm-12">
            <div id="kanaban-container" class="row">
                <div id="kanaban-div">
                <?php $i=1; ?>
                @foreach($taskStatus as $status)
                        <div class="statusBox">
                            <div class="kanban-col-title" style="background:{{$status->color}};">{{$status->name}}</div>
                            <ul data-status="{{$status->id}}" id="sortable{{$status->id}}" class="connectedSortable">
                                @foreach($tasks as $task)
                                    @if($task->status==$status->id)
                                        <?php $uss=[]; foreach($task->users as $user) array_push($uss, $user->id); $uss = implode(",", $uss);?>
                                        <a data-id="{{$task->id}}" data-date0="{{$task->from_date}}" data-date00="{{$task->to_date}}" data-employees="{{$uss}}" data-status="{{$task->status}}" data-priority="{{$task->priority}}"
                                           data-user="{{$task->user_id}}" data-milestone="{{@$task->milestone->id}}" data-project="{{@$task->daftar_id}}" data-client="{{@$task->project->client->id}}"
                                           href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" class="kanban-item kanban-sortable-chosen ui-state-default" >
                                            <span class="avatar" data-toggle="tooltip" title="{{@$task->user->name}}">
                                                @if(@$task->user->image)
                                                    <img src="{{asset("storage/app/".@$task->user->image)}}" class="mCS_img_loaded">
                                                @else
                                                    <img src="{{asset("public/noimage.png")}}" class="mCS_img_loaded">
                                                @endif
                                            </span>
                                            <span>{{$i++}} .</span> <span>{{$task->name}}</span>
                                        </a>
                                    @endif
                                @endforeach
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                @endforeach
                    <div class="statusBox">
                        <div class="kanban-col-title" style="background:#8aeeff;">جديدة</div>
                        <ul data-status="0" id="sortable0" class="connectedSortable">
                            @foreach($tasks as $task)
                                @if($task->status==0)
                                    <?php $uss=[]; foreach($task->users as $user) array_push($uss, $user->id); $uss = implode(",", $uss);?>
                                    <a data-id="{{$task->id}}" data-date0="{{$task->from_date}}" data-date00="{{$task->to_date}}" data-employees="{{$uss}}" data-status="0" data-priority="{{$task->priority}}"
                                       data-user="{{$task->user_id}}" data-milestone="{{@$task->milestone->id}}" data-project="{{@$task->daftar_id}}" data-client="{{@$task->project->client->id}}"
                                       href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" class="kanban-item kanban-sortable-chosen ui-state-default" >
                                            <span class="avatar" data-toggle="tooltip" title="{{@$task->user->name}}">
                                                @if(@$task->user->image)
                                                    <img src="{{asset("storage/app/".@$task->user->image)}}" class="mCS_img_loaded">
                                                @else
                                                    <img src="{{asset("public/noimage.png")}}" class="mCS_img_loaded">
                                                @endif
                                            </span>
                                        <span>{{$i++}} .</span> <span>{{$task->name}}</span>
                                    </a>
                                @endif
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
