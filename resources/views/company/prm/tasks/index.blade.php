@extends("company.layouts.app", ["title"=>"قائمة المهام"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>قائمة المهام</strong></div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#finances_filter0').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                <form id="finances_filter0" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datee date1" autocomplete="off" name="from" placeholder="من تاريخ">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datee date2" autocomplete="off" name="to" placeholder="إلى تاريخ">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x userss" autocomplete="off">
                                            <option value="0">المكلف</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x employees" autocomplete="off">
                                            <option value="0">المساعدون</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x status" autocomplete="off">
                                            <option value="all">جميع الحالات</option>
                                            <option value="0">جديدة</option>
                                            @foreach($taskStatus as $status)
                                                <option value="{{$status->id}}">{{$status->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x priority" autocomplete="off">
                                            <option value="0">جميع الأولويات</option>
                                            <option value="1">عادى</option>
                                            <option value="2">عاجل</option>
                                            <option value="3">عاجل جدا</option>
                                            <option value="4">غير عاجل</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x milestone" autocomplete="off">
                                            <option value="0">جميع المراحل</option>
                                            @foreach($projects as $p)
                                                <optgroup label="مراحل المشروع {{$p->name}}">
                                                    @foreach($p->milestones as $m)
                                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x project" autocomplete="off">
                                            <option value="0">جميع المشاريع</option>
                                            @foreach($projects as $p)
                                                <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="select2x client" autocomplete="off">
                                            <option value="0">جميع العملاء</option>
                                            @foreach($clients as $c)
                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block">فلتر</button>
                                    </div>

                                    <div class="col-sm-6">
                                        <button type="reset" class="btn btn-info btn-block">مسح</button>
                                    </div>

                                    <div class="clearfix"></div>

                                </form>
                                <div class="clearfix"></div>

                                <div class="clearfix"><hr></div>

                                <div id="task-wrapper" style="border: none; padding: 0px; margin: 0px;">

                                    <div id="task-div">

                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#list">قائمة المهام</a></li>
                                            <li><a data-toggle="tab" id="kanabanDiv" href="#kanaban">لوحة المهام</a></li>
                                            <li><a data-toggle="tab" id="GanttDiv" href="#gantt">مخطط المهام</a></li>
                                        </ul>

                                        <div class="tab-content" style="padding-top: 20px;">

                                            <!-- LIST -->
                                            <div id="list" class="tab-pane fade in active">
                                                @include("company.prm.tasks.list")
                                            </div>

                                            <!-- KANABAN -->
                                            <div id="kanaban" class="tab-pane fade" style="background: #e5e9ec;">
                                                @include("company.prm.tasks.kanaban")
                                            </div>

                                            <!-- GANTT -->
                                            <div id="gantt" class="tab-pane fade" style="background: #e5e9ec;">
                                                <div class="w100p pt10" style="min-height: 500px;">
                                                    <div id="gantt-chart" style="width: 100%;"></div>
                                                    <div id="scroll-to-me"></div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                @foreach($taskStatus as $status)
                                    <div class="mt-15">
                                        <div style="background:{{$status->color}}; display: inline-block; width: 15px; height: 15px; border-radius: 2px; float: right;"></div>
                                        <h6 class="pull-right" style="margin: 0px 10px 0px 01px;font-size: 1em;">{{$status->name}}</h6>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach

                                <div class="mt-15">
                                    <div style="background:#8aeeff; display: inline-block; width: 15px; height: 15px; border-radius: 2px; float: right;"></div>
                                    <h6 class="pull-right" style="margin: 0px 10px 0px 01px;font-size: 1em;">جديدة</h6>
                                    <div class="clearfix"></div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .datee{
            margin-top: 3px;
        }
        div:not(.modal) .select2-selection{
            border-radius: 0px !important;
            padding: 8px 12px !important;
            height: 36px !important;
        }
        div:not(.modal) .select2-container{
            margin-top: 3px;
        }
        .kanban-col-title {
            padding: 10px 15px;
            font-weight: bold;
            color: #fff;
            margin-bottom: 15px;
            margin-right: 3px;
        }

        #kanaban-wrapper{
            background: #E4E8EB;
            padding: 5px 15px;
            min-height: 400px;
        }
        #kanaban-div{
            overflow-y: auto;
            min-height: 400px;
            padding-right: 10px;
        }
        .statusBox{
            min-width: 400px;
            max-width: 400px;
            display: table-cell;
            padding-left: 15px;
        }
        #kanaban{
            min-height: 200px;
        }
        .kanban-item {
            padding: 10px;
            margin: 0 3px 10px 0;
            background-color: #fff !important;
            background: #fff !important;
            cursor: default;
            display: table;
            color: #4e5e6a;
            width: 100%;
            cursor: pointer;
        }
        .kanban-item .avatar {
            float: left;
        }
        .avatar {
            display: inline-block;
            white-space: nowrap;
        }
        .kanban-item .avatar img {
            height: 22px;
            width: 22px;
            margin-right: 10px;
        }
        .avatar img {
            height: auto;
            max-width: 100%;
            border-radius: 50%;
        }
        .connectedSortable{
            padding: 0px;
            margin: 0px;
        }
        .tagg{
            background: deepskyblue;
            color: #fff;
            font-weight: bold;
            font-size: 0.6em;
            padding: 0.5px 3px;
            border-radius: 2px;
        }
        .heree{
            font : normal normal normal 14px/1 FontAwesome;
            border: 2px solid red !important;
        }
        a.heree:before{
            content: "\f00c";
            padding-left: 5px;
            color: red;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        $(function () {

            ( function( factory ) {
                    if ( typeof define === "function" && define.amd ) {

                        // AMD. Register as an anonymous module.
                        define( [ "../widgets/datepicker" ], factory );
                    } else {

                        // Browser globals
                        factory( jQuery.datepicker );
                    }
                }( function( datepicker ) {

                    datepicker.regional.ar = {
                        closeText: "إغلاق",
                        prevText: "&#x3C;السابق",
                        nextText: "التالي&#x3E;",
                        currentText: "اليوم",
                        monthNames: [ "يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو",
                            "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر" ],
                        monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
                        dayNames: [ "الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت" ],
                        dayNamesShort: [ "أحد", "اثنين", "ثلاثاء", "أربعاء", "خميس", "جمعة", "سبت" ],
                        dayNamesMin: [ "ح", "ن", "ث", "ر", "خ", "ج", "س" ],
                        weekHeader: "أسبوع",
                        dateFormat: "yy-mm-dd",
                        firstDay: 0,
                        isRTL: true,
                        showMonthAfterYear: false,
                        yearSuffix: "" };
                    datepicker.setDefaults( datepicker.regional.ar );

                    return datepicker.regional.ar;

                } ) );

            $('.datee').datepicker({
                dateFormat: "yy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on("click", ".btn-choose", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                $(href).slideToggle();
            });

            $(document).on("submit", "#finances_filter0", function (e){
                var date1   = $("#finances_filter0 .date1").val();
                var date2   = $("#finances_filter0 .date2").val();
                var employees = $("#finances_filter0 .employees").select2('val')+"";
                var userss = $("#finances_filter0 .userss").select2('val')+"";
                var status  = $("#finances_filter0 .status").select2('val')+"";
                var priority = $("#finances_filter0 .priority").select2('val')+"";
                var project = $("#finances_filter0 .project").select2('val')+"";
                var milestone = $("#finances_filter0 .milestone").select2('val')+"";
                var client = $("#finances_filter0 .client").select2('val')+"";

                $('.grd_view_tasks tbody tr, .kanban-item').hide();
                $('.grd_view_tasks tbody tr, .kanban-item').filter(function () {
                    var datex = true;
                    var date0 = $(this).data("date0");
                    var date00 = $(this).data("date00");
                    if(date0 && date00){
                        if(date1 && !date2) {
                            if(new Date(date1) > new Date(date00))
                                datex = false;
                        }else if(!date1 && date2){
                            if(new Date(date0) > new Date(date2))
                                datex = false;
                        }else if(date1 && date2) {
                            if(new Date(date1)>new Date(date0) || new Date(date2)<new Date(date00))
                                datex = false;
                        }
                    }

                    var userx = true;
                    var user0 = $(this).data("user").toString();
                    if(userss){
                        if(userss!="0"){
                            if(user0!=userss)
                                userx=false;
                        }
                    }

                    var employeesx = true;
                    var employees0 = $(this).data("employees").toString();
                    employees0 = employees0.split(",");
                    if(employees){
                        if(employees!="0"){
                            if(!employees0.includes(employees))
                                employeesx=false;
                        }
                    }

                    var statusx = true;
                    var status0 = $(this).data("status").toString();
                    if(status){
                        if(!status.includes("all"))
                            if(!status.includes(status0.toString()))
                                statusx = false;
                    }

                    var priorityx = true;
                    var priority0 = $(this).data("priority").toString();
                    if(priority){
                        if(!priority.includes("0"))
                            if(!priority.includes(priority0.toString()))
                                priorityx = false;
                    }

                    var milestonex = true;
                    var milestone0 = $(this).data("milestone").toString();
                    if(milestone){
                        if(milestone!="0")
                            if(milestone!=milestone0)
                                milestonex = false;
                    }

                    var projectx = true;
                    var project0 = $(this).data("project").toString();
                    if(project){
                        if(project!="0")
                            if(project!=project0)
                                projectx = false;
                    }

                    var clientx = true;
                    var client0 = $(this).data("client").toString();
                    if(client){
                        if(client!="0")
                            if(client!=client0)
                                clientx = false;
                    }



                    return  (datex && userx && employeesx && statusx && priorityx && milestonex && projectx && clientx);
                }).show();
            });

            $(document).on("reset", "#finances_filter0", function (e){
                $('.grd_view_tasks tbody tr, .kanban-item').show();
                $("#finances_filter0 .date1").val("");
                $("#finances_filter0 .date2").val("");
                $('#finances_filter0 .employees').val(0).trigger('change');
                $('#finances_filter0 .status').val(0).trigger('change');
                $('#finances_filter0 .priority').val(0).trigger('change');
                $('#finances_filter0 .userss').val(0).trigger('change');
                $('#finances_filter0 .milestone').val(0).trigger('change');
                $('#finances_filter0 .project').val(0).trigger('change');
                $('#finances_filter0 .client').val(0).trigger('change');
            });

            function sortablee(){
                $( "#sortable0").sortable({
                    connectWith: ".connectedSortable",
                    receive : function (event, ui) {
                        var status = $(this).data("status");
                        var id = $(ui.item).data("id");
                        $( "#task-wrapper" ).css("opacity", "0.7");
                        $("#task-wrapper").html($("#loading-wrapper").html());
                        $.get('{{route("company.prm.statusTask", @$company->slug)}}/'+id+'/'+status, function () {
                            $("#task-wrapper").load(window.location + " #task-div", function () {
                                goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                                sortablee();
                                $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                                $("#kanabanDiv").trigger("click");
                            });
                        });
                    },
                });
                @foreach($taskStatus as $status)
                $( "#sortable{{$status->id}}").sortable({
                    connectWith: ".connectedSortable",
                    receive : function (event, ui) {
                        var status = $(this).data("status");
                        var id = $(ui.item).data("id");
                        $( "#task-wrapper" ).css("opacity", "0.7");
                        $("#task-wrapper").html($("#loading-wrapper").html());
                        $.get('{{route("company.prm.statusTask", @$company->slug)}}/'+id+'/'+status, function () {
                            $("#task-wrapper").load(window.location + " #task-div", function () {
                                goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                                sortablee();
                                $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                                $("#kanabanDiv").trigger("click");
                            });
                        });
                    },
                });
                @endforeach
            }
            sortablee();

        });

    </script>

    <link rel="stylesheet" href="{{asset("public/plugins/gantt-chart/gantt.css")}}">
    <script src="{{asset("public/plugins/gantt-chart/gantt.js")}}"></script>

    <script>
        var months = ["يناير","فبراير","مارس","أبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"];
        var colors = ["#367fa9", "#e08e0b", "#00a65a"]
        var projects = [];
                @foreach($projects as $project)
        var tasks = [];
                @foreach($tasks as $task)
                    @if($task->daftar_id==$project->id)
                        var start = moment("{{$task->from_date}}").format('YYYY-MM-DD');
                        var end = moment("{{$task->to_date}}").format('YYYY-MM-DD');
                        var url = '<a href="{{route("company.prm.showTask", [$company->slug, $task->id])}}">{{$task->name}}</a>';
                        var task = { "id":{{$task->id}}, "name" : url, "start" : start, "end" : end, "color" : "{{($task->taskStatus)?($task->taskStatus)->color:"#8aeeff"}}"};
                        tasks.push(task);
                    @endif
                @endforeach
        var project = { "id" : {{$project->id}}, "name" : "{{$project->name}}", "series": tasks };
        projects.push(project);
        @endforeach


        $("#gantt-chart").html("<div style='height:100px;'></div>");
        $("#gantt-chart").ganttView({
            data: projects,
            monthNames: months,
            dayText: "يوم",
            daysText: "أيام"
        });
    </script>

@endsection