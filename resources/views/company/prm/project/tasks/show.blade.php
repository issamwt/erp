@extends("company.layouts.app", ["title"=>"تفاصيل المهمة"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
        <small><span>نظام إدارة المشاريع</span> <span> > </span> <span>{{@$project->name}}</span> > <span>تفاصيل المهمة</span></small>
    </section>

    <section class="content">

        <div class="tiles white p-t-15 p-l-15 p-r-15 p-b-25 bx-shade">

            <div class="row" id="showTask">

                <div class="col-md-12">

                    <div class="panel panel-primary">

                        <div class="panel-heading">
                            <strong>تفاصيل المهمة</strong>
                            <a href="{{route("company.prm.project", [@$company->slug, $project->id])}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-reply"></i> <span>العودة للمشروع</span></a>
                        </div>
                        <div class="panel-body">

                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="showtaskInfos boxita">
                                        <div id="aaaaaa">
                                            <div id="bbbbbb">
                                                <h5 class="text-center mb-20" style="font-size: 17px;">تفاصيل المهمة</h5>

                                                <div id="url" class="hidden"></div>
                                                <div class="row">
                                                    <div class="col-sm-4">إسم المهمة :</div>
                                                    <div class="col-sm-8">{{$task->name}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">حالة المهمة :</div>
                                                    <div class="col-sm-8">{{(@$task->status==0)?"غير محددة الحالة":@$task->taskStatus->name}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">إسم المشروع :</div>
                                                    <div class="col-sm-8">{{@$project->name}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">المرحلة :</div>
                                                    <div class="col-sm-8">{{($task->milestone)?@$task->milestone->name:"لا يوجد"}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">من تاريخ :</div>
                                                    <div class="col-sm-8">{{(!$task->from_date)?"لا يوجد":$task->from_date}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">الى تاريخ :</div>
                                                    <div class="col-sm-8">{{(!$task->to_date)?"لا يوجد":$task->to_date}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">عدد أيام العمل :</div>
                                                    <div class="col-sm-8">{{$task->num_days}} <span>يوم</span></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">درجة الأهمية :</div>
                                                    <div class="col-sm-8">
                                                        @if($task->priority==1) <span>عادى</span> @endif
                                                        @if($task->priority==2) <span>عاجل</span> @endif
                                                        @if($task->priority==3) <span>عاجل جدا</span> @endif
                                                        @if($task->priority==4) <span>غير عاجل</span> @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">التفاصيل :</div>
                                                    <div class="col-sm-8">{{($task->note)?$task->note:"لا يوجد"}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">المكلف :</div>
                                                    <div class="col-sm-8">
                                                        @if(@$task->user)
                                                            @if(@$task->user->image)
                                                                <img src="{{asset('storage/app/'.@$task->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                            @else
                                                                <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                            @endif
                                                            <a href="{{route('company.users.show',[$company->slug, @$task->user->id])}}">{{@$task->user->name}}</a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">المساعدون :</div>
                                                    <div class="col-sm-8">
                                                        <ul style="list-style: none; margin: 0px; padding: 0px;">
                                                            @foreach($task->users as $user)
                                                                @if(@$user)
                                                                    <li class="mb-10">
                                                                        @if(@$user->image)
                                                                            <img src="{{asset('storage/app/'.@$user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                                        @else
                                                                            <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                                        @endif
                                                                        <a href="{{route('company.users.show',[$company->slug, @$user->id])}}">{{@$user->name}}</a>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">المرفقات :</div>
                                                    <div class="col-sm-8">
                                                        @if(count($task->attachments)>0)
                                                            <ul style="">
                                                                @foreach($task->attachments as $attachment)
                                                                    <li style="margin-bottom: 5px">
                                                                        <?php $name = substr(basename($attachment->path), 11);?>
                                                                        <a style="font-size: 0.9em; text-decoration: underline;"  target="_blank" href="{{asset('storage/app/'.$attachment->path)}}">{{$name}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @else
                                                            <span>لا يوجد</span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="boxita">

                                        <h5 class="text-center mb-20" style="font-size: 17px;">المهام الفرعية</h5>

                                        @include("company.prm.project.tasks.subtasks")

                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="boxita">

                                        <h5 class="text-center mb-20" style="font-size: 17px;">التعليقات</h5>

                                        @include("company.prm.project.tasks.comments")

                                    </div>
                                </div>

                            </div>
                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <div id="wainting" class="hidden">
        <div class="text-center" style="margin: 30px auto;">
            <i class="fa fa-spinner fa-spin" style="font-size:10em; color:#0090d9"></i>
        </div>
    </div>

@endsection

@section("scripts")

    <style>
        .showtaskInfos {
            font-size: 1.1em;
        }
        .showtaskInfos .row{
            margin-bottom: 10px;
        }
        .showtaskInfos ul{
            list-style: none;
            margin: 0px;
            padding: 0px;
        }
        button, .kv-fileinput-caption{
            border-radius: 0px !important;
        }
        .showtaskInfos .comment:last-of-type hr{
            display: none;
        }
        .boxita{
            border-radius: 5px;
            padding: 5px 15px;
            margin: 10px 0px 20px;
            box-shadow: 0px 0px 6px rgba(86, 96, 117, 0.25);
            min-height: 600px;
        }
        .progress-bar{
            color: #c9c9c9;
        }
        .showtaskInfos ul {
            list-style: none;
            margin: 0px;
            padding: 0px;
        }
        .subtasks-list {
            padding: 0px;
            margin: 0px;
            list-style: none;
        }
        .subtasks-list li .subtasks-show {
            display: block;
        }
        .subtasks-list li .subtasks-edit {
            /*display: none;*/
        }
        .subtasks-list li.edit .subtasks-show {
            /*display: none;*/
        }
        .subtasks-list li.edit .subtasks-edit {
            display: block;
        }
        .comment{
            background: #eee;
            padding: 10px 0px 10px 0px;
        }
        .confirmed .xx{
            text-decoration: line-through;
        }
    </style>

    <script>
        $(function () {

            $(".select2y").select2({dir: "rtl", language: "ar", placeholder : "المكلف"});

            function reloadingsh(){

                $('.datee').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true
                });

                $('.datee2').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true,
                    @if($task->to_date and $task->to_date!="")
                    endDate : '{{$task->to_date}}'
                    @endif
                });
            }

            reloadingsh();

            $(document).on("click", "#add_sub_task", function () {
                var user_id = $("#subtasks #user_id").val();
                var task_id = $("#subtasks #task_id").val();
                var from_date = $("#subtasks #start_date_subtask").val();
                var to_date = $("#subtasks #end_date_subtask").val();
                var name = $("#subtasks #new_subtask").val();
                var token = '{{csrf_token()}}';

                if(name=="" || user_id==""){
                    alert("يجب كتابة جميع البيانات !");
                }else{
                    var url = "{{route('company.prm.add_sub_task', $company->slug)}}";
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                        type: "POST",
                        url: url,
                        data: {user_id: user_id, task_id: task_id, name: name, from_date:from_date, to_date:to_date, token:token},
                        success: function (data) {
                            $(".subtasks-list-wrapper").html(data);
                            reloadingsh();
                            goNotif('success', 'تمت إضافة المهمة الفرعية');
                            $("#subtasks #start_date_subtask").val("");
                            $("#subtasks #end_date_subtask").val("");
                            $("#subtasks #new_subtask").val("");
                            $("#subtasks #user_id").val("");
                            $("#aaaaaa").load(window.location + " #bbbbbb");
                        },
                    }).fail(function(response) {
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    });
                }
            });

            $(document).on("click", ".subtasks-show .btn-show", function () {
                var li = $(this).closest("li");
                $(".subtasks-list li").removeClass("edit");
                $(li).addClass("edit");
            });

            $(document).on("click", "#hide_task", function (e){
                e.preventDefault();
                $(".subtasks-list li").removeClass("edit");
            });

            $(document).on("click", "#confirm_task", function (e) {
                e.preventDefault();
                var li = $(this).closest("li");
                var subtask_id = $("#id_subtask", li).val();
                var task_id = $("#task_id").val();
                $(this).html('<i class="fa fa-spinner fa-spin"></i>');
                var url = "{{route('company.prm.confirm_task', $company->slug)}}";
                $.ajax({
                    headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                    type: "POST",
                    url: url,
                    data: {task_id: task_id, id: subtask_id},
                    success: function (data) {
                        $(".subtasks-list-wrapper").html(data);
                        $("#aaaaaa").load(window.location + " #bbbbbb");
                    },
                    always: function () {
                        $(this).html('<i class="fa fa-check"></i>');
                        reloadingsh();
                    }
                }).fail(function(response) {
                    goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                });
            });

            $(document).on("click", "#edit_task", function (e) {
                e.preventDefault();
                var li = $(this).closest("li");
                var subtask_id = $("#id_subtask", li).val();
                var name = $("#a_subtask", li).val();
                var user_id = $("#a_user_id", li).val();
                var task_id = $("#task_id").val();
                var from_date = $(".start_date_subtask", li).val();
                var to_date = $(".end_date_subtask", li).val();

                if(name=="" || user_id==""){
                    alert("يجب كتابة جميع البيانات !");
                }else{
                    $(this).html('<i class="fa fa-spinner fa-spin"></i>');
                    var url = "{{route('company.prm.edit_task', $company->slug)}}";
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                        type: "POST",
                        url: url,
                        data: {id: subtask_id, name: name, user_id:user_id, task_id: task_id, from_date:from_date, to_date:to_date},
                        success: function (data) {
                            $(".subtasks-list-wrapper").html(data);
                        },
                        always: function () {
                            $(this).html('<i class="fa fa-edit"></i>');
                            reloadingsh();
                        }
                    }).fail(function(response) {
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    });
                }
            });

            $(document).on("click", "#delete_stask", function (e) {
                if(confirm("هل أنت متأكد ؟")){
                    e.preventDefault();
                    var li = $(this).closest("li");
                    var subtask_id = $("#id_subtask", li).val();
                    var task_id = $("#task_id").val();
                    $(this).html('<i class="fa fa-spinner fa-spin"></i>');
                    var url = "{{route('company.prm.delete_stask', $company->slug)}}";
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                        type: "POST",
                        url: url,
                        data: {id: subtask_id, task_id: task_id},
                        success: function (data) {
                            $(".subtasks-list-wrapper").html(data);
                            $("#aaaaaa").load(window.location + " #bbbbbb");
                        },
                        always: function () {
                            $(this).html('<i class="fa fa-edit"></i>');
                            reloadingsh();
                        }
                    }).fail(function(response) {
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    });
                }
            });

            $(document).on("submit", "#addTaskComment", function (e) {
                e.preventDefault();
                var url = $('#showTask #url').html();

                if($("#showTask .comment_id").val()!=""){
                    var formData = new FormData($(this)[0]);
                    console.clear();
                    $('#showTask #commentsss').html($("#wainting").html());
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                        url: "{{route('company.prm.saveTaskComment', $company->slug)}}",
                        data: formData,
                        type: 'post',
                        async: false,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success:function(response){
                            if(response.errors){
                                $.each(response.errors, function (i, e) {
                                    goNotif('error', e);
                                    console.log(e);
                                });
                                return;
                            }
                            goNotif('success', 'تمت إضافة التعليق');
                        }
                    }).fail(function(response) {
                        console.clear();
                        console.log("----------------");
                        console.log(response);
                        console.log("----------------");
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    }).always(function () {
                        $('#showTask #commentsss').load( window.location + " #comments-div");
                    });
                }else{
                    goNotif('error', 'يجب كتابة نص التعليق !');
                }
            });

            $(document).on("click", ".add_chkoun", function (e){
                e.preventDefault();
                var x = $(this).closest(".col-sm-12").find(".project_members");
                $(x).fadeToggle();
            });

            $(document).on("click", ".comment_id", function (e){
                $(".project_members").fadeOut();
            });

            $(document).on("click", ".project_members li", function (e){
                e.preventDefault();
                var name = " @"+$(this).data("name");
                var textarea = $(this).parent().parent().find(".comment_id");
                var text = $(textarea).val();
                text = text + name + " ";
                $(".comment_id").val(text);
            });

            $(document).on("click", ".delete-comment", function (e){
                e.preventDefault();
                var id = $(this).data("comment");
                if(confirm('هل أنت متأكد ؟')){
                    $('#showTask #commentsss').html($("#wainting").html());
                    $.ajax({
                        url: "{{route('company.prm.deleteTaskComment', $company->slug)}}/"+id,
                        type: 'get',
                        success:function(response){
                            goNotif('success', 'تمت حذف التعليق');
                        }
                    }).fail(function(response) {
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    }).always(function () {
                        $('#showTask #commentsss').load( window.location + " #comments-div");
                    });
                }
            });

            $(document).on("click", ".reply-comment", function (e) {
                e.preventDefault();
                var id = $(this).data("comment");
                $("#addReplyTaskComment"+id).slideToggle();
            });

            $(document).on("submit", ".addReplyTaskComment", function (e) {
                e.preventDefault();
                var url = $('#showTask #url').html();

                if($(".comment_id", this).val()!=""){
                    var formData = new FormData($(this)[0]);
                    console.clear();
                    $('#showTask #commentsss').html($("#wainting").html());
                    $.ajax({
                        headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                        url: "{{route('company.prm.saveReplyTaskComment', $company->slug)}}",
                        data: formData,
                        type: 'post',
                        async: false,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success:function(response){
                            if(response.errors){
                                $.each(response.errors, function (i, e) {
                                    goNotif('error', e);
                                    console.log(e);
                                });
                                return;
                            }
                            goNotif('success', 'تمت إضافة التعليق');
                        }
                    }).fail(function(response) {
                        console.clear();
                        console.log("----------------");
                        console.log(response);
                        console.log("----------------");
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    }).always(function () {
                        $('#showTask #commentsss').load( window.location + " #comments-div");
                    });
                }else{
                    goNotif('error', 'يجب كتابة نص التعليق !');
                }
            });

        });
    </script>

@endsection