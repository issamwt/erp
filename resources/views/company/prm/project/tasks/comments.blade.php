
<?php
function ckounName($name){
    return str_replace(" ", "_", $name);
}
function chkoubget($string){
    $temp = explode(" ", $string);
    $return = "";
    foreach ($temp as $word){
        if (substr($word, 0, 1) === '@') {
            $word = "<b style='color: #dd399b'>$word</b>";
        }
        $return.= " ".$word;
    }
    return $return;
}
?>

<div class="row" id="commentsss">
    <div id="comments-div">

        <div class="col-sm-12">
            <form action="{{route('company.prm.saveTaskComment', $company->slug)}}" id="addTaskComment" class="row" method="post" enctype="multipart/form-data" style="margin-bottom: -10px">
                @csrf
                <input type="hidden" name="project_id" value="{{$task->project_id}}">
                <input type="hidden" name="task_id" value="{{$task->id}}">

                <div class="col-sm-12" style="position: relative">
                    <textarea name="comment" rows="6" class="form-control comment_id" style="background-color: #f6f8f9;border-radius: 0px;"></textarea>
                    <a href="#" class="add_chkoun">@</a>
                    <ul class="project_members">
                        @foreach($project->users as $u)
                            <li data-name="{{ckounName($u->name)}}">{{$u->name}}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
                <div class="col-sm-12" style="margin-top: 10px">
                    <div class="form-group" style="margin-bottom: 10px">
                        <div class="file-loading">
                            <input name="attachments[]" id="kv-explorerC" type="file" class="form-control" multiple>
                        </div>
                        <small>jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary" style="background-color: #1377cd;"><i class="fa fa-paper-plane"></i> حفظ التعليق</button>
                </div>
            </form>
            <hr class="mt-20 mb-10">
        </div>

        <div class="col-sm-12">
            <hr>
            @if(count(@$task->comments)<=0)
                <h5 class="text-center">لا توجد تعليقات</h5>
            @else
                <h5 class="text-center">قائمة التعليقات</h5>
                <hr>
                <div class="">
                    <div class="col-sm-12">
                        @foreach(@$task->comments as $comment)
                            <div class="comment row mb-15">
                                <a class="delete-comment" href="#" data-comment="{{$comment->id}}" data-toggle="tooltip" title="حذف التعليق"><i class="fa fa-remove"></i></a>
                                <a class="reply-comment" href="#" data-comment="{{$comment->id}}" data-toggle="tooltip" title="الرد على التعليق"><i class="fa fa-reply"></i></a>
                                <div class="col-sm-2">
                                    @if(@$comment->user)
                                        @if(@$comment->user->image)
                                            <img src="{{asset('storage/app/'.@$comment->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                        @else
                                            <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                        @endif
                                    @endif
                                </div>
                                <div class="col-sm-10">
                                    <span>
                                        <a style="color:#337ab7; display: inline-block; font-weight: bold" href="{{route('company.users.show',[$company->slug, @$comment->user->id])}}">{{@$comment->user->name}}</a>
                                        <span style="font-size: 0.9em">
                                            <span style="display: block">{{$comment->created_at}}</span>
                                        </span>
                                    </span>
                                    <p style="font-size: 0.9em; margin-top: 10px">{!! chkoubget($comment->texte) !!}</p>
                                    <ul style="list-style: none;margin: 0px;padding: 0px;">
                                        @foreach($comment->attachments as $attachment)
                                            <li style="margin-bottom: 0px">
                                                <?php $name = substr(basename($attachment->path), 11);?>
                                                <a style="font-size: 0.9em; text-decoration: underline;"  target="_blank" href="{{asset('storage/app/'.$attachment->path)}}">{{$name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <form action="{{route('company.prm.saveReplyTaskComment', $company->slug)}}" id="addReplyTaskComment{{$comment->id}}" class="addReplyTaskComment col-sm-12" method="post" enctype="multipart/form-data" style="margin-bottom: -10px">
                                    @csrf
                                    <input type="hidden" name="comment_id" value="{{$comment->id}}">

                                    <div class="col-sm-12" style="position: relative">
                                        <textarea name="comment" rows="4" class="form-control comment_id" style="background-color: #f6f8f9;border-radius: 0px;"></textarea>
                                        <a href="#" class="add_chkoun">@</a>
                                        <ul class="project_members">
                                            @foreach($project->users as $u)
                                                <li data-name="{{ckounName($u->name)}}">{{$u->name}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <br>
                                    <div class="col-sm-12" style="margin-top: 10px">
                                        <div class="form-group" style="margin-bottom: 10px">
                                            <div class="file-loading">
                                                <input name="attachments[]" id="kv-explorerC" type="file" class="form-control" multiple>
                                            </div>
                                            <small>jpg,jpeg,png,bmp,pdf,doc,docx,txt,csv,zip</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-10">
                                        <button type="submit" class="btn btn-primary btn-xs" style="background-color: #1377cd;"><i class="fa fa-paper-plane"></i> حفظ التعليق</button>
                                    </div>
                                </form>
                                @if(count($comment->children)>0)
                                    <div class="clearfix"></div>
                                    <h5 style="margin: 10px 0px -10px;padding-right: 15px;color: #414141">الردود : </h5>
                                    <div class="clearfix"></div>
                                    @foreach(@$comment->children as $c)
                                        <div class="col-sm-12"><hr style="border-color: #b1b1b1"></div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <div class="comment child row">
                                                <a class="delete-comment" href="#" data-comment="{{$c->id}}" data-toggle="tooltip" title="حذف التعليق"><i class="fa fa-remove"></i></a>
                                                <div class="col-sm-2">
                                                    @if(@$c->user)
                                                        @if(@$c->user->image)
                                                            <img src="{{asset('storage/app/'.@$c->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                        @else
                                                            <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="col-sm-10">
                                                    <span>
                                                        <a style="color:#337ab7; display: inline-block; font-weight: bold" href="{{route('company.users.show',[$company->slug, @$c->user->id])}}">{{@$c->user->name}}</a>
                                                        <span style="font-size: 0.9em">
                                                            <span style="display: block">{{$c->created_at}}</span>
                                                        </span>
                                                    </span>
                                                    <p style="font-size: 0.9em; margin-top: 10px">{!! chkoubget($c->texte) !!}</p>
                                                    <ul style="list-style: none;margin: 0px;padding: 0px;">
                                                        @foreach($c->attachments as $attachment)
                                                            <li style="margin-bottom: 0px">
                                                                <?php $name = substr(basename($attachment->path), 11);?>
                                                                <a style="font-size: 0.9em; text-decoration: underline;"  target="_blank" href="{{asset('storage/app/'.$attachment->path)}}">{{$name}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>
                                    @endforeach
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>

            @endif
        </div>

    </div>
</div>

<style>
    .add_chkoun{
        position: absolute;
        bottom: 0px;
        left: 15px;
        background: #eeeeee;
        border: 1px solid #d5d5d5;
        color: #3c8dbc;
        transition: all ease 0.2s;
        width: 30px;
        height: 30px;
        font-size: 1.3em;
        text-align: center;
        font-weight: bolder;
        line-height: 29px;
    }
    .add_chkoun:hover{
        background: #d5d5d5;
        color: #3c8dbc;
    }
    .project_members {
        display: none;
        z-index: 9;
        min-width: 170px;
        background: #fff;
        border: 1px solid #eee;
        list-style: none;
        border-radius: 2px;
        position: absolute;
        left: 45px;
        bottom: -10px;
        padding: 4px;
        padding-bottom: 0px;
    }
    .project_members li {
        cursor: pointer;
        background: #eee;
        padding: 6px 4px;
        border-radius: 4px;
        margin-bottom: 4px;
        transition: all ease 0.2s;
    }
    .project_members li:hover {
        background: #F1F3F6;
        color: #4e5e6a;
    }
    .comment{
        position: relative;
    }
    .delete-comment{
        position: absolute;
        color: crimson;
        top: 6px;
        left: 6px;
        width: 20px;
        height: 20px;
        line-height: 20px;
        background: #f5f5f5;
        border-radius: 1px;
        text-align: center;
        z-index: 999;
    }
    .delete-comment:hover{
        color: crimson;
    }
    .reply-comment{
        position: absolute;
        color: #337ab7;
        top: 6px;
        left: 32px;
        width: 20px;
        height: 20px;
        line-height: 20px;
        background: #f5f5f5;
        border-radius: 1px;
        text-align: center;
        z-index: 999;
    }
    .reply-comment:hover{
        color: #337ab7;
    }
    .addReplyTaskComment{
        display: none;
    }
    .comment.child{
        padding: 0px;
    }
</style>

