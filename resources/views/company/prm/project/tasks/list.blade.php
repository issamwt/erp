@if(count(@$task->subtasks)>0)
    <?php
    $total = count(@$task->subtasks);
    $confirmed = 0;
    foreach (@$task->subtasks as $subtask):
        if($subtask->status==1)
            $confirmed++;
    endforeach;
    if($confirmed==0)
        $percentage = 0;
    else
        $percentage = intval($confirmed/$total*100);
    ?>
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="<?=$percentage;?>"
             aria-valuemin="0" aria-valuemax="100" style="width:<?=$percentage;?>%">
            <?=$percentage;?>%
        </div>
    </div>
    <ul class="subtasks-list">
        <?php foreach (@$task->subtasks as $subtask): ?>
        <li class="<?=($subtask->status==1)?"confirmed":"";?>" style="margin-bottom: 20px; border-bottom: 1px solid #eee; padding-bottom: 20px;">
            <input type="hidden" id="id_subtask" value="<?=$subtask->id;?>">
            <!--
            <div class="subtasks-show">
                <a class="btn btn-default btn-xs btn-show" href="#" style="float:left"><i class="fa fa-edit" style="font-size: 0.9em"></i> <span style="font-size: 0.9em">تعديل</span></a>
                <p class="mt-0 mb-10"><strong><b class="xx" style="font-size: 1.2em;">{{$subtask->name}}</b> @if($subtask->status==1) <small>(منجزة)</small> @endif</strong></p>
                <p class="mt-0">
                    <b>
                        @if(@$subtask->user)
                            @if(@$subtask->user->image)
                                <img src="{{asset('storage/app/'.@$subtask->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%; height: 25px; width: 25px;">
                            @else
                                <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%; height: 25px; width: 25px;">
                            @endif
                        @endif
                        <small style="color: #515a63; font-size: 0.8em;">{{@$subtask->user->name}}</small>
                    </b>
                </p>
                <div class="row">
                    <div class="col-sm-6"><span>تاريخ البداية : </span><span style="display:inline-block;"><?=$subtask->from_date;?></span></div>
                    <div class="col-sm-6"><span>تاريخ الإنتهاء : </span><span style="display:inline-block;"><?=$subtask->to_date;?></span></div>
                </div>
            </div>
            -->
            <div class="subtasks-edit">
                <input id="a_subtask" class="@if($subtask->status==1) line @endif form-control mb-5 no-border b" placeholder="إسم المهمة الفرعية" value="{{$subtask->name}}">
                <div class="row">
                    <div class="col-sm-6 mb-5">
                        <input placeholder="تاريخ البداية" value="{{$subtask->from_date}}" name = "start_date"  class = "form-control datee start_date_subtask no-border" autocomplete="off" style = "box-shadow: none;border-radius: 0px;">
                    </div>
                    <div class="col-sm-6 mb-5">
                        <input placeholder="تاريخ الإنتهاء" value="{{$subtask->to_date}}" name = "end_date"  class = "form-control datee2 end_date_subtask no-border"  autocomplete="off" style = "box-shadow: none;border-radius: 0px;">
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-sm-12">
                        <select placeholder="المكلف" id = "a_user_id" name = "a_user_id"  class = "form-control no-border"  autocomplete="off" style = "box-shadow: none;border-radius: 0px;">
                            <option value="">المكلف</option>
                            @foreach($project->users as $u)
                                <option value="{{$u->id}}" @if($u->id==$subtask->user_id) selected @endif>{{$u->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <button class="btn btn-default" id="confirm_task" type="button" data-toggle="tooltip" title="@if($subtask->status==1) تغيير الحالة إلى غير منجزة  @else تغيير الحالة إلى منجزة @endif" style="border-radius:0px;border: 1px solid #1ccacc;">
                    @if($subtask->status==1)
                        <i class="fa fa-reply"></i>
                    @else
                        <i class="fa fa-check"></i>
                    @endif
                </button>
                <button class="btn btn-default" id="edit_task" type="button" data-toggle="tooltip" title="تعديل" style="border-radius:0px"><i class="fa fa-edit"></i></button>
                <button class="btn btn-default" id="delete_stask" type="button" data-toggle="tooltip" title="حذف"><i class="fa fa-trash"></i></button>
                <!--<button class="btn btn-default" id="hide_task" type="button" data-toggle="tooltip" title="إخفاء"><i class="fa fa-times"></i></button>-->
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
@else
    <div style="padding-bottom: 10px; border-bottom: 1px solid #eee; margin-bottom: 10px;">لا توجد مهام فرعية</div><br>
@endif

<style>
    .line.no-border{
        text-decoration: line-through;
    }
    .b.no-border{
        font-weight: bold;
    }
    .no-border{
        border: 1px solid #eee;
        border: none;
    }
    .no-border:focus{
        border: 1px solid #eee !important;
    }
</style>