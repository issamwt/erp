@extends("company.layouts.app", ["title"=>"إضافة مهمة"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
        <small><span>نظام إدارة المشاريع</span> <span> > </span> <span>{{@$project->name}}</span> > <span>إضافة مهمة</span></small>
    </section>

    <section class="content">

        <div class="tiles white p-t-15 p-l-15 p-r-15 p-b-25 bx-shade">

            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-primary">

                        <div class="panel-heading">
                            <strong>إضافة مهمة</strong>
                            <a href="{{route("company.prm.project", [@$company->slug, $project->id])}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-reply"></i> <span>العودة للمشروع</span></a>
                        </div>
                        <div class="panel-body">

                            <form action="{{route('company.prm.saveTask', @$company->slug)}}" method="post" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="project_id" value="{{$project->id}}">

                                <div class="row">

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label><span>اسم المهمة</span> <span class="red">*</span></label>
                                            <input type="text" class="form-control" name="name" value="{{old('name')}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label>المرفقات</label>
                                            <div class="file-loading">
                                                <input name="attachments[]" type="file" class="form-control kv-explorer-discussion">
                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label><span>من تاريخ</span> <span class="red">*</span></label>
                                            <input type="text" class="form-control datee from_date" autocomplete="off" name="from_date" value="{{old('from_date')}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label><span>الى تاريخ</span> <span class="red">*</span></label>
                                            <input type="text" class="form-control datee to_date" autocomplete="off" name="to_date" value="{{old('to_date')}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label><span>عدد أيام العمل</span> <span class="red">*</span></label>
                                            <input type="number" min="1" autocomplete="off" class="form-control num_days" name="num_days" value="{{old('num_days')}}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label><span>درجة الأهمية</span> <span class="red">*</span></label>
                                            <select class="select-hide full-width" name="priority" required>
                                                <option value=""></option>
                                                <option value="1" @if(old('priority')==1) selected @endif>عادى</option>
                                                <option value="2" @if(old('priority')==2) selected @endif>عاجل</option>
                                                <option value="3" @if(old('priority')==3) selected @endif>عاجل جدا</option>
                                                <option value="4" @if(old('priority')==4) selected @endif>غير عاجل</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>التفاصيل</label>
                                            <textarea class="form-control" rows="5" name="details">{{old('details')}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label><span>المكلف</span> <span class="red">*</span></label>
                                            <div class="input-group" style="width: 100%">
                                                <select class="select2x form-control" name="charged" required>
                                                    <option value=""></option>
                                                    @foreach($project->users as $user)
                                                        <option value="{{$user->id}}" @if(old('charged')==$user->id) selected @endif>{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label>المساعدون</label>
                                            <div class="input-group" style="width: 100%">
                                                <select class="select2x form-control" name="collaborators[]" multiple autocomplete="off">
                                                    <option value=""></option>
                                                    @foreach($project->users as $user)
                                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-xs-12">
                                        <div id="milestones-wrapper">
                                            <div id="milestones">
                                                <div class="form-group">
                                                    <label>المرحلة</label>
                                                    <div class="input-group">
                                                        <select class="select2x form-control" name="milestone_id" id="milestone_id">
                                                            <option value=""></option>
                                                            @foreach($milestones as $milestone)
                                                                <option value="{{$milestone->id}}">{{$milestone->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary btn-open" data-toggle="tooltip" title="إضافة مرحلة" data-target="#addMilestone"> <i class="fa fa-plus"></i> </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="addMilestone" class="formi">
                                            <h4 class="modal-title mb-10">إضافة مرحلة</h4>
                                            <div autocomplete="off">
                                                <div class="row">
                                                    <div class="col-md-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>اسم المرحلة <span class="red">*</span></label>
                                                            <input type="text" class="form-control" id="name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>من تاريخ</label>
                                                            <input type="text" class="form-control datee" id="from_date" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>الى تاريخ</label>
                                                            <input type="text" class="form-control datee" id="to_date" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>التفاصيل</label>
                                                            <textarea class="form-control" rows="5" id="details"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <button type="button" id="addMilestoneBtn" class="btn btn-primary"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                    </div>
                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section("scripts")

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.min.js"></script>

    <script>

        $(function () {

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $("#addMilestoneBtn").on("click", function (e) {
                e.preventDefault();
                var button = $(this);
                var name=$("#name").val();
                var from_date=$("#from_date").val();
                var to_date=$("#to_date").val();
                var details=$("#details").val();
                if(name==""){
                    alert("يجب كتابة إسم المرحلة !");
                }else{
                    //toastrr(type, msg)
                    $(button).prop("disabled", true);
                    $.post( "{{route('company.prm.saveMilestonet', $company->slug)}}", {project_id:{{$project->id}}, name: name, from_date: from_date, to_date: to_date, details: details, _token:"{{csrf_token()}}" } , function (data) {
                        $("#milestones-wrapper").load(window.location + " #milestones", function () {
                            $("#milestone_id").val(data);
                            $('#addMilestone').slideUp();
                            $("#name").val("");
                            $("#from_date").val("");
                            $("#to_date").val("");
                            $("#details").val("");
                            goNotif("success", "تم إضافة المرحلة" );
                        });
                    }).fail(function() {
                        goNotif("error", "لقد وقع خطأ ما, الرجاء المحاولة من جديد !" );
                    }).always(function () {
                        $(button).prop("disabled", false);
                    });
                }

            });

            $(".from_date").on("change", function () {
                var from = $(".from_date").val();
                var to = $(".to_date").val();
                var num = $(".num_days").val();
                if(from!=""){
                    if(to!=""){
                        $(".num_days").val(diffdays(from, to));
                    }else{
                        if(num!="" && num!=0){
                            $(".to_date").val(adddays(from, num));
                        }
                    }
                }
            });

            $(".to_date").on("change", function () {
                var from = $(".from_date").val();
                var to = $(".to_date").val();
                var num = $(".num_days").val();
                if(to!=""){
                    if(from!=""){
                        $(".num_days").val(diffdays(from, to));
                    }else{
                        if(num!="" && num!=0){
                            $(".from_date").val(subdays(to, num));
                        }
                    }
                }
            });

            $(".num_days").on("change", function () {
                var from = $(".from_date").val();
                var to = $(".to_date").val();
                var num = $(".num_days").val();
                if(num!="" && num!=0){
                    if(from!=""){
                        $(".to_date").val(adddays(from, num));
                    }else{
                        if(to!=""){
                            $(".from_date").val(subdays(to, num));
                        }
                    }
                }
            });

            function diffdays(start, end) {
                var start = moment(start, "YYYY-MM-DD");
                var end = moment(end, "YYYY-MM-DD");
                return moment.duration(end.diff(start)).asDays();
            }

            function adddays(start, num) {
                return moment(start, "YYYY-MM-DD").add(num, 'days').format("YYYY-MM-DD");
            }

            function subdays(to, num) {
                return moment(to, "YYYY-MM-DD").subtract(num, 'days').format("YYYY-MM-DD");
            }
        });

    </script>

    <script>
        $(function () {
            $(document).on("change", ".kv-explorer-discussion", function (e) {
                if(e.target.files.length>0){
                    $(this).closest(".file-loading").append('<div class="deleteme"></div><input type="file" name="attachments[]" class="form-control kv-explorer-discussion mt-10">');
                }
            });
            $(document).on("click", ".deleteme", function () {
                $(this).next("input").remove();
                $(this).remove();
            });
        });
    </script>

    <style>
        .deleteme{
            position: relative;
        }
        .deleteme:after{
            cursor: pointer;
            position: absolute;
            content: "حذف";
            background: red;
            color: #fff;
            padding: 0px 5px;
            left: 10px;
            top: 16px;
        }
    </style>

@endsection