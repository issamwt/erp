<div id="subtasks-wrapper mt-25">
    <div id="subtasks">

        <div class="subtasks-list-wrapper">
            @include("company.prm.project.tasks.list")
        </div>

        <b>إضافة مهمة فرعية :</b>
        <input type="hidden" id="task_id" value="<?=@$task->id;?>">
        <div class="mb-10">
            <input type="text" id="new_subtask" class="form-control" placeholder="إسم المهمة الفرعية" style="border-radius: 0px">
        </div>
        <div class="row mt-10 mb-10">
            <div class="col-sm-6">
                <input placeholder="تاريخ البداية" id = "start_date_subtask" name = "start_date"  class = "form-control datee" autocomplete="off" style = "box-shadow: none;border-radius: 0px;">
            </div>
            <div class="col-sm-6">
                <input placeholder="تاريخ الإنتهاء" id = "end_date_subtask" name = "end_date"  class = "form-control datee2"  autocomplete="off" style = "box-shadow: none;border-radius: 0px;">
            </div>
        </div>
        <div class="input-group mb-10">
            <select placeholder="المكلف" id = "user_id" name = "user_id"  class = "form-control"  autocomplete="off" style = "box-shadow: none;border-radius: 0px;">
                <option value="">المكلف</option>
                @foreach($project->users as $u)
                    <option value="{{$u->id}}">{{$u->name}}</option>
                @endforeach
            </select>
            <div class="input-group-btn">
                <button class="btn btn-primary" id="add_sub_task" type="button">إضافة</button>
            </div>
        </div>

    </div>
</div>
