<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold pull-right mt-5 mb-5"><i class="fa fa-comments-o"></i> نقاشات المشروع</h3>
        <span class="pull-left m-t-10">
            @if(permissions_prm("add_discussion"))
                <a href="{{route('company.prm.addDiscussion', [$company->slug, $project->id])}}" class="btn btn-primary pull-left"> إضـافة نقاش </a>
            @endif
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">

    <div class="col-sm-12">

        <ul id="tabx">
            <li class="active"><a href="#tab4TeamWorkx" id="tab4TeamWorkBtn" class="btn btn-primary">نقاشات فريق العمل</a></li>
            <li class=""><a href="#tab4Clientx" id="tab4ClientBtn" class="btn btn-primary">نقاشات العميل</a></li>
            <li class="clearfix"></li>
        </ul>

        <div class="clearfix"></div>

        <div class="mt-10">
            <div class="radio inline">
                <label style="padding: 0px; padding-right: 20px"><input type="radio" class="optradiox" name="optradiox" value="1" checked> موجه للكل</label>
            </div>
            <div class="radio inline">
                <label style="padding: 0px"><input type="radio" class="optradiox" name="optradiox" value="2"> موجه لي</label>
            </div>
        </div>


        <div id="contentx">

            <div id="tab4TeamWorkx" class="opsigui">
                <h5 class="mb-20">نقاشات فريق العمل</h5>

                <div class="tab-pane active" id="tab4TeamWork">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="cbp_tmtimeline">
                                <?php $i=0; ?>
                                @foreach($discussions as $discussion)
                                    @if($discussion->type==0)
                                        <?php $data = ["type"=>0]; $i++; ?>
                                        @include('company.prm.project.discussions.single', $data)
                                    @endif
                                @endforeach
                                @if($i==0)
                                    <h5>لا توجد نقاشات حاليا</h5>
                                @endif
                            </ul>
                        </div>
                    </div>

                </div>

            </div>

            <div id="tab4Clientx" class="opsigui">
                <h5 class="mb-20">نقاشات العميل</h5>

                <div class="tab-pane active" id="tab4Client">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="cbp_tmtimeline">
                                <?php $j=0; ?>
                                @foreach($discussions as $discussion)
                                    @if($discussion->type==1)
                                        <?php $data = ["type"=>1]; $j++; ?>
                                        @include('company.prm.project.discussions.single', $data)
                                    @endif
                                @endforeach
                                @if($j==0)
                                    <h5>لا توجد نقاشات حاليا</h5>
                                @endif
                            </ul>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<style>
    #tabx{
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    #tabx li{
        float: right;
        margin-left: 10px;
    }
    #tabx li.active a{
        background-color: #204d74;
        border-color: #122b40;
    }
    #contentx{
        padding-top: 30px;
        min-height: 500px;
    }
    #contentx .opsigui{
        display: none;
    }
    #contentx #tab4TeamWorkx{
        display: block;
    }
    .cbp_tmtimeline {
        margin: 30px 0 0 0;
        padding: 0;
        list-style: none;
        position: relative;
    }
    .rtl .cbp_tmtimeline:before {
        right: auto;
        left: 20px;
        margin-left: -10px;
    }
    .cbp_tmtimeline:before {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        width: 10px;
        background: #f2f4f6;
        right: 20px;
        margin-right: -10px;
    }
    .cbp_tmtimeline li {
        position: relative;
        padding-left: 6px;
        line-height: 20px;
    }
    .rtl .cbp_tmtimeline > li .cbp_tmtime {
        padding-right: 60px;
        color: #606060;
        font-weight: bold;
        padding-left: 0px;
        text-align: right;
    }
    .cbp_tmtimeline > li .cbp_tmtime{
        display: block;
        width: 100%;
        padding-right: 60px;
        color: #606060;
        font-weight: bold;
        position: absolute;
        /* font-family: 'Arial'; */
        font-family: 'Droid Arabic Kufi';
        text-align: right;
    }
    .cbp_tmtimeline > li .cbp_tmtime .date {
        font-size: 1em;
        color: #505458;
        margin-bottom: 5px;
        /* display: block; */
    }
    .cbp_tmtimeline > li .cbp_tmicon.primary {
        background: #0aa699;
        box-shadow: 0 0 0 8px #33b5aa;
        color: #ffffff;
    }
    .rtl .cbp_tmtimeline > li .cbp_tmicon {
        left: auto;
        right: 20%;
        /* margin: 0 0 0 -25px; */
        margin: 0 -25px 0 0;
    }
    .cbp_tmtimeline > li .cbp_tmicon{
        width: 40px;
        height: 40px;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 37px;
        -webkit-font-smoothing: antialiased;
        position: absolute;
        color: #7c8694;
        background: #ffffff;
        border-radius: 50%;
        box-shadow: 0 0 0 8px #ffffff;
        text-align: center;
        right: 35px;
        top: 0;
        margin: 0 -25px 0 0;
        font-size: 1.3em;
    }
    .cbp_tmtimeline > li:nth-child(odd) .cbp_tmlabel {
        background: #f5f5f5;
    }
    .cbp_tmtimeline > li .cbp_tmlabel {
        margin: 0 25% 15px 0;
        background: #f5f5f5;
        font-size: 13px;
        line-height: 1.4;
        position: relative;
    }
    .p-l-30 {
        padding-left: 30px;
    }
    .p-r-20 {
        padding-right: 20px;
    }
    .p-t-10 {
        padding-top: 10px;
    }
    .tiles.grey {
        background-color: #e9ecee;
    }
    .tiles {
        background-color: #bcbcbc;
        color: #ffffff;
        position: relative;
    }
    .cbp_tmtimeline .action-links {
        margin: 4px 0 0;
        list-style: none;
        font-size: 13px;
        padding: 0;
        float: right;
    }
    .unstyled {
        list-style: none !important;
    }
    .deleteDiscussion {
        margin-top: -5px;
        margin-left: -20px;
        color: crimson;
        font-size: 1.2em;
        cursor: pointer;
    }
    .m-t-15 {
        margin-top: 15px !important;
    }
    .client-pic {
        border-radius: 100px 100px 100px 100px;
        display: inline-block;
        height: 35px;
        overflow: hidden;
        width: 35px;
        float: right;
    }
    .client-name-project {
        line-height: 3;
        margin-right: 10px;
        font-size: 14px;
    }
    .cbp_tmtimeline > li:nth-child(odd) .cbp_tmtime span:last-child {
        color: #505458;
    }
    .cbp_tmtimeline > li .cbp_tmtime .time {
        font-size: 1.1em;
        color: #505458;
        display: block;
    }
    .text-success {
        color: #0aa699 !important;
    }
    .grid.simple .grid-title.no-border {
        border: 0px;
        border-bottom: 0px;
    }
    .grid.simple.grey .grid-title {
        padding: 14px 15px 7px 15px;
        border-bottom: 1px solid #eee;
        color: #282323 !important;
        margin-bottom: 0px;
        border: 1px solid #dddddd;
        /* border-bottom: 1px solid #dddddd; */
        border-bottom: 0px;
    }
    .grid.simple .grid-title {
        padding: 14px 15px 7px 15px;
        border-bottom: 1px solid #eee;
        color: #282323 !important;
        margin-bottom: 0px;
        border: 1px solid #dddddd;
        /* border-bottom: 1px solid #dddddd; */
        border-bottom: 0px;
        /* overflow: hidden; */
    }
</style>