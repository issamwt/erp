<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold pull-right mt-5 mb-5"><i class="fa fa-clipboard"></i> مخطط المهام</h3>
        <span class="pull-left m-t-10">
            @if(permissions_prm("add_task"))
                <a href="{{route('company.prm.addTask', [$company->slug, $project->id])}}" class="btn btn-primary pull-left"> إضـافة مهمة </a>
            @endif
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">

    {{--
    <div class="col-sm-12">

        <a href="javascript:void(0)" onclick="$('#gantt_filter').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

        <form id="gantt_filter" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

            <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

            <div class="col-sm-12 mb-10">
                <input type="text" class="form-control datee gant_date1" autocomplete="off" name="from" placeholder="من تاريخ">
            </div>

            <div class="col-sm-12 mb-10">
                <input type="text" class="form-control datee gant_date2" autocomplete="off" name="to" placeholder="إلى تاريخ">
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x ganttmilestones" autocomplete="off">
                    <option value="all">جميع المراحل</option>
                    @foreach($milestones as $milestone)
                        <option value="{{$milestone->id}}">{{$milestone->name}}</option>
                    @endforeach
                    <option value="0">بدون مرحلة</option>
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x ganttusers" autocomplete="off">
                    <option value="0">جميع الموظفين</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x ganttstatus" autocomplete="off">
                    <option value="0">جميع الحالات</option>
                    @foreach($taskStatus as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-block">فلتر</button>
            </div>

            <div class="col-sm-6">
                <button type="reset" class="btn btn-info btn-block">مسح</button>
            </div>

            <div class="clearfix"></div>

        </form>

    </div>

    <div class="col-sm-12"><hr></div>
    --}}

    <div class="col-md-12 ">

        <div id="gantt_here" style="width:100%; height:100%; min-height: 250px; display: none"></div>

        <div id="gantt">
            <div class="w100p pt10" style="min-height: 500px;">
                <div id="gantt-chart" style="width: 100%;"></div>
                <div id="scroll-to-me"></div>
            </div>
        </div>

        <div class="col-md-12">
            @foreach($taskStatus as $status)
                <div class="mt-5">
                    <div class="case-status-indcator case-bg-blue inline" style="background: {{$status->color}}"></div>
                    <span>{{$status->name}}</span>
                </div>
            @endforeach
        </div>

    </div>

</div>


<!--
@foreach($tasks as $task)
    @if($task->from_date and $task->to_date)
        <?php
            $end_time       = \Carbon\Carbon::createFromFormat("Y-m-d",@$task->to_date);
            $start_time     = \Carbon\Carbon::createFromFormat("Y-m-d",@$task->from_date);
            $totalDuration  = @$end_time->diffInDays(@$start_time);
            $end_time       = date('d-m-Y', strtotime($end_time));
            $start_time     = date('d-m-Y', strtotime($start_time));
        ?>
        <div class="gantt-data">
            {"id":{{$task->id}}, "progress": 0.4, "milestone":"{{(@$task->milestone->id)?@$task->milestone->id:0}}", "charged" :"{{@$task->user_id}}","ml" :"{{@$task->charged[0]->name}}", "status":"{{(@$task->taskStatus->id)?@$task->taskStatus->id:0}}", "text":"{{$task->name}}", "name":"{{$task->name}}", "color":"{{(@$task->taskStatus->color)?@$task->taskStatus->color:"#3db9d3"}}", "start_date":"{{$start_time}}", "end_date":"{{$end_time}}", "open": true, "link":"{{route('company.prm.editTask', [$company->slug, $task->id])}}", "href":"{{route('company.prm.showTask', [$company->slug, $task->id])}}"}
        </div>
        @foreach($task->subtasks as $subtask)
            @if($task->from_date and $task->to_date)
            <div class="gantt-data">
                {"id":{{$subtask->id}}, "milestone":"{{(@$task->milestone->id)?@$task->milestone->id:0}}", "charged" :"{{@$task->user_id}}","ml" :"{{@$task->charged[0]->name}}", "status":"{{(@$task->taskStatus->id)?@$task->taskStatus->id:0}}", "text":"#{{$subtask->name}}", "name":"#{{$subtask->name}}", "color":"{{(@$task->taskStatus->color)?@$task->taskStatus->color:"#3db9d3"}}", "start_date":"{{$subtask->from_date}}", "end_date":"{{$subtask->to_date}}", "open": true}
            </div>
            @endif
        @endforeach
    @endif
@endforeach


<style>
    .gantt-data{
        display: none;
        direction: rtl;
    }
</style>
-->
