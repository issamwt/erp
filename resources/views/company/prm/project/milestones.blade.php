<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold mt-5 mb-5 pull-right"><i class="fa fa-th-large"></i> المراحل</h3>
        <span class="pull-left m-t-10">
            @if(permissions_prm("add_milestone"))
                <a href="{{route('company.prm.addMilestone', [$company->slug, $project->id])}}" class="btn btn-primary pull-left" > إضـافة مرحلة </a>
            @endif
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <table class="table table-bordered table-striped dt-responsive last-col grd_view_milestones">
            <thead>
            <tr>
                <th>م</th>
                <th>من تاريخ</th>
                <th>إلى تاريخ</th>
                <th>اسم المرحلة</th>
                <th>نسبة التقدم</th>
                <th width="200">التفاصيل</th>
            </tr>
            </thead>
            <tbody>
            @foreach($milestones as $milestone)
                <tr>
                    <td class="case-serial-td">{{$loop->iteration}}</td>
                    <td class="en-txt">{{$milestone->from_date}}</td>
                    <td class="en-txt">{{$milestone->to_date}}</td>
                    <td><a href="#" class="btn-open" data-target="#showMilestone{{$milestone->id}}"><b>{{$milestone->name}}</b></a></td>
                    <td>
                        <?php
                        $class="grn";
                        if($milestone->score<25) $class="rd";
                        elseif ($milestone->score>=25 and $milestone->score<50) $class="ylw";
                        elseif ($milestone->score>=50 and $milestone->score<75) $class="blu";
                        elseif ($milestone->score>=75) $class="grn";
                        ?>
                        <div class="chart {{$class}}" data-percent="{{$milestone->score}}">{{$milestone->score}}%</div>
                    </td>
                    <td class="text-center">
                        <a href="#" data-toggle="tooltip" title="مشاهدة" data-target="#showMilestone{{$milestone->id}}" class="btn btn-primary btn-open"><i class="fa fa-eye"></i></a>
                        @if(permissions_prm("update_milestone"))
                            <a href="{{route('company.prm.editMilestone', [$company->slug, $milestone->id])}}" class="btn btn-success" data-toggle="tooltip" title="مشاهدة"><i class="fa fa-edit"></i></a>
                        @endif
                        @if(permissions_prm("delete_milestone"))
                            <a href="{{route('company.prm.deleteMilestone', [$company->slug, $milestone->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-danger" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <div class="formi" id="showMilestone{{$milestone->id}}">
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3">إسم المرحلة :</div>
                                <div class="col-sm-9">{{$milestone->name}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3">من تاريخ :</div>
                                <div class="col-sm-9">{{$milestone->from_date}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3">إلى تاريخ :</div>
                                <div class="col-sm-9">{{$milestone->to_date}}</div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3">نسبة التقدم :</div>
                                <div class="col-sm-9"><span>{{$milestone->score}}</span> <span>%</span></div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-3">التفاصيل :</div>
                                <div class="col-sm-9">{{($milestone->details)?$milestone->details:"لا يوجد"}}</div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>