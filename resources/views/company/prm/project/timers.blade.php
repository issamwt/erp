<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold pull-right mt-5 mb-5"><i class="fa fa-clock-o"></i> الجدول الزمنى</h3>
        <span class="pull-left m-t-10">
            <a href="#" data-target="#addTimer" class="btn-open btn btn-primary pull-left"> إضـافة وقت </a>
        </span>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div id="addTimer" class="addFileaddFile formi">
            <h4 class="modal-title mb-15">إضافة وقت</h4>
            <form action="{{route('company.prm.saveTimer', $company->slug)}}"  method="post">
                @csrf
                <input type="hidden" name="project_id" value="{{$project->id}}">

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-sm-3">
                        <label for="start_date">تاريخ البدء <span class="red">*</span></label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="start_date" name="start_date" required class="form-control datee" autocomplete="off">
                    </div>
                    <div class="col-sm-3">
                        <label for="start_time">وقت البدء <span class="red">*</span></label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="start_time" name="start_time" required class="form-control timee" autocomplete="off">
                    </div>
                </div>

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-sm-3">
                        <label for="end_date">تاريخ الانتهاء <span class="red">*</span></label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="end_date" name="end_date" required class="form-control datee" autocomplete="off">
                    </div>
                    <div class="col-sm-3">
                        <label for="end_time">وقت الانتهاء <span class="red">*</span></label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="end_time" name="end_time" required class="form-control timee" autocomplete="off">
                    </div>
                </div>

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-sm-3">
                        <label for="note">الملاحظة</label>
                    </div>
                    <div class="col-sm-9">
                        <textarea id="note" name="note" class="form-control" rows="4"></textarea>
                    </div>
                </div>

                @if(count($tasks)>0)
                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-sm-3">
                            <label for="task_id">المهمة <span class="red">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <select id="task_id" name="task_id" required class="select-hide full-width">
                                <option value="">إختر المهمة</option>
                                @foreach($tasks as $task)
                                    <option value="{{$task->id}}">{{$task->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary"><span class="fa fa-save"></span> حفظ</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div id="timers-wrapper">
    <div id="timers-div">

        <div class="row">

            <div class="col-md-12">

                <table class="table table-bordered dt-responsive last-col grd_view_timers" id="grd_view10">
                    <thead>
                    <tr>
                        <th>م</th>
                        <th>الموظف</th>
                        <th>عنوان المهمة</th>
                        <th>وقت البدء</th>
                        <th>وقت النهاية</th>
                        <th>المدة</th>
                        <th width="200">التفاصيل</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $timer_Total=0;?>
                    @foreach(@$timers as $timer)
                        <?php $timer_Total+=$timer->price;?>
                        <tr class="odd gradeX" data-date0="{{datef($timer->start_time)}}" data-date00="{{datef($timer->end_time)}}"
                            data-timerclients="{{@$timer->user->id}}" data-timertasks="{{(@$timer->task)?@$timer->task->id:0}}">
                            <td class="text-center" style="vertical-align: middle">{{$loop->iteration}}</td>
                            <td style="vertical-align: middle">
                                <div class="m-t-15">
                                    <div class="client-pic">
                                        @if(@$timer->user)
                                            @if(@$timer->user->image)
                                                <img src="{{asset('storage/app/'.@$timer->user->image)}}" alt=""
                                                     data-src="{{asset('storage/app/'.@$timer->user->image)}}"
                                                     data-src-retina="{{asset('storage/app/'.@$timer->user->image)}}" width="35"
                                                     height="35">
                                            @else
                                                <img src="{{asset('public/noimage.png')}}" alt=""
                                                     data-src="{{asset('public/noimage.png')}}"
                                                     data-src-retina="{{asset('public/noimage.png')}}g" width="35"
                                                     height="35">
                                            @endif
                                        @endif
                                    </div>
                                    @if(@$timer->user)
                                        <a href="{{route('company.users.show',[$company->slug, @$timer->user->id])}}" class="client-name-project">{{@$timer->user->name}}</a>
                                    @endif
                                </div>
                            </td>
                            <td style="vertical-align: middle">{{(@$timer->task)?@$timer->task->name:"بدون مهمة"}}</td>
                            <td class="en-txt text-center" style="direction: inherit;vertical-align: middle"><span>{{datef($timer->start_time)}}</span> <span style="display: block">{{timef($timer->start_time)}}</span></td>
                            <td class="en-txt" style="direction: inherit; vertical-align: middle">@if($timer->end_time) <span>{{datef($timer->end_time)}}</span> <span style="display: block">{{timef($timer->end_time)}}</span> @endif</td>
                            <td style="vertical-align: middle" class="text-center">
                                @if($timer->end_time)
                                    <?php
                                    $end_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',@$timer->end_time);
                                    $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',@$timer->start_time);
                                    $totalDuration = @$end_time->diffInSeconds(@$start_time);
                                    $day  = intval(gmdate('d', @$totalDuration)-1);
                                    $hour  = intval(gmdate('H', @$totalDuration));
                                    $minute  = intval(gmdate('i', @$totalDuration));
                                    $string = ($day!=0)?$day." أيام <br>":" ";
                                    $string .=" ".(($hour!=0)?$hour." ساعات <br>":" ");
                                    $string .=" ".(($minute!=0)?$minute." دقائق":" ");
                                    ?>
                                    {!! $string !!}
                                @endif
                            </td>
                            <td style="vertical-align: middle" class="text-center">
                                <a href="#" class="btn btn-success btn-open" data-toggle="tooltip" title="ملاحظة" data-target="#showTimer{{$timer->id}}"><i class="fa fa-eye"></i></a>
                                <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#editTimer{{$timer->id}}"><i class="fa fa-edit"></i></a>
                                <a href="{{route('company.prm.deleteTimer', [$company->slug, $timer->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-danger" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="padding: 0px">
                                <div id="showTimer{{$timer->id}}" class="formi">
                                    <h4 class="modal-title mb-15">ملاحظة الوقت</h4>
                                    <p>{{($timer->note)?$timer->note:"لا توجد ملاحظة"}}</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" style="padding: 0px">
                                <div id="editTimer{{$timer->id}}" class="formi">
                                    <form action="{{route('company.prm.updateTimer', [$company->slug, $timer->id])}}"  method="post">
                                        <h4 class="modal-title mb-15">تعديل الوقت</h4>

                                        @csrf

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3">
                                                <label>تاريخ البدء <span class="red">*</span></label>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" value="{{Carbon\Carbon::parse($timer->start_time)->format('Y-m-d')}}" name="start_date" required class="form-control datee" autocomplete="off">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>وقت البدء <span class="red">*</span></label>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" value="{{Carbon\Carbon::parse($timer->start_time)->format('HH:ii:ss')}}" name="start_time" required class="form-control timee" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3">
                                                <label>تاريخ الانتهاء</label>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" value="{{Carbon\Carbon::parse($timer->end_time)->format('Y-m-d')}}" name="end_date" class="form-control datee" autocomplete="off">
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="end_time">وقت الانتهاء</label>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" value="{{Carbon\Carbon::parse($timer->end_time)->format('HH:ii:ss')}}" name="end_time" class="form-control timee" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-3">
                                                <label for="note">الملاحظة</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <textarea id="note" name="note" class="form-control" rows="4">{{$timer->note}}</textarea>
                                            </div>
                                        </div>

                                        @if(count($tasks)>0)
                                            <div class="row" style="margin-bottom: 15px">
                                                <div class="col-sm-3">
                                                    <label for="task_id">المهمة <span class="red">*</span></label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <select id="task_id" name="task_id" required class="select-hide full-width">
                                                        <option value="">إختر المهمة</option>
                                                        @foreach($tasks as $task)
                                                            <option value="{{$task->id}}" @if($task->id==$timer->task_id) selected @endif;>{{$task->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> حفظ</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </div>
</div>