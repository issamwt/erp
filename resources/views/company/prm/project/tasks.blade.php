<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold pull-right mt-5 mb-5"><i class="fa fa-tasks"></i> مهام المشروع</h3>
        <span class="pull-left m-t-10">
            @if(permissions_prm("add_task"))
                <a href="{{route('company.prm.addTask', [$company->slug, $project->id])}}" class="btn btn-primary pull-left"> إضـافة مهمة </a>
            @endif
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">

    <div class="col-sm-12" style="position: relative">

        <a href="javascript:void(0)" onclick="$('#finances_filter0').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

        <form id="finances_filter0" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

            <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

            <div class="col-sm-12 mb-10">
                <input type="text" class="form-control datee date1" autocomplete="off" name="from" placeholder="من تاريخ">
            </div>

            <div class="col-sm-12 mb-10">
                <input type="text" class="form-control datee date2" autocomplete="off" name="to" placeholder="إلى تاريخ">
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x userss" autocomplete="off">
                    <option value="0">المكلف</option>
                    @foreach($project->users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x employees" autocomplete="off">
                    <option value="0">المساعدون</option>
                    @foreach($project->users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x status" autocomplete="off">
                    <option value="all">جميع الحالات</option>
                    <option value="0">جديدة</option>
                    @foreach($taskStatus as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x priority" autocomplete="off">
                    <option value="0">جميع الأولويات</option>
                    <option value="1">عادى</option>
                    <option value="2">عاجل</option>
                    <option value="3">عاجل جدا</option>
                    <option value="4">غير عاجل</option>
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x milestone" autocomplete="off">
                    <option value="0">جميع المراحل</option>
                    @foreach($milestones as $milestone)
                        <option value="{{$milestone->id}}">{{$milestone->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-block">فلتر</button>
            </div>

            <div class="col-sm-6">
                <button type="reset" class="btn btn-info btn-block">مسح</button>
            </div>

            <div class="clearfix"></div>

        </form>

    </div>

    <div class="col-sm-12"><hr></div>

    <div class="col-md-12 ">

        <table class="table table-bordered table-striped dt-responsive last-col grd_view grd_view_tasks" id="grd_view">
            <thead>
            <tr>
                <th>م</th>
                <th>الحالة</th>
                <th>عنوان المهمة</th>
                <th>المرحلة</th>
                <th>الأولوية</th>
                <th>تاريخ البدء</th>
                <th>الموعد النهائى</th>
                <th>المكلف</th>
                <th>المساعدون</th>
                <th width="200">التفاصيل</th>
            </tr>
            </thead>
            <tbody>
            @if(count($tasks)<=0)

            @else
                @foreach($tasks as $task)
                    <?php $uss=[]; foreach($task->users as $user) array_push($uss, $user->id); $uss = implode(",", $uss);?>
                    <tr class="odd gradeX" data-date0="{{$task->from_date}}" data-date00="{{$task->to_date}}" data-employees="{{$uss}}" data-user="{{$task->user_id}}" data-status="{{$task->status}}" data-priority="{{$task->priority}}"
                    data-milestone="{{@$task->milestone->id}}">
                        <td style="vertical-align: middle;" class="case-serial-td">{{$loop->iteration}}</td>
                        <td style="vertical-align: middle;" class="case-status-td">
                            <div class="case-status-indcator case-bg-blue" style="background: {{@$task->taskStatus->color}}"></div>
                        </td>
                        <td style="vertical-align: middle;">
                            <a href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" data-name="{{$task->name}}">{{$task->name}}</a>
                        </td>
                        <td style="vertical-align: middle;">
                            @if($task->milestone)
                                <a href="{{route("company.prm.editMilestone", [$company->slug, @$task->milestone->id])}}">{{@$task->milestone->name}}</a>
                            @else
                                <span>بدون مرحلة</span>
                            @endif
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            @if($task->priority==1)
                                <label class="label label-info" style="width:100%; display: block; padding: 5px 6px;">عادى</label>
                            @elseif($task->priority==2)
                                <label class="label label-warning" style="width:100%; display: block; padding: 5px 6px;">عاجل</label>
                            @elseif($task->priority==3)
                                <label class="label label-danger" style="width:100%; display: block; padding: 5px 6px;">عاجل جدا</label>
                            @elseif($task->priority==4)
                                <label class="label label-success" style="width:100%; display: block; padding: 5px 6px;">غير عاجل</label>
                            @endif
                        </td>
                        <td style="vertical-align: middle">{{$task->from_date}}</td>
                        <td style="vertical-align: middle">@if(\Carbon\Carbon::now()>$task->to_date)<span style="color: #e20000; text-decoration: line-through;">{{$task->to_date}}</span> @else {{$task->to_date}} @endif</td>
                        <td style="vertical-align: middle">
                            @if(@$task->user)
                                @if(@$task->user->image)
                                    <img src="{{asset('storage/app/'.@$task->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                @else
                                    <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                @endif
                                <a href="{{route('company.users.show',[$company->slug, @$task->user->id])}}">{{@$task->user->name}}</a>
                            @endif
                        </td>
                        <td style="vertical-align: middle;">
                            <ul style="list-style: none; margin: 0px; padding: 0px;">
                                @foreach($task->users as $user)
                                    @if(@$user)
                                        <li class="mb-10">
                                            @if(@$user->image)
                                                <img src="{{asset('storage/app/'.@$user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                            @else
                                                <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                            @endif
                                            <a href="{{route('company.users.show',[$company->slug, @$user->id])}}">{{@$user->name}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </td>
                        <td class="text-center" style="vertical-align: middle; position: relative">
                            <a href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" data-name="{{$task->name}}" class="btn btn-default" data-toggle="tooltip" title="مشاهدة"><i class="fa fa-eye"></i></a>
                            @if(permissions_prm("update_task"))
                                <a href="#choose{{$task->id}}" class="btn btn-primary btn-choose" data-toggle="tooltip" title="الحالة"><i class="fa fa-star"></i></a>
                                <a href="{{route("company.prm.editTask", [$company->slug, $task->id])}}" class="btn btn-success" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                            @endif
                            @if(permissions_prm("delete_task"))
                                <a href="{{route("company.prm.deleteTask", [$company->slug, $task->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-danger" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                            @endif
                            <ul id="choose{{$task->id}}" class="div-choose" style="display: none; list-style: none; margin: 0px; padding: 0px;">
                                @if($task->status!=0)
                                    <li><a href="{{route("company.prm.statusTask", [$company->slug, $task->id, 0])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-primary btn-block" style="color:#fff; ">جديدة</a></li>
                                @else
                                    <li><a href="javascript:void(0)" disabled class="btn btn-primary btn-block disabled heree" style="color:#fff; ">جديدة</a></li>
                                @endif

                                @foreach($taskStatus as $status)
                                    @if($status->id!=$task->status)
                                        <li>
                                            <a href="{{route("company.prm.statusTask", [$company->slug, $task->id, $status->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-default btn-block" style="color:#fff; background: {{$status->color}}">{{$status->name}}</a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="javascript:void(0)" class="btn btn-default btn-block disabled heree" style="color:#fff; background: {{$status->color}}">{{$status->name}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        <div class="col-md-12">
            <hr>
        </div>

        <div class="col-md-12">
            @foreach($taskStatus as $status)
                <div class="mt-5">
                    <div class="case-status-indcator case-bg-blue inline" style="background: {{$status->color}}"></div>
                    <span>{{$status->name}}</span>
                </div>
            @endforeach
            <div class="mt-5">
                <div class="case-status-indcator case-bg-blue inline" style="background: #8aeeff"></div>
                <span>جديدة</span>
            </div>
        </div>

    </div>

</div>
