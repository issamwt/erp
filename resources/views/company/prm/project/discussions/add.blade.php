@extends("company.layouts.app", ["title"=>"إضافة نقاش"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
        <small><span>نظام إدارة المشاريع</span> <span> > </span> <span>{{@$project->name}}</span> > <span>إضافة نقاش</span></small>
    </section>

    <section class="content">

        <div class="tiles white p-t-15 p-l-15 p-r-15 p-b-25 bx-shade">

            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-primary">

                        <div class="panel-heading">
                            <strong>إضافة نقاش</strong>
                            <a href="{{route("company.prm.project", [@$company->slug, $project->id])}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-reply"></i> <span>العودة للمشروع</span></a>
                        </div>

                        <div class="panel-body">

                            <form action="{{route('company.prm.saveDiscussion', @$company->slug)}}" method="post" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="project_id" value="{{$project->id}}">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">نوع النقاش <span class="red">*</span></label>
                                        <div class="controls">
                                            <select class="select-hide full-width disctyped" required name="type">
                                                <option></option>
                                                <option value="0" selected>نقاشات فريق العمل</option>
                                                <option value="1">نقاشات العميل</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">عنوان النقاش <span class="red">*</span></label>
                                        <div class="controls">
                                            <input type="text" required name="name" class="form-control name">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">موجه إلى <span class="red">*</span></label>
                                        <div class="controls">
                                            <select type="text" required name="for_users[]" class="form-control select2x" multiple>
                                                <option value="0">الكل</option>
                                                @foreach($project->users as $u)
                                                    <option value="{{$u->id}}">{{$u->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">نص النقاش <span class="red">*</span></label>
                                        <div class="controls">
                                            <textarea rows="5" name="text" required class="text form-control bg-light-grey"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6">
                                    <div class="file-loading">
                                        <input type="file" name="image[]" class="form-control kv-explorer-discussion">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-6 mt-15">
                                    <div class="file-loading">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section("scripts")

    <script>
        $(function () {
            $(document).on("change", ".kv-explorer-discussion", function (e) {
                if(e.target.files.length>0){
                    $(this).closest(".file-loading").append('<div class="deleteme"></div><input type="file" name="image[]" class="form-control kv-explorer-discussion mt-10">');
                }
            });
            $(document).on("click", ".deleteme", function () {
               $(this).next("input").remove();
               $(this).remove();
            });
        });
    </script>

    <style>
        .deleteme{
            position: relative;
        }
        .deleteme:after{
            cursor: pointer;
            position: absolute;
            content: "حذف";
            background: red;
            color: #fff;
            padding: 0px 5px;
            left: 10px;
            top: 16px;
        }
    </style>

@endsection