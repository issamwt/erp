
<li class="row discx" data-forme="{{($discussion->for_users and $discussion->user_id!=$user->id)?1:0}}">

    <div class="col-sm-2">
        <time class="cbp_tmtime">
            <span class="date"><i class="fa fa-calendar-o"></i> {{$discussion->created_at->format("Y-m-d")}}</span>
            <span class="time"><i class="fa fa-clock-o"></i> {{$discussion->created_at->format("h:i:s")}}</span>
        </time>
        <div class="cbp_tmicon primary animated bounceIn"><i class="fa fa-comments"></i></div>
    </div>
    <div class="col-sm-10">
        <div class="cbp_tmlabel" style="margin: 0px; margin-bottom: 15px">
            <div class="p-t-10 p-l-30 p-r-20 xs-p-r-10 xs-p-l-10 xs-p-t-5">
                <a class="pull-left deleteDiscussion" data-id="{{$discussion->id}}"><i class="fa fa-times"></i></a>
                <h4 class="inline m-b-5"><a href="javascript:;" class="chat-tlt" data-id="{{$discussion->id}}">
                        <span class="text-success semi-bold">{{$discussion->name}}</span></a>
                </h4>
                <p class="m-t-5 dark-text">
                <p>{{$discussion->text}}</p>
                <div class="m-t-15">
                    <div class="client-pic">
                        @if(@$discussion->user)
                            @if(@$discussion->user->image)
                                <img src="{{asset('storage/app/'.@$discussion->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                            @else
                                <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                            @endif
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            @if(@$discussion->user)
                                <a class="client-name-project" href="{{route('company.users.show',[$company->slug, @$discussion->user->id])}}">{{@$discussion->user->name}}</a>
                            @endif
                        </div>
                        <div class="col-sm-7" style="">
                            @if($discussion->for_users)
                                <span>موجه ل :</span>
                                @foreach(explode(",", $discussion->for_users) as $u)
                                    @if($u)
                                        @foreach($project->users as $us)
                                            @if($us and $u==$us->id)
                                                @if(@$us->image)
                                                    <img src="{{asset('storage/app/'.@$us->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                @else
                                                    <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                @endif
                                                <a class="client-name-project" style="margin: 0px 0px 0px 10px" href="{{route('company.users.show',[$company->slug, $us->id])}}">{{$us->name}}</a>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @else
                                <span>موجه للكل</span>
                            @endif
                        </div>
                    </div>
                </div>
                </p>
            </div>
            <div class="clearfix"></div>
            <div class="tiles grey p-t-10 p-b-10 p-l-20" style="padding: 10px 20px 10px 20px">
                <ul class="action-links unstyled" style="width: 100%">
                    <li class="pull-right" style="color:#8b91a0;"><i class="fa fa-clock-o" style="color:#8b91a0;"></i> {{$discussion->created_at->format("h:i:s")}}</li>
                    <li class="pull-right" style="color:#8b91a0;"><i class="fa fa-calendar-o" style="color:#8b91a0;"></i> {{$discussion->created_at->format("Y-m-d")}}</li>
                    @if(count($discussion->attachments)>0)
                        <div class="pull-left">
                        @foreach($discussion->attachments as $attachment)
                            <?php $path = $attachment->path; ?>
                            <li style="display:block;">
                                <a style="color: #2a6496; font-weight: bold;" href="{{asset("storage/app/".$path)}}" target="_blank" class="text-blue"><i class="fa fa-paperclip"></i> الملف المرفق</a>
                            </li>
                        @endforeach
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="chat-dtls"  id="chat-dtls{{$discussion->id}}" style="display: none; margin:0px;">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                <h4 class="pull-right m-b-15"><i class="fa fa-comments-o"></i> الردود</h4>
                <div class="pull-left mt-10"><a href="#" data-id="{{$discussion->id}}" data-target="#editDiscussion{{$discussion->id}}" class="btn btn-xs btn-primary btn-open"><i class="fa fa-comments-o"></i> إضافة رد</a></div>
                <div class="clearfix"></div>
                <form class="formi" action="{{route('company.prm.saveDiscussion', @$company->slug)}}" method="post" id="editDiscussion{{$discussion->id}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="project_id" value="{{$project->id}}">
                    <input type="hidden" name="discussion_id" id="discussion_id" value="{{$discussion->id}}">

                    <div class="form-group">
                        <label class="form-label">اضف الرد <span class="red">*</span></label>
                        <div class="controls">
                            <textarea rows="5" name="text" required class="text form-control bg-light-grey"></textarea>
                        </div>
                    </div>

                    <div class="file-loading">
                        <input type="file" name="image[]" class="form-control kv-explorer-discussion">
                    </div>

                    <button type="submit" class="btn btn-primary mt-15"><i class="fa fa-save"></i> <span>إضـافة</span></button>

                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(count($discussion->discussions)>0)
                    @foreach($discussion->discussions as $disc)
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-11">
                                <div class="grid simple vertical  mt-15 mb-5 @if($loop->iteration%2==1) grey @else white @endif cht" style="border-right: 2px solid #e8e8e8 !important; border-bottom: 2px solid #e8e8e8 !important;">
                                    <div class="grid-title no-border">
                                        <div class="user-profile-pic-wrapper">
                                            <a class="pull-left deleteDiscussion x" data-id="{{$disc->id}}" style="margin-left: 10px;margin-top: 5px;"><i class="fa fa-times"></i></a>
                                            <div class="user-profile-pic-normal pull-right">
                                                @if(@$disc->user)
                                                    @if(@$disc->user->image)
                                                        <img src="{{asset('storage/app/'.@$disc->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35"
                                                             height="35">
                                                    @else
                                                        <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35"
                                                             height="35">
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="username pull-right p-r-10 p-t-0" style="padding-right: 10px">
                                                @if(@$disc->user)
                                                    <a href="{{route('company.users.show',[$company->slug, @$discussion->user->id])}}" class="dark-text">{{@$disc->user->name}}</a>
                                                @endif
                                                <div class="p-t-5"><span class="muted">{{$disc->created_at->format("Y-m-d")}} <i class="fa fa-calendar-o"></i></span>
                                                    <span class="muted">{{$disc->created_at->format("h-i-s")}} <i class="fa fa-clock-o"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tools"><a href="javascript:;" class="collapse"></a></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="grid-body no-border">
                                        <div class="row">
                                            <div class="post col-md-12">
                                                <div class="info-wrapper p-t-15">
                                                    <div class="info">{{$disc->text}}</div>
                                                    <div class="more-details">
                                                        @if(count($disc->attachments)>0)
                                                            <ul class="post-links" style="list-style: none">
                                                                @foreach($disc->attachments as $d)
                                                                    <li class="no-after">
                                                                        <?php $name = substr(basename($d->path), 11);?>
                                                                        <a href="{{asset('storage/app/'.$d->path)}}" target="_blank" class="text-blue"><i class="fa fa-paperclip"></i> {{$name}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                @else
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <hr>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <h4 class="text-center">لا توجد ردود</h4>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <hr>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    </div>
</li>
