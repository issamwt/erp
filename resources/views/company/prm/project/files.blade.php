<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold pull-right mt-5 mb-5"><i class="fa fa-archive"></i> الملفات</h3>
        <span class="pull-left m-t-10">
            @if(permissions_prm("add_file"))
                <a href="#" data-target="#addFile" class="btn-open2 btn btn-primary pull-left"> إضـافة ملف </a>
            @endif
        </span>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div id="addFile" class="addFileaddFile formi">
            <h4 class="modal-title mb-15">إضافة ملف</h4>
            <form class="dropzone myId" style="width: 100%;height: 360px; overflow-y: auto" action="{{route('company.prm.saveFile', $company->slug)}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="project_id" value="{{$project->id}}">
                <input type="hidden" name="file_type_id" id="file_type_id" class="form-control">
            </form>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div id="files-wrapper">
    <div id="files-div">

        <div class="row">

            <div class="col-md-12">

                <ul class="nav nav-pills" id="tab-04">
                    <li><button class="btn btn-info btn-primary" data-toggle="tooltip" title="تحديث الجدول" id="filesrefresh"><i class="fa fa-refresh"></i></button></li>
                    @foreach($fileTypes as $fileType)
                        <li><button href="#" class="btn btn-info btn-primary btn-fileType btn-fileType{{$fileType->id}}" data-type-id="{{$fileType->id}}" data-name="{{$fileType->name}}">{{$fileType->name}}</button></li>
                    @endforeach
                    @if(permissions_prm("add_filetype"))
                        <li><button class="btn btn-info btn-primary btn-open" data-toggle="tooltip" title="أضف مجلد"  data-target="#addFileType"><i class="fa fa-plus"></i> <span>أضف مجلد</span></button></li>
                    @endif
                </ul>

                <div id="addFileType" class="col-sm-6 formi" style="padding-right: 35px; padding-left: 35px;">
                    <h4 class="modal-title mb-15">إضافة مجلد ملفات</h4>
                    <form class="row" id="saveFileTypeForm" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="project_id" value="{{$project->id}}">

                        <div class="form-group">
                            <label class="form-label">إسم المجلد <span class="red">*</span></label>
                            <div class="controls">
                                <input type="text" required name="name" class="form-control name" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group mb-0">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>إضـافة</span></button>
                        </div>

                        <div class="clearfix"></div>

                    </form>
                </div>

            </div>

            <div class="col-sm-12">
                <hr>
            </div>
            <div class="clearfix"></div>

            <div style="min-height: 500px">

                @foreach($fileTypes as $fileType)
                    <div class="col-md-12 filesss" id="filess{{$fileType->id}}" style="display: none; ">

                        <table class="table table-bordered table-striped dt-responsive last-col grd_view_files" id="grd_view_05">
                            <thead>
                            <tr>
                                <th class="case-serial-td all">م</th>
                                <th class="all">اسم الملف</th>
                                <th class="all">مجلد الملف</th>
                                <th class="min-phone-l">حجم الملف</th>
                                <th class="min-tablet">تاريخ الرفع</th>
                                <th class="min-tablet">اسم صاحب الملف</th>
                                <th class="desktop" width="140">التفاصيل</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($files)>0)
                                @foreach($files as $file)
                                    @if($file->file_type_id==$fileType->id)
                                        <tr>
                                            <td class="case-serial-td">
                                                {{$loop->iteration}}
                                            </td>
                                            <td>
                                                <a href="{{route('company.prm.downloadFile', [$company->slug, $file->id])}}">{{substr($file->name, 0, 100)}}</a>
                                            </td>
                                            <td>{{@$file->type->name}}</td>
                                            <td class="en-txt">{{$file->size}}</td>
                                            <td class="en-txt">{{$file->created_at}}</td>
                                            <td>{{@$file->user->name}}</td>
                                            <td class="text-center">
                                                <a href="{{route('company.prm.downloadFile', [$company->slug, $file->id])}}" class="btn btn-primary" data-toggle="tooltip" title="تنزيل"><i class="fa fa-download"></i></a>
                                                @if(permissions_prm("delete_file"))
                                                    <a href="javascript:void(0)" class="btn btn-danger deleteFile" data-id="{{$file->id}}" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @else

                            @endif
                            </tbody>
                        </table>

                        <a class="btn btn-danger btn-deletefiletype" data-id="{{$fileType->id}}"><i class="fa fa-remove"></i> <span>حذف المجلد </span><span>"{{$fileType->name}}"</span></a>

                    </div>
                @endforeach

            </div>

        </div>

    </div>
</div>
