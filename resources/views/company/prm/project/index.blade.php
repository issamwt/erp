@extends("company.layouts.app", ["title"=>@$project->name])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
        <small><span>نظام إدارة المشاريع</span> <span> > </span> <span>{{@$project->name}}</span></small>
    </section>

    <section class="content">

        <div class="tiles white p-t-15 p-l-15 p-r-15 p-b-25 bx-shade">

            <div class="row">

                <div class="col-md-12">

                    <div class="inline">
                        <h4><span>{{$project->name}}</span></h4>
                        <br>
                        @if($project->status==0)
                            <div class="case-status-indcator case-bg-orange" style="display: inherit;"></div> <span>مشروع جديد</span>
                        @elseif($project->status==1)
                            <div class="case-status-indcator case-bg-blue" style="display: inherit;"></div> <span>مشروع قيد التنفيذ</span>
                        @elseif($project->status==2)
                            <div class="case-status-indcator case-bg-gray" style="display: inherit;"></div> <span>مشروع مؤرشف</span>
                        @elseif($project->status==3)
                            <div class="case-status-indcator case-bg-green" style="display: inherit;"></div> <span>مشروع منجز</span>
                        @else
                            <div class="case-status-indcator case-bg-red" style="display: inherit;"></div> <span>مشروع متأخر</span>
                        @endif
                    </div>

                    <div class="pull-left m-t-10">
                        <div class="controls inline m-l-10" style="position: relative;">

                            @if(@$cureent_timer)
                                <a href="#" class="btn btn-danger btn-open" style="margin-left: 10px;" data-target="#closeTimer"><i class="fa fa fa-clock-o"></i> إنتهاء العمل</a>
                                <div id="countup" class="countdown"></div>
                            @else
                                <a href="{{route('company.prm.change_timer', [$company->slug, $project->id, "open"])}}" style="margin-left: 10px;" class="btn btn-info"><i class="fa fa fa-clock-o"></i> بدأ العمل</a>
                            @endif

                        </div>

                        <span class="inline">الأعمال</span>
                        <div class="controls inline" style="width: 180px;">
                            <select class="select-hide w-180" style="width: 180px;" id="actions">
                                <option value="">إختر</option>
                                @if($project->status!=0)
                                    <option value="0">جديد</option>
                                @endif
                                @if($project->status!=1)
                                    <option value="1">قيد التنفيذ</option>
                                @endif
                                @if($project->status!=2)
                                    <option value="2">مؤرشف</option>
                                @endif
                                @if($project->status!=3)
                                    <option value="3">منجز</option>
                                @endif
                                <option value="4">تعديل المشروع</option>
                                <option value="5">قائمة المشاريع</option>
                            </select>
                        </div>

                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            @if(@$cureent_timer)
                                <div class="formi" id="closeTimer">
                                    <h4 class="modal-title" style="color: #2e2e2e;margin: 0px 0px 15px;">إنتهاء العمل</h4>
                                    <form action="{{route('company.prm.change_timer', [$company->slug, $project->id, "closed"])}}" method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>الملاحظات</label>
                                            <textarea class="form-control" name="note" rows="4"></textarea>
                                        </div>
                                        @if(count($tasks)>0)
                                            <div class="form-group">
                                                <label for="pwd">المهمة <span class="red">*</span></label>
                                                <select class="form-control" name="task_id" required>
                                                    <option value="">إختر المهمة</option>
                                                    @foreach($tasks as $task)
                                                        <option value="{{$task->id}}">{{$task->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endif
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div style="background: #fff">

                    <ul class="nav nav-tabs">
                        @if(permissions_prm("tab1"))
                            <li class="@if(!session('projectTab') or session('projectTab')==1) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="1" href="#tab5View"><i class="fa fa-dashboard"></i> نظرة عامة</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab2"))
                            <li class="@if(session('projectTab')==2) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="2" href="#tab5Tasks"><i class="fa fa-tasks"></i> مهام المشروع</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab3"))
                            <li class="@if(session('projectTab')==3) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="3" href="#tab5Board"><i class="fa fa-clipboard"></i> لوحة المهام</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab4"))
                            <li class="@if(session('projectTab')==10) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="10" href="#tab5Chart"><i class="fa fa-bar-chart-o"></i> مخطط المهام</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab5"))
                            <li class="@if(session('projectTab')==4) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="4" href="#tab5Levels"><i class="fa fa-th-large"></i> المراحل</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab6"))
                            <li class="@if(session('projectTab')==5) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="5" href="#tab5Disc"><i class="fa fa-comments-o"></i> نقاشات المشروع</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab7"))
                            <li class="@if(session('projectTab')==7) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="7" href="#tab5Files"><i class="fa fa-archive"></i> ملفات</a>
                            </li>
                        @endif
                        @if(permissions_prm("tab8"))
                            <li class="@if(session('projectTab')==9) active @endif">
                                <a class="projectTabLink" data-toggle="tab" data-num="9" href="#tab5Time"><i class="fa fa-clock-o"></i> الجدول الزمنى</a>
                            </li>
                        @endif
                    </ul>

                    <div class="tab-content">
                        @if(permissions_prm("tab1"))
                        <div class="tab-pane  @if(!session('projectTab') or session('projectTab')==1) active @endif" id="tab5View">
                            @include('company.prm.project.overview')
                        </div>
                        @endif

                            @if(permissions_prm("tab2"))
                        <div class="tab-pane @if(session('projectTab')==2) active @endif" id="tab5Tasks">
                            @include('company.prm.project.tasks')
                        </div>
                            @endif

                            @if(permissions_prm("tab3"))
                        <div class="tab-pane  @if(session('projectTab')==3) active @endif" id="tab5Board">
                            @include('company.prm.project.kanaban')
                        </div>
                            @endif

                            @if(permissions_prm("tab4"))
                        <div class="tab-pane @if(session('projectTab')==4) active @endif" id="tab5Levels">
                            @include('company.prm.project.milestones')
                        </div>
                            @endif

                            @if(permissions_prm("tab5"))
                        <div class="tab-pane @if(session('projectTab')==10) active @endif" id="tab5Chart">
                            @include('company.prm.project.gantt')
                        </div>
                            @endif

                            @if(permissions_prm("tab6"))
                        <div class="tab-pane  @if(session('projectTab')==5) active @endif" id="tab5Disc">
                            @include('company.prm.project.discussions')
                        </div>
                            @endif

                            @if(permissions_prm("tab7"))
                        <div class="tab-pane  @if(session('projectTab')==7) active @endif" id="tab5Files">
                            @include('company.prm.project.files')
                        </div>
                            @endif

                            @if(permissions_prm("tab8"))
                        <div class="tab-pane @if(session('projectTab')==9) active @endif" id="tab5Time">
                            @include('company.prm.project.timers')
                        </div>
                            @endif

                            {{--
                        <div class="tab-pane @if(session('projectTab')==8) active @endif" id="tab5Finn">
                            <div class="text-center" style="margin: 30px auto;">
                                <i class="fa fa-spinner fa-spin" style="font-size:10em; color:#0090d9"></i>
                            </div>
                        </div>
                            --}}

                    </div>

                </div>

            </div>
        </div>

        <div id="wainting" class="hidden">
            <div class="text-center" style="margin: 30px auto;">
                <i class="fa fa-spinner fa-spin" style="font-size:10em; color:#0090d9"></i>
            </div>
        </div>

    </section>

@endsection

@section("scripts")

    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <link rel="stylesheet" href="https://ccreation.co/mouhamat.test/public/assets/plugins/jquery-morris-chart/css/morris.css" type="text/css" media="screen">
    <script src="https://ccreation.co/mouhamat.test/public/assets/plugins/jquery-morris-chart/js/morris.min.js"></script>
    <!--<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/gantt_5.0.5/dhtmlxgantt.css')}}" />
    <script src="{{asset('public/plugins/gantt_5.0.5/dhtmlxgantt.js')}}" type="text/javascript" charset="utf-8"></script>
    <script src="{{asset('public/plugins/gantt_5.0.5/locale/locale_ar.js')}}" charset="utf-8"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.min.js"></script>
    <link href="{{asset("public/plugins/dropzone/css/dropzone.css")}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset("public/plugins/dropzone/dropzone.min.js")}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />



    <link rel="stylesheet" href="{{asset("public/jquery-ui.min.css")}}">
    <script src="{{asset("public/jquery-ui.min.js")}}"></script>

    <style type="text/css">
        #gantt_here{
            background:white;
            width:100%;
            height:100%;
        }

        #chat-lst, #chat-dtls {
            overflow: inherit !important;
        }

        .tab-pane{
            background: #fff;
            padding: 20px;
            min-height: 300px;
            box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.15);
            margin-bottom: 60px;
        }
        .nav-tabs{
            background: #e1eaef;
        }
        .nav-tabs > li> a{
            background: #e1eaef;
            color: #7b8d98;
            border-bottom: 1px solid #ddd;
        }
        .nav-tabs > li> a:hover, .nav-tabs > li> a:focus{
            background: #e1eaef;
            color: #505458;
            border-right-color: transparent;
            border-left-color: transparent;
        }
        .nav-tabs > li.active > a{
            color: #505458;
            background: #fff;
            border: 1px solid #ddd;
            border-bottom: 1px solid transparent;
        }
        .tiles.white{
            background-color: #ffffff;
            color: #8b91a0;
            margin-bottom: 22px;
            box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.15);
            padding-bottom: 25px;
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 15px;
        }
        .inline {
            display: inline-block !important;
        }
        .m-t-10 {
            margin-top: 10px !important;
        }
        .case-status-td {
            width: 15px;
            padding: 5px;
        }
        .case-status-indcator {
            width: 16px;
            margin: 0 auto;
            height: 16px;
            border: 3px solid rgba(0, 0, 0, 0.18);
            border-radius: 50%;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.18);
            display: inline-block;
        }

        .case-bg-blue {background: #8aeeff}/*8bc1e8*/

        .case-bg-green {background: #77ff2d}/*84d25a*/

        .case-bg-orange {background: #fffdab}/*e8c684*/

        .case-bg-gray {background: #c5c5c5}/*e8c684*/

        .case-bg-red {background: #f399b5}

        .user-description-box {
            background-color: #f2f4f6;
            margin: 10px 15px;
            padding: 20px;
        }

        .tasks_indcator {
            width: 14px;
            height: 14px;
            display: inline-block;
            float: right;
            margin-left: 5px;
            margin-top: 4px;
        }

        .table-bordered th {
            background-color: #ecf0f2;
            border: 0 !important;
            color: #6f7b8a;
        }
        .client-pic {
            border-radius: 100px 100px 100px 100px;
            display: inline-block;
            height: 35px;
            overflow: hidden;
            width: 35px;
            float: right;
        }
        .client-name-project {
            line-height: 3;
            margin-right: 10px;
            font-size: 14px;
        }

        .countdown{
            display: none;
            width: 200px;
            direction: ltr;
            float: right;
            margin: 2px 10px;
        }
        .countdown span{
            display: inline-block;
            text-align: center;
            font-size: 20px;
            font-weight: bold;
            font-family: monospace;
            height: 30px;
            line-height: 30px;
            width: 5%;
            transform: scaleY(1.1);
            color: #fff;
            background: linear-gradient(to top, #d73925 0%, #f3523f 50%, #f3523f 50%, #d73925 100%);
        }
        .countdown span.x{
            width: 30%;
        }

        .datepicker {
            direction: rtl;
        }
        .datepicker.dropdown-menu {
            right: initial;
        }

        .heree{
            font : normal normal normal 14px/1 FontAwesome;
            border: 2px solid red !important;
        }
        a.heree:before{
            content: "\f00c";
            padding-left: 5px;
            color: red;
        }

        div.ganttview-block{
            height: 28px;
        }
        div.ganttview-vtheader-series-row, div.ganttview-grid-row-cell{
            height: 32px;
        }

    </style>

    <script>

        $(function () {

            $(document).on("change", "#actions", function () {
               var val = $(this).val();
               var url = "";
               if(val==0)
                   url = "{{route("company.prm.project_status", [$company->slug, $project->id, 99])}}";
               else if(val==1)
                   url = "{{route("company.prm.project_status", [$company->slug, $project->id, 1])}}";
               else if(val==2)
                   url = "{{route("company.prm.project_status", [$company->slug, $project->id, 2])}}";
               else if(val==3)
                   url = "{{route("company.prm.project_status", [$company->slug, $project->id, 3])}}";
               else if(val==4)
                   url = "{{route("company.prm.edit_project", [$company->slug, $project->id])}}";
               else if(val==5)
                   url = "{{route("company.prm.projects", $company->slug)}}";

               if(url!="")
                   window.location.href = url;
            });

            function reloading2(){
                $('.datee').datepicker({
                    format: "yyyy-mm-dd",
                    language:"ar",
                    autoclose: true,
                    todayHighlight: true
                });
            }

            reloading2();

            $(document).on("click", ".projectTabLink", function () {
                var num = $(this).data("num");
                $.get("{{route("company.prm.projectTab", @$company->slug)}}"+"/"+num, function () {
                    donut();
                });
            });

            function donut(){
                Morris.Donut({
                    element: 'donut-example',
                    data: [
                            @foreach($taskStatus as $status)
                        {label: "{{$status->name}}", value: {{count($status->tasks)}} },
                        @endforeach
                    ],
                    colors:[@foreach($taskStatus as $status)'{{$status->color}}',@endforeach]
                });
            }
            donut();

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("click", "#addCollaborator", function () {
                var id = $("#collaboratorsList").val();
                var addCollaborator = $(this);
                if(id){
                    $(addCollaborator).prop("disabled", true);
                    $.post( "{{route('company.prm.addCollaborator', $company->slug)}}", { project_id: {{$project->id}}, id:id , _token:"{{csrf_token()}}" } , function (data) {
                        $('#addContributer').slideUp();
                        $("#collaborators-wrapper").load(window.location + " #collaborators");
                        goNotif('success', 'تمت إضافة المساعد');
                    }).fail(function() {
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    }).always(function () {
                        $(addCollaborator).prop("disabled", false);
                    });
                }
            });

            $(document).on("submit", "form.sendCollaborator", function (e) {
                e.preventDefault();
                var form = $(this);
                var button = $(this).find("button[type='submit']");
                $(button).prop("disabled", true);
                var id = $('.user_id', this).val();
                var subject = $('.subject', this).val();
                var msg = $('.msg', this).val();
                $.post( "{{route('company.prm.sendCollaborator', $company->slug)}}", { id:id ,subject:subject ,msg:msg , _token:"{{csrf_token()}}" } , function (data) {
                    $(form).trigger("reset");
                    $('#sendContributer'+id).slideUp();
                    goNotif('success', 'تمت الإرسال');
                }).fail(function() {
                    goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                }).always(function () {
                    $(button).prop("disabled", false);
                });
            });

            $(document).on("click", ".deleteCollaborator", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    $.post( "{{route('company.prm.deleteCollaborator', $company->slug)}}", { project_id: {{$project->id}}, id:id , _token:"{{csrf_token()}}" } , function (data) {
                        $("#collaborators-wrapper").load(window.location + " #collaborators");
                        goNotif('success', 'تمت حذف المساعد');
                    }).fail(function() {
                        goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                    });
                }
            });

            $.fn.countup = function(diff) {
                var elem = this;
                $(this).html('<span class="h x">00</span><span>:</span><span class="m x">00</span><span>:</span><span class="s x">00</span>');
                setInterval(function () {
                    $(elem).css("display", "block");
                    ++diff;

                    var h = (diff>=0)?Math.floor(diff/3600):Math.round(diff/3600);
                    var m = (diff>=0)?Math.floor((diff-h*3600)/60):Math.round((diff-h*3600)/60);
                    var s = (diff>=0)?Math.floor((diff-h*3600-m*60)):Math.round((diff-h*3600-m*60));

                    //elem.html(pad(h)+":"+pad(m)+":"+pad(s));
                    $(".h", elem).html(pad(h));
                    $(".m", elem).html(pad(m));
                    $(".s", elem).html(pad(s));
                }, 1000);
            };

            function pad(num) {
                size = 2;
                var s = num+"";
                while (s.length < size) s = "0" + s;
                return s;
            }

            @if(@$cureent_timer)
            <?php
                $now    = Carbon\Carbon::now("Asia/Riyadh");
                $start  = $cureent_timer->start_time;
                $diffx  = $now->diffInSeconds($start);
                $dif    = intval($diffx);
            ?>
            setTimeout(function () {
                $('#countup').countup({{$dif}});
            }, 1000);
            @endif

            // Tasks

            $(document).on("click", ".btn-choose", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                $(href).slideToggle();
            });

            $(document).on("submit", "#finances_filter0", function (e){
                var date1   = $("#finances_filter0 .date1").val();
                var date2   = $("#finances_filter0 .date2").val();
                var employees = $("#finances_filter0 .employees").select2('val')+"";
                var userss = $("#finances_filter0 .userss").select2('val')+"";
                var status  = $("#finances_filter0 .status").select2('val')+"";
                var priority = $("#finances_filter0 .priority").select2('val')+"";
                var milestone = $("#finances_filter0 .milestone").select2('val')+"";

                $('.grd_view_tasks tbody tr').hide();
                $('.grd_view_tasks tbody tr').filter(function () {
                    var datex = true;
                    var date0 = $(this).data("date0");
                    var date00 = $(this).data("date00");
                    if(date0 && date00){
                        if(date1 && !date2) {
                            if(new Date(date1) > new Date(date00))
                                datex = false;
                        }else if(!date1 && date2){
                            if(new Date(date0) > new Date(date2))
                                datex = false;
                        }else if(date1 && date2) {
                            if(new Date(date1)>new Date(date0) || new Date(date2)<new Date(date00))
                                datex = false;
                        }
                    }

                    var userx = true;
                    var user0 = $(this).data("user").toString();
                    if(userss){
                        if(userss!="0"){
                            if(user0!=userss)
                                userx=false;
                        }
                    }

                    var employeesx = true;
                    var employees0 = $(this).data("employees").toString();
                    employees0 = employees0.split(",");
                    if(employees){
                        if(employees!="0"){
                            if(!employees0.includes(employees))
                                employeesx=false;
                        }
                    }

                    var statusx = true;
                    var status0 = $(this).data("status").toString();
                    if(status){
                        if(!status.includes("all"))
                            if(!status.includes(status0.toString()))
                                statusx = false;
                    }
                    console.log(status0+" "+statusx);

                    var priorityx = true;
                    var priority0 = $(this).data("priority").toString();
                    if(priority){
                        if(!priority.includes("0"))
                            if(!priority.includes(priority0.toString()))
                                priorityx = false;
                    }

                    var milestonex = true;
                    var milestone0 = $(this).data("milestone").toString();
                    if(milestone){
                        if(milestone!="0"){
                            if(milestone0!=milestone)
                                milestonex=false;
                        }
                    }

                    return  (datex && userx && employeesx && statusx && priorityx && milestonex);
                }).show();
            });

            $(document).on("reset", "#finances_filter0", function (e){
                $('.grd_view_tasks tbody tr').show();
                $("#finances_filter0 .date1").val("");
                $("#finances_filter0 .date2").val("");
                $('#finances_filter0 .employees').val(0).trigger('change');
                $('#finances_filter0 .userss').val(0).trigger('change');
                $('#finances_filter0 .status').val(0).trigger('change');
                $('#finances_filter0 .priority').val(0).trigger('change');
                $('#finances_filter0 .milestone').val(0).trigger('change');
            });

            $(document).on("submit", "#finances_filterk", function (e){
                var date1   = $("#finances_filterk .date1").val();
                var date2   = $("#finances_filterk .date2").val();
                var employees = $("#finances_filterk .employees").select2('val')+"";
                var status  = $("#finances_filterk .status").select2('val')+"";
                var priority = $("#finances_filterk .priority").select2('val')+"";

                $('#kanaban-wrapper .post').hide();
                $('#kanaban-wrapper .post').filter(function () {
                    var datex = true;
                    var date0 = $(this).data("date0");
                    var date00 = $(this).data("date00");
                    if(date0 && date00){
                        if(date1 && !date2) {
                            if(new Date(date1) > new Date(date00))
                                datex = false;
                        }else if(!date1 && date2){
                            if(new Date(date0) > new Date(date2))
                                datex = false;
                        }else if(date1 && date2) {
                            if(new Date(date1)>new Date(date0) || new Date(date2)<new Date(date00))
                                datex = false;
                        }
                    }

                    var employeesx = true;
                    var employees0 = $(this).data("employees").toString();
                    employees0 = employees0.split(",");
                    if(employees){
                        if(employees!="0"){
                            if(!employees0.includes(employees))
                                employeesx=false;
                        }
                    }

                    var statusx = true;
                    var status0 = $(this).data("status").toString();
                    if(status){
                        if(status!="0")
                            if(status!=status0)
                                statusx = false;
                    }
                    console.log("------");
                    console.log(status);
                    console.log(status0);
                    console.log(statusx);
                    console.log("------");

                    var priorityx = true;
                    var priority0 = $(this).data("priority");
                    if(priority){
                        if(priority!="0")
                            if(priority!=priority0)
                                priorityx = false;
                    }

                    return  (datex && employeesx && statusx && priorityx);
                }).show();
            });

            $(document).on("reset", "#finances_filterk", function (e){
                $('#kanaban-wrapper .post').show();
                $("#finances_filterk .date1").val("");
                $("#finances_filterk .date2").val("");
                $('#finances_filterk .employees').val(0).trigger('change');
                $('#finances_filterk .status').val(0).trigger('change');
                $('#finances_filterk .priority').val(0).trigger('change');
            });
            
            /*
            function do_gant() {
                gantt.config.columns=[
                    {name:"name", label:"المهمة",
                        template:function(task){
                            //return task.name+" <small style='direction:rtl'>("+moment(task.start_date).format('DD-MM-YYYY')+" -- "+moment(task.end_date).format('DD-MM-YYYY')+")</small>"}
                            //return task.name+" <small style='direction:rtl'>("+task.ml+")</small>"}
                            //return task.name;
                            return '<a href="'+task.href+'" data-name="'+task.name+'" data-link="'+task.link+'" class="btn-showTask">'+task.name+'</a>';
                        }
                    },
                    //{name:"project", label:"المشروع", align: "right", tree:true, width:"*" },
                    //{name:"duration",   label:"المدة بالأيام", align: "center" }
                ];
                gantt.config.initial_scroll = true;
                gantt.config.autoscroll = true;
                gantt.config.autosize_min_width = 800;
                gantt.config.fit_tasks = true;
                gantt.config.grid_resize = true;
                gantt.config.rtl = true;
                gantt.config.drag_progress = false;
                gantt.config.drag_links = false;
                gantt.config.details_on_dblclick = false;
                gantt.init("gantt_here");
                var demo_tasks = {"data":[]};
                demo_tasks.data = [];
                $('.gantt-data').each(function(i, el) {
                    demo_tasks.data.push(JSON.parse($(el).html()));
                });
                gantt.parse(demo_tasks);
                gantt.attachEvent("onAfterTaskDrag", function(id, mode, old_task){
                    console.clear();
                    console.log(mode);
                    var task = gantt.getTask(id);
                    if(mode=="move" || mode=="resize"){
                        var start_date = moment(task.start_date).format('YYYY-MM-DD');
                        var end_date = moment(task.end_date).format('YYYY-MM-DD');
                        $.post( "{{route('company.prm.project.gantt_update', $company->slug)}}" , {id:id, start_date:start_date, end_date:end_date, _token:"{{csrf_token()}}"}, function (data) {
                            ganttcharged=false;
                            goNotif('success', 'تم تعديل تاريخ المهمة');
                        }).fail(function() {
                            goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                        });
                    }
                });
            }

            do_gant();
            */

            $(document).on("submit", "#gantt_filter", function (e) {
                e.preventDefault();
                var gant_date1 = $(".gant_date1", this).val();
                var gant_date2 = $(".gant_date2", this).val();
                var ganttmilestones = $(".ganttmilestones", this).select2('val')+"";
                var ganttusers = $(".ganttusers", this).select2('val')+"";
                var ganttstatus = $(".ganttstatus", this).select2('val')+"";
                console.log(ganttstatus);

                gantt.clearAll();
                var onBeforeTaskDisplay = gantt.attachEvent("onBeforeTaskDisplay", function(id, task){
                    var datex = true;
                    var date0 = moment(task.start_date).format('YYYY-MM-DD');
                    var date00 = moment(task.end_date).format('YYYY-MM-DD');
                    if(date0 && date00){
                        if(gant_date1 && !gant_date2) {
                            if(new Date(gant_date1) > new Date(date00))
                                datex = false;
                        }else if(!gant_date1 && gant_date2){
                            if(new Date(date0) > new Date(date2))
                                datex = false;
                        }else if(gant_date1 && gant_date2) {
                            if(new Date(gant_date1)>new Date(date0) || new Date(gant_date2)<new Date(date00))
                                datex = false;
                        }
                    }

                    var ganttmilestonesx = true;
                    var ganttmilestones0 = task.milestone;
                    if(ganttmilestones){
                        if(ganttmilestones!="all")
                            if(ganttmilestones.toString()!=ganttmilestones0.toString())
                                ganttmilestonesx = false;
                    }

                    var ganttusersx = true;
                    var ganttusers0 = task.charged.toString();
                    if(ganttusers){
                        if(ganttusers!="0")
                            if(ganttusers!=ganttusers0.toString())
                                ganttusersx = false;
                    }
                    console.log(ganttusers0+" "+ganttusersx);

                    var ganttstatusx = true;
                    var ganttstatus0 = task.status.toString();
                    if(ganttstatus){
                        if(ganttstatus!="0")
                            if(ganttstatus!=ganttstatus0.toString())
                                ganttstatusx = false;
                    }

                    //return (datex && ganttmilestonesx && ganttusersx && ganttstatusx);

                    return ganttstatusx;
                });
                gantt.init("gantt_here");
                var demo_tasks = {"data":[]};
                demo_tasks.data = [];
                $('.gantt-data').each(function(i, el) {
                    demo_tasks.data.push(JSON.parse($(el).html()));
                });
                gantt.parse(demo_tasks);
                gantt.detachEvent(onBeforeTaskDisplay);
            });

            $(document).on("reset", "#gantt_filter", function (e) {
                $('.ganttmilestones').val("all").trigger('change');
                $('.ganttusers').val(0).trigger('change');
                $('.ganttstatus').val(0).trigger('change');
                var onBeforeTaskDisplay = gantt.attachEvent("onBeforeTaskDisplay", function(id, task){
                    return true;
                });
                gantt.init("gantt_here");
                var demo_tasks = {"data":[]};
                demo_tasks.data = [];
                $('.gantt-data').each(function(i, el) {
                    demo_tasks.data.push(JSON.parse($(el).html()));
                });
                gantt.parse(demo_tasks);
                gantt.detachEvent(onBeforeTaskDisplay);
            });

            // Discussions

            $(document).on("click", "#tabx > li > a", function (e){
                e.preventDefault();
                var id = $(this).attr("href");
                $("#tabx > li").removeClass("active");
                $("a", this).addClass("active");
                $("#contentx .opsigui").fadeOut("fast");
                $("#contentx .opsigui"+id).fadeIn("slow");
            });

            $(document).on("click", ".chat-tlt", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                $("#chat-dtls"+id).slideToggle();
            });

            $(document).on("click", ".deleteDiscussion", function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                if(confirm('هل أنت متأكد ؟')){
                    var url = "{{route('company.prm.deleteDiscussion', $company->slug)}}/"+id;
                    window.location.href = url;
                }
            });

            $(document).on("ifChanged", ".optradiox", function (e) {
                $(".discx").css("display", "none");
                e.preventDefault();
                var status_val = $(this).val();
                if(status_val==1) {
                    $(".discx").css("display", "block");
                }else{
                    $.each($(".discx"), function (i,e) {
                        if($(e).data("forme")==1)
                            $(e).css("display", "block");
                    });
                }
            });

            //Files

            var myDropzone = new Dropzone(".myId", { url: "{{route('company.prm.saveFile', $company->slug)}}"});
            myDropzone.on("complete", function(data) {
                data = jQuery.parseJSON(data.xhr.response);
                console.clear();
                console.log(data);
                if(data.errors)
                    jQuery.each(data.errors, function(key, value){
                        goNotif("error", value);
                    });
                if(data.result){
                    $("#files-wrapper").load(window.location+ " #files-div", function () {
                        $(".btn-fileType"+data.result).trigger("click");
                    });
                    $("#file_type_id").val(data.result);
                    goNotif("success", "تمت إضافة الملف بنجاح !");
                }
            });

            $(document).on("submit", "#saveFileTypeForm", function (e) {
                event.preventDefault();
                if($(".name", this).val()==""){
                    alert("يجب إدخال إسم المجلد !");
                    return;
                }
                var button = $("button[type='submit']", this);
                $(button).prop("disabled", true);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: { 'X-CSRF-TOKEN': "{{csrf_token()}}" },
                    url: "{{route('company.prm.saveFileType', $company->slug)}}",
                    data: formData,
                    type: 'post',
                    async: false,
                    processData: false,
                    contentType: false,
                    success:function(response){
                        $("#addFileType").slideUp();
                        setTimeout(function () {$("#files-wrapper").load(window.location+ " #files-div");$("#file_type_id").val("");}, 1000);
                        goNotif('success', 'تمت إضافة المجلد');
                    }
                }).fail(function() {
                    goNotif('error', 'لقد وقع خطأ ما, الرجاء المحاولة من جديد !');
                }).always(function () {
                    $(button).prop("disabled", false);
                });
            });

            $(document).on("click", ".btn-fileType", function (e) {
                e.preventDefault();
                var file_type_id = $(this).data("type-id");
                if(file_type_id){
                    $("#file_type_id").val(file_type_id);
                    $(".filesss").fadeOut("fast");
                    $("#filess"+file_type_id).fadeIn("slow");
                    $(".btn-fileType").removeClass("btn-primary");
                    $(".btn-fileType").addClass("btn-info");
                    $(".btn-fileType"+file_type_id).addClass("btn-primary");
                    $(".btn-fileType"+file_type_id).removeClass("btn-info");
                }
            });

            $(document).on("click", "#filesrefresh", function (e) {
                e.preventDefault();
                $("#files-wrapper").load(window.location+ " #files-div");
                $("#file_type_id").val("");
            });

            $(document).on("click", ".btn-open2", function (e) {
                e.preventDefault();
                var file_type_id = $("#file_type_id").val();
                if(!file_type_id){
                    alert("يجب إختيارالمجلد أولا !");
                    return;
                }
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("click", ".deleteFile", function (e) {
                e.preventDefault();
                var file_type_id = $("#file_type_id").val();
                var id = $(this).data("id");
                if(confirm("هل انت متأكد ؟")){
                    $.get("{{route('company.prm.deleteFile',$company->slug)}}/"+id, function (data) {
                        $("#files-wrapper").load(window.location+ " #files-div", function () {
                            $(".btn-fileType"+data.result).trigger("click");
                            $("#file_type_id").val(data.result);
                            goNotif("success", "تم حذف الملف بنجاح !");
                        });
                    });
                }
            });

            $(document).on("click", ".btn-deletefiletype", function (e) {
                e.preventDefault();
                var file_type_id = $(this).data("id");
                if(confirm("هل انت متأكد ؟")){
                    $.get("{{route('company.prm.deleteFileType',$company->slug)}}/"+file_type_id, function (data) {
                        $("#files-wrapper").load(window.location+ " #files-div", function () {
                            $("#file_type_id").val("");
                            goNotif("success", "تم حذف المجلد بنجاح !");
                        });
                    });
                }
            });



            // Timers

            $('.timee').datetimepicker({
                format: 'HH:mm:ss'
            });
        });

        function sortablee() {
            @foreach($taskStatus as $status)
                $( "#sortable{{$status->id}}").sortable({
                    connectWith: ".connectedSortable",
                    receive : function (event, ui) {
                        var status = $(this).data("status");
                        var id = $(ui.item).data("id");
                        $("#kanaban-wrapper").html($("#loading-wrapper").html());
                        $.get('{{route("company.prm.statusTask", @$company->slug)}}/'+id+'/'+status, function () {
                            $("#kanaban-wrapper").load(window.location + " #kanaban-div", function () {
                                goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                                sortablee();
                            });
                        });
                    },
                });
            @endforeach
        }
        sortablee();

    </script>

    <link rel="stylesheet" href="{{asset("public/plugins/gantt-chart/gantt.css")}}">
    <script src="{{asset("public/plugins/gantt-chart/gantt.js")}}"></script>

    <script>
        $(function () {

            var months = ["يناير","فبراير","مارس","أبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"];
            var tasks = [];
            <?php $i=1; ?>
            @foreach($tasks as $task)
                var subtasks = [];
                var start = moment("{{$task->from_date}}").format('YYYY-MM-DD');
                var end = moment("{{$task->to_date}}").format('YYYY-MM-DD');
                var url = '<span>المهمة الرئيسية</span> : <a href="{{route("company.prm.showTask", [$company->slug, $task->id])}}">{{$task->name}}</a>';
                var aaa = [];
                var a = { "id":{{$task->id}}33, "name" : "drhgr", "start" : start, "end" : end, "color" : "{{($task->taskStatus)?($task->taskStatus)->color:"#8aeeff"}}"};
                aaa.push(a);
                var subtask = { "id":{{$task->id}}, "name" : url, "start" : start, "end" : end, "color" : "{{($task->taskStatus)?($task->taskStatus)->color:"#8aeeff"}}", "series":aaa};

                subtasks.push(subtask);
                    @foreach($task->subtasks as $subtask)
                        var start = moment("{{$subtask->from_date}}").format('YYYY-MM-DD');
                        var end = moment("{{$subtask->to_date}}").format('YYYY-MM-DD');
                        var url = '<span>المهمة الفرعية</span> : {{$subtask->name}}';
                        var subtask = { "id":{{$subtask->id}}, "name" : url, "start" : start, "end" : end, "color" : "#b7b7b7"};
                        subtasks.push(subtask);
                    @endforeach
            var task = { "id" : {{$task->id}}, "name" : "{{$i++}}- {{$task->name}}", "start" : start, "end" : end, "color" : "{{($task->taskStatus)?($task->taskStatus)->color:"#8aeeff"}}", "series": subtasks };
            tasks.push(task);
            @endforeach
            console.log(tasks);

            $("#gantt-chart").html("<div style='height:100px;'></div>");
            $("#gantt-chart").ganttView({
                data: tasks,
                monthNames: months,
                dayText: "يوم",
                daysText: "أيام"
            });

            $("#gantt-chart .toggle-grid").unbind();

            $(document).on("click", "#gantt-chart .toggle-grid",function(){
                var e=$(this).attr("data-target");
                console.log(($(this).attr("class")));
                if($(this).hasClass("grid-hidden")){
                    $(this).removeClass("grid-hidden");
                    $.each($(".ganttview-grid-row."+e), function (i,x) {
                        if(i!=0)
                            $(x).css("display", "block");
                    });
                    $.each($(".ganttview-block-container."+e), function (i,x) {
                        if(i!=0)
                            $(x).css("display", "block");
                    });
                    $.each($(".ganttview-vtheader-series."+e+" .ganttview-vtheader-series-row"), function (i,x) {
                        if(i!=0)
                            $(x).css("display", "block");
                    });
                }else{
                    $(this).addClass("grid-hidden");
                    $.each($(".ganttview-grid-row."+e), function (i,x) {
                        if(i!=0)
                            $(x).css("display", "none");
                    });
                    $.each($(".ganttview-block-container."+e), function (i,x) {
                        if(i!=0)
                            $(x).css("display", "none");
                    });
                    $.each($(".ganttview-vtheader-series."+e+" .ganttview-vtheader-series-row"), function (i,x) {
                        if(i!=0)
                            $(x).css("display", "none");
                    });
                }
            });

            $(".toggle-grid").trigger("click");

        });
    </script>

    <script>
        $(function () {
            $(document).on("change", ".kv-explorer-discussion", function (e) {
                if(e.target.files.length>0){
                    $(this).closest(".file-loading").append('<div class="deleteme"></div><input type="file" name="image[]" class="form-control kv-explorer-discussion mt-10">');
                }
            });
            $(document).on("click", ".deleteme", function () {
                $(this).next("input").remove();
                $(this).remove();
            });
        });
    </script>

    <style>
        .deleteme{
            position: relative;
        }
        .deleteme:after{
            cursor: pointer;
            position: absolute;
            content: "حذف";
            background: red;
            color: #fff;
            padding: 0px 5px;
            left: 10px;
            top: 16px;
        }
    </style>

@endsection