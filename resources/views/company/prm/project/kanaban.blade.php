<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold pull-right mt-5 mb-5"><i class="fa fa-clipboard"></i> لوحة المهام</h3>
        <span class="pull-left m-t-10">
            @if(permissions_prm("add_task"))
                <a href="{{route('company.prm.addTask', [$company->slug, $project->id])}}" class="btn btn-primary pull-left"> إضـافة مهمة </a>
            @endif
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">

    <div class="col-sm-12" style="position: relative">

        <a href="javascript:void(0)" onclick="$('#finances_filterk').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

        <form id="finances_filterk" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

            <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

            <div class="col-sm-12 mb-10">
                <input type="text" class="form-control datee date1" autocomplete="off" name="from" placeholder="من تاريخ">
            </div>

            <div class="col-sm-12 mb-10">
                <input type="text" class="form-control datee date2" autocomplete="off" name="to" placeholder="إلى تاريخ">
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x employees" autocomplete="off">
                    <option value="0">جميع الموظفين</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x status" autocomplete="off">
                    <option value="all">جميع الحالات</option>
                    <option value="0">جديدة</option>
                    @foreach($taskStatus as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-12 mb-10">
                <select class="select2x priority" autocomplete="off">
                    <option value="0">جميع الأولويات</option>
                    <option value="1">عادى</option>
                    <option value="2">عاجل</option>
                    <option value="3">عاجل جدا</option>
                    <option value="4">غير عاجل</option>
                </select>
            </div>

            <div class="col-sm-6">
                <button type="submit" class="btn btn-primary btn-block">فلتر</button>
            </div>

            <div class="col-sm-6">
                <button type="reset" class="btn btn-info btn-block">مسح</button>
            </div>

            <div class="clearfix"></div>

        </form>

    </div>

    <div class="col-sm-12"><hr></div>

    <div class="col-md-12 ">

        <div id="kanaban-wrapper" style="min-height: 400px">
            <div id="kanaban-div">
                <div class="statusBox" data-status="0">

                    <h4 class="text-center" style="background: #8aeeff;">
                        <span class="text-white">جديدة</span>
                    </h4>

                    <ul class="row connectedSortable" id="sortable{{$status->id}}" data-status="{{$status->id}}" style="padding: 0px;">
                            @foreach($tasks as $task)
                                @if($task->status==0)
                                <?php $uss=[]; foreach($task->users as $user) array_push($uss, $user->id); $uss = implode(",", $uss).",".$task->user_id;?>
                                <li class="post col-md-12 sortable kanaban-task ui-state-default" data-name="{{$task->name}}" style="cursor: move;background: transparent;border: navajowhite;"
                                    data-date0="{{$task->from_date}}" data-date00="{{$task->to_date}}"
                                    data-employees="{{$uss}}" data-status="{{$task->status}}" data-priority="{{$task->priority}}"
                                    data-status="{{$status->id}}" data-id="{{$task->id}}">

                                    <div class="grid simple vertical white tsk blue" data-id="{{$task->id}}">

                                        <div class="grid-body no-border padd-10">

                                            <div class="pull-right">
                                                <a href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" class="btn-showTask"  data-name="{{$task->name}}">{{$task->name}}</a>
                                                <div class="p-t-5">
                                                    @if(($task->to_date))
                                                        <span class="muted" style="margin-left: 10px;display: inline-block">{{$task->to_date}} <i class="fa fa-calendar-o"></i></span>
                                                    @endif
                                                    @if($task->priority==1)
                                                        <label class="label label-info" style="width: 100%;padding: 0px 22px;border-radius: 1px;">عادى</label>
                                                    @elseif($task->priority==2)
                                                        <label class="label label-warning" style="width: 100%;padding: 0px 22px;border-radius: 1px;">عاجل</label>
                                                    @elseif($task->priority==3)
                                                        <label class="label label-danger" style="width: 100%;padding: 0px 22px;border-radius: 1px;">عاجل جدا</label>
                                                    @elseif($task->priority==4)
                                                        <label class="label label-success" style="width: 100%;padding: 0px 22px;border-radius: 1px;">غير عاجل</label>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="pull-left">
                                                <div class="user-profile-pic-wrapper">
                                                    <div class="user-profile-pic-normal pull-left">
                                                        @if(@$task->user)
                                                            @if(@$task->user->image)
                                                                <img src="{{asset('storage/app/'.@$task->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                            @else
                                                                <img src="{{asset('public/no-image.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="username pull-right" style="padding: 6px 0px 0px 10px;">
                                                        @if(@$task->user)
                                                            <span class="dark-text">{{@$task->user->name}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>

                                    </div>

                                </li>
                                @endif
                            @endforeach
                    </ul>

                </div>
                @foreach($taskStatus as $status)
                    <div class="statusBox" data-status="{{$status->id}}">

                        <h4 class="text-center" style="background: {{$status->color}};">
                            <span class="text-white">{{$status->name}}</span>
                        </h4>

                        <ul class="row connectedSortable" id="sortable{{$status->id}}" data-status="{{$status->id}}" style="padding: 0px;">
                            @if(count($status->tasks)>0)
                                @foreach($status->tasks as $task)
                                    <?php $uss=[]; foreach($task->users as $user) array_push($uss, $user->id); $uss = implode(",", $uss).",".$task->user_id;?>
                                    <li class="post col-md-12 sortable kanaban-task ui-state-default" data-name="{{$task->name}}" style="cursor: move;background: transparent;border: navajowhite;"
                                         data-date0="{{$task->from_date}}" data-date00="{{$task->to_date}}"
                                         data-employees="{{$uss}}" data-status="{{$task->status}}" data-priority="{{$task->priority}}"
                                         data-status="{{$status->id}}" data-id="{{$task->id}}">

                                        <div class="grid simple vertical white tsk blue" data-id="{{$task->id}}">

                                            <div class="grid-body no-border padd-10">

                                                <div class="pull-right">
                                                    <a href="{{route('company.prm.showTask', [$company->slug, $task->id])}}" class="btn-showTask"  data-name="{{$task->name}}">{{$task->name}}</a>
                                                    <div class="p-t-5">
                                                        @if(($task->to_date))
                                                            <span class="muted" style="margin-left: 10px;display: inline-block">{{$task->to_date}} <i class="fa fa-calendar-o"></i></span>
                                                        @endif
                                                            @if($task->priority==1)
                                                                <label class="label label-info" style="width: 100%;padding: 0px 22px;border-radius: 1px;">عادى</label>
                                                            @elseif($task->priority==2)
                                                                <label class="label label-warning" style="width: 100%;padding: 0px 22px;border-radius: 1px;">عاجل</label>
                                                            @elseif($task->priority==3)
                                                                <label class="label label-danger" style="width: 100%;padding: 0px 22px;border-radius: 1px;">عاجل جدا</label>
                                                            @elseif($task->priority==4)
                                                                <label class="label label-success" style="width: 100%;padding: 0px 22px;border-radius: 1px;">غير عاجل</label>
                                                            @endif
                                                    </div>
                                                </div>

                                                <div class="pull-left">
                                                    <div class="user-profile-pic-wrapper">
                                                        <div class="user-profile-pic-normal pull-left">
                                                            @if(@$task->user)
                                                                @if(@$task->user->image)
                                                                    <img src="{{asset('storage/app/'.@$task->user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                                @else
                                                                    <img src="{{asset('public/no-image.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%;" width="35" height="35">
                                                                @endif
                                                            @endif
                                                        </div>
                                                        <div class="username pull-right" style="padding: 6px 0px 0px 10px;">
                                                            @if(@$task->user)
                                                                <span class="dark-text">{{@$task->user->name}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                            </div>

                                        </div>

                                    </li>
                                @endforeach
                            @else
                                <div class="post col-md-12 sortable kanaban-task" style="cursor: move" data-date0="1999-01-01" data-date00="1999-01-01" data-employees="0" data-status="0"></div>
                            @endif
                        </ul>

                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

</div>

<style>
    #kanaban-wrapper{
        background: #E4E8EB;
        padding: 5px 15px;
    }
    #kanaban-div{
        overflow-y: auto;
        min-height: 400px;
    }
    .statusBox{
        min-width: 400px;
        max-width: 400px;
        display: table-cell;
        padding-left: 15px;
    }
    .statusBox h4{
        padding: 10px 0px;
        margin: 10px 0px;
        font-weight: normal;
        color: #fff;
    }
    .post {
        display: block;
    }
    .grid {
        clear: both;
        margin-top: 0px;
        margin-bottom: 10px;
        padding: 0px;
    }
    .grid.simple {
        padding: 0px !important;
        -webkit-box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.15);
        -moz-box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.15);
        box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.15);
        border-top: 1px solid #e8e8e8 !important;
        border-bottom: none !important;
        border-left: 1px solid #e8e8e8 !important;
    }
    .grid.simple .grid-body.no-border {
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
        border-bottom: 1px solid #dddddd;
        padding: 15px;
        background: #fff;
    }
    .muted {
        color: #b6bfc5;
    }
    .post .dark-text {
        color: #2a2e36;
    }
</style>