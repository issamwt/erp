@extends("company.layouts.app", ["title"=>"تعديل مرحلة"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
        <small><span>نظام إدارة المشاريع</span> <span> > </span> <span>{{@$project->name}}</span> > <span>تعديل مرحلة</span></small>
    </section>

    <section class="content">

        <div class="tiles white p-t-15 p-l-15 p-r-15 p-b-25 bx-shade">

            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>تعديل مرحلة</strong>
                            <a href="{{route("company.prm.project", [@$company->slug, $project->id])}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-reply"></i> <span>العودة للمشروع</span></a>
                        </div>
                        <div class="panel-body">

                            <form action="{{route('company.prm.updateMilestone', @$company->slug)}}" method="post" enctype="multipart/form-data">
                                @csrf

                                <input type="hidden" name="id" value="{{$milestone->id}}">

                                <div class="row">

                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label>اسم المرحلة <span class="red">*</span></label>
                                            <input type="text" class="form-control"  name="name" value="{{$milestone->name}}" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label>من تاريخ</label>
                                            <input type="text" class="form-control datee" name="from_date" value="{{$milestone->from_date}}" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label>الى تاريخ</label>
                                            <input type="text" class="form-control datee" name="to_date" value="{{$milestone->to_date}}" autocomplete="off">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group slidecontainer">
                                            <label>نسبة التقدم <span id="num">{{$milestone->score}}%</span></label>
                                            <input type="range" min="0" max="100" class="form-control slider" id="score" name="score" value="{{$milestone->score}}" style="padding: 6px 0px;">
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-xs-12">
                                        <div class="form-group">
                                            <label>التفاصيل</label>
                                            <textarea class="form-control" name="details" rows="4">{{$milestone->details}}</textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section("scripts")

    <style>
        .slidecontainer {
            width: 100%;
        }

        .slider {
            direction: inherit;
            -webkit-appearance: none;
            width: 100%;
            height: 25px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }

        .slider:hover {
            opacity: 1;
        }

        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            background: #0090d9;
            cursor: pointer;
            border-radius: 50%;
        }

        .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            background: #007fbf;
            cursor: pointer;
        }
    </style>

    <script>

        $(function () {

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $("#score").on("input change", function () {
                var num = $(this).val();
                console.clear();
                console.log(num);
                if(num)
                    $("#num").html(num+"%");
            });

        });
    </script>

@endsection