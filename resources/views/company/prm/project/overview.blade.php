<div class="row">
    <div class="col-md-12">
        <h3 class="semi-bold mt-5 mb-5"><i class="fa fa-dashboard"></i> نظرة عامة</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="form-label"> <i class="fa fa-info-circle"></i> نبذة عن المشروع</label>
            <div class="controls">
                <div class="well bg-grey">
                    @if($project->brief and $project->brief!="")
                        <p>{{$project->brief}}</p>
                    @else
                        <p>لا توجد نبذة عن هذا المشروع</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="user-description-box no-margin well bg-grey">
            <table class="table table-bordered">
                <tr>
                    <td width="40%"><b>إسم العميل</b></td>
                    <td width="60%">{{@$project->client->name}}</td>
                </tr>
                <tr>
                    <td width="40%"><b>تاريخ بداية المشروع</b></td>
                    <td width="60%"><span style="display: inline-block;padding-right: 4px;"> {{@$project->date_from}} </span></td>
                </tr>
                <tr>
                    <td width="40%"><b>تاريخ نهاية المشروع</b></td>
                    <td width="60%"><span style="display: inline-block;padding-right: 4px;"> {{@$project->date_to}} </span></td>
                </tr>
                <tr>
                    <td width="40%"><b>مدة المشروع</b></td>
                    <td width="60%">
                        <?php
                            $end_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', @$project->date_to." 00:00:01");
                            $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', @$project->date_from." 00:00:01");
                            $totalDuration = @$end_time->diffInSeconds(@$start_time);
                            $day  = intval(gmdate('d', @$totalDuration)-1);
                            $hour  = intval(gmdate('H', @$totalDuration));
                            $minute  = intval(gmdate('i', @$totalDuration));
                            $string = ($day!=0)?$day." أيام <br>":" ";
                            $string .=" ".(($hour!=0)?$hour." ساعات <br>":" ");
                            $string .=" ".(($minute!=0)?$minute." دقائق":" ");
                        ?>
                            <span style="display: inline-block;padding-right: 4px;"> {!! $string !!} </span>
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 text-center">

        <div id="donut-example" style="height:200px; margin-top: 5px;"></div>

        <ul class="list-unstyled list-inline">

            @foreach($taskStatus as $status)
                <li>
                    <span>{{$status->name}}</span> <span class="tasks_indcator tsk_g_bg" style="background: {{$status->color}} !important;"></span>
                </li>
            @endforeach
        </ul>

    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="grid simple white">
            <div class="grid-title">
                <div class="controls">
                    <h4 class="m-b-10 pull-right">
                        العاملين على المشروع
                    </h4>
                    <a href="#" data-target="#addContributer" class="btn btn-primary btn-open m-b-10 pull-left"><i class="fa fa-plus"></i> <span>إضـافة مساعد</span></a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="grid-body">

                <div id="collaborators-wrapper">
                    <div id="collaborators">

                        <?php  $collaboratorsIds = []; foreach(@$project->users as $user) array_push($collaboratorsIds, @$user->id); ?>

                        <div id="addContributer" class="formi">
                            <div class="form-group">
                                <label>إسم المساعد</label>
                                <select class="select-hide" style="width: 100%" id="collaboratorsList">
                                    <option value="">إختر المساعد</option>
                                    @foreach($users as $user)
                                        @if(!in_array($user->id, $collaboratorsIds))
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-0">
                                <button class="btn btn-info" id="addCollaborator"><i class="fa fa-save"></i> <span>إضافة</span></button>
                            </div>
                        </div>

                        <table class="table table-striped table-bordered table-hover mt-10" width="100%">
                            <thead>
                            <tr>
                                <th width="55">الرقم</th>
                                <th>الموظف</th>
                                <th>الرتبة</th>
                                <th width="200">الأوامر</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count(@$project->users)>0)
                                @foreach(@$project->users as $user)
                                    <?php  array_push($collaboratorsIds, @$user->id); ?>
                                    <tr>
                                        <td class="v-align-inh">{{$loop->iteration}}</td>
                                        <td>
                                            <div class="m-t-15">
                                                <div class="client-pic">
                                                    @if(@$user->image)
                                                        <img src="{{asset('storage/app/'.@$user->image)}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%; width: 35px; height: 35px;">
                                                    @else
                                                        <img src="{{asset('public/noimage.png')}}" alt="" style="border: 1px solid #cfcfcf; border-radius: 50%; width: 35px; height: 35px;">
                                                    @endif
                                                </div>
                                                <a href="{{route('company.users.show', [$company->slug, @$user->id])}}" class="client-name-project">{{@$user->name}} @if(@$user->id==$project->user_id) <small>(مدير المشروع)</small> @endif</a>
                                            </div>
                                        </td>
                                        <td class="v-align-inh">{{@$user->role->name}}</td>
                                        <td class="text-center">
                                            <a href="#" data-toggle="tooltip" title="إرسال إيميل" data-target="#sendContributer{{$user->id}}" class="btn btn-primary btn-open"><i class="fa fa-envelope-o"></i></a>
                                            <a href="{{route('company.users.show', [$company->slug, @$user->id])}}" data-toggle="tooltip" title="مشاهدة" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                            @if(@$user->pivot->type!=1)
                                                <a href="#" data-toggle="tooltip" title="حذف" class="btn btn-danger deleteCollaborator" data-id="{{@$user->id}}"><i class="fa fa-remove"></i></a>
                                            @else
                                                <a href="#" class="btn btn-danger disabled"><i class="fa fa-remove"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="padding: 0px; background: #fff;">
                                            <div class="formi" id="sendContributer{{$user->id}}">
                                                <h5 class="mt-5 mb-15">أرسل رسالة ل "{{@$user->name}}"</h5>
                                                <form action="{{route('company.prm.sendCollaborator', $company->slug)}}" class="sendCollaborator" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" class="user_id" name="user_id" value="{{@$user->id}}">
                                                    <div class="form-group">
                                                        <label>الموضوع</label>
                                                        <input class="form-control subject" name="subject" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>نص الرسالة</label>
                                                        <textarea class="form-control msg" name="msg" required rows="5"></textarea>
                                                    </div>
                                                    <div class="form-group mb-0">
                                                        <button type="submit" class="btn btn-primary sendCollaborator"><i class="fa fa-envelope"></i> <span>أرسل</span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td class="text-center" colspan="4"><b>لا توجد</b></td></tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
