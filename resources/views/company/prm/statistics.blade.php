
@extends("company.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                @section("content")

                    <section class="content-header">
                        <h1>نظام إدارة المشاريع</h1>
                    </section>

                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12" style="margin-bottom: 100px; padding: 20px;">

                                <div class="col-sm-12"  style="border: 1px solid #d6d6d6; padding: 15px; background: #fff;">

                                    <div class="row">

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h4>عدد المشاريع</h4>
                                                    <h4>{{@$projects_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <a href="{{route("company.prm.projects", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h4>عدد المهام</h4>
                                                    <h4>{{@$tasks_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-tasks"></i>
                                                </div>
                                                <a href="{{route("company.prm.projects", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h4>عدد النقاشات</h4>
                                                    <h4>{{@$disc_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-comments"></i>
                                                </div>
                                                <a href="{{route("company.prm.projects", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-red">
                                                <div class="inner">
                                                    <h4>عدد الملفات</h4>
                                                    <h4>{{@$files_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-archive"></i>
                                                </div>
                                                <a href="{{route("company.prm.projects", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <table class="table table-bordered col-sm-12 mt-20"  style="border: 1px solid #d6d6d6 !important; padding: 15px; background: #fff;">
                                    <tr>
                                        <td width="20%" style="position: relative;">
                                            <h2 class="text-center" style="margin: 10px 0px 20px;">المشاريع</h2>
                                            <a href="{{route("company.prm.projects", @$company->slug)}}" class="small-box-footer pull-left" style="position: absolute;bottom: 10px;left: 10px;"><i class="fa fa-arrow-circle-right"></i> <span>التفاصيل</span> </a>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart1" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>تحت الدراسة</span> <span>{{($projects_num==0)?0:intval($projects_new_num/$projects_num*100)}} %</span></h4>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart2" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>قيد التنفيذ</span> <span>{{($projects_num==0)?0:intval($projects_active_num/$projects_num*100)}} %</span></h4>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart3" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>مؤرشفة</span> <span>{{($projects_num==0)?0:intval($projects_completed_num/$projects_num*100)}} %</span></h4>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table table-bordered col-sm-12"  style="border: 1px solid #d6d6d6 !important; padding: 15px; background: #fff;">
                                    <tr>
                                        <td width="20%" style="position: relative;">
                                            <h2 class="text-center" style="margin: 10px 0px 20px;">المهام</h2>
                                            <a href="{{route("company.prm.projects", @$company->slug)}}" class="small-box-footer pull-left" style="position: absolute;bottom: 10px;left: 10px;"><i class="fa fa-arrow-circle-right"></i> <span>التفاصيل</span> </a>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart4" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>غير محددة</span> <span>{{($tasks_num==0)?0:intval($tasks_num0/$tasks_num*100)}} %</span></h4>
                                                </div>
                                                @foreach($task_statuses as $ts)
                                                    <div class="col-sm-4">
                                                        <div class="chart tab-pane active" id="chartx{{$ts->id}}" style="position: relative; height: 200px;"></div>
                                                        <h4 class="text-center mb-20"><span>{{$ts->name}}</span> <span>{{($tasks_num==0)?0:intval($ts->tasks_count/$tasks_num*100)}} %</span></h4>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </section>

                @endsection


            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .small-box{
            margin: 0px;
        }
        .table-bordered td{
            border-color: #d6d6d6 !important;
        }
        .fc-content{
            padding: 3px;
            border-radius: 0px !important;
        }
        .fc-day-grid-event{
            border-radius: 0px !important;
        }
    </style>

    <link rel="stylesheet" href="{{asset("public/plugins/morris/morris.css")}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{asset("public/plugins/morris/morris.min.js")}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.min.css"  media="print"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

    <script>

        $(function () {

            var completed = {{($projects_num==0)?0:intval($projects_new_num/$projects_num*100)}};
            var donut1 = new Morris.Donut({
                element: 'chart1',
                resize: true,
                colors: ["#1b80e2", "#d6d6d6"],
                data: [
                    {label: "تحت الدراسة", value: completed},
                    {label: "البقية", value: (100-completed)},
                ],
                hideHover: 'auto'
            });
            donut1.select(0);

            var coming = {{($projects_num==0)?0:intval($projects_active_num/$projects_num*100)}};
            var donut2 = new Morris.Donut({
                element: 'chart2',
                resize: true,
                colors: ["#00a65a", "#d6d6d6"],
                data: [
                    {label: "قيد التنفيذ", value: coming},
                    {label: "البقية", value: (100-coming)},
                ],
                hideHover: 'auto'
            });
            donut2.select(0);

            var canceled = {{($projects_num==0)?0:intval($projects_completed_num/$projects_num*100)}};
            var donut3 = new Morris.Donut({
                element: 'chart3',
                resize: true,
                colors: ["#ff0000", "#d6d6d6"],
                data: [
                    {label: "مؤرشفة", value: canceled},
                    {label: "البقية", value: (100-canceled)},
                ],
                hideHover: 'auto'
            });
            donut3.select(0);

            var finished = {{($tasks_num==0)?0:intval($tasks_num0/$tasks_num*100)}};
            var donut4 = new Morris.Donut({
                element: 'chart4',
                resize: true,
                colors: ["#8aeeff", "#d6d6d6"],
                data: [
                    {label: "غير محددة", value: finished},
                    {label: "البقية", value: (100-finished)},
                ],
                hideHover: 'auto'
            });
            donut4.select(0);

            @foreach($task_statuses as $ts)
                var ooz = {{($tasks_num==0)?0:intval($ts->tasks_count/$tasks_num*100)}};
                var chikooz = new Morris.Donut({
                    element: 'chartx{{$ts->id}}',
                    resize: true,
                    colors: ["{{$ts->color}}", "#d6d6d6"],
                    data: [
                        {label: "{{$ts->name}}", value: ooz},
                        {label: "البقية", value: (100-ooz)},
                    ],
                    hideHover: 'auto'
                });
                chikooz.select(0);
            @endforeach
        });

    </script>

@endsection