@extends("company.layouts.app", ["title"=>"قائمة المشاريع"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>قائمة المشاريع</strong>
                        @if(permissions_prm("add_project"))
                            <a href="{{route("company.prm.add_project", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-plus"></i> <span>إضافة مشروع</span></a>
                        @endif
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#filter_projects').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                <form id="filter_projects" class="row" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

                                    <div class="col-sm-12 mb-10">
                                        <div class="checkbox check-info" style="display: block">
                                            <input id="checkbox_all" type="checkbox" class="projectstatus"  value="all" name="status[]">
                                            <label for="checkbox_all" style="margin-left: 15px">الكل </label>
                                        </div>
                                        <div class="checkbox check-info" style="display: block">
                                            <input id="checkbox_01" type="checkbox" class="projectstatus"  value="0" name="status[]">
                                            <label for="checkbox_01" style="margin-left: 15px"> مشاريع جديدة</label>
                                        </div>
                                        <div class="checkbox check-info" style="display: block">
                                            <input id="checkbox_02" type="checkbox" class="projectstatus"  value="1" name="status[]">
                                            <label for="checkbox_02" style="margin-left: 15px"> مشاريع قيد التنفيذ</label>
                                        </div>
                                        <div class="checkbox check-info" style="display: block">
                                            <input id="checkbox_03" type="checkbox" class="projectstatus"  value="2" name="status[]">
                                            <label for="checkbox_03"> مشاريع المؤرشفة</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 mb-10" style="padding-top: 5px">
                                        <select class="select2x full-width projectsuser" autocomplete="off">
                                            <option value="all">جميع الموظفين</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10" style="padding-top: 5px">
                                        <select class="select2x full-width projectsclients" autocomplete="off">
                                            <option value="all">جميع العملاء</option>
                                            @foreach($clients as $client)
                                                <option value="{{$client->id}}">{{$client->name}}</option>
                                            @endforeach
                                            <option class="0">بدون عميل</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <div class="row">
                                            <div class="text-center col-sm-6" style="padding-top: 5px">
                                                <button type="submit" class="btn btn-block btn-primary btn-shade">فلتر</button>
                                            </div>

                                            <div class="text-center col-sm-6" style="padding-top: 5px">
                                                <button type="reset" class="btn btn-block btn-default btn-shade">فسخ</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>

                            <div class="col-sm-12"><hr></div>

                            <div class="col-sm-12">

                                <table class="table table-bordered"  id="tableProjects" style="font-size: 0.9em;">
                                    <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>الحالة</th>
                                        <th>تاريخ الإنتهاء</th>
                                        <th>عنوان المشروع</th>
                                        <th>نوع المشروع</th>
                                        <th>مدير المشروع</th>
                                        <th>العميل</th>
                                        <th>الأوامر</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($projects)>0)
                                        @foreach($projects as $project)
                                        <?php $pusers  = [];?>
                                        @foreach($project->users as $u) <?php array_push($pusers, $u->id);  ?> @endforeach
                                        <?php $pusers = implode(",", $pusers); ?>
                                        <tr class="odd gradeX" data-projectstatus="{{$project->status}}"
                                            data-projectsclients="{{($project->client_id)?$project->client_id:0}}"
                                            data-projectsuser="{{$pusers}}">
                                            <td class="text-center" style="vertical-align: middle;"><strong>{{$loop->iteration}}</strong></td>
                                            <td style="vertical-align: middle;" class="case-status-td text-center">
                                                @if($project->status==0)
                                                    <div class="case-status-indcator case-bg-orange"></div>
                                                @elseif($project->status==1)
                                                    <div class="case-status-indcator case-bg-blue"></div>
                                                @elseif($project->status==2)
                                                    <div class="case-status-indcator case-bg-gray"></div>
                                                @elseif($project->status==3)
                                                    <div class="case-status-indcator case-bg-green"></div>
                                                @else
                                                    <div class="case-status-indcator case-bg-red"></div>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;">
                                                @if($project->status==4)
                                                    <span style="color: darkred; text-decoration: line-through;">{{@$project->date_to}}</span>
                                                @else
                                                    <span>{{@$project->date_to}}</span>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;"><a href="{{route('company.prm.project', [$company->slug, $project->id])}}">{{$project->name}}</a></td>
                                            <td style="vertical-align: middle;">{{@$project->type->name}}</td>
                                            <td style="vertical-align: middle;">
                                                @if(@$project->manager)
                                                    <a href="{{route("company.users.show", [@$company->slug, @$project->manager->id])}}">
                                                        @if(@$project->manager->image)
                                                            <img src="{{asset("storage/app/".@$project->manager->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                        @else
                                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                        @endif
                                                        <h5 style="display: inline-block;font-size: 0.9em;">{{@$project->manager->name}}</h5>
                                                    </a>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;">
                                                @if(@$project->client)
                                                    <a href="{{route("company.clients.show", [@$company->slug, @$project->client->id])}}">
                                                        @if(@$project->client->image)
                                                            <img src="{{asset("storage/app/".@$project->client->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                        @else
                                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: inline-block;">
                                                        @endif
                                                        <h5 style="display: inline-block;font-size: 0.9em;">{{@$project->client->name}}</h5>
                                                    </a>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;" class="text-center">
                                                <a href="{{route('company.prm.project', [$company->slug, $project->id])}}" class="btn btn-50 btn-xs btn-primary" data-toggle="tooltip" title="عرض"><i class="fa fa-eye"></i></a>
                                                @if(permissions_prm("update_project"))
                                                    <a href="{{route('company.prm.edit_project', [$company->slug, $project->id])}}" class="btn btn-50 btn-xs btn-info" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                                                @endif
                                                @if(permissions_prm("delete_project"))
                                                    <a href="{{route('company.prm.delete_project', [$company->slug, $project->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-50 btn-xs btn-danger" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                                                @endif
                                                {{--<a href="{{route('company.prm.pdf_project', [$company->slug, $project->id])}}" class="btn btn-50 btn-xs btn-success" data-toggle="tooltip" title="طباعة"><i class="fa fa-print"></i></a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">لا توجد مشاريع حاليا</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>

                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="col-md-12">

                                <div class="mt-10">
                                    <div class="case-status-indcator case-bg-orange"></div>
                                    <b>مشروع جديد</b>
                                </div>
                                <div class="mt-10">
                                    <div class="case-status-indcator case-bg-blue"></div>
                                    <b>مشروع قيد التنفيذ</b>
                                </div>
                                <div class="mt-10">
                                    <div class="case-status-indcator case-bg-green"></div>
                                    <b>مشروع منجز</b>
                                </div>
                                <div class="mt-10">
                                    <div class="case-status-indcator case-bg-gray"></div>
                                    <b>مشروع مؤرشف</b>
                                </div>
                                <div class="mt-10">
                                    <div class="case-status-indcator case-bg-red"></div>
                                    <b>مشروع متأخر</b>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection


@section("scripts")

    <style>
        .case-status-td {
            width: 15px;
            padding: 5px;
        }
        .case-status-indcator {
            width: 16px;
            margin: 0 auto;
            height: 16px;
            border: 3px solid rgba(0, 0, 0, 0.18);
            border-radius: 50%;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.18);
            display: inline-block;
        }

        .case-bg-blue {background: #8aeeff}/*8bc1e8*/
        .case-bg-green {background: #77ff2d}/*84d25a*/
        .case-bg-orange {background: #fffdab}/*e8c684*/
        .case-bg-gray {background: #c5c5c5}/*e8c684*/
        .case-bg-red {background: #f399b5}
    </style>

    <script>
        $(function () {

            $(document).on("submit", "#filter_projects", function (e) {
                e.preventDefault();

                var projectstatus = [];
                $(".projectstatus:checked", this).each(function(){
                    projectstatus.push($(this).val());
                });
                var projectsclients = $(".projectsclients", this).select2('val')+"";
                var projectsuser = $(".projectsuser", this).select2('val')+"";

                $("#tableProjects tbody tr").hide();
                $("#tableProjects tbody tr").filter(function () {
                    var projectstatusx = true;
                    var projectstatus0 = $(this).data('projectstatus');
                    if(projectstatus.length>0 && !projectstatus.includes("all")){
                        if(!projectstatus.includes(projectstatus0.toString()))
                            projectstatusx = false;
                    }
                    var projectsclientsx = true;
                    var projectsclients0 = $(this).data("projectsclients").toString();
                    if(projectsclients){
                        if(projectsclients!="all")
                            if(projectsclients!=projectsclients0.toString())
                                projectsclientsx = false;
                    }

                    var projectsuserx = true;
                    var projectsuser0 = $(this).data("projectsuser").toString();
                    projectsuser0 = projectsuser0.split(",");
                    if(projectsuser!="all")
                        if(!projectsuser0.includes(projectsuser.toString()))
                            projectsuserx = false;

                    return (projectstatusx && projectsclientsx && projectsuserx);
                }).show();

            });

            $(document).on("reset", "#filter_projects", function (e) {
                e.preventDefault();
                $(".projectstatus").prop('checked', false);
                $('.projectsclients').val(null).trigger('change');
                $('.projectsuser').val(null).trigger('change');
                $("#tableProjects tbody tr").show();
            });

        });
    </script>

@endsection