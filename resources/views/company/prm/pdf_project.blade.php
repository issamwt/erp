@extends("company.layouts.app", ["title"=>@$project->name])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
        <small><span>نظام إدارة المشاريع</span> <span> > </span> <span>{{@$project->name}}</span></small>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>طباعة المشروع</strong> <a href="{{route("company.prm.projects", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>العودة للقائمة</span></a></div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">Coming Soon</div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection