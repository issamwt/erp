@extends("company.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                @section("content")

                    <section class="content-header">
                        <h1>نظام إدارة المشاريع</h1>
                    </section>

                    <section class="content">

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">حالات المهام</div>
                                    <div class="panel-body">
                                        <div id="div1-wrapper">
                                            <div id="div1">
                                                @include("company.prm.settings.task_status")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">أنواع المشاريع</div>
                                    <div class="panel-body">
                                        <div id="div2-wrapper">
                                            <div id="div2">
                                                @include("company.prm.settings.projects")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">أنواع المصاريف</div>
                                    <div class="panel-body">
                                        <div id="div3-wrapper">
                                            <div id="div3">
                                                @include("company.prm.settings.expenses")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">الضرائب و النسب</div>
                                    <div class="panel-body">
                                        <div id="div4-wrapper">
                                            <div id="div4">
                                                @include("company.prm.settings.taxes")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">أنواع الفواتير</div>
                                    <div class="panel-body">
                                        <div id="div5-wrapper">
                                            <div id="div5">
                                                @include("company.prm.settings.invoices")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">طرق الدفع</div>
                                    <div class="panel-body">
                                        <div id="div6-wrapper">
                                            <div id="div6">
                                                @include("company.prm.settings.payment_methods")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">الإشعارات</div>
                                    <div class="panel-body">
                                        <div id="div7-wrapper">
                                            <div id="div7">
                                                @include("company.prm.settings.notifications")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        {{--
                        <div class="row">

                            <div class="col-xs-12" style="margin-bottom: 100px;">

                                <div class="tab">
                                    <button class="tablinks @if(!session('tabtab') or session('tabtab')=="menu1") active @endif" href="#menu1">حالات المهام</button>
                                    <button class="tablinks @if(session('tabtab')=="menu2") active @endif" href="#menu2">أنواع المشاريع</button>
                                    <button class="tablinks @if(session('tabtab')=="menu3") active @endif" href="#menu3">أنواع المصاريف</button>
                                    <button class="tablinks @if(session('tabtab')=="menu4") active @endif" href="#menu4">الضرائب و النسب</button>
                                    <button class="tablinks @if(session('tabtab')=="menu5") active @endif" href="#menu5">أنواع الفواتير</button>
                                    <button class="tablinks @if(session('tabtab')=="menu6") active @endif" href="#menu6">طرق الدفع</button>
                                    <button class="tablinks @if(session('tabtab')=="menu8") active @endif" href="#menu8">الإشعارات</button>
                                </div>

                                <div id="menu1" class="tabcontent @if(!session('tabtab') or session('tabtab')=="menu1") active @endif">
                                    @include("company.prm.settings.task_status")
                                </div>
                                <div id="menu2" class="tabcontent @if(session('tabtab')=="menu2") active @endif">
                                    @include("company.prm.settings.projects")
                                </div>
                                <div id="menu3" class="tabcontent @if(session('tabtab')=="menu3") active @endif">
                                    @include("company.prm.settings.expenses")
                                </div>
                                <div id="menu4" class="tabcontent @if(session('tabtab')=="menu4") active @endif">
                                    @include("company.prm.settings.taxes")
                                </div>
                                <div id="menu5" class="tabcontent @if(session('tabtab')=="menu5") active @endif">
                                    @include("company.prm.settings.invoices")
                                </div>
                                <div id="menu6" class="tabcontent @if(session('tabtab')=="menu6") active @endif">
                                    @include("company.prm.settings.payment_methods")
                                </div>
                                <div id="menu8" class="tabcontent @if(session('tabtab')=="menu8") active @endif">
                                    @include("company.prm.settings.notifications")
                                </div>

                            </div>
                        </div>
                        --}}

                    </section>

                @endsection

            </div>
        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <style>

        .tab {
            float: right;
            border: 1px solid #2d6a8d;
            background-color: #fff;
            width: 15%;
            min-height: 300px;
        }

        .tab button {
            display: block;
            background: #3c8dbc;
            padding: 10px 5px;
            width: 100%;
            border: none;
            outline: none;
            cursor: pointer;
            transition: 0.3s;
            font-size: 17px;
            color: #fff;
            text-align: right;
        }

        .tab button:hover, .tab button.active {
            background-color: #2d6a8d;
        }

        .tabcontent {
            float: left;
            padding: 0px 12px;
            border: 1px solid #2d6a8d;
            width: 85%;
            min-height: 400px;
            display: none;
            background: #fff;
            padding-bottom: 40px;
        }

        .tabcontent.active {
            display: block;
        }

        div.bhoechie-tab-container{
             z-index: 10;
             background-color: #ffffff;
             padding: 0 !important;
             border:1px solid #ddd;
             -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
             box-shadow: 0 6px 12px rgba(0,0,0,.175);
             -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
             background-clip: padding-box;
             opacity: 0.97;
             filter: alpha(opacity=97);
            margin-top: 15px;
         }

        div.bhoechie-tab-menu{
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group{
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a{
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a .glyphicon,
        div.bhoechie-tab-menu div.list-group>a .fa {
            color: #5A55A3;
        }

        div.bhoechie-tab-menu div.list-group>a:first-child{
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group>a:last-child{
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group>a.active,
        div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group>a.active .fa{
            background-color: #337ab7;
            border-radius: 0px;
            color: #ffffff;
        }

        div.bhoechie-tab-menu div.list-group>a.active:after{
            content: '';
            position: absolute;
            right: 100%;
            top: 50%;
            margin-top: -13px;
            border-left: 0;
            border-bottom: 13px solid transparent;
            border-top: 13px solid transparent;
            border-right: 10px solid #337ab7;
        }

        div.bhoechie-tab-content{
            background-color: #ffffff;
            /* border: 1px solid #eeeeee; */
            padding-left: 20px;
            padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active){
            display: none;
        }

        .bhoechie-tab{
            padding-top: 10px;
            padding-bottom: 120px;
            min-height: 400px;
            border-right: 1px solid #ddd;
        }

        .icheckbox_square-blue{
            margin-left: 10px;
        }
        .formi {
            display: none;
            border: 1px solid #2d6a8d;
            padding: 15px 20px;
            margin: 15px auto;
        }
        .panel-heading{
            cursor: pointer;
        }
    </style>

    <script type="text/javascript" src="{{asset("public/plugins/user-color-picker/js/jquery.minicolors.min.js")}}"></script>
    <link rel="stylesheet" type="text/css" media="all" href="{{asset("public/plugins/user-color-picker/css/jquery.minicolors.css")}}">

    <script>

        function colorrpicker() {
            var colpick = $('.demo').each(function () {
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: 'lowercase',
                    opacity: false,
                    change: function (hex, opacity) {
                        if (!hex) return;
                        if (opacity) hex += ', ' + opacity;
                        try {
                            console.log(hex);
                        } catch (e) {
                        }
                        $(this).select();
                    },
                    theme: 'bootstrap'
                });
            });
        }

        $(document).on("submit", ".form1", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div1-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_task_status', @$company->slug)}}', $(form).serialize(), function () {
                $("#div1-wrapper").load(window.location + " #div1", function () {
                    goNotif("success", "تم حفظ البيانات !");
                    colorrpicker();
                });
            });
        });

        $(document).on("click", ".btn-delete1", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div1-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div1-wrapper").load(window.location + " #div1", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        colorrpicker();
                    });
                });
            }
        });

        $(document).on("submit", ".form11", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div1-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.edit_task_status', @$company->slug)}}', $(form).serialize(), function () {
                $("#div1-wrapper").load(window.location + " #div1", function () {
                    goNotif("success", "تم حفظ البيانات !");
                    colorrpicker();
                });
            });
        });

        $(document).on("submit", ".form2", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div2-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_projectType', @$company->slug)}}', $(form).serialize(), function () {
                $("#div2-wrapper").load(window.location + " #div2", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("click", ".btn-delete2", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div2-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div2-wrapper").load(window.location + " #div2", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });

        $(document).on("submit", ".form22", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div2-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.update_ProjectType', @$company->slug)}}', $(form).serialize(), function () {
                $("#div2-wrapper").load(window.location + " #div2", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("submit", ".form3", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div3-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_expense_category', @$company->slug)}}', $(form).serialize(), function () {
                $("#div3-wrapper").load(window.location + " #div3", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("click", ".btn-delete3", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div3-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div3-wrapper").load(window.location + " #div3", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });

        $(document).on("submit", ".form33", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div3-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.update_expense_category', @$company->slug)}}', $(form).serialize(), function () {
                $("#div3-wrapper").load(window.location + " #div3", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("submit", ".form4", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div4-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_tax', @$company->slug)}}', $(form).serialize(), function () {
                $("#div4-wrapper").load(window.location + " #div4", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("click", ".btn-delete4", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div4-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div4-wrapper").load(window.location + " #div4", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });

        $(document).on("submit", ".form44", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div4-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.update_tax', @$company->slug)}}', $(form).serialize(), function () {
                $("#div4-wrapper").load(window.location + " #div4", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("submit", ".form444", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div4-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_percentage', @$company->slug)}}', $(form).serialize(), function () {
                $("#div4-wrapper").load(window.location + " #div4", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("click", ".btn-delete444", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div4-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div4-wrapper").load(window.location + " #div4", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });

        $(document).on("submit", ".form4444", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div4-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.update_percentage', @$company->slug)}}', $(form).serialize(), function () {
                $("#div4-wrapper").load(window.location + " #div4", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("submit", ".form5", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div5-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_invoice_category', @$company->slug)}}', $(form).serialize(), function () {
                $("#div5-wrapper").load(window.location + " #div5", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("click", ".btn-delete5", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div5-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div5-wrapper").load(window.location + " #div5", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });

        $(document).on("submit", ".form55", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div5-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.update_invoice_category', @$company->slug)}}', $(form).serialize(), function () {
                $("#div5-wrapper").load(window.location + " #div5", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });

        $(document).on("submit", ".form6", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div6-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.add_payment_method', @$company->slug)}}', $(form).serialize(), function () {
                $("#div6-wrapper").load(window.location + " #div6", function () {
                    goNotif("success", "تم حفظ البيانات !");
                    $('input:not(.no_ichek)').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%' // optional
                    });
                });
            });
        });

        $(document).on("click", ".btn-delete6", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div6-wrapper").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div6-wrapper").load(window.location + " #div6", function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            }
        });

        $(document).on("submit", ".form66", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div6-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.settings.update_payment_method', @$company->slug)}}', $(form).serialize(), function () {
                $("#div6-wrapper").load(window.location + " #div6", function () {
                    goNotif("success", "تم حفظ البيانات !");
                    $('input:not(.no_ichek)').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%' // optional
                    });
                });
            });
        });

        $(document).on("submit", ".form7", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div7-wrapper").html($("#loading-wrapper").html());
            $.post('{{route('company.prm.savenotif', @$company->slug)}}', $(form).serialize(), function () {
                $("#div7-wrapper").load(window.location + " #div7", function () {
                    goNotif("success", "تم حفظ البيانات !");
                    $('input:not(.no_ichek)').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%' // optional
                    });
                });
            });
        });

    </script>

    <script>
        $(function () {
            $(document).on("click", ".panel-heading", function (e) {
                e.preventDefault();
                $(this).parent().find(".panel-body").slideToggle();
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {

            $(document).on("click", ".tablinks", function () {
                var id =  $(this).attr("href");
                $(".tabcontent").removeClass("active");
                $(id).addClass("active");
                $(".tab button").removeClass("active");
                $(this).addClass("active");
                id = id.substring(1);
                console.log("{{route("company.prm.save_tabtab", @$company->slug)}}/"+id);
                $.get("{{route("company.prm.save_tabtab", @$company->slug)}}/"+id);
            });

            $(document).on("click", "#btn_add_task_status", function (e) {
                $("#add_task_status").slideToggle();
            });

            $(document).on("click", "#btn_addProjectTypeModal", function (e) {
                $("#addProjectTypeModal").slideToggle();
            });

            $(document).on("click", "#btn_add_expense_category", function (e) {
                $("#add_expense_category").slideToggle();
            });

            $(document).on("click", "#btn_add_tax", function (e) {
                $("#add_tax").slideToggle();
            });

            $(document).on("click", "#btn_add_percentage", function (e) {
                $("#add_percentage").slideToggle();
            });

            $(document).on("click", "#btn_add_invoice_category", function (e) {
                $("#add_invoice_category").slideToggle();
            });

            $(document).on("click", "#btn_add_payment_method", function (e) {
                $("#add_payment_method").slideToggle();
            });

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            colorrpicker();

            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });

        });
    </script>

@endsection