<div class="row">

    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12 mt-20">
                <button type="button" class="btn btn-primary" id="btn_add_invoice_category" data-target="#add_invoice_category"><i class="fa fa-plus"></i> <span>إضافة نوع فاتورة جديد</span></button>
            </div>
        </div>

        <form action="{{route('company.prm.settings.add_invoice_category', @$company->slug)}}" method="post" class="formi form5" id="add_invoice_category">
                @csrf

                <div class="form-group">
                    <label class="semi-bold">الإسم</label>
                    <input type="text" name="name" class="form-control" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                </div>

            </form>

    </div>

</div>

<div class="row mt-15">

    <div class="col-sm-12">

        <table class="table table-bordered table-striped dt-responsive last-col grd_view">
            @foreach($invoiceCategories as $invoiceCategory)
                <tr class="odd gradeX">
                    <td width="50%" style="text-align: center; vertical-align: middle;">{{$invoiceCategory->name}}</td>
                    <td width="25%" style="text-align: center; vertical-align: middle;">
                        <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#edit_invoice_category{{$invoiceCategory->id}}"><i class="fa fa-edit"></i></a>
                    </td>
                    <td width="25%" style="text-align: center; vertical-align: middle;">
                            <a href="{{route("company.prm.settings.delete_invoice_category", ["company"=>$company->slug, "id"=>$invoiceCategory->id])}}"  class="btn btn-danger btn-delete5" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 0px 10px">
                        <form action="{{route('company.prm.settings.update_invoice_category', @$company->slug)}}" method="post" class="formi form55" id="edit_invoice_category{{$invoiceCategory->id}}">
                            @csrf

                            <div class="form-group">
                                <label class="semi-bold">الإسم</label>
                                <input type="text" name="name" class="form-control" value="{{$invoiceCategory->name}}" required>
                                <input type="hidden" name="id" value="{{$invoiceCategory->id}}">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">حفظ</button>
                            </div>

                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

</div>
