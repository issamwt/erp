<div class="row">

    <div class="col-md-12">

        <a class="btn btn-primary mt-20" href="#" id="btn_add_task_status"><i class="fa fa-plus"></i> <span>أضف حالة مهام جديدة</span></a>

        <div id="add_task_status" class="formi">
            <form action="{{route('company.prm.settings.add_task_status', @$company->slug)}}" method="post" class="form1">
                @csrf

                <div class="form-group">
                    <label class="semi-bold">الإسم</label>
                    <input type="text" name="name" class="form-control" required>
                </div>

                <div class="form-group">
                    <label class="semi-bold">اللون</label>
                    <input type="text" name="color" class="form-control demo" data-control="hue" value="#000">
                </div>

                <div class="form-group mb-0">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                </div>

            </form>
        </div>

        <div class="row">

            <div class="col-sm-12">

                <table class="table table-bordered mt-30">
                    @foreach($taskStatuses as $taskStatus)
                        <tr>
                            <td style="vertical-align:middle; border-right: 6px solid {{$taskStatus->color}} !important;"><b>{{$taskStatus->name}}</b></td>
                            <td class="text-center">
                                @if($taskStatus->id!=1 and $taskStatus->id!=2)
                                    <a class="btn btn-primary btn-open" style="border-radius: 50%" href="#" title="تعديل"
                                       data-toggle="tooltip" data-target="#edit_task_status{{$taskStatus->id}}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @else
                                    <a class="btn btn-primary disabled" style="border-radius: 50%" href="#"><i class="fa fa-edit"></i></a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($taskStatus->id!=1 and $taskStatus->id!=2)
                                    <a class="btn btn-danger btn-delete1" style="border-radius: 50%" title="حذف" data-toggle="tooltip"
                                        href="{{route("company.prm.settings.delete_task_status", ["company"=>$company->slug, "id"=>$taskStatus->id])}}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @else
                                    <a class="btn btn-danger disabled" style="border-radius: 50%" href="#"><i class="fa fa-times"></i></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 0px 15px;">
                                <form id="edit_task_status{{$taskStatus->id}}" action="{{route('company.prm.settings.edit_task_status', @$company->slug)}}" class="formi form11" method="post">
                                    @csrf

                                    <div class="form-group">
                                        <label class="semi-bold">الإسم</label>
                                        <input type="hidden" name="id" value="{{$taskStatus->id}}">
                                        <input type="text" name="name" class="form-control" value="{{$taskStatus->name}}" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="semi-bold">اللون</label>
                                        <input type="text" name="color"  value="{{$taskStatus->color}}" class="form-control demo" data-control="hue" value="#000">
                                    </div>

                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary" ><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    </div>

                                </form>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td style="vertical-align:middle; border-right: 6px solid deepskyblue !important;"><b>جديدة</b></td>
                        <td class="text-center">
                            <a class="btn btn-primary disabled" style="border-radius: 50%" href="#"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-danger disabled" style="border-radius: 50%" href="#"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                </table>

            </div>

            <div class="col-sm-12 mt-50">
                <small>
                    <b>ملاحظة : </b>
                    <p>رابط البحث في المهام المتأخرة يوميا :  <span>{{route("cronjob")}}</span></p>
                </small>
            </div>

        </div>

    </div>
</div>