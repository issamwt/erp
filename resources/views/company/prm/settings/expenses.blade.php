<div class="row">
    <div class="col-md-12">

        <button type="button" class="btn btn-primary mt-20" id="btn_add_expense_category"><i class="fa fa-plus"></i> <span>إضافة نوع مصاريف جديد</span></button>

        <form action="{{route('company.prm.settings.add_expense_category', @$company->slug)}}" method="post" class="formi form3" id="add_expense_category">
            @csrf

            <div class="form-group">
                <label class="semi-bold">الإسم</label>
                <input type="text" name="name" class="form-control" required>
            </div>

            <div class="form-group mb-0">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
            </div>

        </form>

    </div>
</div>

<div class="row">

    <div class="col-md-12">

        <table class="table table-bordered table-striped dt-responsive last-col grd_view mt-15" id="grd_view">
            @foreach($expenseCategories as $expenseCategory)
                <tr>
                    <td>{{$expenseCategory->name}}</td>
                    <td class="text-center" style="vertical-align: middle;" width="25%">
                            <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#edit_expense_category{{$expenseCategory->id}}"><i class="fa fa-edit"></i></a>
                    </td>
                    <td class="text-center" style="vertical-align: middle;" width="25%">
                        <a href="{{route("company.prm.settings.delete_expense_category", ["company"=>$company->slug, "id"=>$expenseCategory->id])}}" title="حذف" data-toggle="tooltip" class="btn btn-danger btn-delete3"><i class="fa fa-remove"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <form action="{{route('company.prm.settings.update_expense_category', @$company->slug)}}" method="post" class="formi form33" id="edit_expense_category{{$expenseCategory->id}}">
                            @csrf

                            <div class="form-group">
                                <label class="semi-bold">الإسم</label>
                                <input type="text" name="name" value="{{$expenseCategory->name}}" class="form-control" required>
                                <input type="hidden" name="id" value="{{$expenseCategory->id}}">
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                            </div>

                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

</div>
