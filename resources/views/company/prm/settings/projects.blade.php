<div class="row">
    <div class="col-md-12">

        <a href="#" id="btn_addProjectTypeModal" class="btn btn-primary mt-20"><i class="fa fa-plus"></i> <span>إضافة نوع مشروع</span></a>

        <form action="{{route('company.prm.settings.add_projectType', @$company->slug)}}" method="post" class="formi form2" id="addProjectTypeModal">
            @csrf

            <div class="form-group">
                <label>إسم</label>
                <input type="text" name="name" required class="form-control">
            </div>

            <div class="form-group mb-0">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
            </div>

        </form>

        <div class="row">

            <div class="col-sm-12">

                <table class="mt-10 table table-bordered table-striped dt-responsive last-col grd_view" id="grd_view">
                    @foreach($ProjectTypes as $ProjectType)
                        <tr>
                            <td width="50%">{{$ProjectType->name}}</td>
                            <td width="25%" style="vertical-align: middle; text-align: center;">
                                <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#edit_ProjectType{{$ProjectType->id}}"><i class="fa fa-edit"></i></a>
                            </td>
                            <td width="25%" style="vertical-align: middle; text-align: center;">
                                <a href="{{route("company.prm.settings.delete_projectType", ["company"=>$company->slug, "id"=>$ProjectType->id])}}" class="btn btn-danger btn-delete2"  data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 0px 10px">
                                <form action="{{route('company.prm.settings.update_ProjectType', @$company->slug)}}" method="post" class="formi form22" id="edit_ProjectType{{$ProjectType->id}}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$ProjectType->id}}">

                                    <div class="form-group">
                                        <label>إسم</label>
                                        <input type="text" name="name" value="{{$ProjectType->name}}" required class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>تعديل</span></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>

        </div>

    </div>
</div>