{{--
<form action="{{route("company.prm.savenotif", $company->slug)}}" method="post">
    @csrf
    <br><br>
    <input type="hidden" name="company_id" value="{{$company->id}}">

    <input type="text" name="name" placeholder="name" required class="form-control">
    <br>

    <input type="text" name="title" placeholder="title" required class="form-control">
    <br>

    <div class="checkbox">
        <label><input type="checkbox" name="app" value="1" checked required>على البرنامج</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="email" value="1" checked required>عبر ال Eamil</label>
    </div>
    <div class="checkbox disabled">
        <label><input type="checkbox" name="sms" value="1" checked required>عبر ال Sms</label>
    </div>
    <br>

    <textarea name="message" placeholder="message" required class="form-control"></textarea>
    <br>

    <button type="submit">أضف</button>
    <br><br><br>
</form>
--}}

<br><br>
<div class="row mb-30">
    <div class="col-sm-2">نوع الإشعار</div>
    <div class="col-sm-2">على البرنامج</div>
    <div class="col-sm-2">عبر البريد الإلكتروني</div>
    <div class="col-sm-2">عبر الرسائل القصيرة</div>
    <div class="col-sm-4">الرسالة</div>
</div>

<form action="{{route("company.prm.savenotif", $company->slug)}}" method="post" class="mb-10 form7">
    @csrf

@foreach($notifs as $notif)

    <div class="row mb-10">

        <div class="col-sm-2">{{$notif->title}}{{--<br>{{$notif->name}}--}}</div>
        <div class="col-sm-2 text-center">
            <div class="checkbox">
                <label><input type="checkbox" name="app_{{$notif->id}}" value="1"
                              @if($notif->app==1) checked @endif></label>
            </div>
        </div>
        <div class="col-sm-2 text-center">
            <div class="checkbox">
                <label><input type="checkbox" name="email_{{$notif->id}}" value="1"
                              @if($notif->email==1) checked @endif></label>
            </div>
        </div>
        <div class="col-sm-2 text-center">
            <div class="checkbox">
                <label><input type="checkbox" name="sms_{{$notif->id}}" value="1"
                              @if($notif->sms==1) checked @endif></label>
            </div>
        </div>
        <div class="text-justify col-sm-4">
            <textarea name="message_{{$notif->id}}" class="form-control" maxlength="250" required>{{$notif->message}}</textarea>
        </div>
    </div>
@endforeach


<center>
    <button type="submit" class="btn btn-primary mt-20 mb-30" style="width: 120px"><i
                class="fa fa-save"></i> <span>حفظ</span></button>
</center>

</form>