<div class="row">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12">
                <h4 class="mt-15">الضرائب : </h4>
                <button type="button" class="btn btn-primary" data-target="#add_tax" id="btn_add_tax"><i class="fa fa-plus"></i> <span>إضافة</span></button>
            </div>
        </div>

        <form action="{{route('company.prm.settings.add_tax', @$company->slug)}}" method="post" class="formi form4" id="add_tax">
            @csrf

            <div class="form-group">
                <label class="semi-bold">الإسم</label>
                <input type="text" name="name" class="form-control" required>
            </div>

            <div class="form-group">
                <label class="semi-bold">النسبة المئوية (٪)</label>
                <input type="number" step="0.1"  name="percentage" class="form-control" required>
            </div>

            <div class="form-group mb-0">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
            </div>

        </form>

    </div>

</div>

<div class="row">

    <div class="col-md-12 mt-15">

        <table class="table table-bordered table-striped">
            @foreach($taxes as $tax)
                <tr class="odd gradeX">
                    <td width="55%" style="vertical-align: middle">{{$tax->name}}</td>
                    <td width="15%" style="vertical-align: middle">{{$tax->percentage}} %</td>
                    <td width="15%" class="text-center">
                        <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#edit_tax{{$tax->id}}"><i class="fa fa-edit"></i></a>
                    </td>
                    <td width="15%" class="text-center">
                            <a href="{{route("company.prm.settings.delete_tax", ["company"=>$company->slug, "id"=>$tax->id])}}" class="btn btn-danger btn-delete4" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 0px 10px">
                        <form action="{{route('company.prm.settings.update_tax', @$company->slug)}}" method="post" class="formi form44" id="edit_tax{{$tax->id}}">
                            @csrf

                            <div class="form-group">
                                <label class="semi-bold">الإسم</label>
                                <input type="text" name="name" value="{{$tax->name}}" class="form-control" required>
                                <input type="hidden" name="id" value="{{$tax->id}}">
                            </div>

                            <div class="form-group">
                                <label class="semi-bold">النسبة المئوية (٪)</label>
                                <input type="number" step="0.1" name="percentage" value="{{$tax->percentage}}" class="form-control" required>
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                            </div>

                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>

    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12">
                <h4 class="mt-15">النسب : </h4>
                <button type="button" class="btn btn-primary" data-target="#add_percentage" id="btn_add_percentage"><i class="fa fa-plus"></i> <span>إضافة</span></button>
            </div>
        </div>

        <form action="{{route('company.prm.settings.add_percentage', @$company->slug)}}" method="post" class="formi form444" id="add_percentage">
            @csrf

            <div class="form-group">
                <label class="semi-bold">النسبة</label>
                <input type="number" name="percentage" step="0.1" min="0" max="100" class="form-control" required>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">حفظ</button>
            </div>

        </form>

    </div>
</div>

<div class="row">

    <div class="col-md-12 mt-15">

        <table class="table table-bordered table-striped dt-responsive last-col grd_view">
            @foreach($percentages as $percentage)
                <tr class="odd gradeX">
                    <td width="50%" style="text-align: center; vertical-align: middle;">{{$percentage->percentage}} %</td>
                    <td width="25%" style="text-align: center; vertical-align: middle;">
                        <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#edit_percentage{{$percentage->id}}"><i class="fa fa-edit"></i></a>
                    </td>
                    <td width="25%" style="text-align: center; vertical-align: middle;">
                            <a href="{{route("company.prm.settings.delete_percentage", ["company"=>$company->slug, "id"=>$percentage->id])}}"  class="btn btn-danger btn-delete444" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 0px 10px">
                        <form action="{{route('company.prm.settings.update_percentage', @$company->slug)}}" method="post" class="formi form4444" id="edit_percentage{{$percentage->id}}">
                            @csrf

                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$percentage->id}}">
                            </div>

                            <div class="form-group">
                                <label class="semi-bold">النسبة</label>
                                <input type="number" step="0.1" min="0" max="100" name="percentage" value="{{$percentage->percentage}}" class="form-control" required>
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                            </div>

                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

</div>




{{--



@foreach($percentages as $percentage)

    <div id="edit_percentage{{$percentage->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">تعديل النسبة</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('edit_percentage')}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label class="semi-bold">الإسم</label>
                            <input type="hidden" name="id" value="{{$percentage->id}}">
                        </div>

                        <div class="form-group">
                            <label class="semi-bold">النسبة</label>
                            <input type="number" step="0.1" min="0" max="100" name="percentage" value="{{$percentage->percentage}}" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>

@endforeach

--}}