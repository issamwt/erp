<div class="row">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12 mt-20">

                <button type="button" class="btn btn-primary" id="btn_add_payment_method"><i class="fa fa-plus"></i> <span>إضافة طريقة دفع جديدة</span></button>

                <form action="{{route('company.prm.settings.add_payment_method', @$company->slug)}}" method="post" class="formi form6 mt-20" id="add_payment_method">
                    @csrf

                    <div class="form-group">
                        <label class="semi-bold">العنوان</label>
                        <input type="text" name="title" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label class="semi-bold">الوصف</label>
                        <textarea rows="3" name="description" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <div class="checkbox check-info">
                            <input id="available_on_invoice_0" type="checkbox" value="1" name="available_on_invoice">
                            <label for="available_on_invoice_0"> <b>متوفر على الفاتورة</b> </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="semi-bold">الحد الأدنى لمبلغ الدفعة</label>
                        <input type="text" value="0" name="minimum_payment_amount" class="form-control">
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                    </div>

                </form>

            </div>
        </div>



        <div class="row">

            <div class="col-md-12 mt-15">

                <table class="table table-bordered table-striped dt-responsive last-col grd_view">
                    <thead>
                    <tr>
                        <th class="min-phone-l">عنوان</th>
                        <th class="min-tablet">وصف</th>
                        <th class="desktop">متوفرة على فاتورة</th>
                        <th class="desktop">الحد الادنى للدفع</th>
                        <th class="desktop" colspan="2">الأوامر</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($paymentMethods as $paymentMethod)
                            <tr class="odd gradeX">
                                <td width="30%">{{$paymentMethod->title}}</td>
                                <td width="30%">{{$paymentMethod->description}}</td>
                                <td class="text-center" width="15%">
                                    @if($paymentMethod->available_on_invoice==1)
                                        <span>نعم</span>
                                    @else
                                        <span>لا</span>
                                    @endif
                                </td>
                                <td class="text-center" width="15%">
                                    @if($paymentMethod->minimum_payment_amount)
                                        {{$paymentMethod->minimum_payment_amount}} {{@$settings['currency']->value}}
                                    @else
                                        <span>-</span>
                                    @endif
                                </td>
                                <td class="center" width="5%" style="vertical-align: middle">
                                    <a href="#" class="btn btn-primary btn-open" data-toggle="tooltip" title="تعديل" data-target="#edit_payment_method{{$paymentMethod->id}}"><i class="fa fa-edit"></i></a>
                                </td>
                                <td class="center" width="5%" style="vertical-align: middle">
                                    <a href="{{route("company.prm.settings.delete_payment_method", ["company"=>$company->slug, "id"=>$paymentMethod->id])}}" class="btn btn-danger btn-delete6" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" style="padding: 0px 10px">
                                    <form action="{{route('company.prm.settings.update_payment_method', @$company->slug)}}" method="post" class="formi form66" id="edit_payment_method{{$paymentMethod->id}}">
                                        @csrf

                                        <div class="form-group">
                                            <label class="semi-bold">العنوان</label>
                                            <input type="text" name="title" class="form-control" value="{{$paymentMethod->title}}" required>
                                            <input type="hidden" name="id" value="{{$paymentMethod->id}}">
                                        </div>

                                        <div class="form-group">
                                            <label class="semi-bold">الوصف</label>
                                            <textarea rows="3" name="description" class="form-control" required>{{$paymentMethod->description}}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <div class="checkbox check-info">
                                                <input id="available_on_invoice_{{$paymentMethod->id}}" type="checkbox" value="1" name="available_on_invoice" @if($paymentMethod->available_on_invoice==1) checked @endif>
                                                <label for="available_on_invoice_{{$paymentMethod->id}}"> <b>متوفر على الفاتورة</b> </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="semi-bold">الحد الأدنى لمبلغ الدفعة</label>
                                            <input type="text" value="{{$paymentMethod->minimum_payment_amount}}" name="minimum_payment_amount" class="form-control">
                                        </div>

                                        <div class="form-group mb-0">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                        </div>

                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </div>
</div>


