@extends("company.layouts.app", ["title"=>"تعديل مشروع"])

@section("content")

    <section class="content-header">
        <h1>نظام إدارة المشاريع</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>تعديل مشروع</strong> <a href="{{route("company.prm.projects", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>العودة للقائمة</span></a></div>
                    <div class="panel-body">

                        <form action="{{route("company.prm.update_project", @$company->slug)}}" method="post">
                            @csrf

                            <input type="hidden" name="id" value="{{$project->id}}">

                            <div class="row mt-15">

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>اسم المشروع <span class="text-red">*</span></label>
                                        <input type="text" name="name" value="{{$project->name}}" required class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>نوع المشروع <span class="text-red">*</span></label>
                                        <select class="select-hide full-width" name="category_project_id" required>
                                            <option value=""></option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if($project->category_project_id==$category->id) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12" id="clients-wrapper">
                                    <div class="form-group" id="clients">
                                        <label>اسم العميل <span class="text-red">*</span></label>
                                        <select name="client_id" required class="select-hide full-width" id="client_select">
                                            <option value=""></option>
                                            @foreach($clients as  $client)
                                                <option value="{{$client->id}}" @if($project->client_id==$client->id) selected @endif>{{$client->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>تكلفة المشروع <span class="text-red">*</span></label>
                                        <input type="text" name="price" value="{{$project->price}}" required class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label>مدير المشروع <span class="text-red">*</span></label>
                                        <select class="select-hide full-width" name="user_id" required>
                                            <option value=""></option>
                                            @foreach($users as  $user)
                                                <option value="{{$user->id}}" @if($project->user_id==$user->id) selected @endif>{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <?php $pusers = []; foreach ($project->users as $u) if($u->id!=$project->user_id)array_push($pusers, $u->id); ?>
                                    <div class="form-group">
                                        <label>أعضاء المشروع </label>
                                        <select name="collaborators[]" multiple class="select2x form-control">
                                            <option value=""></option>
                                            @foreach($users as  $user)
                                                <option value="{{$user->id}}" @if(in_array($user->id, $pusers)) selected @endif>{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>تاريخ بداية المشروع <span class="text-red">*</span></label>
                                        <input type="text" class="form-control datee" name="date_from" value="{{$project->date_from}}" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>تاريخ نهاية المشروع <span class="text-red">*</span></label>
                                        <input type="text" class="form-control datee" name="date_to" value="{{$project->date_to}}" required>
                                    </div>
                                </div>

                                <div class="col-md-4"></div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>نبذة</label>
                                        <textarea class="form-control" name="brief" rows="5">{{$project->brief}}</textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-10"></div>
                                <div class="col-xs-2">
                                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <script>
        $(function () {

            $(document).on("click", "#btn_addCityModal", function (e) {
                e.preventDefault();
                $("#addCityModal").slideToggle();
            });

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on("click", "#city_add", function () {
                var city_name = $("#city_name").val();
                var city_add = $("#city_add");

                if(city_name!=""){
                    $(city_add).prop("disabled", true);
                    $.post( "{{route("company.prm.add_city", $company->slug)}}", { name: city_name, _token:"{{csrf_token()}}" } , function (data) {
                        $("#cities-wrapper").load(window.location + " #cities", function () {
                            $("#city_select").val(data);
                            $('#addCityModal').modal('hide');
                            $("#city_name").val("");
                            reloading();
                        });
                    }).fail(function() {
                        alert( "لقد وقع خطأ ما, الرجاء المحاولة من جديد !" );
                    }).always(function () {
                        $(city_add).prop("disabled", false);
                    });
                }
            });

        });
    </script>

@endsection