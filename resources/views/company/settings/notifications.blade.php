@extends("company.layouts.app", ["title"=>"الإشعارات"])

@section("content")

    <section class="content-header">
        <h1>الإشعارات</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-12">


                        <div class="panel panel-primary" style="font-size: 0.8em">
                            <div class="panel-heading"><strong>أرشيف الإشعارات</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered" id="aaa">
                                    <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>الإشعار</th>
                                        <th>التاريخ</th>
                                        <th>النظام</th>
                                        <th>المرسل إليه</th>
                                        <th>نص الرسالة</th>
                                        <th>الحالة</th>
                                        <th>الرابط</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($notifications as $n)
                                            <tr>
                                                <td style="vertical-align: middle;" width="20px">{{$n->id}}</td>
                                                <td style="vertical-align: middle;">{{@$n->notif->title}}</td>
                                                <td style="vertical-align: middle;">{{$n->created_at->format("Y-m-d")}}</td>
                                                <td style="vertical-align: middle;">
                                                    @if($n->daftar=="settings")
                                                        <span>الإعدادات</span>
                                                    @elseif($n->daftar=="meetings")
                                                        <span>نظام إجتماعاتي</span>
                                                    @elseif($n->daftar=="crm")
                                                        <span>نظام إدارة علاقات العملاء</span>
                                                    @elseif($n->daftar=="prm")
                                                        <span>نظام إدارة المشاريع</span>
                                                    @elseif($n->daftar=="hrm")
                                                        <span>نظام إدارة الموارد البشرية</span>
                                                    @elseif($n->daftar=="cards")
                                                        <span>بطاقة إهداءاتي</span>
                                                    @elseif($n->daftar=="passwords")
                                                        <span>نظام باسوورداتي</span>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    @if($n->user)
                                                        <a href="{{route("company.hrm.employee", [@$company->slug, @$n->user->id])}}">
                                                            @if($n->user->image)
                                                                <img src="{{asset("storage/app/".$n->user->image)}}" style="display:inline-block;width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto;">
                                                            @else
                                                                <img src="{{asset("public/noimage.png")}}" style="display:inline-block;width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto;">
                                                            @endif
                                                            <h5 style="display:inline-block; font-size: 0.8em">{{$n->user->name}}</h5>
                                                        </a>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;">{{@$n->notif->message}}</td>
                                                <td style="vertical-align: middle;" class="text-center">
                                                    @if($n->status==0)
                                                        <label class="label label-warning" style="padding: 3px 10px;">غير مقروء</label>
                                                    @else
                                                        <label class="label label-success" style="padding: 3px 10px;">مقروء</label>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;" class="text-center">
                                                    <a class="btn btn-primary" style="font-size: 0.8em" href="{{route("company.notification", [$company->slug, $n->id])}}">الرابط</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <br><br>
                                <a href="{{route("company.notification_readed", $company->slug)}}" class="btn btn-primary btn-xs">جعل الكل مقروء</a>
                                <br><br>
                            </div>
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <script>
        $(document).ready(function() {
            $('#aaa').DataTable( {
                "order": [[ 0, "desc" ]],
                retrieve: true,
                    language: {
                        "sProcessing": "جارٍ التحميل...",
                            "sLengthMenu": "أظهر  _MENU_  مدخلات ",
                            "sZeroRecords": "لم يعثر على أية سجلات",
                            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخلات",
                            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                            "sInfoPostFix": "",
                            "sSearch": "البحث : ",
                            "sUrl": "",
                            "oPaginate": {
                            "sFirst": "الأول",
                                "sPrevious": "السابق",
                                "sNext": "التالي",
                                "sLast": "الأخير"
                        }
                    }
                    , "pageLength": 10
                    }
            );
        } );
    </script>

@endsection