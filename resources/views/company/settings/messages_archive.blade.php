@extends("company.layouts.app", ["title"=>"إعدادات الشركة"])

@section("content")

    <section class="content-header">
        <h1>أرشيف الرسائل</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-12">

                        {{--
                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أرشيف رسائل البريد الإلكترونية</strong></div>
                            <div class="panel-body" style="display: none">

                                <form action="{{route("company.messages_archive_save", @$company->slug)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{@$company->id}}">

                                    <div class="form-group">
                                        <label>company_id</label>
                                        <input type="text" name="company_id" value="{{@$company->id}}" class="form-control" required="">
                                    </div>

                                    <div class="form-group">
                                        <label>daftar</label>
                                        <select name="daftar" class="form-control" required="">
                                            <option value="meetings">نظام إجتماعاتي</option>
                                            <option value="crm">نظام إدارة علاقات العملاء</option>
                                            <option value="prm">نظام إدارة المشاريع</option>
                                            <option value="hrm">نظام إدارة الموارد البشرية</option>
                                            <option value="cards">بطاقة إهداءاتي</option>
                                            <option value="passwords">نظام باسوورداتي</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>user_id</label>
                                        <select name="user_id" class="form-control" required="">
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>type</label>
                                        <select name="type" class="form-control" required="">
                                            <option value="0">email</option>
                                            <option value="1">sms</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>message</label>
                                        <input type="text" name="message" class="form-control" required="">
                                    </div>

                                    <div class="form-group">
                                        <label>status</label>
                                        <select name="type" class="form-control" required="">
                                            <option value="0">error</option>
                                            <option value="1">success</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>error</label>
                                        <input type="text" name="error" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="width: 120px;">حفظ</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                        --}}


                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أرشيف رسائل البريد الإلكتروني</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>التاريخ</th>
                                        <th>النظام</th>
                                        <th>المرسل إليه</th>
                                        <th>نص الرسالة</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x=1; ?>
                                    @foreach($archives as $archive)
                                        @if($archive->type==0)
                                            <tr>
                                                <td width="30px" class="text-center" style="vertical-align: middle">{{$x++}}</td>
                                                <td style="vertical-align: middle">{{$archive->created_at}}</td>
                                                <td style="vertical-align: middle">
                                                    @if($archive->daftar=="settings")
                                                        <span>الإعدادات</span>
                                                    @elseif($archive->daftar=="meetings")
                                                        <span>نظام إجتماعاتي</span>
                                                    @elseif($archive->daftar=="crm")
                                                        <span>نظام إدارة علاقات العملاء</span>
                                                    @elseif($archive->daftar=="prm")
                                                        <span>نظام إدارة المشاريع</span>
                                                    @elseif($archive->daftar=="hrm")
                                                        <span>نظام إدارة الموارد البشرية</span>
                                                    @elseif($archive->daftar=="cards")
                                                        <span>بطاقة إهداءاتي</span>
                                                    @elseif($archive->daftar=="passwords")
                                                        <span>نظام باسوورداتي</span>
                                                    @elseif($archive->daftar=="emails")
                                                        <span>قسم الرسائل</span>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle">{{$archive->user}}</td>
                                                <td width="50%" style="vertical-align: middle">{!! $archive->message !!}</td>

                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $archives->links() }}
                                <br><br>
                            </div>
                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أرشيف رسائل الجوال</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered dataTable">
                                    <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>التاريخ</th>
                                        <th>النظام</th>
                                        <th>المرسل إليه</th>
                                        <th>نص الرسالة</th>
                                        <th>الحالة</th>
                                        <th>الخطأ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $y=1; ?>
                                    @foreach($archives as $archive)
                                        @if($archive->type==1)
                                            <tr>
                                                <td width="30px" class="text-center" style="vertical-align: middle">{{$y++}}</td>
                                                <td style="vertical-align: middle">{{$archive->created_at}}</td>
                                                <td style="vertical-align: middle">
                                                    @if($archive->daftar=="settings")
                                                        <span>الإعدادات</span>
                                                    @elseif($archive->daftar=="meetings")
                                                        <span>نظام إجتماعاتي</span>
                                                    @elseif($archive->daftar=="crm")
                                                        <span>نظام إدارة علاقات العملاء</span>
                                                    @elseif($archive->daftar=="prm")
                                                        <span>نظام إدارة المشاريع</span>
                                                    @elseif($archive->daftar=="hrm")
                                                        <span>نظام إدارة الموارد البشرية</span>
                                                    @elseif($archive->daftar=="cards")
                                                        <span>بطاقة إهداءاتي</span>
                                                    @elseif($archive->daftar=="passwords")
                                                        <span>نظام باسوورداتي</span>
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle">{{$archive->user}}</td>
                                                <td class="text-center" style="vertical-align: middle">
                                                    <a class="btn btn-primary btn-xs"  data-toggle="modal" data-target="#myModal{{$archive->id}}"><i class="fa fa-eye"></i> <span>نص الرسالة</span></a>
                                                </td>
                                                <td class="text-center" style="vertical-align: middle">
                                                    @if($archive->status==0)
                                                        <span style="color: crimson">خطأ</span>
                                                    @elseif($archive->status==1)
                                                        <span style="color: #61c300">تم الإرسال</span>
                                                    @endif
                                                </td>
                                                <td width="200px" style="vertical-align: middle"><span style="color: crimson">{{$archive->error}}</span></td>

                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="panel panel-primary" style="font-size: 0.8em">
                            <div class="panel-heading"><strong>أرشيف الإشعارات</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered" id="aaa">
                                    <thead>
                                    <tr>
                                        <th>م</th>
                                        <th>الإشعار</th>
                                        <th>التاريخ</th>
                                        <th>النظام</th>
                                        <th>المرسل إليه</th>
                                        <th>نص الرسالة</th>
                                        <th>الحالة</th>
                                        <th>الرابط</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($notificationss as $n)
                                        <tr>
                                            <td style="vertical-align: middle;" width="20px">{{$n->id}}</td>
                                            <td style="vertical-align: middle;">{{@$n->notif->title}}</td>
                                            <td style="vertical-align: middle;">{{$n->created_at->format("Y-m-d")}}</td>
                                            <td style="vertical-align: middle;">
                                                @if($n->daftar=="settings")
                                                    <span>الإعدادات</span>
                                                @elseif($n->daftar=="meetings")
                                                    <span>نظام إجتماعاتي</span>
                                                @elseif($n->daftar=="crm")
                                                    <span>نظام إدارة علاقات العملاء</span>
                                                @elseif($n->daftar=="prm")
                                                    <span>نظام إدارة المشاريع</span>
                                                @elseif($n->daftar=="hrm")
                                                    <span>نظام إدارة الموارد البشرية</span>
                                                @elseif($n->daftar=="cards")
                                                    <span>بطاقة إهداءاتي</span>
                                                @elseif($n->daftar=="passwords")
                                                    <span>نظام باسوورداتي</span>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;">
                                                @if($n->user)
                                                    <a href="{{route("company.hrm.employee", [@$company->slug, @$n->user->id])}}">
                                                        @if($n->user->image)
                                                            <img src="{{asset("storage/app/".$n->user->image)}}" style="display:inline-block;width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto;">
                                                        @else
                                                            <img src="{{asset("public/noimage.png")}}" style="display:inline-block;width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto;">
                                                        @endif
                                                        <h5 style="display:inline-block; font-size: 0.8em">{{$n->user->name}}</h5>
                                                    </a>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;">{{@$n->notif->message}}</td>
                                            <td style="vertical-align: middle;" class="text-center">
                                                @if($n->status==0)
                                                    <label class="label label-warning" style="padding: 3px 10px;">غير مقروء</label>
                                                @else
                                                    <label class="label label-success" style="padding: 3px 10px;">مقروء</label>
                                                @endif
                                            </td>
                                            <td style="vertical-align: middle;" class="text-center">
                                                <a class="btn btn-primary" style="font-size: 0.8em" href="{{route("company.notification", [$company->slug, $n->id])}}">الرابط</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $notificationss->links() }}
                                <br><br>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>

    @foreach($archives as $archive)
        <div id="myModal{{$archive->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">نص الرسالة</h4>
                    </div>
                    <div class="modal-body">
                        <p>{!! $archive->message !!}</p>
                    </div>
                </div>

            </div>
        </div>
    @endforeach

@endsection

@section("scripts")

    <style>
        .panel-heading{
            cursor: pointer;
        }
    </style>

    <script>
        $(function () {
            $(document).on("click", ".panel-heading", function (e) {
                e.preventDefault();
                $(this).parent().find(".panel-body").slideToggle();
            });
        });
    </script>

@endsection