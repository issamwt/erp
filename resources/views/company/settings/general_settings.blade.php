@extends("company.layouts.app", ["title"=>"إعدادات الشركة"])

@section("content")

    <section class="content-header">
        <h1>إعدادات الشركة</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>إعدادات الشركة العامة</strong></div>
                            <div class="panel-body">

                                <form action="{{route("company.general_settings_save1", @$company->slug)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{@$company->id}}">

                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <label>الإسم</label>
                                            <input type="text" name="name" value="{{@$company->name}}" class="form-control" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>الرابط</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="{{@$company->slug}}" name="slug" maxlength="20" required="" placeholder="thelink" style="text-align: left">
                                                <span class="input-group-addon" style="border-left: 1px solid #d2d6de;direction: initial;padding: 6px 30px 6px 30px;">https://ccreation.sa/erp/</span>
                                            </div>
                                            <small>يجب أن يكون أحرف لاتينية فقط</small>
                                        </div>

                                        <div class="form-group">
                                            <label>الشعار</label>
                                            <input type="file" name="image" accept="image/*" class="form-control">
                                            <br>
                                            @if($company->image)
                                                <img src="{{asset("storage/app/".$company->image)}}" style="width: 120px; height: 120px; border: 1px solid #c3c3c3;">
                                            @else
                                                <img src="{{asset("public/uploads/logo2.png")}}" style="width: 120px; height: 120px; border: 1px solid #c3c3c3;">
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" style="width: 120px;">حفظ</button>
                                        </div>

                                    </div>

                                </form>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <strong>إعدادات البريد الإلكتروني</strong>
                            </div>
                            <div class="panel-body">

                                <div id="div11">
                                    <div id="div1">
                                        <form action="{{route('company.save_generals_smtp', $company->slug)}}" method="post" class="form1" enctype="multipart/form-data">
                                            @csrf

                                            <div class="form-group">
                                                <label>أرسل من الإيميل التالي :</label>
                                                <input type="text" class="form-control" name="email_sent_from_address{{$company->id}}" value="{{@$settings["email_sent_from_address".$company->id]->value}}" required>
                                            </div>

                                            <div class="form-group">
                                                <label>إسم المرسل :</label>
                                                <input type="text" class="form-control" name="email_sent_from_name{{$company->id}}" value="{{@$settings["email_sent_from_name".$company->id]->value}}" required>
                                            </div>

                                            <div class="form-group">
                                                <label>مضيف الSMTP : </label>
                                                <input type="text" class="form-control" name="email_smtp_host{{$company->id}}" value="{{@$settings["email_smtp_host".$company->id]->value}}">
                                            </div>

                                            <div class="form-group">
                                                <label>اسم المستخدم SMTP : </label>
                                                <input type="text" class="form-control" name="email_smtp_user{{$company->id}}" value="{{@$settings["email_smtp_user".$company->id]->value}}" autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <label>كلمة المرور SMTP : </label>
                                                <input type="password" class="form-control" name="email_smtp_password{{$company->id}}" value="{{@$settings["email_smtp_password".$company->id]->value}}"  autocomplete="off">
                                            </div>

                                            <div class="form-group">
                                                <label>منفذ ال SMTP : </label>
                                                <input type="text" class="form-control" name="email_smtp_port{{$company->id}}" value="{{@$settings["email_smtp_port".$company->id]->value}}">
                                            </div>

                                            <div class="form-group">
                                                <hr style="margin-top: 30px">
                                            </div>

                                            <div class="form-group">
                                                <label>أرسل رسالة تجربة إلى</label>
                                                <input type="text" class="form-control" name="send_test_mail_to"/>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"> حفظ البيانات</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <strong><span>إعدادات الرسائل القصيرة</span></strong>
                            </div>
                            <div class="panel-body">

                                <div id="div22">
                                    <div id="div2">

                                        <form action="{{route('company.save_yamamah_sms', $company->slug)}}" method="post" class="form2" enctype="multipart/form-data">
                                            @csrf

                                            <div class="row">

                                                <div class="col-sm-12">

                                                    <div class="form-group row">
                                                        <div class="col-sm-6">
                                                            <h5>يمامه</h5>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>الرصيد : {{$smsnum}} رسالة</h5>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>رابط الشركة</label>
                                                        <input type="text" autocomplete="off" class="form-control" name="yamamah_url{{$company->id}}" value="{{@$settings["yamamah_url".$company->id]->value}}" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>إسم المرسل</label>
                                                        <input type="text" autocomplete="off" class="form-control" name="yamamah_sender{{$company->id}}" value="{{@$settings["yamamah_sender".$company->id]->value}}" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>إسم المستخدم</label>
                                                        <input type="text" autocomplete="off" class="form-control" name="yamamah_username{{$company->id}}" value="{{@$settings["yamamah_username".$company->id]->value}}" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>كلمة المرور</label>
                                                        <input type="password" autocomplete="off" class="form-control" name="yamamah_password{{$company->id}}" value="{{@$settings["yamamah_password".$company->id]->value}}" required>
                                                    </div>

                                                </div>

                                                <div class="col-sm-12">

                                                    <div class="form-group">
                                                        <hr style="margin: 10px 0px 20px">
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>أرسل رسالة تجربة إلى :</label>
                                                                <input type="text" class="form-control" name="sms_test">
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary" style="width: 120px"> حفظ البيانات</button>
                                                    </div>

                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>إعدادات الإشعارات الحينية</strong></div>
                            <div class="panel-body">

                                <div id="div33">
                                    <div id="div3">

                                        <form action="{{route("company.save_pusher", @$company->slug)}}" class="form3" method="post" enctype="multipart/form-data">
                                            @csrf

                                            <div class="col-sm-12">

                                                <div class="form-group">
                                                    <p><span>يجب إنشاء حساب في موقع : </span> <a href="https://pusher.com" target="_blank">https://pusher.com</a></p>
                                                </div>

                                                <div class="form-group">
                                                    <label>Key</label>
                                                    <input type="text" name="pusher_auth_key{{@$company->id}}" value="{{@$settings["pusher_auth_key".$company->id]->value}}" class="form-control" required="">
                                                </div>

                                                <div class="form-group">
                                                    <label>Secret</label>
                                                    <input type="text" name="pusher_secret{{@$company->id}}" value="{{@$settings["pusher_secret".$company->id]->value}}" class="form-control" required="">
                                                </div>

                                                <div class="form-group">
                                                    <label>App id</label>
                                                    <input type="text" name="pusher_app_id{{@$company->id}}" value="{{@$settings["pusher_app_id".$company->id]->value}}" class="form-control" required="">
                                                </div>

                                            </div>

                                            <div class="col-sm-12">

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary" style="width: 120px;">حفظ</button>
                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>إعدادات زووم</strong></div>
                            <div class="panel-body">

                                <div id="div44">
                                    <div id="div4">

                                        <form action="{{route("company.save_zoom", @$company->slug)}}" class="form4" method="post" enctype="multipart/form-data">
                                            @csrf

                                            <div class="col-sm-12">

                                                <div class="form-group">
                                                    <p><span>يجب إنشاء حساب في موقع : </span> <a href="https://developer.zoom.us/me/#api" target="_blank">https://developer.zoom.us/me/#api</a></p>
                                                </div>

                                                <div class="form-group">
                                                    <label>Key</label>
                                                    <input type="text" name="zoom_api_key{{@$company->id}}" value="{{@$settings["zoom_api_key".$company->id]->value}}" class="form-control" required="">
                                                </div>

                                                <div class="form-group">
                                                    <label>Secret</label>
                                                    <input type="text" name="zoom_api_secret{{@$company->id}}" value="{{@$settings["zoom_api_secret".$company->id]->value}}" class="form-control" required="">
                                                </div>

                                            </div>

                                            <div class="col-sm-12">

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary" style="width: 120px;">حفظ</button>
                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>أدوار الموظفين</strong></div>
                            <div class="panel-body">

                                <div id="div55">
                                    <div id="div5">

                                        <form action="{{route("company.save_role", $company->slug)}}" method="post" class="form5">
                                            @csrf

                                            <input type="hidden" name="id" value="{{$company->id}}">

                                            <div class="form-group">
                                                <label>الإسم</label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-10"><b>الإسم</b></div>
                                            <div class="col-sm-2"><b>الأوامر</b></div>
                                        </div>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="name" value="الأدمن" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-danger disabled" disabled>حذف</a>
                                            </div>
                                        </div>
                                        <form action="{{route("company.update_role", $company->slug)}}" method="post" class="row form55" style="margin-bottom: 15px">
                                            @csrf

                                            @foreach($roles as $role)

                                                <div class="col-sm-10 mb-15">
                                                    <input type="text" class="form-control" name="name_{{$role->id}}" value="{{$role->name}}" required>
                                                </div>
                                                <div class="col-sm-2 mb-15">
                                                    <a href="{{route("company.delete_role", ["company"=>$company->slug, "id"=>$role->id])}}" class="btn btn-danger btn-delete5">حذف</a>
                                                </div>

                                            @endforeach

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    @include("company.settings.permissions")

                </div>

            </div>
        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <style>
        .panel-heading{
            cursor: pointer;
        }
        .panel-body{
            /*display: none;*/
        }
    </style>

    <script>
        $(function () {

            function bhoechie(){
                $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });
            }

            bhoechie();

            $(document).on("click", ".panel-heading", function (e) {
                e.preventDefault();
                $(this).parent().find(".panel-body").slideToggle();
            });

            $(document).on("submit", ".form1", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div11").html($("#loading-wrapper").html());
                $.post('{{route("company.save_generals_smtp", $company->slug)}}', $(form).serialize(), function () {
                    $("#div11").load(window.location + " #div1", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("submit", ".form2", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div22").html($("#loading-wrapper").html());
                $.post('{{route("company.save_yamamah_sms", $company->slug)}}', $(form).serialize(), function () {
                    $("#div22").load(window.location + " #div2", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("submit", ".form3", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div33").html($("#loading-wrapper").html());
                $.post('{{route("company.save_pusher", $company->slug)}}', $(form).serialize(), function () {
                    $("#div33").load(window.location + " #div3", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("submit", ".form4", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div44").html($("#loading-wrapper").html());
                $.post('{{route("company.save_zoom", $company->slug)}}', $(form).serialize(), function () {
                    $("#div44").load(window.location + " #div4", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            });

            $(document).on("submit", ".form5", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div55").html($("#loading-wrapper").html());
                $.post('{{route("company.save_role", $company->slug)}}', $(form).serialize(), function () {
                    $("#div55").load(window.location + " #div5", function () {
                        goNotif("success", "تم حفظ البيانات !");

                        $("#div66").html($("#loading-wrapper").html());
                        $("#div66").load(window.location + " #div6", function () {
                            $('input:not(.no_ichek)').iCheck({
                                checkboxClass: 'icheckbox_square-blue',
                                radioClass: 'iradio_square-blue',
                                increaseArea: '20%' // optional
                            });
                            bhoechie();
                        });
                    });
                });
            });

            $(document).on("click", ".btn-delete5", function (e) {
                e.preventDefault();
                var href = $(this).attr("href");
                if(confirm('هل انت متأكد ؟')){
                    $(this).closest("#div55").html($("#loading-wrapper").html());
                    $.get(href, function () {
                        $("#div55").load(window.location + " #div5", function () {
                            goNotif("success", "تم حفظ البيانات !");
                            $("#div66").html($("#loading-wrapper").html());
                            $("#div66").load(window.location + " #div6", function () {
                                $('input:not(.no_ichek)').iCheck({
                                    checkboxClass: 'icheckbox_square-blue',
                                    radioClass: 'iradio_square-blue',
                                    increaseArea: '20%' // optional
                                });
                                bhoechie();
                            });
                        });
                    });
                }
            });

            $(document).on("submit", ".form55", function (e) {
                e.preventDefault();
                var form = $(this);
                $(this).closest("#div55").html($("#loading-wrapper").html());
                $.post('{{route("company.update_role", $company->slug)}}', $(form).serialize(), function () {
                    $("#div55").load(window.location + " #div5", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                    $("#div66").html($("#loading-wrapper").html());
                    $("#div66").load(window.location + " #div6", function () {
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                        bhoechie();
                    });
                });
            });

            $(document).on("submit", ".form1x", function (e) {
                e.preventDefault();
                var form = $(this);
                var id = $(this).data("id");
                $("#divxx"+id).html($("#loading-wrapper").html());
                $.post('{{route("company.save_permissions", $company->slug)}}', $(form).serialize(), function () {
                    $("#divxx"+id).load(window.location + " #divx"+id, function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });

            $(document).on("submit", ".form1y", function (e) {
                e.preventDefault();
                var form = $(this);
                var id = $(this).data("id");
                $("#divyy"+id).html($("#loading-wrapper").html());
                $.post('{{route("company.meetings.save_permissions", $company->slug)}}', $(form).serialize(), function () {
                    $("#divyy"+id).load(window.location + " #divy"+id, function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });

            $(document).on("submit", ".form1z", function (e) {
                e.preventDefault();
                var form = $(this);
                var id = $(this).data("id");
                $("#divzz"+id).html($("#loading-wrapper").html());
                $.post('{{route("company.prm.save_permissions", $company->slug)}}', $(form).serialize(), function () {
                    $("#divzz"+id).load(window.location + " #divz"+id, function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });

            $(document).on("submit", ".form1v", function (e) {
                e.preventDefault();
                var form = $(this);
                var id = $(this).data("id");
                $("#divvv"+id).html($("#loading-wrapper").html());
                $.post('{{route("company.passwords.save_permissions", $company->slug)}}', $(form).serialize(), function () {
                    $("#divvv"+id).load(window.location + " #divv"+id, function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });

            $(document).on("submit", ".form1w", function (e) {
                e.preventDefault();
                var form = $(this);
                var id = $(this).data("id");
                $("#divww"+id).html($("#loading-wrapper").html());
                $.post('{{route("company.hrm.save_permissions", $company->slug)}}', $(form).serialize(), function () {
                    $("#divww"+id).load(window.location + " #divw"+id, function () {
                        goNotif("success", "تم حفظ البيانات !");
                        $('input:not(.no_ichek)').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' // optional
                        });
                    });
                });
            });

        });
    </script>

    <style>

        div.bhoechie-tab-container{
            z-index: 10;
            background-color: #ffffff;
            padding: 0 !important;
            border:1px solid #ddd;
            -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
            box-shadow: 0 6px 12px rgba(0,0,0,.175);
            -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
            background-clip: padding-box;
            opacity: 0.97;
            filter: alpha(opacity=97);
        }

        div.bhoechie-tab-menu{
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group{
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a{
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a .glyphicon,
        div.bhoechie-tab-menu div.list-group>a .fa {
            color: #5A55A3;
        }

        div.bhoechie-tab-menu div.list-group>a:first-child{
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group>a:last-child{
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group>a.active,
        div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group>a.active .fa{
            background-color: #337ab7;
            border-radius: 0px;
            color: #ffffff;
        }

        div.bhoechie-tab-menu div.list-group>a.active:after{
            content: '';
            position: absolute;
            right: 100%;
            top: 50%;
            margin-top: -13px;
            border-left: 0;
            border-bottom: 13px solid transparent;
            border-top: 13px solid transparent;
            border-right: 10px solid #337ab7;
        }

        div.bhoechie-tab-content{
            background-color: #ffffff;
            /* border: 1px solid #eeeeee; */
            padding-left: 20px;
            padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active){
            display: none;
        }

        .bhoechie-tab{
            padding-top: 10px;
            padding-bottom: 120px;
            min-height: 400px;
            border-right: 1px solid #ddd;
        }

        .icheckbox_square-blue{
            margin-left: 10px;
        }

    </style>

@endsection