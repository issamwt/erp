<div class="col-sm-12">

    <div class="panel panel-primary">
        <div class="panel-heading"><strong>الصلاحيات </strong></div>
        <div class="panel-body" style="display: block">

            <div id="div66">
                <div id="div6">

                    <div class="col-xs-12 bhoechie-tab-container">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                            <div class="list-group">
                                @foreach($roles as $role)
                                    <a href="#" class="list-group-item @if($loop->iteration==1) active @endif text-center">{{$role->name}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                            @foreach($roles as $role)
                                <div class="bhoechie-tab-content @if($loop->iteration==1) active @endif">

                                    <center class="mb-50 mt-10">
                                        <h4 style="margin-top: 0;color:#337ab7; margin-bottom: 0px;">صلاحيات "{{$role->name}}"</h4>
                                    </center>

                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#aaa{{$role->id}}">الصلاحيات العامة</a></li>
                                        @if($company->meetings)
                                            <li><a data-toggle="tab" href="#bbb{{$role->id}}">إجتماعاتي</a></li>
                                        @endif
                                        @if($company->prm)
                                            <li><a data-toggle="tab" href="#ccc{{$role->id}}">إدارة المشاريع</a></li>
                                        @endif
                                        @if($company->passwords)
                                            <li><a data-toggle="tab" href="#ddd{{$role->id}}">باسوورداتي</a></li>
                                        @endif
                                        @if($company->hrm)
                                            <li><a data-toggle="tab" href="#eee{{$role->id}}">إدارة الموارد البشرية</a></li>
                                        @endif
                                    </ul>

                                    <div class="tab-content">

                                        <div id="aaa{{$role->id}}" class="tab-pane fade in active" style="padding-top: 30px;">

                                            <div id="divxx{{$role->id}}">
                                                <div id="divx{{$role->id}}">

                                                <?php $permissions = unserialize($role->permissions_general); $permissions = is_array($permissions)?$permissions:[]; ?>
                                                <form action="{{route("company.save_permissions", @$company->slug)}}" method="post" class="form1x" data-id="{{$role->id}}">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$role->id}}">

                                                        <div class="row">

                                                            <div class="col-sm-6">

                                                                <h4 class="mt-0 mb-20">الموظفون</h4>

                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_user" @if(in_array("add_user", $permissions)) checked @endif><span>إضافة موظف</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_user" @if(in_array("update_user", $permissions)) checked @endif><span>تعديل موظف</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_user" @if(in_array("delete_user", $permissions)) checked @endif><span>حذف موظف</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="show_user" @if(in_array("show_user", $permissions)) checked @endif><span>عرض موظف</span></label></div>

                                                            </div>

                                                            <div class="col-sm-6">

                                                                <h4 class="mt-0 mb-20">العملاء</h4>

                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_client" @if(in_array("add_client", $permissions)) checked @endif><span>إضافة عميل</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_client" @if(in_array("update_client", $permissions)) checked @endif><span>تعديل عميل</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_client" @if(in_array("delete_client", $permissions)) checked @endif><span>حذف عميل</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="show_client" @if(in_array("show_client", $permissions)) checked @endif><span>عرض عميل</span></label></div>

                                                            </div>

                                                            <div class="col-sm-6">

                                                                <h4 class="mt-20 mb-20">الأنظمة</h4>

                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="meetings_system" @if(in_array("meetings_system", $permissions)) checked @endif><span> إجتماعاتي</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="prm_system" @if(in_array("prm_system", $permissions)) checked @endif><span> إدارة المشاريع</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="passwords_system" @if(in_array("passwords_system", $permissions)) checked @endif><span> باسوورداتي</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="hrm_system" @if(in_array("hrm_system", $permissions)) checked @endif><span> الموارد البشرية</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="calendar_system" @if(in_array("calendar_system", $permissions)) checked @endif><span> التقويم (إضافة/حذف أحداث)</span></label></div>

                                                            </div>

                                                            <div class="col-sm-6">

                                                                <h4 class="mt-20 mb-20">الإعدادات</h4>

                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="general_settings" @if(in_array("general_settings", $permissions)) checked @endif><span>إعدادات الشركة</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="meetings_settings" @if(in_array("meetings_settings", $permissions)) checked @endif><span>إعدادات إجتماعاتي</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="prm_settings" @if(in_array("prm_settings", $permissions)) checked @endif><span>إعدادات إدارة المشاريع</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="passwords_settings" @if(in_array("passwords_settings", $permissions)) checked @endif><span>إعدادات باسوورداتي</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="hrm_settings" @if(in_array("hrm_settings", $permissions)) checked @endif><span>إعدادات الموارد البشرية</span></label></div>
                                                                <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="messages_archive" @if(in_array("messages_archive", $permissions)) checked @endif><span>أرشيف الرسائل</span></label></div>

                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <center class="mt-50">
                                                                <button type="submit" class="btn btn-primary" style="width: 150px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </center>
                                                        </div>

                                                </form>

                                                </div>
                                            </div>

                                        </div>

                                        <div id="bbb{{$role->id}}" class="tab-pane fade" style="padding-top: 30px;">

                                            <div id="divyy{{$role->id}}">
                                                <div id="divy{{$role->id}}">

                                                    <?php $permissions = unserialize($role->permissions_meetings); $permissions = is_array($permissions)?$permissions:[]; ?>
                                                    <form action="{{route("company.meetings.save_permissions", @$company->slug)}}" class="form1y" data-id="{{$role->id}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$role->id}}">

                                                            <div class="row">

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">الإجتماع</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_meeting" @if(in_array("add_meeting", $permissions)) checked @endif><span>إضافة إجتماع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_meeting" @if(in_array("update_meeting", $permissions)) checked @endif><span>تعديل إجتماع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_meeting" @if(in_array("delete_meeting", $permissions)) checked @endif><span>حذف إجتماع</span></label></div>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="accept_meeting" @if(in_array("accept_meeting", $permissions)) checked @endif><span>بدء الإجتماع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="cancel_meeting" @if(in_array("cancel_meeting", $permissions)) checked @endif><span>إلغاء الإجتماع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delay_meeting" @if(in_array("delay_meeting", $permissions)) checked @endif><span>تأجيل الإجتماع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="notify_meeting" @if(in_array("notify_meeting", $permissions)) checked @endif><span>تذكير الحضور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="finish_meeting" @if(in_array("finish_meeting", $permissions)) checked @endif><span>إنهاء الإجتماع</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">المحاور</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_axis" @if(in_array("add_axis", $permissions)) checked @endif><span>إضافة محور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_axis" @if(in_array("update_axis", $permissions)) checked @endif><span>تعديل محور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_axis" @if(in_array("delete_axis", $permissions)) checked @endif><span>حذف محور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="order_axis" @if(in_array("order_axis", $permissions)) checked @endif><span>ترتيب محور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="start_finish_axis" @if(in_array("start_finish_axis", $permissions)) checked @endif><span>تشغيل/إنهاء محور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_comment_axis" @if(in_array("add_comment_axis", $permissions)) checked @endif><span>إضافة تعليق في محور</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_file_axis" @if(in_array("add_file_axis", $permissions)) checked @endif><span>إضافة ملف في محور</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">المهام</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_task" @if(in_array("add_task", $permissions)) checked @endif><span>إضافة مهمة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_task" @if(in_array("update_task", $permissions)) checked @endif><span>تعديل مهمة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_task" @if(in_array("delete_task", $permissions)) checked @endif><span>حذف مهمة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="order_task" @if(in_array("order_task", $permissions)) checked @endif><span>إنهاء مهمة</span></label></div>

                                                                </div>

                                                            </div>

                                                            <div class="row mt-30"></div>

                                                            <div class="row">

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">الملفات</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_file" @if(in_array("add_file", $permissions)) checked @endif><span>إضافة ملف</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_file" @if(in_array("delete_file", $permissions)) checked @endif><span>حذف ملف</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">التصويت</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_vote" @if(in_array("add_vote", $permissions)) checked @endif><span>إضافة تصويت</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_vote" @if(in_array("update_vote", $permissions)) checked @endif><span>تعديل تصويت</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_vote" @if(in_array("delete_vote", $permissions)) checked @endif><span>حذف تصويت</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">التقييم</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_evaluation" @if(in_array("add_evaluation", $permissions)) checked @endif><span>تقييم الإجتماع</span></label></div>

                                                                </div>

                                                            </div>

                                                            <div class="row">
                                                                <center class="mt-50">
                                                                    <button type="submit" class="btn btn-primary" style="width: 150px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                </center>
                                                            </div>

                                                        </form>

                                                </div>
                                            </div>

                                        </div>

                                        <div id="ccc{{$role->id}}" class="tab-pane fade" style="padding-top: 30px;">

                                            <div id="divzz{{$role->id}}">
                                                <div id="divz{{$role->id}}">

                                                    <?php $permissions = unserialize($role->permissions_prm); $permissions = is_array($permissions)?$permissions:[]; ?>
                                                    <form action="{{route("company.prm.save_permissions", @$company->slug)}}" class="form1z" data-id="{{$role->id}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$role->id}}">

                                                            <div class="row">

                                                                <div class="col-sm-4 mt-20">

                                                                    <h4 class="mt-0 mb-20">المشروع</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_project" @if(in_array("add_project", $permissions)) checked @endif><span>إضافة مشروع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_project" @if(in_array("update_project", $permissions)) checked @endif><span>تعديل مشروع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_project" @if(in_array("delete_project", $permissions)) checked @endif><span>حذف مشروع</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4 mt-20">

                                                                    <h4 class="mt-0 mb-20">المهمة</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_task" @if(in_array("add_task", $permissions)) checked @endif><span>إضافة مهمة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_task" @if(in_array("update_task", $permissions)) checked @endif><span>تعديل مهمة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_task" @if(in_array("delete_task", $permissions)) checked @endif><span>حذف مهمة</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4 mt-20">

                                                                    <h4 class="mt-0 mb-20">المراحل</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_milestone" @if(in_array("add_milestone", $permissions)) checked @endif><span>إضافة مرحلة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="update_milestone" @if(in_array("update_milestone", $permissions)) checked @endif><span>تعديل مرحلة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_milestone" @if(in_array("delete_milestone", $permissions)) checked @endif><span>حذف مرحلة</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4 mt-20">

                                                                    <h4 class="mt-0 mb-20">النقاشات</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_discussion" @if(in_array("add_discussion", $permissions)) checked @endif><span>إضافة نقاش</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4 mt-20">

                                                                    <h4 class="mt-0 mb-20">الملفات</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_filetype" @if(in_array("add_filetype", $permissions)) checked @endif><span>إضافة مجلد</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="add_file" @if(in_array("add_file", $permissions)) checked @endif><span>إضافة ملف</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="delete_file" @if(in_array("delete_file", $permissions)) checked @endif><span>حذف ملف</span></label></div>

                                                                </div>

                                                                <div class="col-sm-4 mt-20">

                                                                    <h4 class="mt-0 mb-20">التبويبات</h4>

                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab1" @if(in_array("tab1", $permissions)) checked @endif><span>نظرة عامة</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab2" @if(in_array("tab2", $permissions)) checked @endif><span>مهام المشروع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab3" @if(in_array("tab3", $permissions)) checked @endif><span>لوحة المهام</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab4" @if(in_array("tab4", $permissions)) checked @endif><span>مخطط المهام</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab5" @if(in_array("tab5", $permissions)) checked @endif><span>المراحل</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab6" @if(in_array("tab6", $permissions)) checked @endif><span>نقاشات المشروع</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab7" @if(in_array("tab7", $permissions)) checked @endif><span>الملفات</span></label></div>
                                                                    <div class="checkbox icheck"><label><input class="form-check-input" type="checkbox" name="tab8" @if(in_array("tab8", $permissions)) checked @endif><span>الجدول الزمنى</span></label></div>

                                                                </div>

                                                            </div>

                                                            <div class="row mt-30"></div>

                                                            <div class="row">
                                                                <center class="mt-50">
                                                                    <button type="submit" class="btn btn-primary" style="width: 150px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                </center>
                                                            </div>

                                                        </form>

                                                </div>
                                            </div>
                                        </div>

                                        <div id="ddd{{$role->id}}" class="tab-pane fade" style="padding-top: 30px;">

                                            <div id="divvv{{$role->id}}">
                                                <div id="divv{{$role->id}}">

                                                    <?php $permissions = unserialize($role->permissions_passwords); $permissions = is_array($permissions) ? $permissions : []; ?>
                                                    <form action="{{route("company.passwords.save_permissions", @$company->slug)}}"  class="form1v" data-id="{{$role->id}}"
                                                              method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$role->id}}">

                                                            <div class="row">

                                                                <div class="col-sm-4">

                                                                    <h4 class="mt-0 mb-20">المواقع</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="show_website"
                                                                                    @if(in_array("show_website", $permissions)) checked @endif><span>عرض المواقع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_website"
                                                                                    @if(in_array("add_website", $permissions)) checked @endif><span>إضافة موقع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_website"
                                                                                    @if(in_array("update_website", $permissions)) checked @endif><span>تعديل موقع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_website"
                                                                                    @if(in_array("delete_website", $permissions)) checked @endif><span>حذف موقع</span></label>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row">
                                                                <center class="mt-50">
                                                                    <button type="submit" class="btn btn-primary"
                                                                            style="width: 150px"><i class="fa fa-save"></i>
                                                                        <span>حفظ</span></button>
                                                                </center>
                                                            </div>

                                                        </form>

                                                </div>
                                            </div>

                                        </div>

                                        <div id="eee{{$role->id}}" class="tab-pane fade" style="padding-top: 30px;">

                                            <div id="divww{{$role->id}}">
                                                <div id="divw{{$role->id}}">

                                                    <?php $permissions = unserialize($role->permissions_hrm); $permissions = is_array($permissions) ? $permissions : []; ?>
                                                    <form action="{{route("company.hrm.save_permissions", @$company->slug)}}"  class="form1w" data-id="{{$role->id}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$role->id}}">

                                                            <div class="row">

                                                                <div class="col-sm-6">

                                                                    <h4 class="mt-0 mb-20">التأمينات و المعلومات
                                                                        المالية</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="insurance_n_finance_infos"
                                                                                    @if(in_array("insurance_n_finance_infos", $permissions)) checked @endif><span> صفحة التأمينات و المعلومات المالية</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_allowance_cat"
                                                                                    @if(in_array("add_allowance_cat", $permissions)) checked @endif><span> إضافة صنف بدل</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_allowance_cat"
                                                                                    @if(in_array("update_allowance_cat", $permissions)) checked @endif><span> تعديل بدل</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_allowance_cat"
                                                                                    @if(in_array("delete_allowance_cat", $permissions)) checked @endif><span> حذف بدل</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_deduction_cat"
                                                                                    @if(in_array("add_deduction_cat", $permissions)) checked @endif><span> إضافة صنف إقتطاع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_deduction_cat"
                                                                                    @if(in_array("update_deduction_cat", $permissions)) checked @endif><span> تعديل صنف إقتطاع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_deduction_cat"
                                                                                    @if(in_array("delete_deduction_cat", $permissions)) checked @endif><span> حذف صنف إقتطاع</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_medical"
                                                                                    @if(in_array("add_medical", $permissions)) checked @endif><span> إضافة التأمين الطبي</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_medical"
                                                                                    @if(in_array("update_medical", $permissions)) checked @endif><span> تعديل التأمين الطبي</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_medical"
                                                                                    @if(in_array("delete_medical", $permissions)) checked @endif><span> حذف التأمين الطبي</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_social"
                                                                                    @if(in_array("add_social", $permissions)) checked @endif><span> إضافة الضمان الإجتماعي</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_social"
                                                                                    @if(in_array("update_social", $permissions)) checked @endif><span> تعديل الضمان الإجتماعي</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_social"
                                                                                    @if(in_array("delete_social", $permissions)) checked @endif><span> حذف الضمان الإجتماعي</span></label>
                                                                    </div>

                                                                </div>

                                                                <div class="col-sm-6">

                                                                    <h4 class="mt-0 mb-20">إدارة الموظفين</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="employees"
                                                                                    @if(in_array("employees", $permissions)) checked @endif><span> صفحة إدارة الموظفين</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_employee"
                                                                                    @if(in_array("add_employee", $permissions)) checked @endif><span> إضافة الموظفين</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="show_employee"
                                                                                    @if(in_array("show_employee", $permissions)) checked @endif><span> عرض الموظفين</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_employee"
                                                                                    @if(in_array("update_employee", $permissions)) checked @endif><span> تعديل الموظفين</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_employee"
                                                                                    @if(in_array("delete_employee", $permissions)) checked @endif><span> حذف الموظفين</span></label>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row mt-30"></div>

                                                            <div class="row">

                                                                <div class="col-sm-6">

                                                                    <h4 class="mt-0 mb-20">الشؤون المالية</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="finances"
                                                                                    @if(in_array("finances", $permissions)) checked @endif><span> صفحة الشؤون المالية</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_allowance"
                                                                                    @if(in_array("tab_allowance", $permissions)) checked @endif><span> تبويب البدلات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_allowance"
                                                                                    @if(in_array("add_allowance", $permissions)) checked @endif><span> إضافة بدل</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_allowance"
                                                                                    @if(in_array("update_allowance", $permissions)) checked @endif><span> تعديل بدل</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_allowance"
                                                                                    @if(in_array("delete_allowance", $permissions)) checked @endif><span> حذف بدل</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_deduction"
                                                                                    @if(in_array("tab_deduction", $permissions)) checked @endif><span> تبويب الإقتطاعات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_deduction"
                                                                                    @if(in_array("add_deduction", $permissions)) checked @endif><span> إضافة إقتطاع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_deduction"
                                                                                    @if(in_array("update_deduction", $permissions)) checked @endif><span> تعديل إقتطاع</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_deduction"
                                                                                    @if(in_array("delete_deduction", $permissions)) checked @endif><span> حذف إقتطاع</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_advance"
                                                                                    @if(in_array("tab_advance", $permissions)) checked @endif><span> تبويب السلف</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_advance"
                                                                                    @if(in_array("add_advance", $permissions)) checked @endif><span> إضافة سلف</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_advance"
                                                                                    @if(in_array("update_advance", $permissions)) checked @endif><span> تعديل سلف</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_advance"
                                                                                    @if(in_array("delete_advance", $permissions)) checked @endif><span> حذف سلف</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_custody"
                                                                                    @if(in_array("tab_custody", $permissions)) checked @endif><span> تبويب العهدات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_custody"
                                                                                    @if(in_array("add_custody", $permissions)) checked @endif><span> إضافة عهدة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_custody"
                                                                                    @if(in_array("update_custody", $permissions)) checked @endif><span> تعديل العهدة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_custody"
                                                                                    @if(in_array("delete_custody", $permissions)) checked @endif><span> حذف العهدة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delivered_custody"
                                                                                    @if(in_array("delivered_custody", $permissions)) checked @endif><span> تأكيد إستلام العهدة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="print_custody"
                                                                                    @if(in_array("print_custody", $permissions)) checked @endif><span> طباعة العهدة</span></label>
                                                                    </div>

                                                                </div>


                                                                <div class="col-sm-6">

                                                                    <h4 class="mt-0 mb-20">الشؤون الإدارية</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="adminisitrative_affairs"
                                                                                    @if(in_array("adminisitrative_affairs", $permissions)) checked @endif><span> صفحة الشؤون الإدارية</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_vacation"
                                                                                    @if(in_array("tab_vacation", $permissions)) checked @endif><span> تبويب الإجازات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_vacation"
                                                                                    @if(in_array("add_vacation", $permissions)) checked @endif><span> إضافة إجازة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_vacation"
                                                                                    @if(in_array("update_vacation", $permissions)) checked @endif><span> تعديل الإجازة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_vacation"
                                                                                    @if(in_array("delete_vacation", $permissions)) checked @endif><span> حذف الإجازة</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_training"
                                                                                    @if(in_array("tab_training", $permissions)) checked @endif><span> تبويب الدورات التدريبية</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_training"
                                                                                    @if(in_array("add_training", $permissions)) checked @endif><span> إضافة دورة تدريبية</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_training"
                                                                                    @if(in_array("update_training", $permissions)) checked @endif><span> تعديل الدورة التدريبية</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_training"
                                                                                    @if(in_array("delete_training", $permissions)) checked @endif><span> حذف الدورة التدريبية</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="tab_mandate"
                                                                                    @if(in_array("tab_mandate", $permissions)) checked @endif><span> تبويب الإنتدابات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_mandate"
                                                                                    @if(in_array("add_mandate", $permissions)) checked @endif><span> إضافة إنتداب</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="update_mandate"
                                                                                    @if(in_array("update_mandate", $permissions)) checked @endif><span> تعديل الإنتداب</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_mandate"
                                                                                    @if(in_array("delete_mandate", $permissions)) checked @endif><span> حذف الإنتداب</span></label>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row">

                                                                <div class="col-sm-6">

                                                                    <h4 class="mt-0 mb-20">الإدارات</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="evaluations"
                                                                                    @if(in_array("evaluations", $permissions)) checked @endif><span> إدارة التقييمات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_evaluation"
                                                                                    @if(in_array("evaluations", $permissions)) checked @endif><span> إضافة تقييم</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_evaluation"
                                                                                    @if(in_array("evaluations", $permissions)) checked @endif><span> حذف تقييم</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="attendances"
                                                                                    @if(in_array("attendances", $permissions)) checked @endif><span> إدارة الحضور و الإنصراف</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="vacationsss"
                                                                                    @if(in_array("vacationsss", $permissions)) checked @endif><span> إدارة الإجازات</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="accept_vacationsss"
                                                                                    @if(in_array("accept_vacationsss", $permissions)) checked @endif><span> موافقة/رفض الإجازة</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="payroll"
                                                                                    @if(in_array("payroll", $permissions)) checked @endif><span> إدارة حساب الراتب</span></label>
                                                                    </div>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_reward"
                                                                                    @if(in_array("add_reward", $permissions)) checked @endif><span> إضافة مكافئة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_reward"
                                                                                    @if(in_array("delete_reward", $permissions)) checked @endif><span> حذف المكافئة</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="add_substraction"
                                                                                    @if(in_array("add_substraction", $permissions)) checked @endif><span> إضافة خصم</span></label>
                                                                    </div>
                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="delete_substraction"
                                                                                    @if(in_array("delete_substraction", $permissions)) checked @endif><span> حذف الخصم</span></label>
                                                                    </div>

                                                                </div>

                                                                <div class="col-sm-6">

                                                                    <h4 class="mt-0 mb-20">المسائلة</h4>

                                                                    <div class="checkbox icheck"><label><input
                                                                                    class="form-check-input" type="checkbox"
                                                                                    name="send_accounting"
                                                                                    @if(in_array("send_accounting", $permissions)) checked @endif><span> إرسال مسائلة</span></label>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="row">
                                                                <center class="mt-50">
                                                                    <button type="submit" class="btn btn-primary"
                                                                            style="width: 150px"><i class="fa fa-save"></i>
                                                                        <span>حفظ</span></button>
                                                                </center>
                                                            </div>

                                                        </form>

                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>