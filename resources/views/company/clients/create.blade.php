@extends("company.layouts.app", ["title"=>"إضافة عميل"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة العملاء
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>إضافة عميل</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("company.clients.index", $company->slug)}}"><i class="fa fa-list"></i> <span>قائمة العملاء</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <form action="{{route("company.clients.store", $company->slug)}}" method="post" enctype="multipart/form-data" style="padding: 0px 0px 40px 20px">
                                    @csrf
                                    <input type="hidden" value="{{$company->id}}" name="company_id" id="company_id" class="form-control" required>

                                    <div class="row">

                                        <div class="col-sm-6">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-4" for="name">الإسم <span class="red">*</span></label>
                                                <input type="text" value="{{old('name')}}" name="name" id="name" class="form-control col-sm-8" required>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="role">المسمى <span class="red">*</span></label>
                                                <select name="role" id="role" class="form-control col-sm-8" required>
                                                    <option value="">إختر</option>
                                                    <option value="0">عميل فرد</option>
                                                    <option value="1">عميل شركة</option>
                                                    <option value="2">عميل جهة خيرية</option>
                                                </select>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="image">الصورة <span class="red">*</span></label>
                                                <input type="file" accept="image/*" name="image" id="image" class="form-control col-sm-8" required>
                                            </div>

                                        </div>

                                        <div class="col-sm-6">

                                            <div class="form-group mt-10 row">
                                                <label class="col-sm-4" for="email">البريد الإلكتروني <span class="red">*</span></label>
                                                <input type="email" value="{{old('email')}}" name="email" id="email" class="form-control col-sm-8" required>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="phone">رقم الجوال <span class="red">*</span></label>
                                                <input type="text" value="{{old('phone')}}" name="phone" id="phone" class="form-control col-sm-8" required>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="status"> الحالة <span class="red">*</span></label>
                                                <select  name="status" id="status" class="form-control col-sm-8" required>
                                                    <option value="">إختر</option>
                                                    <option value="1">فعال</option>
                                                    <option value="0">موقوف</option>
                                                </select>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="password">كلمة المرور <span class="red">*</span></label>
                                                <input type="password" name="password" id="password" class="form-control col-sm-8" required>
                                            </div>

                                        </div>

                                        <div class="col-sm-9"></div>
                                        <div class="col-sm-3">
                                            <div class="form-group mt-20">
                                                <button type="submit" class="btn btn-primary pull-left" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                            <div class="col-sm-5"></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection