@extends("company.layouts.app", ["title"=>"قائمة العملاء"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة العملاء
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة العملاء</b>
                        @if(permissions_general("add_client"))
                            <a class="btn btn-default btn-xs pull-left" href="{{route("company.clients.create", @$company->slug)}}"><i class="fa fa-plus"></i> <span>إضافة عميل</span></a>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px"></th>
                                    <th>الصورة</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>النوع</th>
                                    <th>الحالة</th>
                                    <th>الأوامر</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($clients as $client)
                                    <tr>
                                        <td style="vertical-align: middle;" class="text-center">{{$loop->iteration}}</td>
                                        <td>
                                            @if($client->image)
                                                <img src="{{asset("storage/app/".$client->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                            @else
                                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                            @endif
                                        </td>
                                        <td style="vertical-align: middle;">{{$client->name}}</td>
                                        <td style="vertical-align: middle;">{{$client->email}}</td>
                                        <td style="vertical-align: middle;">
                                            @if($client->role_id==0)
                                                <span>عميل فرد</span>
                                            @else
                                                <span>عميل شركة</span>
                                            @endif
                                        </td>
                                        <td style="vertical-align: middle;">
                                            @if($client->status==1)
                                                <strong class="alert alert-success alert-xs" style="width: 90%;">فعال</strong>
                                            @else
                                                <strong class="alert alert-danger alert-xs" style="width: 90%;">موقوف</strong>
                                            @endif
                                        </td>
                                        <td style="vertical-align: middle;">
                                            @if(permissions_general("show_client"))
                                                <a class="btn btn-info btn-xs" href="{{route("company.clients.show", ["company"=>@$company->slug, "user"=>$client->id])}}"><i class="fa fa-eye"></i> <span>عرض</span></a>
                                            @endif
                                            @if(permissions_general("update_client"))
                                                <a class="btn btn-success btn-xs" href="{{route("company.clients.edit", ["company"=>$company->slug, "user"=>$client->id])}}"><i class="fa fa-edit"></i> <span>تعديل</span></a>
                                            @endif
                                            @if(permissions_general("delete_client"))
                                                <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="document.getElementById('deleteform{{$client->id}}').submit()"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                                <form id="deleteform{{$client->id}}" action="{{route("company.clients.destroy", ["company"=>$company->slug, "user"=>$client->id])}}" method="post">@csrf {{method_field("DELETE")}}</form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection