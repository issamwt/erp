@extends("company.layouts.app", ["title"=>"عرض الموظف"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة الموظفين
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>عرض الموظف</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("company.clients.index", @$company->slug)}}"><i class="fa fa-list"></i> <span>قائمة العملاء</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                @if($user->image)
                                    <img src="{{asset("storage/app/".$user->image)}}" style="width: 150px; height: 150px; border-radius: 50%; border: 1px solid #eee; margin: 10px auto; display: block;">
                                @else
                                    <img src="{{asset("public/noimage.png")}}" style="width: 150px; height: 150px; border-radius: 50%; border: 1px solid #eee; margin: 10px auto; display: block;">
                                @endif
                                <h4 class="mt-20 text-center">{{$user->name}}</h4>
                                <h5 class="mt-10 text-center"><i class="fa fa-at"></i> {{$user->email}}</h5>
                                <h5 class="mt-10 text-center"><i class="fa fa-phone"></i> {{$user->phone}}</h5>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection