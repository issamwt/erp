@if(permissions_meetings("add_file"))
    <div style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

    <form action="{{route("company.meetings.save_attachment", @$company->slug)}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

        <div class="row mb-15">
            <div class="col-sm-2"><strong>تحميل ملف <span class="red">*</span></strong></div>
            <div class="col-sm-3"><input type="file" class="form-control" name="path" accept=".doc,.docx,application/msword,image/*,application/pdf" required></div>
            <div class="col-sm-2"><strong>إسم الملف <span class="red">*</span></strong></div>
            <div class="col-sm-3"><input type="text" class="form-control" name="name" required></div>
            <div class="col-sm-2"><button class="btn btn-primary btn-block" type="submit"><i class="fa fa-plus"></i> إضافة</button></div>
        </div>

    </form>

    <form action="{{route("company.meetings.save_attachment", @$company->slug)}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

        <div class="row mb-15">
            <div class="col-sm-2"><strong>ضع رابط <span class="red">*</span></strong></div>
            <div class="col-sm-3"><input type="url" class="form-control" name="url" required></div>
            <div class="col-sm-2"><strong>إسم الملف <span class="red">*</span></strong></div>
            <div class="col-sm-3"><input type="text" class="form-control" name="name" required></div>
            <div class="col-sm-2"><button class="btn btn-primary btn-block" type="submit"><i class="fa fa-plus"></i> إضافة</button></div>
        </div>

    </form>

</div>
@endif

<div style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

    @if(count($meeting->attachments)>0)
    <table class="table table-bordered table-striped dataTable">
        <thead>
            <tr>
                <th width="40px" class="text-center"></th>
                <th>إسم الملف</th>
                <th>نوع الملف</th>
                <th>تحميل الملف</th>
                <th>التاريخ</th>
                @if(permissions_meetings("delete_file"))
                <th>حذف</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($meeting->attachments as $attachment)
                <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td>{{$attachment->name}}</td>
                    <td class="text-center">
                        @if($attachment->url) <span>ملف</span> @else <span>رابط</span> @endif
                    </td>
                    <td class="text-center">
                        @if($attachment->url)
                            <a href="{{$attachment->url}}" target="_blank" class="btn btn-primary btn-xs" style="width: 120px;"><i class="fa fa-link"></i> <span>الذهاب إلى الرابط</span></a>
                        @else
                            <a href="{{asset("storage/app/".$attachment->path)}}" target="_blank" class="btn btn-primary btn-xs" style="width: 120px;"><i class="fa fa-download"></i> <span>تحميل الملف المرفق</span></a>
                        @endif
                    </td>
                    <td>{{$attachment->created_at->diffForhumans()}}</td>
                    @if(permissions_meetings("delete_file"))
                        <td>
                            <a href="{{route("company.meetings.delete_attachment", ["company"=>$company->slug, "id"=>$attachment->id])}}" onclick="return confirm('هل أنت متأكد ؟')" class="btn btn-danger btn-xs" style="width: 100px"><i class="fa fa-trash"></i> <span>حذف</span></a>
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
        <h4>لا توجد ملفات حاليا !</h4>
    @endif

</div>