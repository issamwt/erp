<div id="invited">

    <?php $userss=[]; ?>
    <table class="table table-bordered mt-20">
        <thead>
        <tr>
            <th>الصورة</th>
            <th>الإسم</th>
            <th>النوع</th>
            <th>المهمة</th>
            <th>الرد</th>
            @if($meeting->status==2 or $meeting->status==1)
                <th>حالة الحضور</th>
            @endif
            <th>الملاحظات</th>
            <th>التعديل</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $u)
            @if(!in_array($u->id, $userss))
                <?php array_push($userss, $u->id); ?>
                    <tr>
                        <td>
                            @if(@$u->image)
                                <img src="{{asset("storage/app/".@$u->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 3px solid #eee; float: right;">
                            @else
                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 3px solid #eee; float: right;">
                            @endif
                        </td>
                        <td style="vertical-align: middle"><h5 style="color: #204c65; margin-top: 0px;vertical-align: middle">{{@$u->name}}</h5></td>
                        <td style="vertical-align: middle"><span class="invited_here" style="width: 100%;display: block;margin: 0px;">@if($u->type==0) <span>موظف</span> @else <span>عميل</span> @endif </span></td>
                        <td style="vertical-align: middle"><span class="invited_type" style="width: 100%;display: block;margin: 0px;padding: 3px 10px;">@if($u->invited=="manager") <span>مدير الإجتماع</span>@elseif($u->invited=="amin")<span>أمين الإجتماع</span>@else <span>مدعو</span> @endif</span></td>
                        <td>
                            <div class="fa-wrapper" style="text-align: center;">
                                    @if(@$u->invited_infos)
                                        @if(@$u->invited_infos->attendance==0)
                                            <span style="color: #2fa360">سأحضر</span>
                                        @elseif(@$u->invited_infos->attendance==1)
                                            <span style="color: crimson">لن أحضر</span>
                                        @elseif(@$u->invited_infos->attendance==2)
                                            <span style="color: coral">ربما أحضر</span>
                                        @else
                                        <span style="color: royalblue"> لم يرد</span>
                                        @endif
                                    @else
                                        <span style="color: royalblue">بإنتظار الرد</span>
                                    @endif
                            </div>
                        </td>
                        @if($meeting->status==2 or $meeting->status==1)
                            <td class="chikas">
                                <div class="fa-wrapper" style="text-align: center;">
                                    @if(@$u->invited_infos)
                                        @if(@$u->invited_infos->attendance2==0)
                                            <span style="color: royalblue">?</span>
                                        @elseif(@$u->invited_infos->attendance2==1)
                                            <span style="color: #2fa360">حضر</span>
                                        @elseif(@$u->invited_infos->attendance2==2)
                                            <span style="color: crimson">لم يحضر</span>
                                        @elseif(@$u->invited_infos->attendance2==3)
                                            <span style="color: #8b2fff">معتذر</span>
                                        @elseif(@$u->invited_infos->attendance2==4)
                                            <span style="color: #ff47cf">إجازة</span>
                                        @elseif(@$u->invited_infos->attendance2==5)
                                            <span style="color: #d47501">حضر متأخر</span>
                                        @else
                                            <span style="color: royalblue">?</span>
                                        @endif
                                    @else
                                        <span style="color: royalblue">?</span>
                                    @endif
                                </div>
                            </td>
                        @endif
                        <td width="30%">
                            @if(@$u->invited_infos->note and @$u->invited_infos->attendance!=0)
                                <p class="mt-20">{{@$u->invited_infos->note}}</p>
                            @else

                            @endif
                        </td>
                        @if($meeting->status==0)
                            <td>
                                @if($user->id==$u->id)

                                    @if($meeting->status!=2 and $meeting->status!=3)

                                        <a class="btn btn-primary btn-block btn-xs" href="#" data-toggle="modal" data-target="#editInvitedModal"><i class="fa fa-edit"></i> <span style="margin-right: 5px;margin-left: 5px;">تعديل</span></a>

                                        <div id="editInvitedModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">الحضور</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route("company.meetings.save_invited", @$company->slug)}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="user_id" value="{{$u->id}}">
                                                            <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

                                                            <div class="form-group">
                                                                <label>الحضور</label>
                                                                <select class="form-control attendancet" required name="attendance">
                                                                    <option value="0">سأحضر</option>
                                                                    <option value="1">لن أحضر</option>
                                                                    <option value="2">ربما أحضر</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group" id="notet" style="display:none;">
                                                                <label>الملاحظة</label>
                                                                <textarea class="form-control"  name="note" rows="4">{{@$u->invited_infos->note}}</textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-primary" style="width: 120px;"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    @endif

                                @endif
                            </td>
                        @elseif($meeting->status==2 or $meeting->status==1)
                            <td>
                                @if($user->id==$meeting->manager_id or $user->id==$meeting->amin_id)
                                    <a class="btn btn-primary btn-block btn-xs btn-chikas" href="#" data-toggle="modal" data-target="#editInvited2Modal{{$u->id}}"><i class="fa fa-edit"></i> <span style="margin-right: 5px;margin-left: 5px;">تعديل</span></a>
                                    <div id="editInvited2Modal{{$u->id}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">الحضور</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{route("company.meetings.save_invited2", @$company->slug)}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$u->id}}">
                                                        <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

                                                        <div class="form-group">
                                                            <label>حالة الحضور</label>
                                                            <select class="form-control attendancet2" required name="attendance2">
                                                                <option value="1">حضر</option>
                                                                <option value="2">لم يحضر</option>
                                                                <option value="5">حضر متأخر</option>
                                                                <option value="3">معتذر</option>
                                                                <option value="4">إجازة</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group" id="notet2" style="display:none;">
                                                            <label>الملاحظة</label>
                                                            <textarea class="form-control"  name="note" rows="4">{{@$u->invited_infos->note}}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary" style="width: 120px;"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endif
                            </td>
                        @endif
                    </tr>
            @endif
        @endforeach
        </tbody>
    </table>

</div>


<style>

    .chikas{
        position: relative;
    }

    .chikas .btn-chikas{
        position: absolute;
        top: 5px;
        left: 5px;
        display: none;
    }

    .chikas:hover .btn-chikas{
        display: block;
    }

    #invited .evaluation:nth-child(2n){
        background: #f8f8f8;
    }

    #invited .invited{
        position: relative;
    }

    #invited .invited .invited_buttons{
        position: absolute;
        top: 10px;
        left: 10px;
    }

    #invited .name{
        width: 100px;
        display: inline-block;
    }

    #invited .invited_here{
        background: gold;
        margin-right: 10px;
        padding: 3px 10px;
        color: #333;
        font-size: 0.9em;
        width: 100px;
        display: inline-block;
        text-align: center;
    }

    #invited .invited_type{
        display: block;
        width: 100px;
        text-align: center;
        margin: 19px auto 0px;
        background: gold;
        padding: 1px 10px;
        color: #333;
        font-size: 0.9em;
        font-weight: bold;
    }

    #invited .fa-round{
        width: 40px;
        height: 40px;
        font-size: 1.4em;
        border-radius: 50%;
        text-align: center;
        line-height: 40px;
        color: #fff;
        display: inline-block;
    }

    #invited span{
        display: inline-block;
        font-size: 1.1em;
        font-weight: bold;
    }

    #invited .fa-wrapper{
        margin: 10px auto 0px;
    }

    #invited .fa-success{
        background: #00c500;
    }

    #invited .fa-danger{
        background: crimson;
    }

    #invited .fa-primary{
        background: #1b80e2;
    }

    #invited .fa-warning{
        background: orange;
    }

    #invited .invited:nth-child(2n){
        background: #f8f8f8;
    }

</style>