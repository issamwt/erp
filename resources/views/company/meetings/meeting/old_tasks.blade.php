
<div id="oldtask-wrapper">

    <div id="oldtask-div">

        @if(count($old_tasks)>0)
            
            <h4 class="mb-20">قائمة المهام السابقة : (سلسلة "{{$meeting->category->name}}")</h4>

            {{--
            
            <form id="fomme">

                <div style="width: 80px !important; display: inline-block">
                    <strong style="font-size: 1.25em;">الفلاتر : </strong>
                </div>

                <div style="width: 180px !important; display: inline-block">
                    <select class="form-control select2Status">
                        <option value="all">-الحالة-</option>
                        <option value="0" selected>مهمة جديدة</option>
                        <option value="1">مهمة قيد التنفيذ</option>
                        <option value="2">مهمة منجزة</option>
                    </select>
                </div>

                <div style="width: 180px !important; display: inline-block">
                    <select class="form-control select2Categories">
                        <option value="all">-السلسلة-</option>
                        @foreach($categories as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div style="width: 180px !important; display: inline-block">
                    <select class="form-control select2Meeting">
                        <option value="all">-الإجتماع-</option>
                        @foreach($meetings as $m)
                            <option value="{{$m->id}}">{{$m->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div style="width: 180px !important; display: inline-block">
                    <select class="form-control select2User">
                        <option value="all">-عضو الفريق-</option>
                        @foreach($users as $u)
                            <option value="{{$u->id}}">{{$u->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div style="width: 180px !important; display: inline-block">
                    <input type="text" class="form-control datepicker dateFrom" placeholder="التاريخ من" style="text-align: right;">
                </div>

                <div style="width: 180px !important; display: inline-block">
                    <input type="text" class="form-control datepicker dateTo" placeholder="التاريخ إلى" style="text-align: right;">
                </div>

            </form>

            <div class="clearfix"><hr></div>

            --}}

            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>الإجتماع</th>
                        <th>الإسم</th>
                        <th style="min-width: 80px">التاريخ</th>
                        <th>المكلف</th>
                        <th>المساعدون</th>
                        <th>الحالة</th>
                        <th>الأوامر</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($old_tasks as $task)
                        <?php $userss = [@$task->user->id]; foreach($task->users as $u) array_push($userss, $u->id); $userss = implode(";", $userss); ?>
                        <tr data-id="{{$task->id}}" data-status="{{$task->status}}" data-meeting="{{$task->daftar_id}}" data-category="{{@$task->meeting->category_id}}" data-users="{{$userss}}" data-date="{{$task->thedate}}"
                        style="@if($task->status!=0) display:none; @endif">
                            <td width="20%">{{@$task->meeting->name}}</td>
                            <td width="20%"><a href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}">{{$task->name}}</a></td>
                            <td class="text-center">{{$task->thedate}}</td>
                            <td width="20%">
                                @if(@$task->user->image)
                                    <img src="{{asset("storage/app/".@$task->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                @else
                                    <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                @endif
                                <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$task->user->name}}</h6>
                            </td>
                            <td width="20%">
                                @if(count($task->users))
                                    @foreach($task->users as $user)
                                        @if(@$user->image)
                                            <img src="{{asset("storage/app/".@$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                        @else
                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                        @endif
                                        <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$user->name}}</h6>
                                        <div class="clearfix mb-10"></div>
                                    @endforeach
                                @endif
                            </td>
                            <td style="vertical-align: middle">
                                @if($task->status==0)
                                    <ul class="nav nav-pills"><li style="background: crimson;border-radius: 10px;color: #fff;display: block;width: 100%;text-align: center;">لم تمت</li></ul>
                                @else
                                    <ul class="nav nav-pills"><li style="background: chartreuse;border-radius: 10px;display: block;width: 100%;text-align: center;">تمت</li></ul>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="mt-15 btn-xs btn btn-info btn-50" href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}" title="المشاهدة"><span class="fa fa-eye"></span></a>
                                @if(permissions_meetings("update_task"))
                                    <a class="mt-15 btn-xs btn btn-primary btn-50" href="#" data-toggle="modal" data-target="#editTaskeModal{{$task->id}}" title="التعديل"><span class="fa fa-edit"></span></a>
                                @else
                                    <a class="mt-15 btn-xs btn btn-primary btn-50 disabled" href="#" title="التعديل"><span class="fa fa-edit"></span></a>
                                @endif
                                
                                @if(permissions_meetings("update_task"))
                                    @if($task->status==0)
                                        <a class="mt-15 btn-xs btn btn-success btn-50 change_task_status" data-id = "{{$task->id}}" data-status="1" href="#" data-toggle="tooltip" title="تمت"><span class="fa fa-check"></span></a>
                                    @else
                                        <a class="mt-15 btn-xs btn btn-danger btn-50 change_task_status" data-id = "{{$task->id}}" data-status="0" href="#" data-toggle="tooltip" title="لم تمت"><span class="fa fa-remove"></span></a>
                                    @endif
                                @endif
                                
                                @if(permissions_meetings("delete_task"))
                                    <a class="delete_oldtask mt-15 btn-xs btn btn-warning btn-50" data-id="{{$task->id}}" href="#" data-toggle="tooltip" data-placement="top"  title="الحذف"><span class="fa fa-remove"></span></a>
                                @else
                                    <a class="mt-15 btn-xs btn btn-warning btn-50 disabled" href="#" data-toggle="tooltip" data-placement="top"  title="الحذف"><span class="fa fa-remove"></span></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        {{--
            <div class="mt-15">
                <div style="background:#367fa9; display: inline-block; width: 15px; height: 15px; border-radius: 2px; float: right;"></div>
                <h6 class="pull-right" style="margin: 0px 10px 0px 01px;font-size: 1em;">مهمة جديدة</h6>
                <div class="clearfix"></div>
            </div>
            <div class="mt-15">
                <div style="background:#e08e0b; display: inline-block; width: 15px; height: 15px; border-radius: 2px; float: right;"></div>
                <h6 class="pull-right" style="margin: 0px 10px 0px 01px;font-size: 1em;">مهمة قيد التنفيذ</h6>
                <div class="clearfix"></div>
            </div>
            <div class="mt-15">
                <div style="background:#00a65a; display: inline-block; width: 15px; height: 15px; border-radius: 2px; float: right;"></div>
                <h6 class="pull-right" style="margin: 0px 10px 0px 01px;font-size: 1em;">مهمة منجزة</h6>
                <div class="clearfix"></div>
            </div>
            --}}

        @else
            <h4>لا توجد مهام سابقة !</h4>
        @endif

        @foreach($old_tasks as $task)

            <div id="showTaskNoteModal{{$task->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{$task->name}}</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>السلسة</strong></div>
                                <div class="col-sm-9">{{$meeting->category->name}}</div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>الإجتماع</strong></div>
                                <div class="col-sm-9">{{@$task->meeting->name}}</div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>إسم المهمة</strong></div>
                                <div class="col-sm-9">{{$task->name}}</div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>حالة المهمة</strong></div>
                                <div class="col-sm-9">
                                    @if($task->status==0)
                                        <div><b>لم تتم</b></div>
                                    @else
                                        <div><b>تمت</b></div>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>المكلف</strong></div>
                                <div class="col-sm-9">
                                    @foreach($users as $user)
                                        @if($user->id==$task->user_id) <span style="margin-left: 10px">{{$user->name}}</span> @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>المساعدون</strong></div>
                                <div class="col-sm-9">
                                    <?php $userss=[]; foreach ($task->users as $u): array_push($userss, $u->id); endforeach; ?>
                                    @foreach($users as $user)
                                        @if(in_array($user->id, $userss)) <span style="margin-left: 5px">[{{$user->name}}]</span> @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>التاريخ</strong></div>
                                <div class="col-sm-9">{{$task->thedate}}</div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>التفاصيل</strong></div>
                                <div class="col-sm-9 text-justify">{{$task->note}}</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="editTaskeModal{{$task->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">تعديل مهمة</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{route("company.meetings.update_task", @$company->slug)}}" method="post" class="update_old_task" data-id="{{$task->id}}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$task->id}}">

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>إسم المهمة <span class="red">*</span></strong></div>
                                        <div class="col-sm-9"><input autocomplete="off" type="text" value="{{$task->name}}" class="form-control" name="name" required></div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>التاريخ </strong></div>
                                        <div class="col-sm-9"><input value="{{$task->thedate}}" type="text" class="form-control datepicker" autocomplete="off" style="text-align: right" name="thedate"></div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>التفاصيل <span class="red">*</span></strong></div>
                                        <div class="col-sm-9"><textarea class="form-control" name="note" rows="5" required>{{$task->note}}</textarea></div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3">
                                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>تعديل</span></button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

        @endforeach

    </div>

</div>

<style>
    #oldtask-wrapper{
        border: 1px solid #eee;
        padding: 20px;
        margin-top: 25px;
    }
</style>

