<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>نظام ERP @if(@$title) | {{@$title}}@endif</title>
        <link rel="icon" type="image/png" href="{{asset("public/uploads/favicon.png")}}" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.4 -->
        <link rel="stylesheet" href="{{asset("public/bootstrap/css/bootstrap.min.css")}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="{{asset("public/plugins/datatables/dataTables.bootstrap.css")}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset("public/dist/css/AdminLTE.min.css")}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset("public/dist/css/skins/_all-skins.min.css")}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            body { font-family: "DejaVu Sans";}
        </style>
    </head>

<body>

    <center class="mt-20 mb-20">
        <button id="cmd" class="btn btn-primary"><i class="fa fa-spinner fa-spin fa-large" style="display: none"></i><i class="fa fa-file-pdf-o"></i> طباعة بي دي آف</button>
    </center>

    <div id="content" class="container-fluid" style="padding: 0px 40px;">

        <h1 class="text-center" style="margin-top: 70px; margin-bottom: 140px;">{{$meeting->name}}</h1>

        <hr>

        <div class="row">
            <div class="col-sm-4">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>سلسلة إجتماعات</label></div>
                    <div class="col-xs-8">{{@$meeting->category->name}}</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>موضوع الإجتماع</label></div>
                    <div class="col-xs-8">{{$meeting->name}}</div>
                </div>
                <div class="row" style="margin-bottom: 0px">
                    <div class="col-xs-4"><label>مكان الإجتماع</label></div>
                    <div class="col-xs-8">{{@$meeting->location->name}}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-3"><label>التاريخ</label></div>
                    <div class="col-xs-9">{{$meeting->from_date}}</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-3"><label>المدة</label></div>
                    <div class="col-xs-9">{{$meeting->duration}} <span>دقيقة</span></div>
                </div>
                <div class="row" style="margin-bottom: 0px">
                    <div class="col-xs-3"></div>
                    <div class="col-xs-9"></div>
                </div>
                <div class="row" style="margin-top: 10px">
                    <div class="col-xs-3"><b>تكرار كل</b></div>
                    <div class="col-xs-9">
                        @if($meeting->repeats==0)
                            <span>لا يوجد</span>
                        @elseif($meeting->repeats==1)
                            <span>يوم</span>
                        @elseif($meeting->repeats==2)
                            <span>أسبوع</span>
                        @elseif($meeting->repeats==3)
                            <span>شهر</span>
                        @else
                            <span>لا يوجد</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4">
                        <label class="choice @if($meeting->type==0) active @endif">
                            <i class="fa fa-check"></i><span>إجتماع عادي</span>
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <label class="choice @if($meeting->type==1) active @endif">
                            <i class="fa fa-times"></i><span>إجتماع مهم</span>
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <label class="choice @if($meeting->type==2) active @endif">
                            <i class="fa fa-exclamation"></i><span>إجتماع طارئ</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-2"><label>طريقة الإجتماع</label></div>
                    <div class="col-xs-10">
                        @if($meeting->online==0)
                            <div>عن بعد</div>
                        @elseif($meeting->online==1)
                            <div>تسجيل الإجتماع</div>
                        @else
                            <div>حضوريا</div>
                        @endif
                    </div>
                </div>
            </div>
            @if($meeting->online==2)
            <div class="col-sm-8">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-2"><label>رابط الغوغل ماب</label></div>
                    <div class="col-xs-10">{{$meeting->gmap_url}}</div>
                </div>
            </div>
            @endif
        </div>

        <hr>

        <div class="row">
            <div class="col-sm-5">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>مدير الإجتماع</label></div>
                    <div class="col-xs-8">@if(@$meeting->manager){{@$meeting->manager->name}}@else<span>لا يوجد</span>@endif</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>أمين الإجتماع</label></div>
                    <div class="col-xs-8">@if(@$meeting->amin){{@$meeting->amin->name}}@else<span>لا يوجد</span>@endif</div>
                </div>
            </div>
            <div class="col-sm-2"><div style="height: 80px; width: 2px; background: red;margin: 0px auto"></div></div>
            <div class="col-sm-5">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>الموظفين المدعوين</label></div>
                    <div class="col-xs-8">
                        @if(count(@$meeting->users)>0)
                            @foreach(@$meeting->users as $user) <span >{{@$user->name}}</span>@endforeach
                        @else
                            <span>لا يوجد</span>
                        @endif
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>العملاء المدعوين</label></div>
                    <div class="col-xs-8">
                        @if(count(@$meeting->clients)>0)
                            @foreach(@$meeting->clients as $client) <span >{{@$client->name}}</span>@endforeach
                        @else
                            <span>لا يوجد</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr style="margin-top: 30px;">

        <h3>محاور الإجتماع : </h3>

        @if(count($axes)>0)
            @foreach($axes as $axis)
                <div style="border: 1px solid #eee; padding: 10px; margin-top: 25px; @if($axis->status==1) background:rgba(255,0,0,0.19); @endif">

                    <div class="row">

                        <strong class="col-sm-7">
                            {{$axis->name}}
                            <hr style="margin: 10px 0px">
                            <div class="row">
                                <div class="col-sm-6"><i class="fa fa-user"></i> <span>{{@$axis->speaker->name}}</span></div>
                                <div class="col-sm-6"><i class="fa fa-clock-o"></i> <span>ساعة : </span><span>{{$axis->hours}}</span> <span style="padding-right: 20px;">دقيقة : </span> <span>{{$axis->minutes}}</span></div>
                            </div>
                        </strong>

                        <div class="col-sm-5" style="margin-top: 20px;">
                            @if($meeting->status==1)
                                @if($axis->status==0)

                                @elseif($axis->status==1)

                                @else
                                    <span>منته</span>
                                @endif
                            @else
                                @if($axis->status==0)
                                    <span>جديد</span>
                                @else
                                    <span>منته</span>
                                @endif
                            @endif
                        </div>

                    </div>

                </div>
            @endforeach
        @else
            <strong>لا توجد محاور حاليا</strong>
        @endif

        <hr style="margin: 40px 0px">

        <h3>المهام : </h3>

            @if(count($tasks)>0)
                <table class="table table-bordered" id="dataTable">
                    <thead>
                    <tr>
                        <th width="30" class="text-center">م</th>
                        <th style="min-width: 150px">الإسم</th>
                        <th style="min-width: 180px">المكلف</th>
                        <th style="min-width: 180px">المساعدون</th>
                        <th style="min-width: 80px">التاريخ</th>
                        <th>الملاحظات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                        <tr style="@if($task->status==1) background:rgba(255,0,0,0.19); @endif">
                            <td width="30" class="text-center">{{$loop->iteration}}</td>
                            <td>{{$task->name}}</td>
                            <td>
                                @if(@$task->user->image)
                                    <img src="{{asset("storage/app/".@$task->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                @else
                                    <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                @endif
                                <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$task->user->name}}</h6>
                            </td>
                            <td>
                                @if(count($task->users))
                                    @foreach($task->users as $user)
                                        @if(@$user->image)
                                            <img src="{{asset("storage/app/".@$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                        @else
                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                        @endif
                                        <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$user->name}}</h6>
                                        <div class="clearfix mb-10"></div>
                                    @endforeach
                                @endif
                            </td>
                            <td>{{$task->thedate}}</td>
                            <td class="text-center">
                                <p class="text-justify">{{$task->note}}</p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h4>لا توجد مهام حاليا !</h4>
            @endif


        <hr style="margin: 40px 0px">

        <h3>المرفقات : </h3>

        @if(count($meeting->attachments)>0)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="40px" class="text-center"></th>
                    <th>التاريخ</th>
                    <th>إسم الملف</th>
                    <th>نوع الملف</th>
                    <th>رابط الملف</th>
                </tr>
                </thead>
                <tbody>
                @foreach($meeting->attachments as $attachment)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$attachment->created_at->diffForhumans()}}</td>
                        <td>{{$attachment->name}}</td>
                        <td class="text-center">
                            @if($attachment->url) <span>ملف</span> @else <span>رابط</span> @endif
                        </td>
                        <td class="text-center">
                            @if($attachment->url)
                                <span>{{$attachment->url}}</span>
                            @else
                                <span>{{asset("storage/app/".$attachment->path)}}</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h4>لا توجد ملفات حاليا !</h4>
        @endif

        <hr style="margin: 40px 0px">

        <h3>التصويت : </h3>

        @if(count($meeting->votes)>0)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="40px" class="text-center">م</th>
                    <th>موضوع التصويت</th>
                    <th>مدة النقاش</th>
                    <th>طريقة التصويت</th>
                    <th width="50%">الحالة</th>
                </tr>
                </thead>
                <tbody>
                @foreach($meeting->votes as $vote)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$vote->name}}</td>
                        <td>
                            <div class="row">
                                <div class="col-sm-6 text-center"><span style="padding-left: 10px">ساعة : </span> <span>{{$vote->hours}}</span></div>
                                <div class="col-sm-6 text-center"><span style="padding-left: 10px">دقيقة : </span> <span>{{$vote->minutes}}</span></div>
                            </div>
                        </td>
                        <td>
                            @if($vote->type==0)
                                <span>موافق أو غير موافق</span>
                            @elseif($vote->type==1)
                                <span>خيارات متعددة</span>
                            @elseif($vote->type==2)
                                <span>نص</span>
                            @endif
                        </td>
                        <td>
                            @if(count($vote->votes)>0)
                                @if($vote->type==2)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>النص : </strong></div>
                                        <div class="col-sm-12 mt-5">{{$vote->options}}<hr></div>
                                    </div>
                                    @foreach($vote->votes as $vote_user)
                                        <div class="row mb-15">
                                            <div class="col-sm-12">
                                                <div style="background: #eee;border: 1px solid #d0d0d0;padding: 15px 0px;">
                                                    <div class="col-sm-12">
                                                        @if(@$vote_user->user->image)
                                                            <img src="{{asset("storage/app/".@$vote_user->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                        @else
                                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                        @endif
                                                        <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$vote_user->user->name}}</h6>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-12 mt-5"><strong>الرد على النص : </strong></div>
                                                            <div class="col-sm-12 mt-5 text-justify">{{$vote_user->result}}</div>
                                                            <div class="col-sm-12 mt-5"><strong>الملاحظة : </strong></div>
                                                            <div class="col-sm-12 mt-5 text-justify">@if($vote_user->note){{$vote_user->note}}@else <span>لا توجد</span>@endif</div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @elseif($vote->type==1)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>خيارات متعددة : </strong></div>
                                        <div class="col-sm-12 mt-5">
                                            <ul>
                                                @foreach(unserialize($vote->options) as $option)
                                                    <li>{{$option}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @foreach($vote->votes as $vote_user)
                                        <div class="row mb-15">
                                            <div class="col-sm-12">
                                                <div style="background: #eee;border: 1px solid #d0d0d0;padding: 15px 0px;">
                                                    <div class="col-sm-12">
                                                        @if(@$vote_user->user->image)
                                                            <img src="{{asset("storage/app/".@$vote_user->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                        @else
                                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                        @endif
                                                        <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$vote_user->user->name}}</h6>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-12 mt-5"><strong>الخيار : </strong></div>
                                                            <div class="col-sm-12 mt-5 text-justify">
                                                                <span>{{@unserialize($vote->options)[intval($vote_user->result)]}}</span>
                                                            </div>
                                                            <div class="col-sm-12 mt-5"><strong>الملاحظة : </strong></div>
                                                            <div class="col-sm-12 mt-5 text-justify">@if($vote_user->note){{$vote_user->note}}@else <span>لا توجد</span>@endif</div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <?php
                                    $yes = 0; $accepted = true;
                                    foreach($vote->votes as $vote_user){
                                        if($vote_user->result==0)
                                            $yes++;
                                    }
                                    if($yes<(count($vote->votes)/2))
                                        $accepted =false;
                                    ?>
                                    <a href="#" class="alert @if($accepted) alert-success @else alert-danger @endif alert-xs alert-block" data-toggle="modal" data-target="#resultVoteModal1{{$vote->id}}">@if($accepted) <span>موافق</span> @else <span>غير موافق</span> @endif</a>
                                        <div class="row mb-15">
                                            <div class="col-sm-12 mt-5"><strong>موافق أم غير موافق : </strong></div>
                                            <div class="col-sm-12 mt-5">
                                                <ul>
                                                    <li>موافق</li>
                                                    <li>غير موافق</li>
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach($vote->votes as $vote_user)
                                            <div class="row mb-15">
                                                <div class="col-sm-12">
                                                    <div style="background: #eee;border: 1px solid #d0d0d0;padding: 15px 0px;">
                                                        <div class="col-sm-12">
                                                            @if(@$vote_user->user->image)
                                                                <img src="{{asset("storage/app/".@$vote_user->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                            @else
                                                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                            @endif
                                                            <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$vote_user->user->name}}</h6>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-12 mt-5"><strong>الخيار : </strong></div>
                                                                <div class="col-sm-12 mt-5 text-justify">
                                                                    @if($vote_user->result==0)
                                                                        <span style="color: #1c8732">موافق</span>
                                                                    @else
                                                                        <span style="color: crimson">غير موافق</span>
                                                                    @endif
                                                                </div>
                                                                <div class="col-sm-12 mt-5"><strong>الملاحظة : </strong></div>
                                                                <div class="col-sm-12 mt-5 text-justify">@if($vote_user->note){{$vote_user->note}}@else <span>لا توجد</span>@endif</div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                @endif
                            @else
                                <strong class="alert alert-primary alert-xs alert-block">جديد</strong>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h4>لا توجد تصويتات حاليا !</h4>
        @endif

        <hr style="margin: 40px 0px">

        <h3>تقييم الإجتماع : </h3>

        <div id="evaluation">

            <?php
            $all = collect(new \App\Evaluation);
            foreach ($evas as $eva)
                foreach ($eva->evaluations as $evaluation)
                    $all->push($evaluation);
            ?>

            <div class="evaluation" style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

                <div class="row">

                    <div class="col-sm-6">


                        <h5 style="margin: 20px 10px 0px 01px;color: #204c65">إجمالي التصويت</h5>
                        <div class="clearfix"></div>
                        <div class="mt-20" style="margin-right: 20px">
                            {{stars(sum_evaluations(@$all))}}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        @foreach(@$items->reverse() as $item)
                            <div class="row mb-10">
                                <div class="col-sm-6"><strong>{{@$item->name}}</strong></div>
                                <?php $item->evaluations = collect(new \App\Evaluation); ?>
                                @foreach($all as $a) @if($a->item_id==$item->id) <?php $item->evaluations->push($a); ?> @endif @endforeach
                                <div class="col-sm-6">{{stars(sum_evaluations(@$item->evaluations))}}</div>
                            </div>
                        @endforeach
                    </div>

                </div>

            </div>

            @foreach($evas as $eva)

                <div class="evaluation" style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

                    <div class="row">

                        <div class="col-sm-6">

                            @if(@$eva->user->image)
                                <img src="{{asset("storage/app/".$eva->user->image)}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                            @else
                                <img src="{{asset("public/noimage.png")}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                            @endif
                            <h5 class="pull-right" style="margin: 20px 10px 0px 01px;color: #204c65">{{@$eva->user->name}}</h5>
                            <div class="clearfix"></div>
                            <div class="mt-20" style="margin-right: 20px">
                                {{stars(sum_evaluations(@$eva->evaluations))}}
                            </div>
                        </div>

                        <div class="col-sm-6">
                            @foreach(@$eva->evaluations as $evaluation)
                                <div class="row mb-10">
                                    <div class="col-sm-6"><strong>{{@$evaluation->item->name}}</strong></div>
                                    <div class="col-sm-6">{{stars(@$evaluation->num)}}</div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                </div>

            @endforeach

        </div>

        <hr style="margin: 40px 0px">

        <h3>المدعوين : </h3>

        <div id="invited">

            <?php $userss=[]; ?>
                <table class="table table-bordered mt-20">
                    <thead>
                    <tr>
                        <th>الصورة</th>
                        <th>الإسم</th>
                        <th>النوع</th>
                        <th>المهمة</th>
                        <th>الحالة</th>
                        <th>الملاحظات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $u)
                        @if(!in_array($u->id, $userss))
                            <?php array_push($userss, $u->id); ?>
                            <tr>
                                <td>
                                    @if(@$u->image)
                                        <img src="{{asset("storage/app/".@$u->image)}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #eee; float: right;">
                                    @else
                                        <img src="{{asset("public/noimage.png")}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #eee; float: right;">
                                    @endif
                                </td>
                                <td><h5 style="color: #204c65; margin-top: 0px">{{@$u->name}}</h5></td>
                                <td><span class="invited_here" style="width: 100%;display: block;margin: 0px;">@if($u->type==0) <span>موظف</span> @else <span>عميل</span> @endif </span></td>
                                <td><span class="invited_type" style="width: 100%;display: block;margin: 0px;padding: 3px 10px;">@if($u->invited=="manager") <span>مدير الإجتماع</span>@elseif($u->invited=="amin")<span>أمين الإجتماع</span>@else <span>مدعو</span> @endif</span></td>
                                <td>
                                    <div class="fa-wrapper" style="text-align: center;">
                                        @if(@$u->invited_infos->attendance==0)
                                            <i class="fa fa-check fa-round fa-success"></i> <span>سأحضر</span>
                                        @elseif(@$u->invited_infos->attendance==1)
                                            <i class="fa fa-check fa-round fa-warning"></i> <span>لن أحضر</span>
                                        @elseif(@$u->invited_infos->attendance==2)
                                            <i class="fa fa-check fa-round fa-primary"></i> <span>ربما أحضر</span>
                                        @else
                                            <i class="fa fa-check fa-round fa-danger"></i> <span>غائب</span>
                                        @endif
                                    </div>
                                </td>
                                <td width="30%">
                                    @if(@$u->invited_infos->note and @$u->invited_infos->attendance!=0)
                                        <p class="mt-20">{{@$u->invited_infos->note}}</p>
                                    @else

                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>

        </div>

        <br><br><br>

    </div>

    <div id="editor"></div>

    <div id="printMeWarapper" style="padding: 0px 10px"></div>

    <style>

        .alert-primary{
            color: #fff;
            background-color: #3c8dbc;
            border-color: #367fa9;
        }

        .choice .fa {
            font-size: 1.5em;
            background: #fff;
            padding: 4px;
            border-radius: 50%;
            border: 1px solid #b9b9b9;
            width: 31px;
            height: 31px;
            text-align: center;
            line-height: 21px;
        }

        #invited .evaluation:nth-child(2n){
            background: #f8f8f8;
        }

        #invited .invited{
            position: relative;
        }

        #invited .invited .invited_buttons{
            position: absolute;
            top: 10px;
            left: 10px;
        }

        #invited .name{
            width: 100px;
            display: inline-block;
        }

        #invited .invited_here{
            background: gold;
            margin-right: 10px;
            padding: 3px 10px;
            color: #333;
            font-size: 0.9em;
            width: 100px;
            display: inline-block;
            text-align: center;
        }

        #invited .invited_type{
            display: block;
            width: 100px;
            text-align: center;
            margin: 19px auto 0px;
            background: gold;
            padding: 1px 10px;
            color: #333;
            font-size: 0.9em;
            font-weight: bold;
        }

        #invited .fa-round{
            width: 40px;
            height: 40px;
            font-size: 1.4em;
            border-radius: 50%;
            text-align: center;
            line-height: 40px;
            color: #fff;
            display: inline-block;
        }

        #invited span{
            display: inline-block;
            margin-right: 10px;
            font-size: 1.1em;
            font-weight: bold;
        }

        #invited .fa-wrapper{
            margin: 10px auto 0px;
        }

        #invited .fa-success{
            background: #00c500;
        }

        #invited .fa-danger{
            background: crimson;
        }

        #invited .fa-primary{
            background: #1b80e2;
        }

        #invited .fa-warning{
            background: orange;
        }

        #invited .invited:nth-child(2n){
            background: #f8f8f8;
        }

        .choice{
            width: 90px;
            height: 90px;
            border: 1px solid #b9b9b9;
            border-radius: 4px;
            text-align: center;
            line-height: 35px;
            display: block;
            margin: 0px auto;
            padding-top: 15px;
        }

        .col-xs-4:nth-child(1) .choice{
            background: #60bd03;
        }

        .col-xs-4:nth-child(1) .choice.active{
            border: 7px solid #60bd03;
            border-color: #458802;
        }

        .col-xs-4:nth-child(2) .choice{
            background: hotpink;
        }

        .col-xs-4:nth-child(2) .choice.active{
            border: 7px solid hotpink;
            border-color: #b74b81;
        }

        .col-xs-4:nth-child(3) .choice{
            background: crimson;
        }

        .col-xs-4:nth-child(3) .choice.active{
            border: 7px solid crimson;
            border-color: #a80f2e;
        }

        .choice .fa{
            font-size: 1.5em;
            background: #fff;
            padding: 4px;
            border-radius: 50%;
            border: 1px solid #b9b9b9;
            width: 31px;
            height: 31px;
            text-align: center;
            line-height: 21px;
        }

        .choice span{
            font-size: 0.6em;
            background: #fff;
            display: block;
            height: 17px;
            line-height: 18px;
            width: 70%;
            margin: 0px auto;
        }

        .alert-xs{font-size: 0.9em;
            padding: 1px 3px !important;
            display: inline-block;
            width: 70px;
            margin: 0px 10px 0px 0px;
            border-radius: 0px !important;
            text-align: center;
        }

        #meeting{
            padding-bottom: 100px;
        }

        #meeting .tab-content{
            min-height: 300px;
        }

        #meeting .nav.nav-tabs{
            margin-top: 30px;
        }

        #meeting .nav-tabs>li, #meeting .nav-tabs>li a{
            border-radius: 0px;
            font-size: 1.1em;
        }

        #comments .comment{
            background: #eee;
            border: 1px solid #d0d0d0;
            padding: 15px;
        }

        .num{
            width: 18px;
            height: 18px;
            background: #fff;
            border: 1px solid #eee;
            border-radius: 50%;
            text-align: center;
            line-height: 17px;
            font-size: 0.8em;
            display: inline-block;
            color: #333;
            font-style: normal;
            margin-left: 3px;
        }

        .icheck>label{
            padding-left: 15px;
            padding-right: 0px;
        }

        .btnn .fa{
            width: 20px;
            height: 20px;
            line-height: 19px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            border: 1px solid #eee;
            color: #333;
        }

        #evaluation .evaluation:nth-child(2n){
            background: #f8f8f8;
        }

        #evaluation .evaluation{
            position: relative;
        }

        #evaluation .evaluation-buttons{
            position: absolute;
            z-index: 1;
            left: 10px;
            top: 10px;
        }

        .dataTables_length, .dataTables_info{
            float: left;
        }
        .panel, .panel-heading, .panel-body, .btn{
            border-radius: 0px;
        }
        #dataTable th{
            text-align: right;
        }
        .form-control {
            border-radius: 0px !important;
        }
        .mt-0{
            margin-top: 0px !important;
        }
        .mt-5{
            margin-top: 5px;
        }
        .mt-10{
            margin-top: 10px;
        }
        .mt-15{
            margin-top: 15px;
        }
        .mt-20{
            margin-top: 20px;
        }
        .mt-30{
            margin-top: 30px;
        }
        .mb-5{
            margin-bottom: 5px;
        }
        .mb-10{
            margin-bottom: 10px;
        }
        .mb-15{
            margin-bottom: 15px;
        }
        .mb-20{
            margin-bottom: 20px;
        }
        .mb-50{
            margin-bottom: 50px;
        }
        .bootstrap-timepicker-widget{
            direction: ltr !important;
            right: auto !important;
        }
        .select2-container{
            width: 100% !important;
        }
        .select2-container--default .select2-selection--multiple{
            border-radius: 0px;
            border-color: #d2d6de;
        }
        #dataTable_length{
            float: right !important;
        }
        #dataTable_filter, #dataTable_paginate{
            float: left;
        }
        #ui_notifIt p{
            font-size: 1.2em;
            font-weight: bold;
        }
        .stars{
            padding: 0px;
            margin: 0px;
            list-style: none;
        }
        .stars li{
            float: right;
            padding-left: 5px;
        }
        .stars li .fa.active{
            color: gold;
        }
        .stars li .fa:not(.active){
            color: dimgray;
        }
        .stars li .fa-2x {
            font-size: 1.6em;
        }
    </style>

    <script src="{{asset("public/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <script src="{{asset('public/html2canvas.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.12.3/printThis.min.js"></script>

    <script>

    function pdfInvoice(){

        $("#cmd .fa-spinner").css("display", "inline-block");
        $("#cmd .fa-file-pdf-o").css("display", "none");

        html2canvas($("#content"), {
            onrendered: function(canvas) {
                var imgData = canvas.toDataURL('image/png');
                console.log(imgData);
                var imgWidth = 210;
                var pageHeight = 295;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;

                var doc = new jsPDF('p', 'mm');
                var position = 0;

                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = heightLeft - imgHeight;
                    doc.addPage();
                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save('meeting{{$meeting->id}}.pdf');

                setTimeout(function () {
                    //$('#printMeWarapper').fadeOut("fast");
                    $("#cmd .fa-spinner").css("display", "none");
                    $("#cmd .fa-file-pdf-o").css("display", "inline-block");
                    window.history.back();
                }, 1000);
            }
        });

    }

    $('#cmd').click(function () {
        pdfInvoice();
    });

    pdfInvoice();

</script>

</body>

</html>