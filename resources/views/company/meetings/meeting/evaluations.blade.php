@if(!$eva_exist)

    <div class="row mt-20">
        <div class="col-sm-9"></div>
        <div class="col-sm-3">
            @if($meeting->status==2 and permissions_meetings("add_evaluation"))
                <a href="#" class="btn btn-primary btn-xs btn-block" data-toggle="modal" data-target="#addEvaluationModal"><i class="fa fa-plus"></i> <span>إضافة تقييم</span></a>
            @endif
        </div>
    </div>

@endif

<div id="evaluation">

    <?php
        $all = collect(new \App\Evaluation);
        foreach ($evas as $eva)
            foreach ($eva->evaluations as $evaluation)
                $all->push($evaluation);
    ?>

    <div class="evaluation" style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

            <div class="row">

                <div class="col-sm-6">


                    <h5 style="margin: 20px 10px 0px 01px;color: #204c65">إجمالي التصويت</h5>
                    <div class="clearfix"></div>
                    <div class="mt-20" style="margin-right: 20px">
                        {{stars(sum_evaluations(@$all))}}
                    </div>
                </div>

                <div class="col-sm-6">
                    @foreach(@$items->reverse() as $item)
                        <div class="row mb-10">
                            <div class="col-sm-6"><strong>{{@$item->name}}</strong></div>
                            <?php $item->evaluations = collect(new \App\Evaluation); ?>
                            @foreach($all as $a) @if($a->item_id==$item->id) <?php $item->evaluations->push($a); ?> @endif @endforeach
                            <div class="col-sm-6">{{stars(sum_evaluations(@$item->evaluations))}}</div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>

    @foreach($evas as $eva)

        <div class="evaluation" style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

            <div class="row">

                @if($eva->user_id==$user->id)

                    <div class="evaluation-buttons">
                        <a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target="#editEvaluationModal"><i class="fa fa-edit"></i> <span>تعديل</span></a>
                        <span style="width: 10px"></span>
                        <a class="btn btn-danger btn-xs" href="{{route("company.meetings.delete_evaluation", ["company"=>@$company->slug, "id"=>$eva->id])}}" onclick="return confirm('هل أنت متأكد ؟')"><i class="fa fa-trash"></i> <span>حذف</span></a>
                    </div>

                    <div id="editEvaluationModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">تعديل التقييم</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route("company.meetings.update_evaluation", @$company->slug)}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$eva->id}}">

                                        @foreach(@$eva->evaluations as $evaluation)
                                            <div class="row">
                                                <div class="col-sm-12 mb-20">
                                                    <div class="col-sm-6">{{$evaluation->item->name}}</div>
                                                    <div class="col-sm-6" style="direction: ltr !important;">
                                                        <input id="input-id" name="{{$evaluation->id}}" value="{{$evaluation->num}}" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="sm" data-language="ar" required title="sdf">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                        <div class="row mt-30 mb-10">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                @endif

                <div class="col-sm-6">

                    @if(@$eva->user->image)
                        <img src="{{asset("storage/app/".$eva->user->image)}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                    @else
                        <img src="{{asset("public/noimage.png")}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                    @endif
                    <h5 class="pull-right" style="margin: 20px 10px 0px 01px;color: #204c65">{{@$eva->user->name}}</h5>
                    <div class="clearfix"></div>
                    <div class="mt-20" style="margin-right: 20px">
                        {{stars(sum_evaluations(@$eva->evaluations))}}
                    </div>
                </div>

                <div class="col-sm-6">
                    @foreach(@$eva->evaluations as $evaluation)
                        <div class="row mb-10">
                            <div class="col-sm-6"><strong>{{@$evaluation->item->name}}</strong></div>
                            <div class="col-sm-6">{{stars(@$evaluation->num)}}</div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>

    @endforeach

</div>
