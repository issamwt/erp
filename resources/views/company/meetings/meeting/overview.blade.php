<div id="buttons-wrapper">
    <div id="buttons-div">
        
        @include("company.meetings.meeting.buttons")
        
        <hr>

        <div class="row">
            <div class="col-sm-4">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>حالة الإجتماعات</label></div>
                    <div class="col-xs-8">
                        @if($meeting->status==0)
                            <strong class="alert alert-success alert-xs">جديد</strong>
                        @elseif($meeting->status==1)
                            <strong class="alert alert-info alert-xs">جاري</strong>
                        @elseif($meeting->status==2)
                            <strong class="alert alert-warning alert-xs">منته</strong>
                        @elseif($meeting->status==3)
                            <strong class="alert alert-danger alert-xs">ملغى</strong>
                        @else
                            <span class="alert alert-default">coming soon</span>
                        @endif
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>سلسلة إجتماعات</label></div>
                    <div class="col-xs-8">{{@$meeting->category->name}}</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>موضوع الإجتماع</label></div>
                    <div class="col-xs-8">{{$meeting->name}} (#{{$meeting->id}})</div>
                </div>
                <div class="row" style="margin-bottom: 0px">
                    <div class="col-xs-4"><label>مكان الإجتماع</label></div>
                    <div class="col-xs-8">{{@$meeting->location->name}}</div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-3"><label>التاريخ</label></div>
                    <div class="col-xs-9">{{$meeting->from_date}}</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-3"><label>الوقت</label></div>
                    <div class="col-xs-9">{{$meeting->from_time}}</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-3"><label>المدة</label></div>
                    <div class="col-xs-9"><span>{{floor($meeting->duration/60)}}</span> <span>ساعة</span> : <span>{{floor($meeting->duration%60)}}</span> <span>دقيقة</span></div>
                </div>
                <div class="row" style="margin-bottom: 0px">
                    <div class="col-xs-3"></div>
                    <div class="col-xs-9"></div>
                </div>
                <div class="row" style="margin-top: 10px">
                    {{--
                    <div class="col-xs-3"><b>تكرار كل</b></div>
                    <div class="col-xs-9">
                        @if($meeting->repeats==0)
                            <span>لا يوجد</span>
                        @elseif($meeting->repeats==1)
                            <span>يوم</span>
                        @elseif($meeting->repeats==2)
                            <span>أسبوع</span>
                        @elseif($meeting->repeats==3)
                            <span>شهر</span>
                        @else
                            <span>لا يوجد</span>
                        @endif
                    </div>
                    --}}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4">
                        <label class="choice @if($meeting->type==0) active @endif">
                            <i class="fa fa-check"></i><span>إجتماع عادي</span>
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <label class="choice @if($meeting->type==1) active @endif">
                            <i class="fa fa-times"></i><span>إجتماع مهم</span>
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <label class="choice @if($meeting->type==2) active @endif">
                            <i class="fa fa-exclamation"></i><span>إجتماع طارئ</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom: 10px"></div>
            <div class="col-sm-8">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-2"><label>طريقة الإجتماع</label></div>
                    <div class="col-xs-10">
                        @if($meeting->online==0)
                            <div>عن بعد</div>
                        @elseif($meeting->online==1)
                            <div>حضوريا</div>
                        @endif
                    </div>
                </div>
            </div>
            @if($meeting->online==1)
                <div class="col-sm-8">
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-xs-2"><label>رابط الغوغل ماب</label></div>
                        <div class="col-xs-10"><a style="text-decoration: underline !important;" href="{{$meeting->gmap_url}}" target="_blank">{{$meeting->gmap_url}}</a></div>
                    </div>
                </div>
            @endif
            <div class="col-sm-8">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-2"><label>تسجيل الإجتماع</label></div>
                    <div class="col-xs-10">
                        @if($meeting->record==0)
                            <div>لا</div>
                        @else
                            <div>نعم</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-sm-5">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>مدير الإجتماع</label></div>
                    <div class="col-xs-8">@if(@$meeting->manager){{@$meeting->manager->name}}@else<span>لا يوجد</span>@endif</div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>أمين الإجتماع</label></div>
                    <div class="col-xs-8">@if(@$meeting->amin){{@$meeting->amin->name}}@else<span>لا يوجد</span>@endif</div>
                </div>
            </div>
            <div class="col-sm-2"><div style="height: 80px; width: 2px; background: red;margin: 0px auto"></div></div>
            <div class="col-sm-5">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>الموظفين المدعوين</label></div>
                    <div class="col-xs-8">
                        @if(count(@$meeting->users)>0)
                            @foreach(@$meeting->users as $user) <span style="margin-left: 10px;"><b>[</b>{{@$user->name}}<b>]</b> </span>@endforeach
                        @else
                            <span>لا يوجد</span>
                        @endif
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4"><label>العملاء المدعوين</label></div>
                    <div class="col-xs-8">
                        @if(count(@$meeting->clients)>0)
                            @foreach(@$meeting->clients as $client) <span style="margin-left: 10px;"><b>[</b>{{@$client->name}}<b>]</b> </span>@endforeach
                        @else
                            <span>لا يوجد</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr style="margin-top: 30px;">

    </div>
</div>

<style>
    .choice{
        width: 90px;
        height: 90px;
        border: 1px solid #b9b9b9;
        border-radius: 4px;
        text-align: center;
        line-height: 35px;
        display: block;
        margin: 0px auto;
        padding-top: 15px;
    }
    .col-xs-4:nth-child(1) .choice{
        background: #60bd03;
    }
    .col-xs-4:nth-child(1) .choice.active{
        box-shadow: 0px 0px 3px 3px #60bd03;
        border-color: #458802;
    }
    .col-xs-4:nth-child(2) .choice{
        background: hotpink;
    }
    .col-xs-4:nth-child(2) .choice.active{
        box-shadow: 0px 0px 3px 3px hotpink;
        border-color: #b74b81;
    }
    .col-xs-4:nth-child(3) .choice{
        background: crimson;
    }
    .col-xs-4:nth-child(3) .choice.active{
        box-shadow: 0px 0px 3px 3px crimson;
        border-color: #a80f2e;
    }
    .choice .fa{
        font-size: 1.5em;
        background: #fff;
        padding: 4px;
        border-radius: 50%;
        border: 1px solid #b9b9b9;
        width: 31px;
        height: 31px;
        text-align: center;
        line-height: 21px;
    }
    .choice span{
        font-size: 0.6em;
        background: #fff;
        display: block;
        height: 17px;
        line-height: 18px;
        width: 70%;
        margin: 0px auto;
    }
</style>