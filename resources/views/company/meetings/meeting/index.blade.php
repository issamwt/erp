@extends("company.layouts.app", ["title"=>$meeting->name])

@section("content")

    <section class="content-header">

        <div class="pull-right">
            <h1>نظام إجتماعاتي</h1>
            <small>قائمة الإجتماعات > {{$meeting->name}}</small>
        </div>
        <div class="pull-left">
            @if($meeting->status==1)
                <div style="border: 1px solid #d5d5d5;width: auto;display: inline-block; width: 200px">
                    <div class="countdown" id="countdown0"></div>
                </div>
            @endif
        </div>
        <div class="clearfix"></div>

    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">

                    <div class="panel-heading">
                        <strong>{{$meeting->name}}</strong>
                        <a href="{{route("company.meetings.meetings", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>العودة للقائمة</span></a>
                        @if(permissions_meetings("update_meeting"))
                            <a href="{{route("company.meetings.edit_meeting", ["company"=>@$company->slug, "id"=>$meeting->id])}}" class="btn btn-default btn-xs pull-left" style="margin-left: 10px;"><i class="fa fa-edit"></i> <span>تعديل الإجتماع</span></a>
                        @endif
                    </div>

                    <div class="panel-body" id="meeting">

                        @include("company.meetings.meeting.overview")

                        <ul class="nav nav-tabs">
                            <li class="aa @if(!session('meeting_nav') or session('meeting_nav')==7) active @endif"><a data-toggle="tab" href="#menu7" data-num="7">المهام السابقة</a></li>
                            <li class="aa @if(session('meeting_nav')==1) active @endif"><a data-toggle="tab" href="#menu0" data-num="1">محاور الإجتماع</a></li>
                            <li class="aa @if(session('meeting_nav')==2) active @endif"><a data-toggle="tab" href="#menu1" data-num="2">المهام</a></li>
                            <li class="aa @if(session('meeting_nav')==3) active @endif"><a data-toggle="tab" href="#menu2" data-num="3">المرفقات</a></li>
                            <li class="aa @if(session('meeting_nav')==4) active @endif"><a data-toggle="tab" href="#menu3" data-num="4">التصويت</a></li>
                            <li class="aa @if(session('meeting_nav')==5) active @endif"><a data-toggle="tab" href="#menu4" data-num="5">تقييم الإجتماع</a></li>
                            <li class="aa @if(session('meeting_nav')==6) active @endif"><a data-toggle="tab" href="#menu5" data-num="6">المدعوين</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="menu7" class="tab-pane fade @if(!session('meeting_nav') or session('meeting_nav')==7) in active @endif">
                                @include("company.meetings.meeting.old_tasks")
                            </div>
                            <div id="menu0" class="tab-pane fade @if(session('meeting_nav')==1) in active @endif">
                                @include("company.meetings.meeting.axes")
                            </div>
                            <div id="menu1" class="tab-pane fade @if(session('meeting_nav')==2) in active @endif">
                                @include("company.meetings.meeting.tasks")
                            </div>
                            <div id="menu2" class="tab-pane fade @if(session('meeting_nav')==3) in active @endif">
                                @include("company.meetings.meeting.attachments")
                            </div>
                            <div id="menu3" class="tab-pane fade @if(session('meeting_nav')==4) in active @endif">
                                @include("company.meetings.meeting.votes")
                            </div>
                            <div id="menu4" class="tab-pane fade @if(session('meeting_nav')==5) in active @endif">
                                @include("company.meetings.meeting.evaluations")
                            </div>
                            <div id="menu5" class="tab-pane fade @if(session('meeting_nav')==6) in active @endif">
                                @include("company.meetings.meeting.invited")
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    @if(!$eva_exist)

        <div id="addEvaluationModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">إضافة تقييم</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{route("company.meetings.save_evaluation", @$company->slug)}}" method="post">
                            @csrf
                            <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

                            @foreach($items->reverse() as $item)
                                <div class="row">
                                    <div class="col-sm-12 mb-20">
                                        <div class="col-sm-6">{{$item->name}}</div>
                                        <div class="col-sm-6" style="direction: ltr !important;">
                                            <input id="input-id" name="{{$item->id}}" value="0" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="sm" data-language="ar" required title="sdf">
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="row mt-30 mb-10">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    @if($user->id==$meeting->manager->id)
                                        <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-save"></i> <span>إنهاء الإجتماع</span></button>
                                    @else
                                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                    @endif
                                </div>
                                <div class="col-sm-4"></div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

    @endif

@endsection

@section("scripts")

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.5/css/star-rating.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.5/js/star-rating.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.5/js/locales/ar.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>

    <style>
        .alert-xs{font-size: 0.9em;
            padding: 1px 3px !important;
            display: inline-block;
            width: 70px;
            margin: 0px 10px 0px 0px;
            border-radius: 0px !important;
            text-align: center;
        }
        #meeting{
            padding-bottom: 100px;
        }
        #meeting .tab-content{
            min-height: 300px;
        }
        #meeting .nav.nav-tabs{
            margin-top: 30px;
        }
        #meeting .nav-tabs>li, #meeting .nav-tabs>li a{
            border-radius: 0px;
            font-size: 1.1em;
        }
        #comments .comment{
            background: #eee;
            border: 1px solid #d0d0d0;
            padding: 15px;
        }
        .num{
             width: 18px;
             height: 18px;
             background: #fff;
             border: 1px solid #eee;
             border-radius: 50%;
             text-align: center;
             line-height: 17px;
             font-size: 0.8em;
             display: inline-block;
             color: #333;
             font-style: normal;
             margin-left: 3px;
         }
        .icheck>label{
            padding-left: 15px;
            padding-right: 0px;
        }
        .btnn .fa{
            width: 20px;
            height: 20px;
            line-height: 19px;
            text-align: center;
            background: #fff;
            border-radius: 50%;
            border: 1px solid #eee;
            color: #333;
        }
        #evaluation .evaluation:nth-child(2n){
            background: #f8f8f8;
        }
        #evaluation .evaluation{
            position: relative;
        }
        #evaluation .evaluation-buttons{
            position: absolute;
            z-index: 1;
            left: 10px;
            top: 10px;
        }
        #sortable li{
            cursor: move;
            background: #f5f5f5;
        }
        #sortable li:hover{
            background: #fafafa;
        }
        .countdown{
            display: none;
            width: 200px;
            direction: ltr;
        }
        .countdown span{
            display: inline-block;
            text-align: center;
            font-size: 20px;
            font-weight: bold;
            font-family: monospace;
            height: 30px;
            line-height: 30px;
            width: 5%;
            transform: scaleY(1.1);
            color: #fff;
            background: linear-gradient(to top, #1c8732 0%, #64c900 50%,#64c900 50%,#1c8732 100%);
        }
        .countdown span.x{
            width: 30%;
        }
        .content-header h1{
            margin: 0;
            font-size: 24px;
        }
        .countdown.countdown1 span{
            background: linear-gradient(to top, #d47501 0%, #ffab38 50%,#ffab38 50%,#d47501 100%);
        }
        .countdown.countdown2 span{
            background: linear-gradient(to top, #ff0000 0%, #ff978e 50%,#ff978e 50%,#ff0000 100%);
        }
        .countdown.countdown3 span{
            background: linear-gradient(to top, #000000 0%, #606060 50%,#606060 50%,#000000 100%);
        }
        #votes-wrapper{
            border: 1px solid #eee;
            padding: 20px;
            margin-top: 25px
        }
    </style>

    <script>

        $(function () {

            $("#input-id").rating({language:"ar"});

            $(document).on("click", ".aa > a", function () {
                var num = $(this).data("num");
                if(num){
                    $.post("{{route("company.meetings.meeting_nav", @$company->slug)}}", {_token:"{{csrf_token()}}", num:num}, function () {});
                }
            });

            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });

            @if($meeting->status==1 and permissions_meetings("order_axis"))
            var array = [];
            $( "#sortable" ).sortable({
                stop: function( event, ui ) {
                    $("#sortable li").each(function(idx, li) {
                        var id = $(li).data("id");
                        array[id] = idx;
                    });
                    console.log(array);
                    var url = "{{route("company.meetings.reorder_axes", ["company"=>@$company->slug, "id"=>@$meeting->id])}}";
                    $.post(url, {_token: "{{csrf_token()}}", data:array}, function () {

                    });
                    array = [];
                }
            });
            @endif

            $(document).on("change", ".type_options", function (e) {
                console.log($(this).closest("form"));
                var type = $(this).val();
                if(type==1 || type==2){
                    $(this).closest("form").find("#options1").css("display", "block");
                    $(this).closest("form").find("#options2").css("display", "none");
                }else if(type==3){
                    $(this).closest("form").find("#options1").css("display", "none");
                    $(this).closest("form").find("#options2").css("display", "block");
                    console.log($(this).closest("form").find(".opzn"));
                    $(this).closest("form").find(".opzn").iCheck('check');
                    $(this).closest("form").find(".opzn").prop('checked', true);
                }else{
                    $(this).closest("form").find("#options1").css("display", "none");
                    $(this).closest("form").find("#options2").css("display", "none");
                }
            });

            $(document).on("click", ".btn-addoption", function (e) {
                e.preventDefault();
                var bloogeez = $(this).closest('.bloogeez');
                bloogeez.append('<div class="row mt-10">\n' +
                    '                    <div class="col-sm-8">\n' +
                    '                        <input type="text" class="form-control" name="aaa[]" >\n' +
                    '                    </div>\n' +
                    '                    <div class="col-sm-2">\n' +
                    '                        <a class="btn btn-primary btn-block btn-addoption" href="#"><i class="fa fa-plus"></i> <span>إضافة</span></a>\n' +
                    '                    </div>\n' +
                    '                    <div class="col-sm-2">\n' +
                    '                        <a class="btn btn-danger btn-block btn-deleteoption" href="#"><i class="fa fa-trash"></i> <span>حذف</span></a>\n' +
                    '                    </div>\n' +
                    '                </div>');
            });

            $(document).on("click", ".btn-deleteoption", function (e) {
                e.preventDefault();
                var row = $(this).closest(".row");
                row.remove();
            });

            $(document).on("change", ".attendancet", function (e) {
                var type = $(this).val();
                if(type==0){
                    $(this).closest("form").find("#notet").css("display", "none");
                }else{
                    $(this).closest("form").find("#notet").css("display", "block");
                }
            });

            $(document).on("change", ".attendancet2", function (e) {
                var type = $(this).val();
                if(type==2 || type==5 || type==3){
                    $(this).closest("form").find("#notet2").css("display", "block");
                }else{
                    $(this).closest("form").find("#notet2").css("display", "none");
                }
            });

            // OLD TASKS

            $(document).on("change", ".select2Status, .select2User, .select2Meeting, .select2Categories, .dateFrom, .dateTo", function () {
                var status  = $(".select2Status").val();
                var meeting = $(".select2Meeting").val();
                var category = $(".select2Categories").val();
                var user    = parseInt($(".select2User").val());
                var dateFrom= $(".dateFrom").val();
                var dateTo  = $(".dateTo").val();

                $("#dataTable tbody tr").fadeOut();
                $("#dataTable tbody tr").filter(function (i, elem) {
                    var statusT = $(elem).data("status");
                    var statusX = true;
                    if(status!="all")
                        if(statusT!=status)
                            statusX = false;

                    var meetingT = $(elem).data("meeting");
                    var meetingX = true;
                    if(meeting!="all")
                        if(meetingT!=meeting)
                            meetingX = false;

                    var categoryT = $(elem).data("category");
                    var categoryX = true;
                    if(category!="all")
                        if(categoryT!=category)
                            categoryX = false;

                    var usersP = $(elem).data("users");
                    usersP = usersP.split(";")
                    var userX = false;
                    if(user)
                        $.each(usersP, function( index, value ) {
                            if(parseInt(user)==parseInt(value))
                                userX=true;
                        });
                    else
                        userX=true;

                    var dateT = $(elem).data("date");
                    var dateX = true;
                    var dateT = new Date(dateT);
                    var d1    = new Date(dateFrom);
                    var d2    = new Date(dateTo);

                    if(dateFrom!="" && dateTo==""){
                        if(dateT<d1)
                            dateX = false;
                    } else if(dateFrom=="" && dateTo!=""){
                        if(dateT>d2)
                            dateX = false;
                    } else if(dateFrom!="" && dateTo!=""){
                        if(dateT>d2 || dateT<d1)
                            dateX = false;
                    } else if(dateFrom=="" && dateTo==""){
                        dateX = true;
                    }

                    return statusX && categoryX && meetingX && userX && dateX;
                }).fadeIn();
            });

            // TASKS

            $(document).on("submit", "#save_task", function (e) {
                e.preventDefault();
                var form = $(this);
                $("button", form).prop("disabled", true);
                $( "#task-wrapper" ).css("opacity", "0.7");
                $("#task-wrapper").html($("#loading-wrapper").html());
                $.post('{{route("company.meetings.save_task", @$company->slug)}}', $(form).serialize(), function () {
                    $("#task-wrapper").load(window.location + " #task-div", function () {
                        goNotif("success", "تم إضافة المهمة بنجاح !");
                        reloading();
                        $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                        $(form).trigger("reset");
                        $("button", form).prop("disabled", false);
                    });
                });
            });

            $(document).on("submit", ".update_task", function (e) {
                e.preventDefault()
                var form = $(this);
                var id = $(this).data("id");
                $("#editTaskeModal"+id).modal("hide");
                setTimeout(function () {
                    $("button", form).prop("disabled", true);
                    $( "#task-wrapper" ).css("opacity", "0.7");
                    $("#task-wrapper").html($("#loading-wrapper").html());
                    $.post('{{route("company.meetings.update_task", @$company->slug)}}', $(form).serialize(), function () {
                        $("#task-wrapper").load(window.location + " #task-div", function () {
                            goNotif("success", "تم تعديل المهمة بنجاح !");
                            reloading();
                            $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                            $("button", form).prop("disabled", false);
                        });
                    });
                }, 1000);
            });

            $(document).on("submit", ".update_old_task", function (e) {
                e.preventDefault()
                var form = $(this);
                var id = $(this).data("id");
                $("#editTaskeModal"+id).modal("hide");
                setTimeout(function () {
                    $("button", form).prop("disabled", true);
                    $( "#oldtask-wrapper" ).css("opacity", "0.7");
                    $("#oldtask-wrapper").html($("#loading-wrapper").html());
                    $.post('{{route("company.meetings.update_task", @$company->slug)}}', $(form).serialize(), function () {
                        $("#oldtask-wrapper").load(window.location + " #oldtask-div", function () {
                            goNotif("success", "تم تعديل المهمة بنجاح !");
                            reloading();
                            $( "#oldtask-wrapper" ).animate({opacity: 1,}, 1000);
                            $("button", form).prop("disabled", false);
                        });
                    });
                }, 1000);
            });

            $(document).on("click", ".delete_task", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    $( "#task-wrapper" ).css("opacity", "0.7");
                    $("#task-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.delete_task", @$company->slug)}}/'+id, function () {
                        $("#task-wrapper").load(window.location + " #task-div", function () {
                            goNotif("success", "تم حذف المهمة بنجاح !");
                            reloading();
                            $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            $(document).on("click", ".delete_oldtask", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    $( "#oldtask-wrapper" ).css("opacity", "0.7");
                    $("#oldtask-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.delete_task", @$company->slug)}}/'+id, function () {
                        $("#oldtask-wrapper").load(window.location + " #oldtask-div", function () {
                            goNotif("success", "تم حذف المهمة بنجاح !");
                            reloading();
                            $( "#oldtask-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            $(document).on("click", ".change_task_status", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    var status = $(this).data("status");
                    $( "#task-wrapper" ).css("opacity", "0.7");
                    $("#task-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.change_task_status", @$company->slug)}}/'+id+'/'+status, function () {
                        $("#task-wrapper").load(window.location + " #task-div", function () {
                            goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                            reloading();
                            $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            $(document).on("click", ".change_oldtask_status", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    var status = $(this).data("status");
                    $( "#oldtask-wrapper" ).css("opacity", "0.7");
                    $("#oldtask-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.change_task_status", @$company->slug)}}/'+id+'/'+status, function () {
                        $("#oldtask-wrapper").load(window.location + " #oldtask-div", function () {
                            goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                            reloading();
                            $( "#oldtask-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            // votes

            $(document).on("submit", "#save_vote", function (e) {
                e.preventDefault();
                var form = $(this);
                $("button", form).prop("disabled", true);
                $( "#votes-wrapper" ).css("opacity", "0.7");
                $("#votes-wrapper").html($("#loading-wrapper").html());
                $.post('{{route("company.meetings.save_vote", @$company->slug)}}', $(form).serialize(), function () {
                    $("#votes-wrapper").load(window.location + " #votes-div", function () {
                        goNotif("success", "تم إضافة التصويت بنجاح !");
                        reloading();
                        $( "#votes-wrapper" ).animate({opacity: 1,}, 1000);
                        $(form).trigger("reset");
                        $("button", form).prop("disabled", false);
                    });
                });
            });

            $(document).on("submit", ".update_vote", function (e) {
                e.preventDefault()
                var form = $(this);
                var id = $(this).data("id");
                $("#editVoteModal"+id).modal("hide");
                setTimeout(function () {
                    $("button", form).prop("disabled", true);
                    $( "#votes-wrapper" ).css("opacity", "0.7");
                    $("#votes-wrapper").html($("#loading-wrapper").html());
                    $.post('{{route("company.meetings.update_vote", @$company->slug)}}', $(form).serialize(), function () {
                        $("#votes-wrapper").load(window.location + " #votes-div", function () {
                            goNotif("success", "تم تعديل التصويت بنجاح !");
                            reloading();
                            $( "#votes-wrapper" ).animate({opacity: 1,}, 1000);
                            $("button", form).prop("disabled", false);
                        });
                    });
                }, 1000);
            });

            $(document).on("click", ".delete_vote", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    $( "#votes-wrapper" ).css("opacity", "0.7");
                    $("#votes-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.delete_vote", @$company->slug)}}/'+id, function () {
                        $("#votes-wrapper").load(window.location + " #votes-div", function () {
                            goNotif("success", "تم حذف التصويت بنجاح !");
                            reloading();
                            $( "#votes-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            $(document).on("click", ".start_vote", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    var status = $(this).data("status");
                    //console.log('{{route("company.meetings.start_vote", @$company->slug)}}/'+id+'/'+status);
                    $( "#votes-wrapper" ).css("opacity", "0.7");
                    $("#votes-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.start_vote", @$company->slug)}}/'+id+'/'+status, function () {
                        setTimeout(function () {
                            $("#votes-wrapper").load(window.location + " #votes-div", function () {
                                goNotif("success", "تم تغير حالة التصويت بنجاح !");
                                reloading();
                                $( "#votes-wrapper" ).animate({opacity: 1,}, 1000);
                            });
                        }, 1500);
                    });
                }

            });

            $(document).on("submit", ".vote_vote", function (e) {
                e.preventDefault()
                var form = $(this);
                var id = $(this).data("id");
                $("#voteVoteModal1"+id).modal("hide");
                setTimeout(function () {
                    $("button", form).prop("disabled", true);
                    $( "#votes-wrapper" ).css("opacity", "0.7");
                    $("#votes-wrapper").html($("#loading-wrapper").html());
                    $.post('{{route("company.meetings.vote_vote", @$company->slug)}}', $(form).serialize(), function () {
                        $("#votes-wrapper").load(window.location + " #votes-div", function () {
                            goNotif("success", "تم التصويت بنجاح !");
                            reloading();
                            $( "#votes-wrapper" ).animate({opacity: 1,}, 1000);
                            $("button", form).prop("disabled", false);
                        });
                    });
                }, 1000);
            });

            /*setInterval(function () {
                $("#votes-wrapper .modal").modal("hide");
                setTimeout(function () {
                    $( "#votes-wrapper" ).css("opacity", "0.7");
                    $("#votes-wrapper").html($("#loading-wrapper").html());
                    $("#votes-wrapper").load(window.location + " #votes-div", function () {
                        reloading();
                        $( "#votes-wrapper" ).animate({opacity: 1,}, 1000);
                    });
                }, 1000);
            }, 60000);*/

        });

        $.fn.countdown = function(diff) {
            var elem = this;
            console.log(diff);
            var countdown1 = {{(@$settings["countdown1".$company->id]->value)?intval(@$settings["countdown1".$company->id]->value):0}};
            var countdown2 = {{(@$settings["countdown2".$company->id]->value)?intval(@$settings["countdown2".$company->id]->value):0}};
            $(this).html('<span class="h x">00</span><span>:</span><span class="m x">00</span><span>:</span><span class="s x">00</span>');
            setInterval(function () {
                $(elem).css("display", "block");
                --diff;

                var h = (diff>=0)?Math.floor(diff/3600):Math.round(diff/3600);
                var m = (diff>=0)?Math.floor((diff-h*3600)/60):Math.round((diff-h*3600)/60);
                var s = (diff>=0)?Math.floor((diff-h*3600-m*60)):Math.round((diff-h*3600-m*60));

                if(countdown1!=0 && diff<=(countdown1*60))
                    if(!$(elem).hasClass("countdown1"))
                        $(elem).addClass("countdown1");

                if(countdown2!=0 && diff<=(countdown2*60))
                    if(!$(elem).hasClass("countdown2"))
                        $(elem).addClass("countdown2");

                if(diff<0)
                    if(!$(elem).hasClass("countdown3"))
                        $(elem).addClass("countdown3");

                //elem.html(pad(h)+":"+pad(m)+":"+pad(s));
                $(".h", elem).html(pad(h));
                $(".m", elem).html(pad(m));
                $(".s", elem).html(pad(s));
            }, 1000);
        };

        $.fn.countup = function(diff) {
            var elem = this;
            $(this).html('<span class="h x">00</span><span>:</span><span class="m x">00</span><span>:</span><span class="s x">00</span>');
            setInterval(function () {
                $(elem).css("display", "block");
                ++diff;

                var h = Math.floor(diff/3600);
                var m = Math.floor((diff-h*3600)/60);
                var s = Math.floor((diff-h*3600-m*60));

                //elem.html(pad(h)+":"+pad(m)+":"+pad(s));
                $(".h", elem).html(pad(h));
                $(".m", elem).html(pad(m));
                $(".s", elem).html(pad(s));
            }, 1000);
        };

        function pad(num) {
            size = 2;
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        }

    </script>

    @if($meeting->status==1)
        <?php
            $now    = Carbon\Carbon::now();
            $start  = $meeting->start_at;
            $diffx   = $now->diffInMinutes($start);
            $dif   = intval($meeting->duration)-intval($diffx);
            $diff   = $dif*60;
            ?>
        <script>
            $('#countdown0').countdown({{$diff}});
        </script>

    @endif

    <script>

    </script>

    @if(@$zoom_time)
        <?php
        $now    = Carbon\Carbon::now();
        $start  = \Carbon\Carbon::parse($zoom_time);
        $start->setTimezone("Asia/Riyadh");
        $diffx   = $now->diffInMinutes($start);
        $dif   = intval($diffx);
        $diff   = $dif*60;
        ?>
        <script>
            $('#countdownx').countup({{$diff}});
        </script>

    @endif

    @foreach($axes as $axis)
        @if($axis->status==1)
            <?php
            $now    = Carbon\Carbon::now();
            $start  = $axis->start_at;
            $difference   = $now->diffInSeconds($start);
            $diff   = intval((($axis->hours*60)+$axis->minutes)*60)-intval($difference);
            ?>
            <script>
                /*
                console.log("{{$now}}");
                console.log("{{$start}}");
                console.log("{{$difference}}");
                console.log("{{$diff}}");
                */
                $('#countdown{{$axis->id}}').countdown({{$diff}});
            </script>
        @endif
    @endforeach

@endsection