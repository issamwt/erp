@if(@$zoom_url)
    <h3 class="text-center">رابط الإجتماع : </h3>
    <h3 class="text-center mb-20"><a target="_blank" style="text-decoration: underline;" href="{{@$zoom_url}}">{{@$zoom_url}}</a></h3>
    @if(@$zoom_time)
        <div class="countdown mb-50" id="countdownx" style="margin: 0px auto 30px;"></div>
    @endif
@endif

<div class="row">
    @if($meeting->status==0)
        @if(permissions_meetings("accept_meeting"))
            <div class="col-sm-3">
                <a class="btn btn-success btn-block" href="{{route("company.meetings.accept_meeting", ["company"=>@$company->slug, "id"=>$meeting->id])}}" onclick="return confirm('هل أنت متأكد ؟')"> بدء الإجتماع</a>
            </div>
        @endif
        @if(permissions_meetings("cancel_meeting"))
            <div class="col-sm-3">
                <a class="btn btn-info btn-block" href="{{route("company.meetings.cancel_meeting", ["company"=>@$company->slug, "id"=>$meeting->id])}}" onclick="return confirm('هل أنت متأكد ؟')"> إلغاء الإجتماع</a>
            </div>
        @endif
        @if(permissions_meetings("delay_meeting"))
            <div class="col-sm-3">
                <a class="btn btn-primary btn-block" href="#" data-toggle="modal" data-target="#delayMeetingModal"> تأجيل الإجتماع</a>
            </div>
        @endif
        @if(permissions_meetings("notify_meeting"))
            <div class="col-sm-3">
                <a class="btn btn-warning btn-block" href="#" data-toggle="modal" data-target="#notifyMeetingModal"> تذكير الحضور</a>
            </div>
        @endif
    @elseif($meeting->status==1)
        @if(permissions_meetings("accept_meeting"))
            <div class="col-sm-2">
                <a class="btn btn-success btn-block" href="{{route("company.meetings.zoom_meeting", ["company"=>@$company->slug, "id"=>$meeting->id])}}" onclick="return confirm('هل أنت متأكد ؟')"> تشغيل zoom</a>
            </div>
        @endif
        @if(permissions_meetings("cancel_meeting"))
            <div class="col-sm-2">
                <a class="btn btn-info btn-block" href="{{route("company.meetings.cancel_meeting", ["company"=>@$company->slug, "id"=>$meeting->id])}}" onclick="return confirm('هل أنت متأكد ؟')"> إلغاء الإجتماع</a>
            </div>
        @endif
        @if(permissions_meetings("delay_meeting"))
            <div class="col-sm-2">
                <a class="btn btn-primary btn-block" href="#" data-toggle="modal" data-target="#delayMeetingModal"> تأجيل الإجتماع</a>
            </div>
        @endif
        @if(permissions_meetings("finish_meeting"))
            <div class="col-sm-2">
                <a class="btn btn-danger btn-block" href="{{route("company.meetings.finish_meeting", ["company"=>@$company->slug, "id"=>$meeting->id])}}" onclick="return confirm('هل أنت متأكد ؟')"> إنهاء الإجتماع</a>
            </div>
        @endif
        @if(permissions_meetings("notify_meeting"))
            <div class="col-sm-2">
                <a class="btn btn-warning btn-block" href="#" data-toggle="modal" data-target="#notifyMeetingModal"> تذكير الحضور</a>
            </div>
        @endif
    @elseif($meeting->status==2)
        <div class="col-sm-12"><h4>هذا الإجتماع منته</h4></div>
    @elseif($meeting->status==3)
        <div class="col-sm-12"><h4>هذا الإجتماع ملغي</h4></div>
    @endif


    @if($meeting->status==0 or $meeting->status==1)

            @if(permissions_meetings("delay_meeting"))
            <div id="delayMeetingModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">تأجيل الجتماع</h4>
                        </div>
                        <div class="modal-body">

                            <form class="row" action="{{route("company.meetings.delay_meeting", @$company->slug)}}" method="post">
                                @csrf

                                <input type="hidden" value="{{$meeting->id}}" name="id">

                                <div class="col-sm-12">

                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-3"><label>التاريخ</label></div>
                                        <div class="col-xs-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker" autocomplete="off" name="from_date" value="{{$meeting->from_date}}" id="from_date" required style="text-align: right">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-2">من</div>
                                        <div class="col-xs-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" autocomplete="off" name="from_time" value="{{$meeting->from_time}}" id="from_time" required style="text-align: right">
                                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-2">إلى</div>
                                        <div class="col-xs-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" autocomplete="off" name="to_time" value="{{$meeting->to_time}}" id="to_time" required style="text-align: right">
                                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary" style="min-width: 120px;"><i class="fa fa-save"></i> <span>تأجيل الإجتماع</span></button>
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
            @endif

            @if(permissions_meetings("notify_meeting"))
            <div id="notifyMeetingModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">تنبيه الحضور</h4>
                        </div>
                        <div class="modal-body">

                            <form action="{{route("company.meetings.notify_meeting", @$company->slug)}}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{$meeting->id}}">

                                <div class="form-group">
                                    <label>لمن سترسل التنبيه ؟</label>
                                    <select name="users[]" class="form-control select2x" multiple required>
                                        <?php $userxx=[]; ?>
                                        <option value="0">الجميع</option>
                                        @foreach($users as $u)
                                            {{--@if($user->id!=$u->id and !in_array($u->id, $userxx))--}}
                                                <?php array_push($userxx, $u->id); ?>
                                                <option value="{{$u->id}}">{{$u->name}}</option>
                                            {{--@endif--}}
                                        @endforeach
                                    </select>
                                </div>

                                {{--
                                <div class="form-group">
                                    <label>طريقة التنبيه</label>
                                    <select name="methods[]" class="form-control select2x" multiple required>
                                        <option value="0">رسالة بريد إلكتروني</option>
                                        <option value="1">رسالة جوال</option>
                                        <option value="2">إشعار على البرنامج</option>
                                    </select>
                                </div>
                                --}}

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" style="min-width: 100px"><i class="pull-right fa fa-bell-o"></i> <span>إرسال التنبيه</span></button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
            @endif

    @endif

</div>