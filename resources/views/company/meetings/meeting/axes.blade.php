@if(permissions_meetings("add_axis"))
<div style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

    <form action="{{route("company.meetings.save_axis", @$company->slug)}}" method="post">
        @csrf
        <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

        <div class="row mb-15">
            <div class="col-sm-2"><strong>إسم المحور <span class="red">*</span></strong></div>
            <div class="col-sm-7"><input type="text" class="form-control" name="name" required></div>
        </div>

        <div class="row mb-15">
            <div class="col-sm-2"><strong>المتحدث <span class="red">*</span></strong></div>
            <div class="col-sm-7">
                <select class="form-control" name="speaker_id" required>
                    <option value="">إختر</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mb-15">
            <div class="col-sm-2"><strong>مدة النقاش</strong></div>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-2">ساعة <span class="red">*</span></div>
                    <div class="col-sm-4"><select class="form-control" required name="hours">@for($i=0;$i<=24;$i++)<option value="{{$i}}">{{$i}}</option>@endfor</select></div>
                    <div class="col-sm-2">دقيقة <span class="red">*</span></div>
                    <div class="col-sm-4"><select class="form-control" required name="minutes">@for($i=0;$i<=60;$i++)<option value="{{$i}}">{{$i}}</option>@endfor</select></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> <span>إضافة</span></button>
            </div>
        </div>

    </form>

</div>
@endif

@if(count($axes)>0)

    <ul  id="sortable" style="list-style: none; margin: 0px; padding: 0px">

    @foreach($axes as $axis)

        <li data-orderx="{{$axis->orderx}}" data-id="{{$axis->id}}" style="border: 1px solid #eee; padding: 10px; margin-top: 25px;">

            <div class="row">

                <strong class="col-sm-5">
                    <span class="pull-right">{{$axis->name}}</span>
                    <span class="pull-left">
                        @if($meeting->status==1)
                            @if($axis->status==1)
                                <div style="border: 1px solid #d5d5d5;width: auto;display: inline-block; width: 200px">
                                    <div class="countdown" id="countdown{{$axis->id}}"></div>
                                </div>
                            @endif
                        @endif
                    </span>
                    <div class="clearfix"></div>
                    <hr style="margin: 10px 0px">
                    <div class="row">
                        <div class="col-sm-6"><i class="fa fa-user"></i> <span>{{@$axis->speaker->name}}</span></div>
                        <div class="col-sm-6"><i class="fa fa-clock-o"></i> <span>ساعة : </span><span>{{$axis->hours}}</span> <span style="padding-right: 20px;">دقيقة : </span> <span>{{$axis->minutes}}</span></div>
                    </div>
                </strong>

                <div class="col-sm-1">
                    @if($axis->status==0)
                        <div class="mt-15 text-center" style="color:#367fa9"><b>جديد</b></div>
                    @elseif($axis->status==1)
                        <div class="mt-15 text-center" style="color:#e08e0b"><b>جاري</b></div>
                    @elseif($axis->status==2)
                        <div class="mt-15 text-center" style="color:#00a65a"><b>منجز</b></div>
                    @elseif($axis->status==3)
                        <div class="mt-15 text-center" style="color:#d73925"><b>ملغى</b></div>
                    @endif
                </div>

                <div class="col-sm-6">
                    <a class="mt-15 btn-xs btn btn-info btn-50" href="#" data-toggle="modal" data-target="#AxisComments{{$axis->id}}" data-toggle="tooltip" title="التعليقات"><i class="num">{{count($axis->comments)}}</i> <span class="fa fa-comment"></span></a>
                    <a class="mt-15 btn-xs btn btn-success btn-50" href="#" data-toggle="modal" data-target="#AxisAttachments{{$axis->id}}" data-toggle="tooltip" title="الملفات"><i class="num">{{count($axis->attachments)}}</i> <span class="fa fa-file"></span></a>
                    @if(permissions_meetings("update_axis"))
                        <a class="mt-15 btn-xs btn btn-warning btn-50" href="#" data-toggle="modal" data-target="#AxisEdit{{$axis->id}}" data-toggle="tooltip" title="التعديل"><span class="fa fa-edit"></span></a>
                        <a class="mt-15 btn-xs btn btn-primary btn-50" href="{{route("company.meetings.cancel_axis", ["company"=>@$company->slug, "id"=>$axis->id])}}" data-toggle="tooltip" title="الإلغاء" onclick="return confirm('هل أنت متأكد ؟')"><span class="fa fa-trash"></span></a>
                        <a class="mt-15 btn-xs btn btn-default btn-50" href="#" data-toggle="modal" data-target="#AxisDelay{{$axis->id}}" data-toggle="tooltip" title="التأجيل"><span class="fa fa-reply"></span></a>
                    @else
                        <a class="mt-15 btn-xs btn btn-warning btn-50 disabled" href="#" data-toggle="tooltip" title="التعديل"><span class="fa fa-edit"></span></a>
                        <a class="mt-15 btn-xs btn btn-primary btn-50 disabled" data-toggle="tooltip" title="الإلغاء"><span class="fa fa-trash"></span></a>
                        <a class="mt-15 btn-xs btn btn-default btn-50 disabled" data-toggle="tooltip" title="التأجيل"><span class="fa fa-reply"></span></a>
                    @endif
                    @if(permissions_meetings("delete_axis"))
                        <a class="mt-15 btn-xs btn btn-danger btn-50" href="{{route("company.meetings.delete_axis", ["company"=>@$company->slug, "id"=>$axis->id])}}" onclick="return confirm('هل أنت متأكد ؟')" data-toggle="tooltip" title="الحذف"><span class="fa fa-remove"></span></a>
                    @else
                        <a class="mt-15 btn-xs btn btn-danger btn-50 disabled" data-toggle="tooltip" title="الحذف"><span class="fa fa-remove"></span></a>
                    @endif
                    @if(permissions_meetings("start_finish_axis") and $meeting->status==1 and $axis->status==1)
                        <a class="mt-15 btn-xs btn btn-success btn-50 pull-left" href="{{route("company.meetings.finish_axis", ["company"=>@$company->slug, "id"=>$axis->id])}}" onclick="return confirm('هل أنت متأكد ؟')" data-toggle="tooltip" title="الإنهاء"><span class="fa fa-thumbs-down"></span></a>
                    @else
                        <a class="mt-15 btn-xs btn btn-success btn-50 pull-left disabled" href="#" data-toggle="tooltip" title="الإنهاء"><span class="fa fa-thumbs-down"></span></a>
                    @endif
                    @if(permissions_meetings("start_finish_axis") and $meeting->status==1 and $axis->status==0)
                        <a class="mt-15 ml-10 btn-xs btn btn-warning btn-50 pull-left" href="{{route("company.meetings.start_axis", ["company"=>@$company->slug, "id"=>$axis->id])}}" onclick="return confirm('هل أنت متأكد ؟')" data-toggle="tooltip" title="التشغيل"><span class="fa fa-thumbs-up"></span></a>
                    @else
                        <a class="mt-15 ml-10 btn-xs btn btn-warning btn-50 pull-left disabled" href="#" data-toggle="tooltip" title="التشغيل"><span class="fa fa-thumbs-up"></span></a>
                    @endif
                </div>


            </div>

        </li>

    @endforeach

    </ul>

@else
    <div style="border: 1px solid #eee; padding: 20px; margin-top: 25px">
        <h4>لا توجد محاور إجتماع حاليا !</h4>
    </div>
@endif



@foreach($axes as $axis)

    <div id="AxisDelay{{$axis->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">تأجيل المحور</h4>
                </div>
                <div class="modal-body">

                    <form action="{{route("company.meetings.delay_axis", ["company"=>@$company->slug, "id"=>$axis->id])}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label>إسم المحور</label>
                            <input class="form-control" type="text" disabled value="{{$axis->name}}">
                        </div>

                        <div class="form-group">
                            <label>تأجيل إلى الإجتماع <span class="red">*</span></label>
                            <select class="form-control" required name="meeting_id">
                                <option value="">إختر</option>
                                @foreach($meetings as $m)
                                    <option value="{{$m->id}}">{{$m->name}} (#{{$m->id}})</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <span>حفظ</span></button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div id="AxisComments{{$axis->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">التعليقات</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-4">{{$axis->name}}</div>
                        <div class="col-sm-4"><i class="fa fa-user"></i> <span>{{@$axis->speaker->name}}</span></div>
                        <div class="col-sm-4"><i class="fa fa-clock-o"></i> <span>ساعة : </span><span>{{$axis->hours}}</span> <span style="padding-right: 20px;">دقيقة : </span> <span>{{$axis->minutes}}</span></div>
                        <div class="col-sm-12"><hr></div>
                    </div>
                    
                    @if(permissions_meetings("add_comment_axis"))
                    <form class="row" action="{{route("company.meetings.save_axis_comment", @$company->slug)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="axis_id" value="{{$axis->id}}">

                        <div class="col-sm-12">
                            <h4 class="mt-0">إضافة تعليق : </h4>
                            <div class="form-group mb-10">
                                <label>نص التعليق <span class="red">*</span></label>
                                <textarea class="form-control" name="texte" required rows="4"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-group mb-10">
                                <label>ملف مرفق</label>
                                <input type="file" class="form-control" name="attachment" accept=".doc,.docx,application/msword,image/*,application/pdf">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group mb-10">
                                <label style="opacity: 0;">ملف مرفق</label>
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> <span>إضافة</span></button>
                            </div>
                        </div>

                        <div class="col-sm-12"><hr></div>

                    </form>
                    @endif

                    <div class="row mb-10">
                        <div class="col-sm-12" id="comments">
                            <h4 class="mt-0 mb-15">قائمة التعليقات : </h4>
                            @if(count($axis->comments)>0)
                                @foreach($axis->comments as $comment)
                                    <div class="comment">
                                        @if(@$comment->user->image)
                                            <img src="{{asset("storage/app/".$comment->user->image)}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                                        @else
                                            <img src="{{asset("public/noimage.png")}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                                        @endif
                                        <h4 class="pull-right" style="margin: 20px 10px 0px 01px;color: #204c65">{{@$comment->user->name}}</h4>
                                        <div class="clearfix"></div>
                                        <p class="mt-10 text-justify">{{$comment->texte}}</p>
                                        @if(count($comment->attachments)>0)
                                            @foreach($comment->attachments as $a)
                                                <div><a href="{{asset("storage/app/".$a->path)}}" target="_blank" style="text-decoration: underline !important;"><i class="fa fa-paperclip"></i> <span>المرفق المرفق</span> <span>{{$loop->iteration}}</span></a></div>
                                            @endforeach
                                        @endif
                                    </div>
                                    @if($loop->iteration<count($axis->comments))<hr>@endif
                                @endforeach
                            @else
                                <p>لا توجد تعليقات</p>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="AxisAttachments{{$axis->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">الملفات</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-4">{{$axis->name}}</div>
                        <div class="col-sm-4"><i class="fa fa-user"></i> <span>{{@$axis->speaker->name}}</span></div>
                        <div class="col-sm-4"><i class="fa fa-clock-o"></i> <span>ساعة : </span><span>{{$axis->hours}}</span> <span style="padding-right: 20px;">دقيقة : </span> <span>{{$axis->minutes}}</span></div>
                        <div class="col-sm-12"><hr></div>
                    </div>

                    @if(permissions_meetings("add_file_axis"))
                    <form class="row" action="{{route("company.meetings.save_axis_attachments", @$company->slug)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="axis_id" value="{{$axis->id}}">

                        <div class="col-sm-12">
                            <h4 class="mt-0">إضافة الملفات :  <span class="red">*</span></h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="attachments[]" multiple accept=".doc,.docx,application/msword,image/*,application/pdf" required>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group mb-10">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> <span>إضافة</span></button>
                            </div>
                        </div>

                        <div class="col-sm-12"><hr></div>

                    </form>
                    @endif

                    <div class="row mb-10">
                        <div class="col-sm-12" id="comments">
                            <h4 class="mt-0 mb-15">قائمة الملفات : </h4>
                            @if(count($axis->attachments)>0)
                                @foreach($axis->attachments as $attachment)
                                    <div class="comment">
                                        @if(@$attachment->user->image)
                                            <img src="{{asset("storage/app/".$attachment->user->image)}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                                        @else
                                            <img src="{{asset("public/noimage.png")}}" style="width: 56px; height: 56px; border-radius: 50%; border: 3px solid #fff; float: right;">
                                        @endif
                                        <h4 class="pull-right" style="margin: 20px 10px 0px 01px;color: #204c65">{{@$attachment->user->name}}</h4>
                                        <div class="clearfix"></div>
                                        <div class="mt-10"><a href="{{asset("storage/app/".$attachment->path)}}" target="_blank" style="text-decoration: underline !important;"><i class="fa fa-paperclip"></i> <span>المرفق المرفق</span> <span>{{$loop->iteration}}</span></a></div>
                                    </div>
                                    @if($loop->iteration<count($axis->attachments))<hr>@endif
                                @endforeach
                            @else
                                <p>لا توجد ملفات</p>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="AxisEdit{{$axis->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">تعديل المحور</h4>
                </div>
                <div class="modal-body">

                    <form action="{{route("company.meetings.update_axis", @$company->slug)}}" method="post">
                        @csrf
                        <input type="hidden" name="axis_id" value="{{$axis->id}}">

                        <div class="row mb-15">
                            <div class="col-sm-3"><strong>إسم المحور <span class="red">*</span></strong></div>
                            <div class="col-sm-9"><input type="text" class="form-control" name="name" value="{{$axis->name}}" required></div>
                        </div>

                        <div class="row mb-15">
                            <div class="col-sm-3"><strong>المتحدث <span class="red">*</span></strong></div>
                            <div class="col-sm-9">
                                <select class="form-control" name="speaker_id" required>
                                    <option value="">إختر</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}" @if($axis->speaker_id==$user->id) selected @endif>{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mb-15">
                            <div class="col-sm-3"><strong>مدة النقاش</strong></div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-2">ساعة <span class="red">*</span></div>
                                    <div class="col-sm-4"><select class="form-control" required name="hours">@for($i=0;$i<=24;$i++)<option value="{{$i}}" @if($axis->hours==$i) selected @endif>{{$i}}</option>@endfor</select></div>
                                    <div class="col-sm-2">دقيقة <span class="red">*</span></div>
                                    <div class="col-sm-4"><select class="form-control" required name="minutes">@for($i=0;$i<=60;$i++)<option value="{{$i}}" @if($axis->minutes==$i) selected @endif>{{$i}}</option>@endfor</select></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>تعديل</span></button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endforeach