@if(permissions_meetings("add_vote"))
    <div style="border: 1px solid #eee; padding: 20px; margin-top: 25px">
        <form action="{{route("company.meetings.save_vote", @$company->slug)}}" method="post" id="save_vote">
        @csrf
        <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

        <div class="row mb-15">
            <div class="col-sm-3"><strong>موضوع التصويت <span class="red">*</span></strong></div>
            <div class="col-sm-7"><input type="text" class="form-control" name="name" required></div>
        </div>

        <div class="row mb-15">
            <div class="col-sm-3"><strong>طريقة التصويت <span class="red">*</span></strong></div>
            <div class="col-sm-7">
                <select class="form-control type_options" name="type" required>
                    <option value="">إختر</option>
                    <option value="0">موافق أو غير موافق</option>
                    <option value="1">خيارات</option>
                    <option value="2">خيارات متعددة</option>
                    <option value="3">نص</option>
                </select>
            </div>
        </div>

        <div class="row mb-15 options" id="options1" style="display: none;">
            <div class="col-sm-3"><strong>الإختيارات <span class="red">*</span></strong></div>
            <div class="col-sm-7 bloogeez">
                <div class="row">
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="aaa[]" >
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-primary btn-block btn-addoption" href="#"><i class="fa fa-plus"></i> <span>إضافة</span></a>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-danger btn-block disabled" disabled href="javascript:void(0)"><i class="fa fa-trash"></i> <span>حذف</span></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-15 options" id="options2" style="display: none;">
            <div class="col-sm-3"><strong>النص</strong></div>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="options2" >
            </div>
        </div>

        <div class="row mb-15">
            <div class="col-sm-3"><strong>مدة التصويت</strong></div>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-2">ساعة <span class="red">*</span></div>
                    <div class="col-sm-4"><select class="form-control" required name="hours">@for($i=0;$i<=24;$i++)<option value="{{$i}}">{{$i}}</option>@endfor</select></div>
                    <div class="col-sm-2">دقيقة <span class="red">*</span></div>
                    <div class="col-sm-4"><select class="form-control" required name="minutes">@for($i=0;$i<=60;$i++)<option value="{{$i}}">{{$i}}</option>@endfor</select></div>
                </div>
            </div>
        </div>

        <div class="row mb-15">
            <div class="col-sm-3" class="mt-10"><strong>كتابة ملاحظة</strong></div>
            <div class="col-sm-7">
                <div class="checkbox icheck opzn">
                    <label>
                        <input class="form-check-input opzn" type="checkbox" name="open" id="open" {{ old('remember') ? 'checked' : '' }}>
                        نعم
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> <span>إضافة</span></button>
            </div>
        </div>

    </form>
    </div>
@endif

<div id="votes-wrapper">

    <div id="votes-div">

        @if(count($meeting->votes)>0)
            <table class="table table-bordered dataTable">
                <thead>
                <tr>
                    <th width="40px" class="text-center">م</th>
                    <th>موضوع التصويت</th>
                    <th>مدة التصويت</th>
                    <th>طريقة التصويت</th>
                    <th>الحالة</th>
                    <th>الأوامر</th>
                </tr>
                </thead>
                <tbody>
                @foreach($meeting->votes as $vote)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$vote->name}}</td>
                        <td>
                            <div class="row">
                                <div class="col-sm-6 text-center"><span style="padding-left: 10px">ساعة : </span> <span>{{$vote->hours}}</span></div>
                                <div class="col-sm-6 text-center"><span style="padding-left: 10px">دقيقة : </span> <span>{{$vote->minutes}}</span></div>
                            </div>
                        </td>
                        <td>
                            @if($vote->type==0)
                                <span>موافق أو غير موافق</span>
                            @elseif($vote->type==1)
                                <span>خيارات</span>
                            @elseif($vote->type==2)
                                <span>خيارات متعددة</span>
                            @elseif($vote->type==3)
                                <span>نص</span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($vote->status==0)
                                <b style="color: #1d68a7">جديد</b>
                            @elseif($vote->status==1)
                                <b style="color: #5bb600">جاري</b>
                            @else
                                <b style="color: crimson">إنتهى</b>
                            @endif
                        </td>
                        <td>
                            <a class="btn-xs btn btn-success btn-50" href="#" data-toggle="modal" data-target="#showVoteModal{{$vote->id}}" title="المشاهدة"><span class="fa fa-eye"></span></a>
                            @if($vote->status==1 and $vote->vote_me)
                                <a class="btn-xs btn btn-default btn-50" href="#" data-toggle="modal" data-target="#voteVoteModal1{{$vote->id}}" title="قم بالتصويت الآن"><span class="fa fa-star"></span></a>
                            @else
                                <a class="btn-xs btn btn-default btn-50 disabled" href="#"  title="قم بالتصويت الآن"><span class="fa fa-star"></span></a>
                            @endif
                            @if(permissions_meetings("update_vote"))
                                <a class="btn-xs btn btn-primary btn-50" href="#" data-toggle="modal" data-target="#editVoteModal{{$vote->id}}" title="التعديل"><span class="fa fa-edit"></span></a>
                            @else
                                <a class="btn-xs btn btn-primary btn-50 disabled" data-toggle="tooltip" title="التعديل"><span class="fa fa-edit"></span></a>
                            @endif

                            @if($meeting->status==1 and ($user->id==$meeting->manager_id or $user->id==$meeting->manager_id))
                                @if($vote->status==0)
                                    <a class="btn-xs btn btn-info btn-50 start_vote" data-id="{{$vote->id}}" data-status="1" data-toggle="tooltip" title="تشغيل التصويت"><span class="fa fa-gear"></span></a>
                                @else
                                    <a class="btn-xs btn btn-warning btn-50 start_vote" data-id="{{$vote->id}}" data-status="2" data-toggle="tooltip" title="إغلاق التصويت"><span class="fa fa-gear"></span></a>
                                @endif
                            @else
                                <a class="btn-xs btn btn-info btn-50 disabled" data-toggle="tooltip" title="تشغيل التصويت"><span class="fa fa-gear"></span></a>
                            @endif
                            @if(permissions_meetings("delete_vote"))
                                <a class="btn-xs btn btn-danger btn-50 delete_vote" href="#" data-id="{{$vote->id}}" data-toggle="tooltip" title="الحذف"><span class="fa fa-remove"></span></a>
                            @else
                                <a class="btn-xs btn btn-danger btn-50 disabled" data-toggle="tooltip" title="الحذف"><span class="fa fa-remove"></span></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h4>لا توجد تصويتات حاليا !</h4>
        @endif

        @foreach($meeting->votes as $vote)

            <div id="showVoteModal{{$vote->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">التصويت</h4>
                            </div>
                            <div class="modal-body">

                                @if($vote->type==0)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>موافق أم غير موافق : </strong></div>
                                        <div class="col-sm-12 mt-5">
                                            <span style="margin-left: 5px">[موافق]</span><span>[غير موافق]</span>
                                        </div>
                                        <div class="col-sm-12 mt-5"><hr style="margin: 13px 0px 10px;"></div>
                                    </div>
                                @elseif($vote->type==1)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>خيارات : </strong></div>
                                        <div class="col-sm-12 mt-5">
                                            @foreach(unserialize($vote->options) as $option)
                                                <span style="margin-left: 5px">[{{$option}}]</span>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-12 mt-5"><hr style="margin: 13px 0px 10px;"></div>
                                    </div>
                                @elseif($vote->type==2)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>خيارات متعددة : </strong></div>
                                        <div class="col-sm-12 mt-5">
                                            @foreach(unserialize($vote->options) as $option)
                                                <span style="margin-left: 5px">[{{$option}}]</span>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-12 mt-5"><hr style="margin: 13px 0px 10px;"></div>
                                    </div>
                                @else
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>نص : </strong></div>
                                        <div class="col-sm-12 mt-5">{{$vote->options}}<hr style="margin: 13px 0px 10px;"></div>
                                    </div>
                                @endif

                                @foreach($users as $u)
                                    <div class="mb-15">
                                        <div class="col-sm-12" style="background: #eee;border: 1px solid #d0d0d0;padding: 15px">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    @if(@$u->image)
                                                        <img src="{{asset("storage/app/".@$u->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                    @else
                                                        <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                                    @endif
                                                    <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$u->name}}</h6>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-sm-8">
                                                    @foreach($vote->votes as $v)
                                                        @if($v->user_id==$u->id)
                                                            <div>
                                                                @if($vote->type==0)
                                                                    @if($v->result==0)<span>[موافق]</span>@else<span>[غير موافق]</span>@endif
                                                                @elseif($vote->type==1)
                                                                    <?php $my_options = explode(";", $v->result); ?>
                                                                    @foreach(unserialize($vote->options) as $option)
                                                                        @if(in_array($loop->iteration-1, $my_options))
                                                                            <span style="margin-left: 5px">[{{$option}}]</span>
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($vote->type==2)
                                                                    <?php $my_options = explode(";", $v->result); ?>
                                                                    @foreach(unserialize($vote->options) as $option)
                                                                        @if(in_array($loop->iteration-1, $my_options))
                                                                            <span style="margin-left: 5px">[{{$option}}]</span>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    <div class="text-justify"><b>رد على النص : </b>{{$vote->options}}</div>
                                                                @endif
                                                            </div>
                                                            @if($v->note)
                                                                <div class="mt-10 text-justify"><b>الملاحظة : </b>{{$v->note}}</div>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                 @endforeach

                            </div>
                        </div>

                    </div>
                </div>

            <div id="editVoteModal{{$vote->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">تعديل التصويت</h4>
                        </div>
                        <div class="modal-body">

                            <form action="{{route("company.meetings.update_vote", @$company->slug)}}" method="post" class="update_vote" data-id="{{$vote->id}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$vote->id}}">

                                <div class="row mb-15">
                                    <div class="col-sm-3"><strong>موضوع التصويت <span class="red">*</span></strong></div>
                                    <div class="col-sm-9"><input type="text" value="{{$vote->name}}" class="form-control" name="name" required></div>
                                </div>

                                <div class="row mb-15">
                                    <div class="col-sm-3"><strong>مدة النقاش</strong></div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">ساعة <span class="red">*</span></div>
                                            <div class="col-sm-4"><select class="form-control" required name="hours">@for($i=0;$i<=24;$i++)<option value="{{$i}}" @if($vote->hours==$i) selected @endif>{{$i}}</option>@endfor</select></div>
                                            <div class="col-sm-2">دقيقة <span class="red">*</span></div>
                                            <div class="col-sm-4"><select class="form-control" required name="minutes">@for($i=0;$i<=60;$i++)<option value="{{$i}}" @if($vote->minutes==$i) selected @endif>{{$i}}</option>@endfor</select></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mb-15">
                                    <div class="col-sm-3"><strong>طريقة التصويت <span class="red">*</span></strong></div>
                                    <div class="col-sm-9">
                                        <select class="form-control type_options" name="type" required>
                                            <option value="">إختر</option>
                                            <option value="0" @if($vote->type==0) selected @endif>موافق أو غير موافق</option>
                                            <option value="1" @if($vote->type==1) selected @endif>خيارات</option>
                                            <option value="2" @if($vote->type==2) selected @endif>خيارات متعددة</option>
                                            <option value="3" @if($vote->type==3) selected @endif>نص</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-15 options" id="options1" style="@if($vote->type!=1 and $vote->type!=2) display: none; @endif">
                                    <div class="col-sm-3"><strong>الإختيارات <span class="red">*</span></strong></div>
                                    <div class="col-sm-9 bloogeez">
                                        @if($vote->type!=1 and $vote->type!=2)
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="aaa[]" >
                                                </div>
                                                <div class="col-sm-2">
                                                    <a class="btn btn-primary btn-block btn-addoption" href="#"><i class="fa fa-plus"></i> <span>إضافة</span></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a class="btn btn-danger btn-block disabled" disabled href="javascript:void(0)"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                                </div>
                                            </div>
                                        @else
                                            @foreach(unserialize($vote->options) as $option)
                                                <div class="row mb-10">
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="aaa[]" value="{{@$option}}">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a class="btn btn-primary btn-block btn-addoption" href="#"><i class="fa fa-plus"></i> <span>إضافة</span></a>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a class="btn btn-danger btn-block btn-deleteoption" href="#"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                                <div class="row mb-15 options" id="options2" style="@if($vote->type!=3) display: none; @endif">
                                    <div class="col-sm-3"><strong>النص</strong></div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="options2" @if($vote->type==3) value="{{$vote->options}}" @endif>
                                    </div>
                                </div>

                                <div class="row mb-15">
                                    <div class="col-sm-3" class="mt-10"><strong>كتابة ملاحظة</strong></div>
                                    <div class="col-sm-7">
                                        <div class="checkbox icheck opzn">
                                            <label>
                                                <input class="form-check-input opzn" type="checkbox" name="open" id="open" {{ ($vote->open==1)? 'checked' : '' }}>
                                                نعم
                                            </label>
                                        </div>
                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حذف</span></button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>

            <div id="voteVoteModal1{{$vote->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">الموافقة على موضوع التصويت</h4>
                        </div>
                        <div class="modal-body">

                            <form action="{{route("company.meetings.vote_vote", @$company->slug)}}" method="post" class="vote_vote" data-id="{{$vote->id}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$vote->id}}">
                                <input type="hidden" name="user_id" value="{{$user->id}}">

                                @if($vote->type==0)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>موافق أم غير موافق :  <span class="red">*</span></strong></div>
                                        <div class="col-sm-12 mt-5">
                                            <select class="form-control" name="result" required>
                                                <option value="0">موافق</option>
                                                <option value="1">غير موافق</option>
                                            </select>
                                        </div>
                                    </div>
                                @elseif($vote->type==1)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>خيارات :  <span class="red">*</span></strong></div>
                                        <div class="col-sm-12 mt-5">
                                            <select class="form-control" name="result" required>
                                                @foreach(unserialize($vote->options) as $option)
                                                    <option value="{{$loop->index}}">{{$option}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @elseif($vote->type==2)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>خيارات متعددة :  <span class="red">*</span></strong></div>
                                        <div class="col-sm-12 mt-5">
                                            <select class="form-control select2x" name="result[]" multiple required>
                                                @foreach(unserialize($vote->options) as $option)
                                                    <option value="{{$loop->index}}">{{$option}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @else
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>النص : </strong></div>
                                        <div class="col-sm-12 mt-5">{{$vote->options}}</div>
                                    </div>
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>الرد على النص :  <span class="red">*</span></strong></div>
                                        <div class="col-sm-12 mt-5"><textarea class="form-control" name="result" rows="4" required></textarea></div>
                                    </div>
                                @endif

                                @if($vote->open==1)
                                    <div class="row mb-15">
                                        <div class="col-sm-12 mt-5"><strong>الملاحظة : </strong></div>
                                        <div class="col-sm-12 mt-5">
                                            <textarea class="form-control" name="note" rows="4">{{$vote->note}}</textarea>
                                        </div>
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-thumbs-up"></i> <span>تصويت</span></button>
                                    </div>
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><i class="fa fa-sign-out"></i> <span>إغلاق</span></button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>

        @endforeach

    </div>

</div>
