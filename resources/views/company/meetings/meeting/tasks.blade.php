@if(permissions_meetings("add_task"))

    <div style="border: 1px solid #eee; padding: 20px; margin-top: 25px">

        <form action="{{route("company.meetings.save_task", @$company->slug)}}" method="post" id="save_task">
            @csrf
            <input type="hidden" name="meeting_id" value="{{$meeting->id}}">

            <div class="row mb-15">
                <div class="col-sm-2"><strong>إسم المهمة <span class="red">*</span></strong></div>
                <div class="col-sm-4"><input type="text" class="form-control" name="name" required></div>
                <div class="col-sm-2"><strong>المكلف <span class="red">*</span></strong></div>
                <div class="col-sm-4"><select class="form-control" name="user_id" required><option value="">إختر</option>@foreach($users as $user)<option value="{{$user->id}}">{{$user->name}}</option>@endforeach</select></div>
            </div>

            <div class="row mb-15">
                <div class="col-sm-2"><strong>التاريخ </strong></div>
                <div class="col-sm-4"><input type="text" class="form-control datepicker" autocomplete="off" name="thedate" style="text-align: right;"></div>
                <div class="col-sm-2"><strong>المساعدون</strong></div>
                <div class="col-sm-4"><select name="users[]" class="form-control select2x" multiple>@foreach($users as $user)<option value="{{$user->id}}">{{$user->name}}</option>@endforeach</select></div>
            </div>

            <div class="row">
                <div class="col-sm-2"><strong>التفاصيل <span class="red">*</span></strong></div>
                <div class="col-sm-10"><textarea class="form-control" name="note" rows="5" required></textarea></div>
            </div>

            <div class="row mt-30">
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> <span>إضافة</span></button>
                </div>
            </div>

        </form>

    </div>

@endif

<div id="task-wrapper">

    <div id="task-div">

        @if(count($tasks)>0)
            <h4>قائمة المهام :</h4>
            <table class="table table-bordered dataTable">
                <thead>
                    <tr>
                        <th width="30" class="text-center">م</th>
                        <th>الإسم</th>
                        <th style="min-width: 80px">التاريخ</th>
                        <th>المكلف</th>
                        <th>المساعدون</th>
                        <th>الحالة</th>
                        <th>الأوامر</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td width="20" class="text-center" style=" vertical-align: middle;">{{$loop->iteration}}</td>
                            <td>{{$task->name}}</td>
                            <td class="text-center">{{$task->thedate}}</td>
                            <td>
                                @if(@$task->user->image)
                                    <img src="{{asset("storage/app/".@$task->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                @else
                                    <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                @endif
                                <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$task->user->name}}</h6>
                            </td>
                            <td>
                                @if(count($task->users))
                                    @foreach($task->users as $user)
                                        @if(@$user->image)
                                            <img src="{{asset("storage/app/".@$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                        @else
                                            <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                                        @endif
                                        <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$user->name}}</h6>
                                        <div class="clearfix mb-10"></div>
                                    @endforeach
                                @endif
                            </td>
                            <td style="vertical-align: middle">
                                @if($task->status==0)
                                    <ul class="nav nav-pills"><li style="background: crimson;border-radius: 10px;color: #fff;display: block;width: 100%;text-align: center;">لم تمت</li></ul>
                                @else
                                    <ul class="nav nav-pills"><li style="background: chartreuse;border-radius: 10px;display: block;width: 100%;text-align: center;">تمت</li></ul>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="mt-15 btn-xs btn btn-info btn-50" href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}" title="المشاهدة"><span class="fa fa-eye"></span></a>
                                @if(permissions_meetings("update_task"))
                                    <a class="mt-15 btn-xs btn btn-primary btn-50" href="#" data-toggle="modal" data-target="#editTaskeModal{{$task->id}}" title="التعديل"><span class="fa fa-edit"></span></a>
                                @else
                                    <a class="mt-15 btn-xs btn btn-primary btn-50 disabled" href="#" title="التعديل"><span class="fa fa-edit"></span></a>
                                @endif
                                @if(permissions_meetings("update_task"))
                                    @if($task->status==0)
                                        <a class="mt-15 btn-xs btn btn-success btn-50 change_task_status" data-id = "{{$task->id}}" data-status="1" href="#" data-toggle="tooltip" title="تمت"><span class="fa fa-check"></span></a>
                                    @else
                                        <a class="mt-15 btn-xs btn btn-danger btn-50 change_task_status" data-id = "{{$task->id}}" data-status="0" href="#" data-toggle="tooltip" title="لم تمت"><span class="fa fa-remove"></span></a>
                                    @endif
                                @endif
                                {{--
                                    <div class="dropdown" style="display: inline-block">
                                        <a class="mt-15 btn-xs btn btn-default btn-50 dropdown-toggle" href="#" type="button" data-toggle="dropdown" data-placement="top"  title="تغيير الحالة"><span class="fa fa-gear"></span> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a class="change_task_status" data-id = "{{$task->id}}" data-status="0" style="background: #367fa9; margin-bottom: 5px; color: #fff; font-weight: bold;text-align: center;" href="#">جديد</a></li>
                                            <li><a  style="background: #e08e0b; margin-bottom: 5px; color: #fff; font-weight: bold;text-align: center;" href="#">جاري</a></li>
                                            <li><a class="change_task_status" data-id = "{{$task->id}}" data-status="2" style="background: #00a65a; margin-bottom: 0px; color: #fff; font-weight: bold;text-align: center;" href="#">منجز</a></li>
                                        </ul>
                                    </div>
                                @else
                                    <div class="dropdown" style="display: inline-block">
                                        <a class="mt-15 btn-xs btn btn-default btn-50 disabled" href="#" type="button" data-toggle="dropdown" data-placement="top"  title="تغيير الحالة"><span class="fa fa-gear"></span></a>
                                    </div>
                                @endif
                                --}}
                                @if(permissions_meetings("delete_task"))
                                    <a class="delete_task mt-15 btn-xs btn btn-warning btn-50" data-id="{{$task->id}}" href="#" data-toggle="tooltip" data-placement="top"  title="الحذف"><span class="fa fa-trash"></span></a>
                                @else
                                    <a class="mt-15 btn-xs btn btn-warning btn-50 disabled" href="#" data-toggle="tooltip" data-placement="top"  title="الحذف"><span class="fa fa-trash"></span></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h4>لا توجد مهام حاليا !</h4>
        @endif

        @foreach($tasks as $task)

            <div id="showTaskNoteModal{{$task->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{$task->name}}</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>إسم المهمة</strong></div>
                                <div class="col-sm-9">{{$task->name}}</div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>حالة المهمة</strong></div>
                                <div class="col-sm-9">
                                    @if($task->status==0)
                                        <div><b>لم تتم</b></div>
                                    @else
                                        <div><b>تمت</b></div>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>المكلف</strong></div>
                                <div class="col-sm-9">
                                    @foreach($users as $user)
                                        @if($user->id==$task->user_id) <span style="margin-left: 10px">{{$user->name}}</span> @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>المساعدون</strong></div>
                                <div class="col-sm-9">
                                    <?php $userss=[]; foreach ($task->users as $u): array_push($userss, $u->id); endforeach; ?>
                                    @foreach($users as $user)
                                        @if(in_array($user->id, $userss)) <span style="margin-left: 5px">[{{$user->name}}]</span> @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>التاريخ</strong></div>
                                <div class="col-sm-9">{{$task->thedate}}</div>
                            </div>

                            <div class="row mb-15">
                                <div class="col-sm-3"><strong>التفاصيل</strong></div>
                                <div class="col-sm-9 text-justify">{{$task->note}}</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="editTaskeModal{{$task->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">تعديل مهمة</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{route("company.meetings.update_task", @$company->slug)}}" method="post" class="update_task" data-id="{{$task->id}}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$task->id}}">

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>إسم المهمة <span class="red">*</span></strong></div>
                                        <div class="col-sm-9"><input autocomplete="off" type="text" value="{{$task->name}}" class="form-control" name="name" required></div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>المكلف <span class="red">*</span></strong></div>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="user_id" required>
                                                <option value="">إختر</option>
                                                @foreach($users as $user)<option value="{{$user->id}}" @if($user->id==$task->user_id) selected @endif>{{$user->name}}</option>@endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>المساعدون</strong></div>
                                        <div class="col-sm-9">
                                            <?php $userss=[]; foreach ($task->users as $u): array_push($userss, $u->id); endforeach; ?>
                                            <select name="users[]" class="form-control select2x" multiple>
                                                @foreach($users as $user)<option value="{{$user->id}}" @if(in_array($user->id, $userss)) selected @endif>{{$user->name}}</option>@endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>التاريخ </strong></div>
                                        <div class="col-sm-9"><input value="{{$task->thedate}}" type="text" class="form-control datepicker" autocomplete="off" style="text-align: right" name="thedate"></div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3"><strong>التفاصيل <span class="red">*</span></strong></div>
                                        <div class="col-sm-9"><textarea class="form-control" name="note" rows="5" required>{{$task->note}}</textarea></div>
                                    </div>

                                    <div class="row mb-15">
                                        <div class="col-sm-3">
                                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>تعديل</span></button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

        @endforeach

    </div>

</div>

