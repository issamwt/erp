@extends("company.layouts.app", ["title"=>"إضافة إجتماع"])

@section("content")

    <section class="content-header">
        <h1>نظام إجتماعاتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>إضافة إجتماع</strong> <a href="{{route("company.meetings.meetings", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-list"></i> <span>العودة للقائمة</span></a></div>
                    <div class="panel-body">

                        <form action="{{route("company.meetings.save_meeting", @$company->slug)}}" method="post">
                            @csrf

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4"><label>سلسلة إجتماعات <span class="red">*</span></label></div>
                                        <div class="col-xs-8">
                                            <select class="form-control" name="category_id" required>
                                                <option value="0">إختر</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4"><label>موضوع الإجتماع <span class="red">*</span></label></div>
                                        <div class="col-xs-8"><input type="text" class="form-control" name="name" required></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 0px">
                                        <div class="col-xs-4"><label>مكان الإجتماع <span class="red">*</span></label></div>
                                        <div class="col-xs-8" id="loc-wrapper">
                                            <div class="input-group" id="loc-div">
                                                <select class="form-control" name="location_id" id="location_id" required>
                                                    <option value="">إختر مكان الإجتماع</option>
                                                    @foreach($locations as $location)
                                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                                    @endforeach
                                                </select>
                                                <a href="#" id="addLocationModalOpener" class="btn btn-primary input-group-addon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-3"><label>التاريخ <span class="red">*</span></label></div>
                                        <div class="col-xs-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker" autocomplete="off" name="from_date" id="from_date" required style="text-align: right">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-3">الوقت <span class="red">*</span></div>
                                        <div class="col-xs-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" autocomplete="off" name="from_time" id="from_time" required style="text-align: right">
                                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>
                                        {{--
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" autocomplete="off" name="to_time" id="to_time" required style="text-align: right">
                                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            </div>
                                        </div>--}}
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        {{--
                                        <div class="col-xs-3"><b>تكرار كل</b></div>
                                        <div class="col-xs-9">
                                            <select class="form-control" name="repeats" required>
                                                <option value="0">إختر</option>
                                                <option value="1">يوم</option>
                                                <option value="2">أسبوع</option>
                                                <option value="3">شهر</option>
                                            </select>
                                        </div>
                                        --}}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4">
                                            <label class="choice">
                                                <i class="fa fa-check"></i><span>إجتماع عادي</span><input class="no_ichek" type="radio" name="type" value="0" checked>
                                            </label>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="choice">
                                                <i class="fa fa-times"></i><span>إجتماع مهم</span><input class="no_ichek" type="radio" name="type" value="1">
                                            </label>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="choice">
                                                <i class="fa fa-exclamation"></i><span>إجتماع طارئ</span><input class="no_ichek" type="radio" name="type" value="2">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-4">
                                    <div id="addLocationModal" style="border: 1px solid #eee;margin-top:15px; padding:10px 15px 15px 15px; display: none;">
                                        <h4>إضافة مكان إجتماع جديد <span class="red">*</span></h4>
                                        <input type="text" id="loc" class="form-control" maxlength="200">
                                        <br>
                                        <button type="button" id="addLocation" class="btn btn-primary"><i class="fa fa-plus"></i> <span>إضافة</span></button>
                                    </div>
                                </div>
                                <div class="clearfix" style="margin-bottom: 10px"></div>
                                <div class="col-sm-8">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-xs-2"><label style="margin-top:10px;">طريقة الإجتماع</label></div>
                                        <div class="col-xs-10">
                                            <div class="checkbox" style="display: inline-block;min-width: 160px;">
                                                <label><input class="online" type="radio" name="online" value="0" checked> <b>عن بعد</b></label>
                                            </div>
                                            <div class="checkbox" style="display: inline-block;min-width: 160px;">
                                                <label><input class="online" type="radio" name="online" value="1"> <b>حضوريا</b></label>
                                            </div>
                                            <input style="display: none" type="text" id="gmap_url" class="form-control" placeholder="رابط الغوغل ماب" name="gmap_url" >
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-8">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-xs-12">
                                            <div class="checkbox" style="display: inline-block;min-width: 160px;">
                                                <label><input type="checkbox" name="record" value="1"> <b>تسجيل الإجتماع</b></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4"><label>مدير الإجتماع <span class="red">*</span></label></div>
                                        <div class="col-xs-8">
                                            <select name="manager" required class="form-control"><option value="">إختر</option>@foreach($users as $user)<option value="{{$user->id}}">{{$user->name}}</option>@endforeach</select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4"><label>أمين الإجتماع <span class="red">*</span></label></div>
                                        <div class="col-xs-8">
                                            <select name="amin" required class="form-control"><option value="">إختر</option>@foreach($users as $user)<option value="{{$user->id}}">{{$user->name}}</option>@endforeach</select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><div style="height: 80px; width: 2px; background: red;margin: 0px auto"></div></div>
                                <div class="col-sm-5">
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4"><label>الموظفين المدعوين</label></div>
                                        <div class="col-xs-8">
                                            <select name="users[]" class="form-control select2x" multiple>@foreach($users as $user)<option value="{{$user->id}}">{{$user->name}}</option>@endforeach</select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-xs-4"><label>العملاء المدعوين</label></div>
                                        <div class="col-xs-8">
                                            <select name="clients[]" class="form-control select2x" multiple>@foreach($clients as $client)<option value="{{$client->id}}">{{$client->name}}</option>@endforeach</select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-xs-10"></div>
                                <div class="col-xs-2">
                                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>

    <style>
        .choice{
            width: 90px;
            height: 90px;
            border: 1px solid #b9b9b9;
            border-radius: 4px;
            text-align: center;
            line-height: 35px;
            display: block;
            margin: 0px auto;
            cursor: pointer;
            padding-top: 15px;
        }
        .col-xs-4:nth-child(1) .choice{
            background: #60bd03;
        }
        .col-xs-4:nth-child(1) .choice:hover, .col-xs-4:nth-child(1) .choice.active{
            box-shadow: 0px 0px 3px 3px #60bd03;
            border-color: #458802;
        }
        .col-xs-4:nth-child(2) .choice{
            background: hotpink;
        }
        .col-xs-4:nth-child(2) .choice:hover, .col-xs-4:nth-child(2) .choice.active{
            box-shadow: 0px 0px 3px 3px hotpink;
            border-color: #b74b81;
        }
        .col-xs-4:nth-child(3) .choice{
            background: crimson;
        }
        .col-xs-4:nth-child(3) .choice:hover, .col-xs-4:nth-child(3) .choice.active{
            box-shadow: 0px 0px 3px 3px crimson;
            border-color: #a80f2e;
        }
        .choice .fa{
            font-size: 1.5em;
            background: #fff;
            padding: 4px;
            border-radius: 50%;
            border: 1px solid #b9b9b9;
            width: 31px;
            height: 31px;
            text-align: center;
            line-height: 21px;
        }
        .choice span{
            font-size: 0.7em;
            background: #fff;
            display: block;
            height: 17px;
            line-height: 18px;
            width: 70%;
            margin: 0px auto;
        }
        .choice input[type='radio']{
            display: none;
        }
        .btn.input-group-addon{
            background-color: #277AB3;
            color: #fff;
        }
        .datepicker.datepicker-dropdown{
            z-index: 9999 !important;
        }
        .bootstrap-timepicker-widget{
            direction: ltr !important;
            right: auto !important;
        }
    </style>

    <script>
        $(function () {

            $('.datepicker').datepicker({format: 'yyyy-mm-dd', rtl: true, language: 'ar', autoclose: true});

            $(".timepicker").timepicker({ showMeridian:false, minuteStep:1, icons:{up: 'fa fa-chevron-up', down: 'fa fa-chevron-down'}});

            $(".select2x").select2({dir: "rtl", language: "ar", placeholder : "إختر"});

            $(document).on("click", ".choice", function () {
               $("input[type='radio']", this).prop("checked", true);
               $(".choice").removeClass("active");
                $(this).addClass("active");
            });

            $(document).on("ifChanged", ".online", function () {
                if($(this).val()==1)
                    $("#gmap_url").slideDown();
                else
                    $("#gmap_url").slideUp();
            });

            $(document).on("click", "#addLocationModalOpener", function () {
                $("#addLocationModal").slideDown("slow");
            });

            $(document).on("click", "#addLocation", function () {
                var name = $("#loc").val();
                if(name){
                    $.post("{{route("company.meetings.save_location", @$company->slug)}}", {_token:'{{csrf_token()}}', name:name}, function (data) {
                        $("#loc").val("");
                        $("#addLocationModal").slideUp("slow");
                        $("#loc-wrapper").load(window.location +" #loc-div", function () {
                            $("#location_id").val(data);
                        });
                    });
                }else{
                    alert("يجب كتابة مكان الإجتماع !");
                }
            });

        });
    </script>

@endsection