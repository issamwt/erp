
@extends("company.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                @section("content")

                    <section class="content-header">
                        <h1>نظام إجتماعاتي</h1>
                    </section>

                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12" style="margin-bottom: 100px; padding: 20px;">

                                <div class="col-sm-12"  style="border: 1px solid #d6d6d6; padding: 15px; background: #fff;">

                                    <div class="row">

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h4>عدد المستخدمين</h4>
                                                    <h4>{{@$users_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <a href="{{route("company.clients.index", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h4>عدد العملاء الأفراد</h4>
                                                    <h4>{{@$clients0_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-stats-bars"></i>
                                                </div>
                                                <a href="{{route("company.clients.index", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h4>عدد عملاء الشركات</h4>
                                                    <h4>{{@$clients1_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-person-add"></i>
                                                </div>
                                                <a href="{{route("company.clients.index", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <div class="small-box bg-red">
                                                <div class="inner">
                                                    <h4>عدد الموظفين</h4>
                                                    <h4>{{@$employees_num}}</h4>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-pie-graph"></i>
                                                </div>
                                                <a href="{{route("company.users.index", @$company->slug)}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-left"></i></a>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <table class="table table-bordered col-sm-12 mt-20"  style="border: 1px solid #d6d6d6 !important; padding: 15px; background: #fff;">
                                    <tr>
                                        <td width="20%" style="position: relative;">
                                            <h2 class="text-center" style="margin: 10px 0px 20px;">الإجتماعات</h2>
                                            <a href="{{route("company.meetings.meetings", @$company->slug)}}" class="small-box-footer pull-left" style="position: absolute;bottom: 10px;left: 10px;"><i class="fa fa-arrow-circle-right"></i> <span>التفاصيل</span> </a>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart1" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>المنتهية</span> <span>{{($all_meetings==0)?0:intval($completed_meetings/$all_meetings*100)}} %</span></h4>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart2" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>القادمة</span> <span>{{($all_meetings==0)?0:intval($coming_meetings/$all_meetings*100)}} %</span></h4>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="chart tab-pane active" id="chart3" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>الملغية</span> <span>{{($all_meetings==0)?0:intval($canceled_meetings/$all_meetings*100)}} %</span></h4>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table table-bordered col-sm-12"  style="border: 1px solid #d6d6d6 !important; padding: 15px; background: #fff;">
                                    <tr>
                                        <td width="20%" style="position: relative;">
                                            <h2 class="text-center" style="margin: 10px 0px 20px;">المهام</h2>
                                            <a href="{{route("company.meetings.meetings", @$company->slug)}}" class="small-box-footer pull-left" style="position: absolute;bottom: 10px;left: 10px;"><i class="fa fa-arrow-circle-right"></i> <span>التفاصيل</span> </a>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="chart tab-pane active" id="chart4" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>تمت</span> <span>{{($all_tasks==0)?0:intval($finished_tasks/$all_tasks*100)}} %</span></h4>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="chart tab-pane active" id="chart5" style="position: relative; height: 200px;"></div>
                                                    <h4 class="text-center mb-20"><span>لم تتم</span> <span>{{($all_tasks==0)?0:intval($new_tasks/$all_tasks*100)}} %</span></h4>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                {{--<div class="col-sm-12"  style="border: 1px solid #d6d6d6; padding: 15px; background: #fff;">

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <h3 class="mt-5 mb-20">تقويم الأحداث</h3>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="box box-solid" style="border: 1px solid #d6d6d6">
                                                <div class="box-body no-padding">
                                                    <div id="calendar"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>--}}

                            </div>
                        </div>
                    </section>

                @endsection


            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .small-box{
            margin: 0px;
        }
        .table-bordered td{
            border-color: #d6d6d6 !important;
        }
        .fc-content{
            padding: 3px;
            border-radius: 0px !important;
        }
        .fc-day-grid-event{
            border-radius: 0px !important;
        }
    </style>

    <link rel="stylesheet" href="{{asset("public/plugins/morris/morris.css")}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{asset("public/plugins/morris/morris.min.js")}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.min.css"  media="print"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale-all.js"></script>

    <script>

        $(function () {

            var completed = {{($all_meetings==0)?0:intval($completed_meetings/$all_meetings*100)}};
            var donut1 = new Morris.Donut({
                element: 'chart1',
                resize: true,
                colors: ["#1b80e2", "#d6d6d6"],
                data: [
                    {label: "المنتهية", value: completed},
                    {label: "البقية", value: (100-completed)},
                ],
                hideHover: 'auto'
            });
            donut1.select(0);

            var coming = {{($all_meetings==0)?0:intval($coming_meetings/$all_meetings*100)}};
            var donut2 = new Morris.Donut({
                element: 'chart2',
                resize: true,
                colors: ["#00a65a", "#d6d6d6"],
                data: [
                    {label: "القادمة", value: coming},
                    {label: "البقية", value: (100-coming)},
                ],
                hideHover: 'auto'
            });
            donut2.select(0);

            var canceled = {{($all_meetings==0)?0:intval($canceled_meetings/$all_meetings*100)}};
            var donut3 = new Morris.Donut({
                element: 'chart3',
                resize: true,
                colors: ["#ff0000", "#d6d6d6"],
                data: [
                    {label: "الملغية", value: canceled},
                    {label: "البقية", value: (100-canceled)},
                ],
                hideHover: 'auto'
            });
            donut3.select(0);

            var finished = {{($all_tasks==0)?0:intval($finished_tasks/$all_tasks*100)}};
            var donut4 = new Morris.Donut({
                element: 'chart4',
                resize: true,
                colors: ["#00a65a", "#d6d6d6"],
                data: [
                    {label: "تمت", value: finished},
                    {label: "البقية", value: (100-finished)},
                ],
                hideHover: 'auto'
            });
            donut4.select(0);

            var neww = {{($all_tasks==0)?0:intval($new_tasks/$all_tasks*100)}};
            var donut5 = new Morris.Donut({
                element: 'chart5',
                resize: true,
                colors: ["#ff0000", "#d6d6d6"],
                data: [
                    {label: "لم تتم", value: neww},
                    {label: "البقية", value: (100-neww)},
                ],
                hideHover: 'auto'
            });
            donut5.select(0);

            var colors = ["#00a65a", "#00c0ef", "#f39c12", "#dd4b39"];
            $('#calendar').fullCalendar({
                locale: 'ar',
                events: [
                    @foreach($meetings as $meeting)
                        {
                            title: '{{$meeting->name}}',
                            start: '{{$meeting->from_date}}',
                            url: '{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}',
                            backgroundColor: colors[{{$meeting->status}}],
                            borderColor: "#fff",
                        },
                        @if($meeting->repeats==1)
                            @for($i=1;$i<10000;$i++)
                                {
                                    title: '{{$meeting->name}} {{$i+1}}',
                                    start: '{{\Carbon\Carbon::createFromFormat('Y-m-d',$meeting->from_date)->modify("+".$i." day")->format('Y-m-d')}}',
                                    url: '{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}',
                                    backgroundColor: colors[{{$meeting->status}}],
                                    borderColor: "#fff",
                                },
                            @endfor
                        @elseif($meeting->repeats==2)
                            @for($i=1;$i<1429;$i++)
                                {
                                    title: '{{$meeting->name}} {{$i+1}}',
                                    start: '{{\Carbon\Carbon::createFromFormat('Y-m-d',$meeting->from_date)->modify("+".$i." week")->format('Y-m-d')}}',
                                    url: '{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}',
                                    backgroundColor: colors[{{$meeting->status}}],
                                    borderColor: "#fff",
                                },
                            @endfor
                        @elseif($meeting->repeats==3)
                            @for($i=1;$i<1429;$i++)
                                {
                                    title: '{{$meeting->name}} {{$i+1}}',
                                    start: '{{\Carbon\Carbon::createFromFormat('Y-m-d',$meeting->from_date)->modify("+".$i." month")->format('Y-m-d')}}',
                                    url: '{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}',
                                    backgroundColor: colors[{{$meeting->status}}],
                                    borderColor: "#fff",
                                },
                            @endfor
                        @endif
                    @endforeach
                ]
            });

        });

    </script>

@endsection