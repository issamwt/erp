@extends("company.layouts.app", ["title"=>"الإعدادات"])

@section("content")

    <section class="content-header">
        <h1>نظام إجتماعاتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <strong>إعدادات الإجتماعات</strong>
                            </div>
                            <div class="panel-body">

                                <div id="div1-wrapper">
                                    <div id="div1">
                                        <form action="{{route('company.meetings.save_meetings_settings', $company->slug)}}" method="post" class="form1" enctype="multipart/form-data">
                                            @csrf

                                            <div class="form-group">
                                                <label class="mb-15">إرسال <a href="{{route("reminder")}}" target="_blank">تذكير تلقائي</a> قبل * من بدء الإجتماع : </label>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <select class="form-control" name="reminder_day{{$company->id}}" required>
                                                                <option value="">يوم</option>
                                                                @for($i=0; $i<=356;$i++)
                                                                    <option value="{{$i}}" @if(@$settings["reminder_day".$company->id]->value==$i) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            </select>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <select class="form-control" name="reminder_hour{{$company->id}}" required>
                                                                <option value="">ساعة</option>
                                                                @for($i=0; $i<=24;$i++)
                                                                    <option value="{{$i}}" @if(@$settings["reminder_hour".$company->id]->value==$i) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            </select>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <select class="form-control" name="reminder_minute{{$company->id}}" required>
                                                                <option value="">دقيقة</option>
                                                                @for($i=1; $i<=60;$i++)
                                                                    <option value="{{$i}}" @if(@$settings["reminder_minute".$company->id]->value==$i) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            </select>
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <small style="display: inline"> رابط التذكير في ال cronjob </small> <small style="color: #4aa0e6;display: inline">{{route("reminder")}}</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <label class="mb-15">تسجيل حضور جزئي بعد % من بدا الإجتماع : </label>
                                                <input type="number" min="1" max="100" class="form-control" name="partialAttendance{{$company->id}}" value="{{@$settings["partialAttendance".$company->id]->value}}" required>
                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <label class="mb-15">تغيير لون عداد الإجتماع إلى <span style="color: orange">البرتقالي</span> قبل إنتهاء الإجتماع ب * (دقيقة) :</label>
                                                <input type="number" min="0" class="form-control" name="countdown1{{$company->id}}" value="{{@$settings["countdown1".$company->id]->value}}" required>
                                            </div>

                                            <div class="form-group">
                                                <label class="mb-15">تغيير لون عداد الإجتماع إلى <span style="color: red;">الأحمر</span> قبل إنتهاء الإجتماع ب * (دقيقة) :</label>
                                                <input type="number" min="0" class="form-control" name="countdown2{{$company->id}}" value="{{@$settings["countdown2".$company->id]->value}}" required>
                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <label class="mb-15">رسالة التذكير بالإجتماع</label>
                                                <textarea class="form-control" name="reminder_message{{$company->id}}" required>{{@$settings["reminder_message".$company->id]->value}}</textarea>
                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"> حفظ البيانات</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>قائمة سلاسل الإجتماعات</strong></div>
                            <div class="panel-body">

                                <div id="div2-wrapper">
                                    <div id="div2">

                                        <form class="form2" action="{{route("company.meetings.save_category", $company->slug)}}" method="post">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم</label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <form action="{{route("company.meetings.update_category", $company->slug)}}" method="post" class="row form22" style="margin-bottom: 15px">
                                            @csrf
                                            <div class="">
                                                <div class="col-sm-10 mb-15"><b>الإسم</b></div>
                                                <div class="col-sm-2 mb-15"><b>الحذف</b></div>
                                            </div>

                                            <div class="">
                                            @foreach($categories as $category)
                                                    <div class="col-sm-10 mb-15">
                                                        <input type="text" class="form-control" name="name_{{$category->id}}" value="{{$category->name}}" required>
                                                    </div>
                                                    <div class="col-sm-2 mb-15">
                                                        <a href="{{route("company.meetings.delete_category", ["company"=>$company->slug, "id"=>$category->id])}}" class="btn-delete2 btn btn-danger">حذف</a>
                                                    </div>
                                            @endforeach
                                            </div>

                                            <div class="col-sm-12">
                                            <div class="form-group mt-0 mb-0">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"> حفظ البيانات</button>
                                            </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>بنود تقييم الإجتماعات</strong></div>
                            <div class="panel-body">

                                <div id="div3-wrapper">
                                    <div id="div3">

                                        <form class="form3" action="{{route("company.meetings.save_item", $company->slug)}}" method="post">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم</label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-10"><b>الإسم</b></div>
                                            <div class="col-sm-2"><b>الحذف</b></div>
                                        </div>

                                        <form action="{{route("company.meetings.update_item", $company->slug)}}" method="post" class="row form33" style="margin-bottom: 15px">
                                            @csrf
                                            @foreach($items as $item)

                                                <div class="col-sm-10 mb-15">
                                                    <input type="text" class="form-control" name="name_{{$item->id}}" value="{{$item->name}}" required>
                                                </div>
                                                <div class="col-sm-2 mb-15">
                                                    <a href="{{route("company.meetings.delete_item", ["company"=>$company->slug, "id"=>$item->id])}}" type="submit" class="btn-delete3 btn btn-danger">حذف</a>
                                                </div>

                                            @endforeach

                                                <div class="col-sm-12">
                                                    <div class="form-group mt-0 mb-0">
                                                        <button type="submit" class="btn btn-primary" style="width: 120px"> حفظ البيانات</button>
                                                    </div>
                                                </div>

                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>قائمة أماكن الإجتماع</strong></div>
                            <div class="panel-body">

                                <div id="div4-wrapper">
                                    <div id="div4">

                                        <form class="form4" action="{{route("company.meetings.save_location_settings", $company->slug)}}" method="post">
                                            @csrf

                                            <div class="form-group">
                                                <label>الإسم</label>
                                                <input type="text" class="form-control" name="name" required>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>إضافة</span></button>
                                            </div>
                                            <hr>

                                        </form>

                                        <div class="row" style="margin-bottom: 15px">
                                            <div class="col-sm-10"><b>الإسم</b></div>
                                            <div class="col-sm-2"><b>الأوامر</b></div>
                                        </div>

                                        <form action="{{route("company.meetings.update_location_settings", $company->slug)}}" method="post" class="row form44" style="margin-bottom: 15px">
                                            @csrf

                                            @foreach($locations as $location)
                                                <div class="col-sm-10 mb-10">
                                                    <input type="text" class="form-control" name="name_{{$location->id}}" value="{{$location->name}}" required>
                                                </div>
                                                <div class="col-sm-2 mb-10">
                                                    <a href="{{route("company.meetings.delete_location_settings", ["company"=>$company->slug, "id"=>$location->id])}}" type="submit" class="btn-delete4 btn btn-danger">حذف</a>
                                                </div>
                                            @endforeach

                                            <div class="col-sm-12">
                                                <div class="form-group mt-0 mb-0">
                                                    <button type="submit" class="btn btn-primary" style="width: 120px"> حفظ البيانات</button>
                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><strong>الإشعارات</strong></div>
                            <div class="panel-body">

                                <div id="div5-wrapper">
                                    <div id="div5">

                                        <br>
                                        <div class="row mb-30">
                                            <div class="col-sm-2">نوع الإشعار</div>
                                            <div class="col-sm-2">على البرنامج</div>
                                            <div class="col-sm-2">عبر البريد الإلكتروني</div>
                                            <div class="col-sm-2">عبر الرسائل القصيرة</div>
                                            <div class="col-sm-4">الرسالة</div>
                                        </div>

                                        <form action="{{route("company.meetings.savenotif", $company->slug)}}" method="post" class="form5 mb-10">
                                            @csrf
                                            @foreach($notifs as $notif)

                                                <div class="row mb-10">

                                                    <div class="col-sm-2">{{$notif->title}}{{--<br>{{$notif->name}}--}}</div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="app_{{$notif->id}}" value="1"
                                                                          @if($notif->app==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="email_{{$notif->id}}" value="1"
                                                                          @if($notif->email==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-center">
                                                        <div class="checkbox">
                                                            <label><input type="checkbox" name="sms_{{$notif->id}}" value="1"
                                                                          @if($notif->sms==1) checked @endif></label>
                                                        </div>
                                                    </div>
                                                    <div class="text-justify col-sm-4">
                                            <textarea name="message_{{$notif->id}}" class="form-control"
                                                      maxlength="250" required>{{$notif->message}}</textarea>
                                                    </div>

                                                </div>

                                            @endforeach

                                            <div class="row mt-20 mb-30">
                                                <center>
                                                    <button type="submit" class="btn btn-primary" style="width: 120px;"><i
                                                                class="fa fa-save"></i> <span>حفظ</span></button>
                                                </center>
                                            </div>

                                        </form>

                                    </div>
                                </div>

                                {{--
                                <form action="{{route("company.meetings.savenotif", $company->slug)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="company_id" value="{{$company->id}}">

                                    <input type="text" name="name" placeholder="name" required class="form-control">
                                    <br>

                                    <input type="text" name="title" placeholder="title" required class="form-control">
                                    <br>

                                    <div class="checkbox">
                                        <label><input type="checkbox" name="app" value="1" checked required>على البرنامج</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="email" value="1" checked required>عبر ال Eamil</label>
                                    </div>
                                    <div class="checkbox disabled">
                                        <label><input type="checkbox" name="sms" value="1" checked required>عبر ال Sms</label>
                                    </div>
                                    <br>

                                    <textarea name="message" placeholder="message" required class="form-control"></textarea>
                                    <br>

                                    <button type="submit">أضف</button>
                                    <br><br><br>

                                </form>
                                --}}



                            </div>
                        </div>

                </div>

                </div>

            </div>
        </div>
    </section>

    <div id="loading-wrapper" style="">
        <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
    </div>

@endsection

@section("scripts")

    <script>
        $(document).on("submit", ".form1", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div1").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.save_meetings_settings', $company->slug)}}', $(form).serialize(), function () {
                $("#div1").load(window.location + " #div1", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("submit", ".form2", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div2").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.save_category', $company->slug)}}', $(form).serialize(), function () {
                $("#div2").load(window.location + " #div2", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("click", ".btn-delete2", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div2").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div2").load(window.location + " #div2", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });
        $(document).on("submit", ".form22", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div2").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.update_category', $company->slug)}}', $(form).serialize(), function () {
                $("#div2").load(window.location + " #div2", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("submit", ".form3", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div3").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.save_item', $company->slug)}}', $(form).serialize(), function () {
                $("#div3").load(window.location + " #div3", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("click", ".btn-delete3", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div3").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div3").load(window.location + " #div3", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });
        $(document).on("submit", ".form33", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div3").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.update_item', $company->slug)}}', $(form).serialize(), function () {
                $("#div3").load(window.location + " #div3", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("submit", ".form4", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div4").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.save_location_settings', $company->slug)}}', $(form).serialize(), function () {
                $("#div4").load(window.location + " #div4", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("click", ".btn-delete4", function (e) {
            e.preventDefault();
            var href = $(this).attr("href");
            if(confirm('هل انت متأكد ؟')){
                $(this).closest("#div4").html($("#loading-wrapper").html());
                $.get(href, function () {
                    $("#div4").load(window.location + " #div4", function () {
                        goNotif("success", "تم حفظ البيانات !");
                    });
                });
            }
        });
        $(document).on("submit", ".form44", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div4").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.update_location_settings', $company->slug)}}', $(form).serialize(), function () {
                $("#div4").load(window.location + " #div4", function () {
                    goNotif("success", "تم حفظ البيانات !");
                });
            });
        });
        $(document).on("submit", ".form5", function (e) {
            e.preventDefault();
            var form = $(this);
            $(this).closest("#div5").html($("#loading-wrapper").html());
            $.post('{{route('company.meetings.savenotif', $company->slug)}}', $(form).serialize(), function () {
                $("#div5").load(window.location + " #div5", function () {
                    goNotif("success", "تم حفظ البيانات !");
                    $('input:not(.no_ichek)').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%' // optional
                    });
                });
            });
        });

    </script>

    <style>
        .panel-heading{
            cursor: pointer;
        }
        .panel-body{
            /*display: none;*/
        }
    </style>

    <script>
        $(function () {
            $(document).on("click", ".panel-heading", function (e) {
                e.preventDefault();
                $(this).parent().find(".panel-body").slideToggle();
            });
        });
    </script>

    <style>

        div.bhoechie-tab-container{
            z-index: 10;
            background-color: #ffffff;
            padding: 0 !important;
            border:1px solid #ddd;
            -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
            box-shadow: 0 6px 12px rgba(0,0,0,.175);
            -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
            background-clip: padding-box;
            opacity: 0.97;
            filter: alpha(opacity=97);
        }

        div.bhoechie-tab-menu{
            padding-right: 0;
            padding-left: 0;
            padding-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group{
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a{
            margin-bottom: 0;
        }

        div.bhoechie-tab-menu div.list-group>a .glyphicon,
        div.bhoechie-tab-menu div.list-group>a .fa {
            color: #5A55A3;
        }

        div.bhoechie-tab-menu div.list-group>a:first-child{
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group>a:last-child{
            border-radius: 0px;
        }

        div.bhoechie-tab-menu div.list-group>a.active,
        div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group>a.active .fa{
            background-color: #337ab7;
            border-radius: 0px;
            color: #ffffff;
        }

        div.bhoechie-tab-menu div.list-group>a.active:after{
            content: '';
            position: absolute;
            right: 100%;
            top: 50%;
            margin-top: -13px;
            border-left: 0;
            border-bottom: 13px solid transparent;
            border-top: 13px solid transparent;
            border-right: 10px solid #337ab7;
        }

        div.bhoechie-tab-content{
            background-color: #ffffff;
            /* border: 1px solid #eeeeee; */
            padding-left: 20px;
            padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active){
            display: none;
        }

        .bhoechie-tab{
            padding-top: 10px;
            padding-bottom: 120px;
            min-height: 400px;
            border-right: 1px solid #ddd;
        }

        .icheckbox_square-blue{
            margin-left: 10px;
        }

    </style>

    <script>
        $(document).ready(function() {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>

@endsection