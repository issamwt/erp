@extends("company.layouts.app", ["title"=>"قائمة الإجتماعات"])

@section("content")

    <section class="content-header">
        <h1>نظام إجتماعاتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>قائمة الإجتماعات</strong>  <a href="{{route("company.meetings.add_meeting", @$company->slug)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-plus"></i> <span>إضافة إجتماع</span></a></div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#filters').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                <form id="filters" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12"><h4 style="margin-top: 0px">الفلاتر : </h4></div>

                                    <div class="col-sm-12 mb-10">
                                        <div class="input-group">
                                            <input id="search" type="text" autocomplete="off" class="form-control">
                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <div class="checkbox icheck" style="display: block"><label><input type="checkbox" class="type all" value="100" checked> كل الإجتماعات</label></div>
                                        <div class="checkbox icheck" style="display: block"><label><input type="checkbox" class="type" value="0"> جديد</label></div>
                                        <div class="checkbox icheck" style="display: block"><label><input type="checkbox" class="type" value="1"> منته</label></div>
                                        <div class="checkbox icheck" style="display: block"><label><input type="checkbox" class="type" value="2"> ملغى</label></div>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <div class="row" style="margin-bottom: 6px">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker" autocomplete="off" id="from_date" placeholder="من يوم" required style="text-align: right">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 mb-10">
                                                <div class="input-group">
                                                    <input type="text" placeholder="إلى يوم" class="form-control datepicker" autocomplete="off" id="to_date" required style="text-align: right">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <div class="row">
                                            <div class="col-sm-6"><button type="button" class="btn btn-primary btn-block" id="filter">فلتر</button></div>
                                            <div class="col-sm-6"><button type="button" class="btn btn-default btn-block" id="reset">فسخ</button></div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                </form>
                                <div class="clearfix"></div>
                                <div class="clearfix"><hr></div>

                            </div>


                            <div class="col-sm-12">

                                <table class="table table-bordered" id="tableMeetings">
                                    <thead>
                                        <tr>
                                            <th>م</th>
                                            <th>السلسلة</th>
                                            <th>موضوع الإجتماع</th>
                                            <th>مكان الإجتماع</th>
                                            <th>مدير الإجتماع</th>
                                            <th>التاريخ</th>
                                            <th>الحالة</th>
                                            <th>المهام</th>
                                            <th style="width: 100px">نسبة الإنجاز</th>
                                            <th style="width: 100px">التفاصيل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($meetings)>0)
                                            @foreach($meetings as $meeting)
                                                <tr data-serach="{{$meeting->name." ".@$meeting->location->name." ".@$meeting->manager->name}}" data-type="{{$meeting->status}}" data-date="{{$meeting->from_date}}">
                                                    <td class="text-center">{{$loop->iteration}}</td>
                                                    <td>{{@$meeting->category->name}}</td>
                                                    <td>{{$meeting->name}}</td>
                                                    <td>{{@$meeting->location->name}}</td>
                                                    <td>{{@$meeting->manager->name}}</td>
                                                    <td class="text-center">{{$meeting->from_date}}</td>
                                                    <td class="text-center">
                                                        @if($meeting->status==0)
                                                            <strong class="alert alert-success alert-xs">جديد</strong>
                                                        @elseif($meeting->status==1)
                                                            <strong class="alert alert-info alert-xs">جاري</strong>
                                                        @elseif($meeting->status==2)
                                                            <strong class="alert alert-warning alert-xs">منته</strong>
                                                        @elseif($meeting->status==3)
                                                            <strong class="alert alert-danger alert-xs">ملغى</strong>
                                                        @else
                                                            <span class="alert alert-default">coming soon</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-center" style="min-width: 140px;">
                                                        <ul class="nav nav-pills">
                                                            <li><span>{{count($meeting->finishedtasks)}}</span> تمت</li>
                                                        </ul>
                                                        <ul class="nav nav-pills">
                                                            <li><span>{{count($meeting->newtasks)}}</span> لم تتم</li>
                                                        </ul>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $tasks = count($meeting->finishedtasks)+count($meeting->newtasks);
                                                            $percentage = ($tasks==0)?0:intval(count($meeting->finishedtasks)/$tasks*100);
                                                        ?>
                                                        <h5 class="text-center progresst">{{$percentage}}%</h5>
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{$percentage}}"
                                                                 aria-valuemin="0" aria-valuemax="100" style="width:{{$percentage}}%">
                                                                <span class="sr-only">{{$percentage}}% Complete</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle">
                                                        <div class="dropdown">
                                                            <button class="btn btn-default btn-block btn-xs dropdown-toggle" type="button" data-toggle="dropdown">الأوامر <span class="caret"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}"><i class="pull-right fa fa-eye"></i> <span>عرض</span></a></li>
                                                                {{--<li><a href="{{route("company.meetings.pdf_meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}"><i class="pull-right fa fa-file-pdf-o"></i> <span>طباعة PDF</span></a></li>--}}
                                                                @if(permissions_meetings("notify_meeting"))
                                                                    <li><a href="#" data-toggle="modal" data-target="#notifyMeetingModal{{$meeting->id}}"><i class="pull-right fa fa-bell-o"></i> <span>تذكير</span></a></li>
                                                                @endif
                                                                @if(permissions_meetings("update_meeting"))
                                                                    <li><a href="{{route("company.meetings.edit_meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}"><i class="pull-right fa fa-edit"></i> <span>تعديل</span></a></li>
                                                                @endif
                                                                @if(permissions_meetings("delete_meeting"))
                                                                    <li><a href="{{route("company.meetings.delete_meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}" onclick="return confirm('هل أنت متأكد ؟')"><i class="pull-right fa fa-trash"></i> <span>حذف</span></a></li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan="9" class="text-center"><strong>لا توجد إجتماعات حاليا !</strong></td></tr>
                                        @endif
                                    </tbody>
                                </table>

                                @foreach($meetings as $meeting)
                                    @if(permissions_meetings("notify_meeting"))
                                    <div id="notifyMeetingModal{{$meeting->id}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">تنبيه الحضور</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <form action="{{route("company.meetings.notify_meeting", @$company->slug)}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$meeting->id}}">

                                                        <div class="form-group">
                                                            <label>لمن سترسل التنبيه ؟</label>
                                                            <select name="users[]" class="form-control select2x" multiple required>
                                                                <?php $userxx=[]; ?>
                                                                <option value="0">الجميع</option>
                                                                @foreach($meeting->usersx as $u)
                                                                    @if(@$u->id and @$u->name)
                                                                        <option value="{{$u->id}}">{{$u->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>


                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary" style="min-width: 100px"><i class="pull-right fa fa-bell-o"></i> <span>إرسال التنبيه</span></button>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                @endforeach

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

    <style>
        #filters{
            padding: 20px;
            border: 1px solid #eee;
        }
        #filters h4{
            margin-top: 25px;
        }
        .nav-pills{
            margin: 0px auto;
        }
        .nav-pills li{
            padding: 3px;
            width: 100%;
            font-size: 12px;
            position: relative;
        }
        .nav-pills:nth-child(1) li{
            background: chartreuse;
            border-radius: 10px;
        }
        .nav-pills li span{
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 20px;
            background: #fff;
            display: inline-block;
            border-radius: 50%;
            font-size: 0.94em;
            color: #333;
            position: absolute;
            right: 2px;
            top: 1px;
        }
        .nav-pills:nth-child(2) li{
            margin-top: 5px;
            background: crimson;
            border-radius: 10px;
            color: #fff;
        }
        .progress{
            height: 8px;
        }
        .progresst{
            margin: 0px;
            font-size: 0.7em;
        }
        .table .dropdown-menu {
            right: auto;
            left: 0px;
            text-align: center;
        }
    </style>

    <script>
        $(function () {

            $('.datepicker').datepicker({format: 'yyyy-mm-dd', rtl: true, language: 'ar', autoclose: true});

            $(document).on("click", "#reset", function () {
                $("#search").val("");
                $("#from_date").val("");
                $("#to_date").val("");
                $(".icheck .type").iCheck('uncheck');
                $(".icheck .type.all").iCheck('check');
                $("#tableMeetings tbody tr").fadeIn();
            });

            $(document).on("click", "#filter", function () {
                var search  = $("#search").val();
                var types   = $(".type:checked").map(function(){return parseInt($(this).val());}).toArray();
                var from    = $("#from_date").val();
                var to      = $("#to_date").val();
                console.clear();
                console.log(from);
                console.log(to);
                var format = 'YYYY-MM-DD';

                $("#tableMeetings tbody tr").fadeOut();
                $("#tableMeetings tbody tr").filter(function( index ) {
                    var searchx = true;
                    if(search!=""){
                        var s = $(this).data("serach");
                        if (s.indexOf(search) < 0)
                            searchx = false;
                    }

                    var typex = true;
                    if($.inArray(100, types)=== -1){
                        var t = $(this).data("type");
                        if ($.inArray(t, types)=== -1)
                            typex = false;

                        console.log(types);
                        console.log(t);
                        console.log(typex);
                        console.log("------------");
                    }

                    var datex = true;
                    if(from!="" && to!=""){
                        var date    = moment($(this).data("date")).format(format);
                        from    = moment(from).format(format);
                        to      = moment(to).format(format);
                        if(date<from || date>to)
                            datex = false;
                    }

                    return searchx && typex && datex;
                }).fadeIn();
            });

        });
    </script>

@endsection