<div class="row">
    <div class="col-sm-12" style="padding: 15px; padding-bottom: 7px">
        <div class="col-sm-12">
            <div id="kanaban-container" class="row">
                <div class="col-sm-6">
                    <div class="kanban-col-title" style="background:crimson;">مهام لم تتم</div>
                    <ul data-status="0" id="sortable1" class="connectedSortable">
                        @foreach($tasks as $task)
                            @if($task->status==0)
                                <?php $userss = [@$task->user->id]; foreach($task->users as $u) array_push($userss, $u->id); $userss = implode(";", $userss).";"; ?>
                                <a data-id="{{$task->id}}" data-category="{{@$task->meeting->category_id}}" data-status="{{$task->status}}" data-meeting="{{$task->daftar_id}}" data-users="{{$userss}}" data-date="{{$task->thedate}}"
                                   href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}" class="kanban-item kanban-sortable-chosen ui-state-default" >
                                                                        <span class="avatar" data-toggle="tooltip" title="{{@$task->user->name}}">
                                                                            @if(@$task->user->image)
                                                                                <img src="{{asset("storage/app/".@$task->user->image)}}" class="mCS_img_loaded">
                                                                            @else
                                                                                <img src="{{asset("public/noimage.png")}}" class="mCS_img_loaded">
                                                                            @endif
                                                                        </span>
                                    <span>{{$loop->iteration}} .</span> <span>{{$task->name}}</span>
                                </a>
                            @endif
                        @endforeach
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-6">
                    <div class="kanban-col-title" style="background:chartreuse;">مهام تمت</div>
                    <ul  data-status="1" id="sortable2" class="connectedSortable">
                        @foreach($tasks as $task)
                            @if($task->status!=0)
                                <?php $userss = [@$task->user->id]; foreach($task->users as $u) array_push($userss, $u->id); $userss = implode(";", $userss); ?>
                                <a data-id="{{$task->id}}" data-category="{{@$task->meeting->category_id}}" data-status="{{$task->status}}" data-meeting="{{$task->daftar_id}}" data-users="{{$userss}}" data-date="{{$task->thedate}}"
                                   href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}" class="kanban-item kanban-sortable-chosen  ui-state-default" >
                                <span class="avatar" data-toggle="tooltip" title="{{@$task->user->name}}">
                                                                            @if(@$task->user->image)
                                        <img src="{{asset("storage/app/".@$task->user->image)}}" class="mCS_img_loaded">
                                    @else
                                        <img src="{{asset("public/noimage.png")}}" class="mCS_img_loaded">
                                    @endif
                                                                        </span>
                                <span>{{$loop->iteration}} .</span> <span>{{$task->name}}</span>
                                </a>
                            @endif
                        @endforeach
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>