@extends("company.layouts.app", ["title"=>"قائمة المهام"])

@section("content")

    <section class="content-header">
        <h1>نظام إجتماعاتي</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                <div class="panel panel-primary">
                    <div class="panel-heading"><strong>قائمة المهام</strong></div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="javascript:void(0)" onclick="$('#fomme').slideToggle();" class="btn btn-primary pull-left" style="width: 105px"> الفلاتر </a>

                                <form id="fomme" class="row" action="javascript:void(0);" style="display: none; padding: 20px; border: 1px solid #eee; width: 300px; background: #eee; border: 1px solid #dadada; position: absolute; top: 36px; left: 15px; z-index: 333">

                                    <div class="col-sm-12">
                                        <strong style="font-size: 1.25em;">الفلاتر : </strong>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="form-control select2Status">
                                            <option value="all">-الحالة-</option>
                                            <option value="0">لم تتم</option>
                                            <option value="1">تمت</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="form-control select2Categories">
                                            <option value="all">-السلسلة-</option>
                                            @foreach($categories as $c)
                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="form-control select2Meeting">
                                            <option value="all">-الإجتماع-</option>
                                            @foreach($meetings as $m)
                                                <option value="{{$m->id}}">{{$m->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <select class="form-control select2User">
                                            <option value="all">-عضو الفريق-</option>
                                            @foreach($users as $u)
                                                <option value="{{$u->id}}">{{$u->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datepicker dateFrom" placeholder="التاريخ من" style="border: 1px solid #ababab;height: 35px;text-align: right;">
                                    </div>

                                    <div class="col-sm-12 mb-10">
                                        <input type="text" class="form-control datepicker dateTo" placeholder="التاريخ إلى" style="border: 1px solid #ababab;height: 35px;text-align: right;">
                                    </div>

                                </form>

                            </div>

                            <div class="col-sm-12">

                                <div class="clearfix"><hr></div>

                                <div id="task-wrapper" style="border: none; padding: 0px; margin: 0px;">

                                    <div id="task-div">

                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#list">قائمة المهام</a></li>
                                            <li><a data-toggle="tab" id="kanabanDiv" href="#kanaban">لوحة المهام</a></li>
                                            <li><a data-toggle="tab" id="GanttDiv" href="#gantt">مخطط المهام</a></li>
                                        </ul>

                                        <div class="tab-content" style="padding-top: 20px;">

                                            <!-- LIST -->
                                            <div id="list" class="tab-pane fade in active">
                                                @include("company.meetings.tasks.list")
                                            </div>

                                            <!-- KANABAN -->
                                            <div id="kanaban" class="tab-pane fade" style="background: #e5e9ec;">
                                                @include("company.meetings.tasks.kanaban")
                                            </div>

                                            <!-- GANTT -->
                                            <div id="gantt" class="tab-pane fade" style="background: #e5e9ec;">
                                                <div class="w100p pt10" style="min-height: 500px;">
                                                    <div id="gantt-chart" style="width: 100%;"></div>
                                                    <div id="scroll-to-me"></div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                                @foreach($tasks as $task)

                                    <div id="showTaskNoteModal{{$task->id}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">{{$task->name}}</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="row mb-15">
                                                        <div class="col-sm-3"><strong>إسم المهمة</strong></div>
                                                        <div class="col-sm-9">{{$task->name}}</div>
                                                    </div>

                                                    <div class="row mb-15">
                                                        <div class="col-sm-3"><strong>حالة المهمة</strong></div>
                                                        <div class="col-sm-9">
                                                            @if($task->status==0)
                                                                <div><b>لم تتم</b></div>
                                                            @else
                                                                <div><b>تمت</b></div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="row mb-15">
                                                        <div class="col-sm-3"><strong>المكلف</strong></div>
                                                        <div class="col-sm-9">
                                                            @foreach($users as $user)
                                                                @if($user->id==$task->user_id) <span style="margin-left: 10px">{{$user->name}}</span> @endif
                                                            @endforeach
                                                        </div>
                                                    </div>

                                                    <div class="row mb-15">
                                                        <div class="col-sm-3"><strong>المساعدون</strong></div>
                                                        <div class="col-sm-9">
                                                            <?php $userss=[]; foreach ($task->users as $u): array_push($userss, $u->id); endforeach; ?>
                                                            @foreach($users as $user)
                                                                @if(in_array($user->id, $userss)) <span style="margin-left: 5px">[{{$user->name}}]</span> @endif
                                                            @endforeach
                                                        </div>
                                                    </div>

                                                    <div class="row mb-15">
                                                        <div class="col-sm-3"><strong>التاريخ</strong></div>
                                                        <div class="col-sm-9">{{$task->thedate}}</div>
                                                    </div>

                                                    <div class="row mb-15">
                                                        <div class="col-sm-3"><strong>التفاصيل</strong></div>
                                                        <div class="col-sm-9 text-justify">{{$task->note}}</div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="editTaskeModal{{$task->id}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">تعديل مهمة</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{route("company.meetings.update_task", @$company->slug)}}" method="post" class="update_task" data-id="{{$task->id}}">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$task->id}}">

                                                        <div class="row mb-15">
                                                            <div class="col-sm-3"><strong>إسم المهمة</strong></div>
                                                            <div class="col-sm-9"><input autocomplete="off" type="text" value="{{$task->name}}" class="form-control" name="name" required></div>
                                                        </div>

                                                        <div class="row mb-15">
                                                            <div class="col-sm-3"><strong>المكلف</strong></div>
                                                            <div class="col-sm-9">
                                                                <select class="form-control" name="user_id" required>
                                                                    <option value="">إختر</option>
                                                                    @foreach($users as $user)<option value="{{$user->id}}" @if($user->id==$task->user_id) selected @endif>{{$user->name}}</option>@endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row mb-15">
                                                            <div class="col-sm-3"><strong>المساعدون</strong></div>
                                                            <div class="col-sm-9">
                                                                <?php $userss=[]; foreach ($task->users as $u): array_push($userss, $u->id); endforeach; ?>
                                                                <select name="users[]" class="form-control select2x" multiple>
                                                                    @foreach($users as $user)<option value="{{$user->id}}" @if(in_array($user->id, $userss)) selected @endif>{{$user->name}}</option>@endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row mb-15">
                                                            <div class="col-sm-3"><strong>التاريخ</strong></div>
                                                            <div class="col-sm-9"><input value="{{$task->thedate}}" type="text" class="form-control datepicker" autocomplete="off" style="text-align: right" name="thedate"></div>
                                                        </div>

                                                        <div class="row mb-15">
                                                            <div class="col-sm-3"><strong>التفاصيل</strong></div>
                                                            <div class="col-sm-9"><textarea class="form-control" name="note" rows="5" required>{{$task->note}}</textarea></div>
                                                        </div>

                                                        <div class="row mb-15">
                                                            <div class="col-sm-3">
                                                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <span>تعديل</span></button>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section("scripts")

    <style>
        div:not(.modal) .select2-selection{
            border-radius: 0px !important;
            padding: 8px 12px !important;
            height: 36px !important;
        }
        div:not(.modal) .select2-container{
            margin-top: 3px;
        }
        .kanban-col-title {
            padding: 10px 15px;
            font-weight: bold;
            color: #fff;
            margin-bottom: 15px;
            margin-right: 3px;
        }
        #kanaban{
            min-height: 200px;
        }
        .kanban-item {
            padding: 10px;
            margin: 0 3px 10px 0;
            background-color: #fff;
            cursor: default;
            display: table;
            color: #4e5e6a;
            width: 100%;
            cursor: pointer;
        }
        .kanban-item .avatar {
            float: left;
        }
        .avatar {
            display: inline-block;
            white-space: nowrap;
        }
        .kanban-item .avatar img {
            height: 22px;
            width: 22px;
            margin-right: 10px;
        }
        .avatar img {
            height: auto;
            max-width: 100%;
            border-radius: 50%;
        }
        .connectedSortable{
            padding: 0px;
            margin: 0px;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $(function () {

            ( function( factory ) {
                if ( typeof define === "function" && define.amd ) {

                    // AMD. Register as an anonymous module.
                    define( [ "../widgets/datepicker" ], factory );
                } else {

                    // Browser globals
                    factory( jQuery.datepicker );
                }
            }( function( datepicker ) {

                datepicker.regional.ar = {
                    closeText: "إغلاق",
                    prevText: "&#x3C;السابق",
                    nextText: "التالي&#x3E;",
                    currentText: "اليوم",
                    monthNames: [ "يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو",
                        "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر" ],
                    monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
                    dayNames: [ "الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت" ],
                    dayNamesShort: [ "أحد", "اثنين", "ثلاثاء", "أربعاء", "خميس", "جمعة", "سبت" ],
                    dayNamesMin: [ "ح", "ن", "ث", "ر", "خ", "ج", "س" ],
                    weekHeader: "أسبوع",
                    dateFormat: "yy-mm-dd",
                    firstDay: 0,
                    isRTL: true,
                    showMonthAfterYear: false,
                    yearSuffix: "" };
                datepicker.setDefaults( datepicker.regional.ar );

                return datepicker.regional.ar;

            } ) );
            
            function reloading2(){
                $('.datepicker').datepicker( "option", "dateFormat", "yy-mm-dd");   
            }

            function sortablee(){
                $( "#sortable1, #sortable2, #sortable3").sortable({
                    connectWith: ".connectedSortable",
                    receive : function (event, ui) {
                        var status = $(this).data("status");
                        var id = $(ui.item).data("id");
                        $( "#task-wrapper" ).css("opacity", "0.7");
                        $("#task-wrapper").html($("#loading-wrapper").html());
                        $.get('{{route("company.meetings.change_task_status", @$company->slug)}}/'+id+'/'+status, function () {
                            $("#task-wrapper").load(window.location + " #task-div", function () {
                                goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                                reloading(); reloading2();
                                sortablee();
                                $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                                $("#kanabanDiv").trigger("click");
                            });
                        });
                    },
                });
            }
            sortablee();
        });
    </script>

    <script>

        $(function () {

            $(".select2Status, .select2User, .select2Meeting, .select2Categories").select2({dir: "rtl", language: "ar", placeholder : "الحالة" });

            $(document).on("submit", "#save_task", function (e) {
                e.preventDefault();
                var form = $(this);
                $("button", form).prop("disabled", true);
                $( "#task-wrapper" ).css("opacity", "0.7");
                $("#task-wrapper").html($("#loading-wrapper").html());
                $.post('{{route("company.meetings.save_task", @$company->slug)}}', $(form).serialize(), function () {
                    $("#task-wrapper").load(window.location + " #task-div", function () {
                        goNotif("success", "تم إضافة المهمة بنجاح !");
                        reloading(); reloading2();
                        $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                        $(form).trigger("reset");
                        $("button", form).prop("disabled", false);
                    });
                });
            });

            $(document).on("submit", ".update_task", function (e) {
                e.preventDefault()
                var form = $(this);
                var id = $(this).data("id");
                $("#editTaskeModal"+id).modal("hide");
                setTimeout(function () {
                    $("button", form).prop("disabled", true);
                    $( "#task-wrapper" ).css("opacity", "0.7");
                    $("#task-wrapper").html($("#loading-wrapper").html());
                    $.post('{{route("company.meetings.update_task", @$company->slug)}}', $(form).serialize(), function () {
                        $("#task-wrapper").load(window.location + " #task-div", function () {
                            goNotif("success", "تم تعديل المهمة بنجاح !");
                            reloading(); reloading2();
                            $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                            $("button", form).prop("disabled", false);
                        });
                    });
                }, 1000);
            });

            $(document).on("click", ".delete_task", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    $( "#task-wrapper" ).css("opacity", "0.7");
                    $("#task-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.delete_task", @$company->slug)}}/'+id, function () {
                        $("#task-wrapper").load(window.location + " #task-div", function () {
                            goNotif("success", "تم حذف المهمة بنجاح !");
                            reloading(); reloading2();
                            $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            $(document).on("click", ".change_task_status", function (e) {
                e.preventDefault();
                if(confirm('هل أنت متأكد ؟')){
                    var id = $(this).data("id");
                    var status = $(this).data("status");
                    $( "#task-wrapper" ).css("opacity", "0.7");
                    $("#task-wrapper").html($("#loading-wrapper").html());
                    $.get('{{route("company.meetings.change_task_status", @$company->slug)}}/'+id+'/'+status, function () {
                        $("#task-wrapper").load(window.location + " #task-div", function () {
                            goNotif("success", "تم تغيير حالة المهمة بنجاح !");
                            reloading(); reloading2();
                            $( "#task-wrapper" ).animate({opacity: 1,}, 1000);
                        });
                    });
                }

            });

            $(document).on("change", ".select2Status, .select2User, .select2Meeting, .select2Categories, .dateFrom, .dateTo", function () {
                var status  = $(".select2Status").val();
                var meeting = $(".select2Meeting").val()+"";
                var category = $(".select2Categories").val();
                var user    = parseInt($(".select2User").val());
                var dateFrom= $(".dateFrom").val();
                var dateTo  = $(".dateTo").val();


                $("#dataTableX tbody tr, .kanban-item").fadeOut();
                $("#dataTableX tbody tr, .kanban-item").filter(function (i, elem) {
                    var statusT = $(elem).data("status");
                    var statusX = true;
                    if(status!="all")
                        if(statusT!=status)
                            statusX = false;

                    var meetingT = $(elem).data("meeting")+"";
                    var meetingX = true;
                    if(meeting!="all")
                        if(meetingT!=meeting)
                            meetingX = false;


                    var categoryT = $(elem).data("category");
                    var categoryX = true;
                    if(category!="all")
                        if(categoryT!=category)
                            categoryX = false;

                    var usersP = $(elem).data("users")+"";
                    usersP = usersP.split(";")
                    var userX = false;
                    if(user)
                        $.each(usersP, function( index, value ) {
                            if(parseInt(user)==parseInt(value))
                                userX=true;
                        });
                    else
                        userX=true;

                    var dateT = $(elem).data("date");
                    var dateX = true;
                    var dateT = new Date(dateT);
                    var d1    = new Date(dateFrom);
                    var d2    = new Date(dateTo);

                    if(dateFrom!="" && dateTo==""){
                        if(dateT<d1)
                            dateX = false;
                    } else if(dateFrom=="" && dateTo!=""){
                        if(dateT>d2)
                            dateX = false;
                    } else if(dateFrom!="" && dateTo!=""){
                        if(dateT>d2 || dateT<d1)
                            dateX = false;
                    } else if(dateFrom=="" && dateTo==""){
                        dateX = true;
                    }

                    return (statusX && categoryX && meetingX && userX && dateX);
                }).fadeIn();
            });

        });
    </script>

    <link rel="stylesheet" href="{{asset("public/plugins/gantt-chart/gantt.css")}}">
    <script src="{{asset("public/plugins/gantt-chart/gantt.js")}}"></script>

    <script>
        var months = ["يناير","فبراير","مارس","أبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"];
        var colors = ["#367fa9", "#e08e0b", "#00a65a"];
        var meetings = [];
        @foreach($meetings as $meeting)
            var tasks = [];
            @foreach($tasks as $task)
                @if($task->daftar_id==$meeting->id)
                    var start = moment("{{$task->thedate}}").format('YYYY-MM-DD');
                    var url = '<a href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}">{{$task->name}}</a>';
                    var task = { "id":{{$task->id}}, "name" : url, "start" : start, "end" : start, "color" : '@if($task->status==0) crimson @else chartreuse @endif'};
                    tasks.push(task);
                @endif
            @endforeach
            var meeting = { "id" : {{$meeting->id}}, "name" : "{{$meeting->name}}", "series": tasks };
            meetings.push(meeting);
        @endforeach


        $("#gantt-chart").html("<div style='height:100px;'></div>");
        $("#gantt-chart").ganttView({
            data: meetings,
            monthNames: months,
            dayText: "يوم",
            daysText: "أيام"
        });
    </script>

    <style>
        /*div.ganttview-grid, div.ganttview-grid-row-cell, div.ganttview-hzheader-day, div.ganttview-hzheader-month, div.ganttview-vtheader{
            float: right;
        }
        div.ganttview-vtheader-group-name{
            padding-right: 20px;
        }
        div.ganttview-vtheader-group-name:before{
            transform: rotateX(30deg);
        }*/
    </style>

@endsection