<table class="table table-bordered" id="dataTableX">
    <thead>
    <tr>
        <th style="min-width: 80px">التاريخ</th>
        <th>الإسم</th>
        <th>الإجتماع</th>
        <th>المكلف</th>
        <th>المساعدون</th>
        <th>الحالة</th>
        <th>الأوامر</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tasks as $task)
        <?php if($task->status==0) $color = "#367fa9"; elseif ($task->status==1) $color = "#e08e0b"; elseif($task->status==2) $color="#00a65a"; else $color="#000"; ?>
        <?php $userss = [@$task->user->id]; foreach($task->users as $u) array_push($userss, $u->id); $userss = implode(";", $userss).";"; ?>
        <tr data-id="{{$task->id}}" data-status="{{$task->status}}" data-meeting="{{$task->daftar_id}}" data-category="{{@$task->meeting->category_id}}" data-users="{{$userss}}" data-date="{{$task->thedate}}">
            <td width="10%" class="text-center">{{$task->thedate}}</td>
            <td><a href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}">{{$task->name}}</a></td>
            <td>
                <a href="{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>@$task->meeting->id])}}">{{@$task->meeting->name}}</a>
            </td>
            <td>
                <?php $url = ""; if(@$task->user->type==0) $url = route("company.users.show", ["comapny"=>$company->slug, "id"=>@$task->user->id]); else $url = route("company.clients.show", ["comapny"=>$company->slug, "id"=>@$task->user->id]); ?>
                <a href="{{$url}}" style="display: inline-block; margin-left: 5px;">
                    @if(@$task->user->image)
                        <img src="{{asset("storage/app/".@$task->user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                    @else
                        <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                    @endif
                    <h6 class="pull-right" style="margin: 10px 10px 0px 01px;color: #204c65">{{@$task->user->name}}</h6>
                </a>
            </td>
            <td>
                @if(count($task->users))
                    @foreach($task->users as $user)
                        <?php $url = ""; if(@$user->type==0) $url = route("company.users.show", ["comapny"=>$company->slug, "id"=>@$user->id]); else $url = route("company.clients.show", ["comapny"=>$company->slug, "id"=>@$user->id]); ?>
                        <a href="{{$url}}" data-toggle="tooltip" data-placement="top" title="{{@$user->name}}" style="display: inline-block; margin-left: 5px;">
                            @if(@$user->image)
                                <img src="{{asset("storage/app/".@$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                            @else
                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; float: right;">
                            @endif
                        </a>
                    @endforeach
                @else
                    <span>لا يوجد</span>
                @endif
            </td>
            <td  width="80px">
                @if($task->status==0)
                    <ul class="nav nav-pills"><li style="background: crimson;border-radius: 10px;color: #fff;display: block;width: 100%;text-align: center;">لم تمت</li></ul>
                @else
                    <ul class="nav nav-pills"><li style="background: chartreuse;border-radius: 10px;display: block;width: 100%;text-align: center;">تمت</li></ul>
                @endif
            </td>
            <td class="text-center" style="vertical-align: middle;">
                <a class="mt-15 btn-xs btn btn-info btn-50" href="#" data-toggle="modal" data-target="#showTaskNoteModal{{$task->id}}" title="المشاهدة"><span class="fa fa-eye"></span></a>
                @if(permissions_meetings("update_task"))
                    <a class="mt-15 btn-xs btn btn-primary btn-50" href="#" data-toggle="modal" data-target="#editTaskeModal{{$task->id}}" title="التعديل"><span class="fa fa-edit"></span></a>
                @else
                    <a class="mt-15 btn-xs btn btn-primary btn-50 disabled" href="#" title="التعديل"><span class="fa fa-edit"></span></a>
                @endif
                @if(permissions_meetings("update_task"))
                    @if($task->status==0)
                        <a class="mt-15 btn-xs btn btn-success btn-50 change_task_status" data-id = "{{$task->id}}" data-status="1" href="#" data-toggle="tooltip" title="تمت"><span class="fa fa-check"></span></a>
                    @else
                        <a class="mt-15 btn-xs btn btn-danger btn-50 change_task_status" data-id = "{{$task->id}}" data-status="0" href="#" data-toggle="tooltip" title="لم تمت"><span class="fa fa-remove"></span></a>
                    @endif
                @endif
                @if(permissions_meetings("delete_task"))
                    <a class="delete_task mt-15 btn-xs btn btn-warning btn-50" data-id="{{$task->id}}" href="#" data-toggle="tooltip" data-placement="top"  title="الحذف"><span class="fa fa-remove"></span></a>
                @else
                    <a class="mt-15 btn-xs btn btn-warning btn-50 disabled" href="#" data-toggle="tooltip" data-placement="top"  title="الحذف"><span class="fa fa-remove"></span></a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>