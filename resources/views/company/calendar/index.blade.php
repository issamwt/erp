@extends("company.layouts.app", ["title"=>"التقويم و الأحداث"])

@section("content")

    <section class="content-header">
        <h1>التقويم و الأحداث</h1>
    </section>

    <section class="content">
        <div class="row">
            
            <div class="col-xs-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>التقويم</strong>
                        @if(permissions_general("calendar_system"))
                            <a href="#" class="pull-left btn btn-default btn-xs btn-open" data-target="#addEvenet"><i class="fa fa-plus"></i> <span>إضافة حدث</span></a>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            @if(permissions_general("calendar_system"))
                            <div class="col-sm-12">

                                <form class="formi" id="addEvenet" action="{{route("company.save_calendar", $company->slug)}}" method="post" enctype="multipart/form-data" style="padding: 10px 20px 10px 20px">
                                    @csrf
                                    <input type="hidden" value="{{$company->id}}" name="company_id" id="company_id" class="form-control" required>

                                    <div class="row">

                                        <div class="col-sm-10">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-2" for="name">الإسم <span class="red">*</span></label>
                                                <input type="text" value="{{old('name')}}" name="name" id="name" class="form-control col-sm-10" autocomplete="off" required>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-sm-5">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-4" for="start_date">تاريخ البداية <span class="red">*</span></label>
                                                <input type="text" autocomplete="off" value="{{old('start_date')}}" name="start_date" id="start_date" class="form-control col-sm-8 datee" required>
                                            </div>

                                        </div>

                                        <div class="col-sm-5">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-4" for="start_time">وقت البداية <span class="red">*</span></label>
                                                <input type="text" autocomplete="off" value="{{old('start_time')}}" name="start_time" id="start_time" class="timee form-control col-sm-8" required>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-sm-5">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-4" for="end_date">تاريخ الإنتهاء <span class="red">*</span></label>
                                                <input type="text" autocomplete="off" value="{{old('end_date')}}" name="end_date" id="end_date" class="form-control col-sm-8 datee" required>
                                            </div>

                                        </div>

                                        <div class="col-sm-5">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-4" for="end_time">وقت الإنتهاء <span class="red">*</span></label>
                                                <input type="text" autocomplete="off" value="{{old('end_time')}}" name="end_time" id="end_time" class="timee form-control col-sm-8" required>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-sm-10">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-2" for="description">الوصف <span class="red">*</span></label>
                                                <textarea rows="6" value="{{old('description')}}" name="description" id="description" class="form-control col-sm-10" required></textarea>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-sm-10">
                                            <div class="form-group mt-20">
                                                <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>
                            @endif

                            <div class="col-sm-12">
                                <div id="calendar"></div>
                            </div>

                        </div>

                    </div>

                </div>

                @if(permissions_general("calendar_system"))

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة الأحداث</b>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="25px">م</th>
                                            <th>الحدث</th>
                                            <th>تاريخ البداية</th>
                                            <th>تاريخ النهاية</th>
                                            <th>الأوامر</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($events as $event)
                                            <tr>
                                                <td class="text-center">{{$loop->iteration}}</td>
                                                <td>{{$event->name}}</td>
                                                <td class="text-center">{{$event->start_date}}<br>{{$event->start_time}}</td>
                                                <td class="text-center">{{$event->end_date}}<br>{{$event->end_time}}</td>
                                                <td class="text-center">
                                                    <a class="btn btn-xs btn-info btn-open" data-target="#showEvent{{$event->id}}" href="#" data-toggle="modal" title="مشاهدة"><i class="fa fa-eye"></i></a>
                                                    <a class="btn btn-xs btn-success btn-open" data-target="#editEvent{{$event->id}}" href="#" data-toggle="tooltip" title="تعديل"><i class="fa fa-edit"></i></a>
                                                    <a class="btn btn-xs btn-danger" href="{{route("company.delete_event", [$company->slug, $event->id])}}" onclick="return confirm('هل أنت متأكد ؟')" data-toggle="tooltip" title="حذف"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" class="formi" id="editEvent{{$event->id}}">
                                                    <form action="{{route("company.update_event", $company->slug)}}" method="post" enctype="multipart/form-data" style="padding: 0px 0px 40px 20px">
                                                        @csrf
                                                        <input type="hidden" value="{{$event->id}}" name="id" class="form-control" required>

                                                        <div class="row">

                                                            <div class="col-sm-10">

                                                                <div class="form-group row mt-10">
                                                                    <label class="col-sm-2" for="name">الإسم <span class="red">*</span></label>
                                                                    <input type="text" value="{{$event->name}}" name="name" id="name" class="form-control col-sm-10" autocomplete="off" required>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="col-sm-5">

                                                                <div class="form-group row mt-10">
                                                                    <label class="col-sm-4" for="start_date">تاريخ البداية <span class="red">*</span></label>
                                                                    <input type="text" autocomplete="off" value="{{$event->start_date}}" name="start_date" id="start_date" class="form-control col-sm-8 datee" required>
                                                                </div>

                                                            </div>

                                                            <div class="col-sm-5">

                                                                <div class="form-group row mt-10">
                                                                    <label class="col-sm-4" for="start_time">وقت البداية <span class="red">*</span></label>
                                                                    <input type="text" autocomplete="off" value="{{$event->start_time}}" name="start_time" id="start_time" class="timee form-control col-sm-8" required>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="col-sm-5">

                                                                <div class="form-group row mt-10">
                                                                    <label class="col-sm-4" for="end_date">تاريخ الإنتهاء <span class="red">*</span></label>
                                                                    <input type="text" autocomplete="off" value="{{$event->end_date}}" name="end_date" id="end_date" class="form-control col-sm-8 datee" required>
                                                                </div>

                                                            </div>

                                                            <div class="col-sm-5">

                                                                <div class="form-group row mt-10">
                                                                    <label class="col-sm-4" for="end_time">وقت الإنتهاء <span class="red">*</span></label>
                                                                    <input type="text" autocomplete="off" value="{{$event->end_time}}" name="end_time" id="end_time" class="timee form-control col-sm-8" required>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="col-sm-10">

                                                                <div class="form-group row mt-10">
                                                                    <label class="col-sm-2" for="description">الوصف <span class="red">*</span></label>
                                                                    <textarea rows="6" name="description" id="description" class="form-control col-sm-10" required>{{$event->description}}</textarea>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>

                                                            <div class="col-sm-10">
                                                                <div class="form-group mt-20">
                                                                    <button type="submit" class="btn btn-primary" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </form>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>


                            </div>

                            <div class="col-sm-5"></div>

                        </div>

                    </div>
                </div>

                @endif

                @foreach($events as $event)
                    <div id="showEvent{{$event->id}}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">{{$event->name}}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-3">تاريخ البداية : </div>
                                        <div class="col-sm-3">{{$event->start_date}}</div>
                                        <div class="col-sm-3">وقت البداية : </div>
                                        <div class="col-sm-3">{{$event->start_time}}</div>
                                    </div>
                                    <div class="row mt-10">
                                        <div class="col-sm-3">تاريخ الإنتهاء : </div>
                                        <div class="col-sm-3">{{$event->end_date}}</div>
                                        <div class="col-sm-3">وقت الإنتهاء : </div>
                                        <div class="col-sm-3">{{$event->end_time}}</div>
                                    </div>
                                    <div class="row mt-20 mb-30">
                                        <div class="col-sm-3">الوصف : </div>
                                        <div class="col-sm-9">{{$event->description}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

@endsection

@section("scripts")


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.min.css"  media="print"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/ar.js"></script>

    <script>
        $(function () {

            $('.datee').datepicker({
                format: "yyyy-mm-dd",
                language:"ar",
                autoclose: true,
                todayHighlight: true
            });

            $('.timee ').timepicker();

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            var colors = ["#00a65a", "#00c0ef", "#f39c12", "#dd4b39"];

            $('#calendar').fullCalendar({
                locale: 'ar',
                events: [
                    @foreach($events as $event)
                        {
                            title: 'حدث :  {{$event->name}}',
                            start: '{{$event->start_date}}',
                            end: '{{$event->end_date}}',
                            url: '',
                            backgroundColor: "#346BFF",
                            borderColor: "#fff",
                            description : "{{$event->description}}",
                            id : '{{$event->id}}',
                        },
                    @endforeach

                    @foreach($meetings as $meeting)
                        {
                            title: 'إجتماع : {{$meeting->name}}',
                            start: '{{$meeting->from_date}}',
                            url: '{{route("company.meetings.meeting", ["company"=>$company->slug, "id"=>$meeting->id])}}',
                            backgroundColor: colors[{{$meeting->status}}],
                            borderColor: "#fff",
                        },
                    @endforeach
                ],
                eventClick:  function(event, jsEvent, view) {
                    if(event.id)
                        $('#showEvent'+event.id).modal();
                },
            });

        });
    </script>
    <style>
        .fc-content{
            padding: 3px;
            border-radius: 0px !important;
            cursor: pointer;
        }
        .fc-day-grid-event{
            border-radius: 0px !important;
        }
    </style>

@endsection