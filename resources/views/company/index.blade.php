<html>
    <head>
        <title>نظام ERP</title>
        <link rel="icon" type="image/png" href="{{asset("public/uploads/favicon.png")}}" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    </head>
    <body>

    <div id="wrapper" class="animate">
        <nav class="navbar header-top fixed-top navbar-expand-lg">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route("home")}}">الصفحة الرئيسية</a>
                        </li>
                        <li class="nav-item">
                            <div style="padding:8px;">{{@$company->name}}</div>
                        </li>
                        <li class="nav-item">
                            <div style="padding:8px;">{{@$user->name}}</div>
                        </li>
                        <li class="nav-item" style="margin: 0px 20px">
                            <a class="nav-link" href="{{route("company.logout", @$company->slug)}}"><i class="fas fa-sign-out-alt"></i> <span>خروج</span></a>
                        </li>
                    </ul>
                </div>
                <a class="navbar-brand" href="#"><small>ERP</small> <strong>نظام</strong></a>
            </div>
        </nav>

        <div class="container-fluid" style="text-align: justify; direction: rtl;">

            <div class="row">

                @if(permissions_general("meetings_system"))
                @if($company->meetings)
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #EAA737;border-color: #EAA737;">
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام إجتماعاتي</h4>
                                <a href="{{route("company.meetings.statistics", @$company->slug)}}"><img src="{{asset("public/uploads/company/meeting.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="{{route("company.meetings.statistics", @$company->slug)}}"><b>دخول الآن</b></a>
                            </div>
                        </div>
                    </div>
                @endif
                @endif

                    @if(permissions_general("prm_system"))
                    @if($company->prm)
                        <div class="col-sm-3">
                            <div class="card" style="box-shadow: 0 3px 5px #006EEB;border-color: #006EEB;">
                                <div class="card-body">
                                    <h4 class="card-title text-center">نظام إدارة المشاريع</h4>
                                    <a href="{{route("company.prm.statistics", @$company->slug)}}"><img src="{{asset("public/uploads/company/group.png")}}"></a>
                                    <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                    <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                    <strong>{{@$company->name}}</strong>
                                    <a style="float: left;" href="{{route("company.prm.statistics", @$company->slug)}}"><b>دخول الآن</b></a>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endif

                    @if(permissions_general("passwords_system"))
                    @if($company->passwords)
                        <div class="col-sm-3">
                            <div class="card" style="box-shadow: 0 3px 5px #913CC1;border-color: #913CC1;">
                                <div class="card-body">
                                    <h4 class="card-title text-center">نظام باسوورداتي</h4>
                                    <a href="{{route("company.passwords.websites", @$company->slug)}}"><img src="{{asset("public/uploads/company/password.png")}}"></a>
                                    <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                    <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                    <strong>{{@$company->name}}</strong>
                                    <a style="float: left;" href="{{route("company.passwords.websites", @$company->slug)}}"><b>دخول الآن</b></a>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endif

                @if(permissions_general("hrm_system"))
                @if($company->hrm)
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #92DD66;border-color: #92DD66;">
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام إدارة الموارد البشرية</h4>
                                <a href="{{route("company.hrm.statistics", @$company->slug)}}"><img src="{{asset("public/uploads/company/projects.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted text-center">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="{{route("company.hrm.statistics", @$company->slug)}}"><b>دخول الآن</b></a>
                            </div>
                        </div>
                    </div>
                @endif
                @endif

                @if($company->crm)
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #DC0028;border-color: #DC0028;">
                            <div class="coming-soon">Coming Soon</div>
                            <div class="card-body">
                                <h4 class="card-title text-center">نظام إدارة علاقات العملاء</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/crm.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b>دخول الآن</b></a>
                            </div>
                        </div>
                    </div>
                @endif

                @if($company->cards)
                    <div class="col-sm-3">
                        <div class="card" style="box-shadow: 0 3px 5px #29CEFC;border-color: #29CEFC;">
                            <div class="coming-soon">Coming Soon</div>
                            <div class="card-body">
                                <h4 class="card-title text-center">بطاقة إهداءاتي</h4>
                                <a href="#"><img src="{{asset("public/uploads/company/gift.png")}}"></a>
                                <h6 class="card-subtitle mb-2 text-muted">خلافاَ للإعتقاد السائد فإن لوريم إيبسوم ليس نصاَ عشوائياً</h6>
                                <p class="card-text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.</p>
                                <strong>{{@$company->name}}</strong>
                                <a style="float: left;" href="#"><b>دخول الآن</b></a>
                            </div>
                        </div>
                    </div>
                @endif

            </div>

        </div>

    </div>

    <style>
        .navbar{
            background: #fff;
        }
        body{background:#f9f9f9;}
        #wrapper{padding:90px 15px; margin-bottom: 100px}
        .navbar-expand-lg .navbar-nav.side-nav{flex-direction: column;}
        .card{margin-bottom: 15px; border-radius:0; box-shadow: 0 3px 5px rgba(0,0,0,.1); }
        .header-top{box-shadow: 0 3px 5px rgba(0,0,0,.1)}
        .leftmenutrigger{display: none}
        @media(min-width:992px) {
            .leftmenutrigger{display: block;display: block;margin: 7px 20px 4px 0;cursor: pointer;}
            #wrapper{padding: 90px 15px 15px 15px; }
            .navbar-nav.side-nav.open {left:0;}
            .navbar-nav.side-nav{background: #585f66;box-shadow: 2px 1px 2px rgba(0,0,0,.1);position:fixed;top:56px;flex-direction: column!important;left:-220px;width:200px;overflow-y:auto;bottom:0;overflow-x:hidden;padding-bottom:40px}
        }
        .animate{-webkit-transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out;-o-transition:all .3s ease-in-out;-ms-transition:all .3s ease-in-out;transition:all .3s ease-in-out}
        .card-body img{
            margin: 10px auto 30px;
            display: block;
            border: 1px solid #d2d2d2;
            width: 100px;
        }
        .col{
            margin-top: 20px;
        }
        .card-title{
            margin-top: 5px;
            margin-bottom: 20px;
        }
        .card{
            position: relative;
            overflow: hidden;
        }
        .card .coming-soon{
            position: absolute;
            background: #d00202;
            padding: 2px 10px;
            width: 150px;
            text-align: center;
            color: #fff;
            font-size: 0.8em;
            top: 25px;
            left: -40px;
            transform: rotate(-45deg);
        }
    </style>

    </body>
</html>
