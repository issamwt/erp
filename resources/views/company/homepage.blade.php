@extends("company.layouts.app", ["title"=>"الرئيسية"])

@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 100px;">

                @section("content")

                    <section class="content-header">
                        <h1>الرئيسية</h1>
                    </section>

                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12" style="margin-bottom: 100px;">

                                <h3 class="text-center" style="margin-top: 100px;">صفحة الإحصائيات : قيد التنفيذ</h3>

                            </div>
                        </div>
                    </section>

                @endsection


            </div>
        </div>
    </section>

@endsection
