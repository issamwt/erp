<!-- jQuery 2.1.4 -->
<script src="{{asset("public/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- Bootstrap 3.3.4 -->
<script src="{{asset("public/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- DataTables -->
<script src="{{asset("public/plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("public/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- SlimScroll -->
<script src="{{asset("public/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("public/plugins/fastclick/fastclick.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{asset("public/dist/js/app.min.js")}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset("public/dist/js/demo.js")}}"></script>

<!-- iCheck -->
<link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">
<script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>
<!-- Select 2 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<!-- timepicker &  datepicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>
<!-- iCheck -->
<script src="{{asset("public/front/vendor/notif/notifIt.min.js")}}"></script>
<link rel="stylesheet" href="{{asset("public/front/vendor/notif/notifIt.min.css")}}">

<link rel="stylesheet" href="{{asset("public/plugins/iCheck/square/blue.css")}}">
<script src="{{asset("public/plugins/iCheck/icheck.min.js")}}"></script>

<div id="loading-wrapper" style="">
    <div id="loading"><i class="fa fa-spinner fa-spin fa-5x"></i></div>
</div>

<script src="{{asset("public/jquery.playSound.js")}}"></script>
<audio controls style="display: none">
    <source src="{{asset("public/notif.mp3")}}" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

<script>

    var dataTableVal = null;

    function reloading(){

        $('.datepicker').datepicker({format: 'yyyy-mm-dd', rtl: true, language: 'ar', autoclose: true});

        $(".timepicker").timepicker({ showMeridian:false, minuteStep:1, icons:{up: 'fa fa-chevron-up', down: 'fa fa-chevron-down'}});

        $(".select2x, .select-hide").select2({dir: "rtl", language: "ar", placeholder : "إختر"});

        if (dataTableVal){
            //dataTableVal.fnClearTable();
            dataTableVal.fnDestroy();
        }

        dataTableVal = $("#dataTable, .dataTable").dataTable(
            {
                retrieve: true,
                language: {
                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر  _MENU_  مدخلات ",
                    "sZeroRecords": "لم يعثر على أية سجلات",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخلات",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "البحث : ",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
                , "pageLength": 25
            }
        );

        $('input:not(.no_ichek)').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    }

    $(function () {

        reloading();

        setTimeout(function () {
            $(".alert-msg").fadeOut();
        }, 3000);

        function goNotif(type, text){
            notif({msg: text, type: type});
            $.playSound('{{asset("public/notif.mp3")}}');
        }

        @if(count($errors)>0)
            @foreach($errors->all() as $error)
                goNotif("error", "{{$error}}");
            @endforeach
        @endif

        @if(session()->has('error'))
            goNotif("error", "{{session('error')}}");
        @endif

        @if(session()->has('success'))
            goNotif("success", "{!! session('success') !!}");
        @endif

    });
</script>

<style>
    .dataTables_length, .dataTables_info{
        float: left;
    }
    .panel, .panel-heading, .panel-body, .btn{
        border-radius: 0px;
    }
    #dataTable th, .dataTable th{
         text-align: right;
     }
    .form-control {
        border-radius: 0px !important;
    }
    .mt-0{
        margin-top: 0px !important;
    }
    .mt-5{
        margin-top: 5px;
    }
    .mt-10{
        margin-top: 10px;
    }
    .mt-15{
        margin-top: 15px;
    }
    .mt-20{
        margin-top: 20px;
    }
    .mt-30{
        margin-top: 30px;
    }
    .mt-50{
        margin-top: 50px;
    }
    .mb-0{
        margin-bottom: 0px;
    }
    .mb-5{
        margin-bottom: 5px;
    }
    .mb-10{
        margin-bottom: 10px;
    }
    .mb-15{
        margin-bottom: 15px;
    }
    .mb-20{
        margin-bottom: 20px;
    }
    .mb-30{
        margin-bottom: 30px;
    }
    .mb-50{
        margin-bottom: 50px;
    }
    .ml-10{
        margin-left: 10px;
    }
    .bootstrap-timepicker-widget{
         direction: ltr !important;
         right: auto !important;
     }
    .select2-container{
        width: 100% !important;
    }
    .select2-container--default .select2-selection--multiple{
        border-radius: 0px;
        border-color: #d2d6de;
    }
    #dataTable_length, .dataTables_length, .dataTables_info{
        float: right !important;
    }
    #dataTable_filter, #dataTable_paginate, .dataTables_paginate, .dataTables_filter{
        float: left;
    }
    #ui_notifIt p{
        font-size: 1.2em;
        font-weight: bold;
    }
    .stars{
        padding: 0px;
        margin: 0px;
        list-style: none;
    }
    .stars li{
        float: right;
        padding-left: 5px;
    }
    .stars li .fa.active{
        color: gold;
    }
    .stars li .fa:not(.active){
        color: dimgray;
    }
    .stars li .fa-2x {
        font-size: 1.6em;
    }

    .alert-xs{
        padding: 1px 5px !important;
        display: block;
        width: 100%;
        border-radius: 0px !important;
        text-align: center;
        margin: 0px auto;
    }

    .alert-block{
        display: block !important;
        width: inherit !important;
    }

    .alert-primary{
        color: #fff;
        background-color: #3c8dbc;
        border-color: #367fa9;
    }

     select.form-control{
        padding: 3px;
    }

    span.red{
        color: crimson;
    }

    .checkbox.icheck label{
        padding: 0px;
        font-weight: bold;
        font-size: 1.1em;
    }
    .checkbox label{
        padding-right: initial;
        padding-left: 0px;
    }
    .checkbox label .iradio_square-blue{
        margin-left: 10px;
    }
    .checkbox label .icheckbox_square-blue{
        margin-left: 10px;
    }
    .btn-50{
        width: 54px;
    }
    #loading{
        text-align: center;
        padding: 30px 0px;
    }
    #loading .fa{
        color: #286090;
        font-size: 8em;
    }
    #task-wrapper{
        border: 1px solid #eee;
        padding: 20px;
        margin-top: 25px;
    }
    #loading-wrapper{
        display: none;
    }
    .w200{
        width: 200px !important;
    }
    .select2-container--default .select2-selection--single{
        border-radius: 0px;
        border:1px solid #d2d6de;
    }
    .select2-container .select2-selection--single{
        height: 34px;
    }
    .btn{
        border-radius: 0px !important;
    }
    .formi{
        display: none;
        border: 1px solid #2d6a8d;
        padding: 15px 20px;
        margin: 15px auto;
    }
    .datepicker{
        z-index: 999999 !important;
    }
</style>

<script src="https://js.pusher.com/4.3/pusher.min.js"></script>

<script>
    function goNotif(type, text){
        notif({msg: text, type: type});
    }
    {{init()}}
    //Pusher.logToConsole = true;
    var pusher = new Pusher("{{config("pusher.connections.main.auth_key")}}", {
        cluster: 'eu'
    });
    var channel = pusher.subscribe('my-channel');
    channel.bind('all', function(data) {
        $.playSound('{{asset("public/notif.mp3")}}');
        goNotif("info", data.message);
    });
    channel.bind('user_{{Auth::guard("user")->user()->id}}', function(data) {
        $.playSound('{{asset("public/notif.mp3")}}');
        goNotif("info", data.message);
    });
    channel.bind('accept_meeting', function(data) {
        if(data.company_id=={{Auth::guard("user")->user()->company_id}} && data.user_id!={{Auth::guard("user")->user()->id}}){
            $.playSound('{{asset("public/notif.mp3")}}');
            goNotif("info", data.message);
            setTimeout(function () {
                window.location.href ="{{route('company.meetings.meeting', @$company->slug)}}/"+data.meeting_id;
            }, 5000);
        }
    });
    channel.bind('end_meeting', function(data) {
        if(data.company_id=={{Auth::guard("user")->user()->company_id}} && data.user_id!={{Auth::guard("user")->user()->id}}){
            $.playSound('{{asset("public/notif.mp3")}}');
            goNotif("info", data.message);
            setTimeout(function () {
                $("#addEvaluationModal").modal("show");
                $("#buttons-wrapper").load(window.location + " #buttons-div")
            }, 1000);
        }
    });
</script>




@section("scripts")@show