<aside class="main-sidebar">

    <section class="sidebar">
        <ul class="sidebar-menu">

            <li>
                @if(@$company->image)
                    <img src="{{asset("storage/app/".@$company->image)}}" style="display: block; margin: 10px auto; width: 90px; height:90px;border-radius: 50%; border: 3px solid #fff;">
                @else
                    <img src="{{asset("public/uploads/logo2.png")}}" style="display: block; margin: 10px auto;  width: 90px; height:90px; border-radius: 50%;border: 3px solid #fff;" >
                @endif
                <h5 class="text-center mt-20" style="color: #fff;">{{@$company->name}}</h5>
            </li>

            <li>
                <hr style="margin-bottom: 10px;border-color: #4a4a4a !important">
            </li>

            <li class="@if(Request::url()==route("company.homepage", @$company->slug)) active @endif">
                <a href="{{route("company.homepage", @$company->slug)}}">
                    <i class="fa fa-dashboard"></i> <span>الصفحة الرئيسية</span></i>
                </a>
            </li>

            <li class="@if(Request::url()==route("company.calendar", @$company->slug)) active @endif">
                <a href="{{route("company.calendar", @$company->slug)}}">
                    <i class="fa fa-calendar"></i> <span>التقويم و الأحداث</span></i>
                </a>
            </li>

            <li class="@if(Request::url()==route("company.inbox", @$company->slug) or Request::url()==route("company.outbox", @$company->slug) or Request::url()==route("company.draft", @$company->slug)) active @endif">
                <a href="{{route("company.inbox", @$company->slug)}}">
                    <i class="fa fa-envelope"></i> <span>الرسائل</span></i>
                </a>
            </li>

            @if(!$company->hrm)
                <li class="@if(Request::url()==route("company.users.index", @$company->slug)) active @endif">
                    <a href="{{route("company.users.index", @$company->slug)}}">
                        <i class="fa fa-users"></i> <span>قائمة الموظفين</span></i>
                    </a>
                </li>
            @endif

            <li class="@if(Request::url()==route("company.clients.index", @$company->slug)) active @endif">
                <a href="{{route("company.clients.index", @$company->slug)}}">
                    <i class="fa fa-star"></i> <span>قائمة العملاء</span></i>
                </a>
            </li>

            @if(permissions_general("meetings_system"))
            @if($company->meetings)
                <li class="treeview @if(Request::url()==route("company.meetings.statistics", @$company->slug) or Request::url()==route("company.meetings.add_meeting", @$company->slug)
                 or Request::url()==route("company.meetings.meetings", @$company->slug) or Request::url()==route("company.meetings.tasks", @$company->slug)
                 or Request::url()==route("company.meetings.settings", @$company->slug)) active @endif">
                    <a href="#">
                        <i class="fa fa-comments-o"></i>
                        <span>نظام إجتماعاتي</span>
                        <span class="pull-left"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(Request::url()==route("company.meetings.statistics", @$company->slug)) active @endif">
                            <a href="{{route("company.meetings.statistics", @$company->slug)}}"><i class="fa fa-bar-chart"></i> الإحصائيات</a>
                        </li>
                        <li class="@if(Request::url()==route("company.meetings.meetings", @$company->slug)) active @endif">
                            <a href="{{route("company.meetings.meetings", @$company->slug)}}"><i class="fa fa-list"></i> قائمة الإجتماعات</a>
                        </li>
                        <li class="@if(Request::url()==route("company.meetings.tasks", @$company->slug)) active @endif">
                            <a href="{{route("company.meetings.tasks", @$company->slug)}}"><i class="fa fa-tasks"></i> قائمة المهام</a>
                        </li>
                        @if(permissions_meetings("add_meeting"))
                            <li class="@if(Request::url()==route("company.meetings.add_meeting", @$company->slug)) active @endif">
                                <a href="{{route("company.meetings.add_meeting", @$company->slug)}}"><i class="fa fa-plus"></i> إضافة إجتماع</a>
                            </li>
                        @endif
                        @if(permissions_general("meetings_settings"))
                            <li class="@if(Request::url()==route("company.meetings.settings", @$company->slug)) active @endif">
                                <a href="{{route("company.meetings.settings", @$company->slug)}}"><i class="fa fa-gear"></i> إعدادات إجتماعاتي</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @endif

            @if(permissions_general("prm_system"))
            @if($company->prm)
                <li class="treeview @if(Request::url()==route("company.prm.statistics", @$company->slug) or Request::url()==route("company.prm.projects", @$company->slug)
                or Request::url()==route("company.prm.add_project", @$company->slug) or Request::url()==route("company.prm.tasks", @$company->slug)
                or Request::url()==route("company.prm.settings", @$company->slug)) active @endif">
                    <a href="#">
                        <i class="fa fa-list"></i>
                        <span>نظام إدارة المشاريع</span>
                        <span class="pull-left"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(Request::url()==route("company.prm.statistics", @$company->slug)) active @endif">
                            <a href="{{route("company.prm.statistics", @$company->slug)}}"><i class="fa fa-bar-chart"></i> الإحصائيات</a>
                        </li>
                        <li class="@if(Request::url()==route("company.prm.projects", @$company->slug)) active @endif">
                            <a href="{{route("company.prm.projects", @$company->slug)}}"><i class="fa fa-list"></i> قائمة المشاريع</a>
                        </li>
                        <li class="@if(Request::url()==route("company.prm.tasks", @$company->slug)) active @endif">
                            <a href="{{route("company.prm.tasks", @$company->slug)}}"><i class="fa fa-tasks"></i> قائمة المهام</a>
                        </li>
                        @if(permissions_prm("add_project"))
                            <li class="@if(Request::url()==route("company.prm.add_project", @$company->slug)) active @endif">
                                <a href="{{route("company.prm.add_project", @$company->slug)}}"><i class="fa fa-plus"></i> إضافة مشروع</a>
                            </li>
                        @endif
                        @if(permissions_general("prm_settings"))
                            <li class="@if(Request::url()==route("company.prm.settings", @$company->slug)) active @endif">
                                <a href="{{route("company.prm.settings", @$company->slug)}}"><i class="fa fa-gear"></i> إعدادات إدارة المشاريع</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @endif

            @if(permissions_general("passwords_system"))
            @if($company->passwords)
                <li class="treeview @if(Request::url()==route("company.passwords.websites", @$company->slug) or Request::url()==route("company.passwords.settings", @$company->slug)) active @endif">
                    <a href="#">
                        <i class="fa fa-lock"></i>
                        <span>نظام باسوورداتي</span>
                        <span class="pull-left"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(Request::url()==route("company.passwords.websites", @$company->slug)) active @endif">
                            <a href="{{route("company.passwords.websites", @$company->slug)}}"><i class="fa fa-list"></i> قائمة المواقع</a>
                        </li>
                        @if(permissions_passwords("add_website"))
                            <li class="@if(Request::url()==route("company.passwords.add_website", @$company->slug)) active @endif">
                                <a href="{{route("company.passwords.add_website", @$company->slug)}}"><i class="fa fa-plus"></i> إضافة موقع</a>
                            </li>
                        @endif
                        @if(permissions_general("passwords_settings"))
                            <li class="@if(Request::url()==route("company.passwords.settings", @$company->slug)) active @endif">
                                <a href="{{route("company.passwords.settings", @$company->slug)}}"><i class="fa fa-gear"></i> إعدادات باسوورداتي</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @endif

            @if(permissions_general("hrm_system"))
            @if($company->hrm)
                <li class="treeview @if(Request::url()==route("company.hrm.statistics", @$company->slug) or Request::url()==route("company.hrm.insurance_n_finance_infos", @$company->slug)
                or Request::url()==route("company.hrm.employees", @$company->slug)
                or Request::url()==route("company.hrm.attendances", @$company->slug)
                or Request::url()==route("company.hrm.payroll", @$company->slug)
                or Request::url()==route("company.hrm.vacations", @$company->slug)
                or Request::url()==route("company.hrm.evaluations", @$company->slug)
                or Request::url()==route("company.hrm.settings", @$company->slug)) active @endif">
                    <a href="#">
                        <i class="fa fa-user-secret"></i>
                        <span>نظام إدارة الموارد البشرية</span>
                        <span class="pull-left"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(Request::url()==route("company.hrm.statistics", @$company->slug)) active @endif">
                            <a href="{{route("company.hrm.statistics", @$company->slug)}}"><i class="fa fa-bar-chart"></i> الإحصائيات</a>
                        </li>
                        @if(permissions_hrm("employees"))
                            <li class="@if(Request::url()==route("company.hrm.employees", @$company->slug)) active @endif">
                                <a href="{{route("company.hrm.employees", @$company->slug)}}"><i class="fa fa-users"></i> إدارة الموظفين</a>
                            </li>
                        @endif
                        @if(permissions_hrm("insurance_n_finance_infos"))
                            <li class="@if(Request::url()==route("company.hrm.insurance_n_finance_infos", @$company->slug)) active @endif">
                                <a href="{{route("company.hrm.insurance_n_finance_infos", @$company->slug)}}"><i class="fa fa-gears"></i> التأمينات و المعلومات المالية</a>
                            </li>
                        @endif
                        @if(permissions_hrm("evaluations") or permissions_hrm("attendances") or permissions_hrm("vacationsss") or permissions_hrm("payroll"))
                        <li class="treeview @if(Request::url()==route("company.hrm.attendances", @$company->slug) or Request::url()==route("company.hrm.payroll", @$company->slug)
                        or Request::url()==route("company.hrm.vacations", @$company->slug) or Request::url()==route("company.hrm.evaluations", @$company->slug)) active @endif">
                            <a href="#">
                                <i class="fa fa-briefcase"></i>
                                <span>الإدارات</span>
                                <span class="pull-left"><i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                @if(permissions_hrm("evaluations"))
                                    <li class="@if(Request::url()==route("company.hrm.evaluations", @$company->slug)) active @endif">
                                        <a href="{{route("company.hrm.evaluations", @$company->slug)}}"><i class="fa fa-circle"></i>إدارة التقييمات</a>
                                    </li>
                                @endif
                                @if(permissions_hrm("attendances"))
                                    <li class="@if(Request::url()==route("company.hrm.attendances", @$company->slug)) active @endif">
                                        <a href="{{route("company.hrm.attendances", @$company->slug)}}"><i class="fa fa-circle"></i>إدارة الحضور و الإنصراف</a>
                                    </li>
                                @endif
                                @if(permissions_hrm("vacationsss"))
                                    <li class="@if(Request::url()==route("company.hrm.vacations", @$company->slug)) active @endif">
                                        <a href="{{route("company.hrm.vacations", @$company->slug)}}"><i class="fa fa-circle"></i>إدارة الإجازات </a>
                                    </li>
                                @endif
                                @if(permissions_hrm("payroll"))
                                    <li class="@if(Request::url()==route("company.hrm.payroll", @$company->slug)) active @endif">
                                        <a href="{{route("company.hrm.payroll", @$company->slug)}}"><i class="fa fa-circle"></i>إدارة حساب الراتب (payroll)</a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        @endif
                        @if(permissions_general("hrm_settings"))
                            <li class="@if(Request::url()==route("company.hrm.settings", @$company->slug)) active @endif">
                                <a href="{{route("company.hrm.settings", @$company->slug)}}"><i class="fa fa-gear"></i> إعدادات الموارد البشرية</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            @endif

            <li class="treeview @if(Request::url()==route("company.general_settings", @$company->slug)
              or Request::url()==route("company.messages_archive", @$company->slug)) active @endif">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>الإعدادات</span>
                    <span class="pull-left"><i class="fa fa-angle-down"></i></span>
                </a>
                <ul class="treeview-menu">
                    @if(permissions_general("general_settings"))
                        <li class="@if(Request::url()==route("company.general_settings", @$company->slug)) active @endif">
                            <a href="{{route("company.general_settings", @$company->slug)}}"><i class="fa fa-gear"></i> إعدادات الشركة</a>
                        </li>
                    @endif
                    @if(permissions_general("messages_archive"))
                        <li class="@if(Request::url()==route("company.messages_archive", @$company->slug)) active @endif">
                            <a href="{{route("company.messages_archive", @$company->slug)}}"><i class="fa fa-envelope"></i> ارشيف الرسائل</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{route("company.logout", @$company->slug)}}"><i class="fa fa-sign-out"></i> تسجيل الخروج</a>
                    </li>
                </ul>
            </li>

        </ul>
    </section>

</aside>