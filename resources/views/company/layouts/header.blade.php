<header class="main-header">
    <!-- Logo -->
    <a href="{{route("admin.home")}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b style="font-size: 0.8em">ERP</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>نظام ERP</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        @if($company->hrm)
            <a href="{{route("company.hrm.save_attendance", @$company->slug)}}" class="attendance-btn" style="@if(@$present) background:red; border-color: red; @endif">
                @if(@$present)
                    <i class="fa fa-user" style="margin-left: 6px"></i><span>تسجيل الخروج</span>
                @else
                    <i class="fa fa-user" style="margin-left: 6px"></i><span>تسجيل الحضور</span>
                @endif
            </a>
        @endif
        @if($company->hrm)
            <a href="{{route("company.hrm.accounting", @$company->slug)}}" class="attendance-btn">
                <i class="fa fa-star" style="margin-left: 6px"></i><span>المسائلات</span>
            </a>
        @endif
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">{{count($new_notifications)}}</span>
                    </a>
                    <ul class="dropdown-menu" style="text-align: right !important;">
                        @if(count($new_notifications)>0)
                            <li class="header">توجد {{count($new_notifications)}} إشعارات جديدة</li>
                        @else
                            <li class="header">لا توجد إشعارات أخرى</li>
                        @endif
                        <li>
                            <ul class="menu">
                                @foreach($new_notifications as $n)
                                    <li>
                                        <a href="{{route("company.notifications", $company->slug)}}">
                                            <i class="fa fa-bell-o text-yellow"></i> <span>{{@$n->notif->title}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer"><a href="{{route("company.notifications", $company->slug)}}">شاهد الجميع</a></li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::guard("user")->user()->image)
                            <img src="{{asset("storage/app/".Auth::guard("user")->user()->image)}}" class="user-image" alt="User Image">
                        @else
                            <img src="{{asset("public/noimage.png")}}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{Auth::guard("user")->user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(Auth::guard("user")->user()->image)
                                <img src="{{asset("storage/app/".Auth::guard("user")->user()->image)}}" class="img-circle" alt="User Image">
                            @else
                                <img src="{{asset("public/noimage.png")}}" class="img-circle" alt="User Image">
                            @endif
                            <p>
                                {{Auth::guard("user")->user()->name}}
                                <small>إدارة نظام إجتماعاتي</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{route("company.general_settings", @$company->slug)}}" class="btn btn-default btn-flat">الإعدادات</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route("company.logout", @$company->slug)}}" class="btn btn-default btn-flat">تسجيل الخروج</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{url('/')}}" target="_blank"><i class="fa fa-globe" style="font-size: 18px;"></i></a>
                </li>
                <li>
                    <a href="{{route("company.home", @$company->slug)}}" target="_blank"><i class="fa fa-home" style="font-size: 18px;"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<style>
    .attendance-btn{
        margin-top: 10px;
        margin-bottom: 10px;
        display: inline-block;
        border: 1px solid #fff;
        padding: 5px 15px;
        border-radius: 20px;
        color: #fff;
        transition: all linear .2s;
    }
    .attendance-btn:hover{
        color: #318CB8;
        background: #fff;
    }
</style>