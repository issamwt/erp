<!DOCTYPE html>
<html>
@include("company.layouts.head")
<body class="skin-blue sidebar-mini"  dir="rtl" style="direction: rtl;">
<div class="wrapper">

    @include("company.layouts.header")

    @include("company.layouts.sidebar")

    <div class="content-wrapper">

        @include("company.layouts.messages")

        @section("content")@show

    </div>

    @include("company.layouts.footer")

</div>

@include("company.layouts.foot")

</body>
</html>