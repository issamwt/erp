<div class="col-sm-8 col-sm-offset-2">
    <form action="{{route("company.sendEmail", $company->slug)}}" id="addEmail" class="formi" method="post" enctype="multipart/form-data" style="background: #fff">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <h5 class="mt-5 mb-5">إرسال رسالة</h5>
            </div>
        </div>
        <div class="form-group row mt-20 mb-20">
            <div class="col-sm-3"><label><span>إلى</span> <span class="red">*</span></label></div>
            <div class="col-sm-9">
                <select required name="to_id" class="form-control select2x">
                    <option value="">إلى</option>
                    @foreach($users as $u)
                        <option value="{{$u->id}}" @if(old('to_id')==$u->id) selected @endif>{{$u->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row mt-20">
            <div class="col-sm-3"><label><span>الموضوع</span> <span class="red">*</span></label></div>
            <div class="col-sm-9">
                <input required name="subject" value="{{old('subject')}}" class="form-control" placeholder="الموضوع ...">
            </div>
        </div>
        <div class="form-group row mt-20">
            <div class="col-sm-12">
                <textarea required name="message" class="form-control" placeholder="اكتب رسالة..." rows="10">{{old('message')}}</textarea>
            </div>
        </div>
        <div class="form-group row mt-20 mb-5">
            <div class="col-sm-3">
                <label class="btn btn-primary btn-block">
                    <i class="fa fa-camera"></i> <span>رفع الملف</span>
                    <input type="file" name="image" accept="image/*" style="display: none;">
                </label>
            </div>
            <div class="col-sm-5"></div>
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-send"></i> <span>إرسال</span></button>
            </div>
            <div class="col-sm-2">
                <button type="button" data-target="#addEmail" class="btn-close btn btn-default btn-block"><i class="fa fa-remove"></i> <span>إغلاق</span></button>
            </div>
        </div>
    </form>
</div>