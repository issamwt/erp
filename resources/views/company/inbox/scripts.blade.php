<style>
    .boxz{
        box-shadow: 0 1px 1px rgba(131, 131, 131, 0.55);
        background: #fff;
        width: 100%;
        min-height: 300px;
        border: 1px solid #e7e8ea;
        padding: 10px 15px;
    }
    .oxi{
        list-style: none;
        padding: 0px;
        margin: 0px;
        margin-bottom: 15px;
    }
    .oxi li{
        display: inline-block;
    }
    .oxi li a{
        color: #333;
        display: block;
        margin-left: 12px;
        font-size: 0.80em;
        font-weight: bold;
    }
    .oxi li a.active{
        color: #1b4b72;
    }
    .emails{
        list-style: none;
        padding: 0px;
        margin: 0px;
    }
    .emails li{
        cursor: pointer;
        padding: 5px 0px;
        display: block;
        margin-bottom: 3px;
    }
    .emails li:hover{
        background: #eee;
    }
</style>

<script>
    $(function () {

        $(document).on("click", ".btn-open", function (e) {
            e.preventDefault();
            var id = $(this).data("target");
            $(id).slideToggle();
        });

        $(document).on("click", ".btn-close", function (e) {
            e.preventDefault();
            var id = $(this).data("target");
            $(id).slideUp();
        });

        $(document).on("click", ".emails li", function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            $('.x1').fadeIn(10);
            $('.x2').html();
            $.get("{{route("company.email_details", $company->slug)}}/"+id, function (data) {
                $('.x1').fadeOut(10);
                $('.x2').html(data);
            });
        })

    });
</script>

