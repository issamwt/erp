@extends("company.layouts.app", ["title"=>"قسم الرسائل"])

@section("content")

    <section class="content-header">
        <h1>قسم الرسائل</h1>
    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>الرسائل المحذوفة</strong>
                        <a href="#" class="pull-left btn btn-default btn-xs btn-open" data-target="#addEmail"><i class="fa fa-send"></i> <span>إنشاء رسالة</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body" style="background: #E5E9EC; padding: 20px; padding-bottom: 50px">

                        <div class="row">

                            @include("company.inbox.send")

                            <div class="col-sm-5">
                                <div class="boxz">
                                    <ul class="oxi">
                                        <li><a href="#" data-target="#addEmail" class="btn-open"><i class="fa fa-send"></i> <span>إنشاء</span></a></li>
                                        <li><a href="{{route("company.inbox", $company->slug)}}"><i class="fa fa-inbox"></i> <span>الرسائل الواردة</span></a></li>
                                        <li><a href="{{route("company.outbox", $company->slug)}}"><i class="fa fa-envelope-o"></i> <span>الرسائل المرسلة</span></a></li>
                                        <li><a href="{{route("company.draft", $company->slug)}}" class="active"><i class="fa fa-trash"></i> <span>الرسائل المحذوفة</span></a></li>
                                        <li class="clearfix"></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div class="boxz">
                                    @include("company.inbox.email")
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

@section("scripts")

    <style>
        .boxz{
            box-shadow: 0 1px 1px rgba(131, 131, 131, 0.55);
            background: #fff;
            width: 100%;
            min-height: 300px;
            border: 1px solid #e7e8ea;
            padding: 10px 15px;
        }
        .oxi{
            list-style: none;
            padding: 0px;
            margin: 0px;
        }
        .oxi li{
            display: inline-block;
        }
        .oxi li a{
            color: #333;
            display: block;
            margin-left: 12px;
            font-size: 0.83em;
            font-weight: bold;
        }
        .oxi li a.active{
            color: #1b4b72;
        }
    </style>

    <script>
        $(function () {

            $(document).on("click", ".btn-open", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideToggle();
            });

            $(document).on("click", ".btn-close", function (e) {
                e.preventDefault();
                var id = $(this).data("target");
                $(id).slideUp();
            });

        });
    </script>

@endsection
