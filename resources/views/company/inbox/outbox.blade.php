@extends("company.layouts.app", ["title"=>"قسم الرسائل"])

@section("content")

    <section class="content-header">
        <h1>قسم الرسائل</h1>
    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>الرسائل المرسلة</strong>
                        <a href="#" class="pull-left btn btn-default btn-xs btn-open" data-target="#addEmail"><i class="fa fa-send"></i> <span>إنشاء رسالة</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body" style="background: #E5E9EC; padding: 20px; padding-bottom: 50px">

                        <div class="row">

                            @include("company.inbox.send")

                            <div class="col-sm-5">
                                <div class="boxz">
                                    <ul class="oxi">
                                        <li><a href="#" data-target="#addEmail" class="btn-open"><i class="fa fa-send"></i> <span>إنشاء</span></a></li>
                                        <li><a href="{{route("company.inbox", $company->slug)}}"><i class="fa fa-inbox"></i> <span>الرسائل الواردة</span></a></li>
                                        <li><a href="{{route("company.outbox", $company->slug)}}" class="active"><i class="fa fa-envelope-o"></i> <span>الرسائل المرسلة</span></a></li>
                                        <li><a href="{{route("company.draft", $company->slug)}}"><i class="fa fa-trash"></i> <span>الرسائل المحذوفة</span></a></li>
                                        <li class="clearfix"></li>
                                    </ul>

                                    @if(count($emails)>0)
                                        <ul class="emails">
                                            @foreach($emails as $email)
                                                <li class="row" data-id="{{$email->id}}">
                                                <div class="col-sm-1">
                                                    @if(@$email->to->image)
                                                        <img data-toggle="tooltip" title="{{$email->to->name}}" src="{{asset("storage/app/".$email->to->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                                    @else
                                                        <img data-toggle="tooltip" title="{{$email->to->name}}" src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                                    @endif
                                                </div>
                                                    <div class="col-sm-7">
                                                        <h6 style="margin: 14px 10px 0px 10px">{{mb_substr($email->subject, 0, 32)}}</h6>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <small class="pull-left mt-15">{{$email->created_at->format("H:i:s Y-m-d")}}</small>
                                                        @if($email->image)
                                                            <small class="pull-right mt-15"><i class="fa fa-paperclip"></i></small>
                                                        @endif
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <h5 class="text-center">لا توجد رسائل</h5>
                                    @endif

                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div class="boxz">
                                    @include("company.inbox.email")
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

@section("scripts")

    @include("company.inbox.scripts")

@endsection
