@extends("company.layouts.app", ["title"=>"قائمة الموظفين"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة الموظفين
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>قائمة الموظفين</b>
                        @if(permissions_general("add_user"))
                            <a class="btn btn-default btn-xs pull-left" href="{{route("company.users.create", @$company->slug)}}"><i class="fa fa-plus"></i> <span>إضافة موظف</span></a>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table id="dataTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px"></th>
                                    <th>الصورة</th>
                                    <th>الإسم</th>
                                    <th>المسمى</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الحالة</th>
                                    <th>الأوامر</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>
                                            @if($user->image)
                                                <img src="{{asset("storage/app/".$user->image)}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                            @else
                                                <img src="{{asset("public/noimage.png")}}" style="width: 40px; height: 40px; border-radius: 50%; border: 1px solid #eee; margin: 0px auto; display: block;">
                                            @endif
                                        </td>
                                        <td style="vertical-align: middle;">{{$user->name}}</td>
                                        <td style="vertical-align: middle;">{{@$user->role->name}}</td>
                                        <td style="vertical-align: middle;">{{$user->email}}</td>
                                        <td style="vertical-align: middle;">
                                            @if($user->status==1)
                                                <strong class="alert alert-success alert-xs" style="width: 90%;">فعال</strong>
                                            @else
                                                <strong class="alert alert-danger alert-xs" style="width: 90%;">موقوف</strong>
                                            @endif
                                        </td>
                                        <td style="vertical-align: middle;">
                                            @if(permissions_general("show_user"))
                                                <a class="btn btn-info btn-xs" href="{{route("company.users.show", ["company"=>@$company->slug, "user"=>$user->id])}}"><i class="fa fa-eye"></i> <span>عرض</span></a>
                                            @endif
                                            @if(permissions_general("update_user"))
                                                <a class="btn btn-success btn-xs" href="{{route("company.users.edit", ["company"=>@$company->slug, "user"=>$user->id])}}"><i class="fa fa-edit"></i> <span>تعديل</span></a>
                                            @endif
                                            @if(permissions_general("delete_user"))
                                                <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="document.getElementById('deleteform{{$user->id}}').submit()"><i class="fa fa-trash"></i> <span>حذف</span></a>
                                                <form id="deleteform{{$user->id}}" action="{{route("company.users.destroy", ["company"=>@$company->slug, "user"=>$user->id])}}" method="post">@csrf {{method_field("DELETE")}}</form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection