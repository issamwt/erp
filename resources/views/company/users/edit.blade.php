@extends("company.layouts.app", ["title"=>"تعديل موظف"])

@section("content")

    <section class="content-header">
        <h1>
            قائمة الموظفين
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>تعديل موظف</b>
                        <a class="btn btn-default btn-xs pull-left" href="{{route("company.users.index", @$company->slug)}}"><i class="fa fa-list"></i> <span>قائمة الموظفين</span></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

                                <form action="{{route("company.users.update", ["company"=>@$company->slug, "user"=>$user->id])}}" method="post" enctype="multipart/form-data" style="padding: 0px 0px 40px 20px">
                                    @csrf
                                    {{method_field("PUT")}}

                                    <div class="row">

                                        <div class="col-sm-6">

                                            <div class="form-group row mt-10">
                                                <label class="col-sm-4" for="name">الإسم <span class="red">*</span></label>
                                                <input type="text" value="{{$user->name}}" name="name" id="name" class="form-control col-sm-8" required>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="role">المسمى <span class="red">*</span></label>
                                                <select name="role" id="role" class="form-control col-sm-8" required>
                                                    <option value="">إختر</option>
                                                    <option value="1" @if($user->role_id==1) selected @endif>أدمن</option>
                                                    @foreach($roles as $role)
                                                        <option value="{{$role->id}}" @if($user->role_id==$role->id) selected @endif>{{$role->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="image">الصورة </label>
                                                <input type="file" accept="image/*" name="image" id="image" class="form-control col-sm-8">
                                                <div class="col-sm-4"></div>
                                                <div class="col-sm-8 row mt-20">
                                                    @if($user->image)
                                                        <img src="{{asset("storage/app/".$user->image)}}" style="width: 120px; height: 120px; border: 1px solid #eee;">
                                                    @else
                                                        <img src="{{asset("public/noimage.png")}}" style="width: 120px; height: 120px; border: 1px solid #eee;">
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-6">

                                            <div class="form-group mt-10 row">
                                                <label class="col-sm-4" for="email">البريد الإلكتروني <span class="red">*</span></label>
                                                <input type="email" value="{{$user->email}}" name="email" id="email" class="form-control col-sm-8" required>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="phone">رقم الجوال <span class="red">*</span></label>
                                                <input type="text" value="{{$user->phone}}" name="phone" id="phone" class="form-control col-sm-8" required>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="status"> الحالة <span class="red">*</span></label>
                                                <select  name="status" id="status" class="form-control col-sm-8" required>
                                                    <option value="">إختر</option>
                                                    <option value="1" @if($user->status==1) selected @endif>فعال</option>
                                                    <option value="0" @if($user->status==0) selected @endif>موقوف</option>
                                                </select>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-4" for="password">كلمة المرور </label>
                                                <input type="password" name="password" id="password" class="form-control col-sm-8" autocomplete="off">
                                                <div class="col-sm-4"></div>
                                                <div class="col-sm-8">أترك الحقل فارغا إذا لم تكن تريد تغيير الحقل</div>
                                            </div>

                                        </div>

                                        <div class="col-sm-9"></div>
                                        <div class="col-sm-3">
                                            <div class="form-group mt-20">
                                                <button type="submit" class="btn btn-primary pull-left" style="width: 120px"><i class="fa fa-save"></i> <span>حفظ</span></button>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>

                            <div class="col-sm-5"></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection