<?php

Route::group(["namespace" => "Company", 'prefix' => 'erp/{company}/', "as"=>"company.", "middleware"=>"CompantMid"], function ($comapny) {

    Route::get('/', "Companies@index")->name("home");
    Route::get('/homepage', "Companies@homepage")->name("homepage");
    Route::get('/login', "Companies@login")->name("login");
    Route::post('/do_login', "Companies@do_login")->name("do_login");
    Route::get('/logout', "Companies@logout")->name("logout");
    Route::get('/password/request', "Companies@showLinkRequestForm")->name("password.request");
    Route::post('/password/email', "Companies@sendResetLinkEmail")->name("password.email");

    // Settings
    Route::get('/general_settings', "Companies@general_settings")->name("general_settings");
    Route::post('/general_settings_save1', "Companies@general_settings_save1")->name("general_settings_save1");
    Route::post('/save_generals_smtp', "Companies@save_generals_smtp")->name("save_generals_smtp");
    Route::post('/save_yamamah_sms', "Companies@save_yamamah_sms")->name("save_yamamah_sms");
    Route::post('/save_pusher', "Companies@save_pusher")->name("save_pusher");
    Route::post('/save_zoom', "Companies@save_zoom")->name("save_zoom");
    Route::post('/save_role', "Companies@save_role")->name("save_role");
    Route::post('/update_role', "Companies@update_role")->name("update_role");
    Route::get('/delete_role/{id?}', "Companies@delete_role")->name("delete_role");
    Route::post('/save_permissions', "Companies@save_permissions")->name("save_permissions");
    Route::get('/messages_archive', "Companies@messages_archive")->name("messages_archive");
    Route::post('/messages_archive_save', "Companies@messages_archive_save")->name("messages_archive_save");
    Route::get('/notifications', "Companies@notifications")->name("notifications");
    Route::get('/notification/{id?}', "Companies@notification")->name("notification");
    Route::get('/notification_readed', "Companies@notification_readed")->name("notification_readed");


    // Calendar

    Route::get('/calendar', "Companies@calendar")->name("calendar");
    Route::post('/save_calendar', "Companies@save_calendar")->name("save_calendar");
    Route::get('/delete_event/{id?}', "Companies@delete_event")->name("delete_event");
    Route::post('/update_event', "Companies@update_event")->name("update_event");

    // Inbox
    Route::get('/inbox', "Companies@inbox")->name("inbox");
    Route::get('/outbox', "Companies@outbox")->name("outbox");
    Route::get('/draft', "Companies@draft")->name("draft");
    Route::get('/email_details/{id?}', "Companies@email_details")->name("email_details");
    Route::post('/sendEmail', "Companies@sendEmail")->name("sendEmail");

    // Users
    Route::resource("users", "UserController");

    // Clients
    Route::resource("clients", "ClientController");

    // Meetings
    require ("meetings.php");

    // HRM
    require ("prm.php");

    // PASSWORDS
    require ("passwords.php");

    // HRM
    require ("hrm.php");



});