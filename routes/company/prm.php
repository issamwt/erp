<?php

Route::group(['prefix' => 'prm', "as"=>"prm."], function () {
    Route::get('/statistics', "Prm@statistics")->name("statistics");

    // Settings

    Route::get('/settings', "Prm@settings")->name("settings");
    Route::post('/settings/add_task_status', "Prm@add_task_status")->name("settings.add_task_status");
    Route::get('/settings/delete_task_status/{id}', "Prm@delete_task_status")->name("settings.delete_task_status");
    Route::post('/settings/edit_task_status', "Prm@edit_task_status")->name("settings.edit_task_status");
    Route::post('/settings/add_projectType', "Prm@add_projectType")->name("settings.add_projectType");
    Route::get('/settings/delete_projectType/{id}', "Prm@delete_projectType")->name("settings.delete_projectType");
    Route::post('/settings/update_ProjectType', "Prm@update_ProjectType")->name("settings.update_ProjectType");
    Route::post('/settings/add_expense_category', "Prm@add_expense_category")->name("settings.add_expense_category");
    Route::get('/settings/delete_expense_category/{id}', "Prm@delete_expense_category")->name("settings.delete_expense_category");
    Route::post('/settings/update_expense_category', "Prm@update_expense_category")->name("settings.update_expense_category");
    Route::post('/settings/add_tax', "Prm@add_tax")->name("settings.add_tax");
    Route::get('/settings/delete_tax/{id}', "Prm@delete_tax")->name("settings.delete_tax");
    Route::post('/settings/update_tax', "Prm@update_tax")->name("settings.update_tax");
    Route::post('/settings/add_percentage', "Prm@add_percentage")->name("settings.add_percentage");
    Route::get('/settings/delete_percentage/{id}', "Prm@delete_percentage")->name("settings.delete_percentage");
    Route::post('/settings/update_percentage', "Prm@update_percentage")->name("settings.update_percentage");
    Route::post('/settings/add_invoice_category', "Prm@add_invoice_category")->name("settings.add_invoice_category");
    Route::get('/settings/delete_invoice_category/{id}', "Prm@delete_invoice_category")->name("settings.delete_invoice_category");
    Route::post('/settings/update_invoice_category', "Prm@update_invoice_category")->name("settings.update_invoice_category");
    Route::post('/settings/add_payment_method', "Prm@add_payment_method")->name("settings.add_payment_method");
    Route::get('/settings/delete_payment_method/{id}', "Prm@delete_payment_method")->name("settings.delete_payment_method");
    Route::post('/settings/update_payment_method', "Prm@update_payment_method")->name("settings.update_payment_method");
    Route::post('/save_permissions', "Prm@save_permissions")->name("save_permissions");
    Route::get('/save_tabtab/{tab?}', "Prm@save_tabtab")->name("save_tabtab");

    Route::post('/savenotif', "Prm@savenotif")->name("savenotif");


    //Projects

    Route::get('/projects', "Prm@projects")->name("projects");
    Route::get('/add_project', "Prm@add_project")->name("add_project");
    Route::post('/add_city', "Prm@add_city")->name("add_city");
    Route::post('/save_project', "Prm@save_project")->name("save_project");
    Route::get('/edit_project/{id?}', "Prm@edit_project")->name("edit_project");
    Route::get('/delete_project/{id?}', "Prm@delete_project")->name("delete_project");
    Route::post('/update_project', "Prm@update_project")->name("update_project");
    Route::get('/pdf_project/{id?}', "Prm@pdf_project")->name("pdf_project");
    Route::get('/project_status/{id?}/{status?}', "Prm@project_status")->name("project_status");

    // Project

    Route::get('/project/{id?}', "Prm@project")->name("project");
    Route::get('/projectTab/{num?}', "Prm@projectTab")->name("projectTab");
    Route::post('/addCollaborator', "Prm@addCollaborator")->name("addCollaborator");
    Route::post('/sendCollaborator', "Prm@sendCollaborator")->name("sendCollaborator");
    Route::post('/deleteCollaborator', "Prm@deleteCollaborator")->name("deleteCollaborator");
    Route::get('/change_timer/{project_id?}/{start?}', "Prm@change_timer")->name("change_timer");
    Route::post('/change_timer/{project_id?}/{start?}', "Prm@change_timer")->name("change_timer");

    // Milestones

    Route::get('/addMilestone/{project_id?}', "Prm@addMilestone")->name("addMilestone");
    Route::post('/saveMilestone', "Prm@saveMilestone")->name("saveMilestone");
    Route::get('/editMilestone/{id?}', "Prm@editMilestone")->name("editMilestone");
    Route::post('/updateMilestone', "Prm@updateMilestone")->name("updateMilestone");
    Route::get('/deleteMilestone/{id?}', "Prm@deleteMilestone")->name("deleteMilestone");

    // Tasks

    Route::get('/addTask/{project_id?}', "Prm@addTask")->name("addTask");
    Route::post('/saveTask', "Prm@saveTask")->name("saveTask");
    Route::post('/saveMilestonet', "Prm@saveMilestonet")->name("saveMilestonet");
    Route::get('/showTask/{id?}', "Prm@showTask")->name("showTask");
    Route::get('/statusTask/{id?}/{status?}', "Prm@statusTask")->name("statusTask");
    Route::get('/editTask/{id?}', "Prm@editTask")->name("editTask");
    Route::post('/updateTask', "Prm@updateTask")->name("updateTask");
    Route::get('/deleteTaskAttachment/{id?}', "Prm@deleteTaskAttachment")->name("deleteTaskAttachment");
    Route::get('/deleteTask/{id?}', "Prm@deleteTask")->name("deleteTask");

    // Subtasks

    Route::post('/add_sub_task', 'Prm@add_sub_task')->name('add_sub_task');
    Route::post('/confirm_task', 'Prm@confirm_task')->name('confirm_task');
    Route::post('/edit_task', 'Prm@edit_task')->name('edit_task');
    Route::post('/delete_stask', 'Prm@delete_stask')->name('delete_stask');
    Route::post('/saveTaskComment', 'Prm@saveTaskComment')->name('saveTaskComment');
    Route::get('/deleteTaskComment/{id?}', 'Prm@deleteTaskComment')->name('deleteTaskComment');
    Route::post('/saveReplyTaskComment', 'Prm@saveReplyTaskComment')->name('saveReplyTaskComment');

    // Gannt Project

    Route::get('/projects/gantt/{id?}', 'Prm@gantt')->name('project.gantt');
    Route::post('/projects/gantt_update', 'Prm@gantt_update')->name('project.gantt_update');

    // Discussions

    Route::get('/addDiscussion/{id?}', 'Prm@addDiscussion')->name('addDiscussion');
    Route::post('/saveDiscussion', 'Prm@saveDiscussion')->name('saveDiscussion');
    Route::get('/deleteDiscussion/{id?}', 'Prm@deleteDiscussion')->name('deleteDiscussion');

    // Files

    Route::post('/saveFile', 'Prm@saveFile')->name('saveFile');
    Route::post('/saveFileType', 'Prm@saveFileType')->name('saveFileType');
    Route::get('/downloadFile/{id?}', 'Prm@downloadFile')->name('downloadFile');
    Route::get('/deleteFile/{id?}', 'Prm@deleteFile')->name('deleteFile');
    Route::get('/deleteFileType/{id?}', 'Prm@deleteFileType')->name('deleteFileType');


    // Timers

    Route::post('/saveTimer', 'Prm@saveTimer')->name('saveTimer');
    Route::get('/deleteTimer/{id?}', 'Prm@deleteTimer')->name('deleteTimer');
    Route::post('/updateTimer/{id?}', 'Prm@updateTimer')->name('updateTimer');

    // Tasks
    Route::get('/tasks', 'Prm@tasks')->name('tasks');


















});