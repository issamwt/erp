<?php

Route::group(['prefix' => 'hrm', "as"=>"hrm."], function () {
    Route::get('/statistics', "Hrm@statistics")->name("statistics");

    // Settings

    Route::get('/settings', "Hrm@settings")->name("settings");
    Route::post('/save_designation', "Hrm@save_designation")->name("save_designation");
    Route::post('/update_designation', "Hrm@update_designation")->name("update_designation");
    Route::get('/delete_designation/{id?}', "Hrm@delete_designation")->name("delete_designation");
    Route::post('/save_vacation_category', "Hrm@save_vacation_category")->name("save_vacation_category");
    Route::post('/update_vacation_category', "Hrm@update_vacation_category")->name("update_vacation_category");
    Route::get('/delete_vacation_category/{id?}', "Hrm@delete_vacation_category")->name("delete_vacation_category");
    Route::post('/save_violation', "Hrm@save_violation")->name("save_violation");
    Route::post('/update_violation', "Hrm@update_violation")->name("update_violation");
    Route::get('/delete_violation/{id?}', "Hrm@delete_violation")->name("delete_violation");
    Route::post('/save_location', "Hrm@save_location")->name("save_location");
    Route::post('/update_location', "Hrm@update_location")->name("update_location");
    Route::get('/delete_location/{id?}', "Hrm@delete_location")->name("delete_location");
    Route::post('/save_itemh', "Hrm@save_itemh")->name("save_itemh");
    Route::post('/update_itemh', "Hrm@update_itemh")->name("update_itemh");
    Route::get('/delete_itemh/{id?}', "Hrm@delete_itemh")->name("delete_itemh");
    Route::post('/save_document', "Hrm@save_document")->name("save_document");
    Route::post('/update_document', "Hrm@update_document")->name("update_document");
    Route::get('/delete_document/{id?}', "Hrm@delete_document")->name("delete_document");
    Route::get('/download_document/{id?}', "Hrm@download_document")->name("download_document");
    Route::post('/save_department', "Hrm@save_department")->name("save_department");
    Route::post('/update_department', "Hrm@update_department")->name("update_department");
    Route::get('/delete_department/{id?}', "Hrm@delete_department")->name("delete_department");
    Route::post('/save_section', "Hrm@save_section")->name("save_section");
    Route::post('/update_section', "Hrm@update_section")->name("update_section");
    Route::get('/delete_section/{id?}', "Hrm@delete_section")->name("delete_section");


    Route::post('/savenotif', "Hrm@savenotif")->name("savenotif");

    // InsurancenFinanceInfos

    Route::get('/insurance_n_finance_infos', "Hrm@insurance_n_finance_infos")->name("insurance_n_finance_infos");
    Route::post('/save_allowance_category', "Hrm@save_allowance_category")->name("save_allowance_category");
    Route::post('/update_allowance_category', "Hrm@update_allowance_category")->name("update_allowance_category");
    Route::get('/delete_allowance_category/{id?}', "Hrm@delete_allowance_category")->name("delete_allowance_category");
    Route::post('/save_deduction_category', "Hrm@save_deduction_category")->name("save_deduction_category");
    Route::post('/update_deduction_category', "Hrm@update_deduction_category")->name("update_deduction_category");
    Route::get('/delete_deduction_category/{id?}', "Hrm@delete_deduction_category")->name("delete_deduction_category");
    Route::post('/save_medical_category', "Hrm@save_medical_category")->name("save_medical_category");
    Route::post('/update_medical_category', "Hrm@update_medical_category")->name("update_medical_category");
    Route::get('/delete_medical_category/{id?}', "Hrm@delete_medical_category")->name("delete_medical_category");
    Route::post('/save_social_category', "Hrm@save_social_category")->name("save_social_category");
    Route::post('/update_social_category', "Hrm@update_social_category")->name("update_social_category");
    Route::get('/delete_social_category/{id?}', "Hrm@delete_social_category")->name("delete_social_category");

    // Employees

    Route::get('/employees', "Hrm@employees")->name("employees");
    Route::get('/add_employee', "Hrm@add_employee")->name("add_employee");
    Route::post('/save_employee', "Hrm@save_employee")->name("save_employee");
    Route::post('/get_sects_by_dep', "Hrm@get_sects_by_dep")->name("get_sects_by_dep");
    Route::get('/employee/{id?}', "Hrm@employee")->name("employee");
    Route::get('/edit_employee/{id?}', "Hrm@edit_employee")->name("edit_employee");
    Route::post('/update_employee', "Hrm@update_employee")->name("update_employee");
    Route::get('/delete_employee/{id?}', "Hrm@delete_employee")->name("delete_employee");

    // Attendance

    Route::get('/save_attendance', "Hrm@save_attendance")->name("save_attendance");
    Route::get('/attendances', "Hrm@attendances")->name("attendances");
    Route::post('/filter_attendance', "Hrm@filter_attendance")->name("filter_attendance");

    // Finances

    Route::get('/finances/{id?}', "Hrm@finances")->name("finances");
    Route::get('/tabsnum/{num?}', "Hrm@tabsnum")->name("tabsnum");
    Route::post('/add_allowance', "Hrm@add_allowance")->name("add_allowance");
    Route::get('/delete_allowance/{id?}', "Hrm@delete_allowance")->name("delete_allowance");
    Route::post('/update_allowance', "Hrm@update_allowance")->name("update_allowance");
    Route::post('/add_deduction', "Hrm@add_deduction")->name("add_deduction");
    Route::get('/delete_deduction/{id?}', "Hrm@delete_deduction")->name("delete_deduction");
    Route::post('/update_deduction', "Hrm@update_deduction")->name("update_deduction");
    Route::post('/add_advance', "Hrm@add_advance")->name("add_advance");
    Route::get('/delete_advance/{id?}', "Hrm@delete_advance")->name("delete_advance");
    Route::post('/update_advance', "Hrm@update_advance")->name("update_advance");
    Route::post('/add_custody', "Hrm@add_custody")->name("add_custody");
    Route::get('/delete_custody/{id?}', "Hrm@delete_custody")->name("delete_custody");
    Route::post('/update_custody', "Hrm@update_custody")->name("update_custody");
    Route::get('/delivered_custody/{id?}', "Hrm@delivered_custody")->name("delivered_custody");

    // Adminisitrative Affairs

    Route::get('/adminisitrative_affairs/{id?}', "Hrm@adminisitrative_affairs")->name("adminisitrative_affairs");
    Route::post('/add_vacation', "Hrm@add_vacation")->name("add_vacation");
    Route::get('/delete_vacation/{id?}', "Hrm@delete_vacation")->name("delete_vacation");
    Route::post('/update_vacation', "Hrm@update_vacation")->name("update_vacation");
    Route::post('/add_training', "Hrm@add_training")->name("add_training");
    Route::get('/delete_training/{id?}', "Hrm@delete_training")->name("delete_training");
    Route::post('/update_training', "Hrm@update_training")->name("update_training");

    Route::post('/add_mandate', "Hrm@add_mandate")->name("add_mandate");
    Route::get('/delete_mandate/{id?}', "Hrm@delete_mandate")->name("delete_mandate");
    Route::post('/update_mandate', "Hrm@update_mandate")->name("update_mandate");

    // Payroll

    Route::get('/payroll', "Hrm@payroll")->name("payroll");
    Route::post('/filter_pay', "Hrm@filter_pay")->name("filter_pay");
    Route::post('/save_reward', "Hrm@save_reward")->name("save_reward");
    Route::get('/delete_reward/{id?}', "Hrm@delete_reward")->name("delete_reward");
    Route::post('/save_substraction', "Hrm@save_substraction")->name("save_substraction");
    Route::get('/delete_substraction/{id?}', "Hrm@delete_substraction")->name("delete_substraction");

    // Vacation

    Route::get("/vacations", "Hrm@vacations")->name("vacations");
    Route::get('/accept_vacation/{id?}', "Hrm@accept_vacation")->name("accept_vacation");
    Route::get('/reject_vacation/{id?}', "Hrm@reject_vacation")->name("reject_vacation");

    // Evaluations

    Route::get("/evaluations", "Hrm@evaluations")->name("evaluations");
    Route::get("/get_items_hrm/{user_id?}/{des_id?}", "Hrm@get_items_hrm")->name("get_items_hrm");
    Route::get("/delete_ev/{id?}", "Hrm@delete_ev")->name("delete_ev");
    Route::post("/save_evaluation", "Hrm@save_evaluation")->name("save_evaluation");

    // Permissiosn
    Route::post('/save_permissions', "Hrm@save_permissions")->name("save_permissions");

    // Accounting
    Route::get("/accounting", "Hrm@accounting")->name("accounting");
    Route::post("/send_accounting", "Hrm@send_accounting")->name("send_accounting");

});