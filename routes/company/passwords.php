<?php

Route::group(['prefix' => 'passwords', "as"=>"passwords."], function () {

    // Settings
    Route::get('/settings', "Passwords@settings")->name("settings");
    Route::post('/save_permissions', "Passwords@save_permissions")->name("save_permissions");

    // Websites

    Route::get('/websites', "Passwords@websites")->name("websites");
    Route::get('/add_website', "Passwords@add_website")->name("add_website");
    Route::post('/save_website', "Passwords@save_website")->name("save_website");
    Route::get('/edit_website/{id?}', "Passwords@edit_website")->name("edit_website");
    Route::post('/update_website', "Passwords@update_website")->name("update_website");
    Route::get('/delete_website/{id?}', "Passwords@delete_website")->name("delete_website");

    Route::post('/savenotif', "Passwords@savenotif")->name("savenotif");


});