<?php

Route::group(['prefix' => 'meetings', "as"=>"meetings."], function () {
    Route::get('/statistics', "Meetings@statistics")->name("statistics");
    Route::get('/settings', "Meetings@settings")->name("settings");
    Route::post('/save_category', "Meetings@save_category")->name("save_category");
    Route::post('/update_category', "Meetings@update_category")->name("update_category");
    Route::get('/delete_category/{id}', "Meetings@delete_category")->name("delete_category");
    Route::post('/save_item', "Meetings@save_item")->name("save_item");
    Route::post('/update_item', "Meetings@update_item")->name("update_item");
    Route::get('/delete_item/{id}', "Meetings@delete_item")->name("delete_item");
    Route::post('/save_location_settings', "Meetings@save_location_settings")->name("save_location_settings");
    Route::post('/update_location_settings', "Meetings@update_location_settings")->name("update_location_settings");
    Route::get('/delete_location_settings/{id}', "Meetings@delete_location_settings")->name("delete_location_settings");
    Route::post('/save_permissions', "Meetings@save_permissions")->name("save_permissions");
    Route::post('/save_meetings_settings', "Meetings@save_meetings_settings")->name("save_meetings_settings");

    Route::post('/savenotif', "Meetings@savenotif")->name("savenotif");

    //Meeting index
    Route::get('/meetings', "Meetings@meetings")->name("meetings");
    Route::get('/add_meeting', "Meetings@add_meeting")->name("add_meeting");
    Route::get('/meeting/{id?}', "Meetings@meeting")->name("meeting");
    Route::post('/meeting_nav', "Meetings@meeting_nav")->name("meeting_nav");
    Route::post('/save_meeting', "Meetings@save_meeting")->name("save_meeting");
    Route::post('/save_location', "Meetings@save_location")->name("save_location");
    Route::get('/edit_meeting/{id}', "Meetings@edit_meeting")->name("edit_meeting");
    Route::post('/update_meeting', "Meetings@update_meeting")->name("update_meeting");
    Route::get('/finish_meeting/{id}', "Meetings@finish_meeting")->name("finish_meeting");
    // Meetings Tasks
    Route::get('/tasks', "Meetings@tasks")->name("tasks");
    // Meeting Axis
    Route::post('/save_axis', "Meetings@save_axis")->name("save_axis");
    Route::post('/save_axis_comment', "Meetings@save_axis_comment")->name("save_axis_comment");
    Route::post('/save_axis_attachments', "Meetings@save_axis_attachments")->name("save_axis_attachments");
    Route::get('/delete_axis/{id?}', "Meetings@delete_axis")->name("delete_axis");
    Route::post('/update_axis', "Meetings@update_axis")->name("update_axis");
    Route::get('/start_axis/{id?}', "Meetings@start_axis")->name("start_axis");
    Route::get('/finish_axis/{id?}', "Meetings@finish_axis")->name("finish_axis");
    Route::post('/reorder_axes/{id?}', "Meetings@reorder_axes")->name("reorder_axes");
    Route::get('/cancel_axis/{id?}', "Meetings@cancel_axis")->name("cancel_axis");
    Route::post('/delay_axis/{id?}', "Meetings@delay_axis")->name("delay_axis");

    // Meeting Task
    Route::post('/save_task', "Meetings@save_task")->name("save_task");
    Route::post('/update_task', "Meetings@update_task")->name("update_task");
    Route::get('/delete_task/{id?}', "Meetings@delete_task")->name("delete_task");
    Route::get('/change_task_status/{id?}/{status?}', "Meetings@change_task_status")->name("change_task_status");
    // Meeting Attachments
    Route::post('/save_attachment', "Meetings@save_attachment")->name("save_attachment");
    Route::get('/delete_attachment/{id?}', "Meetings@delete_attachment")->name("delete_attachment");
    // Meeting Votes
    Route::post('/save_vote', "Meetings@save_vote")->name("save_vote");
    Route::post('/update_vote', "Meetings@update_vote")->name("update_vote");
    Route::get('/delete_vote/{id?}', "Meetings@delete_vote")->name("delete_vote");
    Route::get('/start_vote/{id?}/{status?}', "Meetings@start_vote")->name("start_vote");
    Route::post('/vote_vote', "Meetings@vote_vote")->name("vote_vote");
    // Meeting Evaluation
    Route::post('/save_evaluation', "Meetings@save_evaluation")->name("save_evaluation");
    Route::get('/delete_evaluation/{id?}', "Meetings@delete_evaluation")->name("delete_evaluation");
    Route::post('/update_evaluation', "Meetings@update_evaluation")->name("update_evaluation");
    // Meeting Invited
    Route::post('/save_invited', "Meetings@save_invited")->name("save_invited");
    Route::post('/save_invited2', "Meetings@save_invited2")->name("save_invited2");
    // Meeting Buttons
    Route::get('/accept_meeting/{id?}', "Meetings@accept_meeting")->name("accept_meeting");
    Route::get('/zoom_meeting/{id?}', "Meetings@zoom_meeting")->name("zoom_meeting");
    Route::get('/cancel_meeting/{id?}', "Meetings@cancel_meeting")->name("cancel_meeting");
    Route::post('/delay_meeting', "Meetings@delay_meeting")->name("delay_meeting");
    Route::post('/notify_meeting', "Meetings@notify_meeting")->name("notify_meeting");
    Route::get('/pdf_meeting/{id?}', "Meetings@pdf_meeting")->name("pdf_meeting");
    Route::get('/delete_meeting/{id?}', "Meetings@delete_meeting")->name("delete_meeting");
    // Meeting Zoom
    Route::get('/zoom/{id?}', "Meetings@zoom")->name("zoom");

});