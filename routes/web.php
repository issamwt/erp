<?php


//Auth
Auth::routes();
Route::post('/verification/{id}', 'Auth\LoginController@verifyUser')->name('verification');
Route::get('/resend/{id}', 'Auth\LoginController@resendVerificationCode')->name('resend');
Route::get('/send_new/{id}', 'Auth\LoginController@sendNewVerificationCode')->name('send_new');





Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/contact', 'HomeController@contact')->name('contact');
Route::get('/cronjob', 'HomeController@cronjob')->name('cronjob');

Route::get('company/password/reset/{token}', "HomeController@showResetForm")->name("company.password.reset");
Route::post('company/password/reset', "HomeController@reset")->name("company.password.update");

require ("admin.php");

require ("company/company.php");

Route::get('/cmd', function (){
    //return shell_exec("php composer.phar -v");
    phpinfo();
});

Route::get("/meeting/{meeting?}/{user_id?}", "HomeController@meeting")->name("meeting_url");
Route::post("/save_invited", "HomeController@save_invited")->name("save_invited");
Route::get("/reminder", "HomeController@reminder")->name("reminder");

