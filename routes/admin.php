<?php

Route::group(["namespace" => "Admin", 'middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/home', 'HomeController@index')->name("home");
    Route::get('/', 'HomeController@index')->name("home");

    // Admin
    Route::resource("admins", "AdminController");

    // Comapany
    Route::get('/companies', 'CompaniesController@index')->name("companies");
    Route::get('/companies/add_company', 'CompaniesController@add_company')->name("add_company");
    Route::post('/companies/save_company', 'CompaniesController@save_company')->name("save_company");
    Route::get('/companies/edit_company/{id?}', 'CompaniesController@edit_company')->name("edit_company");
    Route::post('/companies/update_company', 'CompaniesController@update_company')->name("update_company");
    Route::get('/companies/delete_company/{id?}', 'CompaniesController@delete_company')->name("delete_company");

    // Users
    Route::resource("users", "UserController");

    // Clients
    Route::resource("clients", "ClientController");

    // messages
    Route::resource("messages", "MessageController");

    // Settings
    Route::get('/settings', 'SettingsController@index')->name("settings.index");
    //Route::post('/save_generals_smtp', 'SettingsController@save_generals_smtp')->name("settings.save_generals_smtp");
    //Route::post('/save_generals_nexmo', 'SettingsController@save_generals_nexmo')->name("settings.save_generals_nexmo");
    //Route::post('/save_pusher', 'SettingsController@save_pusher')->name("settings.save_pusher");

});